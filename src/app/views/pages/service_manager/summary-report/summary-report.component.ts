import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { ComponentPortal } from '@angular/cdk/portal';
@Component({
	selector: 'kt-summary-report',
	templateUrl: './summary-report.component.html',
	styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {
	[x: string]: any;
	from: NgbDateStruct;
	to: NgbDateStruct;
	service_group: any;
	techId: any;
	assesment_data: any;
	visit_count: any;
	from_date: any;
	to_date: any;
	summary_report: [];
	employee_code: any;
	filterSummary: FormGroup;    //form group 
	filterSegment: FormGroup;
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	spares: any;
	spare_request_details: any;
	product_id: any;
	today: any;
	status: any;
	service_id: any;
	showTable: boolean = false;
	dataTable: any;
	loadingSub: boolean = false;    //button spinner by Sowndarya
	loadingEX: boolean = false;
	dtInstance = {};
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	overlayRef: OverlayRef;
	segments: any;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })

	@ViewChild('dataTable', { static: true }) table;
	customer_code: any;
	segment_id: string;
	states: any;
	setr_export_report: any;
	region_wise: string;
	assigned_state: any;
	minDate: NgbDateStruct;
	maxDate: NgbDateStruct;
	maxDate2: NgbDateStruct;
	minDate2: NgbDateStruct;
	// settingsObj: any = {
	// 	"order": [[1, "asc"]],
	// 	deferRender: true,
	// 	scrollY: 200,
	// 	scrollCollapse: true,
	// 	scroller: true
	// }
	// return_draw: number;

	constructor(config: NgbDatepickerConfig, private ajax: ajaxservice, private renderer: Renderer2, private modalService: NgbModal, private chRef: ChangeDetectorRef, private overlay: Overlay, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		var current_month: number = new Date().getUTCMonth();
		var current_year: number = new Date().getFullYear();

		console.log(current_month, "current month")
		// if (current_month > 2) {
		// 	config.minDate = { "year": current_year, "month": current_month - 1, "day": 1 };
		// 	config.maxDate = { year: current_year, month: current_month + 1, day: 31 };
		// }
		// else if (current_month == 1 || current_month == 0) {
		// 	config.minDate = { "year": current_year - 1, "month": current_month - 1, "day": 1 };
		// 	config.maxDate = { year: current_year, month: current_month + 1, day: 31 };
		// }
		// this.minDate = config.minDate
		// this.maxDate = config.maxDate
		this.maxDate2 = { year: current_year, month: current_month, day: 28 };
		console.log(this.from, "this.from")
		console.log(this.to, "this.to")
		this.filterSummary = new FormGroup({
			"customer_code": new FormControl(""),
			"employee_code": new FormControl(localStorage.getItem('employee_code')),
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to, Validators.required),
			"segment_id": new FormControl('', Validators.required),
			"status": new FormControl('', Validators.required),
			"setr_details": new FormControl('2', Validators.required),
			"region": new FormControl(''),
		})
		this.filterSegment = new FormGroup({
			"employee_code": new FormControl(localStorage.getItem('employee_code')),
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to, Validators.required),
			"segment_id": new FormControl(""),
			"setr_details": new FormControl('', Validators.required),
			"status": new FormControl('2', Validators.required),
			"region": new FormControl(''),
		})
		console.log(this.filterSummary.value.from_date, 'from date')
		console.log(this.filterSummary.value.to_date, "to date")
	}


	ngOnInit() {
        this.excelerror = "";
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.overlayRef.attach(this.LoaderComponentPortal);
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');

		this.assigned_state = localStorage.getItem('assigned_state')
		if (this.assigned_state == "All") {
			this.region_wise = 'Yes'
		}
		else {
			this.region_wise = 'No'
		}
		this.today = Fdate;
		var params = {
			"to_date": Fdate,
			"from_date": Fdate,
			'status': null,
			"segment_id": null,
			"region": null,
			"setr_details": "2",
			'customer_code': localStorage.getItem('customer_code')
		}
		this.getSummary(params, 0);
		this.customer_code = localStorage.getItem('customer_code')
		// this.get_summary_report(this.customer_code)
		// this.get_ticket_product_location();
		// this.getServices();
		this.getsegment();
		this.getstate();

	}
	getstate() {
		this.excelerror = ""
		var employee_code = localStorage.getItem('employee_code')
		var url = 'get_state_manager/?';             // api url for getting the details with using post params
		var id = "employee_code";
		console.log(url, id, employee_code);
		this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.states = result.response.data;

			}

		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});

	}
	getsegment() {
		this.excelerror = ""
		var employee_code = localStorage.getItem('employee_code');
		console.log(employee_code, "this.employee_code")
		var data = { "employee_code": employee_code };// storing the form group value
		console.log(data, "datasegment")
		var url = 'get_segment/'                                         //api url of remove license
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.segments = result.response.data;

				// this.overlayRef.detach();
			}

			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');        //toastr message for error
				// this.overlayRef.detach();
			}
			else {
				this.toastr.error(result.response.message, 'Error');        //toastr message for error
			}
		}, (err) => {
			console.log(err);                                             //prints if it encounters an error
		});

	}
	exportAsXLSX(): void {
		this.excelerror = ""
		this.filterSegmentF();
		// this.excelservice.exportAsExcelFile(this.summary_report, 'summary_report');
		// this.excelservice.exportAsExcelFile(this.setr_export_report, 'setr_report');
	}

	getServices() {
		this.excelerror = ""
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'get_servicegroup/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.service_group = result.response.data;
			}
			else if (result.response.response_code == "500") {

			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	spareDetails(data) { //get Spare Details
		this.excelerror = ""
		this.spares = data;
		console.log(data);
		var tk_id = this.spares.ticket_id;
		var ticket_id = "ticket_id";
		var url = 'spare_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_request_details = result.response.data; //bind result in this variable
				console.log(this.spare_request_details);
			}
			else if (result.response.response_code == "500") {

			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	get_ticket_product_location() {
		this.excelerror = ""
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var data = { "employee_code": emp_code };
		var url = 'get_ticket_product_location/';                                  // api url for getting the details
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.products = result.response.product;              //storing the api response in the array
			}
			else if (result.response.response_code == "500") {
				console.log(result.response.message);
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterSummaryF() {
		this.excelerror = ""
		console.log(this.filterSummary.value, "this.filterSummary.value")
		this.from_date = this.filterSummary.value.from_date;
		this.to_date = this.filterSummary.value.to_date;
		this.status = this.filterSummary.value.status;
		this.segment_id = this.filterSummary.value.segment_id;
		this.setr_details = this.filterSummary.value.setr_details;
		if (this.filterSummary.value.from_date.year == undefined) {
			this.filterSummary.value.from_date = this.filterSummary.value.from_date;
		}
		else {
			var Fyear = this.filterSummary.value.from_date.year;
			var Fmonth = this.filterSummary.value.from_date.month;
			var Fday = this.filterSummary.value.from_date.day;
			this.filterSummary.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
		}
		if (this.filterSummary.value.to_date.year == undefined) {
			this.filterSummary.value.to_date = this.filterSummary.value.to_date;
		}
		else {
			var Tyear = this.filterSummary.value.to_date.year;
			var Tmonth = this.filterSummary.value.to_date.month;
			var Tday = this.filterSummary.value.to_date.day;
			this.filterSummary.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;

		}
		if (this.status == "" || this.status == '0' || this.status == null) {
			this.filterSummary.value.status = null;
		}
		else {
			var status = +this.filterSummary.value.status
			this.filterSummary.value.status = status;
		}
		if (this.segment_id == "" || this.segment_id == '0' || this.segment_id == null) {
			this.filterSummary.value.segment_id = null;
		}
		else {
			var segment_id = +this.filterSummary.value.segment_id
			this.filterSummary.value.segment_id = segment_id;
		}

		this.filterSummary.value.customer_code = this.customer_code;
		this.filterSummary.value.employee_code = localStorage.getItem('employee_code')
		if (this.filterSummary.value.region == "") {
			this.filterSummary.value.region = null
		}
		else {
			this.filterSummary.value.region = this.filterSummary.value.region
		}
		this.showTable = false;
		this.chRef.detectChanges();
		this.getSummary(this.filterSummary.value, 1);
	}

	getSummary(data, type) {
		this.excelerror = ""
		this.loadingSub = true;
		this.summary_report = [];
		this.return_draw = 0;
		var length = 10
		var start = 0
		var draw = this.return_draw
		var sort = "desc"
		var column = ""
		// if(this.name_product==null||this.name_product==undefined||this.name_product==''){
		//   var search_value = "";
		// }
		// else{
		//    search_value = this.name_product;
		// }
		//  console.log(search_value)
		var search_value = ""
		data['employee_code'] = localStorage.getItem('employee_code')
		data['length'] = length
		data['start'] = start
		data['draw'] = draw
		data['search_value'] = search_value
		data['sort'] = sort
		data['column'] = column
		var url = "summery_report/"
		//var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
		// api url for getting the details                                  

		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.summary_report = []
				// this.unassigned_tckt = result.response.data;              //storing the api response in the array    
				this.showTable = true;
				// this.overlayRef.detach();           //detach the loader
				// this.chRef.detectChanges();         //detech the changes 
				this.summary_report = result.response.data;              //storing the api response in the array             
				// this.showTable = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();
				this.settingsObj = {

					"deferLoading": result.response.recordsTotal,

					'processing': true,
					'serverSide': true,
					'serverMethod': 'post',

					ajax: (dataTablesParameters: any, callback) => {
						var data = dataTablesParameters;
						var method = "post";
						var limit = data.limit
						var draw = data.draw
						var leng = data.length
						var length = data.length

						var start = data.start
						var search_value = data.search.value
						console.log(search_value, "search_value")
						// var country_name = data.columns['Country']
						// var state_name = data.columns['State']
						// var city_name = data.columns['City']
						var sort = data.order[0]['dir']
						var column = data.order[0]['column']
						// var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, 
						var data1 = data
						data1['length'] = length
						data1['start'] = start
						data1['draw'] = draw
						data1['search_value'] = search_value
						data1['sort'] = sort
						data1['column'] = column
						data1['employee_code'] = localStorage.getItem('employee_code')
						if (this.status == "" || this.status == '0' || this.status == null) {
							this.filterSummary.value.status = null;
						}
						else {
							var status = +this.filterSummary.value.status
							this.filterSummary.value.status = status;
						}
						if (this.segment_id == "" || this.segment_id == '0' || this.segment_id == null) {
							this.filterSummary.value.segment_id = null;
						}
						else {
							var segment_id = +this.filterSummary.value.segment_id
							this.filterSummary.value.segment_id = segment_id;
						}
						data1['status'] = this.filterSummary.value.status
						data1['segment_id'] = this.filterSummary.value.segment_id
						data1['setr_details'] = this.filterSummary.value.setr_details
						if (this.filterSummary.value.from_date.year == undefined) {
							this.filterSummary.value.from_date = this.filterSummary.value.from_date;
						}
						else {
							var Fyear = this.filterSummary.value.from_date.year;
							var Fmonth = this.filterSummary.value.from_date.month;
							var Fday = this.filterSummary.value.from_date.day;
							this.filterSummary.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
						}
						if (this.filterSummary.value.to_date.year == undefined) {
							this.filterSummary.value.to_date = this.filterSummary.value.to_date;
						}
						else {
							var Tyear = this.filterSummary.value.to_date.year;
							var Tmonth = this.filterSummary.value.to_date.month;
							var Tday = this.filterSummary.value.to_date.day;
							this.filterSummary.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;
				
						}
						if (this.filterSummary.value.region == "") {
							this.filterSummary.value.region = null
						}
						else {
							this.filterSummary.value.region = this.filterSummary.value.region
						}
						data1['from_date'] = this.filterSummary.value.from_date
						data1['to_date'] = this.filterSummary.value.to_date
						data1['region'] = this.filterSummary.value.region
						// var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
						var url_forming = "summery_report/"
						var url = url_forming;
						this.ajax.postdata(url, data).subscribe(result => {
							this.summary_report = result.response.data;              //storing the api response in the array             
							this.return_draw = result.response.draw
							// this.showTable = true;
							callback({
								recordsTotal: result.response.recordsTotal,
								recordsFiltered: result.response.totalRecordswithFilter,
								data: [],

							});
							this.chRef.detectChanges();

						});

					},
					"columnDefs": [
						{ "name": "Ticket Id", "targets": 1, "searchable": true },
						{ "name": "Customer Name", "targets": 2, "searchable": true },
						{ "name": "Product Category", "targets": 3, "searchable": true },
						{ "name": "Model", "targets": 4, "searchable": true },
						{ "name": "Customer Requested Date", "targets": 5, "searchable": true },
						{ "name": "Accepted Time", "targets": 6, "searchable": true },
						{ "name": "Start Time", "targets": 7, "searchable": true },
						{ "name": "Completed Time", "targets": 8, "searchable": true },
						{ "name": "Status", "targets": 9, "searchable": true },


					],
				}

				$('#datatables').DataTable().destroy();
				setTimeout(() => {
					this.table = $('#datatables').DataTable(

						this.settingsObj
					);
				}, 5);
				console.log(this.settingsObj)
			}
			else if (result.response.response_code == "400") {
				this.summary_report = [];
				// this.toastr.error(result.response.message, 'Error');          
				this.showTable = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();


			}
			else if (result.response.response_code = "500") {                                                         //if not Success
				this.toastr.error(result.response.message, 'Error');        //toastr message for error
				this.chRef.detectChanges();
				this.overlayRef.detach();                 //detach the loader
			}
			else {                                                         //if not Success
				this.toastr.error("Some thing went wrong");        //toastr message for error
				this.chRef.detectChanges();
				this.overlayRef.detach();                    //detach the loader
			}
			this.loadingSub = false;
			this.chRef.detectChanges();
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});

		// data['employee_code'] = localStorage.getItem('employee_code')
		// var url = 'summery_report/';                                  // api url for getting the cities
		// if (type == 1) {
		// 	this.loadingSub = true;
		// }

		// this.ajax.postdata(url, data).subscribe((result) => {
		// 	if (result.response.response_code == "200" || result.response.response_code == "400") {
		// 		this.overlayRef.detach();
		// 		this.summary_report = result.response.data;
		// 		this.showTable = true;
		// 		this.chRef.detectChanges();
		// 		this.loadingSub = false;
		// 	}
		// 	else if (result.response.response_code == "500") {
		// 		this.overlayRef.detach();
		// 		this.summary_report = [];
		// 		this.toastr.error(result.response.message, 'Error');
		// 		this.showTable = true;
		// 		this.chRef.detectChanges();
		// 		this.loadingSub = false;

		// 	}
		// 	else {
		// 		this.overlayRef.detach();
		// 		this.summary_report = [];
		// 		this.toastr.error(result.response.message, 'Error');
		// 		this.showTable = true;
		// 		this.chRef.detectChanges();
		// 		this.loadingSub = false;
		// 	}
		// }, (err) => {
		// 	console.log(err);
		// 	this.overlayRef.detach();
		// 	this.loadingSub = false;                                       //prints if it encounters an error
		// });
	}
	// selectsetr(value){
	// 	console.log(value,"12")
    //  if(value == 2){
	// 	console.log("if")
	// 	this.excelerror = ""
	//  }
	// }
	summarysegment(){
		if((this.filterSummary.value.segment_id == null || this.filterSummary.value.segment_id == "" ) && this.filterSummary.value.setr_details == 1 ){
			this.excelerror = "Please Select the Segment";
		}
		else{
			this.excelerror = "";
		}
	}
	filterSegmentF() {
		this.excelerror = ""
		if((this.filterSummary.value.segment_id == null || this.filterSummary.value.segment_id == "" ) && this.filterSummary.value.setr_details == 1 ){
            this.excelerror = "Please Select the Segment";
		}
		else{
			if (this.filterSummary.value.from_date.year == undefined) {
				this.filterSummary.value.from_date = this.filterSummary.value.from_date;
			}
			else {
				var Fyear = this.filterSummary.value.from_date.year;
				var Fmonth = this.filterSummary.value.from_date.month;
				var Fday = this.filterSummary.value.from_date.day;
				this.filterSummary.value.from_date = Fyear + "-" + Fmonth + "-" + 1;
			}
			if (this.filterSummary.value.to_date.year == undefined) {
				this.filterSummary.value.to_date = this.filterSummary.value.to_date;
			}
			else {
				var Tyear = this.filterSummary.value.to_date.year;
				var Tmonth = this.filterSummary.value.to_date.month;
				var Tday = this.filterSummary.value.to_date.day;
				this.filterSummary.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;
	
			}
			if (this.filterSummary.value.status == "") {
				this.filterSummary.value.status = null
			}
			else {
				this.filterSummary.value.status = this.filterSummary.value.status
			}
			if (this.filterSummary.value.segment_id == "") {
				this.filterSummary.value.segment_id = null
			}
			else {
				this.filterSummary.value.segment_id = +this.filterSummary.value.segment_id;
			}
			if (this.filterSummary.value.region == "") {
				this.filterSummary.value.region = null
			}
			else {
				this.filterSummary.value.region = this.filterSummary.value.region
			}
			if (this.filterSummary.value.customer_code == "") {
				this.filterSummary.value.customer_code = null
			}
			else {
				this.filterSummary.value.customer_code = this.filterSummary.value.customer_code
			}
			var data = this.filterSummary.value
			var url = 'summary_export/';
			// var url = 'summary_report_export/';                                  // api url for getting the cities
			// var url = 'setr_export/';
			this.loadingEX = true;
			this.ajax.postdata(url, data).subscribe((result) => {
				if (result.response.response_code == "200") {
					// this.setr_export_report = result.response.data;
					this.excelservice.exportAsExcelFile(result.response.data, 'setr_report');
	
					this.loadingEX = false;
				}
				else if (result.response.response_code == "400") {
					this.toastr.error("There is no data", 'Error');
					this.loadingEX = false;
				}
				else if (result.response.response_code == "500") {
					this.toastr.error(result.response.message, 'Error');
					this.loadingEX = false;
				}
				else {
					this.toastr.error(result.response.message, 'Error');
					this.loadingEX = false;
				}
			}, (err) => {
				console.log(err);
				this.loadingEX = false;                                       //prints if it encounters an error
			});
		}
		
	}
	openLarge(content6) {
		this.modalService.open(content6, {
			size: 'lg'
		});
	}

	dateChange(eve) {   
		this.excelerror = ""                                                                  //datechange event for only 3 months
		var year3: number = eve.year;
		var month3: number = eve.month;
		var day3 = eve.day;
		var newDate = new Date(year3, (month3 + 2), day3);
		var year2: number = newDate.getFullYear()
		var month2: number = newDate.getMonth()
		if (month2 == 0) {
			month2 = 12;
			year2 = year2 - 1;
		}
		else {
			month2 = month2;
			year2 = year2;
		}
		var day2 = new Date(new Date(new Date().setMonth(month2)).setDate(0)).getDate()
		this.minDate2 = { "year": year3, "month": month3, "day": 1 };
		this.maxDate2 = { year: year2, month: month2, day: day2 };
		const EDate: NgbDate = new NgbDate(year2, month2, day2);                            //conversion to ngbdate for datepicker 
		this.to = EDate;
	}
}
