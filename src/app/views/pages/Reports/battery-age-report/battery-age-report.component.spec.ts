import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatteryAgeReportComponent } from './battery-age-report.component';

describe('BatteryAgeReportComponent', () => {
  let component: BatteryAgeReportComponent;
  let fixture: ComponentFixture<BatteryAgeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatteryAgeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatteryAgeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
