import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { ChartComponent } from '@syncfusion/ej2-angular-charts';
import { FormControl } from '@angular/forms';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
let series1: any[] = [];
let yValue = [7.66, 8.03, 8.41, 8.97, 8.77, 8.20, 8.16, 7.89, 8.68, 9.48, 10.11, 11.36, 12.34, 12.60, 12.95,
  13.91, 16.21, 17.50, 22.72, 28.14, 31.26, 31.39, 32.43, 35.52, 36.36,
  41.33, 43.12, 45.00, 47.23, 48.62, 46.60, 45.28, 44.01, 45.17, 41.20, 43.41, 48.32, 45.65, 46.61, 53.34, 58.53];
let point1; let i; let j = 0;
for (i = 1973; i <= 2013; i++) {
  point1 = { x: i, y: yValue[j] };
  series1.push(point1); j++;
}
import { DataTableDirective } from 'angular-datatables';
import { ExcelService } from '../../../../excelservice'
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import * as xlsx from 'xlsx';
@Component({
  selector: 'kt-ticketagereport',
  templateUrl: './ticketagereport.component.html',
  styleUrls: ['./ticketagereport.component.scss']
})
export class TicketagereportComponent implements OnInit {

  public data: Object[] = series1;
  public primaryYAxis: Object = {
    title: '',
    interval: 200
  };
  public primaryXAxis: Object = {
    title: 'Months',
    labelIntersectAction: 'Rotate45'
  };
  showTable: boolean = false;

  public chartArea: Object = {
    border: { width: 0 }
  };
  public tooltip: Object;
  public title: string = 'Pending Complaints Age wise and State wise Breakup (No of days pending)';
  public primaryXAxis1: Object;
  public chartData: Object[];
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  public title1: string;
  public primaryYAxis1: Object;
  public palette: string[];
  public chartData1: Object[];
  total: any;
  Total_total: Object[];
  value: any;
  public marker: Object;

  ChartTrendlines: {
    //...
    //Customize the Trendline styles
    fill: string; width: number; opacity: number; dashArray: string;
  }[];
  dtOptions: DataTables.Settings = {};
  @ViewChild('chartid', { static: false }) chartObj: ChartComponent;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  // @ViewChild('chartid', { static: true })
  // public chartObj: ChartComponent;
  export_table_data: any[];
  ticket_table: any;
  Finmonth: Object[];
  fin_total: Object[];
  @ViewChild('div', { static: true }) div: ElementRef;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  constructor(public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private excelservice: ExcelService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 25, 50]
    };
    this.marker = {
      dataLabel: { visible: true }
    }
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    this.end_date = moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    if (this.start_date.month > 4) {
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear + 1) + "-" + '03' + "-" + '31';
      var finance = Fyear + "-" + (Fyear + 1)
    }
    else {
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance = (Fyear - 1) + "-" + Fyear
    }
    var datas = {
      "employee_code": emp_code, "start_date": this.start_date,
      "end_date": this.end_date, "service_desk": [], "state": [],
      "technician": [],
      "customer": [],
      "segment": [],
      "product": [],
      "product_sub_id": [],
      "model_no": [],
      "region": [],
      "call_catgory": [],
      "flag": 1,
      "finance_year": finance,
      "mfg_date": Fyear + "-" + Fmonth + "-" + 1,
      "fin_flag": "Year"
    };
    this.ticket_age_report(datas)
    this.primaryXAxis = { valueType: 'Category',labelIntersectAction: 'Rotate45' };
    this.title = 'Financial Year wise Complaints';
  }
  exportToExcel() {
    const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'ComplaintAgeWiseReport.xlsx');
  }
  export_table() {
    this.excelservice.exportAsExcelFile(this.ticket_table, 'Financial Reports');
  }
  export() {
    this.chartObj.exportModule.export('PNG', 'Complaint Age Wise Report');

  }
  updateDataMain(datas) {
    this.ticket_age_report(datas)

  }
  ticket_age_report(data) {
    // this.ticket_table = [];
    var url = 'ticketage_wiseReport/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.ticket_table = result.response.data;
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 50,
          lengthMenu: [[50, 100, -1], [50, 100, "All"]],
          data: this.ticket_table,
          search: false,
          processing: true,
          columns: [{ data: 'State', orderable: false }, { data: '0-2' }, { data: '4-6' }, { data: '6-8' }, { data: '8-15' }, { data: 'Above15' }, { data: 'GrandTotal' }
          ],
          "columnDefs": [
            {
              "targets": [], //first column / numbering column
              "orderable": false, //set not orderable
              "visible": false,
              "searchable": false,
            },
          ],
          order: [],
          destroy: true
        };
        this.ticket_table = [];
        this.chartData1 = result.response.chart;              //storing the api response in the array

        $('#datatables').DataTable(this.dtOptions)
        // this.Finmonth = result.response.Finmonth;
        this.showTable = true;
        this.title = 'Pending Complaints Age wise and State wise Breakup (No of days pending)' + " " + "(" + result.response.Financial_Year[0] + "-" + result.response.Financial_Year[1] + ")";
        this.chRef.detectChanges();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
}
