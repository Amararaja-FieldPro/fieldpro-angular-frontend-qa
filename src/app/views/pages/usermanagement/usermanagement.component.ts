import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';   // its for alert
//--------------------------- import for location mapping -----------------------------------//
import { environment } from '../../../../environments/environment';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables'; // datatable
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';



@Component({
  selector: 'kt-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class UsermanagementComponent implements OnInit {
  techerror: string;
  servicedesk_list: any;
  loadingBulk: boolean = false;
  loadingEdit: boolean = false;
  loadingAdd: boolean = false;
  role_type: any
  selectedViewStates: any;
  selectedViewcitys: any;
  selectedViewLocation: any;
  nextId: any;
  editprofile_disable: any;
  selectedStates = [];
  assigned_label: any;
  country_profile: any;
  locations: any = [];
  profile_label: any;
  profile_label_city: any;
  profile_label_location: any;
  state: any = [];
  product_array: any = [];
  citys: any = [];
  stateArr: any = [];
  location_profile: any = [];
  disabled = false;
  ShowFilter = true;
  limitSelection = false;
  citiees = [];
  httplink: any;
  selectedItems = [];
  dropdownSettings: any = {};
  dropdownSettingscountry: any = {};
  dropdownCitySettings: any = {};
  dropdownLocationSettings: any = {};
  dropdownProductSettings: any = {};
  dropdownservicedeskSettings: any = {};
  dropdownsubProductSettings: any = {};
  dropdownservicegroupSettings: any = {};
  node_id: any;
  emp_error: any;
  currentJustify = 'end';
  addcountry: any;
  employee_id: any;
  user_details: any;                                         //variable for get the user details based on the employee_id
  user_detail: any;
  user: any;
  getlevel: any;
  level: any;
  selecteRole: any;
  //level : any = 1;              
  tech: any;                 //variable for get the technician details based on the employee_id
  tech_detail: any;
  EditUser: FormGroup;
  EditCompany: FormGroup;
  EditProfile: FormGroup;                         //Form group variable for edit User
  AddUser: FormGroup;                              //Form group variable for add user
  EditTech: FormGroup;                             //Form group variable for edit technician
  AddProduct: FormGroup;
  AddProfile: FormGroup;
  AddCompany: FormGroup;
  WorkProfile: FormGroup;
  form: FormGroup;
  form1: any;
  form2: any;
  form3: any;
  file_name: any;
  states: any;
  cities: any;
  country: any;                                                             //variable for store the country
  product: any;
  edit_sub: any;
  subproduct: any;
  node: any;
  tech_product_details: any;
  employee_code: any;
  viewtec: any;                                                     //variable for store technician details for view                   
  //----for location mapping---------------//
  Edit_node: FormGroup;
  edit_data: any;
  servicegroup: any;
  getNode: any;
  getorgnode: any;
  value: any;
  node_value: any;
  location: any;                                          //variable for store the location
  showTable: boolean = false;
  overlayRef: OverlayRef;
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  evt: any;
  role: any;
  contactnum_erroe: any;
  email_error: any;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  ByDefault: boolean = false;
  Filename: any;
  activeId: any;
  preId: any;
  activerId: any;
  prerId: any;
  nextrId: any;
  prefId: any;
  EactiveId: any;
  EpreId: any;
  EactiverId: any;
  EprerId: any;
  EnextrId: any;
  EprefId: any;
  data: any;
  validation_data: any;
  params: any;
  showmodelTable: boolean = false;
  showtech: boolean = false;
  TechProfile: FormGroup;
  TechUser: FormGroup;
  TechCompany: FormGroup;
  type: any;
  error: any;
  employee_name: any;
  loadingSubmit: boolean = false;
  org_type: any;
  sleectedStates: any = [];
  selectedCitys: any = [];
  selectedCityName: any = [];
  selectedLocations: any = [];
  selectedLocation_name: any = [];
  bulk: any;
  showmore: any = 5;
  showstate: any = 5;
  punch: string;
  @ViewChild('t', { static: true }) t;
  // @ViewChild('fileUploader', { static: true }) fileUploader:ElementRef;
  // @ViewChild('acc') acc;

  // currentAccordianTab: string;
  @ViewChild('tab', { static: true }) tab: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  product_details: any;
  products: any = [];
  product_label: string;
  subproducts: any = [];
  servicegroups: any = [];
  subproducts_data: any;
  servicegroup_data: any = [];
  product_check: string;
  selectedServicegroup = [];
  selectedProducts = [];
  selectedSubProducts: any = [];
  selectedSub = [];
  state_flag: any;
  service_flag: any;
  product_flag: any;
  city_flag: string;
  location_flag: string;
  subproduct_flag: string;
  userdetails: any;
  errors: string;
  fileUploader: any;
  employee: string;
  country_flag: string;
  profile_label_state: string;
  employee_product: any;
  dtOptions1: DataTables.Settings = {};
  city: string[];
  state_name: any;
  subproduct_ids: any;
  product_ids: any;
  location_name: any;
  user_status: any;
  city_name: any;
  state_names: any;
  alternate: string;
  contact: string;
  select: any = [];
  items: any = [];
  productsall: any = [];
  Editloading: boolean = false;
  all_state: any = [];
  all_city: any = [];
  all_location: any = [];
  service_all: any = [];
  productdata: any;
  service_location: any;
  servcedesk_code: any;
  assigncountry: any;
  assignstates: any;
  technician_type: string;
  service_ids: any;
  type_tech: string;
  sleectedSubproduct: any[];
  product_arr: any = [];
  city__all: any;
  all_subproduct: any;
  selectedStatestech: any;
  service_type: string;
  servicetype_tech: string;
  allocate_seg: any;
  segment_details: any;
  dropdownSegmenttSettings: { singleSelection: boolean; idField: string; textField: string; selectAllText: string; unSelectAllText: string; itemsShowLimit: number; allowSearchFilter: boolean; };
  segment_data: any = [];
  segmentsall: any;
  bulk_error: any;
  bulk_succ: any;
  selectdrop: any;
  // services: any;
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private fb: FormBuilder,
    private router: Router, private toastr: ToastrService, public _location: Location,
    public activeModal: NgbActiveModal, private http: Http, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    this.dtOptions1 = {
      pagingType: 'full_numbers',
      pageLength: 5,
      lengthMenu: [5, 10, 15]
    };
    var level = 1;
    //Form group for adding user
    this.WorkProfile = this.fb.group({
      states: this.fb.array([], [Validators.required])
    })


    this.AddCompany = new FormGroup({
      "employee_code": new FormControl('', [Validators.required, , this.noWhitespace]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "full_name": new FormControl('', [Validators.required, this.noWhitespace, Validators.pattern("^[a-z A-Z.]*$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),

    });
    this.AddUser = new FormGroup({
      "country_id": new FormControl(101),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "post_code": new FormControl('', [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
      "landmark": new FormControl(''),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "plot_number": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[0-9%+-/A-Z a-z]{2,32}$")]),

      "location_id": new FormControl(35),
      "address": new FormControl(""),
      "location_label": new FormControl('state'),

    });
    this.AddProfile = new FormGroup({
      "role_id": new FormControl('', [Validators.required]),
      "node_id": new FormControl(''),
      "technician_type": new FormControl(''),
      "assigned_service_desk": new FormControl(''),
      "country_profile": new FormControl(101),
      "states_profile": new FormControl('', [Validators.required]),
      "servicedesk_type": new FormControl(''),
      "allocated_segment": new FormControl(''),
      "products": new FormControl(''),
      "servicegroup": new FormControl(''),
      "subproducts": new FormControl(''),
      "city_profile": new FormControl(''),
      "loc_profile": new FormControl(''),
    });
    //form group for adding product
    this.AddProduct = new FormGroup({
      "employee_code": new FormControl(this.employee_code),
      "product_id": new FormControl('', [Validators.required]),
      "product_sub_id": new FormControl('', [Validators.required]),
      "skill_level": new FormControl('', [Validators.required]),
      "work_type": new FormControl('', [Validators.required]),
    });

    //form group for edit company in edit user
    this.EditCompany = new FormGroup({
      "employee_id": new FormControl(''),
      // "address": new FormControl(this.user.address),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "full_name": new FormControl('', [Validators.required, this.noWhitespace, Validators.pattern("^[a-z A-Z.]*$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),

    });
    //form group for edit company in technician

    this.TechCompany = new FormGroup({
      "employee_id": new FormControl(''),
      // "address": new FormControl(this.user.address),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "full_name": new FormControl('', [Validators.required, this.noWhitespace, Validators.pattern("^[a-z A-Z.]*$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
    });
  }

  //function to remove whitespaces
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  //function to block the user 
  blockuser(id, status) {
    var datas = { "user_status": +status, "employee_code": id };
    var url = 'block_user/';             // api url for getting the details with using post params
    this.ajax.postdata(url, datas).subscribe((result) => {
      if (result.response.response_code == "200") {                     //if sucess
        this.showTable = false;
        this.chRef.detectChanges();
        this.getuserdetails();                                    //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
      }
      else if (result.response.response_code == "400") {                   //if block
        this.showTable = false;
        this.chRef.detectChanges();
        this.getuserdetails();
        this.toastr.error(result.response.message);        //toastr message for success
        // this.error = result.response.message;
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error                                                         //if not sucess
        this.error = result.response.message;
      }
      else {
        this.error = "Something went wrong";
      }
    }, (err) => {                                                       //if error
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //function to block technician
  blocktech(id, status) {
    var datas = { "user_status": +status, "employee_code": id };
    var url = 'block_user/';             // api url for getting the details with using post params
    this.ajax.postdata(url, datas).subscribe((result) => {
      if (result.response.response_code == "200") {                     //if sucess
        this.showtech = false;
        this.chRef.detectChanges();
        this.gettechdetails();                                             //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
      }
      else if (result.response.response_code == "400") {                   //if failure
        this.showtech = false;
        this.chRef.detectChanges();
        this.gettechdetails();                                             //reloading the component
        this.toastr.error(result.response.message);        //toastr message for error
      }
      else if (result.response.response_code == "500") {                                                           //if not sucess
        this.toastr.error(result.response.message, 'Error');
        // this.error = result.response.message;
      }
      else {
        this.error = "Something went wrong";
      }
    }, (err) => {                                                       //if error
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //Function to get states using countries
  onChangecountries(country) {
    // var country = 101; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    //calling api for state list using country id
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.state_names = result.response.data;   //storing the api response in the array
        // this.city_name = [];
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }
  onChangecountriesuseredit(country) {
    // var country = 101; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    //calling api for state list using country id
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;   //storing the api response in the array
        // this.sleectedStates = this.user.assigned_state;
        // this.city_name = [];
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }
  onChangeCountry(country_id) {
    var country = country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    //calling api for state list using country id
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;   //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }

  onChangeRole(role_id) {
    console.log(this.org_type, "this.org_type")
    if (this.org_type == 1) {
      this.products = [];
      this.segment_data = [];
      this.select = [];
      this.role_type = role_id;
      this.AddProfile.controls['states_profile'].setValue(null);
      this.selectedStates = [];
      this.selectedCityName = [];
      this.selectedLocation_name = [];
      this.profile_label_location = '';
      this.profile_label_city = '';
      if (this.role_type == '5') {
        var url = 'user_product_details/';
        // this.AddProfile.value.valid = false;
        // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        this.AddProfile.controls['subproducts'].setValue('');
        this.AddProfile.controls['servicegroup'].setValue('')
        this.product_label = 'subproduct';
        this.product_check = "false";
        this.subproducts_data = [];
        this.sleectedSubproduct = [];
        this.selectedSub = [];
        this.ajax.getdata(url).subscribe((result) => {
          if (result.response.response_code == "200") {                    //if sucess
            this.product_details = result.response.data;                    //storing the api response in the array
            // this.subproducts_data = [];
            this.selectedSub = this.subproducts_data;
            this.chRef.detectChanges();
            this.overlayRef.detach();
          }
        }, (err) => {                                                      //if error
          console.log(err);                                                 //prints if it encounters an error
        });
        this.get_servicegroup();
        this.onChangeCountryProfile(101);

      }
      else if (this.role_type == '6') {
        var url = 'service_desk_users/';
        this.AddProfile.value.valid = true;
        this.AddProfile.controls['technician_type'].setValue(null);
        this.AddProfile.controls['assigned_service_desk'].setValue(null);
        // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        this.ajax.getdata(url).subscribe((result) => {
          if (result.response.response_code == "200") {                    //if sucess
            this.servicedesk_list = result.response.data;                    //storing the api response in the array
            this.chRef.detectChanges();
          }
        }, (err) => {                                                      //if error
          console.log(err);                                                 //prints if it encounters an error
        });
        this.selectedCityName = [];
      }
      else if (this.role_type == '4') {
        this.profile_label = "state";
        this.onChangeCountryProfile(101);
      }
      else {
        this.onChangeCountryProfile(101);
        this.AddProfile.value.valid = true;
        this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
        this.product_check = "true";
      }
    }
    else if (this.org_type == 2) {
      this.products = [];
      this.select = [];
      this.role_type = role_id;
      this.AddProfile.controls['states_profile'].setValue(null);
      this.selectedStates = [];
      this.selectedCityName = [];
      this.selectedLocation_name = [];
      this.profile_label_location = '';
      this.profile_label_city = '';
      if (this.role_type == '5') {
        var url = 'user_product_details/';
        // this.AddProfile.value.valid = false;
        // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        this.AddProfile.controls['subproducts'].setValue('');
        this.AddProfile.controls['servicegroup'].setValue('')
        this.product_label = 'subproduct';
        this.product_check = "false";
        this.subproducts_data = [];
        this.sleectedSubproduct = [];
        this.selectedSub = [];
        this.ajax.getdata(url).subscribe((result) => {
          if (result.response.response_code == "200") {                    //if sucess
            this.product_details = result.response.data;                    //storing the api response in the array
            // this.subproducts_data = [];
            this.selectedSub = this.subproducts_data;
            this.chRef.detectChanges();
            this.overlayRef.detach();
          }
        }, (err) => {                                                      //if error
          console.log(err);                                                 //prints if it encounters an error
        });
        this.get_servicegroup();
        this.onChangeCountryProfile(101);

      }
      else if (this.role_type == '6') {
        var url = 'service_desk_users/';
        this.AddProfile.value.valid = true;
        this.AddProfile.controls['technician_type'].setValue(null);
        this.AddProfile.controls['assigned_service_desk'].setValue(null);
        // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        this.ajax.getdata(url).subscribe((result) => {
          if (result.response.response_code == "200") {                    //if sucess
            this.servicedesk_list = result.response.data;                    //storing the api response in the array
            this.chRef.detectChanges();
          }
        }, (err) => {                                                      //if error
          console.log(err);                                                 //prints if it encounters an error
        });
        this.selectedCityName = [];
      }
      else {
        this.onChangeCountryProfile(101);
        this.AddProfile.value.valid = true;
        this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
        this.product_check = "true";
      }
    }

  }
  onChangeCountryProfile(country_id) {
    this.profile_label_location = '';
    // this.selectedStates = [];
    // this.sleectedStates = [];
    this.country_profile = country_id;
    var country = country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    this.assignstates = []
    //calling api for state list using country id
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {

        this.assignstates = result.response.data;   //storing the api response in the array
      }
      else if (result.response.response_code == "400") {

        this.assignstates = result.response.data;
      }

    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
    if (this.org_type == 1) {
      this.selectedStates = [];
      this.profile_label = 'state';
      this.profile_label_city = '';
    }
    else if (this.org_type == 2) {
      this.selectedStates = [];
      this.profile_label = '';
      this.profile_label_city = '';
    }
    // }
  }
  onChangeState(state_id) {
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.city_name = result.response.data;
        // console.log(this.cities);            //storing the api response in the array                   
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onChangeStatesedit(state_id) {
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.city_name = result.response.data;              //storing the api response in the array                   
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    if (this.EditUser) {
      this.EditUser.controls["city_id"].setValue('')
    }
    if (this.TechUser) {
      this.TechUser.controls["city_id"].setValue('')
    }
  }
  onChangeStates(state_id) {
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.city_name = result.response.data;              //storing the api response in the array                   
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    if (this.AddUser) {
      this.AddUser.controls['city_id'].setErrors({ 'incorrect': true });
      this.AddUser.controls["city_id"].setValue('')
    }
  }

  //select state change event
  //get city dropdown 
  onChangeStateProfile(data, mode) {

    if (this.AddProfile) {
      var url = 'mult_city_location/'
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          if (mode == 'state') {
            // this.AddProfile.value.valid = false;
            // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
            this.cities = result.response.data;
            this.selectedCityName = []

          }
          else if (mode == 'city') {
            // this.AddProfile.value.valid = false;
            // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
            this.locations = result.response.data;
          }
        } else if (result.response.response_code == "400") {
          if (mode == 'state') {
            this.AddProfile.value.valid = true;
            this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
            this.cities = result.response.data;             //storing the api response in the array                   
            this.selectedCityName = [];
          }
          else if (mode == 'city') {
            // this.profile_label_city = '';
            this.AddProfile.value.valid = true;
            this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
            this.locations = result.response.data;
          }
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }


  }


  onChangeStateProfileedit(data, mode) {
    var assigned_city = [];
    var assigned_location = [];
    var url = 'mult_city_location/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        if (mode == 'state') {
          this.cities = result.response.data;
          var isPresent = assigned_city.some(function (el) { return el === this.user.assigned_city.city_id });
          if (isPresent) {
          } else {
            assigned_city.forEach(value => {
              this.citys.push(value.city_id);
            })
          }

        }
        else if (mode == 'city') {

          this.locations = result.response.data;
          var isPresent = assigned_location.some(function (el) { return el === this.user.assigned_location.location_id });
          if (isPresent) {
          } else {
            assigned_location.forEach(value => {
              this.location_profile.push(value.location_id);
            })
          }
          // this.location_profile=this.user.assigned_location;
        }
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onchangelandmark(event) {
    if (this.AddUser) {
      if (this.AddUser.value.landmark != '') {
        var landmark = event.target.value.trim();
        this.AddUser.controls['landmark'].setValue(landmark)
      }
      else {
        this.AddUser.controls['landmark'].setValue('')
      }
    }
    else if (this.EditUser) {
      if (this.EditUser.value.landmark != '') {
        var landmark = event.target.value.trim();
        this.EditUser.controls['landmark'].setValue(landmark)
      }
      else {
        this.EditUser.controls['landmark'].setValue('')
      }
    }
    else if (this.TechUser) {
      if (this.TechUser.value.landmark != '') {
        var landmark = event.target.value.trim();
        this.TechUser.controls['landmark'].setValue(landmark)
      }
      else {
        this.TechUser.controls['landmark'].setValue('')
      }
    }
  }
  onChangegeteditProduct(data) {
    this.subproducts_data = [];
    var subproducts_data = [];
    this.all_subproduct = [];
    var url = 'multi_subproduct_details/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproducts_data = result.response.data;
        this.product_label = 'subproduct';
        this.selectedSub = this.subproducts_data;
        this.subproducts = "All";
        var isPresent = subproducts_data.some(function (el) { return el === this.subproducts_data.product_sub_id });
        if (isPresent) {
        } else {
          this.subproducts_data.forEach(value => {
            this.all_subproduct.push(value.product_sub_id);
          })
        }
        console.log(this.all_subproduct, "this.all_subproduct")
        console.log(this.subproducts)
        if (this.user.product_flag == "All") {
          if (this.user.subproduct == null || this.user.subproduct == []) {
            this.selectedSubProducts = this.subproducts_data
          }
          else {
            this.selectedSubProducts = this.user.subproduct;
          }

        }
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.subproducts = "All";

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onChangegetProduct(data) {
    this.subproducts_data = [];
    this.all_subproduct = [];
    var subproducts_data = [];
    var url = 'multi_subproduct_details/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproducts_data = result.response.data;
        this.product_label = 'subproduct';
        this.selectedSub = this.subproducts_data;
        this.subproducts = "All";

        var isPresent = subproducts_data.some(function (el) { return el === this.subproducts_data.product_sub_id });
        if (isPresent) {
        } else {
          this.subproducts_data.forEach(value => {
            this.all_subproduct.push(value.product_sub_id);
          })
        }
        console.log(this.all_subproduct, "this.all_subproduct")
        console.log(this.subproducts)
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.AddProfile.value.valid = true;
        this.subproducts = "All";
        this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  techstatesadd(data) {
    this.locations = [];
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.locations = result.response.data;
        this.selectedLocation_name = [];
        this.profile_label_location = "location";
        // this.AddProfile.value.valid = false;
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.AddProfile.value.valid = true;
        this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  techstates(data) {
    this.all_state = [];
    this.assignstates = [];
    var state = [];
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.assignstates = result.response.data;
        // this.selectedStates = this.assignstates;
        console.log(this.assignstates)
        var isPresent = state.some(function (el) { return el === this.assignstates.state_id });
        if (isPresent) {
        } else {
          this.assignstates.forEach(value => {
            this.all_state.push(value.state_id);
          })
        }
        console.log(this.all_state)
        this.state = "All";
        this.assigned_label = "state";
        // this.AddProfile.value.valid = true;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onChangeservice(data) {
    console.log(data)
    this.servcedesk_code = data;
    var code = { "employee_code": this.servcedesk_code }
    // if (this.AddProfile) {
    //   var url = 'service_desk_locations/'
    //   this.ajax.postdata(url, code).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.service_location = result.response.data;
    //       this.AddProfile.value.valid = false;
    //       this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    //       if (this.service_location[0].country_id) {
    //         this.country_profile = this.service_location[0].country_id;
    //         this.AddProfile.controls['country_profile'].setValue("India")
    //         this.profile_label = 'state';
    //         this.selectedStates = [];
    //         var datas = { "employee_code": this.servcedesk_code, "country_id": this.service_location[0].country_id }
    //         this.techstates(datas);
    //       }

    //     }
    //     else if (result.response.response_code == "400" || result.response.response_code == "500") {
    this.AddProfile.value.valid = true;
    //       this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    //     }
    //   }, (err) => {
    //     console.log(err);                                        //prints if it encounters an error
    //   });
    // }
    // if (this.TechProfile) {
    if (this.service_location == '') {

      this.country_profile = this.tech.assigned_country.assigned_country_id
    }


    else {
      var url = 'service_desk_locations/'
      this.ajax.postdata(url, code).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.service_location = result.response.data;
          // this.TechProfile.value.valid = false;
          // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
          if (this.service_location[0].country_id) {
            this.country_profile = this.service_location[0].country_id;
            if (this.org_type == 1) {
              this.profile_label = 'state';
              this.profile_label_city = '';
              this.profile_label_location = '';
            }
            else if (this.org_type == 2) {
              this.profile_label = '';
              this.profile_label_city = '';
              this.profile_label_location = '';
            }

            // this.selectedStates = [];
            this.selectedCityName = [];

            // this.TechProfile.controls['country_profile'].setValue(this.country_profile)
            var datas = { "employee_code": this.servcedesk_code, "country_id": this.country_profile }
            this.techstates(datas);
          }

        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          // this.TechProfile.value.valid = true;
          // this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
      // this.country_profile = this.service_location[0].country_id;
    }

    // }
  }

  onItemSelectservicedesk(item: any) {
    // this.AddProfile.value.valid = false;
    // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    this.servcedesk_code = item.employee_code;
    var data = { "employee_code": this.servcedesk_code }
    this.onChangeservice(data);
  }
  onItemSelectProduct(item: any) {
    this.selectedSubProducts = [];
    // this.AddProfile.value.valid = false;
    // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    this.product_flag = "";
    console.log(this.products)
    if (this.products != "All") {
      var isPresent = this.products.some(function (el) { return el === item.product_id });
      if (isPresent) {
      } else {
        this.products.push(item.product_id);
        var data = { "product_id": this.products }
        this.subproducts_data = [];
        this.onChangegetProduct(data);
      }
    }
    else {
      this.products.push(item.product_id);
      var data = { "product_id": this.products }
      this.onChangegetProduct(data);
    }


    if (this.products) {
      console.log(this.products)
      if (this.products.length != 0) {
        console.log(this.subproducts_data)
        // if (this.subproducts_data) {
        console.log(this.subproducts_data.length)
        if (this.subproducts_data.length != 0) {
          this.product_label = 'subproduct';
          this.editprofile_disable = true;
          this.selectedSub = this.subproducts_data;
          // this.subproducts = [];
        } else {
          this.product_label = ''
          this.editprofile_disable = false;
          this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
        }
        // }
      }
    }


  }
  onItemSelectSegment(item: any) {
    // this.selectedSubProducts = [];
    // this.AddProfile.value.valid = false;
    // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    // this.product_flag = "";
    // console.log(this.products)
    // if (this.products != "All") {
    //   var isPresent = this.products.some(function (el) { return el === item.product_id });
    //   if (isPresent) {
    //   } else {
    //     this.products.push(item.product_id);
    //     var data = { "product_id": this.products }
    //     this.subproducts_data = [];
    //     this.onChangegetProduct(data);
    //   }
    // }
    // else {
    console.log(item.segment_id, "item.segment_id")
    this.segment_data.push(item.segment_id);
    // var data = { "product_id": this.products }
    // this.onChangegetProduct(data);
    // }


    // if (this.products) {
    console.log(this.segment_data)
    //   if (this.products.length != 0) {
    //     console.log(this.subproducts_data)
    //     // if (this.subproducts_data) {
    //     console.log(this.subproducts_data.length)
    //     if (this.subproducts_data.length != 0) {
    //       this.product_label = 'subproduct';
    //       this.editprofile_disable = true;
    //       this.selectedSub = this.subproducts_data;
    //       // this.subproducts = [];
    //     } else {
    //       this.product_label = ''
    //       this.editprofile_disable = false;
    //       this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    //     }
    //     // }
    //   }
    // }


  }
  onSelectAllservicedesk(items: any) {

    console.log(items)
    this.items = items;
    console.log(this.items)
    if (this.AddProfile) {
      this.AddProfile.value.valid = true;
      this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    }
    if (this.EditProfile) {
      this.EditProfile.value.valid = true;
      this.EditProfile.controls['role_id'].setValue(this.EditProfile.value.role_id);
    }
  }
  onSelectAllProduct(items: any) {
    this.productsall = [];
    var isPresent = this.products.some(function (el) { return el === items.product_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.productsall.push(value.product_id);
      })
    }
    this.product_flag = "All";
    this.product_label = "";
    this.products = "All";
    this.subproducts = [];
    this.product_check = "false";
    // this.selectedSub = [];

    this.items = items;
    if (this.AddProfile) {
      this.AddProfile.value.valid = true;
      this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    }


  }
  onSelectAllSegment(items: any) {
    this.segmentsall = [];
    var isPresent = this.segment_data.some(function (el) { return el === items.segment_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.segmentsall.push(value.segment_id);
      })
    }

    this.segment_data = this.segmentsall;
    this.items = items;



  }
  onItemDeSelectAllProduct(items: any) {
    // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    // this.AddProfile.value.valid = false;
    this.products = [];
    this.productsall = [];
    this.product_flag = "";
    this.product_label = 'subproduct';
    this.product_check = "true";
    this.selectedSub = [];
    this.selectedSubProducts = [];
    this.subproducts = [];
    var data = { "product_id": [] }
    this.onChangegetProduct(data);

  }
  onItemDeSelectAllSegment(items: any) {
    // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
    // this.AddProfile.value.valid = false;
    this.segment_data = [];
    this.segmentsall = [];
    // this.product_flag = "";
    // this.product_label = 'subproduct';
    // this.product_check = "true";
    // this.selectedSub = [];
    // this.selectedSubProducts = [];
    // this.subproducts = [];
    // var data = { "product_id": [] }
    // this.onChangegetProduct(data);

  }

  onItemDeSelectProduct(item: any) {

    if (this.products == "All") {
      this.product_flag = "";
      this.selectedSub = [];
      this.subproducts = [];
      const index: number = this.productsall.indexOf(item.product_id);
      if (index !== -1) {
        this.productsall.splice(index, 1);
      }
      this.products = this.productsall;
      if (this.products.length != 0) {
        this.product_label = 'subproduct';
        this.product_check = "false";
        this.selectedSub = [];
        this.subproducts = [];
        this.subproducts_data = [];
      }
      else {
        this.product_label = '';
        this.product_check = "true";
        this.selectedSub = [];
        this.subproducts = [];
        this.subproducts_data = [];
        this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      }
    }
    else {
      this.product_flag = "";
      // this.selectedSub = [];
      this.subproducts = [];
      this.products = this.products.filter(items => items !== item.product_id);
      if (this.products.length != 0) {
        this.product_label = "subproduct";
        this.product_check = "false";

      } else {
        this.product_label = "";
        this.product_check = "true";

      }
    }

    var data = { "product_id": this.products }
    this.onChangegetProduct(data);
    // this.AddProfile.controls['subproduct'].setValue('');
  }
  onItemDeSelectSegment(item: any) {

    if (this.segment_data == "All") {
      // this.product_flag = "";
      // this.selectedSub = [];
      // this.subproducts = [];
      const index: number = this.segmentsall.indexOf(item.segment_id);
      if (index !== -1) {
        this.segmentsall.splice(index, 1);
      }
      this.segment_data = this.segmentsall;
      // if (this.products.length != 0) {
      //   this.product_label = 'subproduct';
      //   this.product_check = "false";
      //   this.selectedSub = [];
      //   this.subproducts = [];
      //   this.subproducts_data = [];
      // }
      // else {
      //   this.product_label = '';
      //   this.product_check = "true";
      //   this.selectedSub = [];
      //   this.subproducts = [];
      //   this.subproducts_data = [];
      //   this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      // }
    }
    else {
      // this.product_flag = "";
      // this.selectedSub = [];
      // this.subproducts = [];
      this.segment_data = this.segment_data.filter(items => items !== item.segment_id);
      console.log(this.segment_data, "this.segment_data")
      // if (this.products.length != 0) {
      //   this.product_label = "subproduct";
      //   this.product_check = "false";

      // } else {
      //   this.product_label = "";
      //   this.product_check = "true";

      // }
    }

    // var data = { "product_id": this.products }
    // this.onChangegetProduct(data);
    // this.AddProfile.controls['subproduct'].setValue('');
  }

  onItemSelectservicegroup(item: any) {
    this.service_flag = "";
    var isPresent = this.servicegroups.some(function (el) { return el === item.work_type_id });
    if (isPresent) {
    } else {
      this.servicegroups.push(item.work_type_id);
    }
  }
  onSelectAllservicegroup(items: any) {
    this.service_all = [];
    var isPresent = this.servicegroups.some(function (el) { return el === items.work_type_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.service_all.push(value.work_type_id);
      })
    }
    this.service_flag = "All";
    // this.product_label = "";
    this.servicegroups = "All";
  }
  onItemDeSelectAllservicegroup(items: any) {
    this.service_flag = "";
    this.servicegroups = [];
    this.servicegroups = items;
    if (this.servicegroups.length != 0) {
      //this.editprofile_disable='false'
    } else {
      // this.editprofile_disable = 'true';
    }
  }
  onItemDeSelectservicegroup(item: any) {

    this.service_flag = "";
    // this.servicegroups = [];
    if (this.servicegroups == "All") {
      const index: number = this.service_all.indexOf(item.product_id);
      if (index !== -1) {
        this.service_all.splice(index, 1);
      }
      this.servicegroups = this.service_all;
    }
    else {

    }
    this.servicegroups = this.servicegroups.filter(items => items !== item.work_type_id);
    var index = this.products.indexOf(item.work_type_id);
    if (this.servicegroups.length != 0) {
    } else {
      // this.product_label = "";
    }
  }
  onSelectAllsubProduct(items: any) {
    this.subproduct_flag = "All";
    this.product_label = "subproduct";
    if (this.subproducts.length != 0) {
      //this.editprofile_disable='false'
    } else {
      //  this.editprofile_disable = 'true';
    }
    console.log(this.subproducts)
    this.all_subproduct = [];
    var isPresent = this.subproducts.some(function (el) { return el === items.product_sub_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_subproduct.push(value.product_sub_id);
      })
    }
    this.subproducts = "All";
    this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
  }
  onItemSelectsubProduct(item: any) {
    console.log(item)
    this.subproduct_flag = "";
    this.product_label = "subproduct";
    var isPresent = this.subproducts.some(function (el) { return el === item.product_sub_id });
    if (isPresent) {
    } else {
      this.subproducts.push(item.product_sub_id);
    }
    console.log(this.subproducts)
    if (this.subproducts.length != 0) {
    } else {
      this.editprofile_disable = 'true';
    }
    this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
  }

  onItemDeSelectAllsubProduct(items: any) {
    // this.selectedSubProducts = [];
    this.subproduct_flag = "";
    this.product_label = "subproduct";
    this.subproducts = items;
    this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
  }
  onItemDeSelectsubProduct(item: any) {
    console.log(item)
    console.log(this.all_subproduct)
    console.log(this.subproducts)
    this.subproduct_flag = "";
    if (this.subproducts == "All") {
      this.subproducts = [];
      const index: number = this.all_subproduct.indexOf(item.product_sub_id);
      if (index !== -1) {
        this.all_subproduct.splice(index, 1);
        this.subproducts = this.all_subproduct;
      }
    }
    else {
      const index: number = this.subproducts.indexOf(item.product_sub_id);
      if (index !== -1) {
        this.subproducts.splice(index, 1);
      }
    }


    console.log(this.subproducts)
    this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);

  }
  onItemSelectCountry(item: any) {
    this.country_flag = "";
    var isPresent = this.country.some(function (el) { return el === item.country_id });
    if (isPresent) {
    } else {
      this.country.push(item.country_id);
    }
    this.assigned_label = "country"
    this.profile_label_state = 'state';
    var data = { "country_id": this.country }
    this.onChangeCountryProfile(this.country);
    if (this.state) {
      if (this.state.length != 0) {
        if (this.citys) {
          if (this.citys.length != 0) {
            this.editprofile_disable = 'false'
          } else {
            this.editprofile_disable = 'true';
          }
        }
      } else {
        this.editprofile_disable = 'true';
      }
    }
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    this.selectedLocations = [];    //storing the api response in the array                   
    this.selectedLocation_name = [];
  }
  onChangetechnicianlocation(data) {
    if (this.AddProfile) {
      this.locations = [];
      var url = 'service_desk_locations/'
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.locations = result.response.data;
          this.selectedLocation_name = [];
          this.profile_label_location = "location";
          // this.AddProfile.value.valid = false;
          // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.AddProfile.value.valid = true;
          this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }
    else if (this.TechProfile) {
      this.locations = [];
      this.profile_label_location = location;
      var url = 'service_desk_locations/'
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.locations = result.response.data;
          this.selectedLocation_name = this.locations;
          // this.TechProfile.value.valid = false;
          // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.TechProfile.value.valid = true;
          this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }
  }
  onchangetechniciancityedittech(data) {
    console.log(data)
    this.cities = [];
    this.assigned_label = "city";
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;
        this.selectedCityName = this.cities;
        // this.TechProfile.value.valid = false;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });


  }
  onchangetechniciancity(data) {
    console.log(data)
    this.cities = [];
    // this.selectedCityName = [];
    if (this.AddProfile) {
      var url = 'service_desk_locations/'
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data;
          // this.AddProfile.value.valid = false;
          // this.AddProfile.controls['role_id'].setErrors({ 'incorrect': true });
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.AddProfile.value.valid = true;
          this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }
    if (this.TechProfile) {
      this.assigned_label = "city";
      var url = 'service_desk_locations/'
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data;
          this.selectedCityName = this.cities;
          // this.TechProfile.value.valid = false;
          // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.TechProfile.value.valid = true;
          this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }

  }
  onchangetechniciancitytech(data) {
    console.log(data)
    this.cities = [];

    this.assigned_label = "city";
    this.citys = "All";
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;
        this.selectedCityName = this.cities;
        console.log(this.assigned_label, "this.assigned_label")
        // this.TechProfile.value.valid = false;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });


  }
  onItemSelectStateedittech(item: any) {

    // this.TechProfile.value.valid = false;
    // this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.selectedCityName = [];
    this.state_flag = "";
    var isPresent = this.state.some(function (el) { return el === item.state_id });
    if (isPresent) {
    } else {
      this.state.push(item.state_id);
    }

    this.profile_label_city = 'city';
    var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
    this.onchangetechniciancityedittech(data);
    if (this.state) {
      if (this.state.length != 0) {
        if (this.citys) {
          if (this.citys.length != 0) {

            // this.TechProfile.value.valid = false;
            console.log(this.TechProfile.valid);
            // this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
            this.editprofile_disable = 'false'
          } else {
            this.TechProfile.value.valid = true;
            console.log(this.TechProfile.valid);
            this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
            this.editprofile_disable = 'true';
          }
        }
      } else {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
        this.editprofile_disable = 'true';
      }
    }
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.profile_label_location = '';
    this.selectedCitys = val;
    this.selectedLocations = [];    //storing the api response in the array                   
    this.selectedLocation_name = [];


  }
  onItemSelectState(item: any) {
    this.citys = [];
    this.selectedLocation_name = [];
    console.log(this.AddProfile.value.role_id);
    if (this.AddProfile) {
      if (this.AddProfile.value.role_id == 6) {
        // this.AddProfile.value.valid = false;
        console.log(this.AddProfile.value.valid);
        // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
        this.selectedCityName = [];
        this.state_flag = "";
        var isPresent = this.state.some(function (el) { return el === item.state_id });
        if (isPresent) {
        } else {
          this.state.push(item.state_id);
        }
        this.assigned_label = "state";
        this.profile_label_city = 'city';
        console.log(this.servcedesk_code)
        var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
        this.onchangetechniciancity(data);
        if (this.state) {
          if (this.state.length != 0) {
            if (this.citys) {
              if (this.citys.length != 0) {

                // this.AddProfile.value.valid = false;
                console.log(this.AddProfile.valid);
                // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
                // this.editprofile_disable = 'false'
              } else {
                this.AddProfile.value.valid = true;
                console.log(this.AddProfile.valid);
                this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
                // this.editprofile_disable = 'true';
              }
            }
          } else {
            this.AddProfile.value.valid = true;
            console.log(this.AddProfile.valid);
            this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
            // this.editprofile_disable = 'true';
          }
        }
        const val = this.citys.filter(f => f === this.selectedCitys.city_id);
        this.profile_label_location = '';
        this.selectedCitys = val;
        this.selectedLocations = [];    //storing the api response in the array                   
        this.selectedLocation_name = [];
      }
      else {
        // this.AddProfile.value.valid = false;
        console.log(this.AddProfile.value.valid);
        // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
        this.selectedCityName = [];
        this.state_flag = "";
        var isPresent = this.state.some(function (el) { return el === item.state_id });
        if (isPresent) {
        } else {
          this.state.push(item.state_id);
        }
        this.assigned_label = "state"
        this.profile_label_city = 'city';
        var datas = { "state_id": this.state }
        this.onChangeStateProfile(datas, "state");
        if (this.state) {
          if (this.state.length != 0) {
            if (this.citys) {
              if (this.citys.length != 0) {

                // this.AddProfile.value.valid = false;
                console.log(this.AddProfile.valid);
                // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
                this.editprofile_disable = 'false'
              } else {
                this.AddProfile.value.valid = true;
                console.log(this.AddProfile.valid);
                this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
                this.editprofile_disable = 'true';
              }
            }
          } else {
            this.AddProfile.value.valid = true;
            console.log(this.AddProfile.valid);
            this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
            this.editprofile_disable = 'true';
          }
        }
        const val = this.citys.filter(f => f === this.selectedCitys.city_id);
        this.profile_label_location = '';
        this.selectedCitys = val;
        this.selectedLocations = [];    //storing the api response in the array                   
        this.selectedLocation_name = [];
      }
    }
    if (this.TechProfile) {
      // if (this.TechProfile.value.role_id == 6) {
      // this.TechProfile.value.valid = false;
      console.log(this.TechProfile.value.valid);
      // this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
      this.selectedCityName = [];
      this.state_flag = "";
      var isPresent = this.state.some(function (el) { return el === item.state_id });
      if (isPresent) {
      } else {
        this.state.push(item.state_id);
      }
      this.assigned_label = "state"
      this.profile_label_city = 'city';
      console.log(this.servcedesk_code)
      var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
      this.onchangetechniciancity(data);
      if (this.state) {
        if (this.state.length != 0) {
          if (this.citys) {
            if (this.citys.length != 0) {

              // this.TechProfile.value.valid = false;
              console.log(this.TechProfile.valid);
              // this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
              this.editprofile_disable = 'false'
            } else {
              this.TechProfile.value.valid = true;
              console.log(this.TechProfile.valid);
              this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
              this.editprofile_disable = 'true';
            }
          }
        } else {
          this.TechProfile.value.valid = true;
          console.log(this.TechProfile.valid);
          this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
          this.editprofile_disable = 'true';
        }
      }
      const val = this.citys.filter(f => f === this.selectedCitys.city_id);
      this.profile_label_location = '';
      this.selectedCitys = val;
      this.selectedLocations = [];    //storing the api response in the array                   
      this.selectedLocation_name = [];
      // }
      // else {
      //   this.TechProfile.value.valid = false;
      //   console.log(this.TechProfile.value.valid);
      //   this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
      //   this.selectedCityName = [];
      //   this.state_flag = "";
      //   var isPresent = this.state.some(function (el) { return el === item.state_id });
      //   if (isPresent) {
      //   } else {
      //     this.state.push(item.state_id);
      //   }
      //   this.assigned_label = "state"
      //   this.profile_label_city = 'city';
      //   var datas = { "state_id": this.state }
      //   this.onChangeStateProfile(datas, "state");
      //   if (this.state) {
      //     if (this.state.length != 0) {
      //       if (this.citys) {
      //         if (this.citys.length != 0) {

      //           this.TechProfile.value.valid = false;
      //           console.log(this.TechProfile.valid);
      //           this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
      //           this.editprofile_disable = 'false'
      //         } else {
      //           this.TechProfile.value.valid = true;
      //           console.log(this.TechProfile.valid);
      //           this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
      //           this.editprofile_disable = 'true';
      //         }
      //       }
      //     } else {
      //       this.TechProfile.value.valid = true;
      //       console.log(this.TechProfile.valid);
      //       this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
      //       this.editprofile_disable = 'true';
      //     }
      //   }
      //   const val = this.citys.filter(f => f === this.selectedCitys.city_id);
      //   this.profile_label_location = '';
      //   this.selectedCitys = val;
      //   this.selectedLocations = [];    //storing the api response in the array                   
      //   this.selectedLocation_name = [];
      // }
    }

  }
  onItemSelectedituser(item: any) {
    // if (this.EditProfile.value.role_id == 6) {
    //   this.EditProfile.value.valid = false;
    //   console.log(this.EditProfile.value.valid);
    //   this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    //   this.selectedCityName = [];
    //   this.state_flag = "";
    //   var isPresent = this.state.some(function (el) { return el === item.state_id });
    //   if (isPresent) {
    //   } else {
    //     this.state.push(item.state_id);
    //   }
    //   this.assigned_label = "state"
    //   this.profile_label_city = 'city';
    //   console.log(this.servcedesk_code)
    //   var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
    //   this.onchangetechniciancity(data);
    //   if (this.state) {
    //     if (this.state.length != 0) {
    //       if (this.citys) {
    //         if (this.citys.length != 0) {

    //           this.EditProfile.value.valid = false;
    //           console.log(this.EditProfile.valid);
    //           this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    //           this.editprofile_disable = 'false'
    //         } else {
    //           this.EditProfile.value.valid = true;
    //           console.log(this.EditProfile.valid);
    //           this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    //           this.editprofile_disable = 'true';
    //         }
    //       }
    //     } else {
    //       this.EditProfile.value.valid = true;
    //       console.log(this.EditProfile.valid);
    //       this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    //       this.editprofile_disable = 'true';
    //     }
    //   }
    //   const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    //   this.profile_label_location = '';
    //   this.selectedCitys = val;
    //   this.selectedLocations = [];    //storing the api response in the array                   
    //   this.selectedLocation_name = [];
    // }
    // else {
    this.EditProfile.value.valid = false;
    console.log(this.EditProfile.value.valid);
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.selectedCityName = [];
    this.state_flag = "";
    var isPresent = this.state.some(function (el) { return el === item.state_id });
    if (isPresent) {
    } else {
      this.state.push(item.state_id);
    }
    this.assigned_label = "state"
    this.profile_label_city = 'city';
    var datas = { "state_id": this.state }
    this.onChangeStateProfile(datas, "state");
    if (this.state) {
      if (this.state.length != 0) {
        if (this.citys) {
          if (this.citys.length != 0) {

            this.EditProfile.value.valid = false;
            console.log(this.EditProfile.valid);
            // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
            this.editprofile_disable = 'false'
          } else {
            this.EditProfile.value.valid = true;
            console.log(this.EditProfile.valid);
            this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
            this.editprofile_disable = 'true';
          }
        }
      } else {
        this.EditProfile.value.valid = true;
        console.log(this.EditProfile.valid);
        this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
        this.editprofile_disable = 'true';
      }
    }
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.profile_label_location = '';
    this.selectedCitys = val;
    this.selectedLocations = [];    //storing the api response in the array                   
    this.selectedLocation_name = [];
    // }

  }
  onSelectAllStateuseredit(items: any) {
    this.all_state = [];
    console.log(items)
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_state.push(value.state_id);
      })
    }
    // this.citys = [];
    this.assigned_label = "state"
    this.state = "All";
    this.citys = [];
    this.location_profile = [];
    this.profile_label = 'state';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.selectedCitys = [];
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    this.editprofile_disable = 'false';
    this.state_flag = "All";
    this.city_flag = '';
    this.country_profile = this.EditProfile.value.country_profile;
    this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);



  }
  onSelectAllStateeditech(items: any) {
    this.all_state = [];
    var isPresent = this.state.some(function (el) { return el === items.state_id });

    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_state.push(value.state_id);
      })
    }
    this.assigned_label = "state"
    this.state = "All";
    this.profile_label = 'state';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.selectedCitys = [];
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    this.editprofile_disable = 'false';
    this.state_flag = "All";
    this.citys = [];
    this.location_profile = [];


  }
  onSelectAllState(items: any) {
    this.all_state = [];
    console.log(items)
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_state.push(value.state_id);
      })
    }
    this.assigned_label = "state"
    this.state = "All";
    this.profile_label = 'state';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.selectedCitys = [];
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    this.editprofile_disable = 'false';
    this.state_flag = "All";
    this.citys = [];
    this.location_profile = [];
    if (this.AddProfile.value.valid == false) {
      this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      this.country_profile = this.AddProfile.value.country_profile;
      this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    }
    // if (this.EditProfile.value.valid == false) {
    //   this.EditProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    //   this.country_profile = this.EditProfile.value.country_profile;
    //   this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    // }
    // else {

    // }
  }
  onItemDeSelectAllStateedituser(items: any) {
    this.EditProfile.value.valid = false;
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.state_flag = "";
    this.citys = [];
    this.location_profile = [];
    this.profile_label_city = 'city';
    this.state = items;
    if (this.state.length != 0) {
      if (this.citys.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }
    this.profile_label_location = '';
    this.onChangeStateProfile(this.state, "state");
    this.profile_label_city = '';
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    this.selectedLocations = [];
    this.selectedLocation_name = [];
  }
  onItemDeSelectAllStateedittech(item: any) {
    // this.AddProfile.value.valid = false;

    console.log(this.AddProfile.value.valid);
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });

    this.state_flag = "";
    this.citys = [];
    this.location_profile = [];
    this.profile_label_city = 'city';
    this.state = item;
    if (this.state.length != 0) {
      if (this.citys.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }
    this.profile_label_location = '';
    var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
    this.onchangetechniciancitytech(data)
    this.profile_label_city = '';
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    this.selectedLocations = [];
    this.selectedLocation_name = [];
  }
  onItemDeSelectAllState(items: any) {
    // this.AddProfile.value.valid = false;

    console.log(this.AddProfile.value.valid);
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });

    this.state_flag = "";
    this.citys = [];
    this.location_profile = [];
    this.profile_label_city = 'city';
    this.state = items;
    if (this.state.length != 0) {
      if (this.citys.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }
    this.profile_label_location = '';
    this.onChangeStateProfile(this.state, "state");
    this.profile_label_city = '';
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    this.selectedLocations = [];
    this.selectedLocation_name = [];
  }
  onItemDeSelectStateedituser(item: any) {

    this.state_flag = "";
    this.citys = [];
    this.location_profile = [];
    this.profile_label_city = 'city';
    if (this.state == "All") {
      const index: number = this.all_state.indexOf(item.state_id);
      if (index !== -1) {
        this.all_state.splice(index, 1);
      }
      this.state = this.all_state;
      if (this.state.length != 0) {
        this.profile_label_city = 'city';
      }
      else {
        this.profile_label_city = '';
      }
      this.profile_label_location = '';
    }
    else {
      const index: number = this.state.indexOf(item.state_id);
      if (index !== -1) {
        this.state.splice(index, 1);
      }
      if (this.state.length != 0) {
        this.profile_label_city = 'city';
      }
      else {
        this.profile_label_city = '';
      }
      this.profile_label_location = '';


    }
    var data = { "state_id": this.state }
    this.onChangeStateProfile(data, "state");
    this.citys = [];
    this.location_profile = [];
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    if (this.state.length != 0) {
      if (this.citys.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }

  }
  onItemDeSelectStateedittech(item: any) {

    this.state_flag = "";
    this.citys = [];
    this.location_profile = [];
    this.profile_label_city = 'city';
    // this.profile_label_city = '';
    if (this.state == "All") {
      const index: number = this.all_state.indexOf(item.state_id);
      if (index !== -1) {
        this.all_state.splice(index, 1);
      }
      this.state = this.all_state;
      if (this.state.length != 0) {
        this.profile_label_city = 'city';
      }
      else {
        this.profile_label_city = '';
      }
      this.profile_label_location = '';
    }
    else {
      const index: number = this.state.indexOf(item.state_id);
      if (index !== -1) {
        this.state.splice(index, 1);
      }
      if (this.state.length != 0) {
        this.profile_label_city = 'city';
      }
      else {
        this.profile_label_city = '';
      }
      this.profile_label_location = '';


    }
    console.log(this.states);
    console.log(this.selectedStates);
    console.log(this.state);
    var datas = { "employee_code": this.servcedesk_code, "state_id": this.state }
    this.onchangetechniciancityedittech(datas);
    this.citys = [];
    this.location_profile = [];
    const val = this.citys.filter(f => f === this.selectedCitys.city_id);
    this.selectedCitys = val;
    if (this.state.length != 0) {
      if (this.citys.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }

  }
  onItemDeSelectState(item: any) {
    console.log(item);
    console.log(this.all_state)
    console.log(this.state)
    if (this.AddProfile.value.role_id == 6) {
      this.state_flag = "";
      this.citys = [];
      this.location_profile = [];
      if (this.state == "All") {
        const index: number = this.all_state.indexOf(item.state_id);
        if (index !== -1) {
          this.all_state.splice(index, 1);
        }

        this.state = this.all_state;
        // const val = this.state.filter(f => f === this.state.state_id);
        // this.state = val;
        console.log(this.state)
        if (this.state.length != 0) {
          this.profile_label_city = 'city';
        }
        else {
          this.profile_label_city = '';
        }
        this.profile_label_location = '';
      }
      else {
        const index: number = this.state.indexOf(item.state_id);
        if (index !== -1) {
          this.state.splice(index, 1);
        }
        if (this.state.length != 0) {
          this.profile_label_city = 'city';
        }
        else {
          this.profile_label_city = '';
        }
        this.profile_label_location = '';


      }
      // console.log(this.states);
      // console.log(this.selectedStates);
      console.log(this.state);
      var datas = { "employee_code": this.servcedesk_code, "state_id": this.state }
      this.onchangetechniciancity(datas);
      this.citys = [];
      this.location_profile = [];
      const val = this.citys.filter(f => f === this.selectedCitys.city_id);
      this.selectedCitys = val;
      if (this.state.length != 0) {
        if (this.citys.length != 0) {
          this.editprofile_disable = 'false'
        } else {
          this.editprofile_disable = 'true';
        }
      } else {
        this.editprofile_disable = 'true';
      }
    }
    else {
      this.state_flag = "";
      this.citys = [];
      this.location_profile = [];
      this.profile_label_city = 'city';
      if (this.state == "All") {
        const index: number = this.all_state.indexOf(item.state_id);
        if (index !== -1) {
          this.all_state.splice(index, 1);
        }
        console.log(this.all_state, "this.all_state")
        this.state = this.all_state;
        console.log(this.state, "this.state");
        // this.selectedStates = this.state;
        if (this.state.length != 0) {
          this.profile_label_city = 'city';
        }
        else {
          this.profile_label_city = '';
        }
        this.profile_label_location = '';
      }
      else {
        const index: number = this.state.indexOf(item.state_id);
        if (index !== -1) {
          this.state.splice(index, 1);
        }
        if (this.state.length != 0) {
          this.profile_label_city = 'city';
        }
        else {
          this.profile_label_city = '';
        }
        this.profile_label_location = '';


      }
      var data = { "state_id": this.state }
      this.onChangeStateProfile(data, "state");
      this.citys = [];
      this.location_profile = [];
      const val = this.citys.filter(f => f === this.selectedCitys.city_id);
      this.selectedCitys = val;
      if (this.state.length != 0) {
        if (this.citys.length != 0) {
          this.editprofile_disable = 'false'
        } else {
          this.editprofile_disable = 'true';
        }
      } else {
        this.editprofile_disable = 'true';
      }
    }

  }
  onItemSelectCityedituser(item: any) {

    this.city_flag = "";
    this.location_profile = [];
    this.assigned_label = "location"
    this.profile_label_location = 'location';
    var isPresent = this.citys.some(function (el) { return el === item.city_id });
    if (isPresent) {
    } else {
      this.citys.push(item.city_id);
    }
    var datas = { "city_id": this.citys }
    this.onChangeStateProfile(datas, "city");
    const val = this.locations.filter(f => f === this.selectedLocations.location_id);

    this.selectedLocations = val;          //storing the api response in the array                   
    if (this.citys.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }

  }
  onItemSelectCityedittech(item: any) {
    this.city_flag = "";
    this.location_profile = [];
    this.assigned_label = "location"
    this.profile_label_location = 'location';
    var isPresent = this.citys.some(function (el) { return el === item.city_id });
    if (isPresent) {
    } else {
      this.citys.push(item.city_id);
    }
    var data = { "employee_code": this.tech.assigned_service_desk, "city_id": this.citys }
    this.onChangetechnicianlocation(data);
    const val = this.locations.filter(f => f === this.selectedLocations.location_id);

    this.selectedLocations = val;          //storing the api response in the array                   
    if (this.citys.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }
  }
  onItemSelectCity(item: any) {
    // console.log(this.AddProfile.value.role_id)
    if (this.AddProfile.value.role_id == 6) {
      this.city_flag = "";
      this.location_profile = [];
      this.assigned_label = "location"
      this.profile_label_location = 'location';
      var isPresent = this.citys.some(function (el) { return el === item.city_id });
      if (isPresent) {
      } else {
        this.citys.push(item.city_id);
      }
      var data = { "employee_code": this.servcedesk_code, "city_id": this.citys }
      this.onChangetechnicianlocation(data);
      const val = this.locations.filter(f => f === this.selectedLocations.location_id);

      this.selectedLocations = val;          //storing the api response in the array                   
      if (this.citys.length != 0) {
        if (this.location_profile.length != 0) {
          this.editprofile_disable = 'false'
        } else {
          this.editprofile_disable = 'true';
        }
      } else {
        this.editprofile_disable = 'true';
      }
    }
    else {
      this.city_flag = "";
      this.location_profile = [];
      this.assigned_label = "location"
      this.profile_label_location = 'location';
      var isPresent = this.citys.some(function (el) { return el === item.city_id });
      if (isPresent) {
      } else {
        this.citys.push(item.city_id);
      }
      var datas = { "city_id": this.citys }
      this.onChangeStateProfile(datas, "city");
      const val = this.locations.filter(f => f === this.selectedLocations.location_id);

      this.selectedLocations = val;          //storing the api response in the array                   
      if (this.citys.length != 0) {
        if (this.location_profile.length != 0) {
          this.editprofile_disable = 'false'
        } else {
          this.editprofile_disable = 'true';
        }
      } else {
        this.editprofile_disable = 'true';
      }
    }


  }
  onSelectAllCityedituser(items: any) {
    this.all_city = [];
    this.assigned_label = "city"
    this.citys = "All";
    this.location_profile = [];
    console.log(this.assigned_label)
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_city.push(value.city_id);
      })
    }

    // this.profile_label = 'state';
    // this.profile_label_city = 'city';
    this.profile_label_location = '';
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    this.editprofile_disable = 'false';
    this.city_flag = "All";
    this.EditProfile.controls['role_id'].setValue(this.EditProfile.value.role_id);
    this.country_profile = this.EditProfile.value.country_profile;
    this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);

    // this.city_flag = "All";
    // this.assigned_label = "city"
    // this.profile_label_location = '';
    // this.citys = "All";
    // this.location_profile = [];
    // this.locations = []
    // this.editprofile_disable = 'false';
    // // this.location_profile = [];
    // this.selectedLocations = [];
    // this.selectedLocation_name = [];
    // this.EditProfile.controls['role_id'].setValue(this.EditProfile.value.role_id);
    // this.country_profile = this.EditProfile.value.country_profile;
    // this.EditProfile.controls['country_profile'].setValue(this.EditProfile.value.country_profile);

  }
  onSelectAllCitytech(items: any) {
    this.city__all = items;
    this.all_city = [];
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_city.push(value.city_id);
      })
    }
    this.city_flag = "All";
    this.location_profile = [];
    this.assigned_label = "city"
    this.profile_label_location = '';
    this.citys = "All";
    this.editprofile_disable = 'false';
    this.location_profile = [];
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    // if (this.AddProfile.value.valid == false) {
    //   this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    //   this.country_profile = this.AddProfile.value.country_profile;
    //   this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    // }

    // else if (this.EditProfile.value.valid == false) {
    //   this.EditProfile.controls['role_id'].setValue(this.EditProfile.value.role_id);
    //   this.country_profile = this.EditProfile.value.country_profile;
    //   this.EditProfile.controls['country_profile'].setValue(this.EditProfile.value.country_profile);
    // }
    // else if (this.TechProfile.value.valid == false) {
    this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
    this.country_profile = this.TechProfile.value.country_profile;
    this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
    // }
    // else {

    // }
  }
  onSelectAllCity(items: any) {
    this.all_city = [];
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_city.push(value.city_id);
      })
    }
    this.city_flag = "All";
    this.location_profile = [];
    this.assigned_label = "city"
    this.profile_label_location = '';
    this.citys = "All";
    this.editprofile_disable = 'false';
    this.location_profile = [];
    this.selectedLocations = [];
    this.selectedLocation_name = [];
    if (this.AddProfile.value.valid == false) {
      this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      this.country_profile = this.AddProfile.value.country_profile;
      this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);
    }

    // else if (this.EditProfile.value.valid == false) {
    //   this.EditProfile.controls['role_id'].setValue(this.EditProfile.value.role_id);
    //   this.country_profile = this.EditProfile.value.country_profile;
    //   this.EditProfile.controls['country_profile'].setValue(this.EditProfile.value.country_profile);
    // }
    // else if (this.TechProfile.value.valid == false) {
    //   this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
    //   this.country_profile = this.TechProfile.value.country_profile;
    //   this.TechProfile.controls['country_profile'].setValue(this.TechProfile.value.country_profile);
    // }
    else {

    }
  }
  onItemDeSelectAllCityuseredit(items: any) {
    // this.AddProfile.value.valid = false;
    this.EditProfile.value.valid = false;
    // console.log(this.AddProfile.value.valid);
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.city_flag = "";
    this.location_profile = [];
    this.profile_label_location = '';
    this.citys = items;
    if (this.citys.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    } this.citys = this.citys.filter(item => item !== items.city_id);
    var data = { "city_id": this.citys }
    this.onChangeStateProfile(data, "city");
  }
  onItemDeSelectAllCity(items: any) {
    // this.AddProfile.value.valid = false;
    // this.EditProfile.value.valid = false;
    // console.log(this.AddProfile.value.valid);
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.city_flag = "";
    this.location_profile = [];
    this.profile_label_location = '';
    this.citys = items;
    if (this.citys.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    } this.citys = this.citys.filter(item => item !== items.city_id);
    var data = { "city_id": this.citys }
    this.onChangeStateProfile(data, "city");
  }
  onItemDeSelectCityedituser(item: any) {
    this.selectedCitys
    // console.log(this.all_city)
    this.all_city = [];
    this.city_flag = "";
    this.location_profile = [];
    if (this.citys == "All") {
      const index: number = this.selectedCitys.indexOf(item.city_id);
      if (index !== -1) {
        this.selectedCitys.splice(index, 1);
      }
      this.selectedCitys.forEach(value => {
        this.all_city.push(value.city_id);
      })
      this.citys = this.all_city;
      // this.citys = this.selectedCitys;
      if (this.citys.length != 0) {
        this.profile_label_location = 'location';
      }
      else {
        this.profile_label_location = '';
      }
      this.profile_label_location = '';

    }
    else {
      const index: number = this.citys.indexOf(item.city_id);
      if (index !== -1) {
        this.citys.splice(index, 1);
      }

      if (this.citys.length != 0) {
        this.profile_label_location = 'location';
      }
      else {
        this.profile_label_location = '';
      }

    }

    var datas = { "employee_code": this.servcedesk_code, "city_id": this.citys }
    // this.techstates(datas);
    this.onChangeStateProfile(datas, "city");
    this.selectedLocation_name = [];
    if (this.location_profile.length != 0) {
      this.editprofile_disable = 'false'
    } else {
      this.editprofile_disable = 'true';
    }

  }
  onItemDeSelectCityedittech(item: any) {
    this.city_flag = "";
    // console.log(this.citys)

    // console.log(this.all_city)
    // console.log(this.city__all)
    this.location_profile = [];
    if (this.citys == "All") {
      this.citys = [];
      const index: number = this.all_city.indexOf(item.city_id);
      if (index !== -1) {
        this.all_city.splice(index, 1);
      }
      // const index1: number = this.city__all.indexOf(item.city_id);
      // if (index1 !== -1) {
      //   this.city__all.splice(index, 1);
      // }
      // console.log(this.all_city)
      // console.log(this.city__all)
      // this.citys = this.all_city

      this.selectedCityName = this.all_city
      this.selectedCityName.forEach((city) => {
        this.citys.push(city.city_id);
      })
      // console.log(this.citys)
      if (this.citys.length != 0) {
        this.profile_label_location = 'location';
      }
      else {
        this.profile_label_location = '';
      }
      this.profile_label_location = '';

    }
    else {
      const index: number = this.citys.indexOf(item.city_id);
      if (index !== -1) {
        this.citys.splice(index, 1);
      }
      // console.log(this.citys)
      if (this.citys.length != 0) {
        this.profile_label_location = 'location';
      }
      else {
        this.profile_label_location = '';
      }

    }
    // console.log(this.citys)
    var datas = { "employee_code": this.tech.assigned_service_desk, "city_id": this.citys }
    this.onChangetechnicianlocation(datas);
    this.selectedLocation_name = [];
    if (this.location_profile.length != 0) {
      this.editprofile_disable = 'false'
    } else {
      this.editprofile_disable = 'true';
    }
  }
  onItemDeSelectCity(item: any) {
    // console.log(item)
    if (this.AddProfile.value.role_id == 6) {
      this.city_flag = "";
      this.location_profile = [];
      if (this.citys == "All") {
        const index: number = this.all_city.indexOf(item.city_id);
        if (index !== -1) {
          this.all_city.splice(index, 1);
        }
        this.citys = this.all_city;
        if (this.citys.length != 0) {
          this.profile_label_location = 'location';
        }
        else {
          this.profile_label_location = '';
        }
        this.profile_label_location = '';

      }
      else {
        const index: number = this.citys.indexOf(item.city_id);
        if (index !== -1) {
          this.citys.splice(index, 1);
        }
        // console.log(this.citys)
        if (this.citys.length != 0) {
          this.profile_label_location = 'location';
        }
        else {
          this.profile_label_location = '';
        }

      }
      // console.log(this.citys)
      var datas = { "employee_code": this.servcedesk_code, "city_id": this.citys }
      this.techstatesadd(datas);
      this.selectedLocation_name = [];
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    }
    // else if (this.EditProfile.value.role_id == 6) {
    //   this.city_flag = "";
    //   this.location_profile = [];
    //   if (this.citys == "All") {
    //     const index: number = this.all_city.indexOf(item.city_id);
    //     if (index !== -1) {
    //       this.all_city.splice(index, 1);
    //     }
    //     this.citys = this.all_city;
    //     if (this.citys.length != 0) {
    //       this.profile_label_location = 'location';
    //     }
    //     else {
    //       this.profile_label_location = '';
    //     }
    //     this.profile_label_location = '';

    //   }
    //   else {
    //     const index: number = this.citys.indexOf(item.city_id);
    //     if (index !== -1) {
    //       this.citys.splice(index, 1);
    //     }

    //     if (this.citys.length != 0) {
    //       this.profile_label_location = 'location';
    //     }
    //     else {
    //       this.profile_label_location = '';
    //     }

    //   }

    //   var datas = { "employee_code": this.servcedesk_code, "city_id": this.citys }
    //   this.techstates(datas);
    //   this.selectedLocation_name = [];
    //   if (this.location_profile.length != 0) {
    //     this.editprofile_disable = 'false'
    //   } else {
    //     this.editprofile_disable = 'true';
    //   }
    // }
    else {
      this.city_flag = "";
      this.location_profile = [];
      if (this.citys == "All") {
        const index: number = this.all_city.indexOf(item.city_id);
        if (index !== -1) {
          this.all_city.splice(index, 1);
        }
        this.citys = this.all_city;
        if (this.citys.length != 0) {
          this.profile_label_location = 'location';
        }
        else {
          this.profile_label_location = '';
        }
        this.profile_label_location = '';

      }
      else {
        const index: number = this.citys.indexOf(item.city_id);
        if (index !== -1) {
          this.citys.splice(index, 1);
        }

        if (this.citys.length != 0) {
          this.profile_label_location = 'location';
        }
        else {
          this.profile_label_location = '';
        }

      }


      var data = { "city_id": this.citys }

      this.onChangeStateProfile(data, "city");
      this.selectedLocation_name = [];
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    }


  }
  onItemSelectLocationuseredit(item: any) {

    this.location_flag = "";
    this.assigned_label = "location";
    var isPresent = this.location_profile.some(function (el) { return el === item.location_id });
    if (isPresent) {
    } else {
      this.location_profile.push(item.location_id);
    }
    if (this.location_profile.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }

  }
  onItemSelectLocationtech(item: any) {
    this.location_flag = "";
    this.assigned_label = "location"
    var isPresent = this.location_profile.some(function (el) { return el === item.location_id });
    if (isPresent) {
    } else {
      this.location_profile.push(item.location_id);
    }
    // console.log(this.location_profile)
  }
  onItemSelectLocation(item: any) {
    if (this.AddProfile.value.role_id == 6) {
      this.AddProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
      this.location_flag = "";
      this.assigned_label = "location"
      var isPresent = this.location_profile.some(function (el) { return el === item.location_id });
      if (isPresent) {
      } else {
        this.location_profile.push(item.location_id);
      }
    }

    else {
      this.location_flag = "";
      this.assigned_label = "location"
      var isPresent = this.location_profile.some(function (el) { return el === item.location_id });
      if (isPresent) {
      } else {
        this.location_profile.push(item.location_id);
      }
      // console.log(this.location_profile)
      // if (this.location_profile.length != 0) {
      //   if (this.location_profile.length != 0) {
      //     this.editprofile_disable = 'false'
      //   } else {
      //     this.editprofile_disable = 'true';
      //   }
      // } else {
      //   this.editprofile_disable = 'true';
      // }
    }
  }
  ProfileCountry(data) {
    this.onChangeStateProfile(data, "state");
  }
  onSelectAllLocationuseredit(items: any) {
    this.all_location = [];
    var isPresent = this.location_profile.some(function (el) { return el === items.location_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_location.push(value.location_id);
      })
    }
    this.location_flag = "All";
    this.assigned_label = "location"
    this.location_profile = "All";
    if (this.location_profile.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }

    this.EditProfile.controls['role_id'].setValue(this.AddProfile.value.role_id);
    this.country_profile = this.EditProfile.value.country_profile;
    this.EditProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);

  }
  onSelectAllLocation(items: any) {
    this.all_location = [];
    var isPresent = this.location_profile.some(function (el) { return el === items.location_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.all_location.push(value.location_id);
      })
    }
    this.location_flag = "All";
    this.assigned_label = "location"
    this.location_profile = "All";
    if (this.location_profile.length != 0) {
      if (this.location_profile.length != 0) {
        this.editprofile_disable = 'false'
      } else {
        this.editprofile_disable = 'true';
      }
    } else {
      this.editprofile_disable = 'true';
    }
    this.country_profile = this.AddProfile.value.country_profile;
    this.AddProfile.controls['country_profile'].setValue(this.AddProfile.value.country_profile);

  }
  onItemDeSelectAllLocationuseredit(items: any) {
    this.EditProfile.value.valid = false;
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.location_flag = "";
    this.location_profile = items;
    if (this.location_profile.length != 0) {
      this.editprofile_disable = 'false'
    } else {
      this.editprofile_disable = 'true';
    }
  }
  onItemDeSelectAllLocation(items: any) {
    // this.AddProfile.value.valid = false;
    // console.log(this.AddProfile.value.valid);
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.location_flag = "";
    this.location_profile = items;
    if (this.location_profile.length != 0) {
      this.editprofile_disable = 'false'
    } else {
      this.editprofile_disable = 'true';
    }
  }
  onItemDeSelectLocationuseredit(item: any) {
    this.location_flag = "";
    // console.log(this.location_profile)
    // console.log(this.selectedLocations)
    // console.log(this.all_location)
    if (this.location_flag == "All") {
      this.location_profile = [];
      this.all_location = [];
      const index: number = this.selectedLocations.indexOf(item.location_id);
      if (index !== -1) {
        this.selectedLocations.splice(index, 1);
      }
      this.selectedLocations.forEach(value => {
        this.all_location.push(value.location_id);
      })
      this.location_profile = this.all_location;


    }
    else {
      this.location_profile = [];
      this.all_location = [];
      const index: number = this.selectedLocations.indexOf(item.location_id);
      if (index !== -1) {
        this.selectedLocations.splice(index, 1);
      }
      this.selectedLocations.forEach(value => {
        this.all_location.push(value.location_id);
      })
      this.location_profile = this.all_location;


    }
    // // if (this.location_profile == "All") {
    //   // console.log(this.all_location)
    //   // if (this.all_location != []) {
    //     const index: number = this.selectedLocations.indexOf(item.location_id);
    //     if (index !== -1) {
    //       this.selectedLocations.splice(index, 1);
    //     }

    //     var location_profile = this.selectedLocations;
    //   // }
    //   // else {
    //   //   const index: number = this.selectedLocations.indexOf(item.location_id);
    //   //   if (index !== -1) {
    //   //     this.selectedLocselectedLocationsations.splice(index, 1);
    //   //   }
    //   //   this.location_profile = this.selectedLocations;
    // console.log(this.location_profile)
    //   // }


    // // }
    // // else {

    //   this.location_profile = location_profile.filter(items => items !== item.location_id);
    // // }
    // // var isPresent = location_profile.some(function (el) { return el === item.location_id });
    // // if (isPresent) {
    // // } else {
    // //   this.location_profile.push(item.location_id);
    // // }
    // // var isPresent = location_profile.indexOf(item.location_id);
    // // if (isPresent) {
    // // } else {
    // //   this.location_profile.push(item.location_id);
    // // }
    // console.log(this.location_profile)
    // // if (this.location_profile.length != 0) {
    // //   this.editprofile_disable = 'false'
    // // } else {
    // //   this.editprofile_disable = 'true';
    // // }
  }
  onItemDeSelectLocation(item: any) {
    this.location_flag = "";
    if (this.location_profile == "All") {
      const index: number = this.all_location.indexOf(item.location_id);
      if (index !== -1) {
        this.all_location.splice(index, 1);
      }
      this.location_profile = this.all_location;
    }
    else {
      this.location_profile = this.location_profile.filter(items => items !== item.location_id);
    }
    if (this.location_profile.length != 0) {
      this.editprofile_disable = 'false'
    } else {
      this.editprofile_disable = 'true';
    }
  }
  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
  }
  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
    }
  }
  ngOnInit() {
    this.employee = localStorage.getItem('employee_code');
    this.httplink = environment.image_static_ip;
    // console.log(this.httplink)
    this.userdetails = localStorage.getItem('user_status')
    this.onChangecountries(101);
    this.onChangeCountry(101);
    this.getsegment();
    this.citiees = [
      { state_id: 1, state_name: 'New Delhi' },
      { state_id: 2, state_name: 'Mumbai' },
      { state_id: 3, state_name: 'Bangalore' },
      { state_id: 4, state_name: 'Pune' },
      { state_id: 5, state_name: 'Chennai' },
      { state_id: 6, state_name: 'Navsari' }
    ];
    // this.selectedStates = this.selectedStates;
    this.dropdownSettingscountry = {
      singleSelection: false,
      idField: 'country_id',
      textField: 'country_name',
      itemsShowLimit: 5,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'state_id',
      textField: 'state_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownCitySettings = {
      singleSelection: false,
      idField: 'city_id',
      textField: 'city_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownLocationSettings = {
      singleSelection: false,
      idField: 'location_id',
      textField: 'location_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownProductSettings = {
      singleSelection: false,
      idField: 'product_id',
      textField: 'product_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownSegmenttSettings = {
      singleSelection: false,
      idField: 'segment_id',
      textField: 'segment_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownservicedeskSettings = {
      singleSelection: false,
      idField: 'employee_code',
      textField: 'full_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: this.ShowFilter
    }
    this.dropdownsubProductSettings = {
      singleSelection: false,
      idField: 'product_sub_id',
      textField: 'product_sub_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    this.dropdownservicegroupSettings = {
      singleSelection: false,
      idField: 'work_type_id',
      textField: 'work_type',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: this.ShowFilter
    };
    this.org_type = localStorage.getItem('org_type');
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    this.getuserdetails();             //get individual user details from api


  }
  //--------------------------------------------------------------------------------------------------//
  //get country dropdown
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.country = []
      }
      else {
        this.country = [];
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  getassigncountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.assigncountry = result.response.data;              //storing the api response in the array
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.assigncountry = []
      }
      else {
        this.assigncountry = [];
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_servicegroup() {
    var url = 'get_servicegroup/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.servicegroup_data = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment_details = result.response.data;

      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  getnode() {
    var url = 'get_org_node/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.node = result.response.data;                    //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  getrole() {
    var url = 'role_data/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.role = result.response.data;                    //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  getproduct() {
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.product = result.response.data;                    //storing the api response in the array
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  onChangeProduct(product_id) {

    var product = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;
        this.AddProduct.controls['product_sub_id'].setErrors({ 'incorrect': true });


      }

    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }
  getuserdetails() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_all_userdetails/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.user_details = result.response.data;
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();                  //storing the api response in the array


      }
      else if (result.response.response_code == "500") {
        this.overlayRef.detach();
        this.toastr.error(result.response.message);

      }
      else {
        this.overlayRef.detach();
        this.toastr.error("Something Went Wrong")
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  gettechdetails() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_alltechnician_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.tech_detail = result.response.data;                    //storing the api response in the array
        this.showtech = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        this.overlayRef.detach();
        this.toastr.error(result.response.message);

      }
      else {
        this.overlayRef.detach();
        this.toastr.error("Something Went Wrong")
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  opentech(tech_details) {
    this.modalService.open(tech_details, {
      size: 'lg',
      // windowClass: "center-modalsm",     //modal popup custom size                                    //specifies the size of the dialog box
      backdrop: 'static',
      keyboard: false
    });
  }
  gettechproduct(name, code) {
    this.AddProduct.reset();
    this.showmodelTable = false;
    this.chRef.detectChanges();
    this.employee_product = code;
    this.employee_name = name;
    // this.overlayRef.attach(this.LoaderComponentPortal);
    // this.employee_product = this.productdata.employee_code; // country: number    
    // this.employee_name =  this.productdata.full_name;
    var url = 'get_technician_products/?';             // api url for getting the details with using post params
    var id = "employee_code";
    this.ajax.getdataparam(url, id, this.employee_product).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.tech_product_details = result.response.data;                    //storing the api response in the array
        this.showmodelTable = true;
        this.chRef.detectChanges();
        // this.overlayRef.detach();



      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');
        // this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error');
        // this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }



  //--------------------------------------------------------------------------------------------------//
  fetchNews(evt: any) {
    this.preId = evt.activeId;
    this.activeId = evt.nextId;
    this.evt = evt; // has nextId that you can check to invoke the desired function
    if (this.activeId == "tab-selectbyid1") {
      this.showTable = false;
      this.chRef.detectChanges();
      this.getuserdetails();
    }
    else if (this.activeId == "tab-selectbyid2") {
      this.showtech = false;
      this.chRef.detectChanges();
      this.gettechdetails();

    }
  }
  //Starts Add User Functions 

  //function for add user

  submit() {
    this.loadingAdd = true;
    if (this.AddProfile.value.role_id == '4') {
      if (this.AddProfile.value.servicedesk_type == 1) {
        // this.s
      }
      else if (this.AddProfile.value.servicedesk_type == 2) {

      }
    }

    var data = {
      "employee_code": this.AddCompany.value.employee_code, "email_id": this.AddCompany.value.email_id, "full_name": this.AddCompany.value.full_name, "contact_number": this.AddCompany.value.contact_number, "alternate_contact_number": this.AddCompany.value.alternate_contact_number, "country_id": 101, "state_id": this.AddUser.value.state_id, "city_id": this.AddUser.value.city_id, "post_code": this.AddUser.value.post_code, "landmark": this.AddUser.value.landmark, "street": this.AddUser.value.street, "plot_number": this.AddUser.value.plot_number, "location_id": 35, "location_label": "state", "address": "", "role_id": this.AddProfile.value.role_id, "node_id": this.AddProfile.value.node_id
      , "assigned_country_id": 101, "assigned_state": this.state, "assigned_city": this.citys, "assigned_location": this.location_profile, "assigned_label": this.assigned_label, "product": this.products, "subproduct": this.subproducts, "service_group": this.servicegroups, "assigned_service_desk": this.servcedesk_code, "technician_type": this.AddProfile.value.technician_type, "servicedesk_type": this.AddProfile.value.servicedesk_type, "allocated_segment": this.segment_data
    };
    var url = 'add_userdetails/'                                         //api url of add
    this.ajax.postdata(url, data).subscribe((result) => {
      //if sucess
      if (result.response.response_code == "200") {
        this.loadingAdd = false;
        this.modalService.dismissAll();
        if (this.AddProfile.value.role_id == 6) {
          this.activeId = "tab-selectbyid2";
          this.showtech = false;
          this.chRef.detectChanges();
          this.gettechdetails();
        }
        else {
          this.activeId = "tab-selectbyid1";
          this.showTable = false;
          this.chRef.detectChanges();
          this.getuserdetails();
        }
        this.toastr.success(result.response.message, 'Success');    //toastr message for success

      }
      //if error
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingAdd = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingAdd = false;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  add_cus_info(evt: any) {
    if (this.email_error == '' && this.contactnum_erroe == '' && this.emp_error == '') {
      this.activeId = "tab-selectbyidj";
      this.preId = "tab-selectbyidi";

      this.onChangecountries(101);                     //get country details
    }
    else {
      this.activeId = "tab-selectbyidi";
      this.preId = "tab-selectbyidj";
    }
  }
  add_cus_location() {

    this.preId = "tab-selectbyidj";
    this.activeId = "tab-selectbyidk";
    this.nextId = "tab-selectbyidi";
    this.getnode();
    this.getlocationdata();
    this.getrole();
    this.getlocationstructure();
    this.level = this.getlevel;
    if (this.org_type == 1) {
      this.AddProfile = new FormGroup({
        "role_id": new FormControl(this.AddProfile.value.role_id, [Validators.required]),
        "node_id": new FormControl(this.AddProfile.value.node_id),
        "technician_type": new FormControl(this.AddProfile.value.technician_type),
        "assigned_service_desk": new FormControl(this.AddProfile.value.assigned_service_desk),
        "servicedesk_type": new FormControl(this.AddProfile.value.servicedesk_type),
        "allocated_segment": new FormControl(this.AddProfile.value.allocated_segment),
        "country_profile": new FormControl(101),
        "states_profile": new FormControl(this.AddProfile.value.states_profile, [Validators.required]),
        "products": new FormControl(this.AddProfile.value.products),
        "servicegroup": new FormControl(this.AddProfile.value.servicegroup),
        "subproducts": new FormControl(this.AddProfile.value.subproducts),
        "city_profile": new FormControl(this.AddProfile.value.city_profile),
        "loc_profile": new FormControl(this.AddProfile.value.loc_profile),
      });
    }
    else {
      this.AddProfile = new FormGroup({
        "role_id": new FormControl(this.AddProfile.value.role_id, [Validators.required]),
        "node_id": new FormControl(this.AddProfile.value.node_id, [Validators.required]),
        "technician_type": new FormControl(this.AddProfile.value.technician_type),
        "assigned_service_desk": new FormControl(this.AddProfile.value.assigned_service_desk),
        "servicedesk_type": new FormControl(this.AddProfile.value.servicedesk_type),
        "allocated_segment": new FormControl(this.AddProfile.value.allocated_segment),
        "country_profile": new FormControl(101),
        "states_profile": new FormControl(this.AddProfile.value.states_profile),
        "products": new FormControl(this.AddProfile.value.products),
        "servicegroup": new FormControl(this.AddProfile.value.servicegroup),
        "subproducts": new FormControl(this.AddProfile.value.subproducts),
        "city_profile": new FormControl(this.AddProfile.value.city_profile),
        "loc_profile": new FormControl(this.AddProfile.value.loc_profile),
      });
    }
  }

  get log() {
    return this.AddCompany.controls;                       //error logs for create user
  }
  get profile() {
    return this.AddProfile.controls;                       //error logs for create user
  }
  get users() {
    return this.AddUser.controls;                       //error logs for create user
  }
  addEmailValidation() {
    this.AddCompany.value.invalid = true;
    this.params = {
      "email_id": this.AddCompany.value.email_id
    }

    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.AddCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.AddCompany.value.invalid = true;
        this.AddCompany.controls['email_id'].setErrors({ 'incorrect': true });

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });



  }
  addContactValidation() {
    this.AddCompany.value.invalid = true;
    this.params = {
      "contact_number": this.AddCompany.value.contact_number
    }

    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_erroe = '';
        this.AddCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_erroe = result.response.message;
        this.AddCompany.value.invalid = true;
        this.AddCompany.controls['contact_number'].setErrors({ 'incorrect': true });


      }



    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }

  addacontact() {
    if (this.AddCompany.value.alternate_contact_number == this.AddCompany.value.contact_number) {
      this.contact = "Contact and alternate contact number are should n't be same.";
      this.AddCompany.value.invalid = true;
      this.AddCompany.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact = '';
      this.AddCompany.value.invalid = false;
      this.AddCompany.controls['contact_number'].setValue(this.AddCompany.value.contact_number);
    }
  }
  addEmployeeValidation() {
    this.AddCompany.value.invalid = true;
    this.params = {
      "employee_code": this.AddCompany.value.employee_code
    }

    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.emp_error = '';
        this.AddCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.emp_error = result.response.message;
        this.AddCompany.value.invalid = true;
        this.AddCompany.controls['employee_code'].setErrors({ 'incorrect': true });



      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  //Ends add User functions
  // function for modal box
  openLarge(add, row) {
    this.Filename = "";
    this.ByDefault = false;
    this.bulk_error = '';
    this.bulk_succ = "";
    // this.product_details=[];
    this.select = [];
    this.selectedSub = [];
    // this.subproducts_data='';
    this.profile_label = '';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.employee_code = row
    this.modalService.open(add, {
      size: 'lg',                              //specifies the size of the dialog box
      windowClass: "center-modalsm",      //modal popup custom size           
      backdrop: 'static',
      keyboard: false
    });
  }
  // function for modal box
  openModal(add, row) {
    this.contact = '';
    this.product_details = [];
    this.role_type != 5;
    this.city_name = [];
    this.activeId = "tab-selectbyidi";
    this.employee_code = row
    this.modalService.open(add, {
      size: 'lg',                              //specifies the size of the dialog box
      backdrop: 'static',
      keyboard: false
    });
  }
  openModaluser(add, row) {
    this.profile_label = '';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.employee_code = row
    this.modalService.open(add, {
      size: 'lg',                              //specifies the size of the dialog box
      backdrop: 'static',
      keyboard: false
    });
  }
  openlarge(add, row) {
    this.employee_code = row
    this.modalService.open(add, {
      size: 'lg',                              //specifies the size of the dialog box
      windowClass: "center-modalsm",      //modal popup custom size           
      backdrop: 'static',
      keyboard: false
    });
  }
  openproduct(add, row) {
    this.employee_code = row
    this.modalService.open(add, {
      size: 'lg',                                  //specifies the size of the dialog box
      windowClass: "center-modalsm",      //modal popup custom size       
      backdrop: 'static',
      keyboard: false
    });
  }
  openmodal(add) {
    this.ByDefault = false;
    this.bulk_error = '';
    this.bulk_succ = "";
    this.contact = '';
    this.role_type = '1';
    this.techerror = '';
    this.select = [];
    this.selectedSub = [];
    this.products = [];
    this.subproducts = [];
    this.servicegroups = [];
    this.state = [];
    this.citys = [];
    this.assignstates = [];
    this.assigncountry = [];
    this.getassigncountry();
    this.location_profile = [];
    this.loadingBulk = false;
    this.loadingEdit = false;
    this.loadingAdd = false;

    this.AddCompany.reset();
    this.AddUser.reset();
    this.AddProfile.reset();
    this.getproduct();
    this.get_servicegroup();
    this.contactnum_erroe = '';
    this.email_error = '';
    this.emp_error = '';
    this.error = '';
    this.errors = '';
    this.bulk = '';
    this.file_name = undefined;
    this.Filename = '';
    this.activeId = "tab-selectbyidi";
    this.profile_label = '';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.modalService.open(add, {
      size: 'lg',
      windowClass: "center-modalsm",      //modal popup custom size                                    //specifies the size of the dialog box
      backdrop: 'static',
      keyboard: false
    });
    this.AddProduct.reset();
  }
  //--------------------------------------------------------------------------------------------------//
  //functions for bulk upload
  fileUpload(event) {
    //read the file
    this.bulk = '';
    this.error = "";
    this.loadingBulk = false;
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    this.ByDefault = true;
  };
  fileproUpload(event) {
    this.type = '';
    this.Filename = '';
    this.file_name = '';
    this.bulk = [];
    //read the file
    this.error = "";
    this.loadingBulk = false;
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    this.ByDefault = true;
  };

  //after submit the file for product
  filesubmit(bulkupload) {
    this.bulk = [];
    this.bulk_error = '';
    this.bulk_succ = "";
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined && this.file_name == '') {
      // this.toastr.error("Please select file", 'Error');
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {                   //if file is not empty
      if (validExts[0] != this.type) {
        // this.toastr.error("Please select correct extension", 'Error');
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) {
        this.loadingBulk = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        var gh = file;
        var method = "post";                                    //post method 
        var url = 'user_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          // if (data['response_code'] == "200") {
          //   this.error = '';
          //   this.loadingBulk = false;
          //   if (data['response'].fail.length > 0) {                                    //failure response 
          //     //if employee code or employee id is error to put this
          //     for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
          //       var a = [];
          //       data['response'].fail.forEach(value => {
          //         a.push(value.error_message
          //         );
          //       });

          //       this.bulk = a;
          //       this.file_name = '';
          //       // this.ByDefault = false;
          //     }
          //   }
          //   //success response
          //   //if newly add product for excisting employee code or employee id
          //   if (data['success'].length > 0) {                         //sucess response
          //     for (i = 0; i < data['success'].length; i++) {
          //       this.bulk = '';
          //       this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
          //       this.showTable = false;
          //       this.chRef.detectChanges();
          //       this.getuserdetails();
          //       this.file_name = '';
          //       // this.ByDefault = false;

          //     }
          //   }
          // }

          // //if other errors
          // else if (data['response'].response_code == "500") {
          //   this.bulk = [];
          //   this.file_name = '';
          //   // this.ByDefault = false;
          //   this.loadingBulk = false;
          //   this.error = data['response'].message
          // }
          // else {
          //   this.bulk = [];
          //   this.file_name = '';
          //   // this.ByDefault = false;
          //   this.loadingBulk = false;
          //   this.error = "Something Went Wrong";                                                                  //if not sucess
          // }
          if (data['response_code'] == "200") { //if sucess
            this.loadingBulk = false;
            // if (data['success'].length >= 0) {
            //   this.bulk_error = '';
            //   this.bulk_succ = ""
            //   for (i = 0; i < data['success'].length; i++) {

            //     this.showTable = false;
            //     this.chRef.detectChanges();

            //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success

            //   }
            // }

            if (data['success'].length > 0) {
              // data['success'].forEach(value => {
              //   var b = [];
              //   b.push(value.success_message);
              //   this.bulk_succ = b;
              // })
              var b = [];
              data['success'].forEach(value => {
                b.push(value.success_message
                );

              });
              this.bulk_succ = b;
              this.showTable = false;
              this.chRef.detectChanges();
            }
            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );

                });

                this.bulk_error = a;
                this.file_name = '';
                this.loadingBulk = false;
              }
            }
            this.loadingBulk = false;
            this.getuserdetails();
          }
          //if other errors
          else if (data['response'].response_code == "500") {
            this.bulk_error = '';
            this.bulk_succ = ""
            this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = data['response'].message
          }
          else {
            this.loadingBulk = false;

            this.bulk_error = '';
            this.bulk_succ = ""
            this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = "Something Went Wrong";                                                                  //if not sucess
          }
        },

          (err) => {
            this.bulk = [];
            this.file_name = '';
            // this.ByDefault = false;                                                                 //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }

  };
  tabBack(preId) {
    this.activeId = preId;
    this.preId = this.activeId
  }
  tabBackR(prerId) {
    this.activerId = this.prerId;
    this.prerId = this.activerId
  }
  tabBackF(prefId) {
    this.activeId = this.prefId
    this.preId = this.activerId
  }

  get addp() {
    return this.AddProduct.controls;
  }
  //Edit user Functions starts

  //Function to show the user details based on the employee_code for edit
  edituser(data, edit_user) {

    this.alternate = "";
    this.email_error = '';
    this.activeId = "tab-selectbyidx";
    var employee_code = data.employee_code; // country: number                                    

    var url = 'get_userdetails/?';             // api url for getting the details with using post params
    var id = "employee_code";
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.user = result.response.data[0];
        this.chRef.detectChanges();
        this.modalService.open(edit_user, {
          size: 'lg',                              //specifies the size of the dialog box
          windowClass: "center-modalsm",      //modal popup custom size           
          backdrop: 'static',
          keyboard: false
        });

        this.employee_id = this.user.employee_id;
        this.EditCompany.setValue({
          "employee_id": this.employee_id,
          "email_id": this.user.email_id,
          "full_name": this.user.full_name,
          "contact_number": this.user.contact_number,
          "alternate_contact_number": this.user.alternate_contact_number,

        });

        this.onChangeState(this.user.state.state_id);
        this.EditUser = new FormGroup({
          "country_id": new FormControl(this.user.country.country_id, [Validators.required]),
          "state_id": new FormControl(this.user.state.state_id, [Validators.required]),
          "city_id": new FormControl(this.user.city.city_id, [Validators.required]),
          "post_code": new FormControl(this.user.post_code, [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
          "landmark": new FormControl(this.user.landmark),
          "street": new FormControl(this.user.street, [Validators.required, this.noWhitespace]),
          "plot_number": new FormControl(this.user.plot_number, [Validators.required, this.noWhitespace, Validators.pattern("^[0-9%+-/a-z A-Z]{2,32}$")]),
          "location_id": new FormControl('35'),

          "location_label": new FormControl(this.user.location_label),

        });
        this.onChangecountriesuseredit(this.user.assigned_country.assigned_country_id);
        // this.onChangeCountry(this.user.assigned_country.assigned_country_id);

        this.user_status = this.user.user_status;
        this.assigned_label = this.user.assigned_label;//check assigened label
        if (this.assigned_label == "location") { //if the location is location the assign label as state city and location also
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = "location";
          this.selecteRole = this.user.role.role_id;
          this.selectedViewStates = this.user.assigned_state;
          this.selectedViewcitys = this.user.assigned_city;
          this.selectedViewLocation = this.user.assigned_location;
        }
        else if (this.assigned_label == "city") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = '';
          this.selectedViewStates = this.user.assigned_state;
          this.selectedViewcitys = this.user.assigned_city;
          this.selectedViewLocation = [];
          this.citys = "All";
        }
        else if (this.assigned_label == "state") {
          this.profile_label = "state";
          this.profile_label_city = '';
          this.profile_label_location = '';
          this.selectedViewStates = this.user.assigned_state;
          this.selectedViewcitys = [];
          this.selectedViewLocation = [];
          this.location_profile = [];
          this.state = "All";
        }
        else {
          this.profile_label = '';
          this.profile_label_city = '';
          this.profile_label_location = '';
          this.selectedViewStates = [];
          this.selectedViewcitys = [];
          this.selectedViewLocation = [];
        }

        this.EditProfile = new FormGroup({
          "role_id": new FormControl(this.user.role.role_id),
          "node_id": new FormControl(this.user.node_id),
          "country_profile": new FormControl(this.user.assigned_country.assigned_country_id),
          "states_profile": new FormControl(this.user.assigned_state),
          "city_profile": new FormControl(this.user.assigned_city),
          "servicedesk_type": new FormControl(this.user.servicedesk_type),
          "allocated_segment": new FormControl(this.user.allocated_segment),
          "loc_profile": new FormControl(this.user.assigned_location),
          "products": new FormControl(this.user.product),
          "servicegroup": new FormControl(this.user.service_group),
          "subproducts": new FormControl(this.user.subproduct),
          "assigned_service_desk": new FormControl(this.user.assigned_service_desk),
        });
        this.servcedesk_code = this.user.assigned_service_desk;
        this.role_type = this.user.role.role_id;
        this.getrole();
        this.onChangeRole(this.role_type);
        this.selectedProducts = this.user.product;
        this.selectedSubProducts = this.user.subproduct;
        this.selectedServicegroup = this.user.service_group;
        this.user.allocated_segment.forEach((seg) => {
          var isproductPresent = this.user.allocated_segment.some(function (el) { return el === seg.segment_id });
          if (isproductPresent) {
          } else {
            this.segment_data.push(seg.segment_id);
          }
        })
        if (this.role_type == 5) {
          if (this.user.product != null) {
            this.selectedProducts = this.selectedProducts;
          } else {
            this.selectedProducts = [];
          }
          if (this.user.service_group != null) {
            this.selectedServicegroup = this.selectedServicegroup;
          } else {
            this.selectedServicegroup = [];
          }

        }

        else {
          this.selectedProducts = [];
          this.selectedSubProducts = [];
          this.selectedServicegroup = [];
        }
        this.selectedProducts.forEach((product) => {
          var isproductPresent = this.selectedProducts.some(function (el) { return el === product.product_id });
          if (isproductPresent) {
          } else {
            this.products.push(product.product_id);
          }
        })
        var data = { "product_id": this.products }
        this.subproducts_data = [];
        this.subproducts = this.user.subproduct;
        this.onChangegeteditProduct(data);
        this.country_profile = this.user.assigned_country.assigned_country_id;
        if (this.user.assigned_state) {
          this.sleectedStates = this.user.assigned_state;
          this.sleectedStates.forEach(value => {
            this.all_state.push(value.state_id);
          })
          this.all_state = this.all_state;
          this.state = this.all_location;
        }
        else {
          this.sleectedStates = [];
        }
        if (this.user.assigned_city) {
          this.selectedCitys = this.user.assigned_city;
          this.selectedCitys.forEach(value => {
            this.all_city.push(value.city_id);
          })
          this.citys = this.all_city;
        }
        if (this.user.assigned_location) {
          this.selectedLocations = this.user.assigned_location;
          this.selectedLocations.forEach(value => {
            this.all_location.push(value.location_id);
          })
          this.location_profile = this.all_location;
        }
        if (this.sleectedStates != "All") {
          this.state = [];
          this.sleectedStates.forEach((st) => {
            var isstate = this.sleectedStates.some(function (el) {
              return el === st.state_id
            });
            if (isstate) {
            } else {
              this.state.push(st.state_id);
            }
          })
        }



        if (this.selectedCitys == null) {
          this.citys = [];
        }
        else if (this.selectedCitys != "All" || this.selectedCitys != null) {
          this.citys = [];
          this.selectedCitys.forEach((city) => {
            this.citys.push(city.city_id);
          })
        }

        var state_data = { "state_id": this.state }
        this.onChangeStateProfileedit(state_data, "state");
        var city_data = { "city_id": this.citys }
        this.onChangeStateProfileedit(city_data, "city");




        this.selectedLocations.forEach((loc) => {
          this.locations.push(loc.location_id);
        })

        this.state_flag = this.user.state_flag;
        this.city_flag = this.user.city_flag;
        this.location_flag = this.user.location_flag;
        this.service_flag = this.user.service_flag;
        this.product_flag = this.user.product_flag;
        this.subproduct_flag = this.user.subproduct_flag;
        this.service_flag = this.user.service_flag;
        if (this.product_flag == "All" || this.subproduct_flag == "All" || this.subproduct_flag == null || this.user.subproduct != null || this.selectedSubProducts == null) {
          this.product_label == "sub";
          this.subproducts = [];

        }


        else if (this.selectedSubProducts != "All" || this.selectedSubProducts != [] || this.selectedSubProducts != null) {
          this.subproducts = [];
          this.all_subproduct = [];
          this.selectedSubProducts.forEach((subproduct) => {
            var isSubproductPresent = this.selectedSubProducts.some(function (el) { return el === subproduct.product_sub_id });
            if (isSubproductPresent) {
            } else {
              this.subproducts.push(subproduct.product_sub_id);
              this.all_subproduct.push(subproduct.product_sub_id)
            }
          })
        }
        this.subproducts = this.all_subproduct;
        this.selectedServicegroup.forEach((servicegroup) => {
          var isPresent = this.servicegroup_data.some(function (el) { return el === servicegroup.work_type_id });
          if (isPresent) {
          } else {
            this.servicegroup_data.push(servicegroup.work_type_id);
          }

        })
        this.assigned_label = this.user.assigned_label;
        if (this.assigned_label == "location") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = "location";
        }
        else if (this.assigned_label == "city") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.citys = "All";
          this.profile_label_location = '';
        }
        else if (this.assigned_label == "state") {
          this.profile_label = "state";
          this.state = "All";
          this.profile_label_city = '';
          this.profile_label_location = '';
        }
        else {
          this.profile_label = '';
          this.profile_label_city = '';
          this.profile_label_location = '';
        }
        this.onChangeState(this.user.state.state_id);

      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  userValidation(param) {
    this.params = param;
    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;

      }
      else if (result.response.response_code == "500") {
        this.validation_data = result.response.message;
        if (this.validation_data == "Email already exist") {
          this.email_error = "Email already exist";
          this.emp_error = undefined;
          this.contactnum_erroe = undefined;
        }
        else if (this.validation_data == "Contact Number already exist") {
          this.contactnum_erroe = "Contact Number already exist";
          this.email_error = undefined;
          this.emp_error = undefined;
        }
        else if (this.validation_data == "Employee code already exist") {
          this.emp_error = "Employee code already exist";
          this.email_error = undefined;
          this.contactnum_erroe = undefined;
        }
        else {

        }
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  edit_cus_info(evt: any) {
    // if (this.email_error == '' && this.contactnum_erroe == '') {

    this.activeId = "tab-selectbyidy";
    this.preId = "tab-selectbyidx";
    this.getcountry();                        //get country details
    // }

    // else {
    //   this.activeId = "tab-selectbyidx";
    //   this.preId = "tab-selectbyidy";
    // }
  }

  edit_cus_location() {
    this.activeId = "tab-selectbyidz";
    this.preId = "tab-selectbyidy";
    if (this.org_type === 1) {

      this.getnode();
      this.getlocationdata();
      this.getrole();
      this.getlocationstructure();
      this.level = this.getlevel;
    }
    else if (this.org_type == 2) {
      // this.activeId = "tab-selectbyidz";
      // this.preId = "tab-selectbyidy";
      this.getnode();
      this.getlocationdata();
      this.getrole();
      this.getlocationstructure();
      this.level = this.getlevel;
      this.profile_label = '';
      this.profile_label_city = '';
      this.profile_label_location = '';

    }
  }
  editEmailValidation() {

    this.params = {
      "email_id": this.EditCompany.value.email_id
    }
    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.EditCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.EditCompany.value.invalid = true;
        this.EditCompany.controls['email_id'].setErrors({ 'incorrect': true });


      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });


  }
  editContactValidation() {

    this.params = {
      "contact_number": this.EditCompany.value.contact_number
    }
    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_erroe = '';
        this.EditCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {

        this.contactnum_erroe = result.response.message;
        this.EditCompany.value.invalid = true;
        this.EditCompany.controls['contact_number'].setErrors({ 'incorrect': true });

      }



    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });


  }

  editEmployeeValidation() {
    var params = {
      "employee_code": this.EditCompany.value.employee_code
    }
    this.userValidation(this.params)
  }
  // Function for submit the user edit value
  useredit() {
    // if (this.activeId == "tab-selectbyid1" || this.activerId == "tab-selectbyid1") {
    //   this.submitted = true;
    // }
    // else if (this.activeId == "tab-selectbyid2" || this.activerId == "tab-selectbyid2") {
    //   this.submitted = false;
    //   this.submitted1 = true;
    // }
    // else {
    //   this.submitted2 = true;
    // }
    // if (this.EditCompany.invalid) {
    //   this.submitted = true;
    // }
    // else if (this.EditUser.invalid) {
    //   this.submitted1 = true;
    // }
    // else if (this.EditProfile.invalid) {
    //   this.submitted2 = true;
    // }
    // else {
    if (this.assigned_label == "state") {
      this.state = "All";
      this.citys = [];
      this.location_profile = [];
    }
    // else {
    //   this.state = this.state;
    // }
    if (this.assigned_label == "city") {
      this.citys = "All";
      this.location_profile = [];
    }
    //  else {
    //   this.citys = this.citys;
    // }
    if (this.assigned_label == "location") {
      // if
      this.location_profile = this.location_profile;
    }
    // else {
    //   this.location_profile = this.location_profile;
    // }
    if (this.product_flag == "All") {
      this.subproducts = [];
      this.products = "All";
    } else {
      this.products = this.products;
      this.subproducts = this.subproducts;
    }
    if (this.subproduct_flag == "All") {
      this.subproducts = "All";
    } else {
      this.subproducts = this.subproducts;
    }
    if (this.service_flag == "All") {
      this.servicegroups = "All";
    } else {
      this.servicegroups = this.servicegroups;
    }
    if (this.EditProfile.value.node_id == '' || this.EditProfile.value.node_id == null || this.EditProfile.value.node_id == undefined) {
      this.EditProfile.value.node_id = '';
    }
    else {
      this.EditProfile.value.node_id = this.EditProfile.value.node_id;
    }
    if (this.EditProfile.value.role_id == '4') {
      if (this.EditProfile.value.servicedesk_type == 1) {
        // this.s
      }
      else if (this.EditProfile.value.servicedesk_type == 2) {

      }
    }
    var data = {
      "employee_id": this.EditCompany.value.employee_id, "email_id": this.EditCompany.value.email_id, "full_name": this.EditCompany.value.full_name, "contact_number": this.EditCompany.value.contact_number, "alternate_contact_number": this.EditCompany.value.alternate_contact_number, "country_id": this.EditUser.value.country_id, "state_id": this.EditUser.value.state_id, "city_id": this.EditUser.value.city_id, "post_code": this.EditUser.value.post_code, "landmark": this.EditUser.value.landmark, "street": this.EditUser.value.street, "plot_number": this.EditUser.value.plot_number, "location_id": this.EditUser.value.location_id, "location_label": this.EditUser.value.location_label, "address": this.EditCompany.value.address, "role_id": this.user.role.role_id, "node_id": this.EditProfile.value.node_id, "assigned_country_id": 101,
      "assigned_state": this.state, "assigned_city": this.citys, "assigned_location": this.location_profile, "assigned_label": this.assigned_label, "product": this.products, "subproduct": this.subproducts, "service_group": this.servicegroups, "user_status": this.user_status, "assigned_service_desk": this.servcedesk_code, "servicedesk_type": this.EditProfile.value.servicedesk_type, "allocated_segment": this.segment_data
    };
    // return data;
    this.Editloading = true;
    var url = 'edit_userdetails/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                   //if sucess
        {
          this.Editloading = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.modalService.dismissAll();
          this.getuserdetails();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success

        }
        else if (result.response.response_code == "500") {                 //if failiure
          this.Editloading = false;
          this.toastr.error(result.response.message, 'Error');            //toastr message for error
        }
        else {                                                             //if not sucess
          this.Editloading = false;
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        this.Editloading = false;
        console.log(err);                                                 //prints if it encounters an error
      });
    // }
  }

  // convenience getter for easy access to form fields

  get logs() {
    return this.EditCompany.controls;                       //error logs for edit user
  }
  get editprofile() {
    return this.EditProfile.controls;                       //error logs for create user
  }
  get editusers() {
    return this.EditUser.controls;                       //error logs for create user
  }

  editcontact() {
    if (this.EditCompany.value.contact_number == this.EditCompany.value.alternate_contact_number) {
      this.alternate = "Contact number and alternate contact number are should n't be same.";
      this.EditCompany.value.invalid = true;
      this.EditCompany.controls['contact_number'].setErrors({ 'incorrect': true });

    }

    else {
      this.alternate = '';
      this.EditCompany.value.invalid = false;
      this.EditCompany.controls['contact_number'].setValue(this.EditCompany.value.contact_number);
    }
  }
  //Edit user Function ends
  //Technician validation Function Starts
  techcontact() {
    if (this.TechCompany.value.contact_number == this.TechCompany.value.alternate_contact_number) {
      this.alternate = "Contact number and alternate contact number are should n't be same.";
      this.TechCompany.value.invalid = true;
      this.TechCompany.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.alternate = '';
      this.TechCompany.value.invalid = false;
      this.TechCompany.controls['contact_number'].setValue(this.TechCompany.value.contact_number);
    }
  }


  techEmailValidation() {
    this.params = {
      "email_id": this.TechCompany.value.email_id
    }
    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.TechCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.TechCompany.value.invalid = true;
        this.TechCompany.controls['email_id'].setErrors({ 'incorrect': true });


      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  techContactValidation() {

    this.params = {
      "contact_number": this.TechCompany.value.contact_number
    }
    var url = 'user_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_erroe = '';
        this.TechCompany.value.invalid = false;
      }
      else if (result.response.response_code == "500") {

        this.contactnum_erroe = result.response.message;
        this.TechCompany.value.invalid = true;
        this.TechCompany.controls['contact_number'].setErrors({ 'incorrect': true });

      }



    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  tech_cus_info() {
    this.activeId = "tab-selectbyid1";
    this.preId = "tab-selectbyid1";
    this.activeId = "tab-selectbyidb";
    this.preId = "tab-selectbyida";

    this.getcountry();                        //get country details
  }
  onchangetechcityedit(data) {
    this.cities = [];

    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;
        this.selectedCityName = this.cities;
        // this.TechProfile.value.valid = false;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onchangetechcity(data) {
    // this.cities = [];

    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;
        this.selectedCityName = this.tech.assigned_city;
        // this.TechProfile.value.valid = false;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onchangetechlocation(data) {
    this.locations = [];
    this.location_profile = [];
    this.profile_label_location = "location";
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.locations = result.response.data;
        this.selectedLocation_name = this.tech.assigned_location;
        this.tech.assigned_location.forEach((lo) => {
          var isstate = this.tech.assigned_location.some(function (el) {
            return el === lo.location_id
          });
          if (isstate) {
          } else {
            this.location_profile.push(lo.location_id);
          }
        })
        // this.TechProfile.value.valid = false;
        // this.TechProfile.controls['role_id'].setErrors({ 'incorrect': true });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.TechProfile.value.valid = true;
        this.TechProfile.controls['role_id'].setValue(this.TechProfile.value.role_id);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  tech_cus_location() {
    this.activeId = "tab-selectbyidc";
    this.preId = "tab-selectbyidb";
    this.getrole();
    this.level = this.getlevel;
    if (this.tech.assigned_label == "location") {
      this.location_profile = "All";
      this.profile_label = "state";
      this.profile_label_city = "city";
      this.profile_label_location = "location";
      this.assigned_label = "location"
      this.state = [];
      this.all_city = [];
      this.sleectedStates = this.tech.assigned_state;
      this.tech.assigned_state.forEach((st) => {
        var isstate = this.tech.assigned_state.some(function (el) {
          return el === st.state_id
        });
        if (isstate) {
        } else {
          this.state.push(st.state_id);
        }
      })
      var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
      this.onchangetechcity(data);
      this.all_state = this.state;
      this.selectedCityName = this.tech.assigned_city;
      // this.tech.assigned_city.forEach((city) => {
      //   var iscity = this.tech.assigned_city.some(function (el) {
      //     return el === city.city_id
      //   });
      //   if (iscity) {
      //   } else {
      //     this.citys.push(city.city_id);
      //   }
      // })
      this.selectedCityName.forEach(value => {
        this.all_city.push(value.city_id);
      })
      this.citys = this.all_city;
      var data1 = { "employee_code": this.tech.assigned_service_desk, "city_id": this.citys }
      this.onchangetechlocation(data1);
      this.city__all = this.citys;
    }
    else if (this.tech.assigned_label == "city") {
      this.assigned_label = "city"
      this.profile_label = "state";
      this.profile_label_city = "city";
      this.citys = "All";
      this.state = [];
      this.tech.assigned_state.forEach((st) => {
        var isstate = this.tech.assigned_state.some(function (el) {
          return el === st.state_id
        });
        if (isstate) {
        } else {
          this.state.push(st.state_id);
        }
      })
      var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
      this.onchangetechcity(data);
      this.all_city = this.tech.assigned_city;
    }
    else {
      this.assigned_label = "state";
      this.state = "All";
    }
    this.all_city = this.tech.assigned_city;
  }

  get techC() {
    return this.TechCompany.controls;                       //error logs for edit technicians
  }
  get techusers() {
    return this.TechUser.controls;                       //error logs for edit technicians
  }
  get techprofile() {
    return this.TechProfile.controls;                       //error logs for edit technicians
  }
  //Technician validation function ends

  //Function to show the technician details based on the employee_code for edit
  techview(view_tech, data) {

    var employee_code = data.employee_code; // country: number                                    
    var url = 'view_user_data/?';             // api url for getting the details with using post params
    var id = "employee_code";
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.tech = result.response.data[0];
        this.chRef.detectChanges();
        this.modalService.open(view_tech, {
          size: 'lg',                              //specifies the size of the dialog box
          windowClass: "center-modalsm",      //modal popup custom size           
          backdrop: 'static',
          keyboard: false
        });
        this.assigned_label = this.tech.assigned_label;//check assigened label
        if (this.tech.role.role_id == 6) {
          this.punch = "Enable";
          this.service_type = "Disable";
          this.technician_type = 'Enable';
          if (this.tech.technician_type == 1) {
            this.type_tech = 'Inhouse Technician';
          }
          else if (this.tech.technician_type == 2) {
            this.type_tech = 'Partner Service Engineer';
          }
        }
        else if (this.tech.role.role_id == 4) {
          this.service_type = "Enable";
          this.allocate_seg = this.tech.allocated_segment;
          if (this.tech.service_desk_type == 1) {
            this.servicetype_tech = 'ARBL service desk';
          }
          else if (this.tech.service_desk_type == 2) {
            this.servicetype_tech = 'Partner service desk';
          }
          else {
            this.servicetype_tech = '-';
          }
        }
        else {
          this.service_type = "Disable";
          this.punch = "Disable";
          this.technician_type = '';
        }

        if (this.assigned_label == "location") { //if the location is location the assign label as state city and location also

          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = "location";
          this.selecteRole = this.tech.role.role_id;
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = this.tech.assigned_city;
          this.selectedViewLocation = this.tech.assigned_location;

        }
        else if (this.assigned_label == "city") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = '';
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = this.tech.assigned_city;
          this.selectedViewLocation = [];
          this.citys = "All";

        }
        else if (this.assigned_label == "state") {
          this.profile_label = "state";
          this.profile_label_city = '';
          this.profile_label_location = '';
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = [];
          this.selectedViewLocation = [];
          this.location_profile = [];
          this.state = "All";
        }

        this.city = this.tech.assigned_city;
        this.state_name = this.tech.assigned_state;
        this.product_ids = this.tech.product;
        this.subproduct_ids = this.tech.subproduct;
        this.service_ids = this.tech.service_group;
        this.location_name = this.tech.assigned_location;
        var isPresent = this.state.some(function (el) { return el === this.tech.assigned_state.state_id });
        if (isPresent) {
        } else {
          this.state.push(this.tech.assigned_state.state_id);
        }
        var data = { "employee_code": this.servcedesk_code, "state_id": this.state }
        this.onchangetechniciancity(data);
        var isPresent = this.citys.some(function (el) { return el === this.tech.assigned_city.city_id });
        if (isPresent) {
        } else {
          this.citys.push(this.tech.assigned_city.city_id);
        }
        var data1 = { "employee_code": this.tech.assigned_service_desk, "city_id": this.citys }
        this.onChangetechnicianlocation(data1);
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');
      }
      else {
        this.toastr.error("Something Went Wrong", 'Error');
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //Function to show the technician details based on the employee_code for edit
  edittech(edit_tech, data) {
    this.chRef.detectChanges();
    this.techerror = '';
    this.alternate = '';
    this.loadingEdit = false;
    this.sleectedStates = [];
    this.selectedCitys = [];
    this.selectedLocations = [];
    this.servicegroups = [];
    this.products = [];
    // this.profile_label = '';
    // this.profile_label_city = '';
    // this.profile_label_location = '';

    var employee_code = data.employee_code; // country: number                                    

    var url = 'get_userdetails/?';             // api url for getting the details with using post params
    var id = "employee_code";
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.activeId = "tab-selectbyid2";
        this.tech = result.response.data[0];
        this.punch = "Disable";
        this.chRef.detectChanges();
        this.modalService.open(edit_tech, {
          size: 'lg',                              //specifies the size of the dialog box
          windowClass: "center-modalsm",      //modal popup custom size           
          backdrop: 'static',
          keyboard: false
        });

        this.TechUser = new FormGroup({
          "country_id": new FormControl(this.tech.country.country_id, [Validators.required]),
          "state_id": new FormControl(this.tech.state.state_id, [Validators.required]),
          "city_id": new FormControl(this.tech.city.city_id, [Validators.required]),
          "post_code": new FormControl(this.tech.post_code, [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
          "landmark": new FormControl(this.tech.landmark),
          "street": new FormControl(this.tech.street, [Validators.required, this.noWhitespace]),
          "plot_number": new FormControl(this.tech.plot_number, [Validators.required, this.noWhitespace, Validators.pattern("^[0-9%+-/a-z A-Z]{2,32}$")]),
          "location_id": new FormControl('35', [Validators.required]),
          "location_label": new FormControl(this.tech.location_label),

        });
        this.getrole();
        this.getassigncountry();
        this.onChangecountries(this.tech.assigned_country.assigned_country_id);
        this.onChangeCountryProfile(this.tech.assigned_country.assigned_country_id)
        this.onChangeStates(this.tech.state.state_id);
        this.TechProfile = new FormGroup({
          "role_id": new FormControl(this.tech.role.role_id, [Validators.required]),
          "node_id": new FormControl(this.tech.node_id, [Validators.required]),
          "country_profile": new FormControl(this.tech.assigned_country.assigned_country_id),
          "states_profile": new FormControl(this.tech.assigned_state),
          "city_profile": new FormControl(this.tech.assigned_city),
          "loc_profile": new FormControl(this.tech.assigned_location),
          "products": new FormControl(this.tech.product),
          "servicegroup": new FormControl(this.tech.service_group),
          "subproducts": new FormControl(this.tech.subproduct),
          "assigned_service_desk": new FormControl(this.tech.assigned_service_desk),
          "technician_type": new FormControl(this.tech.technician_type),
        });
        this.TechCompany.setValue({
          "employee_id": this.tech.employee_id,
          // "address": new FormControl(this.user.address),
          "email_id": this.tech.email_id,
          "full_name": this.tech.full_name,
          "contact_number": this.tech.contact_number,
          "alternate_contact_number": this.tech.alternate_contact_number,

        });
        this.user_status = this.tech.user_status
        this.servcedesk_code = this.tech.assigned_service_desk
        this.role_type = this.tech.role.role_id;
        this.onChangeRole(this.role_type);
        this.onChangeservice(this.tech.assigned_service_desk);

        this.selectedServicegroup = [];

        // this.country_profile = this.tech.assigned_country.assigned_country_id;
        // this.TechProfile.controls['country_profile'].setValue(this.tech.assigned_country.assigned_country_id);

        // if (this.tech.assigned_state != []) {
        //   this.sleectedStates = this.tech.assigned_state;
        // }
        // else {
        //   this.sleectedStates = [];
        // }
        this.selectedStatestech = this.tech.assigned_state;
        if (this.tech.assigned_city != []) {
          this.selectedCityName = this.tech.assigned_city;
        }
        if (this.tech.assigned_location != []) {
          this.selectedLocation_name = this.tech.assigned_location;
        }

        if (this.sleectedStates != "All") {
          this.state = [];
          this.sleectedStates.forEach((st) => {
            var isstate = this.sleectedStates.some(function (el) {
              return el === st.state_id
            });
            if (isstate) {
            } else {
              this.state.push(st.state_id);
            }
          })
        }
        var state_data = { "state_id": this.state }
        var data = { "employee_code": this.tech.employee_id, "state_id": this.state }


        if (this.tech.city_flag != 'All') {


          if (this.selectedCityName == null) {
            this.citys = [];
          }
          else if (this.selectedCityName != "All" || this.selectedCitys != null) {
            this.citys = [];
            this.selectedCityName.forEach((city) => {
              this.citys.push(city.city_id);
            })
          }
          var datas = { "employee_code": this.tech.employee_id, "city_id": this.citys }
          this.selectedCityName = this.tech.assigned_location

        }
        else if (this.tech.city_flag == "All") {
          this.selectedCityName = 'All';
        }
        if (this.tech.location_flag != 'All') {

          this.selectedLocations.forEach((loc) => {
            this.locations.push(loc.location_id);
          })
        }
        else if (this.tech.location_flag == "All") {
          this.selectedLocation_name = 'All';
        }
        this.state_flag = this.tech.state_flag;
        this.city_flag = this.tech.city_flag;
        this.location_flag = this.tech.location_flag;
        this.assigned_label = this.tech.assigned_label;
        if (this.assigned_label == "location") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = "location";
          this.selecteRole = this.tech.role.role_id;
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = this.tech.assigned_city;
          this.selectedViewLocation = this.tech.assigned_location;
        }
        else if (this.assigned_label == "city") {
          this.profile_label = "state";
          this.profile_label_city = "city";
          this.profile_label_location = '';
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = this.tech.assigned_city;
          this.selectedViewLocation = [];
        }
        else if (this.assigned_label == "state") {
          this.profile_label = "state";
          this.profile_label_city = '';
          this.profile_label_location = '';
          this.selectedViewStates = this.tech.assigned_state;
          this.selectedViewcitys = [];
          this.selectedViewLocation = [];
        }
        else {
          this.profile_label = '';
          this.profile_label_city = '';
          this.profile_label_location = '';
          this.selectedViewStates = [];
          this.selectedViewcitys = [];
          this.selectedViewLocation = [];
        }




      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

  // Function for submit the tech edit value
  techedit() {

    if (this.assigned_label == "state") {
      this.state = "All";
      this.citys = [];
      this.location_profile = [];
    } else {
      this.state = this.state;
    }
    if (this.assigned_label == "city") {
      this.citys = "All";
      this.location_profile = [];
    } else {
      this.citys = this.citys;
    }
    if (this.assigned_label == "location") {
      this.location_profile = this.location_profile;
    }

    if (this.product_flag == "All") {
      this.products = "All";
      this.subproducts = [];
    } else {
      this.products = this.products;
    }
    if (this.subproduct_flag == "All") {
      this.subproducts = "All";
    } else {
      this.subproducts = this.subproducts;
    }
    if (this.service_flag == "All") {
      this.servicegroups = "All";
    } else {
      this.servicegroups = this.servicegroups;
    }



    var data = {
      "employee_id": this.TechCompany.value.employee_id, "email_id": this.TechCompany.value.email_id, "full_name": this.TechCompany.value.full_name, "contact_number": this.TechCompany.value.contact_number, "alternate_contact_number": this.TechCompany.value.alternate_contact_number, "country_id": this.TechUser.value.country_id, "state_id": this.TechUser.value.state_id, "city_id": this.TechUser.value.city_id, "post_code": this.TechUser.value.post_code, "landmark": this.TechUser.value.landmark, "street": this.TechUser.value.street, "plot_number": this.TechUser.value.plot_number, "location_id": this.TechUser.value.location_id, "location_label": this.TechUser.value.location_label, "address": this.TechCompany.value.address, "role_id": this.TechProfile.value.role_id, "node_id": this.TechProfile.value.node_id
      , "assigned_country_id": 101, "assigned_state": this.state, "assigned_city": this.citys, "assigned_location": this.location_profile, "assigned_label": this.assigned_label, "product": this.products, "subproduct": this.subproducts, "service_group": this.servicegroups, "user_status": this.user_status, "assigned_service_desk": this.servcedesk_code, "technician_type": this.TechProfile.value.technician_type
    };
    this.loadingEdit = true;
    var url = 'edit_userdetails/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                   //if sucess
        {

          this.showtech = false;
          this.chRef.detectChanges();
          this.modalService.dismissAll();
          this.gettechdetails();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingEdit = false;
          this.activeId = "tab-selectbyid2";
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');            //toastr message for error
          this.loadingEdit = false;                                    //if failiure
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          this.loadingEdit = false;                                 //if not sucess
        }
      }, (err) => {
        this.loadingEdit = true;
        console.log(err);                                                 //prints if it encounters an error
      });
  }



  //Function to show the technician details based on the employee_code for edit


  // Function for adding product for
  submitaddproduct(tech_details) {
    this.submitted = true;
    this.loadingSubmit = true;
    this.AddProduct.controls['employee_code'].setValue(this.employee_product);
    if (this.AddProduct.invalid) {
      this.toastr.error("Please choose the type", 'Error');
    }
    else {
      var url = 'add_technician/'                                         //api url of add
      this.ajax.postdata(url, this.AddProduct.value).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.techerror = '';
          this.showmodelTable = false;
          this.chRef.detectChanges();
          this.modalService.dismissAll();   //to close the modal box
          // var datas = { "employee_code": this.employee_product }
          // this.employee_product=code;
          // this.employee_name=name;

          this.gettechproduct(this.employee_name, this.employee_product);
          this.opentech(tech_details);
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingSubmit = false;
        }
        else if (result.response.response_code == "500") {
          this.techerror = result.response.message        //toastr message for error
          this.loadingSubmit = false;
        }
        else {
          this.techerror = result.response.message
          this.loadingSubmit = false;
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  trashtech(data) {
    var dat = { "employee_code": data.employee_code }
    // var employeecode=data.employee_code;           //Data to be passed for delete api                        
    var url = 'user_delete/';             // api url for delete warehouse
    var id = "employee_code";                       //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, dat).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {                       //if sucess
            this.showtech = false;
            this.chRef.detectChanges();
            this.gettechdetails();                                               //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "500") {                  //if failiure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                          //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  //delete for user based on employee code
  trashuser(data) {
    var dat = { "employee_code": data.employee_code }
    // var employeecode=data.employee_code;           //Data to be passed for delete api                        
    var url = 'user_delete/';             // api url for delete warehouse
    var id = "employee_code";                       //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, dat).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {                       //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.getuserdetails();                                               //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "500") {                  //if failiure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                          //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }

  //delete for technician_product based on technician id
  trashtectproduct(data, tech_details) {
    data["technican_details_id"] = data.technican_details_id;
    var url = 'delete_technician/';             // api url for delete warehouse
    var id = "technican_details_id";                       //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {                       //if sucess
            this.showmodelTable = false;
            this.chRef.detectChanges();
            // var datas = { "employee_code": this.employee_product }
            this.gettechproduct(this.employee_name, this.employee_product);                                  //reloading the component
            this.opentech(tech_details);
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "500") {                  //if failiure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                          //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }


  //after submit the file for product
  filesubmit_tech(tech_details) {
    this.bulk = '';
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
    }
    if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.error = "Please select correct extension";
      }                //if file is not empty
      else if (validExts = this.type) {
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        var method = "post";                                    //post method 
        var url = 'user_product_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          // if (data['response_code'] == "200") {                        //if sucess
          //   if (data['response'].error_code == "404") {
          //     if (data['response'].fail.length > 0) {                                    //failure response
          //       for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
          //         var a = [];
          //         data['response'].fail.forEach(value => {
          //           a.push(value.error_message
          //           );
          //         });
          //         this.bulk = a;
          //       }
          //     }
          //   }

          //   if (data['success'].length > 0) {                         //sucess response
          //     for (i = 0; i < data['success'].length; i++) {                   //count the length
          //       this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess

          //       this.showmodelTable = false;
          //       this.chRef.detectChanges();
          //       // this.modalService.dismissAll();   //to close the modal box
          //       console.log(this.employee_product)
          //       // var datas = { "employee_code": this.employee_product }
          //       this.gettechproduct(this.employee_name, this.employee_product);

          //     }
          //   }
          // }
          // else if (data['response'].response_code == "500") {
          //   this.errors = data['response'].message
          // }
          // else {                                                                 //if not sucess
          //   this.error = "Something Went Wrong";
          // }
          if (data['response_code'] == "200") {    //if sucess
            this.error = '';
            this.loadingBulk = false;
            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );

                });

                this.bulk_error = a;
                // this.file_name = undefined;   //refresh the file 
                this.Filename = '';

              }
            }
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {
              this.loadingBulk = false;                    //sucess response
              for (i = 0; i < data['success'].length; i++) {
                this.bulk_error = '';
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.loadingBulk = false;
                this.showmodelTable = false;
                this.chRef.detectChanges();
                this.gettechproduct(this.employee_name, this.employee_product);                           //to close the modal box
              }
            }
          }
          else if (data['response'].response_code == "500") {
            this.bulk_error = '';
            this.bulk_succ = "";
            this.loadingBulk = false;
            this.error = data['response'].message
          }

          else {
            this.bulk_error = '';
            this.bulk_succ = "";
            this.loadingBulk = false;                                                       //if not sucess
            this.error = "Something Went Wrong";        //toastr message for error
          }
        },

          (err) => {                                                                     //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }
  };
  //---------------------------------------functions for location mapping------------------------------------------------//
  getlocationdata() {
    var url = 'get_location/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getorgnode = result.response.nodes;
      }
    });
  }
  getlocationstructure() {
    var url = 'get_org_structure/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getorgnode = result.response.nodes;
        this.getlevel = result.response.nodes[0].level;
        this.level = this.getlevel;
      }
    });
  }

  //  function for modal boxes
  openlocation(content) {
    this.modalService.open(content, {
      size: 'lg', //specifies the size of the dialog box      
      // windowClass: "center-modalsm",     //modal popup resize                    
      backdrop: 'static',
      keyboard: false
    });
  }
  openXl(content) {
    this.modalService.open(content, {
      size: 'lg',
      backdropClass: 'light-blue-backdrop',
      backdrop: 'static',
      keyboard: false
    });
  }

  // onChangestate(state_id) {
  //   var state = +state_id; // state: number
  //   var id = "state_id";
  //   var url = 'get_location/?';
  //   var node_id = "node_id";
  //   var node = +this.edit_data.id;                                  // api url for getting the cities
  //   this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       this.cities = result.response.data;              //storing the api response in the array                   
  //       this.Edit_node.controls['city_id'].setValue(this.edit_data.cities.city_id);             //view city name in view modal box
  //       this.Edit_node.controls['city_id'].setValue(this.edit_data.cities.city_id);             //view city name in edit modal box
  //     }

  //   }, (err) => {
  //     console.log(err);                                        //prints if it encounters an error
  //   });

  // }
  //Function to call the edit api 
  editform(edit_data) {
    var url = 'update_location/'
    var data = this.Edit_node.value;
    if (data.state_id == null) {
      data.state_id = "";
    }
    if (data.city_id == null) {
      data.city_id = "";
    }
    this.value = {
      "node_id": data.id,
      "country_id": data.country_id,
      "state_id": data.state_id,
      "city_id": data.city_id
    };
    this.ajax.postdata(url, this.value).subscribe((result) => {
      if (result.response.response_code == "200")                           //if sucess
      {
        // setTimeout(() => {                                           //reconstructing the table with new datas with a time out function to wait for the table destroy
        //   $('.datatable').DataTable();
        // }, 1000);
        this.ngOnInit();                                                //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
      }
      else if (result.response.response_code == "400") {                  //if failure
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      }
      else {                                                                     //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {                                                           //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }


  test(data) {
    this.node_value = data;
    // this.AddProfile.value.valid = false;
    this.profile_label = '';
    this.assigned_label = "state";
    this.state = "All";
    // this.AddProfile.controls['country_profile'].setValue('null');
    // this.AddProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.AddProfile.controls['node_id'].setValue(this.node_value.id);
    // if (this.node_value.level == '1') {
    //   // this.AddUser.get('addcountry');
    //   // this.onChangeCountry(this.node_value.country);
    //   // if (this.AddUser) {
    //   //   this.AddUser.controls['country_id'].setValue(data.country);
    //   //   this.AddUser.controls['location_id'].setValue(this.node_value.country);
    //   //   this.AddUser.controls['location_label'].setValue("country")
    //   //   this.activeModal.close()

    //   // }
    //   // else {
    //   //   this.EditUser.controls['country_id'].setValue(data.country);
    //   //   this.EditUser.controls['location_id'].setValue(this.node_value.country);
    //   //   this.EditUser.controls['location_label'].setValue("country")
    //   //   this.activeModal.close()
    //   // }


    // }
    // else if (this.node_value.level == '2') {
    //   var country_id = this.node_value.country;
    //   var country = +country_id; // state: number
    //   var id = "country_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, country, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.states = result.response.data;              //storing the api response in the array                   
    //       if (this.AddUser) {
    //         // this.AddUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //         // this.AddUser.controls['location_id'].setValue(this.node_value.state);
    //         // this.AddUser.controls['location_label'].setValue("state")
    //         this.activeModal.close()
    //       }
    //       else {
    //         // this.EditUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //         // this.EditUser.controls['location_id'].setValue(this.node_value.state);
    //         // this.EditUser.controls['location_label'].setValue("state")
    //         this.activeModal.close()
    //       }
    //     }
    //   }, (err) => {
    //     console.log(err);                                        //prints if it encounters an error
    //   });
    // }
    // else if (this.node_value.level == '3') {
    //   var stateid = this.node_value.state;
    //   var state = +stateid; // state: number
    //   var id = "state_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.cities = result.response.data;              //storing the api response in the array
    //       if (this.AddUser) {
    //         // this.AddUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //         // this.AddUser.controls['location_id'].setValue(this.node_value.city);
    //         // this.AddUser.controls['location_label'].setValue("city")
    //         this.activeModal.close()
    //       }
    //       else {
    //         // this.EditUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //         // this.EditUser.controls['location_id'].setValue(this.node_value.city);
    //         // this.EditUser.controls['location_label'].setValue("city");
    //         this.activeModal.close()
    //       }
    //     }
    //   },
    //     (err) => {
    //       console.log(err);                                        //prints if it encounters an error
    //     });
    // }
    // else if (this.node_value.level == '4') {
    //   if (this.AddUser) {           //view city name in view modal box
    //     // this.AddUser.controls['location_id'].setValue(this.node_value.location);
    //     // this.AddUser.controls['location_label'].setValue("location")
    //     this.activeModal.close()
    //   }
    //   else {            //view city name in view modal box
    //     // this.EditUser.controls['location_id'].setValue(this.node_value.location);
    //     // this.EditUser.controls['location_label'].setValue("location")
    //     this.activeModal.close()
    //   }
    // }
  }
  location_id(data) {
    this.node_value = data;
    this.editprofile_disable = true;
    this.EditProfile.value.valid = false;
    this.profile_label = '';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.user.assigned_country.assigned_country_id = '';
    this.EditProfile.controls['country_profile'].setValue('null');
    // this.EditProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.EditProfile.controls['node_id'].setValue(this.node_value.id);
    // if (this.node_value.level == '1') {
    //   this.onChangeCountry(this.node_value.country);
    //   this.EditUser.controls['country_id'].setValue(data.country);
    //   this.EditUser.controls['location_id'].setValue(this.node_value.country);
    //   this.EditUser.controls['location_label'].setValue("country");
    //   this.TechUser.controls['country_id'].setValue(data.country);
    //   this.TechUser.controls['location_id'].setValue(this.node_value.country);
    //   this.TechUser.controls['location_label'].setValue("country");
    //   this.activeModal.close()

    // }
    // else if (this.node_value.level == '2') {
    //   var country_id = this.node_value.country;
    //   var country = +country_id; // state: number
    //   var id = "country_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, country, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.states = result.response.data;              //storing the api response in the array                   
    //       this.EditUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //       this.EditUser.controls['location_id'].setValue(this.node_value.state);
    //       this.EditUser.controls['location_label'].setValue("state");
    //       this.TechUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.state);
    //       this.TechUser.controls['location_label'].setValue("state");
    //       this.activeModal.close()
    //     }

    //   }, (err) => {
    //     console.log(err);                                        //prints if it encounters an error
    //   });
    // }
    // else if (this.node_value.level == '3') {
    //   var stateid = this.node_value.state;
    //   var state = +stateid; // state: number
    //   var id = "state_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.cities = result.response.data;              //storing the api response in the array
    //       this.EditUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //       this.EditUser.controls['location_id'].setValue(this.node_value.city);
    //       this.EditUser.controls['location_label'].setValue("city");
    //       this.TechUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.city);
    //       this.TechUser.controls['location_label'].setValue("city");
    //       this.activeModal.close()

    //     }
    //   },
    //     (err) => {
    //       console.log(err);                                        //prints if it encounters an error
    //     });
    // }
    // else {
    //   this.EditUser.controls['location_id'].setValue(this.node_value.location);
    //   this.TechUser.controls['location_id'].setValue(this.node_value.location);
    //   this.EditUser.controls['location_label'].setValue("location");
    //   this.TechUser.controls['location_label'].setValue("location");
    // }
  }
  nextTab(currentId, preId, nextId) {
    this.preId = preId;
    this.activeId = currentId;
    this.nextId = nextId;
  }

  location_tech(data) {
    this.node_value = data;
    this.editprofile_disable = true;
    this.EditProfile.value.valid = false;
    this.profile_label = '';
    this.profile_label_city = '';
    this.profile_label_location = '';
    this.tech.assigned_country.assigned_country_id = '';
    this.TechProfile.controls['country_profile'].setValue('null');
    // this.TechProfile.controls['country_profile'].setErrors({ 'incorrect': true });
    this.TechProfile.controls['node_id'].setValue(this.node_value.id);
    // if (this.node_value.level == '1') {
    //   this.onChangeCountry(this.node_value.country);
    //   this.TechUser.controls['country_id'].setValue(data.country);
    //   this.TechUser.controls['location_id'].setValue(this.node_value.country);
    //   this.TechUser.controls['location_label'].setValue("country");
    //   this.TechUser.controls['country_id'].setValue(data.country);
    //   this.TechUser.controls['location_id'].setValue(this.node_value.country);
    //   this.TechUser.controls['location_label'].setValue("country");
    //   this.activeModal.close()

    // }
    // else if (this.node_value.level == '2') {
    //   var country_id = this.node_value.country;
    //   var country = +country_id; // state: number
    //   var id = "country_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, country, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.states = result.response.data;              //storing the api response in the array                   
    //       this.TechUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.state);
    //       this.TechUser.controls['location_label'].setValue("state");
    //       this.TechUser.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.state);
    //       this.TechUser.controls['location_label'].setValue("state");
    //       this.activeModal.close()
    //     }

    //   }, (err) => {
    //     console.log(err);                                        //prints if it encounters an error
    //   });
    // }
    // else if (this.node_value.level == '3') {
    //   var stateid = this.node_value.state;
    //   var state = +stateid; // state: number
    //   var id = "state_id";
    //   var url = 'get_location/?';
    //   var node_id = "node_id";
    //   var node = +this.node_value.id;                                  // api url for getting the cities
    //   this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
    //     if (result.response.response_code == "200") {
    //       this.cities = result.response.data;              //storing the api response in the array
    //       this.TechUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.city);
    //       this.TechUser.controls['location_label'].setValue("city");
    //       this.TechUser.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
    //       this.TechUser.controls['location_id'].setValue(this.node_value.city);
    //       this.TechUser.controls['location_label'].setValue("city");
    //       this.activeModal.close()

    //     }
    //   },
    //     (err) => {
    //       console.log(err);                                        //prints if it encounters an error
    //     });
    // }
    // else {
    //   this.TechUser.controls['location_id'].setValue(this.node_value.location);
    //   this.TechUser.controls['location_id'].setValue(this.node_value.location);
    //   this.TechUser.controls['location_label'].setValue("location");
    //   this.TechUser.controls['location_label'].setValue("location");
    // }
  }

  viewmoredistrict() {
    this.showmore = this.city.length;
  }

  viewlessdistrict() {
    this.showmore = 5;
  }

  viewmorestate() {
    this.showstate = this.selectedViewStates;
  }

  viewlessstate() {
    this.showstate = 5;
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}