// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { Observable, Subject } from 'rxjs';
// import { finalize, takeUntil, tap } from 'rxjs/operators';
// import { style } from '@angular/animations';

// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
// import { Store } from '@ngrx/store';
// import { AppState } from '../../../../core/reducers';
// Auth
import { ToastrService } from 'ngx-toastr';// To use toastr
// import { AuthNoticeService, AuthService, Login } from '../../../../core/auth';

import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
// Layout
// import {  MenuAsideService } from '../../../../core/_base/layout';
import { MenuConfig } from '../../../../core/_config/menu.config';

// Services
import { MenuConfigService } from '../../../../core/_base/layout/services/menu-config.service';
import { MenuHorizontalService } from '../../../../core/_base/layout/services/menu-horizontal.service';
// import Swal from 'sweetalert2';

/**
 * ! Just example => Should be removed in development
 */
// Object-Path
import * as objectPath from 'object-path';
// import { fakeAsync } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
// import {auth} from 'firebase';
import { MessagingService } from '../../../../message.service';

const DEMO_PARAMS = {
	EMAIL: '',
	PASSWORD: ''
};

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	loginForm: FormGroup;
	forgotPasswordForm: FormGroup;
	loadingAdd: boolean = false;
	error_msgs: any; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	// loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];
	menu: any = []; // array push
	submenu: any = []; // array push
	today: number = Date.now();
	stop$ = new Subject();
	private unsubscribe: Subject<any>;

	private returnUrl: any;
	// Private properties
	private menuConfig: MenuConfig;
	aside: any;
	private headerMenus: any;
	private asideMenus: any;
	role_id: any;
	employee_code: any;
	error_msg: any;
	loading: boolean;
	fieldTextType: boolean;
	login: boolean = true;
	forgot: boolean = false;
	loaderclass: string;
	loaderclasssubmit: string;
	message;
	current_token: string;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 * @param route
	 * @param menuConfigService: MenuConfigService
	 * @param MenuHorizontalService:MenuHorizontalService
	 * @param authService
	
	 
	 */
	constructor(private chRef: ChangeDetectorRef,
		public menuConfigService: MenuConfigService,
		private router: Router,
		private MenuHorizontalService: MenuHorizontalService,
		// private auth: AuthService,
		// private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		// private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		public ajax: ajaxservice,
		public toastr: ToastrService,
		// private authService: AuthService,
        private afAuth: AngularFireAuth,

		private messagingService: MessagingService,
		// public menuAsideService: MenuAsideService,
	) {
		this.unsubscribe = new Subject();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {

		this.initLoginForm();
		this.initRegistrationForm();
		this.loaderclass = "kt-spinner--right kt-spinner--md ";
		this.loaderclasssubmit = "kt-spinner--right kt-spinner--md ";
		// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'] || 'dashboard';
		});
		this.messagingService.getToken()

	}
	initRegistrationForm() {
		this.forgotPasswordForm = this.fb.group({
			email: ['', Validators.compose([
				Validators.required,
				Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			]
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {

		// this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		// demo message to show
		// if (!this.authNoticeService.onNoticeChanged$.getValue()) {
		// 	const initialNotice = `Use account
		// 	<strong>${DEMO_PARAMS.EMAIL}</strong> and password
		// 	<strong>${DEMO_PARAMS.PASSWORD}</strong> to continue.`;
		// 	this.authNoticeService.setNotice(initialNotice, 'info');
		// }

		this.loginForm = this.fb.group({
			email: [DEMO_PARAMS.EMAIL, Validators.compose([
				Validators.required,
				// Validators.email, 
				Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			],
			password: [DEMO_PARAMS.PASSWORD, Validators.compose([
				Validators.required,
				Validators.minLength(6),
				Validators.maxLength(100)
			])],
			device_token : [""],
		});
	}

	toggleFieldTextType() {
		this.fieldTextType = !this.fieldTextType;
	}

	/**
	 * Form Submit
	 */
	loginclick() {
		this.login = false;
		this.forgot = true;
	}
	forgotclick() {
		this.forgotPasswordForm.reset();
		this.login = true;
		this.forgot = false;
	}
	submit() {
		this.error_msg = "";
		this.loading = true;
		const controls = this.loginForm.controls;
		this.loaderclass = "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--login signin-btn': loading";


		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.current_token = localStorage.getItem('current_token')
		const authData = {
			username: controls['email'].value,
			password: controls['password'].value,
			// device_token: this.current_token 
		};

		var data = { package_name: authData, status: 1 }
		var url = 'fp_sass_login/'
		this.ajax.postdata(url, authData).subscribe((result) => {
			if (result) {

				if (result.response.response_code == "200") {
					// console.log(result)
					localStorage.setItem('user_status', result.response.user_details.user_status);
					localStorage.setItem('organization_type', result.response.user_details.organization_type);
					localStorage.setItem('user_name', result.response.user_details.full_name);
					localStorage.setItem('user_role', result.response.user_details.role.role_name);
					localStorage.setItem('role_id', result.response.user_details.role.role_id);
					localStorage.setItem('org_type', result.response.user_details.organization_type);
					this.role_id = localStorage.getItem('role_id');
					localStorage.setItem('assigned_state', result.response.user_details.assigned_state);
					localStorage.setItem('employee_code', result.response.user_details.employee_code);
					localStorage.setItem('employee_id', result.response.user_details.employee_id);
					// console.log(result.response.customer_type_details)
					localStorage.setItem('customer_type',JSON.stringify(result.response.customer_type_details) );
					localStorage.setItem('priority_details',JSON.stringify(result.response.priority_details) );
					this.headerMenus = objectPath.get(this.menuConfigService.getMenus(), 'header');
					this.asideMenus = objectPath.get(this.menuConfigService.getMenus(), 'aside');
					localStorage.setItem('asideMenus', this.asideMenus);
					// this.store.dispatch(new Login({ authToken: "1234567" }));
					if (this.role_id == 1) {
						this.router.navigate(["/maindashboard"]);

					}
					else if (this.role_id == 2) {
						this.router.navigate(["/maindashboard"]);

					}
					else if (this.role_id == 3) {
						this.router.navigate(["/dashboard"]);

					}
					else if (this.role_id == 4) {
						this.router.navigate(["service_dashboard"]);

					}
					else if (this.role_id == 5) {
						this.router.navigate(["/manager_dashboard"]);
					}
					else if (this.role_id == 8) {
						this.router.navigate(["/raiseticket"]);

					}	
					this.messagingService.requestPermission()
					this.messagingService.receiveMessage()
					this.message = this.messagingService.currentMessage		
					// this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());

				}
				else {
					this.error_msg = result.response.message;
					// console.log(this.error_msg);
					this.loaderclass = "kt-spinner--right kt-spinner--md kt-spinner--login ";
					this.chRef.detectChanges();

				}
			}

		});
	}

	submits() {
		this.error_msgs = "";
		
		const controls = this.forgotPasswordForm.controls;
		// this.loaderclasssubmit = "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--login signin-btn': loading";
		/** check form */
		if (this.forgotPasswordForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		const authData = {
			email: controls['email'].value,
		};
		this.loading = true;
		this.loadingAdd=true;
		this.chRef.detectChanges();
		var data = { package_name: authData, status: 1 }
		var url = 'forget_password/'
		this.ajax.postdata(url, authData).subscribe((result) => {
			if (result) {
				if (result.response.response_code == "200") {
					this.loadingAdd=false;
					this.chRef.detectChanges();
					this.toastr.success(result.response.message, 'Success');
					//this.store.dispatch(new Login({authToken: result.response.response_code}));
					this.router.navigate(["/login"]);
				}
				else {
					this.loadingAdd=false;
					// this.toastr.error(result.response_code.message, 'Error');
					this.error_msgs = result.response.message
					// this.loaderclasssubmit = "kt-spinner--right kt-spinner--md kt-spinner--login ";
					//  this.toastr.error( this.error_msgs, 'Error');    
					this.chRef.detectChanges();
				}
			}
		}, (err) => {
			console.log(err);
		});
		// const email = controls['email'].value;
		// this.authService.requestPassword(email).pipe(
		// 	tap(response => {
		// 		if (response) {
		// 			this.authNoticeService.setNotice(this.translate.instant('AUTH.FORGOT.SUCCESS'), 'success');
		// 			this.router.navigateByUrl('/auth/login');
		// 		} else {
		// 			this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.NOT_FOUND', {name: this.translate.instant('AUTH.INPUT.EMAIL')}), 'danger');
		// 		}
		// 	}),
		// 	takeUntil(this.unsubscribe),
		// 	finalize(() => {
		// 		this.loading = false;
		// 		this.cdr.markForCheck();
		// 	})
		// ).subscribe();
	}


	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
	isControlHasError1(controlName: string, validationType: string): boolean {
		const control = this.forgotPasswordForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
