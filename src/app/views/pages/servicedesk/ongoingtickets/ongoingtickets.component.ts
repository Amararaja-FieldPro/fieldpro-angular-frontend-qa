import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
//Reactive form 
import { FormGroup, FormControl, Validators } from '@angular/forms';
// Common API service for both get and post
import { ajaxservice } from '../../../../ajaxservice';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
// Modal box
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// Alert message 
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'; // To enable routing for this component
declare var require: any
const FileSaver = require('file-saver');

// import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-ongoingtickets',
  templateUrl: './ongoingtickets.component.html',
  styleUrls: ['./ongoingtickets.component.scss']
})
export class OngoingticketsComponent implements OnInit {
  //------------------------------------Variable Declarations Starts----------------------------------------//
  //url navigations
  asideMenus: any;
  current_url: any;
  exist: any;
  // Formgroup 
  ProductLocation: FormGroup;
  CustomerfeedbackForm: FormGroup;
  //local storage 
  employee_code: any;       //store employee code
  // Filter variables
  product_id: any;
  location_id: any;
  //To store the data from master
  working_in_progress_tckts = [];          // work in progress
  esclated_tickets: any;                   //escalated Ticket
  spare_requested_ticket: any;             //spare requested Ticket
  spare_details: any;                      //Spare details
  ticket_details: any;                     //Ticket details for view
  technicians_details: any                 //Technician details for view
  reassigned_technician: any;              //Reassigned Technician details
  //Filter
  products: any;                           //Product details
  locations: any;                          //Location details
  subproduct: any;                       //  subproduct 
  //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  currentJustify = 'end';
  //Table 
  showTable: boolean = false;                    //work in progress
  showTableEscalate: boolean = false;            //Escalated
  showTableRequestSpare: boolean = false;        //Requested Spare details
  showTablespareDetails: boolean = false;        //Spare details View
  showTechTable: boolean = false;               //Technician Table
  tabvalue: any;             //Tab id
  ticket_id: any;             //store the ticket id
  pdf: any;
  servicedesk_id: any;
  loadingSub: boolean;
  //------------------------------------Variable Declarations ends----------------------------------------//
  constructor(private ajax: ajaxservice, private overlay: Overlay, public modalService: NgbModal, private toastr: ToastrService, private chRef: ChangeDetectorRef, private router: Router) {
    //Formgroup Initialize
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required)
    })
    this.CustomerfeedbackForm = new FormGroup({
      "ticket_id": new FormControl(''),
      "customer_code": new FormControl(''),
      "customer_name": new FormControl(''),
      "contact_number": new FormControl(''),
      "email_id": new FormControl(''),
      'cust_comments': new FormControl('', Validators.required),
      // 'cust_feedback': new FormControl('', Validators.required),
      // 'cust_rating': new FormControl('', Validators.required),

    })
  }
  ngOnInit() {
    this.asideMenus = localStorage.getItem('asideMenus');
    console.log(typeof (JSON.parse(this.asideMenus)));
    console.log(this.router.url);
    this.current_url = this.router.url;
    const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    console.log(result.length)
    if (result.length <= 0) {
      // alert("u cannpot go")
      localStorage.clear();
      this.router.navigate(["/login"]);
    } else {
      // alert("else")
      this.employee_code = localStorage.getItem('employee_code'); //local storage
      //loader
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);  //loader
      this.product_id = null                        //initialize the product id as null
      this.location_id = null                       //initialize the product id as null
      this.tabvalue = 'tab-selectbyid1'              //initialize the tabvalue with first tab id
      this.get_working_in_progress();         //call work in progress api
      this.get_ticket_product_location();      //call the function to get product and location
    }
    this.servicedesk_id = localStorage.getItem('employee_code')
    console.log(this.servicedesk_id, "this.servicedesk_id")
  }
  downloadPdf(item) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var id = "ticket_id";
    var url = "setr_pdf/?";
    var value = item;
    this.ajax.getPdfDocument(url, id,value).subscribe((response) => {
      console.log(response, "response")
      let file = response.url
      console.log(response.url, "fileURL")    
      FileSaver.saveAs(file, value);
      this.overlayRef.detach();

    }, (err) => {

      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });
   
  }
    //Customer feedback 
    customer_feedback() {
      this.loadingSub = true;
      var data = this.CustomerfeedbackForm.value;
      data['flag'] = 1;
      console.log(data)
      var url = 'customer_feedback/' //api url of edit api
      this.ajax.postdata(url, data)
        .subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") //if sucess
          {
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            // this.overlayRef.attach(this.LoaderComponentPortal);
            this.loadingSub = false;
            this.get_working_in_progress();
            this.showTable = false;
            this.chRef.detectChanges();
            this.modalService.dismissAll(); //to close the modal box
          }
          else if (result.response.response_code == "500") { //if failiure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
            this.loadingSub = false;
          }
          else { //if not sucess
            this.toastr.error("Something went wrong", 'Error'); //toastr message for error
            this.loadingSub = false;
          }
        }, (err) => {
          //prints if it encounters an error
        });
    }
    get feed() {
      return this.CustomerfeedbackForm.controls;                       //error logs for create user
    }
  customer_feedback_modal(modal, item) {
    //set the value to the customerfeedback formgroup
    this.CustomerfeedbackForm.patchValue({
      "ticket_id": item.ticket_id,
      "customer_code": item.customer_code,
      "customer_name": item.customer_name,
      "contact_number": item.contact_number,
      "email_id": item.email_id,
    })
    //open modal box
    this.modalService.open(modal, {
      size: 'lg',
      windowClass: "center-modalsm",
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  //-------------------------------------- getters for formgroup start-------------------------------------//
  get log() {
    return this.ProductLocation.controls;                       //error logs for productlocation 
  }
  //-------------------------------------- getters for formgroup end-------------------------------------//


  // ------------------------------------Filter Starts-------------------------------------------------//
  //product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);  //prints if it encounters an error
    });
  }
  //filter
  filter_tickets() {
    this.location_id = this.ProductLocation.value.location_id;
    this.product_id = this.ProductLocation.value.product_id;
    if (this.location_id == "") {
      this.location_id = null;
    }
    else {
      this.location_id = this.ProductLocation.value.location_id;
    }
    if (this.product_id == "") {
      this.product_id = null;
    }
    else {
      this.product_id = this.ProductLocation.value.product_id;
    }
    var tab_data = { "nextId": this.tabvalue }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTable = false;
      this.get_working_in_progress();       //call the work in progress api
    }
    else if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableEscalate = false;
      this.get_esclated_tickets();            //call the escalated api
    }
  }
  // ------------------------------------Filter Ends-------------------------------------------------//

  // ----------------------------------------- Table View Starts ----------------------------------------------//
  //work in progress ticket based on the product and location  and employee code
  get_working_in_progress() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.working_in_progress_tckts=[];
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    var url = 'working_in_progress/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // this.working_in_progress_tckts = result.response.data;              //storing the api response in the array
        result.response.data.forEach(element => {
          var date= element.schedule_next.split(",",2);
          element.schedule_next=date[0];
          this.working_in_progress_tckts.push(element);
         })
        this.showTable = true;
        this.chRef.detectChanges();                      //detech changes
        this.overlayRef.detach();                        //detach loader
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);  //prints if it encounters an error
    });
  }
  //e escalated ticket based on the product and location  and employee code
  get_esclated_tickets() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    var url = 'esclated_tickets/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.esclated_tickets = result.response.data;              //storing the api response in the array
        this.showTableEscalate = true;
        this.chRef.detectChanges();                         //detect changes
        this.overlayRef.detach();                           //detach loader
      }
      else if (result.response.response_code == "500") {
        this.showTableEscalate = true;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      this.overlayRef.detach();                           //detach loader
      console.log(err);  //prints if it encounters an error
    });
  }
  //requested tiket based on employee code
  get_spare_requested_ticket() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var data = this.employee_code
    var url = 'sapre_request_tickets/?';             // api url for getting the details with using post params
    var employee_code = "employee_code";
    this.ajax.getdataparam(url, employee_code, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.spare_requested_ticket = result.response.data;   //storing the api response in the array                                    //reloading the component
        this.showTableRequestSpare = true;
        this.chRef.detectChanges();                       //detect changes
        this.overlayRef.detach();                         //detach loader
      }
      else if (result.response.response_code == "500") {
        this.showTableRequestSpare = true;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);  //prints if it encounters an error
    });
  }
  // ----------------------------------------- Table View Ends ----------------------------------------------//

  //----------------------------------------- Functions for view Starts------------------------------------//
  //ticket details based on TicketId 
  get_ticket_details(tickt_id, view_tcktdetails) {
    var ticket_id = tickt_id; // Ticket id: number                                    
    var url = 'get_ticket_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details = result.response.data;  //storing the api response in the array
        this.modalService.open(view_tcktdetails, {
          size: 'lg',
          // windowClass: "center-modalsm",
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);   //prints if it encounters an error
    });
  }
  //Technician details based on TechnicianId 
  get_technician_details(ticket_id, technician_id, view_techniciandetails) {
    var ticket_id = ticket_id
    var technician_id = technician_id
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;    //storing the api response in the array
        this.modalService.open(view_techniciandetails, {
          size: 'lg',
          windowClass: "center-modalsm",
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);  //prints if it encounters an error
    });
  }
  // Spare details
  get_spare_details(ticket_id, view_requested_spare) {
    var data = ticket_id
    var url = 'spare_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    this.ajax.getdataparam(url, id, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.spare_details = result.response.data;  //storing the api response in the array
        this.showTablespareDetails = true;
        this.modalService.open(view_requested_spare, {
          size: 'lg',
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);  //prints if it encounters an error
    });
  }
  //----------------------------------------- Functions for view Ends------------------------------------//

  //-------------------------------------Assign the ticket to technician starts--------------------------------//
  //reassign the tickets
  re_assign_technician(ticket_id, TechnicianTable) {
    var ticket_id = ticket_id; //ticket id                                   
    var url = 're_assign_technician/?';             // api url for getting the details with using post params
    var data = { "ticket_id": ticket_id, "employee_code": this.employee_code }
    this.ajax.postdata(url, data)
      .subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.reassigned_technician = result.response.data;

        this.showTechTable = true;                                        //reloading the component
        this.ticket_id = ticket_id;
        this.modalService.open(TechnicianTable, {
          size: 'lg',
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);                                     //prints if it encounters an error
    });
  }
  //available technicians
  technician_assign(id_details) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to assign",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Assign!',
      allowOutsideClick: false,         //alert outside click by Sowndarya
    }).then((result) => {
      if (result.value) {
        var data = {"servicedesk_id": this.servicedesk_id, "technician_id": id_details.technician_id, "ticket_id": this.ticket_id, "secondary_tec_id": null }
        var url = 'technician_assign/'                                           //api url of Assign Ticket                                    
        this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200")                           //if sucess
            {
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
              this.showTechTable = false;
              this.chRef.detectChanges();
              var tab_data = { "nextId": this.tabvalue }
              this.findtab(tab_data);                                                //reloading the component
              this.modalService.dismissAll();                                 //to close the modal box
            }
            else if (result.response.response_code == "400") {                  //if failure
              this.toastr.error(result.response.message, 'Error');            //toastr message for error
            }
            else if (result.response.response_code == "500") {
              this.toastr.error(result.response.message, 'Error');        //toastr message for error
            }
            else {                                                                     //if not sucess
              this.toastr.error("Something went wrong", 'Error');        //toastr message for error
            }
          }, (err) => {                                                           //if error
            console.log(err);   //prints if it encounters an error
          });
      }
    })
  }
  //-------------------------------------Assign the ticket to technician Ends--------------------------------//

  findtab(evt: any) {
    this.tabvalue = evt.nextId;
    this.location_id = null;
    this.product_id = null;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['location_id'].setValue('');
    if (evt.nextId == 'tab-selectbyid1') {
      this.showTable = false;
      this.chRef.detectChanges();
      this.get_working_in_progress();     // call work in progress api
    }
    if (evt.nextId == 'tab-selectbyid2') {
      this.showTableEscalate = false
      this.chRef.detectChanges();
      this.get_esclated_tickets();              // call escalated ticket api
    }
    if (evt.nextId == 'tab-selectbyid3') {
      this.showTableRequestSpare = false;
      this.chRef.detectChanges();
      this.get_spare_requested_ticket();            //call spare request ticket api 
    }
  }



}
