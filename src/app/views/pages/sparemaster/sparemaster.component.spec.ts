import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparemasterComponent } from './sparemaster.component';

describe('SparemasterComponent', () => {
  let component: SparemasterComponent;
  let fixture: ComponentFixture<SparemasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparemasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparemasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
