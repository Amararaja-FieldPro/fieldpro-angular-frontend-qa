import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsiComponent } from './csi.component';

describe('CsiComponent', () => {
  let component: CsiComponent;
  let fixture: ComponentFixture<CsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
