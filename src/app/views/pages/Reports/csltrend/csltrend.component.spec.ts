import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CSLTrendComponent } from './csltrend.component';

describe('CSLTrendComponent', () => {
  let component: CSLTrendComponent;
  let fixture: ComponentFixture<CSLTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSLTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSLTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
