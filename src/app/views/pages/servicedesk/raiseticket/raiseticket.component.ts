import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbDateStruct, NgbDate, NgbCalendar, NgbTabset } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl, SelectControlValueAccessor } from '@angular/forms';
import { Location } from '@angular/common';
import { NoopScrollStrategy, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { MatRadioChange } from '@angular/material'; //for matradio button
import * as _ from 'lodash';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ExcelService } from '../../../../excelservice';
declare var require: any
const FileSaver = require('file-saver');
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-raiseticket',
  templateUrl: './raiseticket.component.html',
  styleUrls: ['./raiseticket.component.scss']
})
export class RaiseticketComponent implements OnInit {
  //url navigations
  [x: string]: any;
  segment_value: any;
  application_value: any;
  httplink: string;
  labelwarranty: string;
  ticketexcel: any;
  customerexcel: any;
  productdata: any = [];
  asideMenus: any;
  current_url: any;
  selectedCountry: any;
  states: any; //variable to store the state
  cus_states: any;
  cities: any; //variable to store the city
  country: any;
  locations: any; //variable to store the country
  RaiseTicket: FormGroup; //FormGroup variable for get raise ticket
  NewCustomer: FormGroup; //FormGroup variable for creating new customer
  RaiseTicketCusInfo: FormGroup;
  AddSiteInfo: FormGroup;
  AddProductInfo: FormGroup;
  RaiseTicketProdInfo: FormGroup;
  NewCustomerInfo: FormGroup;
  NewCustomerProdInfo: FormGroup;
  Addnameinfo: FormGroup;
  Addnamesiteinfo: FormGroup;
  Addbatterybank: FormGroup;
  Addnameprodinfo: FormGroup;
  RaiseCall: FormGroup;
  AddIdinfo: FormGroup;
  customer_details: any; //variable to store the customer data getting from the api call
  contract_details: any; //variable to store the contract data getting from the api call
  contract_detail: any;
  customer_code: any; //variable to get the customer code and pass it to get the customer details
  contact_number: any; //variable to get the contact number and pass it to get the customer details
  product: any;  //to store all the products
  subproduct: any;     //to store the subproduct
  work_type: any;             //store the work type
  call_category: any;            //store the call category
  setPriority: any;
  // selectedfile = true;
  image: any;
  mfg3: any;
  cropped_img = true;
  // contract_id: any;
  raiseticket_details: any;            //store the raise tickst details
  contrac_type: any;              //store the contract details
  employee_id: any;
  contract_data: any
  isShow: any;
  submitted = false;
  contact_no: any;
  model: NgbDateStruct;
  models: NgbDateStruct;
  activeId: any;
  preId: any;
  activerId: any;
  prerId: any;
  nextrId: any;
  prefId: any;
  evt: any;
  form: any;
  params: any;
  emp_error: any;
  email_error: any;
  validation_data: any;
  contactnum_error: any;
  serial_validation: any;
  prioritys = [];
  duration_month: any;
  contact_setvalue: any;
  email_setvalue: any;
  overlayRef: OverlayRef;
  radiochange_value: any;
  image_validation: any;
  firstDate: any;
  secondDate: any;
  expiry_day: any;
  AddmoreBtnDisable: boolean = false;
  battery_bank: any = [];
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  // @ViewChild(DataTableDirective, { static: false })
  // dtElement: DataTableDirective;
  // dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject<any> = new Subject();
  showTable: boolean = false;
  duration_values: any;
  multiple_products_error: string;
  subproducts: any;
  submit_error_msg: string;
  count_value: any;
  employee_code: any;
  success_msg: any;
  multi_contract_details: any;
  image_name: any;
  ByDefault: boolean = false;
  allowed_types = "['image/png', 'image/jpeg', 'image/jpg']";
  error: string;
  serial_error: string;
  raise_error: any;
  customer_error: any;
  con_number: any;
  remove_button_hide: boolean = true;
  show_cancel_button: boolean = false;
  show_field: boolean = false;
  alternate: string;
  contact: string;
  loadingSubmit: boolean = false;
  loadingExport: boolean = false;
  buttonmainedit: boolean = false;
  maxChars: number;
  role: any;
  chars: number;
  cardImageBase64: string;
  isImageSaved: boolean;
  imageError: any;
  customer_names: any;
  sitedetails: any;
  customerSelect: any;
  siteSelect: any = [];
  contactNumber: any;
  contactSelect: any;
  customerID: any;
  application: any;
  segment: any;
  model_id: any;
  warranty_id: any;
  call: any;
  site_details: any;
  batteryData: any;
  add: boolean;
  serial_numsber: any;
  batteryBankArray: any = [];
  battery_data: string;
  battery_datas: string;
  raise_data: string;
  products_id: number;
  subid: any;
  save_continue: boolean;
  cus_id: any;
  message: string;
  code: any;
  customer_info: any;
  BatteryBankId: any;
  ExpiryDate: any;
  VDate: { day: number; month: number; year: number; };
  validwarranty: string;
  productdatas: any;
  expiry_flag: string = "1";
  serialSaveContinue: any = [];
  serial: any = [];
  batteryBank_date: any;
  bbSelect: any;
  serialProduct: any = [];
  cus_name: any;
  cus_cus_id: any;
  cus_siteId: any;
  batteryBankSample: any = [];
  serialNumbersdata: any;
  flag: any;
  customer_save_continue: boolean;
  mfg_date: any;
  numberSelect: any;
  battery_bank_id: any;
  site_id: any;
  type: string;
  title: string;
  warranty_type: any;
  value: number;
  set: number;
  role_id: any;
  logged_customer_code: any;
  products: any;
  to: NgbDate;
  select: any[];
  name: any[];
  customer_type: string;
  customer_type_id: any;
  customer_code_data: any;
  AllSerial: any = [];
  value_disabled: boolean = false;
  name_disabled: boolean = false;
  customer_name_ref: any[];
  warranty_type_flag: any;
  radio_val: number = 0;
  cus_type: any;
  oem_customers: any;
  customerdetail: any;
  // bulk: any[];
  loadingUpload: boolean;
  bulks: any;
  bulk: any;
  file_name: any;
  customer_state: any = [];
  loadingSub: boolean = false;
  placeholder: string;
  // isLoading:boolean;
  // message: any;

  //----------------trails---------------------//

  constructor(private excelservice: ExcelService, private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private overlay: Overlay,
    private toastr: ToastrService, public _location: Location, private calendar: NgbCalendar, public tabset: NgbTabset,
    private chRef: ChangeDetectorRef, private formBuilder: FormBuilder, private config: NgbDatepickerConfig,) {
    this.model = this.calendar.getToday();
    this.models = this.calendar.getToday();
    this.RaiseTicketCusInfo = new FormGroup({
      //------------------------formcontrol for customer details--------------------------------------//
      "lable_type": new FormControl(""),
      "cus_site_id": new FormControl(""),
      "customer_type": new FormControl("", [Validators.required]),
      "customer_state": new FormControl(""),
      "employee_id": new FormControl(this.employee_id),
      // "ticket_id": new FormControl(""),
      "customer_id": new FormControl("", [Validators.required]),
      "customer_code": new FormControl("", [Validators.required]),
      "battery_bank_id": new FormControl("", [Validators.required]),
      "address": new FormControl(""),
      "customer_name": new FormControl('', [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      // "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),

      "alternate_contact_number": new FormControl('', [Validators.required]),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_id": new FormControl(""),
      "contact_person_name": new FormControl(""),
      "contact_person_number": new FormControl(""),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(''),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "cus_state": new FormControl(''),
      "cus_city": new FormControl(''),
      "cus_town": new FormControl(''),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
    });
    this.RaiseTicketCusInfo.controls['customer_type'].setErrors({ 'incorrect': true });
    this.RaiseTicketCusInfo.controls['customer_type'].setValue(null);
    this.RaiseTicketCusInfo.controls['customer_state'].setValue(null);
    this.AddIdinfo = new FormGroup({
      "customer_id": new FormControl("", [Validators.required]),
      "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl([Validators.required]),
      "sap_reference_number": new FormControl(""),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_id": new FormControl(""),
      "site_contact_person_name": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_contact_person_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(''),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required])
    });
    this.Addnameinfo = new FormGroup({
      "employee_code": new FormControl(this.employee_code),
      "customer_type": new FormControl(""),
      "customer_code": new FormControl(""),
      "customer_name": new FormControl('', [Validators.required]),
      "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl([Validators.required]),
      "sap_reference_number": new FormControl(""),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl('', [Validators.required]),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl(''),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
      "site_id": new FormControl(""),
    })
    this.Addnameprodinfo = new FormGroup({

      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model),
      "proioritys": new FormControl(""),
      "call_category_id": new FormControl(""),
      "work_type_id": new FormControl(""),
      "desc": new FormControl("")
    });
    this.Addnameprodinfo.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_id'].setValue(null);
    this.Addnameprodinfo.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_sub_id'].setValue(null);
    this.Addnameprodinfo.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['model_no'].setValue(null);
    this.Addnameprodinfo.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['warranty_type_id'].setValue(null);
    this.Addnameprodinfo.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['segment_id'].setValue(null);
    this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['application_id'].setValue(null);
    this.Addnameprodinfo.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['work_type_id'].setValue(null);
    this.Addnameprodinfo.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['call_category_id'].setValue(null);
    this.Addnameprodinfo.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['proioritys'].setValue(null);
    this.Addnamesiteinfo = new FormGroup({
      "employee_code": new FormControl(this.employee_code),
      "site_id": new FormControl(""),
      "customer_code": new FormControl(""),
      "customer_id": new FormControl(""),
      "customer_name": new FormControl('', [Validators.required, this.noWhitespace]),
      "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl("", [Validators.required]),
      "sap_reference_number": new FormControl(""),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(''),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),

    })
    this.Addbatterybank = new FormGroup({
      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model, [Validators.required]),
      "proioritys": new FormControl("", [Validators.required]),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl(''),
      "desc": new FormControl("", [Validators.required])
    });
    this.Addbatterybank.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_id'].setValue(null);
    this.Addbatterybank.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_sub_id'].setValue(null);
    this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['model_no'].setValue(null);
    this.Addbatterybank.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['warranty_type_id'].setValue(null);
    this.Addbatterybank.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['segment_id'].setValue(null);
    this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['application_id'].setValue(null);
    this.Addbatterybank.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['work_type_id'].setValue(null);
    this.Addbatterybank.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['call_category_id'].setValue(null);
    this.Addbatterybank.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['proioritys'].setValue(null);
    this.RaiseTicketProdInfo = new FormGroup({
      //------------------------formcontrol for contract details--------------------------------------//
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required]),
      "serial_no": new FormControl("", [Validators.required]),
      "contract_type": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl("", [Validators.required]),
      "cust_preference_date": new FormControl(this.model),
      "call_category_id": new FormControl("", [Validators.required]),
      "problem_desc": new FormControl("", [Validators.required, Validators.maxLength(250), this.noWhitespace]),
      "image": new FormControl(''),
      "priority": new FormControl("", [Validators.required]),
    });
    this.AddSiteInfo = new FormGroup({
      "employee_code": new FormControl(this.employee_code),
      "customer_name": new FormControl("", [Validators.required]),
      "customer_code": new FormControl(""),
      "contact_number": new FormControl("", [Validators.required]),
      "alternate_number": new FormControl("", [Validators.required]),
      "email_id": new FormControl(""),
      "sap_reference_number": new FormControl(""),
      "site_id": new FormControl(""),
      "contact_person_number": new FormControl("", [Validators.required]),
      "contact_person_name": new FormControl("", [Validators.required]),// product_sub_id
      "plot_number": new FormControl("", Validators.pattern("^[0-9%+-/a-z A-Z]{2,32}$")),
      "street": new FormControl("", [Validators.required]),
      "landmark": new FormControl(""),
      "country_id": new FormControl("", [Validators.required]),
      "state_id": new FormControl("", [Validators.required]),
      "city_id": new FormControl("", [Validators.required,]),
      "location_id": new FormControl('', [Validators.required,]),
      "post_code": new FormControl("", [Validators.required]),
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required]),
      "serial_no": new FormControl("", [Validators.required, this.noWhitespace]),
      "purchase_date": new FormControl(this.model),
      "sys_ah": new FormControl("", [Validators.required]),
      "warranty_type_id": new FormControl(""),
      "segment_id": new FormControl("", [Validators.required]),
      "application_id": new FormControl("", [Validators.required,]),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(""),
      "contract_duration": new FormControl(""),
      "invoice_date": new FormControl(""),
      "expiry_day": new FormControl(""),
      "mfg_date": new FormControl(""),
      "battery_bank_id": new FormControl(""),
      "voltage": new FormControl(""),
    });
    this.RaiseCall = new FormGroup({
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model),
      "proioritys": new FormControl('', [Validators.required]),
      "call_category_id": new FormControl('', [Validators.required]),
      "work_type_id": new FormControl(""),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])
    }),
      this.AddProductInfo = new FormGroup({
        "product": new FormControl("", [Validators.required]),
        "product_sub": new FormControl("", [Validators.required]),// product_sub_id
        "model": new FormControl("", [Validators.required]),
        "sys_ah": new FormControl("", [Validators.required]),
        "serial_no": new FormControl("", [Validators.required]),
        "warranty_type": new FormControl("", [Validators.required]),
        "warranty_duration": new FormControl("", [Validators.required]),
        "segment": new FormControl("", [Validators.required]),
        "application": new FormControl("", [Validators.required,]),
        "organization_name": new FormControl(''),
        "invoice_number": new FormControl("", [Validators.required]),
        "invoice_date": new FormControl("", [Validators.required]),

      })



    //--------------------------Formgroup for creating new customer-----------------------------------//
    this.NewCustomer = new FormGroup({
      //------------------------formcontrol for customer details--------------------------------------//
      "customer_code": new FormControl("", [Validators.required]),
      "customer_name": new FormControl('', [Validators.required, this.noWhitespace]),
      // "employee_id": new FormControl("", [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl([Validators.required]),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[0-9%+-/a-z0-9]{2,5}$"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(""),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$"), this.noWhitespace]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
      //------------------------formcontrol for contract details--------------------------------------//
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required, this.noWhitespace]),
      "serial_no": new FormControl("", [Validators.required, this.noWhitespace]),
      "purchase_date": new FormControl(this.model),
      // "contract_type":new FormControl("", [Validators.required]),
      "amc_id": new FormControl("", [Validators.required]),
      "duration": new FormControl("", [Validators.required]),
      "contract_data": new FormControl("")
    });
    this.NewCustomerInfo = new FormGroup({
      //------------------------formcontrol for customer details--------------------------------------//
      "customer_code": new FormControl(""),
      "customer_name": new FormControl('', [Validators.required, this.noWhitespace]),
      // "employee_id": new FormControl("", [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl([Validators.required]),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_id": new FormControl(""),
      "contact_person_name": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(""),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$"), this.noWhitespace]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required])
    });
    this.NewCustomerProdInfo = this.formBuilder.group({
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required, this.noWhitespace]),
      "serial_no": new FormControl("", [Validators.required, Validators.pattern("[a-zA-Z0-9\s]+"), this.noWhitespace]),
      "purchase_date": new FormControl(this.model, [Validators.required]),
      "amc_id": new FormControl("", [Validators.required]),
      "duration": new FormControl("", [Validators.required]),
      "contract_data": new FormArray([])
    });
    // Validators.pattern("/^([0-9]|[a-z])+([0-9a-z]+)$/i")

    //----------------trails 
    //  this.NewCustomerProdInfo = this.formBuilder.group({
    //   // product_details: [1, Validators.required],
    //   contract_data: new FormArray([])
    // });
  }

  get_customer_type(customer_type_id) {
    console.log(this.RaiseTicketCusInfo.value.customer_state, "dfdsf")
    this.name_disabled = false
    console.log(customer_type_id)
    this.customer_names = []; //empty the array while changing the customer type
    this.customer_name_ref = [];
    this.customer_type_id = customer_type_id
    this.Addnameinfo.controls['customer_type'].setValue(this.customer_type_id)

    if (this.customer_state == undefined || this.customer_state == '' || this.customer_state == null) {
      this.toastr.warning("Please select the customer state", "Warning");
    }
    else {
     
      this.customerName(this.customer_type_id)
    }
    if (customer_type_id == 1) {
      this.get_oem_customer();
    }
    // if (this.customer_type_id != 1) {
    //   this.Addnameinfo.reset();
    //   this.customerName(this.customer_type_id)
    //   this.Addnameinfo.controls['customer_type'].setValue(this.customer_type_id)
    // }
    // else if (this.customer_type_id == 1) {
    //   this.name_disabled = true
    //   this.get_oem_customer()

    // }
  }
  fileUpload(event) {
    this.ByDefault = true;
    this.error = "";
    this.loadingUpload = false;
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
  };
  customerfilesubmit(bulkupload) {
    this.bulk = '';
    this.bulks = '';

    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.bulk = '';
      this.bulks = '';
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.bulk = '';
        this.bulks = '';
        this.error = "Only Excel File will be allowed ( xlsx )"
      }
      else if (validExts[0] = this.type) {
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();
        formData.append('excel_file', file);
        var method = "post";
        var url = 'cus_ticket_bulkupload/'
        var i: number;
        this.loadingUpload = true;
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
            this.loadingUpload = false;
            // var a = [];
            // alert('200');// this.file_name = undefined;             //refresh the file
            // this.Filename = '';                     //refersh the file name
            // if (data['success'].length > 0) {
            //   for (i = 0; i < data['success'].length; i++) {
            //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
            //     this.loadingUpload = false;
            //     // this.bulk = '';
            //     // this.modalService.dismissAll();                                //to close the modal box       
            //   }
            if (data['success'].length > 0) {
              // alert("success")
              this.loadingUpload = false;                    //sucess response
              for (i = 0; i < data['success'].length; i++) {
                // this.bulk = '';
                // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                var a = [];
                data['success'].forEach(value => {
                  a.push(value.success_message
                  );
                });
                this.bulks = a;
                this.loadingUpload = false;
                this.showTable = true;
                this.chRef.detectChanges();
                this.get_unassigned_tickets();                                //to close the modal box
              }
            }


            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );
                });
                this.bulk = a;

              }
            }
          }
          else if (data['response'].response_code == "500") {
            this.bulk = '';
            this.bulks = '';
            console.log(data['response'].message)
            this.error = data['response'].message
            this.loadingUpload = false;
          }
          else {
            this.bulk = '';
            this.bulks = '';
            this.error = "Something went wrong."      //toastr message for error
            this.loadingUpload = false;
          }
        },
          (err) => {
            this.bulk = '';
            this.bulks = '';
            this.error = "Something went wrong"          //prints if it encounters an error
            this.loadingUpload = false;
          });

      }
    }
  }
  filesubmit(bulkupload) {
    this.bulk = '';
    // this.loadingUpload = true;
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.bulk = '';
      this.bulks = '';
    }
    else if (this.file_name.length > 0) {
      // if (validExts[0] != this.type) {
      //   this.bulk = '';
      //   this.bulks = '';
      //   this.error = "Only Excel File will be allowed ( xlsx )"
      // }
      // else if (validExts[0] = this.type) {
      let file: File = this.file_name[0];
      const formData: FormData = new FormData();
      formData.append('excel_file', file);
      var method = "post";
      var url = 'ticket_bulk_upload/'
      var i: number;
      this.loadingUpload = true;
      this.ajax.ajaxpost(formData, method, url).subscribe(data => {
        if (data['response_code'] == "200") {
          this.error = '';
          this.loadingUpload = false;

          // this.file_name = undefined;             //refresh the file
          // this.Filename = '';                     //refersh the file name
          // if (data['success'].length > 0) {
          //   for (i = 0; i < data['success'].length; i++) {
          //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
          //     this.loadingUpload = false;
          //     // this.bulk = '';
          //     // this.modalService.dismissAll();                                //to close the modal box       
          //   }
          if (data['success'].length > 0) {
            this.loadingUpload = false;                    //sucess response
            for (i = 0; i < data['success'].length; i++) {
              // this.bulk = '';
              // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
              var a = [];
              data['success'].forEach(value => {
                a.push(value.success_message
                );
              });
              this.bulks = a;
              this.loadingUpload = false;
              this.showTable = true;
              this.chRef.detectChanges();
              // this.get_unassigned_tickets();                                //to close the modal box
            }
          }


          if (data['response'].fail) {
            if (data['response'].fail.length > 0) {                                    //failure response 
              var a = [];
              data['response'].fail.forEach(value => {
                a.push(value.error_message
                );
              });
              this.bulk = a;

            }
          }
        }
        else if (data['response'].response_code == "500") {
          this.bulk = '';
          this.bulks = '';
          console.log(data['response'].message)
          this.error = data['response'].message
          this.loadingUpload = false;
        }
        else {
          this.bulk = '';
          this.bulks = '';
          this.error = "Something went wrong."      //toastr message for error
          this.loadingUpload = false;
        }
      },
        (err) => {
          this.bulk = '';
          this.bulks = '';
          this.error = "Something went wrong"          //prints if it encounters an error
          this.loadingUpload = false;
        });

    }
    // }
  };
  getdetails() {
    var url = 'get_all_customer_contract/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.customerdetail = result.response.data;
        this.excelservice.exportAsExcelFile(this.customerdetail, 'customer');
        this.loadingExport = false;
      }
    }, (err) => {
      this.loadingExport = false;
      console.log(err);                                        //prints if it encounters an error
    });
    // this.loadingExport=false;
  }
  // Excel download
  exportAsXLSX(): void {
    this.loadingExport = true;
    this.getdetails();
    // this.excelservice.exportAsExcelFile(this.customerdetail, 'customer');
  }
  openmodal(modal) {
    this.httplink = environment.image_static_ip;
    this.ticketexcel = 'media/ticket_bulk_usermanual.xlsx';
    this.customerexcel = 'media/customer_bulk_usermanual.xlsx';
    this.ByDefault = false;
    this.file_name = undefined;             //refresh the file
    this.Filename = '';                 //refresh the file name
    this.error = '';                      //refresh the error message
    this.bulk = '';                   //refresh the error from api
    this.bulks = '';
    this.modalService.open(modal, {
      size: 'lg',
      // windowClass: "center-modalsm",
    });
  }
  removespacenew(item) {
    console.log(item);
    if (this.Addnameprodinfo) {
      if (this.Addnameprodinfo.value.invoice_number != '') {
        var number = item.trim();
        this.Addnameprodinfo.controls['invoice_number'].setValue(number)
      }
      else {
        this.Addnameprodinfo.controls['invoice_number'].setValue('')
      }
    }
    if (this.AddSiteInfo) {
      if (this.AddSiteInfo.value.invoice_number != '') {
        var number = item.trim();
        this.AddSiteInfo.controls['invoice_number'].setValue(number)
      }
      else {
        this.AddSiteInfo.controls['invoice_number'].setValue('')
      }
    }
    if (this.Addbatterybank) {
      if (this.Addbatterybank.value.invoice_number != '') {
        var number = item.trim();
        this.Addbatterybank.controls['invoice_number'].setValue(number)
      }
      else {
        this.Addbatterybank.controls['invoice_number'].setValue('')
      }
    }
  }
  get log() {
    return this.RaiseTicketCusInfo.controls; //error logs for create user
  }
  get log2() {
    return this.RaiseTicketProdInfo.controls; //error logs for create user
  }
  // convenience getter for easy access to form fields
  get log1() {
    return this.NewCustomerInfo.controls; //error logs for create user
  }
  get sitelog() {
    return this.AddSiteInfo.controls; //error logs for create user
  }
  get namelog() {
    return this.Addnameinfo.controls; //error logs for create user
  }
  get nameprodinfo() {
    return this.Addnameprodinfo.controls
  }
  get batteryinfo() {
    return this.Addbatterybank.controls
  }
  get raise() {
    return this.RaiseCall.controls
  }
  validaddinfo() {
    console.log(this.Addnameinfo.valid)
    console.log(this.Addnameinfo.value)
    console.log(this.Addnameinfo.value)
  }

  addsite(modalques, addsiteid, addcustomername) {
    this.getCus();
    this.message = '';
    this.contactnum_error = '';
    this.email_error = '';
    this.serial_error = '';
    this.add = false;
    this.cus_id = [];
    this.type = '';
    console.log(this.customerSelect)
    if (this.customerSelect == undefined || this.customerSelect == '' || this.customerSelect == null) {
      this.toastr.warning("Please select the customer name", "Warning");
    }
    else {
      this.modalService.open(modalques, {
        size: 'lg', //specifies the size of the dialog box
        windowClass: "center-modalsm",
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });

    }

  }
  addcusid(modalques, addsiteid, addcustomername) {
    this.value_disabled = true;      // made contact and alternate contact number as editable
    this.name_disabled = true;           //made customer name,
    this.cus_type = this.RaiseTicketCusInfo.value.customer_type;
    this.title = "Add Customer";
    this.mfgdatetype = 'Enable';
    console.log('ad');
    this.getcountry();
    this.getproduct();
    this.get_servicegroup();
    this.get_call_category();
    this.getamc();
    // this.serial_error = '';
    this.loadingSubmit = false;
    this.Addnameinfo.reset();
    this.Addnameprodinfo = new FormGroup({

      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model),
      "proioritys": new FormControl("", [Validators.required]),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl(""),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])
    });
    this.Addnameprodinfo.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_id'].setValue(null);
    this.Addnameprodinfo.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_sub_id'].setValue(null);
    this.Addnameprodinfo.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['model_no'].setValue(null);
    this.Addnameprodinfo.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['warranty_type_id'].setValue(null);
    this.Addnameprodinfo.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['segment_id'].setValue(null);
    this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['application_id'].setValue(null);
    this.Addnameprodinfo.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['work_type_id'].setValue(null);
    this.Addnameprodinfo.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['call_category_id'].setValue(null);
    this.Addnameprodinfo.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['proioritys'].setValue(null);
    // this.Addnameprodinfo.controls['call_category_id'].setValue('');
    // this.Addnameprodinfo.controls['desc'].setValue('');
    this.RaiseTicketCusInfo.controls['customer_type'].setValue(this.cus_type)
    this.Addnameinfo.controls['customer_type'].setValue(this.cus_type)
    this.add = false;
    this.activeId = "tab-selectbyid1";
    this.preId = "tab-selectbyid2";
    // this.cus_id = [];
    // this.type = '';
    this.email_error = '';;
    this.contactnum_error = '';
    this.labelwarranty = "Select Warranty type";
    console.log(this.RaiseTicketCusInfo.value.customer_type)
    if (this.RaiseTicketCusInfo.value.customer_type == undefined || this.RaiseTicketCusInfo.value.customer_type == '' || this.RaiseTicketCusInfo.value.customer_type == null || this.RaiseTicketCusInfo.value.customer_type == "null") {
      this.toastr.warning("Please select the customer type", "Warning");
    }
    else {

      var url = 'get_expiry_details';                                  // api url for getting the details
      this.ajax.getdata(url).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.modalService.open(modalques, {
            size: 'lg', //specifies the size of the dialog box
            backdrop: 'static',              //popup outside click by Sowndarya
            keyboard: false
          });
        }
        else if (result.response.response_code == "500") {
          //if not sucess
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error("Something went wrong");
        }

      }, (err) => {

      });


      // this.Addnameinfo.reset();
      this.Addnameinfo.controls["country_id"].setValue(101);
      this.onChangeCountry(101);
      // this.Addnameprodinfo.reset();
      // this.Addnameprodinfo.controls['call_category_id'].setValue('');
      // this.Addnameprodinfo.controls['desc'].setValue('');
      this.getsegment();
      // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
      // this.get_oem_customer();
    }

  }
  get_oem_customer() {
    var data = { "customer_type_id": 1 }
    var url = 'get_oem_customer/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.oem_customers = result.response.data;              //storing the api response in the array
        // this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        // this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      // this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  fileChangeEvents(fileInput: any) {
    this.ByDefault = true;
    this.image_name = fileInput.target.files[0].name;
    if (this.RaiseTicketProdInfo) {
      this.cardImageBase64 = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.RaiseTicketProdInfo.controls['training_thumb_image'].setErrors({ 'incorrect': true });
          this.RaiseTicketProdInfo.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.RaiseTicketProdInfo.controls['image'].setErrors({ 'incorrect': true });
          this.RaiseTicketProdInfo.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.RaiseTicketProdInfo.controls['image'].setErrors({ 'incorrect': true });
              this.RaiseTicketProdInfo.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.RaiseTicketProdInfo.value.invalid = false;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }

    if (this.RaiseCall) {
      this.cardImageBase64 = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.RaiseCall.controls['image'].setErrors({ 'incorrect': true });
          this.RaiseCall.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.RaiseCall.controls['image'].setErrors({ 'incorrect': true });
          this.RaiseCall.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.RaiseCall.controls['image'].setErrors({ 'incorrect': true });
              this.RaiseCall.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.ByDefault = true;
              this.image_name = fileInput.target.files[0].name;
              this.RaiseCall.value.invalid = false;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }

  }



  removeImages() {

    if (this.RaiseTicketProdInfo) {
      this.ByDefault = false;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
    }
    if (this.RaiseCall) {
      this.ByDefault = false;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
    }


  }
  //---------------------------------------------trails start ---------------------------------------//
  // convenience getters for easy access to form fields
  get f() { return this.NewCustomerProdInfo.controls; }
  get t() { return this.f.contract_data as FormArray; }
  multiple_products() {
    if (this.NewCustomerProdInfo.valid) {
      this.create_new_customer(2);
      this.multiple_products_error = "";
      this.t.push(this.formBuilder.group({
        product_id: ["", [Validators.required]],
        product_sub_id: ["", [Validators.required]],// product_sub_id
        model_no: ["", [Validators.required]],
        serial_no: ["", [Validators.required]],
        amc_id: ["", [Validators.required]],
        purchase_date: [this.models, [Validators.required]],
        contract_duration: [""]
      }));
    }
  }
  remove_dynamic_field(i) {
    this.t.removeAt(i);
  }
  remove_contract(index) {
    var i: number;
    for (i = this.multi_contract_details.length; i >= 0; i--) {
      if (index == i) {
        var url = 'delete_customer_contract/';
        var data = { "contract_id": this.multi_contract_details[i].contract_id };
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.remove_dynamic_field(index);           //delete design pattern from design
            this.multi_contract_details.splice(index, 1); //delete data from array
            // this.contract_id = ''
            this.toastr.success(result.response.message, 'Success');        //toastr message for error
          }
          else if (result.response.response_code == "400") {
            this.toastr.success(result.response.message, 'Error');        //toastr message for error
          }
          else if (result.response.response_code == "500") {
            this.toastr.success(result.response.message, 'Error');        //toastr message for error
          }
          else {                                                         //if not sucess
            this.toastr.error("something went wrong", 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    }
  }
  //-----------------------------------------trails end--------------------------------------------------------------------//

  ngOnInit() {
    this.showTable = false;
    this.save_continue = false
    this.add = true;
    this.labelwarranty = "Select Warranty type";
    this.radiochange_value = 5;
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.asideMenus = localStorage.getItem('asideMenus');
    this.role_id = localStorage.getItem('role_id')
    this.logged_customer_code = localStorage.getItem('customer_code')
    this.customer_type = JSON.parse(localStorage.getItem('customer_type'))
    console.log(this.customer_type, "customer_type")
    this.placeholder = "Select customer name"
    // this.customerName('All');
    this.getstate();
    this.onChangeCountry(101);

    // this.current_url = this.router.url;
    // const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    // if (result.length <= 0) {
    //   localStorage.clear();

    //   this.router.navigate(["/login"]);
    // } else {

    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    // this.batteryBank();
    // this.getsegment();
    // this.getcountry();
    // this.getproduct();
    // this.get_servicegroup();
    // this.get_call_category();
    // this.getamc();
    // this.customerName(this.customer_type['ustomer_type_id']);
    // this.customerNumber();
    // this.customerId();
    //this.getdetails();
    this.employee_id = localStorage.getItem('employee_id');
    this.employee_code = localStorage.getItem('employee_code');
    this.prioritys = [
      { priority_id: "P1", priority_name: "P1" },
      { priority_id: "P2", priority_name: "P2" },
      { priority_id: "P3", priority_name: "P3" },
      { priority_id: "P4", priority_name: "P4" },
    ];
    this.getDatetime();
    this.productdata = [];
    this.batteryData = [];
    // this.productdata = [{ "id": "1", "product_category_id": "12", "product_sub_category": "20", "model": "12", "sysah_vol": "2v/200ah", "serial_number": "IG012563", "invoice_number": "", "invoice_date": "", "mfg_date": "", "warranty_type": "", "expiry_date": "" },
    // { "id": "2", "product_category_id": "12", "product_sub_category": "20", "model": "12", "sysah_vol": "2v/200ah", "serial_number": "IG012563", "invoice_number": "", "invoice_date": "", "mfg_date": "", "warranty_type": "", "expiry_date": "" }];
    // console.log(this.productdata);
    // }
  }
  getstate() {
    var employee_code = localStorage.getItem('employee_code')
    var url = 'get_state_manager/?';             // api url for getting the details with using post params
    var id = "employee_code";
    console.log(url, id, employee_code);
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cus_states = result.response.data;

      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  public noWhitespace(control: FormControl) {
    // console.log(control.value)
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  addareference(event) {
    console.log(this.Addnameinfo.value.sap_reference_number)
    var product_model = event.target.value.trim();
    this.Addnameinfo.controls['sap_reference_number'].setValue(product_model)
  }
  addasite(event) {
    console.log(this.Addnameinfo.value.site_id)
    var product_model = event.target.value.trim();
    this.Addnameinfo.controls['site_id'].setValue(product_model)
  }

  addEmailValidation(event) {
    if(this.Addnameinfo.value.email_id == ""){
      console.log(this.Addnameinfo.value)
      this.email_error = '';
      this.Addnameinfo.value.invalid = false;
    }
    else if(this.Addnameinfo.value.email_id != "" || this.Addnameinfo.value.email_id != null || this.Addnameinfo.value.email_id != '' ){
      this.Addnameinfo.value.invalid = true;
    this.params = {
      "email_id": this.Addnameinfo.value.email_id
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.Addnameinfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.Addnameinfo.value.invalid = true;
        this.Addnameinfo.controls['email_id'].setErrors({ 'incorrect': true });

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    }
  }
  addEmailValidationsite() {
    if(this.Addnameinfo.value.email_id == ""){
      this.email_error = '';
      this.Addnameinfo.value.invalid = false;
    }
   else if(this.Addnameinfo.value.email_id != "" || this.Addnameinfo.value.email_id != null || this.Addnameinfo.value.email_id != ''){
    this.Addnameinfo.value.invalid = true;
    this.params = {
      "email_id": this.Addnameinfo.value.email_id
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.Addnameinfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.Addnameinfo.value.invalid = true;
        this.Addnameinfo.controls['email_id'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
   }
  }

  addContactValidation() {
    this.Addnameinfo.value.invalid = true;
    this.params = {
      "contact_number": this.Addnameinfo.value.contact_number
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_error = '';
        this.Addnameinfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_error = result.response.message;
        this.Addnameinfo.value.invalid = true;
        this.Addnameinfo.controls['contact_number'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  addContactValidationbattery() {
    this.Addnamesiteinfo.value.invalid = true;
    this.params = {
      "contact_number": this.Addnamesiteinfo.value.contact_number
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_error = '';
        this.Addnamesiteinfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_error = result.response.message;
        this.Addnamesiteinfo.value.invalid = true;
        this.Addnamesiteinfo.controls['contact_number'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  addContactValidationsite() {
    this.AddSiteInfo.value.invalid = true;
    this.params = {
      "contact_number": this.AddSiteInfo.value.contact_number
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_error = '';
        this.AddSiteInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_error = result.response.message;
        this.Addnameinfo.value.invalid = true;
        this.AddSiteInfo.controls['contact_number'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  addalternatecontact() {
    if (this.NewCustomerInfo.value.contact_number == this.NewCustomerInfo.value.alternate_number) {
      this.alternate = "Contact and alternate contact number should be different.";;
      this.NewCustomerInfo.value.invalid = true;
      this.NewCustomerInfo.controls['alternate_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.alternate = '';
      this.NewCustomerInfo.value.invalid = false;
    }
  }
  addacontact() {
    if (this.Addnameinfo.value.alternate_number == this.Addnameinfo.value.contact_number) {
      this.contact = "Contact and alternate contact number should be different.";;
      this.Addnameinfo.value.invalid = true;
      this.Addnameinfo.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact = '';
      this.Addnameinfo.controls['contact_number'].setValue(this.Addnameinfo.value.contact_number)
      this.Addnameinfo.value.invalid = false;
    }
  }
  addacontactbattery() {
    if (this.Addbatterybank.value.alternate_number == this.Addbatterybank.value.contact_number) {
      this.contact = "Contact and alternate contact number should be different.";;
      this.Addbatterybank.value.invalid = true;
      this.Addbatterybank.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact = '';
      this.Addbatterybank.controls['contact_number'].setValue(this.Addbatterybank.value.contact_number)
      this.Addbatterybank.value.invalid = false;
    }
  }
  addacontactsite() {
    if (this.AddSiteInfo.value.alternate_number == this.AddSiteInfo.value.contact_number) {
      this.contact = "Contact and alternate contact number should be different.";;
      this.AddSiteInfo.value.invalid = true;
      this.AddSiteInfo.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact = '';
      this.AddSiteInfo.controls['contact_number'].setValue(this.AddSiteInfo.value.contact_number)
      this.AddSiteInfo.value.invalid = false;
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  //serial number validation for new customer 
  onChangeMFGSite(event) {
    console.log(event);
    //  this.Addbatterybank.value.mfg_date.valueChanges.subscribe(selectedValue => {
    //   console.log('firstname value changed')
    //   console.log(selectedValue)                              //latest value of firstname
    //   console.log(this.Addbatterybank.get("mfg_date").value)   //latest value of firstname
    // })
    console.log(this.AddSiteInfo.value);
    this.onchangetsiteMFG(this.AddSiteInfo.value.model_no.model_id);
    //  this.getwarranty(this.Addbatterybank.value.model_no.model_id)
  }
  onchangetsiteMFG(id) {
    console.log('onchangetsiteMFG');
    if (this.AddSiteInfo.value.mfg_date) {
      var year = this.AddSiteInfo.value.mfg_date.year;
      var month = this.AddSiteInfo.value.mfg_date.month;
      var day = this.AddSiteInfo.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
    }
    else {
      var mfg_date = ''
    }
    var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
    var url = 'get_warranty_type/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.warranty_id = result.response.data;
        this.warranty_id.forEach(e => {
          console.log(e.expiry_day);
          if (e.expiry_day) {
            var expdates = e.expiry_day.split("-", 3);
            var Eyear: number = +expdates[0];
            var Emonth: number = +expdates[1];
            var Eday: number = +expdates[2];
          }


          const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);

          this.AddSiteInfo.controls["expiry_day"].setValue(EDate);


        })
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  NewCustomerserialnumber_validation() {
    if (this.NewCustomerProdInfo.value.product_id == '' || this.NewCustomerProdInfo.value.product_id == null ||
      this.NewCustomerProdInfo.value.product_sub_id == '' || this.NewCustomerProdInfo.value.product_sub_id == null ||
      this.NewCustomerProdInfo.value.model_no == '' || this.NewCustomerProdInfo.value.model_no == null) {
      var serial_value = '';
      this.NewCustomerProdInfo.controls['serial_no'].setValue(serial_value); //set the base64 in create product image

      this.serial_error = "Please select Product, Sub Product,Model No. Before select Serial no"
    }
    else {
      this.serial_error = ''
      var data = {
        "product_id": this.NewCustomerProdInfo.value.product_id,
        "product_sub_id": this.NewCustomerProdInfo.value.product_sub_id,
        "model_no": this.NewCustomerProdInfo.value.model_no,
        "serial_no": this.NewCustomerProdInfo.value.serial_no
      }
      var url = 'validate_cus_serial_no/'                                         //api url of add
      this.ajax.postdata(url, data)
        .subscribe((data) => {
          if (data.response.response_code == "200") {
            this.serial_validation = '';
          }
          else if (data.response.response_code == "400") {
            this.serial_validation = data.response.message;
          }
          else if (data.response.response_code == "500") {
            this.serial_validation = data.response.message;
          }
          else {
            this.serial_validation = data.response.message;
          }
        }, (err) => {
          console.log(err);                                             //prints if it encounters an error
        });
    }
  }


  customer_address_validation() {
    if (this.NewCustomerInfo.valid && (this.email_error == '' || this.email_error == undefined) && (this.contactnum_error == '' || this.contactnum_error == undefined)) {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
    else {
      this.activeId = "tab-selectbyid1";
      this.preId = "tab-selectbyid2";
    }
  }

  customer_address_validationn() {
    if (this.RaiseTicketCusInfo.valid) {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
      let Dates = new Date();
      console.log(Dates)
      this.RaiseTicketProdInfo.controls['cust_preference_date'].setValue(Dates);
    }
  }
  tabBack(preId) {
    this.activeId = 'tab-selectbyid1';
  }
  tabBack1(preId) {
    this.activeId = 'tab-selectbyid2';
  }
  tabBackR(prerId) {
    this.activerId = this.prerId;
    this.prerId = this.activerId
  }
  tabBackF(prefId) {
    this.activeId = this.prefId
    this.preId = this.activerId
  }
  product_validationn() {
    if (this.RaiseTicketProdInfo.valid && this.image_validation == '') {
      this.activeId = "tab-selectbyid3";
      this.preId = "tab-selectbyid2";
    }
  }
  addbattery(model, type) {
    // console.log(this.bbSelect.battery_bank_id);
    console.log(this.BatteryBankId, "this.BatteryBankId")
    this.promfgdatetype = 'Enable';
    this.set = type;
    this.serial_error = '';
    // this.Addbatterybank.reset();
    this.Addbatterybank = new FormGroup({

      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model),
      "proioritys": new FormControl("", [Validators.required]),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl(""),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])
    });
    this.Addbatterybank.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_id'].setValue(null);
    this.Addbatterybank.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_sub_id'].setValue(null);
    this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['model_no'].setValue(null);
    this.Addbatterybank.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['warranty_type_id'].setValue(null);
    this.Addbatterybank.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['segment_id'].setValue(null);
    this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['application_id'].setValue(null);
    this.Addbatterybank.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['work_type_id'].setValue(null);
    this.Addbatterybank.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['call_category_id'].setValue(null);
    this.Addbatterybank.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['proioritys'].setValue(null);
    this.Addnameprodinfo.reset();
    this.getproduct();
    this.get_call_category();
    this.labelwarranty = "Select Warranty type";
    this.activeId = "tab-selectbyida"
    console.log(this.radiochange_value);
    console.log(type)
    if (this.radiochange_value == 3) {

      if (type == 2) {
        this.get_call_category()
        // this.Addbatterybank.reset();
        this.set = type;
        this.type = 'Enable';
        this.title = "Add Serial Number";
        console.log(this.bbSelect.battery_bank_id);
        this.Addbatterybank.value.battery_bank_id = this.bbSelect.battery_bank_id;
        this.Addbatterybank.controls["battery_bank_id"].setValue(this.bbSelect.battery_bank_id)

        console.log(this.set)
      }
      else if (type == 1) {
        this.get_call_category()

        this.set = type;
        this.type = '';
        this.title = "Add Battery bank id";
        this.promfgdatetype = 'Enable';
        // this.Addbatterybank.reset();
        this.Addbatterybank.controls["battery_bank_id"].setValue('')
        console.log(this.set)
      }
      console.log(this.batteryData);
      var parmas = {};
      if (this.role_id != '7') {
        console.log("not an customer login")
        parmas = { 'battery_bank_id': this.bbSelect.battery_bank_id, "flag": 1 };
      }
      else {
        console.log("customer logged in")
        parmas = { 'customer_code': this.logged_customer_code, 'battery_bank_id': this.bbSelect.battery_bank_id, "flag": 2 };
      }
      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.error = '';
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            // var date = obj.expiry_date.split("-", 3);
            // var year = date[0];
            // var month = date[1];
            // var day = date[2];
            // obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.getsegment();
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.isShow = false;
          this.getapplication(this.site_details.segment_id);
          // this.Addbatterybank.controls["segment_id"].setValue(this.site_details.segment_id)
          // this.Addbatterybank.controls["application_id"].setValue(this.site_details.application_id)
          this.Addnamesiteinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_id,
            'state_id': this.site_details.state.state_id,
            'city_id': this.site_details.city.city_id,
            'location_id': this.site_details.location.location_id,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          });
          this.Addbatterybank.patchValue({
            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,
          })
          console.log(this.Addbatterybank.value)
          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {

          this.overlayRef.detach();
          this.error = result.response.message;
          this.toastr.error(this.error, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong";
          this.toastr.error(this.error, "Error")
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
      console.log(this.Addbatterybank.value, "this.Addbatterybank.value")
    }
    else if (this.radiochange_value == 1) {

      console.log(this.bbSelect);
      console.log(this.RaiseTicketCusInfo.value.contact_number)
      if (type == 2) {
        this.type = 'Enable';
        this.title = "Add Serial Number";
        this.mfgdatetype = 'Enable';
        console.log(this.bbSelect.battery_bank_id);
        console.log(this.BatteryBankId, "this.BatteryBankId")
        this.Addbatterybank.value.battery_bank_id = this.BatteryBankId;
        this.Addbatterybank.controls["battery_bank_id"].setValue(this.BatteryBankId)
      }
      else if (type == 1) {
        this.type = '';
        this.title = "Add Battery bank id";
        this.Addbatterybank.controls["battery_bank_id"].setValue('')
      }
      console.log(this.batteryData);
      var parmas = {};
      parmas = { 'battery_bank_id': this.batteryData[0], "flag": 1 };
      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.error = '';
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            // var date = obj.expiry_date.split("-", 3);
            // var year = date[0];
            // var month = date[1];
            // var day = date[2];
            // obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.getsegment();
          this.getapplication(this.site_details.segment_id);
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.isShow = false;

          this.Addnamesiteinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_id,
            'state_id': this.site_details.state.state_id,
            'city_id': this.site_details.city.city_id,
            'location_id': this.site_details.location.location_id,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
            'contract_type': this.site_details.warranty.warranty_type_id,
          });
          this.Addbatterybank.patchValue({

            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,
          })
          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          // this.toastr.error(this.error, "Error")
          this.overlayRef.detach();
          // this.error = result.response.message
          this.toastr.error(result.response.message, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong";
          this.toastr.error(this.error, "Error");
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }
    else if (this.radiochange_value == 5) {

      if (type == 2) {
        this.type = 'Enable';
        this.title = "Add Serial Number";
        // console.log(this.bbSelect.battery_bank_id);
        this.Addbatterybank.value.battery_bank_id = this.BatteryBankId;
        this.Addbatterybank.controls["battery_bank_id"].setValue(this.BatteryBankId)
      }
      else if (type == 1) {
        this.type = '';
        this.title = "Add Battery bank id";
        this.Addbatterybank.controls["battery_bank_id"].setValue('')
      }
      console.log(this.batteryData);
      var parmas = {};
      parmas = { 'customer_code': this.RaiseTicketCusInfo.value.customer_code, 'site_id': this.RaiseTicketCusInfo.value.site_id, "flag": 1 };
      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.error = '';
          this.site_details = [];
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          console.log(this.site_details)
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            // var date = obj.expiry_date.split("-", 3);
            // var year = date[0];
            // var month = date[1];
            // var day = date[2];
            // obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.getsegment();
          this.getapplication(this.site_details.segment_id);
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.isShow = false;
          this.Addnamesiteinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_id,
            'state_id': this.site_details.state.state_id,
            'city_id': this.site_details.city.city_id,
            'location_id': this.site_details.location.location_id,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          })
          this.Addbatterybank.patchValue({
            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,

          })
          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, "Error");
          // this.toastr.error(this.error, "Error")
          this.overlayRef.detach();
          // this.error = result.response.message
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong"
        }
        (err) => {
          console.log("something went wrong")
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }
    else if (this.radiochange_value == 4) {
      if (type == 2) {
        this.set = type;
        this.type = 'Enable';
        this.title = "Add Serial Number";
        console.log(this.bbSelect.battery_bank_id);
        this.Addbatterybank.value.battery_bank_id = this.BatteryBankId;
        this.Addbatterybank.controls["battery_bank_id"].setValue(this.BatteryBankId)
      }
      else if (type == 1) {
        this.set = type;
        this.type = '';
        this.title = "Add Battery bank id";
        this.Addbatterybank.controls["battery_bank_id"].setValue('')
      }
      console.log(this.batteryData);
      var parmas = {};
      if (this.role_id != '7') {
        console.log("not an customer login")
        parmas = { 'customer_code': this.RaiseTicketCusInfo.value.customer_code, 'site_id': this.RaiseTicketCusInfo.value.site_id, "flag": 1 };
      }
      else {
        console.log("customer logged in")
        parmas = { 'customer_code': this.logged_customer_code, 'site_id': this.RaiseTicketCusInfo.value.site_id, "flag": 2 };
      }
      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.error = '';
          this.site_details = [];
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            // var date = obj.expiry_date.split("-", 3);
            // var year = date[0];
            // var month = date[1];
            // var day = date[2];
            // obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.getsegment();
          this.getapplication(this.site_details.segment_id);
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.isShow = false;
          this.Addnamesiteinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_id,
            'state_id': this.site_details.state.state_id,
            'city_id': this.site_details.city.city_id,
            'location_id': this.site_details.location.location_id,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          })
          this.Addbatterybank.patchValue({
            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,

          })

          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          // this.toastr.error(this.error, "Error")
          this.overlayRef.detach();
          // this.error = result.response.message
          this.toastr.error(result.response.message, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong"
          this.toastr.error(this.error, "Error");
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }

    else if (this.radiochange_value == 2) {
      if (type == 2) {
        this.set = type;
        this.type = 'Enable';
        this.title = "Add Serial Number";
        console.log(this.bbSelect.battery_bank_id);
        this.Addbatterybank.value.battery_bank_id = this.BatteryBankId;
        this.Addbatterybank.controls["battery_bank_id"].setValue(this.BatteryBankId)
      }
      else if (type == 1) {
        this.set = type;
        this.type = '';
        this.title = "Add Battery bank id";
        this.Addbatterybank.controls["battery_bank_id"].setValue('')
      }
      console.log(this.batteryData);
      var parmas = {};
      if (this.role_id != '7') {
        console.log("not an customer login")
        parmas = { 'customer_code': this.customerSelect.customer_code, 'site_id': this.RaiseTicketCusInfo.value.site_id, "flag": 1 };
      }
      else {
        console.log("customer logged in")
        parmas = { 'customer_code': this.logged_customer_code, 'site_id': this.RaiseTicketCusInfo.value.site_id, "flag": 2 };
      }
      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.error = '';
          this.site_details = [];
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            // var date = obj.expiry_date.split("-", 3);
            // var year = date[0];
            // var month = date[1];
            // var day = date[2];
            // obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.getsegment();
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.isShow = false;
          this.getapplication(this.site_details.segment_id);
          this.Addnamesiteinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_id,
            'state_id': this.site_details.state.state_id,
            'city_id': this.site_details.city.city_id,
            'location_id': this.site_details.location.location_id,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          })
          this.Addbatterybank.patchValue({

            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,
            'proioritys': this.site_details.priority
          })
          this.overlayRef.detach();
          console.log(this.site_details.priority);
          console.log(this.Addbatterybank.value, "this.Addbatterybank")
        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.Addnamesiteinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          // this.toastr.error(this.error, "Error")
          this.overlayRef.detach();
          // this.error = result.response.message
          this.toastr.error(result.response.message, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong";
          this.toastr.error(this.error, "Error");
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }
  }

  cusno(model) {
    this.mfgdatetype = 'Enable';
    this.title = "Add Customer Number";
    this.serial_error = '';
    console.log(this.contactSelect.customer_code);
    if (this.contactSelect.customer_code == '' || this.contactSelect.customer_code == null || this.contactSelect.customer_code == undefined) {
      this.toastr.warning("Please select customer number")
    }
    else {
      var parmas = {}
      parmas = { 'customer_code': this.contactSelect.customer_code, 'site_id': '', "flag": 1 };

      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.Addnameinfo.controls["country_id"].setValue(101)
          this.getsegment();
          this.Addnameinfo.patchValue({
            'country_id': 101,
          });
          this.onChangeCountryas(101);
          this.error = '';
          this.site_details = [];
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            var date = obj.expiry_date.split("-", 3);
            var year = date[0];
            var month = date[1];
            var day = date[2];
            obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.isShow = false;
          this.RaiseTicketCusInfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_name,
            'state_id': this.site_details.state.state_name,
            'city_id': this.site_details.city.city_name,
            'location_id': this.site_details.location.location_name,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          })
          if (this.site_details.priority == 'P1') {
            this.setPriority = "P1"
          }
          else {
            this.setPriority = this.site_details.priority
          }
          this.RaiseCall.patchValue({
            'proioritys': this.site_details.priority,
          })
          this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);

          console.log(this.site_details.priority);
          this.Addnameinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_number': this.customer_details.alternate_number,
            'customer_type': this.customer_details.customer_type_id,
            'plot_number': '',
            'battery_bank_id': '',
            'street': '',
            'landmark': '',
            'post_code': '',
            'country_id': 101,
            'state_id': '',
            'city_id': '',
            'location_id': '',
            'site_id': '',
            'contact_person_name': '',
            'contact_person_number': '',
          });
          this.getapplication(this.site_details.segment_id);
          this.Addnameprodinfo.patchValue({
            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,
            'proioritys': this.site_details.priority
          })
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': 101,
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          this.overlayRef.detach();
          // this.error = result.response.message
          this.toastr.error(result.response.message, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong";
          this.toastr.error(this.error, "Error");
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }
  }
  cusid(model) {
    this.title = "Add Customer Id";
    this.mfgdatetype = 'Enable';
    this.serial_error = '';
    console.log(this.cus_type)
    this.RaiseTicketCusInfo.controls['customer_type'].setValue(this.cus_type)
    this.Addnameinfo.controls['customer_type'].setValue(this.cus_type)
    console.log(this.customerSelect);
    if (this.customerSelect == '' || this.customerSelect == null || this.customerSelect == undefined) {
      this.toastr.warning("Please select customer id")
    }
    else {
      var parmas = {}
      parmas = { 'customer_code': this.customerSelect.customer_code, 'site_id': '', "flag": 1 };

      var url = 'get_customer_contract_details/?';
      this.overlayRef.attach(this.LoaderComponentPortal);
      // api url for getting the customer and contract details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.Addnameinfo.controls["country_id"].setValue(101)
          this.getsegment();
          this.Addnameinfo.patchValue({
            'country_id': 101,
          });
          this.onChangeCountryas(101);
          this.error = '';
          this.site_details = [];
          this.customer_details = result.response.customer_details;
          this.site_details = result.response.site_details;
          this.segment_value = this.site_details.segment_id;
          this.application_value = this.site_details.application_id;
          console.log(this.segment_value, "this.segment_value")
          this.chRef.detectChanges();
          this.contact_number = this.customer_details.contact_number;
          this.customer_code = this.customer_details.customer_code;
          this.batteryData = result.response.battery_bank_array;
          if (this.batteryData.length == 1) {
            this.showTable = false;
            this.getSerialNmber(this.batteryData[0]);
          }
          this.productdata = [];
          this.site_details.serial_no_details.forEach(obj => {
            var date = obj.expiry_date.split("-", 3);
            var year = date[0];
            var month = date[1];
            var day = date[2];
            obj.expiry_date = day + "-" + month + "-" + year;
            this.productdata.push(obj);
          })
          console.log(this.productdata);
          if (this.batteryData.length == 1) {
            this.battery_data = 'Enable';
            this.showTable = true;
          }
          this.isShow = false;
          this.RaiseTicketCusInfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.site_details.plot_number,
            'battery_bank_id': this.batteryData,
            'street': this.site_details.street,
            'landmark': this.site_details.landmark,
            'post_code': this.site_details.post_code,
            'country_id': this.site_details.country.country_name,
            'state_id': this.site_details.state.state_name,
            'city_id': this.site_details.city.city_name,
            'location_id': this.site_details.location.location_name,
            'site_id': this.site_details.site_id,
            'contact_person_name': this.site_details.contact_person_name,
            'contact_person_number': this.site_details.contact_person_number,
          })
          if (this.site_details.priority == 'P1') {
            this.setPriority = "P1"
          }
          else {
            this.setPriority = this.site_details.priority
          }
          this.RaiseCall.patchValue({
            'proioritys': this.site_details.priority,
          })
          this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);

          console.log(this.site_details.priority);
          this.Addnameinfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_number': this.customer_details.alternate_number,
            'customer_type': this.customer_details.customer_type_id,
            'plot_number': '',
            'battery_bank_id': '',
            'street': '',
            'landmark': '',
            'post_code': '',
            'country_id': 101,
            'state_id': '',
            'city_id': '',
            'location_id': '',
            'site_id': '',
            'contact_person_name': '',
            'contact_person_number': '',
          });
          this.getapplication(this.site_details.segment_id);
          this.Addnameprodinfo.patchValue({
            'segment_id': this.site_details.segment_id,
            'application_id': this.site_details.application_id,
            'proioritys': this.site_details.priority
          })
          this.getcountry();
          this.onChangeCountryas(this.site_details.country.country_id);
          this.onChangeStateas(this.site_details.state.state_id);
          this.onChangecityas(this.site_details.city.city_id);
          this.overlayRef.detach();

        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          var contract_detail = result.response.contract_details;
          if (contract_detail.length == '0') {
            this.error = "There is no Contract for this customer";
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': 101,
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
            })
            this.overlayRef.detach();
            this.chRef.detectChanges();
          }
          this.toastr.error(result.response.message, "Error");
        }
        else if (result.response.response_code == "400") {
          // this.raise_ticket_reset();
          this.toastr.error(result.response.message, "Error");
          // var default_radiovalue = '1';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = 1;

          this.overlayRef.detach();
          this.customer_code = '';
          // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
          this.getcountry();
          var today = new Date();
          var today_date = this.calendar.getToday()
          var year = today_date.year;
          var month = today_date.month;
          var day = today_date.day;
          this.config.maxDate = {
            day: day,
            month: month,
            year: year,
          };


        }
        else if (result.response.response_code == "500") {
          this.overlayRef.detach();
          // this.error = result.response.message
          this.toastr.error(result.response.message, "Error");
        }
        else {
          this.overlayRef.detach();
          this.error = "Something went wrong";
          this.toastr.error(this.error, "Error");
        }
        (err) => {
          this.overlayRef.detach();
          console.log(err); //prints if it encounters an error
        }
      });
      this.modalService.open(model, {
        size: 'lg', //specifies the size of the dialog box
        backdrop: 'static',              //popup outside click by Sowndarya
        keyboard: false
      });
    }

  }
  // function for modal box
  openLarge(raise_ticket) {
    this.title = "Add Customer name";
    this.mfgdatetype = 'Enable';
    this.serial_error = '';
    this.serialSaveContinue = [];
    this.product = [];
    this.subproduct = [];
    this.model_id = [];
    this.warranty_id = [];
    this.segment = [];
    this.application = [];
    this.labelwarranty = "Select Warranty type";
    this.call = [];
    this.getsegment();
    this.getproduct();
    this.getcallcategory();
    this.get_servicegroup();
    this.AddProductInfo.reset();
    this.RaiseCall.reset();
    this.modalService.dismissAll();
    this.ByDefault = false;
    this.imageError = '';
    this.message = '';
    this.cardImageBase64 = '';
    this.isImageSaved = false;
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.error = '';
    this.type = '';
    this.loadingSubmit = false;
    this.buttonmainedit = false;
    // this.RaiseTicketProdInfo.reset();
    // console.log(this.RaiseTicketProdInfo.value)
    this.image_validation = '';                 //image error message refreshed
    this.image_name = '';                        //image name is refershed
    this.image = '';                 //image is refershed
    this.raise_error = ''            //refresh the 500 error message while raise the ticket
    // this.Addnameinfo.reset();
    this.activeId = "tab-selectbyid1";
    this.Addnameinfo.patchValue({
      'country_id': 101,
    });

    this.onChangeCountry(101);
    this.modalService.open(raise_ticket, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
    this.to = this.calendar.getToday();
    this.RaiseCall.controls["cust_preference_date"].setValue(this.to)

  }
  getcallcategory() {
    var url = 'get_call_category/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.call = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  //get country dropdown
  getcountry() {
    var url = 'get_country/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.country = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  continue() {
    this.save_continue = true
    this.addSiteIdwithCustomerName(2);
  }

  addSiteIdwithCustomerName(val) {
    console.log(this.AddSiteInfo.value.invoice_date);
    var year = this.AddSiteInfo.value.invoice_date.year;
    var month = this.AddSiteInfo.value.invoice_date.month;
    var day = this.AddSiteInfo.value.invoice_date.day;
    var dateFormated = year + "-" + month + "-" + day;
    this.AddSiteInfo.controls['invoice_date'].setValue(dateFormated);
    if (this.Addnameprodinfo.value.mfg_date) {
      var ids = this.Addnameprodinfo.value.mfg_date.split("-", 3);
      var year = ids[0];
      var month = ids[1];
      var day = ids[2];
      var date = day + "-" + month + "-" + year;
      this.Addnameprodinfo.controls["mfg_date"].setValue(date);
    }
    console.log(this.Addnameprodinfo.value.expire_date)
    if (this.Addnameprodinfo.value.expire_date) {
      var ids = this.Addnameprodinfo.value.expire_date.split("-", 3);
      var year1 = ids[0];
      var month1 = ids[1];
      var day1 = ids[2];
      var date1 = day1 + "-" + month1 + "-" + year1;
      this.Addnameprodinfo.controls["expire_date"].setValue(date1);
    }
    // var year1 = this.AddSiteInfo.value.mfg_date.year;
    // var month1 = this.AddSiteInfo.value.mfg_date.month;
    // var day1 = this.AddSiteInfo.value.mfg_date.day;
    // var dateFormated1 = year1 + "-" + month1 + "-" + day1;
    // this.AddSiteInfo.controls['mfg_date'].setValue(dateFormated1);
    // // var year2 = this.AddSiteInfo.value.expiry_day.year;
    // // var month2 = this.AddSiteInfo.value.expiry_day.month;
    // // var day2 = this.AddSiteInfo.value.expiry_day.day;
    // var dateFormated2 = year2 + "-" + month2 + "-" + day2;
    // this.AddSiteInfo.controls['expiry_day'].setValue(dateFormated2);
    this.AddSiteInfo.controls['employee_code'].setValue(this.employee_code);
    // this.AddSiteInfo.controls['amc_id'].setValue(this.AddSiteInfo.value.warranty_type_id);  //ONLY NEED FOR API DUMMY DATA
    this.AddSiteInfo.controls['voltage'].setValue(this.AddSiteInfo.value.voltage);

    console.log(this.AddSiteInfo.value)
    var serial: any = [];
    this.serial = [];
    var serial_num = '';
    serial = {
      "product_id": this.AddSiteInfo.value.product_id,
      "product_sub_id": this.AddSiteInfo.value.product_sub_id,
      "model": this.AddSiteInfo.value.model_no,
      "sys_ah": this.AddSiteInfo.value.sys_ah,
      "serial_no": this.AddSiteInfo.value.serial_no,
      "invoice_number": this.AddSiteInfo.value.invoice_number,
      "invoice_date": this.AddSiteInfo.value.invoice_date,
      "mfg_date": this.AddSiteInfo.value.mfg_date,
      "warranty_type_id": this.AddSiteInfo.value.warranty_type_id,
      "expiry_date": date1,
      // "voltage": this.AddSiteInfo.value.voltage,
    };

    if (this.save_continue) {
      this.serialSaveContinue.push(serial);
      console.log(this.serialSaveContinue, "serialSaveContinue")
      var serial_num = JSON.stringify(this.serialSaveContinue);
      this.AddSiteInfo.controls['serial_no'].setValue('');
      // serial=this.serialSaveContinue
    } else {
      // this.serial=serial;
      this.serial.push(serial);
      var serial_num = JSON.stringify(this.serial);

    }

    // console.log(this.serialSaveContinue, "serialSaveContinue")
    if (this.Addnameprodinfo.value.battery_bank_id != '' || this.Addnameprodinfo.value.battery_bank_id != null) {
      this.battery_bank_id = this.Addnameprodinfo.value.battery_bank_id
    }
    else {
      this.battery_bank_id = ""
    }
    if (this.Addnameprodinfo.value.site_id != '' || this.Addnameprodinfo.value.site_id != null) {
      console.log("iff")
      this.site_id = this.Addnameprodinfo.value.site_id
    }
    else {
      console.log("else")
      this.site_id = ""
    }
    console.log(serial_num, "serial")
    var data = this.AddSiteInfo.value
    data["serial_no_details"] = serial_num;
    console.log(data)
    this.form = ({
      "employee_code": this.employee_code,
      "customer_code": this.Addnameinfo.value.customer_code,
      "customer_name": this.Addnameinfo.value.customer_name,
      "email_id": this.Addnameinfo.value.email_id,
      "contact_number": this.Addnameinfo.value.contact_number,
      "alternate_number": this.Addnameinfo.value.alternate_number,
      "sap_reference_number": this.Addnameinfo.value.sap_reference_number,
      "plot_number": this.Addnameinfo.value.plot_number,
      "contact_person_name": this.Addnameinfo.value.contact_person_name,
      "contact_person_number": this.Addnameinfo.value.contact_person_number,
      "street": this.Addnameinfo.value.street,
      "landmark": this.Addnameinfo.value.landmark,
      "post_code": this.Addnameinfo.value.post_code,
      "country_id": this.Addnameinfo.value.country_id,
      "state_id": this.Addnameinfo.value.state_id,
      "city_id": this.Addnameinfo.value.city_id,
      "location_id": this.Addnameinfo.value.location_id,
      "product_id": this.Addnameprodinfo.value.product_id,
      "product_sub_id": this.Addnameprodinfo.value.product_sub_id,
      "model_no": this.Addnameprodinfo.value.model_no,
      "serial_no": this.Addnameprodinfo.value.serial_no,
      "invoice_date": this.Addnameprodinfo.value.invoice_date,
      "contract_duration": this.Addnameprodinfo.value.contract_duration,
      "site_id": this.site_id,
      "battery_bank_id": this.battery_bank_id,
      "sys_ah": this.Addnameprodinfo.value.sys_ah,
      "segment_id": this.Addnameprodinfo.value.segment_id,
      "application_id": this.Addnameprodinfo.value.application_id,
      "organization_name": this.Addnameprodinfo.value.organization_name,
      "invoice_number": this.Addnameprodinfo.value.invoice_number,
      "expiry_day": this.Addnameprodinfo.value.expiry_day,
      "warranty_type_id": this.Addnameprodinfo.value.warranty_type_id,
      "mfg_date": this.Addnameprodinfo.value.mfg_date,
      "voltage": this.Addnameprodinfo.value.voltage.toUpperCase(),
      "serial_no_details": serial_num
    })
    console.log(this.form)
    var url = 'add_customer_contract/'                                         //api url of add
    this.ajax.postdata(url, this.form).subscribe((result) => {
      //if sucess
      if (result.response.response_code == "200") {
        // this.modalService.dismissAll();
        if (val == 2) {
          this.AddSiteInfo.patchValue({
            'battery_bank_id': result.response.site_battery_details.battery_bank_id,

          })
          this.AddSiteInfo.controls["customer_code"].setValue(result.response.site_battery_details.customer_code);

          this.Addnameprodinfo.patchValue({
            'segment_id': this.Addnameprodinfo.value.segment_id,
            'application_id': this.Addnameprodinfo.value.application_id,
          });
          this.Addnameprodinfo.controls["product"].setValue("null");
          this.Addnameprodinfo.controls["subproduct"].setValue("null")
        }
        else {
          this.modalService.dismissAll();
        }
        var parmas = {}
        parmas = { 'customer_code': result.response.site_battery_details.customer_code, 'site_id': result.response.site_battery_details.site_id, "flag": 1 };

        var url = 'get_customer_contract_details/?';
        this.overlayRef.attach(this.LoaderComponentPortal);
        // api url for getting the customer and contract details
        this.ajax.postdata(url, parmas).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.Addnameinfo.controls["country_id"].setValue(101)
            this.getsegment();
            this.Addnameinfo.patchValue({
              'country_id': 101,
            });
            this.onChangeCountryas(101);
            this.error = '';
            this.site_details = [];
            this.customer_details = result.response.customer_details;
            this.site_details = result.response.site_details;
            this.chRef.detectChanges();
            this.contact_number = this.customer_details.contact_number;
            this.customer_code = this.customer_details.customer_code;
            this.batteryData = result.response.battery_bank_array;
            if (this.batteryData.length == 1) {
              this.showTable = false;
              this.getSerialNmber(this.batteryData[0]);
            }
            this.productdata = [];
            this.site_details.serial_no_details.forEach(obj => {
              var date = obj.expiry_date.split("-", 3);
              var year = date[0];
              var month = date[1];
              var day = date[2];
              obj.expiry_date = day + "-" + month + "-" + year;
              this.productdata.push(obj);
            })
            console.log(this.productdata);
            if (this.batteryData.length == 1) {
              this.battery_data = 'Enable';
              this.showTable = true;
            }
            this.isShow = false;
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': this.batteryData,
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_name,
              'state_id': this.site_details.state.state_name,
              'city_id': this.site_details.city.city_name,
              'location_id': this.site_details.location.location_name,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            if (this.site_details.priority == 'P1') {
              this.setPriority = "P1"
            }
            else {
              this.setPriority = this.site_details.priority
            }
            this.RaiseCall.patchValue({
              'proioritys': this.site_details.priority,
            })
            this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);

            console.log(this.site_details.priority);
            this.Addnameinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_number': this.customer_details.alternate_number,
              'plot_number': '',
              'battery_bank_id': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': 101,
              'state_id': '',
              'city_id': '',
              'location_id': '',
              'site_id': '',
              'contact_person_name': '',
              'contact_person_number': '',
            });
            this.getapplication(this.site_details.segment_id);
            this.Addnameprodinfo.patchValue({
              'segment_id': this.site_details.segment_id,
              'application_id': this.site_details.application_id,
              'proioritys': this.site_details.priority
            })
            this.getcountry();
            this.onChangeCountryas(this.site_details.country.country_id);
            this.onChangeStateas(this.site_details.state.state_id);
            this.onChangecityas(this.site_details.city.city_id);
            this.overlayRef.detach();

          }
          else if (result.response.response_code == "404") {
            this.customer_details = result.response.customer_details;
            var contract_detail = result.response.contract_details;
            if (contract_detail.length == '0') {
              this.error = "There is no Contract for this customer";
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': 101,
                'state_id': '',
                'city_id': '',
                'location_id': ''
              })
              this.overlayRef.detach();

            }
            else {
              this.error = ''
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
              })
              this.overlayRef.detach();
              this.chRef.detectChanges();
            }
            this.toastr.error(result.response.message, "Error");
          }
          else if (result.response.response_code == "400") {
            // this.raise_ticket_reset();
            this.toastr.error(result.response.message, "Error");
            // var default_radiovalue = '1';
            // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            // this.radiochange_value = 1;

            this.overlayRef.detach();
            this.customer_code = '';
            // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
            this.getcountry();
            var today = new Date();
            var today_date = this.calendar.getToday()
            var year = today_date.year;
            var month = today_date.month;
            var day = today_date.day;
            this.config.maxDate = {
              day: day,
              month: month,
              year: year,
            };


          }
          else if (result.response.response_code == "500") {
            this.overlayRef.detach();
            // this.error = result.response.message
            this.toastr.error(result.response.message, "Error");
          }
          else {
            this.overlayRef.detach();
            this.error = "Something went wrong";
            this.toastr.error(this.error, "Error");
          }
          (err) => {
            this.overlayRef.detach();
            console.log(err); //prints if it encounters an error
          }
        });
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
      }
      //if error
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        // this.loadingAdd = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        // this.loadingAdd = false;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  continueBatteryAdd() {

    this.save_continue = true;
    this.type = 'Enable';
    this.addBatteryBankIdCustomerName(2);
    this.Addbatterybank.reset();
    this.labelwarranty = "Select Warranty type";
    this.Addbatterybank.patchValue({
      'battery_bank_id': this.BatteryBankId,
    });
    this.labelwarranty = '';
  }
  addProduct(batteryadd) {
    console.log(this.productdata)
    this.serialSaveContinue = [];
    this.modalService.open(batteryadd, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
    this.Addbatterybank.patchValue({
      'battery_bank_id': this.BatteryBankId,
    });
    this.getsegment();
    this.productdata.forEach(obj => {
      var item = {
        "product_id": obj.product.product_id,
        "product_sub_id": obj.subproduct.product_sub_id,
        "model": obj.model.model_id,
        "voltage": obj.voltage.toUpperCase(),
        "sys_ah": obj.sys_ah,
        "serial_no": obj.serial_no,
        "invoice_number": obj.invoice_number,
        "invoice_date": obj.invoice_date,
        "mfg_date": obj.mfg_date,
        "warranty_type_id": obj.warranty.warranty_id,
        "expiry_date": obj.expiry_date
      }
      this.serialProduct.push(item)
    })
    console.log(this.serialProduct);
    this.serialSaveContinue = this.serialProduct;
    this.serial = this.serialSaveContinue;
    console.log(this.BatteryBankId);
    console.log(this.productdata);
  }
  addBatteryBankIdCustomerName(val) {
    console.log(this.productdata)
    console.log(this.Addbatterybank.value);
    console.log(this.Addbatterybank.value.invoice_date)
    if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
      this.Addbatterybank.controls['invoice_date'].setValue('');
    }
    else {
      var year = this.Addbatterybank.value.invoice_date.year;
      var month = this.Addbatterybank.value.invoice_date.month;
      var day = this.Addbatterybank.value.invoice_date.day;
      var dateFormated = year + "-" + month + "-" + day;
      this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
    }
    // if (this.Addbatterybank.value.mfg_date) {
    //   console.log(this.Addbatterybank.value.mfg_date)
    //   var ids = this.Addbatterybank.value.mfg_date.split("-", 3);
    //   var year = ids[0];
    //   var month = ids[1];
    //   var day = ids[2];
    //   var date = day + "-" + month + "-" + year;
    //   console.log(date)
    //   this.Addbatterybank.controls["mfg_date"].setValue(date);
    // }
    // if (this.Addbatterybank.value.expiry_day) {
    //   console.log(this.Addbatterybank.value.expiry_day)
    //   var ids = this.Addbatterybank.value.expiry_day.split("-", 3);
    //   var year1 = ids[0];
    //   var month1 = ids[1];
    //   var day1 = ids[2];
    //   var date1 = day1 + "-" + month1 + "-" + year1;
    //   console.log(date1)
    //   this.Addbatterybank.controls["expiry_day"].setValue(date1);
    // }
    // this.Addbatterybank.controls['voltage'].setValue('voltage');
    var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
    var year = ids[0];
    var month = ids[1];
    var day = ids[2];
    var exdate = day + "-" + month + "-" + year;
    console.log(exdate)
    // this.Addbatterybank.controls['expiry_day'].setValue(exdate);

    var id = this.Addbatterybank.value.mfg_date.split("-", 3)
    var year = id[0];
    var month = id[1];
    var day = id[2];
    var mfdate = day + "-" + month + "-" + year;
    console.log(mfdate)
    // this.Addbatterybank.controls['mfg_date'].setValue(mfdate);
    this.Addbatterybank.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
    this.Addbatterybank.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
    console.log(this.Addbatterybank.value);
    this.serialProduct = [];
    var serial: any = {};
    this.serial = [];
    var serial_num = '';
    serial = {
      "product_id": this.Addbatterybank.value.product_id,
      "product_sub_id": this.Addbatterybank.value.product_sub_id,
      "model": this.Addbatterybank.value.model_no,
      "sys_ah": this.Addbatterybank.value.sys_ah,
      "voltage": this.Addbatterybank.value.voltage.toUpperCase(),
      "serial_no": this.Addbatterybank.value.serial_no,
      "invoice_number": this.Addbatterybank.value.invoice_number,
      "invoice_date": this.Addbatterybank.value.invoice_date,
      "mfg_date": this.Addbatterybank.value.mfg_date,
      "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
      "expiry_date": this.Addbatterybank.value.expiry_day
    };
    console.log(serial)
    // console.log(this.serialSaveContinue, "serialSaveContinue")
    // console.log(this.serialNumbersdata, "serialNumbersdata")
    console.log(this.title, "this.title")
    console.log(this.Addbatterybank.value.battery_bank_id)
    console.log(this.set)
    if (this.set == 1) {
      console.log(this.Addbatterybank.value.expiry_day)
      var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
      var year = ids[0];
      var month = ids[1];
      var day = ids[2];
      var exdate = day + "-" + month + "-" + year;
      console.log(exdate)
      this.Addbatterybank.controls['expiry_day'].setValue(exdate);

      var id = this.Addbatterybank.value.mfg_date.split("-", 3)
      var year = id[0];
      var month = id[1];
      var day = id[2];
      var mfdate = day + "-" + month + "-" + year;
      console.log(mfdate)
      this.Addbatterybank.controls['mfg_date'].setValue(mfdate);

      if (val == 2) {

        console.log(this.productdata, " this.productdata")
        this.productdata.forEach(obj => {


          var item = {
            "product_id": obj.product.product_id,
            "product_sub_id": obj.subproduct.product_sub_id,
            "voltage": obj.voltage.toUpperCase(),
            "model": obj.model.model_id,
            "sys_ah": obj.sys_ah,
            "serial_no": obj.serial_no,
            "invoice_number": obj.invoice_number,
            "invoice_date": obj.invoice_date,
            "mfg_date": mfdate,
            "warranty_type_id": obj.warranty.warranty_id,
            "expiry_date": exdate
          }
          this.serialProduct.push(item)
        })
        console.log(this.serialProduct);

        this.serialProduct.push(serial);

        console.log(this.serialProduct, "serialSaveContinue")
        var serial_num = JSON.stringify(this.serialProduct);


      }

      else if (val == 1) {
        if (val == 1) {
          this.Addbatterybank.patchValue({
            'battery_bank_id': '',
          });
          var serial_num = '';
          console.log(serial)
          this.serial.push(serial);
          serial_num = JSON.stringify(this.serial);
        }
        else if (val == 2) {
          console.log(this.productdata, " this.productdata")
          this.productdata.forEach(obj => {
            // var ids = obj.expiry_date.split("-", 3)
            // var year = ids[0];
            // var month = ids[1];
            // var day = ids[2];
            // var exdate = day + "-" + month + "-" + year;
            // console.log(exdate)
            // var id = obj.mfg_date.split("-", 3)
            // var year = id[0];
            // var month = id[1];
            // var day = id[2];
            // var mfdate = day + "-" + month + "-" + year;
            // console.log(mfdate)

            var item = {
              "product_id": obj.product.product_id,
              "product_sub_id": obj.subproduct.product_sub_id,
              "voltage": obj.voltage.toUpperCase(),
              "model": obj.model.model_id,
              "sys_ah": obj.sys_ah,
              "serial_no": obj.serial_no,
              "invoice_number": obj.invoice_number,
              "invoice_date": obj.invoice_date,
              "mfg_date": mfdate,
              "warranty_type_id": obj.warranty.warranty_id,
              "expiry_date": exdate
            }
            this.serialProduct.push(item)
          })
          console.log(this.serialProduct);

          this.serialProduct.push(serial);

          console.log(this.serialProduct, "serialSaveContinue")
          var serial_num = JSON.stringify(this.serialProduct);
        }

      }
    }
    else if (this.set == 2) {
      console.log(this.Addbatterybank.value.expiry_day)
      var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
      var year = ids[0];
      var month = ids[1];
      var day = ids[2];
      var exdate = day + "-" + month + "-" + year;
      console.log(exdate)
      this.Addbatterybank.controls['expiry_day'].setValue(exdate);

      var id = this.Addbatterybank.value.mfg_date.split("-", 3)
      var year = id[0];
      var month = id[1];
      var day = id[2];
      var mfdate = day + "-" + month + "-" + year;
      console.log(mfdate)
      this.Addbatterybank.controls['mfg_date'].setValue(mfdate);

      console.log(this.productdata, " this.productdata")
      this.serialProduct = [];
      this.productdata.forEach(obj => {


        var item = {
          "product_id": obj.product.product_id,
          "product_sub_id": obj.subproduct.product_sub_id,
          "voltage": obj.voltage.toUpperCase(),
          "model": obj.model.model_id,
          "sys_ah": obj.sys_ah,
          "serial_no": obj.serial_no,
          "invoice_number": obj.invoice_number,
          "invoice_date": obj.invoice_date,
          "mfg_date": obj.mfg_date,
          "warranty_type_id": obj.warranty.warranty_id,
          "expiry_date": obj.expiry_date
        }
        this.serialProduct.push(item)
      })
      console.log(this.serialProduct);

      this.serialProduct.push(serial);

      console.log(this.serialProduct, "serialSaveContinue")
      var serial_num = JSON.stringify(this.serialProduct);





    }


    console.log(serial_num, "serial")
    if (serial_num != '') {
      var data = this.Addbatterybank.value
      data["serial_no_details"] = serial_num;
      data["employee_code"] = this.employee_code
      data["customer_id"] = this.Addnamesiteinfo.value.customer_id,
        data["customer_code"] = this.Addnamesiteinfo.value.customer_code,
        data["email_id"] = this.Addnamesiteinfo.value.email_id,
        data["contact_number"] = this.Addnamesiteinfo.value.contact_number,
        data["alternate_number"] = this.Addnamesiteinfo.value.alternate_number,
        data["plot_number"] = this.Addnamesiteinfo.value.plot_number,
        data["street"] = this.Addnamesiteinfo.value.street,
        data["landmark"] = this.Addnamesiteinfo.value.landmark,
        data["post_code"] = this.Addnamesiteinfo.value.post_code,
        data["country_id"] = this.Addnamesiteinfo.value.country_id,
        data["state_id"] = this.Addnamesiteinfo.value.state_id,
        data["city_id"] = this.Addnamesiteinfo.value.city_id,
        data["location_id"] = this.Addnamesiteinfo.value.location_id,
        data["site_id"] = this.Addnamesiteinfo.value.site_id,
        data["contact_person_name"] = this.Addnamesiteinfo.value.contact_person_name,
        data["contact_person_number"] = this.Addnamesiteinfo.value.contact_person_number,
        data["sap_reference_number"] = this.Addnamesiteinfo.value.sap_reference_number,
        console.log(data)
      var url = 'add_customer_contract/'                                         //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        //if sucess
        if (result.response.response_code == "200") {
          if (val == 2) {
            this.Addbatterybank.patchValue({
              'battery_bank_id': result.response.site_battery_details.battery_bank_id,
            })
            this.Addnamesiteinfo.controls["customer_code"].setValue(result.response.site_battery_details.customer_code);

            this.Addbatterybank.controls['segment_id'].setValue(this.Addbatterybank.value.segment_id);
            this.Addbatterybank.controls['application_id'].setValue(this.Addbatterybank.value.application_id);
            // this.Addbatterybank.controls['contract_duration'].setValue(this.Addbatterybank.value.contract_duration);
            console.log(this.Addbatterybank.value);
          }
          else {
            this.Addbatterybank.controls['product_id'].setValue('');
            this.Addbatterybank.controls['product_sub_id'].setValue('');
            this.Addbatterybank.controls['model_no'].setValue('');
            this.Addbatterybank.controls['serial_no'].setValue('');
            this.Addbatterybank.controls['invoice_date'].setValue('');
            this.Addbatterybank.controls['contract_duration'].setValue('');
            this.Addbatterybank.controls['battery_bank_id'].setValue('');
            this.Addbatterybank.controls['sys_ah'].setValue('');
            // this.Addbatterybank.controls['segment_id'].setValue('');
            // this.Addbatterybank.controls['application_id'].setValue('');
            this.Addbatterybank.controls['organization_name'].setValue('');
            this.Addbatterybank.controls['invoice_number'].setValue('');
            this.Addbatterybank.controls['expiry_day'].setValue('');
            this.Addbatterybank.controls['warranty_type_id'].setValue('');
            this.Addbatterybank.controls['mfg_date'].setValue('');
            this.modalService.dismissAll();
            this.batteryBank();
          }
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          var parmas = {};
          parmas = { 'customer_code': result.response.site_battery_details.customer_code, "site_id": result.response.site_battery_details.site_id, "flag": 1 };
          var url = 'get_customer_contract_details/?';
          this.overlayRef.attach(this.LoaderComponentPortal);
          this.ajax.postdata(url, parmas).subscribe((result) => {
            if (result.response.response_code == "200") {
              this.error = '';
              this.site_details = [];
              this.showTable = false;
              this.customer_details = result.response.customer_details;
              this.site_details = result.response.site_details;
              this.segment_value = this.site_details.segment_id;
              this.application_value = this.site_details.application_id;
              console.log(this.segment_value, "this.segment_value")
              this.chRef.detectChanges();
              this.contact_number = this.customer_details.contact_number;
              this.customer_code = this.customer_details.customer_code;
              this.batteryData = result.response.battery_bank_array;
              if (this.batteryData.length == 1) {
                this.battery_data = 'Enable';
                this.showTable = true;
                this.getSerialNmber(this.batteryData[0]);
                this.site_details.serial_no_details.forEach(obj => {
                  var date = obj.expiry_date.split("-", 3);
                  var year = date[0];
                  var month = date[1];
                  var day = date[2];
                  obj.expiry_date = day + "-" + month + "-" + year;
                  this.productdata.push(obj);
                })
                console.log(this.productdata);
              }

              // if (this.batteryData.length == 1) {

              // }
              this.isShow = false;

              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,
                'battery_bank_id': this.batteryData,
                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_id,
                'state_id': this.site_details.state.state_id,
                'city_id': this.site_details.city.city_id,
                'location_id': this.site_details.location.location_name,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              })
              this.Addnamesiteinfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,
                'battery_bank_id': this.batteryData,
                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_id,
                'state_id': this.site_details.state.state_id,
                'city_id': this.site_details.city.city_id,
                'location_id': this.site_details.location.location_name,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              })

              this.getcountry();
              this.onChangeCountryas(this.site_details.country.country_id);
              this.onChangeStateas(this.site_details.state.state_id);
              this.onChangecityas(this.site_details.city.city_id);
              this.overlayRef.detach();

            }
            else if (result.response.response_code == "404") {
              this.customer_details = result.response.customer_details;
              var contract_detail = result.response.contract_details;
              if (contract_detail.length == '0') {
                this.error = "There is no Contract for this customer";
                this.Addnamesiteinfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                  'plot_number': '',
                  'street': '',
                  'landmark': '',
                  'post_code': '',
                  'country_id': '',
                  'state_id': '',
                  'city_id': '',
                  'location_id': ''
                })
                this.overlayRef.detach();

              }
              else {
                this.error = ''
                this.isShow = false;
                this.Addnamesiteinfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                })
                this.overlayRef.detach();
                this.chRef.detectChanges();
              }
              this.overlayRef.detach();
            }
            else if (result.response.response_code == "400") {
              this.raise_ticket_reset();
              // var default_radiovalue = '1';
              // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
              // this.radiochange_value = 1;

              this.overlayRef.detach();
              this.customer_code = '';
              // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
              this.getcountry();
              var today = new Date();
              var today_date = this.calendar.getToday()
              var year = today_date.year;
              var month = today_date.month;
              var day = today_date.day;
              this.config.maxDate = {
                day: day,
                month: month,
                year: year,
              };


            }
            else if (result.response.response_code == "500") {
              this.overlayRef.detach();
              this.error = result.response.message
            }
            else {
              this.overlayRef.detach();
              this.error = "Something went wrong"
            }
            (err) => {
              this.overlayRef.detach();
              console.log(err); //prints if it encounters an error
            }
          });
        }
        //if error
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }

  }
  customerContinue() {
    this.customer_save_continue = true
    this.addcustomer_name(2);
  }
  addcustomer_name(val) {
    console.log(this.radiochange_value)
    console.log(this.Addnameprodinfo.value)
    console.log(this.Addnameprodinfo)
    console.log(this.Addnameprodinfo.valid)
    console.log(this.Addnameprodinfo.value.invoice_date)
    if (this.Addnameprodinfo.value.invoice_date) {
      var year = this.Addnameprodinfo.value.invoice_date.year;
      var month = this.Addnameprodinfo.value.invoice_date.month;
      var day = this.Addnameprodinfo.value.invoice_date.day;
      var dateFormated = year + "-" + month + "-" + day;
      this.Addnameprodinfo.controls['invoice_date'].setValue(dateFormated);
      var ids = this.Addnameprodinfo.value.invoice_date.split("-", 3);
      var year = ids[0];
      var month = ids[1];
      var day = ids[2];
      var date = year + "-" + month + "-" + day;
      this.Addnameprodinfo.controls["invoice_date"].setValue(date);
    }
    else {
      this.Addnameprodinfo.controls['invoice_date'].setValue('');
    }
    if (this.Addnameprodinfo.value.battery_bank_id == null) {
      this.Addnameprodinfo.controls['battery_bank_id'].setValue('');
    }
    else {
      this.Addnameprodinfo.value.battery_bank_id = this.Addnameprodinfo.value.battery_bank_id;
    }
    if (this.Addnameinfo.value.site_id == null) {
      this.Addnameinfo.controls['site_id'].setValue('');
    }
    else {
      this.Addnameinfo.value.site_id = this.Addnameinfo.value.site_id;

    }

    this.Addnameinfo.controls['employee_code'].setValue(this.employee_code);
    this.Addnameprodinfo.controls['voltage'].setValue(this.Addnameprodinfo.value.voltage);
    // this.Addnameinfo.controls['sys_ah'].setValue('sys_ah');

    // "voltage":"Volt",
    // "sys_ah":this.AddSiteInfo.value.sys_ah
    console.log(this.Addnameprodinfo.value.expiry_day)
    // var ids = this.Addnameprodinfo.value.expiry_day.split("-", 3)
    // var year = ids[0];
    // var month = ids[1];
    // var day = ids[2];
    var year = this.Addnameprodinfo.value.expiry_day.year;
    var month = this.Addnameprodinfo.value.expiry_day.month;
    var day = this.Addnameprodinfo.value.expiry_day.day;
    // var dateFormated = year + "-" + month + "-" + day;
    var exdate = day + "-" + month + "-" + year;
    this.Addnameprodinfo.controls["expiry_day"].setValue(exdate);
    // var ids1 = this.Addnameprodinfo.value.mfg_date.split("-", 3)
    // var year1 = ids1[0];
    // var month1 = ids1[1];
    // var day1 = ids1[2];
    var year1 = this.Addnameprodinfo.value.mfg_date.year;
    var month1 = this.Addnameprodinfo.value.mfg_date.month;
    var day1 = this.Addnameprodinfo.value.mfg_date.day;
    var mgdate = day1 + "-" + month1 + "-" + year1;
    this.Addnameprodinfo.controls["mfg_date"].setValue(mgdate);
    console.log(exdate)
    var serial: any = [];
    this.serial = [];
    var serial_num = '';
    serial = {
      "product_id": this.Addnameprodinfo.value.product_id,
      "product_sub_id": this.Addnameprodinfo.value.product_sub_id,
      "model": this.Addnameprodinfo.value.model_no,
      "sys_ah": this.Addnameprodinfo.value.sys_ah,
      "serial_no": this.Addnameprodinfo.value.serial_no,
      "invoice_number": this.Addnameprodinfo.value.invoice_number,
      "invoice_date": this.Addnameprodinfo.value.invoice_date,
      "mfg_date": this.Addnameprodinfo.value.mfg_date,
      "warranty_type_id": this.Addnameprodinfo.value.warranty_type_id,
      "expiry_date": this.Addnameprodinfo.value.expiry_day,
      "voltage": this.Addnameprodinfo.value.voltage.toUpperCase(),
    };
    if (val == 1) {

      this.AddSiteInfo.patchValue({
        'battery_bank_id': '',
      });
      console.log(serial)
      this.serial.push(serial);
      var serial_num = JSON.stringify(this.serial);


    }

    else {
      console.log(this.productdata, " this.productdata")
      this.productdata.forEach(obj => {
        var item = {
          "product_id": obj.product.product_id,
          "product_sub_id": obj.subproduct.product_sub_id,
          "model": obj.model.model_id,
          "voltage": obj.voltage.toUpperCase(),
          "sys_ah": obj.sys_ah,
          "serial_no": obj.serial_no,
          "invoice_number": obj.invoice_number,
          "invoice_date": obj.invoice_date,
          "mfg_date": obj.mfg_date,
          "warranty_type_id": obj.warranty.warranty_id,
          "expiry_date": obj.expiry_date
        }
        this.serialProduct.push(item)
      })
      console.log(this.serialProduct);

      this.serialProduct.push(serial);

      console.log(this.serialProduct, "serialSaveContinue")
      var serial_num = JSON.stringify(this.serialProduct);

    }
    console.log(serial_num, "serial_num")
    console.log(this.Addnameprodinfo.value)


    // data["serial_no_details"] = serial_num;
    console.log(this.Addnameinfo.value.site_id, "this.Addnameinfo.value.site_id")
    console.log(this.Addnameprodinfo.value.battery_bank_id, "this.Addnameprodinfo.value.battery_bank_id")

    // console.log(this.serialSaveContinue, "serialSaveContinue")
    if (this.Addnameprodinfo.value.battery_bank_id != '' || this.Addnameprodinfo.value.battery_bank_id != null || this.Addnameprodinfo.value.battery_bank_id != undefined) {
      this.battery_bank_id = this.Addnameprodinfo.value.battery_bank_id
    }
    else {
      this.battery_bank_id = ""
    }
    if (this.Addnameinfo.value.site_id != '' || this.Addnameinfo.value.site_id != null || this.Addnameinfo.value.site_id != undefined) {
      console.log("if")
      this.site_id = this.Addnameinfo.value.site_id
    }
    else {
      console.log("else")
      this.site_id = ""
    }
    // if (this.Addnameprodinfo.value.invoice_date == "undefined-undefined-undefined" || this.Addnameprodinfo.value.invoice_date == null || this.Addnameprodinfo.value.invoice_date == '') {
    //   this.Addnameprodinfo.controls["invoice_date"].setValue('');
    // }
    // else {
    //   console.log(this.Addnameprodinfo.value.invoice_date)
    //   var ids = this.Addnameprodinfo.value.invoice_date.split("-", 3);
    //   var year = ids[0];
    //   var month = ids[1];
    //   var day = ids[2];
    //   var date = year + "-" + month + "-" + day;
    //   this.Addnameprodinfo.controls["invoice_date"].setValue(date);
    // }
    // if (this.Addnameprodinfo.value.mfg_date != '') {
    //   var ids = this.Addnameprodinfo.value.mfg_date.split("-", 3);
    //   var year = ids[0];
    //   var month = ids[1];
    //   var day = ids[2];
    //   var date = day + "-" + month + "-" + year;
    //   this.Addnameprodinfo.controls["mfg_date"].setValue(date);
    // }
    // if (this.Addnameprodinfo.value.expiry_day != '') {
    //   var ids = this.Addnameprodinfo.value.expiry_day.split("-", 3);
    //   var year1 = ids[0];
    //   var month1 = ids[1];
    //   var day1 = ids[2];
    //   var date1 = day1 + "-" + month1 + "-" + year1;
    //   this.Addnameprodinfo.controls["expiry_day"].setValue(date1);
    // }
    var data = this.Addnameprodinfo.value
    console.log(this.Addnameinfo.value.customer_type)

    console.log(data)
    this.form = ({
      "employee_code": this.employee_code,
      "site_id": this.site_id,
      "customer_code": this.Addnameinfo.value.customer_code,
      "customer_name": this.Addnameinfo.value.customer_name,
      "customer_type": this.Addnameinfo.value.customer_type,
      "email_id": this.Addnameinfo.value.email_id,
      "contact_number": this.Addnameinfo.value.contact_number,
      "alternate_number": this.Addnameinfo.value.alternate_number,
      "sap_reference_number": this.Addnameinfo.value.sap_reference_number,
      "plot_number": this.Addnameinfo.value.plot_number,
      "contact_person_name": this.Addnameinfo.value.contact_person_name,
      "contact_person_number": this.Addnameinfo.value.contact_person_number,
      "street": this.Addnameinfo.value.street,
      "landmark": this.Addnameinfo.value.landmark,
      "post_code": this.Addnameinfo.value.post_code,
      "country_id": this.Addnameinfo.value.country_id,
      "state_id": this.Addnameinfo.value.state_id,
      "city_id": this.Addnameinfo.value.city_id,
      "location_id": this.Addnameinfo.value.location_id,
      "product_id": this.Addnameprodinfo.value.product_id,
      "product_sub_id": this.Addnameprodinfo.value.product_sub_id,
      "model_no": this.Addnameprodinfo.value.model_no,
      "serial_no": this.Addnameprodinfo.value.serial_no,
      "invoice_date": this.Addnameprodinfo.value.invoice_date,
      "contract_duration": this.Addnameprodinfo.value.contract_duration,
      "battery_bank_id": this.battery_bank_id,
      "sys_ah": this.Addnameprodinfo.value.sys_ah,
      "segment_id": this.Addnameprodinfo.value.segment_id,
      "application_id": this.Addnameprodinfo.value.application_id,
      "organization_name": this.Addnameprodinfo.value.organization_name,
      "invoice_number": this.Addnameprodinfo.value.invoice_number,
      "expiry_day": this.Addnameprodinfo.value.expiry_day,
      "warranty_type_id": this.Addnameprodinfo.value.warranty_type_id,
      "mfg_date": this.Addnameprodinfo.value.mfg_date,
      "voltage": this.Addnameprodinfo.value.voltage.toUpperCase(),
      "serial_no_details": serial_num
    });
    console.log(this.form)
    if (serial_num != '') {
      var url = 'add_customer_contract/'                                         //api url of add
      this.ajax.postdata(url, this.form).subscribe((result) => {
        //if sucess
        if (result.response.response_code == "200") {
          if (val == 2) {
            this.type = 'Enable';
            this.Addnameprodinfo.controls["battery_bank_id"].setValue(result.response.site_battery_details.battery_bank_id);
            this.Addnameinfo.controls["site_id"].setValue(result.response.site_battery_details.site_id);
            this.Addnameinfo.controls["customer_code"].setValue(result.response.site_battery_details.customer_code);
            this.Addnameprodinfo.controls["serial_no"].setValue('');
            this.Addnameprodinfo.controls["segment_id"].setValue(this.Addnameprodinfo.value.segment_id);
            this.Addnameprodinfo.controls["application_id"].setValue(this.Addnameprodinfo.value.application_id);
            this.Addnameprodinfo.controls["model_no"].setValue('')
            this.Addnameprodinfo.controls["product_sub_id"].setValue('null')
            this.Addnameprodinfo.controls["product_id"].setValue('null')
          }
          else if (val == 1) {
            this.type = '';
            this.modalService.dismissAll();
          }
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.customerName(this.customer_type['ustomer_type_id']);
          this.customerNumber();
          this.customerId();
          this.batteryBank();
          var parmas = {}
          parmas = { 'customer_code': result.response.site_battery_details.customer_code, 'site_id': result.response.site_battery_details.site_id, "flag": 1 };

          var url = 'get_customer_contract_details/?';
          this.overlayRef.attach(this.LoaderComponentPortal);
          // api url for getting the customer and contract details
          this.ajax.postdata(url, parmas).subscribe((result) => {
            if (result.response.response_code == "200") {
              this.Addnameinfo.controls["country_id"].setValue(101)
              this.getsegment();
              this.Addnameinfo.patchValue({
                'country_id': 101,
              });
              this.onChangeCountryas(101);
              this.error = '';
              this.site_details = [];
              this.customer_details = result.response.customer_details;
              this.site_details = result.response.site_details;
              this.segment_value = this.site_details.segment_id;
              this.application_value = this.site_details.application_id;
              console.log(this.segment_value, "this.segment_value")
              this.chRef.detectChanges();
              this.contact_number = this.customer_details.contact_number;
              this.customer_code = this.customer_details.customer_code;
              this.batteryData = result.response.battery_bank_array;
              if (this.batteryData.length == 1) {
                this.showTable = false;
                this.getSerialNmber(this.batteryData[0]);
              }
              this.productdata = [];
              this.site_details.serial_no_details.forEach(obj => {
                // var date = obj.expiry_date.split("-", 3);
                // var year = date[0];
                // var month = date[1];
                // var day = date[2];
                // obj.expiry_date = day + "-" + month + "-" + year;
                this.productdata.push(obj);
              })
              console.log(this.productdata);
              if (this.batteryData.length == 1) {
                this.battery_data = 'Enable';
                this.showTable = true;
              }
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,
                'battery_bank_id': this.batteryData,
                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_name,
                'state_id': this.site_details.state.state_name,
                'city_id': this.site_details.city.city_name,
                'location_id': this.site_details.location.location_name,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              })
              if (this.site_details.priority == 'P1') {
                this.setPriority = "P1"
              }
              else {
                this.setPriority = this.site_details.priority
              }
              this.RaiseCall.patchValue({
                'proioritys': this.site_details.priority,
              })
              this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);

              console.log(this.site_details.priority);
              this.Addnameinfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_number': this.customer_details.alternate_number,
                'plot_number': '',
                'battery_bank_id': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': 101,
                'state_id': '',
                'city_id': '',
                'location_id': '',
                'site_id': '',
                'contact_person_name': '',
                'contact_person_number': '',
              });
              this.getapplication(this.site_details.segment_id);
              this.Addnameprodinfo.patchValue({
                'segment_id': this.site_details.segment_id,
                'application_id': this.site_details.application_id,
                'proioritys': this.site_details.priority
              })
              this.getcountry();
              this.onChangeCountryas(this.site_details.country.country_id);
              this.onChangeStateas(this.site_details.state.state_id);
              this.onChangecityas(this.site_details.city.city_id);
              this.overlayRef.detach();

            }
            else if (result.response.response_code == "404") {
              this.customer_details = result.response.customer_details;
              var contract_detail = result.response.contract_details;
              if (contract_detail.length == '0') {
                this.error = "There is no Contract for this customer";
                this.RaiseTicketCusInfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                  'plot_number': '',
                  'street': '',
                  'landmark': '',
                  'post_code': '',
                  'country_id': 101,
                  'state_id': '',
                  'city_id': '',
                  'location_id': ''
                })
                this.overlayRef.detach();

              }
              else {
                this.error = ''
                this.isShow = false;
                this.RaiseTicketCusInfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                })
                this.overlayRef.detach();
                this.chRef.detectChanges();
              }
              this.toastr.error(result.response.message, "Error");
            }
            else if (result.response.response_code == "400") {
              // this.raise_ticket_reset();
              this.toastr.error(result.response.message, "Error");
              // var default_radiovalue = '1';
              // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
              // this.radiochange_value = 1;

              this.overlayRef.detach();
              this.customer_code = '';
              // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
              this.getcountry();
              var today = new Date();
              var today_date = this.calendar.getToday()
              var year = today_date.year;
              var month = today_date.month;
              var day = today_date.day;
              this.config.maxDate = {
                day: day,
                month: month,
                year: year,
              };


            }
            else if (result.response.response_code == "500") {
              this.overlayRef.detach();
              // this.error = result.response.message
              this.toastr.error(result.response.message, "Error");
            }
            else {
              this.overlayRef.detach();
              this.error = "Something went wrong";
              this.toastr.error(this.error, "Error");
            }
            (err) => {
              this.overlayRef.detach();
              console.log(err); //prints if it encounters an error
            }
          });
        }
        //if error
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          // this.loadingAdd = false;
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          // this.loadingAdd = false;
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }

  }





  //get state dropdown
  onChangeCountry(country_id) {
    if (country_id == 101) {
      var country = +country_id; // country: number
      var url = 'get_state/?'; // api url for getting the details with using post params
      var id = "country_id";
      this.ajax.getdataparam(url, id, country).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.states = result.response.data;
          // this.cities = [];
          // this.locations = [];
        } else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue('null'); //set the base64 in create product image
          this.states = null;
          this.cities = [];
          this.locations = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
          this.cities = [];
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
          this.cities = [];
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else if (country_id != '') {
      this.RaiseTicketCusInfo.controls['city_id'].setValue("");
      this.RaiseTicketCusInfo.controls['location_id'].setValue("");
      this.RaiseTicketCusInfo.controls['state_id'].setValue("");
      var country = +country_id; // country: number
      var url = 'get_state/?'; // api url for getting the details with using post params
      var id = "country_id";
      this.ajax.getdataparam(url, id, country).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.states = result.response.data;
          this.cities = [];
          this.locations = [];
        } else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue('null'); //set the base64 in create product image
          this.states = null;
          this.cities = [];
          this.locations = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
          this.cities = [];
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
          this.cities = [];
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.states = null;
      this.locations = [];
      this.cities = [];
    }

  }
  onChangeCountryas(country_id) {
    if (country_id != '') {
      var country = +country_id; // country: number
      var url = 'get_state/?'; // api url for getting the details with using post params
      var id = "country_id";
      this.ajax.getdataparam(url, id, country).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.states = result.response.data;
          this.cities = [];
          this.locations = [];
        } else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue('null'); //set the base64 in create product image
          this.states = null;

        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.states = null;
      this.locations = [];
      this.cities = [];
    }

  }

  //get city dropdown
  onChangeState(state_id) {
    if (state_id != '') {
      this.RaiseTicketCusInfo.controls['cus_state'].setValue(state_id);

      var state = +state_id; // state: number
      var id = "state_id";
      var url = 'get_city/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, state).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data; //storing the api response in the array
          this.locations = [];
          this.Addnameinfo.controls['location_id'].setErrors({ 'incorrect': true });

        }
        else if (result.response.response_code == "400") {
          this.cities = 'null';
          this.locations = [];
          this.Addnameinfo.controls['location_id'].setErrors({ 'incorrect': true });
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
          this.Addnameinfo.controls['location_id'].setErrors({ 'incorrect': true });
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
          this.Addnameinfo.controls['location_id'].setErrors({ 'incorrect': true });
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {

      this.locations = [];
      this.cities = [];
    }
  }
  battery_banks = [
    { id: 1, name: 'Vilnius' },
    { id: 2, name: 'Kaunas' },
    { id: 3, name: 'Pavilnys', disabled: true },
    { id: 4, name: 'Pabradė' },
    { id: 5, name: 'Klaipėda' }
  ];
  battery_bank_settings = {
    singleSelection: true,
    idField: 'battery_bank_id',
    textField: 'battery_bank_id',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search battery bank id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customername_cid_sid_settings = {
    singleSelection: true,
    idField: 'customer_site',
    textField: 'customer_site',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search Customer Details',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  }
  siteid_settings = {
    singleSelection: true,
    idField: 'customer_site',
    textField: 'customer_site',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search Site id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customerid_settings = {
    singleSelection: true,
    idField: 'customer_id',
    textField: 'customer_code',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search customer id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customername_settings = {
    singleSelection: true,
    idField: 'city_location_ids',
    textField: 'customer_name',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search customer name',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false,
    lazyLoading: true
  };
  customerstate_settings = {
    singleSelection: false,
    idField: 'state_id',
    textField: 'state_name',
    enableCheckAll: true,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    // limitSelection: -1,
    clearSearchFilter: true,
    // maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search state name',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  contactnumber_settings = {
    singleSelection: true,
    idField: 'customer_code',
    textField: 'number_code',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search contact number',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  oem_customers_settings = {
    singleSelection: true,
    idField: 'oem_name',
    textField: 'oem_customer_name',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search OEM customer',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  onItemDeSelect(items: any) {
    this.customerSelect = '';
    this.name = [];
    this.RaiseTicketCusInfo.controls["customer_name"].setValue("");
    this.sitedetails = [];
    this.select = [];
    this.RaiseTicketCusInfo.controls["site_id"].setValue("");
  }
  onChangeTown(event) {
    console.log("hi i am here")
    this.RaiseTicketCusInfo.controls['cus_town'].setValue(event);
    console.log(this.role_id, "this.role_id ")
    // console.log(this.RaiseTicketCusInfo.value.cus_city)
  }
  onChangeType(event) {
    this.customer_name_ref = [];
    // this.RaiseTicketCusInfo.controls['customer_type_id'].setValue(event);
    // console.log(this.RaiseTicketCusInfo.value);
    var customer_type = event;
    this.customer_type_id = customer_type;
    this.Addnameinfo.controls['customer_type'].setValue(this.customer_type_id)

    if (this.role_id != '7') {
      this.params = {
        "state_id": this.RaiseTicketCusInfo.value.cus_state,
        "city_id": this.RaiseTicketCusInfo.value.cus_city,
        "location_id": this.RaiseTicketCusInfo.value.cus_town,
        "customer_type_id": customer_type
      }
    }
    else {
      this.params = {
        "state_id": this.RaiseTicketCusInfo.value.cus_state,
        "city_id": this.RaiseTicketCusInfo.value.cus_city,
        "location_id": this.RaiseTicketCusInfo.value.cus_town,
        'customer_code': this.logged_customer_code
      }
    }

    this.select = [];
    this.getSiteDetails(this.params);
    this.customerName(this.customer_type_id)
    // this.get_oem_customer();
  }
  public onFilterChange(item: any) {
    console.log(item);
  }
  public onNumberSelect(item) {
    // this.numberSelect=item;
    var ids = item.customer_site.split("-", 3);
    var customer_id = ids[0];
    var site_id = ids[1];
    console.log(ids);
    //  this.Addnameinfo.controls['customer_name'].setValue(customer_name);
    //  this.Addnameinfo.controls['customer_id'].setValue("");
    // this.cus_name = customer_name;
    this.cus_cus_id = customer_id;
    this.cus_siteId = site_id;
    if (this.contactSelect.customer_code) {
      this.numberSelect = { "customer_code": this.contactSelect.customer_code };
      this.getSiteDetails(this.numberSelect);
    }
    else {
      this.toastr.warning("Please select customer number and Site id");

    }
    console.log(this.numberSelect);
  }
  public onItemSelectstate(item: any) {
    console.log(item.state_id);
    this.customer_name_ref = [];
    this.customer_state.push(item.state_id);
    console.log(this.customer_type_id, "this.customer_type_id")
    if (this.customer_type_id != undefined || this.customer_type_id != '' || this.customer_type_id != null) {
      this.customerName(this.customer_type_id)
    }
    // this.RaiseTicketCusInfo.controls["customer_state"].setValue(this.customer_state);
  }
  onItemDeSelectstate(items: any) {
    this.customer_name_ref = [];
    // this.customer_state = this.customer_state.filter(items => items !== items.state_id);
    // this.RaiseTicketCusInfo.controls["customer_state"].setValue("");
    const index: number = this.customer_state.indexOf(items.state_id);
    if (index !== -1) {
      this.customer_state.splice(index, 1);
    }
    if (this.customer_type_id != undefined || this.customer_type_id != '' || this.customer_type_id != null) {
      this.customerName(this.customer_type_id)
    }

  }
  onItemDeSelectAllState(items: any) {
    this.customer_state = [];
    this.customer_names = [];
    this.customer_name_ref = [];
    this.RaiseTicketCusInfo.controls['customer_type'].setValue('null');
    // if (this.customer_type_id != undefined || this.customer_type_id != '' || this.customer_type_id != null) {
    // this.customerName(this.customer_type_id)
    // }

    // this.RaiseTicketCusInfo.controls["customer_state"].setValue("");

  }
  onSelectAllState(items: any) {
    this.customer_name_ref = [];
    var isPresent = this.customer_state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.customer_state.push(value.state_id);
      })
    }
    if (this.customer_type_id != undefined || this.customer_type_id != '' || this.customer_type_id != null) {
      this.customerName(this.customer_type_id)
    }



  }
  public onItemSelect(item: any) {
    console.log(item);
    // this.customer_name_ref =item;

    // this.customerSelect = item;

    // var ids = item.customer_name.split("-", 3);
    // var name = ids[0];
    var id = item.city_location_ids.split("-", 5);
    console.log(id, "id")
    var name = id[0];
    var city = id[1];
    var location = id[2];
    var landmark = id[3];
    var customer_code = id[4];
    var customer_details = { "customer_name": name, "city_id": city, "location_id": location, "landmark": landmark, "customer_code": customer_code };
    this.name = name;
    this.customerSelect = name;
    // var customer_id = ids[1];
    // var site_id = ids[2];
    this.name = item;
    this.sitedetails = [];
    this.select = [];

    this.RaiseTicketCusInfo.controls["cus_site_id"].setValue("");
    this.getSiteDetails(customer_details);
  }
  public onCusSelet(item: any) {
    console.log(item);
    var id = item.city_location_ids.split("-", 5);
    console.log(id, "id")
    var name = id[0];
    var city = id[1];
    var location = id[2];
    var landmark = id[3];
    var customer_code = id[4];
    var customer_details = { "customer_name": name, "city_id": city, "location_id": location, "landmark": landmark, "customer_code": customer_code };
    console.log(customer_details, "customer_details");
    this.Addnameinfo.controls['customer_code'].setValue(customer_code)
    this.get_cus_data(customer_code)
    this.get_customer_details(customer_details)
  }
  public onoemSelet(item: any) {
    console.log(item);
    this.get_oem_customer()
    // var ids = item.customer_site.split("-", 3);
    // var customer_name = ids[0];
    // var customer_id = ids[1];
    // var site_id = ids[2];
    // this.cus_name = customer_name;
    // this.cus_cus_id = customer_id;
    // this.cus_siteId = site_id;
    // this.customerSelect = { "customer_name": customer_name };
  }
  public onItemSelectcusid(item: any) {
    console.log(item);
    this.customerSelect = item;
    this.getSiteDetails(this.customerSelect);
  }
  public onAllIdSelect(item: any) {
    console.log(item);
    var ids = item.customer_site.split("-", 3);
    var customer_name = ids[0];
    var customer_id = ids[1];
    var site_id = ids[2];
    //  this.Addnameinfo.controls['customer_name'].setValue(customer_name);
    //  this.Addnameinfo.controls['customer_id'].setValue("");
    this.cus_name = customer_name;
    this.cus_cus_id = customer_id;
    this.cus_siteId = site_id;
    this.customerSelect = { "customer_name": customer_name };
    //  this.Addnameinfo.patchValue({
    //   'customer_name': customer_name,
    // })
    //  console.log(this.Addnameinfo.value.customer_name);
    // this.customerSelect = item;
    // this.getSiteDetails(this.customerSelect);
  }
  public onBBSelect(item: any) {
    console.log(item);
    this.bbSelect = item;
    // this.getSiteDetails(this.bbSelect);
  }
  public onSiteIdSelect(item: any) {
    console.log(item);
    this.siteSelect = item;
    // this.getSiteDetails(item);
  }
  public onContactSelect(item: any) {
    console.log(item);
    this.contactSelect = item;
    this.sitedetails = [];
    this.select = []
    console.log(this.sitedetails)
    this.getSiteDetails(this.contactSelect);
  }

  public onDropDownClose(item: any) {
    // console.log(item);
  }
  searchReset() {
    this.sitedetails = [];
    console.log("dfd")
  }
  customerName(customer_type_id) {
    console.log(this.customer_state, "this.customer_state")
    console.log(this.RaiseTicketCusInfo.value.customer_state, "this.RaiseTicketCusInfo.value.customer_state")
    console.log(this.RaiseTicketCusInfo.value.customer_state, "this.RaiseTicketCusInfo.value.customer_state")
    var data = { "customer_type_id": customer_type_id, "customer_state": this.customer_state }
    var url = 'get_customer_name/';                                  // api url for getting the details
    this.placeholder = "Loading..."
    this.ajax.postdata1(url, data).then((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customer_names = result.response.data;              //storing the api response in the array                       //show datas if after call API
        this.placeholder = "Select..."
        this.placeholder = "Select customer name"
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
      // this.isLoading =false; 
    }, (err) => {
      // this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  batteryBank() {
    var data ={}
    if (this.role_id != '7') {
      data = {"employee_code": this.employee_code}
     
    }
    else {
       data = { "customer_code": this.logged_customer_code }
    }
    console.log(data)
    var url = 'get_batterybank_id/';     
    this.placeholder = "Loading..."                             // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.batteryBank_date = result.response.data;              //storing the api response in the array
        this.placeholder = "Select..."
        this.placeholder = "Select Battery Bank ID"
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  getSerialNmber(bbid) {
    this.batteryBankArray = [];
    // console.log(bbid);
    this.productdatas = [];
    this.productdata = [];
    this.raise_data = '';
    // this.battery_data = 'Enable';
    this.showTable = false;
    this.BatteryBankId = bbid;
    this.battery_datas = 'Enable';
    var data = { battery_bank_id: this.BatteryBankId };
    var url = 'get_serial_numbers/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.productdatas = result.response.data;
        //storing the api response in the array
        this.productdata = [];
        this.productdatas.forEach(obj => {

          this.ExpiryDate = obj.expiry_date;
          if (this.ExpiryDate) {
            var expdates = this.ExpiryDate.split("-", 3);
            var Eyear: number = +expdates[2];
            var Emonth: number = +expdates[1];
            var Eday: number = +expdates[0];
          }
          // const ExpiryDate:NgbDate={
          //   day: Eday,
          //   month: Emonth,
          //   year: Eyear,
          // }
          var TDate = this.calendar.getToday()
          var year = TDate.year;
          var month = TDate.month;
          var day = TDate.day;
          const VDate: NgbDate = new NgbDate(year, month, day);
          const ExpiryDate: NgbDate = new NgbDate(Eyear, Emonth, Eday);
          if (VDate.before(ExpiryDate)) {
            this.expiry_flag = "1";
          }
          else {
            this.expiry_flag = "0";
          }
          console.log(TDate);
          console.log(ExpiryDate);
          console.log(obj.expiry_date)
          obj.expiry_flag = this.expiry_flag;
          console.log(this.expiry_flag);
          // var exdate = obj.expiry_date.split("-", 3);
          // var exyear = exdate[0];
          // var exmonth = exdate[1];
          // var exday = exdate[2];
          // obj.expiry_date = exday + "-" + exmonth + "-" + exyear;
          // this.ExpiryDate = exday + "-" + exmonth + "-" + exyear;
          // obj.expiry_date = this.ExpiryDate;
          console.log(obj.hasOwnProperty('expiry_flag'));

          this.productdata.push(obj);
        });
        console.log(this.productdata);
        this.battery_data = 'Enable'
        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.battery_data = '';                                                   //if not sucess
        this.productdatas = [];
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }

      else if (result.response.response_code == "400") {
        this.toastr.error("There is no product for this battery bank");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  checkValidity(item) {
    this.ExpiryDate = item.expiry_date;
    if (this.ExpiryDate) {
      var expdates = this.ExpiryDate.split("-", 3);
      var Eyear: number = +expdates[0];
      var Emonth: number = +expdates[1];
      var Eday: number = +expdates[2];
    }
    // const ExpiryDate:NgbDate={
    //   day: Eday,
    //   month: Emonth,
    //   year: Eyear,
    // }
    var TDate = this.calendar.getToday()
    var year = TDate.year;
    var month = TDate.month;
    var day = TDate.day;
    const VDate: NgbDate = new NgbDate(year, month, day);
    const ExpiryDate: NgbDate = new NgbDate(Eyear, Emonth, Eday);
    if (VDate.before(ExpiryDate)) {
      this.expiry_flag = "1";
    }
    else {
      this.expiry_flag = "0";
    }
    console.log(this.expiry_flag);
    console.log(this.VDate);
    console.log(ExpiryDate);
    console.log(this.validwarranty);
  }
  customerNumber() {
    var data = {"employee_code" : this.employee_code}
    var url = 'get_customer_number/';      
    this.placeholder = "Loading..."                            // api url for getting the details
    this.ajax.postdata1(url, data).then((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.contactNumber = result.response.data;              //storing the api response in the array
        this.placeholder = "Select..."
        this.placeholder = "Select customer number"
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      // this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  customerId() {
    var data = {"employee_code" : this.employee_code}
    var url = 'get_customer_id/';     
    this.placeholder = "Loading..."                                // api url for getting the details
    this.ajax.postdata1(url, data).then((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customerID = result.response.data;              //storing the api response in the arra
        this.placeholder = "Select..."
        this.placeholder = "Select customer ID"
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {                                                   //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  getSiteDetails(data) {
    console.log(data)
    var data = data;
    var url = 'get_site_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sitedetails = result.response.data;              //storing the api response in the array
        console.log(this.sitedetails)
        // this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  get_customer_details(customer_data) {
    console.log(customer_data)
    var data = customer_data;
    var url = 'get_customer_code_data/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customer_code = result.response.data;              //storing the api response in the array
        // this.Addnameinfo.patchValue({
        //   "customer_code": new FormControl(""),
        //   "customer_name": new FormControl('', [Validators.required]),
        //   "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
        //   "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
        //   "alternate_number": new FormCo
        // })

        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  // function to get th customer code based on the customer name,city ,location and landmark
  get_cus_data(customer_code) {
    console.log(customer_code)
    var data = { "customer_code": customer_code };
    var url = 'customer_check/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        var customer_data = result.response.customer_details;              //storing the api response in the array
        console.log(customer_data, "customer_data")
        this.Addnameinfo.patchValue({
          "customer_code": customer_data.customer_code,
          // "customer_name": customer_data.customer_code,
          "email_id": customer_data.email_id,
          "contact_number": customer_data.contact_number,
          "alternate_number": customer_data.alternate_number,
        })

        this.chRef.detectChanges();
        console.log(this.Addnameinfo.value)
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  onChangeStateas(state_id) {
    if (state_id != '') {
      var state = +state_id; // state: number
      var id = "state_id";
      var url = 'get_city/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, state).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data; //storing the api response in the array

        }
        else if (result.response.response_code == "400") {
          this.cities = 'null';
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error

      });
    }
    else {
      this.locations = [];
      this.cities = [];
    }
  }

  onChangecity(cty_id) {
    if (cty_id != '') {
      this.RaiseTicketCusInfo.controls['cus_city'].setValue(cty_id);
      this.RaiseTicketCusInfo.controls['location_id'].setValue("");
      var city = +cty_id; // state: number
      var id = "city_id";
      var url = 'get_location_details/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, city).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.locations = result.response.data; //storing the api response in the array
        }
        else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue(null); //set the base64 in create product image
          this.locations = null;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.locations = [];
    }
  }
  onChangecityas(cty_id) {

    // if (cty_id != '') {
    var city = +cty_id; // state: number
    var id = "city_id";
    var url = 'get_location_details/?'; // api url for getting the cities
    this.ajax.getdataparam(url, id, city).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.locations = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "400") {
        this.locations = null;
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
    // }
    // else {
    //   console.log(cty_id, "Empty")
    //   this.locations = null;
    // }
  }
  //get product details
  getproduct() {
    var url = 'get_product_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.subproduct = "";
        this.product = result.response.data; //storing the api response in the array
        this.model_id = "";
        this.warranty_id = "";

      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  duration(item) {
    this.warranty_type.forEach(element => {
      if (element.warranty_id == item) {
        this.labelwarranty = element.warranty_duration;
        this.Addbatterybank.controls['contract_duration'].setValue(this.labelwarranty);
        this.AddSiteInfo.controls['contract_duration'].setValue(this.labelwarranty);
        this.Addnameprodinfo.controls['contract_duration'].setValue(this.labelwarranty)
      }
      else {
        this.labelwarranty = "Select Warranty type";
      }
    });

  }
  //get sub product details based on product id
  onChangeProduct(product_id) {
    this.subproduct = [];
    this.model_id = [];
    this.warranty_id = [];
    this.labelwarranty = "Select Warranty type";
    this.products_id = +product_id; // country: number
    var url = 'get_subproduct_details/?'; // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, this.products_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data; //reloading the component
        // this.Addnameprodinfo.controls['subproduct'].setValue("null")
        //   this.warranty_id = '';
      }
      else if (result.response.response_code == "400") {
        //   this.subproduct = [];
        //   this.model_id = [];
        //   this.warranty_id = [];
      }
      else if (result.response.response_code == "500") {
        //   this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }

  //radio change function
  onchangeradio(event: MatRadioChange) {
    this.customerSelect = '';
    this.showTable = false;
    this.cus_name = '';
    this.cus_cus_id = '';
    this.cus_siteId = '';
    this.bbSelect = '';
    this.cus_cus_id = '';
    this.cus_siteId = '';
    this.contactSelect = '';
    this.siteSelect = '';
    this.siteSelect = '';
    this.customerSelect = '';
    this.batteryBankSample = [];
    this.RaiseTicketCusInfo.reset();
    this.AddSiteInfo.reset();
    this.Addnameprodinfo.reset();
    this.Addbatterybank.reset();
    this.Addnameinfo.reset();
    this.RaiseCall.reset();
    this.batteryData = [];
    this.siteSelect = [];
    this.bbSelect = [];
    this.bbSelect = '';
    this.battery_datas = '';
    this.battery_data = '';
    this.raise_data = '';
    // console.log(event)
    event.source.checked = true;
    // console.log(event.source.checked)
    this.radiochange_value = event.value;
    if (event.value == 4) {
      this.RaiseTicketProdInfo.reset();
      this.onChangeCountry(101);
      this.locations = [];
      this.cities = [];
      this.RaiseTicketCusInfo.controls["cus_state"].setValue(null);
      this.RaiseTicketCusInfo.controls["cus_city"].setValue(null);
      this.RaiseTicketCusInfo.controls["cus_town"].setValue(null);
      this.RaiseTicketCusInfo.controls["customer_type"].setValue(null);
     
    }
    else if (event.value == 3) {
      this.batteryBank();
    }
    // no need to call at the time of radio button clicking 
    // else if (event.value == 5) {
    //   this.customerName(this.customer_type['ustomer_type_id']);

    // }
    else if (event.value == 1) {

      this.customerNumber();
    }
    else if (event.value == 2) {
      this.customerId();
    }
  }
  extractSerialnumber(serialno) {
    // this.productdata='';
    // if (this.AddSiteInfo) {
    //   var serialno: any;
    //   console.log(this.AddSiteInfo.value.serial_no);
    //   if (this.AddSiteInfo.value.serial_no) {
    //     serialno = this.AddSiteInfo.value.serial_no;
    //     var serialnum_length = serialno.toString().length;
    //     console.log(serialnum_length);
    //   }
    //   else {
    //     serialno = "";
    //   }
    //   if (serialno) {
    //     var serial_number = { 'serial_no': serialno };
    //     var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
    //     this.ajax.postdata(url, serial_number).subscribe((result) => {
    //       if (result.response.response_code == "200" || result.response.response_code == "400") {
    //         this.serial_error = '';
    //         this.flag = result.response.flag;
    //         console.log(this.flag, "flag")
    //         if (this.flag == 0) {
    //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
    //           this.product = [];
    //           this.subproduct = [];
    //           this.model_id = [];
    //           this.warranty_id = [];
    //           this.product.push(this.serialNumbersdata.product);
    //           this.model_id.push(this.serialNumbersdata.model);
    //           this.subproduct.push(this.serialNumbersdata.product_sub);
    //           this.products_id = this.serialNumbersdata.product.product_id
    //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
    //           // this.getmodel(this.serialNumbersdata.product_sub.product_sub_id);
    //           this.mfg_date = this.serialNumbersdata.manufacture_date;
    //           console.log(this.serialNumbersdata);
    //           this.AddSiteInfo.patchValue({
    //             'product_id': this.serialNumbersdata.product.product_id,
    //             'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
    //             'model_no': this.serialNumbersdata.model.model_id,
    //             "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
    //             "sys_ah": this.serialNumbersdata.sys_ah,
    //             "voltage": this.serialNumbersdata.voltage,
    //             "mfg_date": this.serialNumbersdata.manufacture_date,
    //             "expiry_day": this.serialNumbersdata.expiry_day,

    //           });
    //           console.log(this.serialNumbersdata.warranty_type.warranty_id);
    //           this.warranty_id.push(this.serialNumbersdata.warranty_type);
    //           console.log(this.serialNumbersdata.warranty_type.warranty_id);
    //           this.duration(this.serialNumbersdata.warranty_type.warranty_id);
    //         }
    //         else if (this.flag == 1) {
    //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
    //           this.product = [];
    //           this.subproduct = [];
    //           this.model_id = [];
    //           this.product.push(this.serialNumbersdata.product);
    //           this.subproduct.push(this.serialNumbersdata.product_sub);
    //           this.products_id = this.serialNumbersdata.product.product_id
    //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
    //           this.serialNumbersdata.model.forEach(element => {
    //             this.model_id.push(element);
    //           });

    //           console.log(this.subid);
    //           this.AddSiteInfo.patchValue({
    //             'product_id': this.products_id,
    //             'product_sub_id': this.subid,
    //             "mfg_date": this.serialNumbersdata.manufacture_date,
    //             "expiry_day": this.serialNumbersdata.expiry_day,

    //           });
    //           this.AddSiteInfo.controls['mfg_date'].setValue(this.serialNumbersdata.manufacture_date);
    //           this.AddSiteInfo.controls['expiry_day'].setValue(this.serialNumbersdata.expiry_day);
    //         }
    //         else if (this.flag == 2) {
    //           // this.getproduct();
    //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
    //           this.AddSiteInfo.controls['mfg_date'].setValue(this.serialNumbersdata.manufacture_date);
    //           this.product = [];
    //           this.subproduct = [];
    //           this.model_id = [];
    //           this.serialNumbersdata.product.forEach(element => {
    //             this.product.push(element);
    //           });
    //           console.log(this.product)
    //         }
    //         else if (this.flag == 3) {

    //           this.AddSiteInfo.patchValue({
    //             "mfg_date": result.response.manufacture_date[0].manufacture_date
    //           });

    //           this.getsegment();
    //           this.getproduct();
    //         }
    //         // }

    //         this.chRef.detectChanges();
    //       }
    //       else if (result.response.response_code == "500") {
    //         this.toastr.error(result.response.message, 'Error')

    //         this.serial_error = result.response.data;
    //       }
    //       else {
    //         this.toastr.error("Something went wrong", 'Error')
    //         this.serial_error = "Something went wrong";
    //       }
    //     }, (err) => {
    //       console.log(err); //prints if it encounters an error
    //     });
    //   }

    // }
    // else
    if (this.Addnameprodinfo) {
      console.log("hihihi")
      var serialno: any;
      console.log(this.Addnameprodinfo.value.serial_no);
      console.log(this.Addnameprodinfo);
      if (this.Addnameprodinfo.value.serial_no) {
        serialno = this.Addnameprodinfo.value.serial_no;
        var serialnum_length = serialno.toString().length;
        console.log(serialnum_length);
      }
      else {
        serialno = "";
      }
      if (serialno) {
        this.Addnameprodinfo.patchValue({
          'product_id': '',
          'product_sub_id': '',
          'model_no': '',
          "warranty_type_id": '',
          "sys_ah": '',
          "voltage": '',
          "mfg_date": '',
          "expiry_day": '',

        });
        var serial_number = { 'serial_no': serialno };
        var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
        this.ajax.postdata(url, serial_number).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {
            // if( result.response.manufacture_date.flag == 3){
            //   this.product=[];
            //   result.response.manufacture_date.forEach(element => {
            //   this.product.push(element);
            //   });
            // }else{
            this.Addnameprodinfo = new FormGroup({

              "product_id": new FormControl('', [Validators.required]),
              "product_sub_id": new FormControl('', [Validators.required]),
              "model_no": new FormControl('', [Validators.required]),
              "serial_no": new FormControl(this.Addnameprodinfo.value.serial_no),
              "invoice_date": new FormControl(''),
              "contract_duration": new FormControl(''),
              "battery_bank_id": new FormControl(this.Addnameprodinfo.value.battery_bank_id),
              "sys_ah": new FormControl('', [Validators.required]),
              "segment_id": new FormControl(this.Addnameprodinfo.value.segment_id, [Validators.required]),
              "application_id": new FormControl(this.Addnameprodinfo.value.application_id, [Validators.required]),
              "organization_name": new FormControl(this.Addnameprodinfo.value.organization_name, [Validators.required]),
              "invoice_number": new FormControl(this.Addnameprodinfo.value.invoice_number),
              "expiry_day": new FormControl(this.Addnameprodinfo.value.expiry_day),
              "warranty_type_id": new FormControl(this.Addnameprodinfo.value.warranty_type_id, [Validators.required]),
              "mfg_date": new FormControl(this.Addnameprodinfo.value.mfg_date),
              "voltage": new FormControl(this.Addnameprodinfo.value.voltage),
              "image": new FormControl(this.Addnameprodinfo.value.image),
              "cust_preference_date": new FormControl(this.Addnameprodinfo.value.cust_preference_date, [Validators.required]),
              "proioritys": new FormControl(this.Addnameprodinfo.value.proioritys, [Validators.required]),
              "call_category_id": new FormControl(this.Addnameprodinfo.value.call_category_id, [Validators.required]),
              "work_type_id": new FormControl(this.Addnameprodinfo.value.work_type_id, [Validators.required]),
              "desc": new FormControl(this.Addnameprodinfo.value.desc, [Validators.required, this.noWhitespace])
            });
            this.serial_error = '';
            this.flag = result.response.flag;
            if (this.flag == 0) {
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.warranty_type = [];
              this.product.push(this.serialNumbersdata.product);
              this.model_id.push(this.serialNumbersdata.model);
              this.subproduct.push(this.serialNumbersdata.product_sub);
              this.products_id = this.serialNumbersdata.product.product_id
              this.subid = this.serialNumbersdata.product_sub.product_sub_id;
              // this.getmodel(this.serialNumbersdata.product_sub.product_sub_id);
              // this.mfg_date = this.serialNumbersdata.manufacture_date;
              // this.getwarranty(this.serialNumbersdata.model.model_id);
              if (result.response.manufacture_date[0]) {
                var mrfdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Myear: number = +mrfdates[0];
                var Mmonth: number = +mrfdates[1];
                var Mday: number = +mrfdates[2];
              }

              const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
              this.Addnameprodinfo.controls["mfg_date"].setValue(MDate);
              this.mfgdatetype = 'Disable';
              if (result.response.manufacture_date[0]) {
                var expdates = result.response.manufacture_date[0].expiry_day.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }

              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addnameprodinfo.controls["expiry_day"].setValue(EDate);
              console.log(this.serialNumbersdata);
              this.Addnameprodinfo.patchValue({
                'product_id': this.serialNumbersdata.product.product_id,
                'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
                'model_no': this.serialNumbersdata.model.model_id,
                "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
                "sys_ah": this.serialNumbersdata.sys_ah,
                "voltage": this.serialNumbersdata.voltage.toUpperCase(),
                // "mfg_date": this.serialNumbersdata.manufacture_date,
                // "expiry_day": this.serialNumbersdata.expiry_day,

              });
              console.log(this.serialNumbersdata.warranty_type.warranty_id);
              // this.warranty_id = this.serialNumbersdata.warranty_type;
              this.warranty_type.push(this.serialNumbersdata.warranty_type);
              console.log(this.serialNumbersdata.warranty_type.warranty_id);
              this.duration(this.serialNumbersdata.warranty_type.warranty_id);
              this.getsegment();
            }
            else if (this.flag == 1) {
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.product.push(this.serialNumbersdata.product);
              // this.model_id.push(this.serialNumbersdata.model);
              this.subproduct.push(this.serialNumbersdata.product_sub);
              this.products_id = this.serialNumbersdata.product.product_id
              this.subid = this.serialNumbersdata.product_sub.product_sub_id;
              // this.mfg_date=this.serialNumbersdata.manufacture_date;
              this.serialNumbersdata.model.forEach(element => {
                this.model_id.push(element);
              });
              if (result.response.manufacture_date[0]) {
                var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }
              this.mfgdatetype = 'Disable';
              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);
              console.log(this.subid);
              this.Addnameprodinfo.patchValue({
                'product_id': this.products_id,
                'product_sub_id': this.subid,
                // 'model_no': this.serialNumbersdata.model.model_id,
                // "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
                // "mfg_date": this.serialNumbersdata.manufacture_date,
                "expiry_day": this.serialNumbersdata.expiry_day,

              });
              // this.Addnameprodinfo.controls["mfg_date"].setValue(this.serialNumbersdata.manufacture_date);
              this.Addnameprodinfo.controls['expiry_day'].setValue(this.serialNumbersdata.expiry_day);
              this.getsegment();
              // this.warranty_id=this.serialNumbersdata.warranty_type;
              // this.duration(this.serialNumbersdata.warranty_type.warranty_id);
            }
            else if (this.flag == 2) {
              this.mfgdatetype = 'Disable';
              // this.getproduct();
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              if (result.response.manufacture_date[0].manufacture_date) {
                var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }


              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);
              // this.Addnameprodinfo.controls["mfg_date"].setValue(this.serialNumbersdata.manufacture_date);
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.serialNumbersdata.product.forEach(element => {
                console.log(element, "element")
                this.product.push(element);
              });
              console.log(this.product, "this.product")
            }
            else if (this.flag == 3) {
              this.mfgdatetype = 'Disable';
              this.mfg3 = result.response.manufacture_date;
              if (result.response.manufacture_date) {
                var expdates = result.response.manufacture_date.split("-");
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }


              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);

              this.getsegment();
              this.getproduct();
            }
            // }

            this.chRef.detectChanges();
          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error')
            this.serial_error = result.response.message;
            // this.message = "please select the customer Name";
          }
          else {
            this.toastr.error("Something went wrong", 'Error')
            this.serial_error = "Something went wrong";
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    }
    if (this.Addbatterybank) {
      var serialno: any;
      console.log(this.Addbatterybank.value.serial_no);
      if (this.Addbatterybank.value.serial_no) {
        // this.mfgdatetype = 'Enable';
        serialno = this.Addbatterybank.value.serial_no;
        var serialnum_length = serialno.toString().length;
        console.log(serialnum_length);
      }
      else {
        serialno = "";
      }
      if (serialno) {
        var serial_number = { 'serial_no': serialno };
        var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
        this.ajax.postdata(url, serial_number).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {
            this.serial_error = '';
            this.flag = result.response.flag;
            if (this.flag == 0) {
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.warranty_type = [];
              this.product.push(this.serialNumbersdata.product);
              this.model_id.push(this.serialNumbersdata.model);
              this.warranty_type.push(this.serialNumbersdata.warranty_type);
              this.subproduct.push(this.serialNumbersdata.product_sub);
              this.products_id = this.serialNumbersdata.product.product_id
              this.subid = this.serialNumbersdata.product_sub.product_sub_id;
              this.mfg_date = this.serialNumbersdata.manufacture_date;
              console.log(this.serialNumbersdata);
              if (result.response.manufacture_date[0]) {
                var mrfdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Myear: number = +mrfdates[0];
                var Mmonth: number = +mrfdates[1];
                var Mday: number = +mrfdates[2];
              }

              const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
              this.Addbatterybank.controls["mfg_date"].setValue(MDate);
              this.Addnameprodinfo.controls["mfg_date"].setValue(MDate);
              this.promfgdatetype = 'Disable';
              if (result.response.manufacture_date[0]) {
                var expdates = result.response.manufacture_date[0].expiry_day.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }

              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addbatterybank.controls["expiry_day"].setValue(EDate);
              this.Addbatterybank.patchValue({
                'product_id': this.serialNumbersdata.product.product_id,
                'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
                'model_no': this.serialNumbersdata.model.model_id,
                "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
                "sys_ah": this.serialNumbersdata.sys_ah,
                "voltage": this.serialNumbersdata.voltage.toUpperCase(),
                // "mfg_date": this.serialNumbersdata.manufacture_date,
                // "expiry_day": this.serialNumbersdata.expiry_day,

              });
              this.Addnameprodinfo.controls["expiry_day"].setValue(EDate);
              this.Addnameprodinfo.patchValue({
                'product_id': this.serialNumbersdata.product.product_id,
                'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
                'model_no': this.serialNumbersdata.model.model_id,
                "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
                "sys_ah": this.serialNumbersdata.sys_ah,
                "voltage": this.serialNumbersdata.voltage.toUpperCase(),

              });
              this.getsegment();
              // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

              this.Addnameprodinfo.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)
              this.Addbatterybank.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)

            }
            else if (this.flag == 1) {
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.warranty_type = [];
              // this.segment = [];
              // this.application = [];
              this.product.push(this.serialNumbersdata.product);
              this.subproduct.push(this.serialNumbersdata.product_sub);
              this.products_id = this.serialNumbersdata.product.product_id
              this.subid = this.serialNumbersdata.product_sub.product_sub_id;
              this.serialNumbersdata.model.forEach(element => {
                this.model_id.push(element);
              });
              if (result.response.manufacture_date[0]) {
                var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }

              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addbatterybank.controls["mfg_date"].setValue(EDate);
              console.log(this.subid);
              this.Addbatterybank.patchValue({
                'product_id': this.products_id,
                'product_sub_id': this.subid,
                // "mfg_date": this.serialNumbersdata.manufacture_date,
                "expiry_day": this.serialNumbersdata.expiry_day,
                "sys_ah": '',
                "segment_id": this.Addbatterybank.value.segment_id,
                "application_id": this.Addbatterybank.value.application_id,
                "organization_name": '',
                "invoice_number": '',
                "invoice_date": '',
                "warranty_type_id": '',
                "contract_duration": '',



              });
              this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);

              this.Addnameprodinfo.patchValue({
                'product_id': this.products_id,
                'product_sub_id': this.subid,
                "expiry_day": this.serialNumbersdata.expiry_day,
                "sys_ah": '',
                "segment_id": this.Addnameprodinfo.value.segment_id,
                "application_id": this.Addnameprodinfo.value.application_id,
                "organization_name": '',
                "invoice_number": '',
                "invoice_date": '',
                "warranty_type_id": '',
                "contract_duration": '',



              });
              this.promfgdatetype = 'Disable';
              this.getsegment();

            }
            else if (this.flag == 2) {
              // this.getproduct();
              if (result.response.manufacture_date) {
                var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
                var Eyear: number = +expdates[0];
                var Emonth: number = +expdates[1];
                var Eday: number = +expdates[2];
              }


              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);
              this.Addbatterybank.controls["mfg_date"].setValue(EDate);
              this.promfgdatetype = 'Disable';
              this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
              this.product = [];
              this.subproduct = [];
              this.model_id = [];
              this.Addnameprodinfo.controls["product_id"].setValue(null);
              this.Addbatterybank.controls["product_id"].setValue(null);
              this.serialNumbersdata.product.forEach(element => {
                this.product.push(element);
              });
              console.log(this.product)
            }
            else if (this.flag == 3) {
              this.promfgdatetype = 'Disable';
              this.mfg3 = result.response.manufacture_date;
              console.log(result.response.manufacture_date)
              // if (result.response.manufacture_date) {
              var expdates = result.response.manufacture_date.split("-");
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
              // }


              const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              this.Addbatterybank.controls["mfg_date"].setValue(EDate);
              console.log(this.Addbatterybank.value.mfg_date)
              // this.product = [];
              // this.subproduct = [];
              // this.model_id = [];
              // this.mfg3=result.response.manufacture_date;
              // if (result.response.manufacture_date) {
              //   var expdates = result.response.manufacture_date.split("-");
              //   var Eyear: number = +expdates[0];
              //   var Emonth: number = +expdates[1];
              //   var Eday: number = +expdates[2];
              // }
              //     if (e.manufacture_date) {
              //       var mdates = this.ManufactureDate.split("-", 3);
              //       var Myear: number = +mdates[0];
              //       var Mmonth: number = +mdates[1];
              //       var Mday: number = +mdates[2];
              //     }

              // const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
              // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
              // this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);
              // this.mfgdatetype = 'Disable';
              // this.Addbatterybank.patchValue({
              //   "mfg_date": result.response.manufacture_date
              // });
              // this.Addbatterybank.value.mfg_date = result.response.manufacture_date; //storing the api response in the array

              this.getsegment();
              this.getproduct();
            }
            // }

            this.chRef.detectChanges();
          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error')

            this.serial_error = result.response.message;
          }
          else {
            this.toastr.error("Something went wrong", 'Error')
            this.serial_error = "";
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }

    }

  }
  extractSerialnumberbatt(serialno) {


    var serialno: any;
    console.log(this.Addbatterybank.value.serial_no);
    if (this.Addbatterybank.value.serial_no) {
      // this.mfgdatetype = 'Enable';
      serialno = this.Addbatterybank.value.serial_no;
      var serialnum_length = serialno.toString().length;
      console.log(serialnum_length);
    }
    else {
      serialno = "";
    }
    if (serialno) {
      this.Addbatterybank.patchValue({
        'product_id': '',
        'product_sub_id': '',
        'model_no': '',
        "warranty_type_id": '',
        "sys_ah": '',
        "voltage": '',
        "mfg_date": '',
        "expiry_day": '',

      });
      var serial_number = { 'serial_no': serialno };
      var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
      this.ajax.postdata(url, serial_number).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          console.log(this.site_details.priority, "this.site_details.priority")
          console.log(this.Addbatterybank.value.proioritys, "this.Addbatterybank.value.proioritys")
          this.Addbatterybank = new FormGroup({

            "product_id": new FormControl('', [Validators.required]),
            "product_sub_id": new FormControl('', [Validators.required]),
            "model_no": new FormControl('', [Validators.required]),
            "serial_no": new FormControl(this.Addbatterybank.value.serial_no),
            "invoice_date": new FormControl(''),
            "contract_duration": new FormControl(''),
            "battery_bank_id": new FormControl(this.Addbatterybank.value.battery_bank_id),
            "sys_ah": new FormControl('', [Validators.required]),
            "segment_id": new FormControl(this.Addbatterybank.value.segment_id, [Validators.required]),
            "application_id": new FormControl(this.Addbatterybank.value.application_id, [Validators.required]),
            "organization_name": new FormControl(this.Addbatterybank.value.organization_name, [Validators.required]),
            "invoice_number": new FormControl(this.Addbatterybank.value.invoice_number),
            "expiry_day": new FormControl(this.Addbatterybank.value.expiry_day),
            "warranty_type_id": new FormControl(this.Addbatterybank.value.warranty_type_id, [Validators.required]),
            "mfg_date": new FormControl(this.Addbatterybank.value.mfg_date),
            "voltage": new FormControl(this.Addbatterybank.value.voltage),
            "image": new FormControl(this.Addbatterybank.value.image),
            "cust_preference_date": new FormControl(this.Addbatterybank.value.cust_preference_date, [Validators.required]),
            "proioritys": new FormControl(this.site_details.priority, [Validators.required]),
            "call_category_id": new FormControl(this.Addbatterybank.value.call_category_id, [Validators.required]),
            "work_type_id": new FormControl(this.Addbatterybank.value.work_type_id, [Validators.required]),
            "desc": new FormControl(this.Addbatterybank.value.desc, [Validators.required, this.noWhitespace])
          });
          this.serial_error = '';
          this.flag = result.response.flag;
          if (this.flag == 0) {
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.warranty_type = [];
            this.product.push(this.serialNumbersdata.product);
            this.model_id.push(this.serialNumbersdata.model);
            this.warranty_type.push(this.serialNumbersdata.warranty_type);
            this.subproduct.push(this.serialNumbersdata.product_sub);
            this.products_id = this.serialNumbersdata.product.product_id
            this.subid = this.serialNumbersdata.product_sub.product_sub_id;
            this.mfg_date = this.serialNumbersdata.manufacture_date;
            console.log(this.serialNumbersdata);
            if (result.response.manufacture_date[0]) {
              var mrfdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Myear: number = +mrfdates[0];
              var Mmonth: number = +mrfdates[1];
              var Mday: number = +mrfdates[2];
            }

            const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
            this.Addbatterybank.controls["mfg_date"].setValue(MDate);
            // this.Addnameprodinfo.controls["mfg_date"].setValue(MDate);
            this.promfgdatetype = 'Disable';
            // alert(this.promfgdatetype)
            if (result.response.manufacture_date[0]) {
              var expdates = result.response.manufacture_date[0].expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            this.Addbatterybank.controls["expiry_day"].setValue(EDate);
            this.Addbatterybank.patchValue({
              'product_id': this.serialNumbersdata.product.product_id,
              'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
              'model_no': this.serialNumbersdata.model.model_id,
              "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
              "sys_ah": this.serialNumbersdata.sys_ah,
              "voltage": this.serialNumbersdata.voltage.toUpperCase(),
              // "mfg_date": this.serialNumbersdata.manufacture_date,
              // "expiry_day": this.serialNumbersdata.expiry_day,

            });
            // this.Addnameprodinfo.controls["expiry_day"].setValue(EDate);
            // this.Addnameprodinfo.patchValue({
            //   'product_id': this.serialNumbersdata.product.product_id,
            //   'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
            //   'model_no': this.serialNumbersdata.model.model_id,
            //   "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
            //   "sys_ah": this.serialNumbersdata.sys_ah,
            //   "voltage": this.serialNumbersdata.voltage,

            // });
            this.getsegment();
            // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

            this.Addnameprodinfo.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)
            this.Addbatterybank.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)

          }
          else if (this.flag == 1) {
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.warranty_type = [];
            // this.segment = [];
            // this.application = [];
            this.product.push(this.serialNumbersdata.product);
            this.subproduct.push(this.serialNumbersdata.product_sub);
            this.products_id = this.serialNumbersdata.product.product_id
            this.subid = this.serialNumbersdata.product_sub.product_sub_id;
            this.serialNumbersdata.model.forEach(element => {
              this.model_id.push(element);
            });
            if (result.response.manufacture_date[0]) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);

            console.log(this.subid);
            this.Addbatterybank.patchValue({
              'product_id': this.products_id,
              'product_sub_id': this.subid,
              'model_no': null,
              // "mfg_date": this.serialNumbersdata.manufacture_date,
              "expiry_day": this.serialNumbersdata.expiry_day,
              "sys_ah": '',
              "segment_id": this.Addbatterybank.value.segment_id,
              "application_id": this.Addbatterybank.value.application_id,
              "organization_name": '',
              "invoice_number": '',
              "invoice_date": '',
              "warranty_type_id": '',
              "contract_duration": '',



            });
            this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            // this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
            // this.Addbatterybank.controls['model_no'].setValue(null);
            // this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
            // this.Addbatterybank.controls['model_no'].setValue('');
            this.promfgdatetype = 'Disable';
            this.Addnameprodinfo.patchValue({
              'product_id': this.products_id,
              'product_sub_id': this.subid,
              "expiry_day": this.serialNumbersdata.expiry_day,
              "sys_ah": '',
              "segment_id": this.Addnameprodinfo.value.segment_id,
              "application_id": this.Addnameprodinfo.value.application_id,
              "organization_name": '',
              "invoice_number": '',
              "invoice_date": '',
              "warranty_type_id": '',
              "contract_duration": '',



            });

            this.getsegment();

          }
          else if (this.flag == 2) {
            // this.getproduct();
            if (result.response.manufacture_date) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            // this.Addnameprodinfo.controls["mfg_date"].setValue(EDate);
            this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            this.promfgdatetype = 'Disable';
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            // this.Addnameprodinfo.controls["product_id"].setValue(null);
            this.Addbatterybank.controls["product_id"].setValue(null);
            this.Addbatterybank.controls['product_sub_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['product_sub_id'].setValue(null);
            this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['model_no'].setValue(null);
            this.serialNumbersdata.product.forEach(element => {
              this.product.push(element);
            });
            console.log(this.product)
          }
          else if (this.flag == 3) {
            this.promfgdatetype = 'Disable';
            this.mfg3 = result.response.manufacture_date;
            console.log(result.response.manufacture_date)
            // if (result.response.manufacture_date) {
            var expdates = result.response.manufacture_date.split("-");
            var Eyear: number = +expdates[0];
            var Emonth: number = +expdates[1];
            var Eday: number = +expdates[2];
            // }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            console.log(this.Addbatterybank.value.mfg_date)
            this.Addbatterybank.controls['product_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['product_id'].setValue(null);
            this.Addbatterybank.controls['product_sub_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['product_sub_id'].setValue(null);
            this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['model_no'].setValue(null);
            this.getsegment();
            this.getproduct();
          }
          // }

          this.chRef.detectChanges();
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')

          this.serial_error = result.response.message;
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
          this.serial_error = "";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }



  }
  changecuspreferdate(events: any) {
    // console.log(events);
    // console.log(this.Addnameprodinfo.value.cust_preference_date)
    this.Addnameprodinfo.controls['cust_preference_date'].setValue(events)
    // console.log(this.Addnameprodinfo.value.cust_preference_date)
  }
  changeExpiry(event: any) {
    console.log(event);
    console.log(this.Addnameprodinfo.value.mfg_date)

    this.Addnameprodinfo = new FormGroup({

      "product_id": new FormControl(this.Addnameprodinfo.value.product_id, [Validators.required]),
      "product_sub_id": new FormControl(this.Addnameprodinfo.value.product_sub_id, [Validators.required]),
      "model_no": new FormControl(this.Addnameprodinfo.value.model_no, [Validators.required]),
      "serial_no": new FormControl(this.Addnameprodinfo.value.serial_no),
      "invoice_date": new FormControl(this.Addnameprodinfo.value.invoice_date),
      "contract_duration": new FormControl(this.Addnameprodinfo.value.contract_duration),
      "battery_bank_id": new FormControl(this.Addnameprodinfo.value.battery_bank_id),
      "sys_ah": new FormControl(this.Addnameprodinfo.value.sys_ah, [Validators.required]),
      "segment_id": new FormControl(this.Addnameprodinfo.value.segment_id, [Validators.required]),
      "application_id": new FormControl(this.Addnameprodinfo.value.application_id, [Validators.required]),
      "organization_name": new FormControl(this.Addnameprodinfo.value.organization_name, [Validators.required]),
      "invoice_number": new FormControl(this.Addnameprodinfo.value.invoice_number),
      "expiry_day": new FormControl(this.Addnameprodinfo.value.expiry_day),
      "warranty_type_id": new FormControl(this.Addnameprodinfo.value.warranty_type_id, [Validators.required]),
      "mfg_date": new FormControl(this.Addnameprodinfo.value.mfg_date),
      "voltage": new FormControl(this.Addnameprodinfo.value.voltage),
      "image": new FormControl(this.Addnameprodinfo.value.image),
      "cust_preference_date": new FormControl(this.Addnameprodinfo.value.cust_preference_date, [Validators.required]),
      "proioritys": new FormControl(this.Addnameprodinfo.value.proioritys, [Validators.required]),
      "call_category_id": new FormControl(this.Addnameprodinfo.value.call_category_id, [Validators.required]),
      "work_type_id": new FormControl(this.Addnameprodinfo.value.work_type_id, [Validators.required]),
      "desc": new FormControl(this.Addnameprodinfo.value.desc, [Validators.required, this.noWhitespace])
    });

    this.getproduct();


  }
  changeExpiryproduct(event: any) {
    console.log(event);
    console.log(this.Addbatterybank.value.mfg_date)
    console.log(this.Addbatterybank.value)
    this.Addbatterybank = new FormGroup({

      "product_id": new FormControl(this.Addbatterybank.value.product_id, [Validators.required]),
      "product_sub_id": new FormControl(this.Addbatterybank.value.product_sub_id, [Validators.required]),
      "model_no": new FormControl(this.Addbatterybank.value.model_no, [Validators.required]),
      "serial_no": new FormControl(this.Addbatterybank.value.serial_no),
      "invoice_date": new FormControl(this.Addbatterybank.value.invoice_date),
      "contract_duration": new FormControl(this.Addbatterybank.value.contract_duration),
      "battery_bank_id": new FormControl(this.Addbatterybank.value.battery_bank_id),
      "sys_ah": new FormControl(this.Addbatterybank.value.sys_ah, [Validators.required]),
      "segment_id": new FormControl(this.Addbatterybank.value.segment_id, [Validators.required]),
      "application_id": new FormControl(this.Addbatterybank.value.application_id, [Validators.required]),
      "organization_name": new FormControl(this.Addbatterybank.value.organization_name, [Validators.required]),
      "invoice_number": new FormControl(this.Addbatterybank.value.invoice_number),
      "expiry_day": new FormControl(this.Addbatterybank.value.expiry_day),
      "warranty_type_id": new FormControl(this.Addbatterybank.value.warranty_type_id, [Validators.required]),
      "mfg_date": new FormControl(this.Addbatterybank.value.mfg_date),
      "voltage": new FormControl(this.Addbatterybank.value.voltage),
      "image": new FormControl(this.Addbatterybank.value.image),
      "cust_preference_date": new FormControl(this.Addbatterybank.value.cust_preference_date, [Validators.required]),
      "proioritys": new FormControl(this.Addbatterybank.value.proioritys, [Validators.required]),
      "call_category_id": new FormControl(this.Addbatterybank.value.call_category_id, [Validators.required]),
      "work_type_id": new FormControl(1, [Validators.required]),
      "desc": new FormControl(this.Addbatterybank.value.desc, [Validators.required, this.noWhitespace])
    });



  }
  onclick_customer(event: MatRadioChange, addsiteid) {
    this.mfgdatetype = 'Enable';
    this.name_disabled = false;
    this.value_disabled = false;
    this.code = event;
    console.log(event);
    this.get_servicegroup();
    this.get_call_category();
    this.getamc();
    this.Addnameinfo.reset();
    this.Addnameprodinfo = new FormGroup({

      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model, [Validators.required]),
      "proioritys": new FormControl("", [Validators.required]),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl("", [Validators.required]),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])
    });
    this.Addnameprodinfo.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_id'].setValue(null);
    this.Addnameprodinfo.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_sub_id'].setValue(null);
    this.Addnameprodinfo.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['model_no'].setValue(null);
    this.Addnameprodinfo.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['warranty_type_id'].setValue(null);
    this.Addnameprodinfo.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['segment_id'].setValue(null);
    this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['application_id'].setValue(null);
    this.Addnameprodinfo.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['work_type_id'].setValue(null);
    this.Addnameprodinfo.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['call_category_id'].setValue(null);
    this.Addnameprodinfo.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['proioritys'].setValue(null);
    // this.AddSiteInfo.reset();
    // this.AddSiteInfo.controls['customer_type'].setValue(this.RaiseTicketCusInfo.value.customer_type)
    this.Addnameinfo.controls['customer_type'].setValue(this.RaiseTicketCusInfo.value.customer_type)
    this.customerSelect = { 'customer_code': this.code };
    var url = 'customer_check/'; // api url for getting the details                                 // api url for getting the details
    this.ajax.postdata(url, this.customerSelect).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customer_info = result.response.customer_details; //storing the api response in the array
        this.chRef.detectChanges();
        this.getcountry();

        this.Addnameinfo.patchValue({
          'customer_code': this.customer_info.customer_code,
          'customer_name': this.customer_info.customer_name,
          'email_id': this.customer_info.email_id,
          'contact_number': this.customer_info.contact_number,
          'alternate_number': this.customer_info.alternate_number,
          'sap_reference_number': this.customer_info.sap_reference_number,
          'battery_bank_id': '',
          'customer_id': this.cus_cus_id,
          'country_id': 101,
        });
        this.onChangeCountry(101);
        this.customerID = [];
        this.save_continue = false;
        this.product = [];
        this.subproduct = [];
        this.model_id = [];
        this.warranty_id = [];
        this.segment = [];
        this.application = [];
        this.labelwarranty = "Select Warranty type";
        this.call = [];
        this.getsegment();
        this.getproduct();
        this.activeId = "tab-selectbyida";
        this.modalService.open(addsiteid, {
          size: 'lg', //specifies the size of the dialog box
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
        this.message = "";
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.data, 'Error')

        this.message = "please select the customer Name";
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.message = "";
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });

  }
  getCus() {
    this.name_disabled = true;
    this.title = "Add Site Id";
    console.log(this.Addnameinfo.value)
    this.radio_val = 1;
    console.log(this.radio_val);
    this.getsegment();
    this.getcountry();
    this.getproduct();
    this.get_servicegroup();
    this.get_call_category();
    this.getamc();
    this.message = "";
    console.log(this.customerSelect)
    var data = { "customer_name": this.customerSelect };
    var url = 'get_customer_code/'; // api url for getting the details                                 // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.cus_id = result.response.data; //storing the api response in the array
        this.message = "";
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.data, 'Error')
        this.message = "please select the customer Name";
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.message = "";
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  onchangeadd(event: MatRadioChange, newadd) {
    this.mfgdatetype = 'Enable';
    console.log(this.model)
    this.Addnameprodinfo = new FormGroup({

      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
      "model_no": new FormControl(''),
      "serial_no": new FormControl(''),
      "invoice_date": new FormControl(''),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl(''),
      "segment_id": new FormControl(''),
      "application_id": new FormControl(''),
      "organization_name": new FormControl(''),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(null),
      "warranty_type_id": new FormControl(''),
      "mfg_date": new FormControl(null),
      "voltage": new FormControl(""),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.model, [Validators.required]),
      "proioritys": new FormControl("", [Validators.required]),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl("", [Validators.required]),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])
    });
    this.Addnameprodinfo.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_id'].setValue(null);
    this.Addnameprodinfo.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['product_sub_id'].setValue(null);
    this.Addnameprodinfo.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['model_no'].setValue(null);
    this.Addnameprodinfo.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['warranty_type_id'].setValue(null);
    this.Addnameprodinfo.controls['segment_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['segment_id'].setValue(null);
    this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['application_id'].setValue(null);
    this.Addnameprodinfo.controls['work_type_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['work_type_id'].setValue(null);
    this.Addnameprodinfo.controls['call_category_id'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['call_category_id'].setValue(null);
    this.Addnameprodinfo.controls['proioritys'].setErrors({ 'incorrect': true });
    this.Addnameprodinfo.controls['proioritys'].setValue(null);
    console.log(this.Addnameprodinfo.value.cust_preference_date)
    console.log(this.cus_type)
    console.log(this.RaiseTicketCusInfo.value.customer_type)
    console.log(this.Addnameinfo.value.customer_type)
    // this.RaiseTicketCusInfo.controls['customer_type'].setValue(this.cus_type)
    // this.Addnameinfo.controls['customer_type'].setValue(this.cus_type)
    this.Addnameinfo.reset();
    this.AddSiteInfo.reset();

    this.Addnameinfo.controls['customer_type'].setValue(this.RaiseTicketCusInfo.value.customer_type)
    console.log(event)
    this.batteryBankSample = [];
    this.message = '';
    this.contactnum_error = '';
    this.email_error = '';
    var radio_value = event.value;
    if (radio_value == 1) {
      // this.AddSiteInfo.controls['customer_type'].setValue(this.RaiseTicketCusInfo.value.customer_type)
      this.value_disabled = true;
      this.title = "Add Site Id";
      console.log(this.Addnameinfo.value)
      this.radio_val = 1;
      this.message = "";
      console.log(this.customerSelect)
      var data = this.customerSelect;
      var url = 'get_customer_code/'; // api url for getting the details                                 // api url for getting the details
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.cus_id = result.response.data; //storing the api response in the array
          this.message = "";
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.data, 'Error')
          this.message = "please select the customer Name";
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
          this.message = "";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else if (radio_value == 2) {
      this.value_disabled = true;
      this.name_disabled = false;
      this.radio_val = 2;
      // this.customerSelect=[];
      this.title = "Add Customer Id";
      this.add = false;
      this.save_continue = false;
      this.product = [];
      this.subproduct = [];
      this.model_id = [];
      this.warranty_id = [];
      this.segment = [];
      this.application = [];
      this.labelwarranty = "Select Warranty type";
      this.call = [];
      this.getsegment();
      this.getproduct();
      this.getcountry();
      // this.Addnameprodinfo.reset();
      // this.Addnameprodinfo.controls['call_category_id'].setValue('');
      // this.Addnameprodinfo.controls['desc'].setValue('');
      // this.get_call_category();
      // this.Addnameinfo.reset();
      // console.log(this.customerSelect)
      // this.customerSelect=[];
      var parmas = { "customer_code": this.sitedetails[0].customer_code, 'site_id': this.sitedetails[0].site_id, "flag": 1 }
      var url = 'get_customer_contract_details/?'; // api url for getting the details                                 // api url for getting the details
      this.ajax.postdata(url, parmas).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          var cus_type = this.Addnameinfo.value.customer_type
          this.Addnameinfo.reset();
          this.Addnameinfo.controls["customer_type"].setValue(cus_type);
          this.Addnameinfo.controls["customer_name"].setValue(result.response.customer_details.customer_name);
          this.Addnameinfo.controls["country_id"].setValue(101);

          this.onChangeCountry(101);

          console.log(this.Addnameinfo.value.customer_name);
          this.activeId = "tab-selectbyida";
          this.modalService.open(newadd, {
            size: 'lg', //specifies the size of the dialog box
            backdrop: 'static',              //popup outside click by Sowndarya
            keyboard: false
          });
          // this.cus_id = result.response.data; //storing the api response in the array
          this.message = "";
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.data, 'Error')
          this.message = "please select the customer Name";
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
          this.message = "";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
      // console.log(this.customerSelect)
      // console.log(this.customerSelect)

    }
  }
  //get work type
  get_servicegroup() {
    var url = 'get_servicegroup/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.work_type = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }

  //get call category
  get_call_category() {
    var url = 'get_call_category/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.call_category = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }

  //get all the amc contract type
  getamc() {
    var url = 'get_amc_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.RequestOptions == "400") {
        this.contrac_type = result.response.data;
      } else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  getapplication(segment_id) {
    console.log(segment_id)
    console.log(this.Addnameprodinfo.value.segment_id)
    if (segment_id != null || segment_id != '') {
      var data = { "segment_id": segment_id };// storing the form group value
      var url = 'get_application/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.application = result.response.data;
          this.chRef.detectChanges();
          if (this.Addnameprodinfo) {
            this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addnameprodinfo.controls['application_id'].setValue(null);
          }
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          if (this.Addnameprodinfo) {
            this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addnameprodinfo.controls['application_id'].setValue(null);
          }
        }
        else {
          if (this.Addnameprodinfo) {
            this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addnameprodinfo.controls['application_id'].setValue(null);
          }
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        if (this.Addnameprodinfo) {
          this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
          this.Addnameprodinfo.controls['application_id'].setValue(null);
        }
        console.log(err);                                             //prints if it encounters an error
      });

    }

    else {
      // alert("ss")
      this.application = [];
      this.Addnameprodinfo.controls['segment_id'].setErrors({ 'incorrect': true });
      this.Addnameprodinfo.controls['application_id'].setValue(null);
      this.Addnameprodinfo.controls['application_id'].setErrors({ 'incorrect': true });
    }


  }
  getmodel(id) {
    this.model_id = [];
    this.subid = id;
    if (this.Addnameprodinfo) {
      var data = { "product_id": this.products_id, "product_sub_id": this.subid };// storing the form group value
      var url = 'get_models/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.model_id = result.response.data;
          this.Addnameprodinfo.controls['model_no'].setValue("null")
          this.chRef.detectChanges();
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });

    }
    if (this.AddSiteInfo) {
      var data = { "product_id": this.products_id, "product_sub_id": this.subid };// storing the form group value
      var url = 'get_models/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.model_id = result.response.data;
          this.AddSiteInfo.controls['model_no'].setValue("null")
          this.chRef.detectChanges();
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });

    }
    if (this.AddProductInfo) {
      var data = { "product_id": this.products_id, "product_sub_id": this.subid };// storing the form group value
      var url = 'get_models/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.model_id = result.response.data;
          this.chRef.detectChanges();
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });

    }

  }
  getwarrantyproduct(id) {
    if (this.Addnameprodinfo.value.mfg_date != '') {
      var year = this.Addnameprodinfo.value.mfg_date.year;
      var month = this.Addnameprodinfo.value.mfg_date.month;
      var day = this.Addnameprodinfo.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
    }
    else {
      mfg_date = "";
    }
    var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
    var url = 'get_warranty_type/'
    if (this.Addnameprodinfo.value.mfg_date != null) {
      //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.warranty_id = result.response.data;

          this.chRef.detectChanges();

          this.warranty_id.forEach(e => {
            if (e.expiry_day) {
              var expdates = e.expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            this.Addnameprodinfo.controls["expiry_day"].setValue(EDate);

            this.Addnameprodinfo.controls["voltage"].setValue(e.voltage);
            this.Addnameprodinfo.controls["sys_ah"].setValue(e.sys_ah);
            this.warranty_type = e.warranty_type;
            this.Addnameprodinfo.controls["warranty_type_id"].setValue(e.warranty_type[0].warranty_id);
            this.Addnameprodinfo.controls["contract_duration"].setValue(e.warranty_type[0].warranty_duration);

          })
          // if(this.warranty_id.length == 1){
          //   // this.warranty_type_flag = 0;
          // }
          // else{
          //   this.warranty_type = e.warranty_type;
          //   // this.warranty_type_flag = 1;
          // }
          // // console.log(this.warranty_type_flag);
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });

    }
    else {

    }
  }
  onchangeMdate(id) {
    console.log(this.AddSiteInfo.value);
    // this.getwarranty()
  }
  onChangeMFG(event) {
    console.log(event);
    //  this.Addbatterybank.value.mfg_date.valueChanges.subscribe(selectedValue => {
    //   console.log('firstname value changed')
    //   console.log(selectedValue)                              //latest value of firstname
    //   console.log(this.Addbatterybank.get("mfg_date").value)   //latest value of firstname
    // })
    console.log(this.Addbatterybank.value);
    this.onchangetest(this.Addbatterybank.value.model_no);
    //  this.getwarranty(this.Addbatterybank.value.model_no.model_id)
  }
  onchangetest(id) {
    console.log('Test');
    if (this.Addbatterybank.value.mfg_date) {
      var year = this.Addbatterybank.value.mfg_date.year;
      var month = this.Addbatterybank.value.mfg_date.month;
      var day = this.Addbatterybank.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
    }
    else {
      var mfg_date = ''
    }
    var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
    var url = 'get_warranty_type/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.warranty_id = result.response.data;
        this.warranty_id.forEach(e => {
          console.log(e.expiry_day);
          if (e.expiry_day) {
            var expdates = e.expiry_day.split("-", 3);
            var Eyear: number = +expdates[0];
            var Emonth: number = +expdates[1];
            var Eday: number = +expdates[2];
          }
          //     if (e.manufacture_date) {
          //       var mdates = this.ManufactureDate.split("-", 3);
          //       var Myear: number = +mdates[0];
          //       var Mmonth: number = +mdates[1];
          //       var Mday: number = +mdates[2];
          //     }

          const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
          //     const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

          this.Addbatterybank.controls["expiry_day"].setValue(EDate);
          //     this.Addbatterybank.controls["mfg_date"].setValue(MDate);

          //     console.log(EDate);
          this.Addbatterybank.controls["voltage"].setValue(e.voltage);
          this.Addbatterybank.controls["sys_ah"].setValue(e.sys_ah);
          this.warranty_type = e.warranty_type;
          this.labelwarranty = this.warranty_type[0].warranty_duration
          this.Addbatterybank.controls["warranty_type_id"].setValue(this.warranty_type[0].warranty_id);
          this.Addbatterybank.controls["contract_duration"].setValue(this.warranty_type[0].warranty_duration);

        })
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  getwarranty(id) {
    if (this.Addbatterybank.value.mfg_date) {
      var year = this.Addbatterybank.value.mfg_date.year;
      var month = this.Addbatterybank.value.mfg_date.month;
      var day = this.Addbatterybank.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
    }

    else {
      var mfg_date = ''
    }
    var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
    var url = 'get_warranty_type/'                                         //api url of remove license
    if (this.Addbatterybank.value.mfg_date != null || this.Addnameprodinfo.value.mfg_date != null) {
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.warranty_id = result.response.data;

          this.chRef.detectChanges();

          this.warranty_id.forEach(e => {
            if (e.expiry_day) {
              var expdates = e.expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }
            //     if (e.manufacture_date) {
            //       var mdates = this.ManufactureDate.split("-", 3);
            //       var Myear: number = +mdates[0];
            //       var Mmonth: number = +mdates[1];
            //       var Mday: number = +mdates[2];
            //     }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);

            this.warranty_type = e.warranty_type;

            // if (this.Addbatterybank) {
            this.Addbatterybank.controls["expiry_day"].setValue(EDate);

            this.Addbatterybank.controls["voltage"].setValue(e.voltage);
            this.Addbatterybank.controls["sys_ah"].setValue(e.sys_ah);

          })
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  //get amc duration based on amc id
  onchangeContractType(amc_id) {
    this.contrac_type.forEach(element => {
      if (element.amc_id == amc_id) {
        this.NewCustomerProdInfo.controls['duration'].setValue(element.duration)
      }
    });
  }
  //get amc duration based on amc id
  onchangeContractType1(amc_id, index) {
    var set_duration = this.t.value
    set_duration.forEach(element1 => {
      this.contrac_type.forEach(element => {
        if (element1.amc_id == element.amc_id) {
          element1.contract_duration = element.duration
          this.duration_values = element.duration
        }
        // else {
        // }
      });
    });
  }
  //-------------------------------------Function for image upload------------------------------------//





  index: any;
  //onchange function for getting the customer code
  passcustomercode(customer_code) {
    this.con_number = ''
    this.customer_code = customer_code
  }
  //onchange function for getting the contact number
  passcontact_no(contact_number) {
    this.customer_code = '';
    this.contact_number = contact_number
    this.con_number = contact_number
  }
  //function for get all customer and contract details based on contact or customer
  get_customer_contract_details(contact_customer_value, type_value: any, raise_ticket) {
    console.log(type_value, "type_value")
    this.serial_error = '';
    this.batteryBankSample = [];
    this.country = [];
    this.product = [];
    this.alternate = '';
    this.contact = '';
    this.contract_details = null;                 //empty the contract details
    this.activeId = "tab-selectbyid1";
    if (type_value == 3) {
      // console.log(contact_customer_value)
      // if (contact_customer_value == '' || contact_customer_value == undefined || contact_customer_value == null) {
      // this.toastr.warning("Please select the battery Bank Id", "Warning");
      // }
      // else if (contact_customer_value != '' || contact_customer_value != undefined || contact_customer_value != null) {
      console.log(this.bbSelect);
      if (this.bbSelect == undefined || this.bbSelect == null || this.bbSelect == '') {
        this.toastr.warning('Please select battery bank', "Warning")
      }
      else if (this.bbSelect != undefined || this.bbSelect != null || this.bbSelect != '') {
        this.Addnameprodinfo.patchValue({
          'battery_bank_id': this.bbSelect.battery_bank_id,
        });
        this.BatteryBankId = this.bbSelect.battery_bank_id;
        if (this.BatteryBankId == null) {
          this.BatteryBankId = "";
        }
        if (this.role_id != '7') {
          console.log("not an customer login")
          parmas = { 'battery_bank_id': this.bbSelect.battery_bank_id, "flag": 1 };
        }
        else {
          console.log("customer logged in")
          parmas = { 'customer_code': this.logged_customer_code, 'battery_bank_id': this.bbSelect.battery_bank_id, "flag": 2 };
        }
        // this.productdata = []; 
        var url = 'get_customer_contract_details/?';
        // this.overlayRef.attach(this.LoaderComponentPortal);
        // api url for getting the customer and contract details
        this.ajax.postdata(url, parmas).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.error = '';
            this.site_details = [];
            this.customer_details = result.response.customer_details;
            this.site_details = result.response.site_details;
            this.segment_value = this.site_details.segment_id;
            this.application_value = this.site_details.application_id;
            console.log(this.segment_value, "this.segment_value")
            this.chRef.detectChanges();
            this.contact_number = this.customer_details.contact_number;
            this.customer_code = this.customer_details.customer_code;
            this.batteryData = result.response.battery_bank_array;
            if (this.batteryData.length == 1) {
              this.showTable = false;
              this.getSerialNmber(this.batteryData[0]);
            }
            this.productdata = [];
            this.site_details.serial_no_details.forEach(obj => {
              // var date = obj.expiry_date.split("-", 3);
              // var year = date[0];
              // var month = date[1];
              // var day = date[2];
              // obj.expiry_date = day + "-" + month + "-" + year;
              this.productdata.push(obj);
            })
            console.log(this.productdata);
            if (this.batteryData.length == 1) {
              this.battery_data = 'Enable';
              this.showTable = true;
            }
            this.isShow = false;
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': this.batteryData,
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_id,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            if (this.site_details.priority == 'P1') {
              this.setPriority = "P1"
              this.RaiseCall.value.proioritys = 'P1';
              this.RaiseCall.controls["proioritys"].setValue('P1');
              // this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);
              // this.RaiseCall.patchValue({
              //   'proioritys': this.site_details.priority,
              // })
            }
            else {
              this.setPriority = this.site_details.priority
              this.RaiseCall.patchValue({
                'proioritys': this.site_details.priority,
              })
            }
            // this.RaiseCall.patchValue({
            //   'proioritys': this.site_details.priority,
            // })
            // this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);

            console.log(this.site_details.priority);
            this.Addnameinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': '',
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_id,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            this.getcountry();
            this.onChangeCountryas(this.site_details.country.country_id);
            this.onChangeStateas(this.site_details.state.state_id);
            this.onChangecityas(this.site_details.city.city_id);
            // this.overlayRef.detach();

          }
          else if (result.response.response_code == "404") {
            this.customer_details = result.response.customer_details;
            var contract_detail = result.response.contract_details;
            if (contract_detail.length == '0') {
              this.error = "There is no Contract for this customer";
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': '',
                'state_id': '',
                'city_id': '',
                'location_id': ''
              })
              this.overlayRef.detach();

            }
            else {
              this.error = ''
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
              })
              this.overlayRef.detach();
              this.chRef.detectChanges();
            }
            this.toastr.error(result.response.message, "Error");
            // this.overlayRef.detach();
          }
          else if (result.response.response_code == "400") {
            // this.raise_ticket_reset();
            this.toastr.error(result.response.message, "Error");
            // var default_radiovalue = '1';
            // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            // this.radiochange_value = 1;

            // this.overlayRef.detach();
            this.customer_code = '';
            // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
            this.getcountry();
            var today = new Date();
            var today_date = this.calendar.getToday()
            var year = today_date.year;
            var month = today_date.month;
            var day = today_date.day;
            this.config.maxDate = {
              day: day,
              month: month,
              year: year,
            };


          }
          else if (result.response.response_code == "500") {
            this.overlayRef.detach();
            // this.error = result.response.message
            this.toastr.error(result.response.message, "Error");
          }
          else {
            // this.overlayRef.detach();
            this.error = "Something went wrong";
            this.toastr.error(this.error, "Error");
          }
          (err) => {
            // this.overlayRef.detach();
            console.log(err); //prints if it encounters an error
          }
        });
      }

      // }

      else {
        this.toastr.warning("Please select the battery Bank Id", "Warning");
      }
    }
    else if (type_value == 1) {
      this.customer_code = '';
      if (type_value == 1 && this.contact_number != undefined) {
        this.contact_no = this.contact_number.length;
      }
      else {
        this.contact_no = 10;
      }
      if (this.contact_number == '' || this.contact_number == null || this.contact_no < 10) {

      }
      else {
        if ((this.customer_code === undefined && type_value == 1) || this.customer_code == '' || this.customer_code == null) {
          var contact_number = this.contact_number;
          var id = "contact_number";
          var parameter = contact_number;
        }
      }
    }
    else {
      if ((this.contact_number === undefined && type_value == 2) || (type_value == 2)) {
        if (this.customer_code !== '' && type_value !== '') {
          var customer_code = this.customer_code;
        }

        var id = "customer_code";
        var parameter = customer_code;
      }
    }
    var parmas = {};
    console.log(this.siteSelect);
    if (type_value == 2) {
      console.log(this.siteSelect.length);

      console.log(this.siteSelect.customer_site)
      if (this.siteSelect.length == 0) {

        this.toastr.warning("Please select customer id and site id");
      }
      else {
        var sites = this.siteSelect.customer_site;
        var splitted = sites.split("-", 3);
        console.log(splitted);
        var customer_code = splitted[0];
        var site_id = splitted[1];
        parmas = { 'customer_code': customer_code, 'site_id': site_id, "flag": 1 }
        var url = 'get_customer_contract_details/?';
        this.overlayRef.attach(this.LoaderComponentPortal);
        // api url for getting the customer and contract details
        this.ajax.postdata(url, parmas).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.error = '';
            this.site_details = [];
            this.customer_details = result.response.customer_details;
            this.site_details = result.response.site_details;
            this.segment_value = this.site_details.segment_id;
            this.application_value = this.site_details.application_id;
            console.log(this.segment_value, "this.segment_value")
            this.chRef.detectChanges();
            this.contact_number = this.customer_details.contact_number;
            this.customer_code = this.customer_details.customer_code;
            this.batteryData = result.response.battery_bank_array;
            if (this.batteryData.length == 1) {
              this.showTable = false;
              this.getSerialNmber(this.batteryData[0]);
            }
            this.productdata = [];
            this.site_details.serial_no_details.forEach(obj => {
              // var date = obj.expiry_date.split("-", 3);
              // var year = date[0];
              // var month = date[1];
              // var day = date[2];
              // obj.expiry_date = day + "-" + month + "-" + year;
              this.productdata.push(obj);
            })
            console.log(this.productdata);
            if (this.batteryData.length == 1) {
              this.battery_data = 'Enable';
              this.showTable = true;
            }
            this.isShow = false;
            if (this.site_details.priority == 'P1') {
              this.setPriority = "P1"
            }
            else {
              this.setPriority = this.site_details.priority
            }
            this.RaiseCall.patchValue({
              'proioritys': this.site_details.priority,

            })
            this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);
            console.log(this.site_details.priority);
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': this.batteryData,
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_name,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            this.Addnameinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': '',
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_id,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            this.getcountry();
            this.onChangeCountryas(this.site_details.country.country_id);
            this.onChangeStateas(this.site_details.state.state_id);
            this.onChangecityas(this.site_details.city.city_id);
            this.overlayRef.detach();

          }
          else if (result.response.response_code == "404") {
            this.customer_details = result.response.customer_details;
            var contract_detail = result.response.contract_details;
            if (contract_detail.length == '0') {
              this.error = "There is no Contract for this customer";
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': '',
                'state_id': '',
                'city_id': '',
                'location_id': ''
              })
              this.overlayRef.detach();

            }
            else {
              this.error = ''
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
              })
              this.overlayRef.detach();
              this.chRef.detectChanges();
            }
          }
          else if (result.response.response_code == "400") {
            // this.raise_ticket_reset();
            this.toastr.error(result.response.message, "Error");
            // var default_radiovalue = '1';
            // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            // this.radiochange_value = 1;

            this.overlayRef.detach();
            this.customer_code = '';
            // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
            this.getcountry();
            var today = new Date();
            var today_date = this.calendar.getToday()
            var year = today_date.year;
            var month = today_date.month;
            var day = today_date.day;
            this.config.maxDate = {
              day: day,
              month: month,
              year: year,
            };
            this.toastr.warning("No Data available, Please create new");

          }
          else if (result.response.response_code == "500") {
            this.overlayRef.detach();
            // this.error = result.response.message
            this.toastr.error(result.response.message, "Error");
          }
          else {
            this.overlayRef.detach();
            this.error = "Something went wrong"
          }
          (err) => {
            this.overlayRef.detach();
            console.log(err); //prints if it encounters an error
          }
        });
      }

    }
    else if (type_value == 1) {
      console.log(this.siteSelect.length);

      console.log(this.siteSelect.customer_site)
      if (this.numberSelect) {
        if (this.numberSelect.length == 0) {

          this.toastr.warning("Please select customer number and customer id");
        }
        else {
          var sites = this.numberSelect.contact_number;
          //     this.cus_cus_id = customer_id;
          // this.cus_siteId = site_id;
          console.log(sites);
          // var splitted = sites.split("-", 3);
          // console.log(splitted);
          // var customer_code = splitted[0];
          // var site_id = splitted[1];
          parmas = { 'customer_code': this.cus_cus_id, 'site_id': this.cus_siteId, "flag": 1 }
          var url = 'get_customer_contract_details/?';
          this.overlayRef.attach(this.LoaderComponentPortal);
          console.log(parmas);

          // api url for getting the customer and contract details
          this.ajax.postdata(url, parmas).subscribe((result) => {
            if (result.response.response_code == "200") {
              this.error = '';
              this.site_details = [];
              this.customer_details = result.response.customer_details;
              this.site_details = result.response.site_details;
              this.segment_value = this.site_details.segment_id;
              this.application_value = this.site_details.application_id;
              console.log(this.segment_value, "this.segment_value")
              this.chRef.detectChanges();
              this.contact_number = this.customer_details.contact_number;
              this.customer_code = this.customer_details.customer_code;
              this.batteryData = result.response.battery_bank_array;
              if (this.batteryData.length == 1) {
                this.showTable = false;
                this.getSerialNmber(this.batteryData[0]);
              }
              this.productdata = [];
              this.site_details.serial_no_details.forEach(obj => {
                // var date = obj.expiry_date.split("-", 3);
                // var year = date[0];
                // var month = date[1];
                // var day = date[2];
                // obj.expiry_date = day + "-" + month + "-" + year;
                this.productdata.push(obj);
              })
              console.log(this.productdata);
              if (this.batteryData.length == 1) {
                this.battery_data = 'Enable';
                this.showTable = true;
              }
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,
                'battery_bank_id': this.batteryData,
                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_id,
                'state_id': this.site_details.state.state_id,
                'city_id': this.site_details.city.city_id,
                'location_id': this.site_details.location.location_name,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              })
              this.Addnamesiteinfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,

                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_id,
                'state_id': this.site_details.state.state_id,
                'city_id': this.site_details.city.city_id,
                'location_id': this.site_details.location.location_id,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              });
              this.Addbatterybank.patchValue({

                'battery_bank_id': this.site_details.battery_bank_id,

              })
              this.getcountry();
              this.onChangeCountryas(this.site_details.country.country_id);
              this.onChangeStateas(this.site_details.state.state_id);
              this.onChangecityas(this.site_details.city.city_id);
              this.overlayRef.detach();

            }
            else if (result.response.response_code == "404") {
              this.customer_details = result.response.customer_details;
              var contract_detail = result.response.contract_details;
              if (contract_detail.length == '0') {
                this.error = "There is no Contract for this customer";
                this.RaiseTicketCusInfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                  'plot_number': '',
                  'street': '',
                  'landmark': '',
                  'post_code': '',
                  'country_id': '',
                  'state_id': '',
                  'city_id': '',
                  'location_id': ''
                })
                this.overlayRef.detach();

              }
              else {
                this.error = ''
                this.isShow = false;
                this.RaiseTicketCusInfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                })
                this.overlayRef.detach();
                this.chRef.detectChanges();
              }
              this.toastr.error(result.response.message, "Error");
            }
            else if (result.response.response_code == "400") {
              // this.raise_ticket_reset();
              // var default_radiovalue = '1';
              // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
              // this.radiochange_value = 1;

              this.overlayRef.detach();
              this.customer_code = '';
              // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
              this.getcountry();
              var today = new Date();
              var today_date = this.calendar.getToday()
              var year = today_date.year;
              var month = today_date.month;
              var day = today_date.day;
              this.config.maxDate = {
                day: day,
                month: month,
                year: year,
              };
              this.toastr.warning("No Data available, Please create new");

            }
            else if (result.response.response_code == "500") {
              // this.toastr.error(this.error, "error")
              this.overlayRef.detach();
              // this.error = result.response.message;
              this.toastr.error(result.response.message, "Error");
            }
            else {
              this.overlayRef.detach();
              this.error = "Something went wrong";
              this.toastr.error(this.error, "Error");
            }
            (err) => {
              this.overlayRef.detach();
              console.log(err); //prints if it encounters an error
            }
          });
        }
      }
      else {
        this.toastr.warning("Please select customer number and customer id");
      }
    }

    else if (type_value == 4) {
      console.log(this.cus_cus_id);
      console.log(this.cus_siteId)
      if (this.cus_cus_id == undefined || this.cus_cus_id == null || this.cus_cus_id == '') {
        this.toastr.warning("please select upto site id", "warning"); // for customer based check
      }
      else {

        if (this.role_id != '7') {
          parmas = { 'customer_code': this.cus_cus_id, 'site_id': this.cus_siteId, "flag": 1 }
        }
        else {
          parmas = { 'customer_code': this.logged_customer_code, 'site_id': this.cus_cus_id, "flag": 2 }
        }
        var url = 'get_customer_contract_details/?';
        // this.overlayRef.attach(this.LoaderComponentPortal);
        console.log(parmas);
        // api url for getting the customer and contract details
        this.ajax.postdata(url, parmas).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.error = '';
            this.site_details = [];
            this.customer_details = result.response.customer_details;
            this.site_details = result.response.site_details;
            this.segment_value = this.site_details.segment_id;
            this.application_value = this.site_details.application_id;
            console.log(this.segment_value, "this.segment_value")
            this.chRef.detectChanges();
            this.contact_number = this.customer_details.contact_number;
            this.customer_code = this.customer_details.customer_code;
            this.batteryData = result.response.battery_bank_array;
            if (this.batteryData.length == 1) {
              this.showTable = false;
              this.getSerialNmber(this.batteryData[0]);
            }
            this.productdata = [];
            this.site_details.serial_no_details.forEach(obj => {
              // var date = obj.expiry_date.split("-", 3);
              // var year = date[0];
              // var month = date[1];
              // var day = date[2];
              // obj.expiry_date = day + "-" + month + "-" + year;
              this.productdata.push(obj);
            })
            console.log(this.productdata);
            if (this.batteryData.length == 1) {
              this.battery_data = 'Enable';
              this.showTable = true;
            }
            // this.getcountry();
            // this.onChangeCountry(this.site_details.country.country_id)
            // this.onChangeState(this.site_details.state.state_id);
            // this.onChangecity(this.site_details.city.city_id);
            // this.onChangeTown(this.site_details.location.location_id);
            this.isShow = false;
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': this.batteryData,
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_name,
              // 'cus_state': this.site_details.state.state_id,
              // 'cus_city': this.site_details.city.city_id,
              // 'cus_town': this.site_details.location.location_id,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            });

            // this.Addnameinfo.patchValue({
            //   'customer_id': this.customer_details.customer_id,
            //   'customer_code': this.customer_details.customer_code,
            //   'customer_name': this.customer_details.customer_name,
            //   'ticket_id': this.customer_details.ticket_id,
            //   'email_id': this.customer_details.email_id,
            //   'contact_number': this.customer_details.contact_number,
            //   'alternate_number': this.customer_details.alternate_number,
            //   'plot_number': this.site_details.plot_number,
            //   'battery_bank_id': '',
            //   'street': this.site_details.street,
            //   'landmark': this.site_details.landmark,
            //   'post_code': this.site_details.post_code,
            //   'country_id': this.site_details.country.country_id,
            //   'state_id': this.site_details.state.state_id,
            //   'city_id': this.site_details.city.city_id,
            //   'location_id': this.site_details.location.location_id,
            //   'site_id': this.site_details.site_id,
            //   'contact_person_name': this.site_details.contact_person_name,
            //   'contact_person_number': this.site_details.contact_person_number,
            // })
            // this.getcountry();
            // this.onChangeCountryas(this.site_details.country.country_id);
            // this.onChangeStateas(this.site_details.state.state_id);
            // this.onChangecityas(this.site_details.city.city_id);
            this.overlayRef.detach();

          }
          else if (result.response.response_code == "404") {
            this.customer_details = result.response.customer_details;
            var contract_detail = result.response.contract_details;
            if (contract_detail.length == '0') {
              this.error = "There is no Contract for this customer";
              this.getcountry();

              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': '101',
                'state_id': '',
                'city_id': '',
                'location_id': ''
              })
              this.overlayRef.detach();

            }
            else {
              this.error = ''
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
              })
              this.overlayRef.detach();
              this.chRef.detectChanges();
            }
            this.toastr.error(result.response.message, "Error");
          }
          else if (result.response.response_code == "400") {
            // this.raise_ticket_reset();
            this.toastr.error(result.response.message, "Error");
            // var default_radiovalue = '1';
            // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            // this.radiochange_value = 1;

            this.overlayRef.detach();
            this.customer_code = '';
            // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
            this.getcountry();
            var today = new Date();
            var today_date = this.calendar.getToday()
            var year = today_date.year;
            var month = today_date.month;
            var day = today_date.day;
            this.config.maxDate = {
              day: day,
              month: month,
              year: year,
            };

            this.toastr.warning("Create new user", "warning");
          }
          else if (result.response.response_code == "500") {
            // this.toastr.error(this.error, "Error")
            this.overlayRef.detach();
            // this.error = result.response.message;
            this.toastr.error(result.response.message, "Error");
          }
          else {
            this.overlayRef.detach();
            this.error = "Something went wrong";
            this.toastr.error(this.error, "Error");
          }
          (err) => {
            this.overlayRef.detach();
            console.log(err); //prints if it encounters an error
          }
        });
      }

    }
    else if (type_value == 5) {
      console.log(this.siteSelect.length);

      console.log(this.siteSelect.customer_site)
      if (this.siteSelect.length == 0) {

        this.toastr.warning("Please select customer name and site id");
      }
      else {
        var sites = this.siteSelect.customer_site;
        var splitted = sites.split("-", 3);
        console.log(splitted);
        var customer_code = splitted[0];
        var site_id = splitted[1];
        parmas = { 'customer_code': customer_code, 'site_id': site_id, "flag": 1 }
        var url = 'get_customer_contract_details/?';
        this.overlayRef.attach(this.LoaderComponentPortal);
        // api url for getting the customer and contract details
        this.ajax.postdata(url, parmas).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.error = '';
            this.customer_details = result.response.customer_details;
            this.site_details = result.response.site_details;
            this.segment_value = this.site_details.segment_id;
            this.application_value = this.site_details.application_id;
            console.log(this.segment_value, "this.segment_value")
            this.chRef.detectChanges();
            this.contact_number = this.customer_details.contact_number;
            this.customer_code = this.customer_details.customer_code;
            this.batteryData = result.response.battery_bank_array;
            if (this.batteryData.length == 1) {
              this.showTable = false;
              this.getSerialNmber(this.batteryData[0]);
            }
            this.productdata = [];
            this.site_details.serial_no_details.forEach(obj => {
              // var date = obj.expiry_date.split("-", 3);
              // var year = date[0];
              // var month = date[1];
              // var day = date[2];
              // obj.expiry_date = day + "-" + month + "-" + year;
              this.productdata.push(obj);
            })
            console.log(this.productdata);
            if (this.batteryData.length == 1) {
              this.battery_data = 'Enable';
              this.showTable = true;
            }
            this.isShow = false;
            if (this.site_details.priority == 'P1') {
              this.setPriority = "P1"
            }
            else {
              this.setPriority = this.site_details.priority
            }
            this.RaiseCall.patchValue({
              'proioritys': this.site_details.priority,

            })
            this.RaiseTicketCusInfo.controls["customer_name"].setValue(this.customer_details.customer_name);
            this.name = this.customer_details.customer_name;
            console.log(this.name)
            this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);
            console.log(this.site_details.priority);
            this.RaiseTicketCusInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': this.batteryData,
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_name,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            this.Addnameinfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_number': this.customer_details.alternate_number,
              'plot_number': this.site_details.plot_number,
              'battery_bank_id': '',
              'street': this.site_details.street,
              'landmark': this.site_details.landmark,
              'post_code': this.site_details.post_code,
              'country_id': this.site_details.country.country_id,
              'state_id': this.site_details.state.state_id,
              'city_id': this.site_details.city.city_id,
              'location_id': this.site_details.location.location_id,
              'site_id': this.site_details.site_id,
              'contact_person_name': this.site_details.contact_person_name,
              'contact_person_number': this.site_details.contact_person_number,
            })
            this.getcountry();
            this.onChangeCountryas(this.site_details.country.country_id);
            this.onChangeStateas(this.site_details.state.state_id);
            this.onChangecityas(this.site_details.city.city_id);
            this.overlayRef.detach();

          }
          else if (result.response.response_code == "404") {
            this.customer_details = result.response.customer_details;
            var contract_detail = result.response.contract_details;
            if (contract_detail.length == '0') {
              this.error = "There is no Contract for this customer";
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': '',
                'street': '',
                'landmark': '',
                'post_code': '',
                'country_id': '',
                'state_id': '',
                'city_id': '',
                'location_id': ''
              })
              this.overlayRef.detach();

            }
            else {
              this.error = ''
              this.isShow = false;
              this.RaiseTicketCusInfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
              })
              this.overlayRef.detach();
              this.chRef.detectChanges();
            }
          }
          else if (result.response.response_code == "400") {
            // this.raise_ticket_reset();
            this.toastr.error(result.response.message, "Error");
            // var default_radiovalue = '1';
            // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            // this.radiochange_value = 1;

            this.overlayRef.detach();
            this.customer_code = '';
            // this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
            this.getcountry();
            var today = new Date();
            var today_date = this.calendar.getToday()
            var year = today_date.year;
            var month = today_date.month;
            var day = today_date.day;
            this.config.maxDate = {
              day: day,
              month: month,
              year: year,
            };
            this.toastr.warning("No Data available, Please create new");

          }
          else if (result.response.response_code == "500") {
            this.overlayRef.detach();
            // this.error = result.response.message
            this.toastr.error(result.response.message, "Error");
          }
          else {
            this.overlayRef.detach();
            this.error = "Something went wrong"
          }
          (err) => {
            this.overlayRef.detach();
            console.log(err); //prints if it encounters an error
          }
        });
      }

    }


  }
  batteryBankClick(event, item) {
    console.log(event)
    this.battery_data = 'Enable';
    // this.batteryBankSample=[];
    this.showTable = false;
    this.RaiseTicketCusInfo.controls['employee_id'].setValue(this.employee_id);
    if (event.target.checked == true) {
      console.log(item);
      this.raise_data = 'Enable';
      this.ExpiryDate = item.expiry_date;
      if (this.ExpiryDate) {
        var expdates = this.ExpiryDate.split("-", 3);
        var Eday: number = +expdates[0];
        var Emonth: number = +expdates[1];
        var Eyear: number = +expdates[2];
      }
      var TDate = this.calendar.getToday()
      var year = TDate.year;
      var month = TDate.month;
      var day = TDate.day;
      const VDate: NgbDate = new NgbDate(year, month, day);
      const ExpiryDate: NgbDate = new NgbDate(Eyear, Emonth, Eday);
      if (VDate.before(ExpiryDate)) {
        this.expiry_flag = "1";
      }
      else {
        this.expiry_flag = "0";        // added on 01.04.2021
      }
      console.log(TDate);
      console.log(ExpiryDate);
      this.expiry_flag = this.expiry_flag;
      console.log(this.expiry_flag);
      console.log(item);
      this.showTable = true;                            //show datas if after call API
      this.chRef.detectChanges();
      this.overlayRef.detach();
      // console.log(serial);
      this.batteryBankSample.push(item);
      // this.batteryBankArray.push(serial);
      console.log(this.batteryBankArray);

      // this.RaiseTicketCusInfo.value.valid=true;
    }
    else if (event.target.checked == false) {
      console.log(this.batteryBankArray);
      console.log(item);
      // this.raise_data = '';
      this.ExpiryDate = item.expiry_date;
      if (this.ExpiryDate) {
        var expdates = this.ExpiryDate.split("-", 3);
        var Eday: number = +expdates[0];
        var Emonth: number = +expdates[1];
        var Eyear: number = +expdates[2];
      }
      var TDate = this.calendar.getToday()
      var year = TDate.year;
      var month = TDate.month;
      var day = TDate.day;
      const VDate: NgbDate = new NgbDate(year, month, day);
      const ExpiryDate: NgbDate = new NgbDate(Eyear, Emonth, Eday);
      if (VDate.before(ExpiryDate)) {
        this.expiry_flag = "1";
      }
      else {
        this.expiry_flag = "0";        // added on 01.04.2021
      }
      console.log(TDate);
      console.log(ExpiryDate);
      this.expiry_flag = this.expiry_flag;
      console.log(this.expiry_flag);
      console.log(item);
      this.showTable = true;                            //show datas if after call API
      this.chRef.detectChanges();
      this.overlayRef.detach();
      // console.log(serial);
      // this.batteryBankSample.push(item);
      // this.batteryBankArray.push(serial);
      // console.log(this.batteryBankArray);
      // console.log(this.productdatas, "this.productdatas")
      // console.log(this.batteryBankSample, "this.batteryBankSample")
      this.batteryBankSample = this.batteryBankSample.filter(({ serial_no }) => serial_no !== item.serial_no);
      // console.log('unchecked');
      console.log(this.batteryBankSample);
      if (this.batteryBankSample.length == 0) {
        this.raise_data = '';
      }
      else {
        this.raise_data = 'Enable';
      }
      // this.raise_data = '';
      // // console.log(serial_no);
    }
    if (this.batteryBankArray) {
      console.log(this.RaiseTicketCusInfo.value);
      if (this.RaiseTicketCusInfo.value.invalid == false) {
      }
      else {
        this.RaiseTicketCusInfo.value.invalid = false
        // this.chRef.detectChanges();
      }

      console.log(this.RaiseTicketCusInfo.invalid);
    }
    else {
      this.RaiseTicketCusInfo.value.invalid = false;
    }
    // console.log(datas);
  }
  //change the expire date as text if date is less than today
  get_expiredate(contract_data) {
    contract_data.forEach((element) => {
      var today_date = this.calendar.getToday()
      var year = today_date.year;
      var month = today_date.month;
      var day = today_date.day;
      this.firstDate = year + "-" + month + "-" + day;
      this.secondDate = element.expiry_day;
      if (new Date(this.firstDate).getTime() > new Date(this.secondDate).getTime()) {
        element.expiry_day = "Contract Expired";
      }
      else {
        element.expiry_day = element.expiry_day
      }
    })
  }
  nextTab(id) {
    console.log(this.Addbatterybank)
    this.get_servicegroup();
    this.get_call_category();
    console.log(this.email_error)
    console.log(this.contactnum_error)
    this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
    if ((this.email_error == '' && this.contactnum_error == '') || (this.email_error == undefined && this.contactnum_error == undefined) || (this.email_error == undefined && this.contactnum_error == '') || (this.email_error == '' && this.contactnum_error == undefined)) {
      this.activeId = "tab-selectbyidb";
      console.log(this.Addbatterybank, "uirr")
    }
    else {
      this.activeId = "tab-selectbyida";
      console.log(this.Addbatterybank, "uirr")
    }
    console.log(this.Addbatterybank)
  }
  prev() {
    this.activeId = "tab-selectbyida";
  }
  //Function for getting the customer,product details based on the contract id
  get_contract_info(contrat_id) {
    var contract_id = +contrat_id;
    var url = 'get_contract_info/?'; // api url for getting the details with using post params
    var id = "contract_id"; //id for making url
    this.ajax.getdataparam(url, id, contract_id).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.raiseticket_details = result.response.data;
        this.RaiseTicketCusInfo.patchValue({
          'customer_id': this.raiseticket_details.customer_id,
          'customer_code': this.raiseticket_details.customer_code,
          'customer_name': this.raiseticket_details.customer_name,
          'ticket_id': this.raiseticket_details.ticket_id,
          'email_id': this.raiseticket_details.email_id,
          'contact_number': this.raiseticket_details.contact_number,
          'alternate_contact_number': this.raiseticket_details.alternate_number,
          'plot_number': this.raiseticket_details.plot_number,
          'street': this.raiseticket_details.street,
          'landmark': this.raiseticket_details.landmark,
          'post_code': this.raiseticket_details.post_code,
          'country_id': this.raiseticket_details.country.country_id,
          'state_id': this.raiseticket_details.state.state_id,
          'city_id': this.raiseticket_details.city.city_id,
          'location_id': this.raiseticket_details.location.location_id,
        })
        this.RaiseTicketProdInfo.patchValue({
          'product_id': this.raiseticket_details.product.product_id,
          'product_sub_id': this.raiseticket_details.sub_product.product_sub_id,
          'model_no': this.raiseticket_details.model_no,
          'segment': this.raiseticket_details.model_no,
          'serial_no': this.raiseticket_details.serial_no,
          'contract_type': this.raiseticket_details.contract.contract_type,
          'cust_preference_date': this.raiseticket_details.expiry_day,
        })
        this.getcountry();
        this.onChangeCountryas(this.raiseticket_details.country.country_id);
        this.onChangeStateas(this.raiseticket_details.state.state_id);
        this.onChangecityas(this.raiseticket_details.city.city_id);
        this.onChangeProduct(this.raiseticket_details.product.product_id);

      }
      else if (result.response.response_code == "400") { //if failure
        this.toastr.error(result.response.message, 'Error'); //toastr message for error
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  modalsuccess(msg_success) {                             // modal pop-up for ticket raise message
    this.email_error = '';
    this.adderror = '';
    this.contactnum_error = '';
    this.contact = '';
    this.mail_error = '';
    this.errorprice = '';
    this.warrantyerror = '';
    this.warranty = 'none'
    this.modalService.open(msg_success, {
      // size: 'lg', //specifies the size of the dialog box
      windowClass: "myCustomModalClass",
      backdrop: 'static',
      keyboard: false
    });
  }
  //Function to reset the raiseticket field value
  raise_ticket_reset() {
    this.select = [];
    this.contact_number = '';
    this.customer_code = '';
    this.image_name = '';
    this.image = '';
    this.RaiseTicketCusInfo.reset();
    this.Addnameprodinfo.reset();
    // this.Addnameprodinfo.controls['call_category_id'].setValue('');
    // this.Addnameprodinfo.controls['desc'].setValue('');
    this.Addnameinfo.reset();
    this.batteryData = [];
    this.battery_data = '';
    this.raise_data = '';
    this.battery_datas = '';
    this.error = ''
    this.radiochange_value = '5';
    // this.RaiseTicketCusInfo.controls['customer_type'].setValue(null)
    this.RaiseTicketCusInfo.controls['lable_type'].setValue(this.radiochange_value);
    this.isShow = true;
  }

  //raising the ticket
  raising_ticket(msg_success) {
    this.loadingSubmit = true;
    if (this.RaiseCall.valid) {
      var expdates = this.ExpiryDate.split("-", 3);
      var Eyear: number = +expdates[2];
      var Emonth: number = +expdates[1];
      var Eday: number = +expdates[0];
      this.ExpiryDate = Eyear + "-" + Emonth + "-" + Eday;
      var expiryDate = Eday + "-" + Emonth + "-" + Eyear;
      this.batteryBankSample.forEach(element => {
        var serial = {
          "product_id": element.product.product_id,
          "product_sub_id": element.subproduct.product_sub_id,
          "model": element.model.model_id,
          "sys_ah": element.sys_ah,
          "voltage": element.voltage.toUpperCase(),
          "serial_no": element.serial_no,
          "invoice_number": element.invoice_number,
          "invoice_date": element.invoice_date,
          "mfg_date": element.mfg_date,
          "warranty_type_id": element.warranty.warranty_id,
          "expiry_date": expiryDate,
        };
        this.batteryBankArray.push(serial);
      })
      console.log(this.RaiseCall.valid);
      console.log(this.RaiseCall.value.cust_preference_date, " this.RaiseCall.value.cust_preference_date")
      var year = this.RaiseCall.value.cust_preference_date.year;
      var month = this.RaiseCall.value.cust_preference_date.month;
      var day = this.RaiseCall.value.cust_preference_date.day;
      var dateFormated = year + "-" + month + "-" + day;
      this.RaiseCall.controls['cust_preference_date'].setValue(dateFormated);
      console.log(this.RaiseCall.value);

      this.RaiseCall.value.image = this.cardImageBase64;
      console.log(this.site_details)
      console.log(this.RaiseTicketProdInfo.value)
      // this.site_details = result.response.site_details;
      this.RaiseTicketCusInfo.value.location_id = this.site_details.location.location_id;
      if (this.site_details.product == null || this.site_details.product == '') {
        this.RaiseTicketProdInfo.value.product_id = null;
      }
      else {
        this.RaiseTicketProdInfo.value.product_id = this.site_details.product.product_id;
      }
      if (this.site_details.sub_product == null || this.site_details.sub_product == '') {
        this.RaiseTicketProdInfo.value.product_sub_id = null
      }
      else {
        this.RaiseTicketProdInfo.value.product_sub_id = this.site_details.sub_product.product_sub_id;
      }
      if (this.site_details.model_no == null || this.site_details.model_no == '') {
        this.RaiseTicketProdInfo.value.model_no = null
      }
      else {
        this.RaiseTicketProdInfo.value.model_no = this.site_details.model_no;
      }
      this.RaiseTicketCusInfo.value.country_id = this.site_details.country.country_id;
      this.RaiseTicketCusInfo.value.state_id = this.site_details.state.state_id;
      this.RaiseTicketCusInfo.value.city_id = this.site_details.city.city_id;
      this.RaiseTicketCusInfo.value.sys_ah = this.site_details.sys_ah;
      this.RaiseTicketCusInfo.value.voltage = this.site_details.voltage;
      this.RaiseCall.value.work_type_id = 1;

      this.RaiseTicketCusInfo.value.employee_id = this.employee_id;
      if (this.RaiseCall.value.desc == null) {
        this.RaiseCall.value.desc = "";
      } else {
        this.RaiseCall.value.desc = this.RaiseCall.value.desc;
      }
      this.RaiseCall.patchValue({
        'proioritys': this.site_details.priority,
      })
      this.RaiseCall.controls['proioritys'].setValue(this.site_details.priority);
      this.RaiseCall.controls['work_type_id'].setValue(1);
      console.log(this.RaiseCall.value.proioritys);
      this.form = {
        "employee_id": this.RaiseTicketCusInfo.value.employee_id,
        "ticket_id": this.RaiseTicketCusInfo.value.ticket_id,
        "contract_id": this.RaiseTicketCusInfo.value.contract_id,
        "alternate_contact_number": this.RaiseTicketCusInfo.value.alternate_contact_number,
        "city_id": this.RaiseTicketCusInfo.value.city_id,
        "contact_number": this.RaiseTicketCusInfo.value.contact_number,
        "country_id": this.RaiseTicketCusInfo.value.country_id,
        "customer_code": this.RaiseTicketCusInfo.value.customer_code,
        "customer_id": this.RaiseTicketCusInfo.value.customer_id,
        "customer_name": this.RaiseTicketCusInfo.value.customer_name,
        "email_id": this.RaiseTicketCusInfo.value.email_id,
        "landmark": this.RaiseTicketCusInfo.value.landmark,
        "location_id": +this.RaiseTicketCusInfo.value.location_id,
        "plot_number": this.RaiseTicketCusInfo.value.plot_number,
        "post_code": this.RaiseTicketCusInfo.value.post_code,
        "state_id": this.RaiseTicketCusInfo.value.state_id,
        "street": this.RaiseTicketCusInfo.value.street,
        "call_category_id": +this.RaiseCall.value.call_category_id,
        "contract_type": this.batteryBankSample[0].warranty.warranty_type,
        "cust_preference_date": dateFormated,
        "image": this.RaiseTicketProdInfo.value.image,
        "model_no": this.RaiseTicketProdInfo.value.model_no,
        "product_id": this.RaiseTicketProdInfo.value.product_id,
        "product_sub_id": this.RaiseTicketProdInfo.value.product_sub_id,
        "segment_id": this.segment_value,
        "application_id": this.application_value,
        "problem_desc": this.RaiseCall.value.desc,
        "quantity": this.RaiseTicketProdInfo.value.quantity,
        "site_id": this.RaiseTicketCusInfo.value.site_id,
        // "serial_no": this.RaiseTicketProdInfo.value.serial_no,
        "work_type_id": this.RaiseCall.value.work_type_id,
        "priority": this.RaiseCall.value.proioritys,
        "serial_no_details": JSON.stringify(this.batteryBankArray),
        "expiry_flag": +this.expiry_flag,
        "battery_bank_id": this.BatteryBankId,
        "voltage": this.RaiseTicketCusInfo.value.voltage.toUpperCase(),
        "sys_ah": this.RaiseTicketCusInfo.value.sys_ah
      }
      // this.Addbatterybank.patchValue({

      //   'segment_id': this.site_details.segment_id,
      //   'application_id': this.site_details.application_id,
      // })
      console.log(this.form);
      // return this.form
      this.overlayRef.attach(this.LoaderComponentPortal);
      var url = 'raise_ticket/' //api url of edit api
      this.ajax.postdata(url, this.form)
        .subscribe((result) => {

          if (result.response.response_code == "200") //if sucess
          {
            this.raise_ticket_reset();
            var default_radiovalue = '5';
            this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
            this.radiochange_value = "5";
            this.modalService.dismissAll(); //to close the modal box
            this.modalsuccess(msg_success);                       //to open modal success pop-up
            this.successraise_message = result.response.message;
            // this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.loadingSubmit = false;

          }
          else if (result.response.response_code == "400") { //if failiure
            this.loadingSubmit = false;
            this.raise_error = result.response.message  //toastr message for error
          }
          else if (result.response.response_code == "500") {
            this.loadingSubmit = false;
            this.toastr.error(this.raise_error, "Error")
            this.raise_error = result.response.message
          }
          else {
            this.loadingSubmit = false;
            this.raise_error = "Something went wrong"
          }
          this.overlayRef.detach();

        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
    }
  }

  create_new_customer(button_type) {
    // this.addEmailValidation();
    // this.addContactValidation();
    var year = this.NewCustomerProdInfo.value.purchase_date.year;
    var month = this.NewCustomerProdInfo.value.purchase_date.month;
    var day = this.NewCustomerProdInfo.value.purchase_date.day;
    var dateFormated = year + "-" + month + "-" + day;
    this.NewCustomerProdInfo.value.purchase_date = dateFormated;
    if (this.customer_code == '' || this.customer_code == null || this.customer_code == undefined) {
      var cus_code = null
    }
    else {
      var cus_code = this.customer_code
    }
    if (this.NewCustomerProdInfo.valid) {
      this.form = {
        "employee_code": this.employee_code,
        "alternate_number": this.NewCustomerInfo.value.alternate_number,
        "city_id": this.NewCustomerInfo.value.city_id,
        "contact_number": this.NewCustomerInfo.value.contact_number,
        "country_id": this.NewCustomerInfo.value.country_id,
        "customer_code": cus_code,
        "customer_name": this.NewCustomerInfo.value.customer_name,
        "email_id": this.NewCustomerInfo.value.email_id,
        "landmark": this.NewCustomerInfo.value.landmark,
        "location_id": +this.NewCustomerInfo.value.location_id,
        "plot_number": this.NewCustomerInfo.value.plot_number,
        "post_code": this.NewCustomerInfo.value.post_code,
        "state_id": this.NewCustomerInfo.value.state_id,
        "street": this.NewCustomerInfo.value.street,
        "amc_id": this.NewCustomerProdInfo.value.amc_id,
        "product_id": this.NewCustomerProdInfo.value.product_id,
        "product_sub_id": this.NewCustomerProdInfo.value.product_sub_id,
        "model_no": this.NewCustomerProdInfo.value.model_no,
        "serial_no": this.NewCustomerProdInfo.value.serial_no,
        "purchase_date": this.NewCustomerProdInfo.value.purchase_date,
        "contract_duration": this.NewCustomerProdInfo.value.duration,
        "voltage": this.NewCustomerProdInfo.value.voltage.toUpperCase(),
        "sys_ah": this.NewCustomerProdInfo.value.sys_ah
      }
      var data = this.form;

      var url = 'add_customer_contract/' //api url of edit api
      this.ajax.postdata(url, data)
        .subscribe((result) => {
          this.overlayRef.attach(this.LoaderComponentPortal);
          if (result.response.response_code == "200") //if sucess
          {
            this.multi_contract_details = result.response.contract_data
            if (button_type == 1) {
              this.raise_ticket_reset();
              // var default_radiovalue = '1';
              // this.radiochange_value = "1";
              // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
              var i: number;
              for (i = this.NewCustomerProdInfo.value.contract_data.length; i >= 0; i--) {
                this.remove_dynamic_field(i);
              }
              this.toastr.success(result.response.message, 'Success'); //toastr message for success
              this.modalService.dismissAll(); //to close the modal box

            }
            else if (button_type == 2) {
              this.NewCustomerProdInfo.reset();
              this.customer_code = ''
              this.customer_code = this.multi_contract_details[0].customer_code
              this.success_msg = result.response.message
              // this.NewCustomerInfo.controls['customer_code'].readonly()
              // this.NewCustomerInfo.controls['email_id'].disable()
              // this.NewCustomerInfo.controls['contact_number'].disable()
              // this.t.clear();          //delete the previous array value
              var i: number;
              for (i = this.NewCustomerProdInfo.value.contract_data.length; i >= 0; i--) {
                this.remove_dynamic_field(i);
              }
              this.multi_contract_details.forEach(element => {
                var splitted = element.purchase_date.split("-", 3);
                var year_date = splitted[2].split(",", 1)
                var year: number = +year_date[0];
                var day: number = +splitted[0];
                var month: number = +splitted[1];
                var purchasedate = {
                  year: year,
                  month: month,
                  day: day,
                }
                this.t.push(this.formBuilder.group({
                  product_id: [element.product_name],
                  product_sub_id: [element.product_sub_name],// product_sub_id
                  model_no: [element.model_no],
                  serial_no: [element.serial_no],
                  purchase_date: [purchasedate],
                  amc_id: [element.amc_type],
                  contract_duration: [element.contract_duration],
                  contract_id: [element.contract_id]
                }));
              });
            }
            this.overlayRef.detach();
          }
          else if (result.response.response_code == "400") { //if failiure
            this.customer_error = result.response.message //toastr message for error
            this.overlayRef.detach();
          }
          else if (result.response.response_code == "500") {
            this.customer_error = result.response.message
            this.toastr.error(this.customer_error, "Error")
            this.overlayRef.detach();
          }
          else {
            this.customer_error = "Something went wrong"
            this.overlayRef.detach();
          }
        }, (err) => {
          this.overlayRef.detach();
        });
    }
    else {
      this.submit_error_msg = "All fields are mandatory"
      // this.overlayRef.detach();
    }
  }
  fetchNews(evt: any) {
    this.activeId = evt.nextId;
    this.evt = evt; // has nextId that you can check to invoke the desired function
  }
  date: any;
  time: any = { hour: 0, minute: 0 };

  _value;
  label;

  getDatetime() {
    let value = null;
    if (!this.date) {
      if (!this.time) value = "yyyy/MM/dd hh:mm";
      else
        value =
          "yyyy/MM/dd " +
          ("0" + this.time.hour).slice(-2) +
          ":" +
          ("0" + this.time.minute).slice(-2);
    }
    if (!value) {
      value = new Date(Date.UTC(
        this.date.year,
        this.date.month - 1,
        this.date.day,
        this.time ? this.time.hour : 0,
        this.time ? this.time.minute : 0
      ));
      this._value = value;
    } else
      this._value = null

    //  this.form.get("control").setValue(this._value);
    this.label = value;

    var date = new Date();
    var objDate = { day: 9, month: 6, year: 2020 }
    var objMinute = { hour: 13, minute: 38, second: 22 }
    function compareDays(dateObj, hour) {
      var objDate = new Date(dateObj.year + '-' + dateObj.month + "-" + dateObj.day +
        " " + hour.hour + ":" + hour.minute + ":" + hour.second + ".000Z");

      return (date.getTime() / 1000) > (objDate.getTime() / 1000) ? true : false;
    }
  }
  remove_function() {
    this.show_field = true;
    this.remove_button_hide = false;
    this.show_cancel_button = true;
  }
  save_exit() {
    this.raise_ticket_reset();
    // var default_radiovalue = '1';
    // this.radiochange_value = "1";
    // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
    var i: number;
    for (i = this.NewCustomerProdInfo.value.contract_data.length; i >= 0; i--) {
      this.remove_dynamic_field(i);
    }
    this.show_field = false;
    this.remove_button_hide = true;
    this.show_cancel_button = false;
    this.toastr.success("Customer with contract created successfully", 'Success'); //toastr message for success
    this.modalService.dismissAll();

  }

  imageClear() {

    this.ByDefault = false
    this.image = '';
    this.buttonmainedit = !this.buttonmainedit;
    this.RaiseTicketProdInfo.controls['image'].setValue('');
    this.error = "please select the image";
    console.log(this.RaiseTicketProdInfo.value);
  }

  //raising the ticket
  cus_contract_ticket(msg_success) {
    console.log(this.Addnameprodinfo)
    console.log(this.Addnameprodinfo.value)
    this.AllSerial = []
    // console.log(this.Addnameprodinfo.value.cust_preference_date, "this.Addnameprodinfo.value.cust_preference_date")
    console.log(this.Addnameprodinfo.value.product_id, " this.Addnameprodinfo.value.product_id")
    console.log(this.Addnameprodinfo.value.serial_no, "this.Addnameprodinfo.value.serial_no")
    // console.log(this.Addnameinfo.value.customer_name)
    if (this.Addnameinfo.value.customer_type == 1) {
      // console.log(this.Addnameinfo.value.alternate_number)
      // console.log(typeof (this.Addnameinfo.value.customer_name))
      // console.log(this.Addnameinfo.value.alternate_number, "this.Addnameinfo.value.alternate_number")
      // console.log(this.Addnameinfo.value.alternate_number[0]['oem_name'])
      if (this.Addnameinfo.value.alternate_number[0]) {
        var id = this.Addnameinfo.value.alternate_number[0]['oem_name']
        // console.log(id, "id")
      }
      else {
        id = this.Addnameinfo.value.alternate_number
        // console.log(id, "id")

      }
      // console.log(id, "id")
      var customer_name = id;
      // console.log(customer_name)
      this.Addnameinfo.controls['alternate_number'].setValue(id)
    }
    // console.log(this.Addnameinfo.value.customer_name)
    // console.log(this.Addnameprodinfo.value.expiry_day)
    if (this.Addnameprodinfo.value.expiry_day == null || this.Addnameprodinfo.value.expiry_day == '' || this.Addnameprodinfo.value.expiry_day == "undefined-undefined-undefined") {
      var date1serial = null
      var date1 = null

    }

    else {
      var year1 = this.Addnameprodinfo.value.expiry_day.year;
      var month1 = this.Addnameprodinfo.value.expiry_day.month;
      var day1 = this.Addnameprodinfo.value.expiry_day.day;
      date1 = year1 + "-" + month1 + "-" + day1;
      date1serial = day1 + "-" + month1 + "-" + year1;
    }
    // console.log(this.Addnameprodinfo.value.mfg_date)
    if (this.Addnameprodinfo.value.mfg_date == null || this.Addnameprodinfo.value.mfg_date == undefined || this.Addnameprodinfo.value.mfg_date == '' || this.Addnameprodinfo.value.mfg_date == "undefined-undefined-undefined") {
      var mfg_dateserial = null
      var mfg_date = null

    }

    else {
      var year1 = this.Addnameprodinfo.value.mfg_date.year;
      var month1 = this.Addnameprodinfo.value.mfg_date.month;
      var day1 = this.Addnameprodinfo.value.mfg_date.day;
      mfg_date = year1 + "-" + month1 + "-" + day1;
      mfg_dateserial = day1 + "-" + month1 + "-" + year1;
    }

    // console.log(this.Addnameprodinfo.value.invoice_date)
    if (this.Addnameprodinfo.value.invoice_date) {
      if (this.Addnameprodinfo.value.invoice_date == null || this.Addnameprodinfo.value.invoice_date == undefined || this.Addnameprodinfo.value.invoice_date == '' || this.Addnameprodinfo.value.invoice_date == "undefined-undefined-undefined") {
        var date2 = null;
        this.Addnameprodinfo.value.invoice_date = date2
      }
      else {
        // console.log(this.Addnameprodinfo.value.invoice_date)
        var ids = this.Addnameprodinfo.value.invoice_date
        // console.log(ids)
        // console.log(ids[2], "ids[2]")
        var year1 = this.Addnameprodinfo.value.invoice_date.year;
        var month1 = this.Addnameprodinfo.value.invoice_date.month;
        var day1 = this.Addnameprodinfo.value.invoice_date.day;
        date2 = year1 + "-" + month1 + "-" + day1;
        this.Addnameprodinfo.value.invoice_date = date2
      }
    }
    // console.log(date2)
    if (this.Addnameprodinfo.value.invoice_date == undefined) {
      this.Addnameprodinfo.value.invoice_date = null
    }
    if (this.Addnameprodinfo.value.product_id == '' || this.Addnameprodinfo.value.product_id == "" || this.Addnameprodinfo.value.product_id == undefined) {

      this.Addnameprodinfo.value.product_id = null;
    }
    else {
      this.Addnameprodinfo.value.product_id = this.Addnameprodinfo.value.product_id
    }
    console.log(this.Addnameprodinfo.value.product_id)
    if (this.Addnameprodinfo.value.product_sub_id == '' || this.Addnameprodinfo.value.product_sub_id == "") {
      this.Addnameprodinfo.value.product_sub_id = null;
    }
    else {
      this.Addnameprodinfo.value.product_sub_id = this.Addnameprodinfo.value.product_sub_id
    }
    if (this.Addnameprodinfo.value.model_no == '' || this.Addnameprodinfo.value.model_no == "") {
      this.Addnameprodinfo.value.model_no = null;
    }
    else {
      this.Addnameprodinfo.value.model_no = this.Addnameprodinfo.value.model_no
    }

    var serial_details = {
      "product_id": this.Addnameprodinfo.value.product_id,
      "product_sub_id": this.Addnameprodinfo.value.product_sub_id,
      "model": this.Addnameprodinfo.value.model_no,
      "sys_ah": this.Addnameprodinfo.value.sys_ah,
      "serial_no": this.Addnameprodinfo.value.serial_no,
      "invoice_number": this.Addnameprodinfo.value.invoice_number,
      "invoice_date": this.Addnameprodinfo.value.invoice_date,
      "mfg_date": mfg_dateserial,
      "warranty_type_id": this.Addnameprodinfo.value.warranty_type_id,
      "expiry_date": date1serial,
      "voltage": this.Addnameprodinfo.value.voltage.toUpperCase(),
    };
    if (this.Addnameprodinfo.value.product_id != null && this.Addnameprodinfo.value.product_sub_id != null && this.Addnameprodinfo.value.model_no != null && this.Addnameprodinfo.value.sys_ah != null && this.Addnameprodinfo.value.voltage != null) {
      this.AllSerial.push(serial_details);
    }
    else {
      this.AllSerial = []
    }


    if (this.Addnameprodinfo.value.battery_bank_id != '' && this.Addnameprodinfo.value.battery_bank_id != null && this.Addnameprodinfo.value.battery_bank_id != undefined) {
      // console.log("if battery")

      this.battery_bank_id = this.Addnameprodinfo.value.battery_bank_id
    }
    else {
      // console.log("else battery")
      this.battery_bank_id = ""
    }
    if (this.Addnameinfo.value.site_id != '' && this.Addnameinfo.value.site_id != null && this.Addnameinfo.value.site_id != undefined) {
      // console.log("if site")
      this.site_id = this.Addnameinfo.value.site_id
    }
    else {
      // console.log("else site")
      this.site_id = ""
    }
    if (this.Addnameprodinfo.value.cust_preference_date == "undefined-undefined-undefined") {
      // var today = new Date();
      var today_date = this.calendar.getToday()
      var to_year = today_date.year;
      var to_month = today_date.month;
      var to_day = today_date.day;
      this.Addnameprodinfo.value.cust_preference_date = to_year + "-" + to_month + "-" + to_day;
    }
    else {
      var year = this.Addnameprodinfo.value.cust_preference_date.year;
      var month = this.Addnameprodinfo.value.cust_preference_date.month;
      var day = this.Addnameprodinfo.value.cust_preference_date.day;
      var dateFormated = year + "-" + month + "-" + day;
      this.Addnameprodinfo.controls['cust_preference_date'].setValue(dateFormated);
      // console.log(this.RaiseCall.value);
    }
    // console.log(this.RaiseCall.value.cust_preference_date, " this.RaiseCall.value.cust_preference_date")

    // console.log(this.RaiseCall.value);



    // this.RaiseCall.value.image = this.cardImageBase64;
    // // this.site_details = result.response.site_details;
    // this.RaiseTicketCusInfo.value.location_id = this.site_details.location.location_id;
    // this.RaiseTicketProdInfo.value.product_id = this.site_details.product.product_id;
    // this.RaiseTicketProdInfo.value.product_sub_id = this.site_details.sub_product.product_sub_id;
    // this.RaiseTicketCusInfo.value.country_id = this.site_details.country.country_id;
    // this.RaiseTicketCusInfo.value.state_id = this.site_details.state.state_id;
    // this.RaiseTicketCusInfo.value.city_id = this.site_details.city.city_id;
    // this.RaiseTicketCusInfo.value.sys_ah = this.site_details.sys_ah;
    // this.RaiseTicketCusInfo.value.voltage = this.site_details.voltage;
    // this.RaiseCall.value.work_type_id = 1;
    // this.RaiseTicketProdInfo.value.model_no = this.site_details.model_no;
    // this.RaiseTicketCusInfo.value.employee_id = this.employee_id;
    if (this.Addnameprodinfo.value.desc == null) {
      this.Addnameprodinfo.value.desc = "";
    } else {
      this.Addnameprodinfo.value.desc = this.Addnameprodinfo.value.desc;
    }
    if (this.Addnameprodinfo.value.proioritys == '' || this.Addnameprodinfo.value.proioritys == null || this.Addnameprodinfo.value.proioritys == undefined) {
      this.Addnameprodinfo.patchValue({
        'proioritys': this.site_details.priority,
      })
      this.Addnameprodinfo.controls['proioritys'].setValue(this.site_details.priority);

    }
    else {
      this.Addnameprodinfo.value.proioritys = this.Addnameprodinfo.value.proioritys
    }
    if (this.Addnameprodinfo.value.contract_duration == '') {
      this.Addnameprodinfo.value.contract_duration = null;
    }
    else {
      this.Addnameprodinfo.value.contract_duration = this.Addnameprodinfo.value.contract_duration
    }


    if (this.Addnameprodinfo.value.invoice_date == '') {
      this.Addnameprodinfo.value.invoice_date = null;
    }
    else {
      this.Addnameprodinfo.value.invoice_date = this.Addnameprodinfo.value.invoice_date
    }
    if (this.Addnameprodinfo.value.segment_id == '' || this.Addnameprodinfo.value.segment_id == "") {
      this.Addnameprodinfo.value.segment_id = null;
    }
    else {
      this.Addnameprodinfo.value.segment_id = this.Addnameprodinfo.value.segment_id
    }
    if (this.Addnameprodinfo.value.application_id == '' || this.Addnameprodinfo.value.application_id == "") {
      this.Addnameprodinfo.value.application_id = null;
    }
    else {
      this.Addnameprodinfo.value.application_id = this.Addnameprodinfo.value.application_id
    }
    if (this.Addnameprodinfo.value.organization_name == '' || this.Addnameprodinfo.value.organization_name == "") {
      this.Addnameprodinfo.value.organization_name = null;
    }
    else {
      this.Addnameprodinfo.value.organization_name = this.Addnameprodinfo.value.organization_name
    }

    if (this.Addnameprodinfo.value.warranty_type_id == '') {
      this.Addnameprodinfo.value.warranty_type_id = null;
    }
    else {
      this.Addnameprodinfo.value.warranty_type_id = this.Addnameprodinfo.value.warranty_type_id
    }
    this.Addnameprodinfo.controls['work_type_id'].setValue(1);
    // console.log(this.RaiseCall.value)
    // console.log(this.Addnameprodinfo.value)
    if (this.Addnameinfo.value.customer_code != "") {
      var cus_code = this.Addnameinfo.value.customer_code
    }
    else {
      var cus_code = null
    }
    if (this.Addnameprodinfo.value.serial_no == '' || this.Addnameprodinfo.value.serial_no == "" || this.Addnameprodinfo.value.serial_no == null || this.Addnameprodinfo.value.serial_no == undefined) {
      // alert("NO")
      this.Addnameprodinfo.value.serial_no = null;
    }
    else {
      this.Addnameprodinfo.value.serial_no = this.Addnameprodinfo.value.serial_no
    }
    if (this.Addnameprodinfo.value.contract_duration == '' || this.Addnameprodinfo.value.contract_duration == "" || this.Addnameprodinfo.value.contract_duration == null || this.Addnameprodinfo.value.contract_duration == undefined) {
      // alert("NO")
      this.Addnameprodinfo.value.contract_duration = null;
    }
    else {
      this.Addnameprodinfo.value.contract_duration = this.Addnameprodinfo.value.contract_duration
    }
    this.form = {
      "employee_id": this.employee_id,
      "employee_code": this.employee_code,
      "customer_code": cus_code,
      "customer_name": this.Addnameinfo.value.customer_name,
      "email_id": this.Addnameinfo.value.email_id,
      "contact_number": this.Addnameinfo.value.contact_number,
      "alternate_number": this.Addnameinfo.value.alternate_number,
      "sap_reference_number": this.Addnameinfo.value.sap_reference_number,
      "customer_type": this.Addnameinfo.value.customer_type,
      "plot_number": this.Addnameinfo.value.plot_number,
      "contact_person_name": this.Addnameinfo.value.contact_person_name,
      "contact_person_number": this.Addnameinfo.value.contact_person_number,
      "street": this.Addnameinfo.value.street,
      "landmark": this.Addnameinfo.value.landmark,
      "post_code": this.Addnameinfo.value.post_code,
      "country_id": this.Addnameinfo.value.country_id,
      "state_id": this.Addnameinfo.value.state_id,
      "city_id": this.Addnameinfo.value.city_id,
      "location_id": this.Addnameinfo.value.location_id,
      "product_id": this.Addnameprodinfo.value.product_id,
      "product_sub_id": this.Addnameprodinfo.value.product_sub_id,
      "model_no": this.Addnameprodinfo.value.model_no,
      "serial_no": this.Addnameprodinfo.value.serial_no,
      "invoice_date": this.Addnameprodinfo.value.invoice_date,
      "contract_duration": this.Addnameprodinfo.value.contract_duration,
      "site_id": this.site_id,
      "battery_bank_id": this.battery_bank_id,
      "sys_ah": this.Addnameprodinfo.value.sys_ah,
      "segment_id": this.Addnameprodinfo.value.segment_id,
      "application_id": this.Addnameprodinfo.value.application_id,
      "organization_name": this.Addnameprodinfo.value.organization_name,
      "invoice_number": this.Addnameprodinfo.value.invoice_number,
      "expiry_day": date1,
      "warranty_type_id": this.Addnameprodinfo.value.warranty_type_id,
      "mfg_date": mfg_date,
      //  "expiry_day": this.Addnameprodinfo.value.expiry_day,
      "voltage": this.Addnameprodinfo.value.voltage.toUpperCase(),
      "serial_no_details": JSON.stringify(this.AllSerial),
      // "ticket_id": this.RaiseTicketCusInfo.value.ticket_id,
      // "contract_id": this.RaiseTicketCusInfo.value.contract_id,       
      "customer_id": this.RaiseTicketCusInfo.value.customer_id,
      "call_category_id": +this.Addnameprodinfo.value.call_category_id,
      "contract_type": this.Addnameprodinfo.value.warranty_type_id,
      "cust_preference_date": dateFormated,
      "image": this.Addnameprodinfo.value.image,
      "desc": this.Addnameprodinfo.value.desc,
      // "quantity": this.RaiseTicketProdInfo.value.quantity,
      "work_type_id": this.Addnameprodinfo.value.work_type_id,
      "priority": this.Addnameprodinfo.value.proioritys,
      "expiry_flag": +this.expiry_flag,
    }
    console.log(this.form);
    this.loadingSubmit = true;
    console.log(this.expiry_flag, "this.expiry_flag")
    var url = 'cus_contract_ticket/' //api url of edit api
    this.ajax.postdata(url, this.form)
      .subscribe((result) => {
        if (result.response.response_code == "200") //if sucess
        {

          // var default_radiovalue = '3';
          // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
          // this.radiochange_value = "3";
          // this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.modalService.dismissAll(); //to close the modal box
          this.modalsuccess(msg_success);                       //to open modal success pop-up
          this.successraise_message = result.response.message;
          var cust_type = this.Addnameinfo.value.customer_type
          this.raise_ticket_reset();
          this.loadingSubmit = false;
          this.customerName(cust_type)
          this.getcountry();
          this.customer_state = [];
          this.Addnameinfo.controls['country_id'].setValue(101);
          this.RaiseCall.controls["cust_preference_date"].setValue(this.model)
          // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
        }
        else if (result.response.response_code == "400") { //if failiure
          this.loadingSubmit = false;
          this.raise_error = result.response.message  //toastr message for error
          this.RaiseCall.controls["cust_preference_date"].setValue(this.model)

          this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)

        }
        else if (result.response.response_code == "500") {
          this.loadingSubmit = false;

          this.raise_error = result.response.message
          this.toastr.error(this.raise_error, "Error")
          this.RaiseCall.controls["cust_preference_date"].setValue(this.model)

          this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)

        }
        else {
          this.loadingSubmit = false;
          this.raise_error = "Something went wrong"
          this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
          this.RaiseCall.controls["cust_preference_date"].setValue(this.model)

        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
        this.loadingSubmit = false;
        this.RaiseCall.controls["cust_preference_date"].setValue(this.model)

        this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)

      });
    // }
    // }
  }
  cus_contract_ticket_serial(val,msg_success) {
    console.log(val)
    console.log(this.RaiseCall.value, "RaiseCall")
    console.log(this.productdata)
    console.log(this.Addbatterybank.value);
    console.log(this.Addbatterybank.value.invoice_date)
    if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '' || this.Addbatterybank.value.mfg_date == "undefined-undefined-undefined") {
      this.Addbatterybank.controls['invoice_date'].setValue('');
    }
    else {
      var year = this.Addbatterybank.value.invoice_date.year;
      var month = this.Addbatterybank.value.invoice_date.month;
      var day = this.Addbatterybank.value.invoice_date.day;
      var invice_date = year + "-" + month + "-" + day;
      this.Addbatterybank.controls['invoice_date'].setValue(invice_date);
    }
    console.log(this.Addbatterybank.value.cust_preference_date)
    var year = this.Addbatterybank.value.cust_preference_date.year;
    var month = this.Addbatterybank.value.cust_preference_date.month;
    var day = this.Addbatterybank.value.cust_preference_date.day;
    var cus_pre_date = year + "-" + month + "-" + day;
    this.Addbatterybank.controls['cust_preference_date'].setValue(cus_pre_date);
    if (this.Addbatterybank.value.expiry_day == undefined || this.Addbatterybank.value.expiry_day == null || this.Addbatterybank.value.expiry_day == '' || this.Addbatterybank.value.expiry_day == "undefined-undefined-undefined") {
      this.Addbatterybank.controls['expiry_day'].setValue('');
    }
    else {
      var year = this.Addbatterybank.value.expiry_day.year;
      var month = this.Addbatterybank.value.expiry_day.month;
      var day = this.Addbatterybank.value.expiry_day.day;
      // var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
      // var year = ids[0];
      // var month = ids[1];
      // var day = ids[2];
      var exdate_serial = day + "-" + month + "-" + year;
      var exdate = year + "-" + month + "-" + day;
    }
    console.log(exdate)
    if (this.Addbatterybank.value.mfg_date == undefined || this.Addbatterybank.value.mfg_date == null || this.Addbatterybank.value.mfg_date == '' || this.Addbatterybank.value.mfg_date == "undefined-undefined-undefined") {
      this.Addbatterybank.controls['mfg_date'].setValue('');
    }
    else {
      var year = this.Addbatterybank.value.mfg_date.year;
      var month = this.Addbatterybank.value.mfg_date.month;
      var day = this.Addbatterybank.value.mfg_date.day;
      // var id = this.Addbatterybank.value.mfg_date.split("-", 3)
      // var year = id[0];
      // var month = id[1];
      // var day = id[2];
      var mfdate_serial = day + "-" + month + "-" + year;
      var mfdate = year + "-" + month + "-" + day;
    }
    console.log(mfdate)
    this.Addbatterybank.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
    this.Addbatterybank.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
    console.log(this.Addbatterybank.value);
    this.serialProduct = [];
    var serial: any = {};
    this.serial = [];
    var serial_num = '';
    console.log(this.Addbatterybank.value.product_id)
    // if(this.Addbatterybank.value.product_id == null)
    // {
    //   this.Addbatterybank.value.product_id = ""
    // }
    // if(this.Addbatterybank.value.product_sub_id == null)
    // {
    //   this.Addbatterybank.value.product_sub_id = ""
    // }
    // if(this.Addbatterybank.value.model_no == null)
    // {
    //   this.Addbatterybank.value.model_no = ""
    // }
    // if(this.Addbatterybank.value.sys_ah == null)
    // {
    //   this.Addbatterybank.value.sys_ah = ""
    // }
    // if(this.Addbatterybank.value.voltage == null)
    // {
    //   this.Addbatterybank.value.voltage = ""
    // }
    // if(this.Addbatterybank.value.invoice_number == null)
    // {
    //   this.Addbatterybank.value.invoice_number = ""
    // }
    // if(this.Addbatterybank.value.invoice_date == null)
    // {
    //   this.Addbatterybank.value.invoice_date = ""
    // }  
    // if(this.Addbatterybank.value.warranty_type_id == null)
    // {
    //   this.Addbatterybank.value.warranty_type_id = ""
    // }

    serial = {
      "product_id": this.Addbatterybank.value.product_id,
      "product_sub_id": this.Addbatterybank.value.product_sub_id,
      "model": this.Addbatterybank.value.model_no,
      "sys_ah": this.Addbatterybank.value.sys_ah,
      "voltage": this.Addbatterybank.value.voltage.toUpperCase(),
      "serial_no": this.Addbatterybank.value.serial_no,
      "invoice_number": this.Addbatterybank.value.invoice_number,
      "invoice_date": this.Addbatterybank.value.invoice_date,
      "mfg_date": mfdate_serial,
      "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
      "expiry_date": exdate_serial
    };
    console.log(serial)
    console.log(this.title, "this.title")
    console.log(this.Addbatterybank.value.battery_bank_id)
    console.log(this.set)
    if (this.set == 1) {

      if (val == 1) {
        if (val == 1) {
          this.Addbatterybank.patchValue({
            'battery_bank_id': '',
          });
          var serial_num = '';
          if (this.Addbatterybank.value.product_id != null && this.Addbatterybank.value.product_sub_id != null && this.Addbatterybank.value.model_no != null && this.Addbatterybank.value.sys_ah != null && this.Addbatterybank.value.voltage != null) {

            this.serial.push(serial);
          }
          else {
            this.serial = []
          }
          serial_num = JSON.stringify(this.serial);
        }


      }
    }
    else if (this.set == 2) {
      console.log(this.Addbatterybank.value.expiry_day)

      if (this.Addbatterybank.value.expiry_day == undefined || this.Addbatterybank.value.expiry_day == null || this.Addbatterybank.value.expiry_day == '' || this.Addbatterybank.value.expiry_day == "undefined-undefined-undefined") {
        this.Addbatterybank.controls['expiry_day'].setValue('');
        exdate = null
      }
      else {

        var year = this.Addbatterybank.value.expiry_day.year;
        var month = this.Addbatterybank.value.expiry_day.month;
        var day = this.Addbatterybank.value.expiry_day.day;

        exdate = year + "-" + month + "-" + day;
      }
      console.log(exdate)
      if (this.Addbatterybank.value.mfg_date == undefined || this.Addbatterybank.value.mfg_date == null || this.Addbatterybank.value.mfg_date == '' || this.Addbatterybank.value.mfg_date == "undefined-undefined-undefined") {
        this.Addbatterybank.controls['mfg_date'].setValue('');
        mfdate = null
      }
      else {
        var year = this.Addbatterybank.value.mfg_date.year;
        var month = this.Addbatterybank.value.mfg_date.month;
        var day = this.Addbatterybank.value.mfg_date.day;

        mfdate = year + "-" + month + "-" + day;
      }
      console.log(mfdate)

      console.log(this.productdata, " this.productdata")
      this.serialProduct = [];
      this.productdata.forEach(obj => {
        var item = {
          "product_id": obj.product.product_id,
          "product_sub_id": obj.subproduct.product_sub_id,
          "voltage": obj.voltage.toUpperCase(),
          "model": obj.model.model_id,
          "sys_ah": obj.sys_ah,
          "serial_no": obj.serial_no,
          "invoice_number": obj.invoice_number,
          "invoice_date": obj.invoice_date,
          "mfg_date": obj.mfg_date,
          "warranty_type_id": obj.warranty.warranty_id,
          "expiry_date": obj.expiry_date
        }
        this.serialProduct.push(item)
      })

      console.log(this.serialProduct);
      if (this.Addbatterybank.value.product_id != null && this.Addbatterybank.value.product_sub_id != null && this.Addbatterybank.value.model_no != null && this.Addbatterybank.value.sys_ah != null && this.Addbatterybank.value.voltage != null) {
        this.serialProduct.push(serial);
      }
      console.log(this.serialProduct, "serialSaveContinue")
      var serial_num = JSON.stringify(this.serialProduct);
    }

    console.log(serial_num, "serial")
    console.log(this.Addnamesiteinfo.value.warranty_type_id);
    this.Addbatterybank.value.expiry_day = exdate
    this.Addbatterybank.value.mfg_date = mfdate
    console.log(this.RaiseCall.value.call_category_id, "this.RaiseCall.value.call_category_id")
    console.log(this.RaiseCall.value.call_category_id, "this.RaiseCall.value.call_category_id")
    if (serial_num != '') {
      var data = this.Addbatterybank.value
      data["serial_no_details"] = serial_num;
      data["employee_code"] = this.employee_code
      data["customer_id"] = this.Addnamesiteinfo.value.customer_id,
        data["customer_code"] = this.Addnamesiteinfo.value.customer_code,
        data["email_id"] = this.Addnamesiteinfo.value.email_id,
        data["contact_number"] = this.Addnamesiteinfo.value.contact_number,
        data["alternate_number"] = this.Addnamesiteinfo.value.alternate_number,
        data["plot_number"] = this.Addnamesiteinfo.value.plot_number,
        data["street"] = this.Addnamesiteinfo.value.street,
        data["landmark"] = this.Addnamesiteinfo.value.landmark,
        data["post_code"] = this.Addnamesiteinfo.value.post_code,
        data["country_id"] = this.Addnamesiteinfo.value.country_id,
        data["state_id"] = this.Addnamesiteinfo.value.state_id,
        data["city_id"] = this.Addnamesiteinfo.value.city_id,
        data["location_id"] = this.Addnamesiteinfo.value.location_id,
        data["site_id"] = this.Addnamesiteinfo.value.site_id,
        data["contact_person_name"] = this.Addnamesiteinfo.value.contact_person_name,
        data["contact_person_number"] = this.Addnamesiteinfo.value.contact_person_number,
        data["sap_reference_number"] = this.Addnamesiteinfo.value.sap_reference_number,
        data["customer_id"] = this.RaiseTicketCusInfo.value.customer_id,
        data["call_category_id"] = +this.Addbatterybank.value.call_category_id,
        data["contract_type"] = this.Addbatterybank.value.warranty_type_id,
        data["cust_preference_date"] = cus_pre_date,
        data["image"] = this.RaiseCall.value.image,
        data["problem_desc"] = this.RaiseCall.value.desc,
        // data["invoice_date"] = invice_date
        // "quantity"= this.RaiseTicketProdInfo.value.quantity,
        data["work_type_id"] = 1,
        data["priority"] = this.RaiseCall.value.proioritys,
        data["expiry_flag"] = +this.expiry_flag,
        data["employee_id"] = this.employee_id
      console.log(data)
      var url = 'cus_contract_ticket/'                                         //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        //if sucess
        if (result.response.response_code == "200") {
          // this.toastr.success(result.response.message, 'Success');
          this.modalService.dismissAll();
          this.modalsuccess(msg_success);                       //to open modal success pop-up
          this.successraise_message = result.response.message;
          this.batteryBank();

          //toastr message for success
          var parmas = {};
          this.loadingSubmit = true;
          parmas = { 'customer_code': result.response.site_battery_details.customer_code, "site_id": result.response.site_battery_details.site_id, "flag": 1 };
          var url = 'get_customer_contract_details/?';
          this.overlayRef.attach(this.LoaderComponentPortal);
          this.ajax.postdata(url, parmas).subscribe((result) => {
            if (result.response.response_code == "200") {
              this.loadingSubmit = false;
              serial_num = ""
              this.raise_data = '';
              this.error = '';
              this.site_details = [];
              this.showTable = false;
              this.customer_details = result.response.customer_details;
              this.site_details = result.response.site_details;
              this.segment_value = this.site_details.segment_id;
              this.application_value = this.site_details.application_id;
              console.log(this.segment_value, "this.segment_value")
              this.chRef.detectChanges();
              this.contact_number = this.customer_details.contact_number;
              this.customer_code = this.customer_details.customer_code;
              this.batteryData = result.response.battery_bank_array;
              if (this.batteryData.length == 1) {
                this.battery_data = 'Enable';
                this.showTable = true;
                this.getSerialNmber(this.batteryData[0]);
                this.site_details.serial_no_details.forEach(obj => {
                  var date = obj.expiry_date.split("-", 3);
                  var year = date[0];
                  var month = date[1];
                  var day = date[2];
                  obj.expiry_date = day + "-" + month + "-" + year;
                  this.productdata.push(obj);
                })
                console.log(this.productdata);
              }

              this.isShow = false;


              this.Addnamesiteinfo.patchValue({
                'customer_id': this.customer_details.customer_id,
                'customer_code': this.customer_details.customer_code,
                'customer_name': this.customer_details.customer_name,
                'ticket_id': this.customer_details.ticket_id,
                'email_id': this.customer_details.email_id,
                'contact_number': this.customer_details.contact_number,
                'alternate_contact_number': this.customer_details.alternate_number,
                'plot_number': this.site_details.plot_number,
                'battery_bank_id': this.batteryData,
                'street': this.site_details.street,
                'landmark': this.site_details.landmark,
                'post_code': this.site_details.post_code,
                'country_id': this.site_details.country.country_id,
                'state_id': this.site_details.state.state_id,
                'city_id': this.site_details.city.city_id,
                'location_id': this.site_details.location.location_name,
                'site_id': this.site_details.site_id,
                'contact_person_name': this.site_details.contact_person_name,
                'contact_person_number': this.site_details.contact_person_number,
              })

              this.getcountry();
              this.onChangeCountryas(this.site_details.country.country_id);
              this.onChangeStateas(this.site_details.state.state_id);
              this.onChangecityas(this.site_details.city.city_id);
              this.overlayRef.detach();

            }
            else if (result.response.response_code == "404") {
              this.loadingSubmit = false;
              this.customer_details = result.response.customer_details;
              var contract_detail = result.response.contract_details;
              if (contract_detail.length == '0') {
                this.error = "There is no Contract for this customer";
                this.Addnamesiteinfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                  'plot_number': '',
                  'street': '',
                  'landmark': '',
                  'post_code': '',
                  'country_id': '101',
                  'state_id': '',
                  'city_id': '',
                  'location_id': ''
                })
                this.overlayRef.detach();
                this.getcountry();
                this.Addnamesiteinfo.controls['country_id'].setValue(101);

              }
              else {
                this.error = ''
                this.isShow = false;
                this.Addnamesiteinfo.patchValue({
                  'customer_id': this.customer_details.customer_id,
                  'customer_code': this.customer_details.customer_code,
                  'customer_name': this.customer_details.customer_name,
                  'ticket_id': this.customer_details.ticket_id,
                  'email_id': this.customer_details.email_id,
                  'contact_number': this.customer_details.contact_number,
                  'alternate_contact_number': this.customer_details.alternate_number,
                })
                this.overlayRef.detach();
                this.chRef.detectChanges();
              }
              this.overlayRef.detach();
            }
            else if (result.response.response_code == "400") {
              this.loadingSubmit = false;
              this.raise_ticket_reset();
              // var default_radiovalue = '1';
              // this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
              // this.radiochange_value = 1;

              this.overlayRef.detach();
              this.customer_code = '';
              // this.Addnamesiteinfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
              this.getcountry();
              var today = new Date();
              var today_date = this.calendar.getToday()
              var year = today_date.year;
              var month = today_date.month;
              var day = today_date.day;
              this.config.maxDate = {
                day: day,
                month: month,
                year: year,
              };


            }
            else if (result.response.response_code == "500") {
              this.loadingSubmit = false;
              this.overlayRef.detach();
              this.error = result.response.message
            }
            else {
              this.loadingSubmit = false;
              this.overlayRef.detach();
              this.error = "Something went wrong"
            }
            (err) => {
              this.loadingSubmit = false;
              this.overlayRef.detach();
              console.log(err); //prints if it encounters an error
            }
          });
          // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
        }
        //if error
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
        }

      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
        // this.Addnameprodinfo.controls["cust_preference_date"].setValue(this.model)
      });
    }

  }

}