import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
// import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// import Swal from 'sweetalert2';
@Component({
  selector: 'kt-popups',
  templateUrl: './popups.component.html',
  styleUrls: ['./popups.component.scss']
})
export class PopupsComponent implements OnInit {
  constructor(public ajax: ajaxservice, private toastr: ToastrService, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) {

  }
  ngOnInit() {
  
  }
open(add){
  this.modalService.open(add, {
    size: 'lg',                                                      //specifies the size of the modal box
    backdrop: 'static',                                    // modal will not close by outside click
    keyboard: false                                       // modal will not close by keyboard click
  });
}
}
