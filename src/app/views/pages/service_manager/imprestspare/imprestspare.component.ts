import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
// import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
@Component({
	selector: 'kt-imprestspare',
	templateUrl: './imprestspare.component.html',
	styleUrls: ['./imprestspare.component.scss']
})
export class ImprestspareComponent implements OnInit {
	tab_name: any;
	employee_code: any;
	emp_code: any;
	currentJustify = 'end';
	imprest_spare_id: any;
	spare_required_details: any;
	spares: any;
	technician_data: any;
	techId: any;
	overlayRef: OverlayRef;

	rejected_spare_transfer: any;
	showTable: boolean = false;
	dataTable: any;
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTableImprest: boolean = false;
	showTableSpare: boolean = false;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	// unassigned_tckt = [];
	// assigned_tckt=[];
	spare_transfer = [];
	accepted_spare_transfer = [];
	@ViewChild('dataTable', { static: true }) table;
	constructor(private ajax: ajaxservice, private chRef: ChangeDetectorRef, private overlay: Overlay, private modalService: NgbModal, private router: Router, private toastr: ToastrService) { }

	ngOnInit() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10
			// processing: true
		}
		// this.getAcceptedImprestSpareTransfer();

		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);
		this.getImpretSpareTransfer();

	}

	getImpretSpareTransfer() {
		this.overlayRef.attach(this.LoaderComponentPortal);

		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";//parms for API
		var url = 'imprest_spare/?';                                  // api url for getting the imprest spare
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_transfer = result.response.data;
				this.showTableImprest = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();

			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message)
				this.showTableImprest = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();

			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getAcceptedImprestSpareTransfer() {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";//API params
		var url = 'imprest_spare_approved_details/?';                                  // api url for getting the imprest spare approved details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.accepted_spare_transfer = result.response.data; //Bind result with variable
				this.showTable = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();

			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}

	imprestSpareApprove(imprest_id) {
		var url = 'imprest_spare_approve/'; //API url for imprest spare approve
		var data = {// parms for API
			"imprest_spare_id": imprest_id
		}
		Swal.fire({ // Sweet alert for imprest spare approve
			title: 'Are you sure?',
			text: "You want to accept this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Accept',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {
						this.showTableImprest = false;
						this.getImpretSpareTransfer();
						this.chRef.detectChanges();
						this.toastr.success(result.response.message, 'Success');        //toastr message for success                           
					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	imprestSpareReject(imprest_id) {
		var url = 'imprest_spare_reject/'; //API URL for imprest spare reject 
		var data = {// Params for API
			"imprest_spare_id": imprest_id
		}
		Swal.fire({ //sweet alert for imprest spare reject
			title: 'Are you sure?',
			text: "You want to reject this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Reject',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {                          //if sucess
						this.showTableImprest = false;
						this.getImpretSpareTransfer();  
						this.chRef.detectChanges();
						this.toastr.success(result.response.message, 'Success');        //toastr message for success			                           
					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
		console.log(evt.nextId);
		// if(evt.nextId == 'tab-selectbyid1'){
		// 	this.getImpretSpareTransfer();
		// } 
		if (evt.nextId == 'tab-selectbyid2') {
			this.showTable = false;
			this.getAcceptedImprestSpareTransfer();

		}
		else if (evt.nextId == 'tab-selectbyid1') {
			this.showTableImprest = false;
			this.getImpretSpareTransfer();

		}
	}
	spareDetails(data, spare_required) { //get spare required details

		this.spares = data;
		var tran_id = this.spares.imprest_spare_id; // get trans id
		var imprest_spare_id = "imprest_spare_id"; // API params
		var url = 'get_imprest_spare/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, imprest_spare_id, tran_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.showTableSpare = true;
				this.spare_required_details = result.response.data;
				this.modalService.open(spare_required, {
					size: 'lg',
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	techDetails(techId, tech_details) {//GEt technician details
		this.techId = techId;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the technician details
		this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.technician_data = result.response.data; //Bind result with variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	// openLarge(add, row) {// Modal popup
	// this.employee_code = row
	// this.modalService.open(add, {
	//   size: 'lg',
	//   windowClass: "center-modalsm"                                  //specifies the size of the dialog box
	// });
	// }

	openSpare(add) {// Modal popup
		this.modalService.open(add, {
			size: 'lg'
		});
	}
}
