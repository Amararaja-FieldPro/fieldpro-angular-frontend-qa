import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
	selector: 'kt-trainingscore',
	templateUrl: './trainingscore.component.html',
	styleUrls: ['./trainingscore.component.scss']
})
export class TrainingscoreComponent implements OnInit {
	trainingscore_data: any;
	techId: any;
	assesment_data: any;
	assessment_id: any;
	technician_data: any;
	employee_code: any;
	filterTraining: FormGroup;    //form group 
	showTable: boolean = false;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	technician_details: any;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService,
		private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.filterTraining = new FormGroup({
			"employee_code": new FormControl(''),
			"assessment_id": new FormControl(''),

		})
	}

	ngOnInit() {
		var data = {"employee_code": "","assessment_id": ""	}
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);
		this.showTable=false;
		this.getTrainingScore(data);
		this.getAssessment();
		this.getTechnician();
	}
	getTechnician() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'technician_filter_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_data = result.response.data;
				this.chRef.detectChanges();
				this.dtTrigger.next();
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getAssessment() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'assessement_filter_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.assesment_data = result.response.data;
				this.chRef.detectChanges();
				this.dtTrigger.next();
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterAssesment(value: string) {
		console.log(this.filterTraining.value);
		var employee_code = this.filterTraining.value.employee_code;
		var assessment_id = this.filterTraining.value.assessment_id;
		var data = {'assessment_id': assessment_id,'employee_code': employee_code}
		this.showTable = false;
		this.getTrainingScore(data);
	}

	getTrainingScore(data) {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var url = 'training_score/?';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.showTable = true;
				this.trainingscore_data = result.response.data;
				this.chRef.detectChanges();
				this.overlayRef.detach();
			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.overlayRef.detach();
			}
			else {
				this.toastr.error( result.response.message, 'Error');
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	techDetails(techId, tech_details) {
		// this.techId = techId;    
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the technician
		this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_details = result.response.data; //bind results in this variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "400") {
				this.technician_details = result.response.data; //bind results in this variable
				// this.modalService.open(tech_details, {
				// 	size: 'lg',
				// 	windowClass: "center-modal"                                  //specifies the size of the dialog box
				//   });
				this.toastr.error(result.response.data, 'error');
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal"                                  //specifies the size of the dialog box
		});
	}
}
