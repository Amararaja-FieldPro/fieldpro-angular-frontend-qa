import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'; //to use modal box
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay'; // loader
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
// import { DataTableDirective } from 'angular-datatables';
// import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './amc.component.html',
  styleUrls: ['./amc.component.scss']
})
export class AmcComponent implements OnInit {
  amc_details: any; //get list from get API
  amcData: any; //get datas from edit API
  editAmcForm: FormGroup; //form group for create amc
  addAmcForm: FormGroup; //form group for edit amc
  showTable: boolean = false; // for loader
  overlayRef: OverlayRef; // for loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>; // for loader
  loadingAdd: boolean = false;    //adding loader for spinner
  loadingEdit: boolean =false;
  error: any;

  constructor(private modalService: NgbModal, public ajax: ajaxservice, private formBuilder: FormBuilder, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.addAmcForm = new FormGroup({
      "amc_type": new FormControl("", [Validators.required, this.noWhitespace]),
      "duration": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
      "cost": new FormControl(0),
    });
    this.getamc();

  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  getamc() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_amc_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") //if sucess
      {
        this.amc_details = result.response.data; //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();

      }
      else if (result.response.response_code == "500") {
        this.error = result.response.message;
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.error = "Something Went Wrong";
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.addAmcForm.controls; } //error logs for create AMC
  get e() { return this.editAmcForm.controls; }
  onSubmit() {
    this.loadingAdd = true;    //adding spinner in submit button 
    // stop here if form is invalid
    if (this.addAmcForm.invalid) {
      this.loadingAdd = false;    //dismissing spinner in submit button 
      return;
    }
    else {
      this.addAmcForm.controls["cost"].setValue(0)
      this.loadingAdd = true;    //adding spinner in submit button 
      var data = this.addAmcForm.value; //Data to be passed for add api
      var url = 'create_amc/' //api url for add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") { //if sucess
          this.loadingAdd = false;    //dismissing spinner in submit button 
          this.showTable = false;
          this.chRef.detectChanges();
          this.getamc(); //reloading the component
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.modalService.dismissAll(); //to close the modal box
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.loadingAdd = false;    //dismissing spinner in submit button 
          this.error = result.response.message;
        }
        else { //if not sucess
          this.loadingAdd = false;    //dismissing spinner in submit button 
          this.error = "Somethng Went Wrong"; //toastr message for error
        }
      });

    }
  }
  editAmc(data) {
    this.amcData = data;

    //form group for edit AMC
    this.editAmcForm = new FormGroup({
      "amc_id": new FormControl(this.amcData.amc_id, [Validators.required]),
      "amc_type": new FormControl(this.amcData.amc_type, [Validators.required,this.noWhitespace]),
      "duration": new FormControl(this.amcData.duration, [Validators.required, Validators.pattern("^[0-9]*$")]),
      "cost": new FormControl(this.amcData.cost),
      "status": new FormControl(this.amcData.status),
    });

  }
  //function for edit API
  updateAmc(amcData) {
    this.loadingEdit = true;
    if (this.editAmcForm.valid) {

      var url = 'edit_amc/' //api url of edit api
      this.ajax.postdata(url, this.editAmcForm.value).subscribe((result) => {
        if (result.response.response_code == "200") { //if sucess
          this.showTable = false;
          this.chRef.detectChanges();
          this.getamc(); //reloading the component
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.loadingEdit = false;
          this.modalService.dismissAll(); //to close the modal box
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.error = result.response.message;
          this.loadingEdit = false;
        }
        else { //if not sucess
          this.error = "Somethng Went Wrong"; //toastr message for error
          this.loadingEdit = false;
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
  }

  // function for modal boxes
  openLarge(content6) {
    this.addAmcForm.reset();
    this.error = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm", //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  // function for delete amc
  trashamc(data) {
    this.amcData = data;
    var amc_id = this.amcData.amc_id;
    var amcid = +amc_id;
    var url = 'delete_amc/?'; // api url for getting the details with using post params
    var id = "amc_id"; //id for using identify the amc name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.getdataparam(url, id, amcid).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.getamc(); //reloading the component
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {
            this.toastr.error(result.response.message,'Error');
          }
          else { //if not sucess
            this.toastr.error("Somethng Went Wrong",'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
  //--------------------------------------------------------------------------------------------------//

  //saranya22-dec-2020 for alphabet block in keyboard
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}