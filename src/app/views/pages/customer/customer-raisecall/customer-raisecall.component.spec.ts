import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRaisecallComponent } from './customer-raisecall.component';

describe('CustomerRaisecallComponent', () => {
  let component: CustomerRaisecallComponent;
  let fixture: ComponentFixture<CustomerRaisecallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRaisecallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRaisecallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
