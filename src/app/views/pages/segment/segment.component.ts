import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
@Component({
  selector: 'kt-segment',
  templateUrl: './segment.component.html',
  styleUrls: ['./segment.component.scss']
})
export class SegmentComponent implements OnInit {
  Addsegment: FormGroup;
  EditSegment: FormGroup;
  editServiceGroupForm: FormGroup;
  overlayRef: OverlayRef;
  UpdateCheckForm: FormGroup;
  loadingChecklist: boolean = false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  error: any;
  submitted = false;
  loadingSubmit: boolean = false;
  segment: [];
  showTables: boolean = false;
  showTable: boolean = false;
  edit_data: any;
  check_list: any;
  errors: string;
  AddCheckForm: FormGroup;
  maxChars: number;
  role: any;
  chars: number;
  question_type: any;
  loadingAdd: boolean;
  constructor(private formBuilder: FormBuilder, private overlay: Overlay, public ajax: ajaxservice, private toastr: ToastrService, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) {

    this.Addsegment = new FormGroup({
      "segment_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
    })
    this.UpdateCheckForm = new FormGroup({
      "sc_id": new FormControl('', [Validators.required]),
      "segment_id": new FormControl('', [Validators.required]),
      "description": new FormControl('', [Validators.required, this.noWhitespace]),
      "cat_type": new FormControl('', [Validators.required]),
      "display": new FormControl(''),
    });
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.showTables = false;
    this.getsegment();
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
  }
  onSubmitcheck() {
    this.submitted = true;

  }
  get log() {
    return this.Addsegment.controls;
  }
  get logedit() {
    return this.EditSegment.controls;
  }
  //function for edit service group
  editGroup(data) {
    this.edit_data = data;
    //form group for edit service group
    this.editServiceGroupForm = new FormGroup({
      "work_type_id": new FormControl(this.edit_data.work_type_id),
      "work_type": new FormControl(this.edit_data.work_type, [Validators.required, this.noWhitespace, Validators.maxLength(30)]),
    });                                         //Stores the data into the array variable

  }
  get fc() {
    return this.AddCheckForm.controls;
  }
  //function for submit add service group
  addchecklist(checklist) {
    this.error = '';
    this.errors = '';
    this.loadingChecklist = true;                                           //adding spinner in submit button 
    if (this.AddCheckForm.invalid)                                                   //Check if the package type is empty
    {
      this.loadingChecklist = false;                                        //dismissing spinner in submit button
      this.toastr.error("Please enter work type!", 'Error');                //If empty display the toast error message 
    }

    else {
      this.loadingChecklist = true;                                           //adding spinner in submit button 
      var data = this.AddCheckForm.value;                  //Data to be passed for add api
      var url = 'add_check_list/'                                         //api url for add
      data["segment_id"] = this.edit_data.segment_id;
      // data['display_value']=[this.AddCheckForm.value.display_value]
      // console.log(data)
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {                      //if sucess
                                              //dismissing spinner in submit button
          this.showTable = false;
          this.chRef.detectChanges();
          this.getcheck(checklist);
          this.loadingChecklist = false;  
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();                                 //to close the modal box
          this.AddCheckForm.reset();                                           //reloading the component
          this.modalService.open(checklist, {
            size: 'lg',                                                //specifies the size of the modal box
            windowClass: "center-modalchecklist"                                    // modal popup for resizing the popup
          });
        }
        else if (result.response.response_code == "500") {
          this.error = result.response.message;        //toastr message for error

        }
        else {
          this.loadingChecklist = false;                                        //dismissing spinner in submit button                                                          //if failure
          this.error = "Something Went Wrong";        //toastr message for error

        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  get uc() {
    return this.UpdateCheckForm.controls;
  }
  //function for edit check list
  editchecklist(data, edit_check) {
    this.UpdateCheckForm.value.display_value = ""
    this.UpdateCheckForm.setValue({
      "sc_id": data.sc_id,
      "segment_id": data.segment_id,
      "description": data.description,
      "cat_type": data.cat_type,
      "display": data.display,
    });
    this.question_type = data.cat_type;
    // console.log(this.question_type)
    this.modalService.open(edit_check, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  //API for edit check list
  updatechecklist(checklist) {
    this.submitted = true;
    this.loadingSubmit = true;
    this.error = '';
    this.errors = '';
    if (this.UpdateCheckForm.invalid)                                                   //Check if the work type is empty
    {
      this.submitted = true;
    }
    else {
      var url = 'edit_check_list/'                                      //api url of edit api
      // console.log(this.UpdateCheckForm.value)
      if (this.UpdateCheckForm.value.cat_type != 3 || this.UpdateCheckForm.value.cat_type != '3') {
        this.UpdateCheckForm.value.display_value = null
      }
      this.ajax.postdata(url, this.UpdateCheckForm.value).subscribe((result) => {
        if (result.response.response_code == "200") {                             //if sucess
          this.showTable = false;
          this.chRef.detectChanges();
          this.getcheck(checklist);                                                   //reloading the component
          this.toastr.success(result.response.message, 'Success');                 //toastr message for success
          this.loadingSubmit = false;
          this.modalService.dismissAll();                                           //to close the modal box
          this.modalService.open(checklist, {
            size: 'lg',                                                //specifies the size of the modal box
            windowClass: "center-modalchecklist"                                    // modal popup for resizing the popup
          });
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure

          this.error = result.response.message;                         //toastr message for error
          this.loadingSubmit = false;
        }
        else {
          this.error = "Something Went Wrong"        //toastr message for error
          this.loadingSubmit = false;
        }
      }, (err) => {
        console.log(err);                                    //prints if it encounters an error
      });
    }
  }
  trashchecklist(data, checklist) {
    this.error = '';
    this.errors = '';
    var url = 'delete_check_list/';             // api url for getting the details with using post params
    var dat = { sc_id: data.sc_id }
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, dat).subscribe((result) => {
          if (result.response.response_code == "200") {                 //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.getcheck(checklist);
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
            this.toastr.error(result.response.message, 'Error');        //toastr message for success
            // this.error = result.response.message;                         //toastr message for error
          }
          else {
            this.toastr.error("Something Went Wrong", 'Error');        //toastr message for success
            // this.error = "Something Went Wrong"        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  // get API for Service group
  getcheck(checklist) {
    this.error = '';
    this.errors = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_check_list/?';                                   // api url for getting the details
    var id = 'segment_id';
    var value = +this.edit_data.segment_id;
    this.ajax.getdataparam(url, id, value).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400")                               //if sucess
      {
        this.check_list = result.response.data;
        this.showTable = true;
        // console.log(this.check_list, "this.check_list")
        this.chRef.detectChanges();                          //storing the api response in the array
        this.overlayRef.detach();
        this.modalService.open(checklist, {
          size: 'lg',                                                //specifies the size of the modal box
          // windowClass: "center-modalchecklist",                                    // modal popup for resizing the popup
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
      }
      else if (result.response.response_code == "500") {
        this.error = result.response.message
        this.overlayRef.detach();
      }
      else {
        this.error = "Something Went wrong";
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  getsegment() {

    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  openLarge(content6) {
    this.error = '';
    this.Addsegment.reset();
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
    this.errors = '';


  }
  openlarge(content6) {
    this.question_type = ""
    this.error = '';
    this.AddCheckForm = new FormGroup({
      "segment_id": new FormControl(''),
      "description": new FormControl('', [Validators.required, this.noWhitespace]),
      "cat_type": new FormControl('', [Validators.required]),
      "display": new FormControl(''),
    });
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.errors = '';


  }
  editsegmentmodal(item, edit) {
    // console.log(item);
    this.EditSegment = new FormGroup({
      "segment_id": new FormControl(item.segment_id, [Validators.required]),
      "segment_name": new FormControl(item.segment_name, [Validators.required, , this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
    });
    this.modalService.open(edit, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  addsegment() {
    this.loadingAdd = true;
    var data = this.Addsegment.value;
    var url = 'add_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
     
        this.showTables = false;
        this.chRef.detectChanges();
        this.getsegment();
        this.modalService.dismissAll();
        this.loadingAdd = false;
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
      }

      else if (result.response.response_code == "400") {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else if (result.response.response_code == "500") {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd = false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  editsegment() {
    this.loadingAdd=true;
    var data = this.EditSegment.value;
    var url = 'edit_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
       
        this.showTables = false;
        this.chRef.detectChanges();
        this.getsegment();
        this.modalService.dismissAll();
        this.loadingAdd=false;
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
      }

      else if (result.response.response_code == "400") {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else if (result.response.response_code == "500") {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd=false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  detele(item) {
    // console.log(item)
    var data = { "segment_id": item };
    var url = 'delete_segment/'; // api url for delete warehouse
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        // console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showTables = false;
            this.chRef.detectChanges();
            this.getsegment();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
}
