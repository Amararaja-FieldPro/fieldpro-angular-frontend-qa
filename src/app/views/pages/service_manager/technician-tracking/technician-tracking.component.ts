import { Component, OnInit ,ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
declare var google;
import '../../../../../assets/js/maps_tracking.js'
const url = 'assets/js/maps_tracking.js';

// import * as MapTracking from 'MapTracking';
declare var MapTracking: any;
declare var Leaflet:any;
declare var Bing:any;
declare var Google:any;
declare var Map:any;
declare var Uploadfile:any;

@Component({
  selector: 'kt-technician-tracking',
  templateUrl: './technician-tracking.component.html',
  styleUrls: ['./technician-tracking.component.scss']
})
export class TechnicianTrackingComponent implements OnInit {
  currentUrl;
  loadAPI: Promise<any>;

  constructor(private chRef: ChangeDetectorRef,private router:Router) {
    // location.reload();

   }

  ngOnInit() {
    this.buttonClicked();
   }

   public buttonClicked() {
       this.loadAPI = new Promise((resolve) => {
           console.log('resolving promise...');
           this.loadScript();
       });
   }

   public loadScript() {
       console.log('preparing to load...')
       let node = document.createElement('script');
       node.src = url;
       node.type = 'text/javascript';
       node.async = true;
       node.charset = 'utf-8';
       document.getElementsByTagName('head')[0].appendChild(node);
   }
    
}
