import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagesubscriptionComponent } from './managesubscription.component';

describe('ManagesubscriptionComponent', () => {
  let component: ManagesubscriptionComponent;
  let fixture: ComponentFixture<ManagesubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagesubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagesubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
