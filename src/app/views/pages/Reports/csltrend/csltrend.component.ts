import { Component, OnInit, ViewChild,ElementRef,ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { ChartComponent } from '@syncfusion/ej2-angular-charts';
import { FormControl } from '@angular/forms';
import * as FileSaver from 'file-saver';
let series1: any[] = [];
let yValue = [7.66, 8.03, 8.41, 8.97, 8.77, 8.20, 8.16, 7.89, 8.68, 9.48, 10.11, 11.36, 12.34, 12.60, 12.95,
  13.91, 16.21, 17.50, 22.72, 28.14, 31.26, 31.39, 32.43, 35.52, 36.36,
  41.33, 43.12, 45.00, 47.23, 48.62, 46.60, 45.28, 44.01, 45.17, 41.20, 43.41, 48.32, 45.65, 46.61, 53.34, 58.53];
let point1; let i; let j = 0;
for (i = 1973; i <= 2013; i++) {
  point1 = { x: i, y: yValue[j] };
  series1.push(point1); j++;
}
import * as xlsx from 'xlsx';

import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject } from 'rxjs';

const moment = _rollupMoment || _moment;
@Component({
  selector: 'kt-csltrend',
  templateUrl: './csltrend.component.html',
  styleUrls: ['./csltrend.component.scss']
})
export class CSLTrendComponent implements OnInit {
  public data: Object[] = series1;
  public primaryYAxis: Object = {
    title: '',
    interval: 100, lineStyle: { width: 0 }, majorTickLines: { width: 0 }
  };
  public primaryXAxis: Object = {
    title: 'Months',
  };
  public chartArea: Object = {
    border: { width: 0 }
  };
  public tooltip: Object;
  public title: string = 'Customer Satisfication Level Trend: Telecom';
  public primaryXAxis1: Object;
  public chartData: Object[];
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  public title1: string;
  public primaryYAxis1: Object;
  public palette: string[];
  public chartData1: Object[];
  CSL_TREND: Object[];
  // public primaryXAxis: Object;
  public marker: Object;
  Description: Object[];
  CSLHeaders: Object[];
  ChartTrendlines: {
    //...
    //Customize the Trendline styles
    fill: string; width: number; opacity: number; dashArray: string;
  }[];
  @ViewChild('chartid', { static: false }) chartObj: ChartComponent;
  // @ViewChild('chartid', { static: true })
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  // public chartObj: ChartComponent;
  export_table_data: any[];
  showTable: string;
  dtOptions: DataTables.Settings = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  tempObj: ChartComponent;
  export_data: any;
  constructor(public ajax: ajaxservice, private calendar: NgbCalendar,private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      responsive: true,
      lengthMenu: [10, 25, 50]
    };
    this.marker = {
      dataLabel: { visible: true }
    }
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    if (this.start_date.month > 4) {
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear + 1) + "-" + '03' + "-" + '31';
      var finance = Fyear + "-" + (Fyear + 1)
    }
    else {
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance = (Fyear - 1) + "-" + Fyear
    }
    var datas = {
      "employee_code": emp_code, "start_date": this.start_date,
      "end_date": this.end_date, "service_desk": [], "state": [],
      "technician": [],
      "customer": [],
      "segment": [],
      "finance_year":finance
    };
    this.csl_chartreport(datas)
    this.primaryXAxis = { valueType: 'Category' };
    this.title = 'Customer Satisfication Level Trend';
  }
  
  updateDataMain(datas) {
    this.csl_chartreport(datas)

  }
  exportToExcel() {
    const ws: xlsx.WorkSheet =   
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'CSLTrendReport.xlsx');
   }
  // export_table() {
  //   this.excelservice.exportAsExcelFile(this.export_data, 'CSL Trend Reports');
  // }
  export() {
    this.chartObj.exportModule.export('PNG', 'CSL Trend');

  }
  public filterData(data, chartObjfilter) {

  }
  linkToggler($event) {

  }
  public csl_chartreport(data) {
    var url = 'csl_report/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10,
          lengthMenu: [10, 25, 50],
          data: result.response.table,
          search: false,
          processing: true,
          columns: [{ data: 'Year' , orderable: false}, { data: 'Apr'}, { data: 'May' }, { data: 'Jun'}, { data: 'Jul' }, { data: 'Aug' }, { data: 'Sep' }, { data: 'Oct' }, { data: 'Nov' }, { data: 'Dec' }, { data: 'Jan' }, { data: 'Feb' }, { data: 'Mar' }, { data: 'Average' }, { data: 'Total_calls' }

          ],
          "columnDefs": [
            {
              "targets": [], //first column / numbering column
              "orderable": false, //set not orderable
              "visible": false,
              "searchable": false,
            },
          ],
          order: [],
          destroy: true
        };
        $('#datatables').DataTable(this.dtOptions)
        this.chartData1 = result.response.chart;
        this.export_data = result.response.table;
        this.showTable = 'true';
        this.title = 'CSL Trend % for the period of'+ " " +"("+result.response.Financial_Year[0]+"-"+result.response.Financial_Year[1]+")";
        this.chRef.detectChanges();

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }


}
