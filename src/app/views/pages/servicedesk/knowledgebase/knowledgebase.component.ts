import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'; //to use modal box
import { ToastrService } from 'ngx-toastr';// To use toastr
import Swal from 'sweetalert2';
import { environment } from '../../../../../environments/environment';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import * as _ from 'lodash';
import { MatRadioChange } from '@angular/material'; //for matradio button
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
// import { E } from '@angular/cdk/keycodes';

// import { validateFile } from '../views/pages/servicedesk/knowledgebase';


@Component({
  selector: 'kt-knowledgebase',
  templateUrl: './knowledgebase.component.html',
  styleUrls: ['./knowledgebase.component.scss']
})
export class KnowledgebaseComponent implements OnInit {
  httplink: any;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  spare_image: any;
  //url navigations
  asideMenus: any;
  current_url: any;
  exist: any;
  submitted = false;
  submitted1 = false;
  submitted3 = false;
  RaiseTicket: FormGroup;
  Raiseticket: FormGroup;
  ProductLocation: FormGroup;
  AddknowledgeFormGeneric: FormGroup;
  RadioGroupForm: FormGroup;
  knowledgebase: any;
  selectedfile = true;
  imageChangedEvent: any = '';
  imageChanged: any = '';
  cropper_image: true;
  product_id: any;
  subproduct: any;
  knowledges: any;                            //variable to store all knowledge base
  products: any;
  knowledgeData: any;                   //get datas from edit API
  AddknowledgeForm: FormGroup;
  editknowledgeForm: FormGroup;
  upload_image: any;
  upload_video: any;
  image_or_video: any;
  approved_spare: any;
  rejected_spare: any;
  image: any;
  video: any;
  product: any;
  pb: any;
  showTable: boolean = false;
  showTables: boolean = false;
  overlayRef: OverlayRef;
  edit_image: any;
  edit_video: any;
  type: boolean = true;
  activeId: any;
  preId: any;
  evt: any;
  private prg: any;
  currentJustify = 'end';
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  productname: any;
  product_sub_id: any;
  video_validation: string;
  image_validation: string;
  radiochange_value: any;
  video_name: any;
  radio_event: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  tabvalue: any;
  knowledgesapp: any;
  technicians_details: any;
  Filenames: any;
  ByDefault: boolean = false;
  ByDefault1: boolean = false;
  image_name: any;
  imageChangedE: any;
  show: any;
  cropped_img: boolean;
  loadingUpdate: boolean = false;
  knowledge_type: any;
  error: any;
  buttonmainedit: boolean = false;
  loadingAdd: boolean = false;    //button spinner by Sowndarya
  loadingAd: boolean = false;
  maxChars: number;
  role: any;
  chars: number;
  name: any;
  image_names: any;
  // image_pattern: boolean;
  constructor(private ajax: ajaxservice, private modalService: NgbModal,
    private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef, private router: Router) {

    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "product_sub_id": new FormControl('', Validators.required)
    });

    this.RadioGroupForm = new FormGroup({
      "knowledge_type": new FormControl('Product'),
    });
    this.AddknowledgeForm = new FormGroup({
      "knowledge_type": new FormControl('Product'),
      "product_id": new FormControl('', [Validators.required]),
      "product_sub_id": new FormControl('', [Validators.required]),// product_sub_id
      "problem_desc": new FormControl('', [Validators.required, Validators.maxLength(512), this.noWhitespace]),
      "upload_image": new FormControl(''),
      "solution": new FormControl('', [Validators.required, this.noWhitespace]),
      "upload_video": new FormControl(''),
    });
    var typ_pro = this.AddknowledgeForm.value.knowledge_type
    this.AddknowledgeForm.controls['knowledge_type'].setValue(typ_pro);

    this.AddknowledgeFormGeneric = new FormGroup({
      "knowledge_type": new FormControl(''),
      "problem_desc": new FormControl('', [Validators.required, Validators.maxLength(512), this.noWhitespace]),
      "upload_image": new FormControl(''),
      "solution": new FormControl('', [Validators.required, this.noWhitespace]),
      "upload_video": new FormControl(''),
    });
    // ,Validators.pattern(/^\S*$/)
    var type_gen = this.AddknowledgeFormGeneric.value.knowledge_type
    this.AddknowledgeFormGeneric.controls['knowledge_type'].setValue(type_gen);
    this.editknowledgeForm = new FormGroup({
      "knowledge_type": new FormControl('', [Validators.required]),
      "knowledge_id": new FormControl('', [Validators.required]),
      "product_id": new FormControl('null', [Validators.required]),
      "product_sub_id": new FormControl('null', [Validators.required]),// product_sub_id
      "problem_desc": new FormControl('', [Validators.required, Validators.maxLength(512), this.noWhitespace]),
      "upload_image": new FormControl(''),
      "solution": new FormControl('', [Validators.required, this.noWhitespace]),
      "upload_video": new FormControl(''),
    });
  }
  get log() {
    return this.AddknowledgeForm.controls;                       //error logs for create user
  }
  get log1() {
    return this.ProductLocation.controls;                       //error logs for create user
  }
  get log2() {
    return this.editknowledgeForm.controls;                       //error logs for create user
  }
  get log3() {
    return this.AddknowledgeFormGeneric.controls;                       //error logs for create user
  }
  @ViewChild("videoPlayer", { static: false }) videoplayer: ElementRef;
  isPlay: boolean = false;
  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  // isControlHasError(controlName: string, validationType: string): boolean {
  //   const control = this.AddknowledgeForm.controls[controlName];
  //   console.log(control)
  //   // let isWhitespace = (control.value || '').trim().length === 0;

  //   if (!control) {
  //     return false;
  //   }

  //   const result = control.hasError(validationType) && (control.dirty || control.touched)&&(control.value || '').trim().length === 0;
  //   return result;
  // }
  // playPause() {
  //   var myVideo: any = document.getElementById("my_video_1");
  //   if (myVideo.paused) myVideo.play();
  //   else myVideo.pause();
  // }

  // makeBig() {
  //   var myVideo: any = document.getElementById("my_video_1");
  //   myVideo.width = 560;
  // }

  // makeSmall() {
  //   var myVideo: any = document.getElementById("my_video_1");
  //   myVideo.width = 320;
  // }

  // makeNormal() {
  //   var myVideo: any = document.getElementById("my_video_1");
  //   myVideo.width = 420;
  // }

  // skip(value) {
  //   let video: any = document.getElementById("my_video_1");
  //   video.currentTime += value;
  // }

  // restart() {
  //   let video: any = document.getElementById("my_video_1");
  //   video.currentTime = 0;
  // }


  ngOnInit() {
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.asideMenus = localStorage.getItem('asideMenus');
    this.current_url = this.router.url;
    const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    if (result.length <= 0) {
      localStorage.clear();

      this.router.navigate(["/login"]);
    } else {
      this.ProductLocation.value.product_id = null;
      this.ProductLocation.value.product_sub_id = null;
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      // this.overlayRef.attach(this.LoaderComponentPortal);
      this.tabvalue = 'tab-selectbyid1'
      this.onChangeLocation();
      this.getproduct();
    }

  }
  get_knowledge_details() {
    // var url = 'get_approved_knowledge_details/';
    var url = 'get_knowledge_details/'
    var data = this.ProductLocation.value;
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.knowledgesapp = result.response.data;
        this.showTable = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        this.knowledgesapp = result.response.data;
        this.showTable = true;
        this.overlayRef.detach();
      }
      else {                                                         //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  editKnowledgebase(data, EditKnowBase) {
    this.buttonmainedit = false;
    this.image_validation = '';
    this.video_validation = '';
    this.imageChangedEvent = '';
    this.error = ''
    this.Filenames = '';
    this.video_name = '';
    this.imageError='';
    this.knowledgeData = data;
    this.knowledge_type = this.knowledgeData.knowledge_type;
    this.productname = this.knowledgeData.product.product_name;
    if (this.knowledgeData.product.product_id == '-') {
      this.knowledgeData.product.product_id = '0'
    }
    if (this.knowledgeData.product.product_sub_id == '-') {
      this.knowledgeData.product.product_sub_id = '0'
    }
    // this.knowledgeData.knowledge_type = '0'
    this.editknowledgeForm.setValue({
      "knowledge_type": this.knowledgeData.knowledge_type,
      "knowledge_id": this.knowledgeData.knowledge_id,
      "product_id": this.knowledgeData.product.product_id,
      "product_sub_id": this.knowledgeData.sub_product.product_sub_id,
      "problem_desc": this.knowledgeData.problem_desc,
      "upload_image": '',
      "solution": this.knowledgeData.solution,
      "upload_video": ''
    });
    if (this.knowledgeData.upload_image != '') {
      this.spare_image = environment.image_static_ip + this.knowledgeData.upload_image;
      this.isImageSaved = true;
      this.ByDefault = true;
      this.name = "Image";
    }
    else {
      this.isImageSaved = false;
      this.ByDefault = false;
    }

    this.edit_image = this.knowledgeData.upload_image
    this.edit_video = this.knowledgeData.upload_video;
    this.image_validation = '';
    this.video_validation = '';
    this.image_or_video = data.upload_image;
    this.modalService.open(EditKnowBase, {
      size: 'lg',                                                        //specifies the size of the modal box
      windowClass: 'custommodalsm',
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
    this.onChangeProduct(this.knowledgeData.product.product_id)
  }
  getproduct() {
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.products = result.response.data;                    //storing the api response in the array

      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //get sub product details based on product id
  onChangeProduct(product_id) {
    // this.subproduct = null;
    this.ProductLocation.controls['product_sub_id'].setValue('');
    if (product_id == null) {
      return;
    }
    var product = +product_id;
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var product_name = "product_id";
    this.ajax.getdataparam(url, product_name, product).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.subproduct = result.response.data;
      }
      // else if (result.response.response_code == "400")
      // {
      //   this.subproduct = [];
      // }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    // } 

  }
  //radio change function
  onchangeradio(event: MatRadioChange) {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
    this.ByDefault = false;
    this.ByDefault1 = false;
    this.buttonmainedit = false;
    this.knowledge_reset();
    this.upload_image = '';
    this.upload_video = '';
    this.image_validation = '';
    this.video_validation = '';         //hide the image and video file format validation
    this.radiochange_value = event.value;
    this.imageChangedEvent = '';
    this.role = '';
    this.maxChars = 512;
  }

  openLarge(edit_Knowledgebase, image_or_video) {
    this.buttonmainedit = false;
    this.image_validation = '';
    this.video_validation = '';
    this.upload_image = '';
    this.upload_video = '';
    this.image_or_video = image_or_video
    this.modalService.open(edit_Knowledgebase, {
      size: 'lg',                                                        //specifies the size of the modal box
      windowClass: 'custommodalsm',
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  openmodal(view_content, image_or_video) {
    this.httplink = environment.image_static_ip;
    this.image_or_video = image_or_video
    this.modalService.open(view_content, {
      size: 'lg',                                                        //specifies the size of the modal box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  fileChangeEvents(fileInput: any) {
    console.log(fileInput.target.files[0].name);
    this.ByDefault = true;
    this.name = fileInput.target.files[0].name;
    if (this.AddknowledgeForm) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      this.image_names='';
      const allowed_types = ['image/png', 'image/jpeg'];

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.image_names='';
        this.imageError = "Only Images are allowed ( JPG | PNG  )"
        // this.toastr.error("Only Images are allowed ( JPG | PNG )")
        // this.myFiles.reduce(e.target.files)
        // this.productraise.value.invalid = true;
      }
      else {
        this.imageError = '';
        let fileList: FileList = fileInput.target.files[0];
        this.image_names = fileList;
      }
      // if (fileInput.target.files && fileInput.target.files[0]) {
      //   const max_size = 20971520;
      //   const allowed_types = ['image/png', 'image/jpeg'];
      //   const max_height = 15200;
      //   const max_width = 25600;

      //   if (fileInput.target.files[0].size > max_size) {
      //     this.imageError =
      //       'Maximum size allowed is ' + max_size / 1000 + 'Mb';
      //     this.AddknowledgeForm.value.invalid = true;
      //     this.AddknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
      //     this.cardImageBase64 = null;
      //     this.isImageSaved = false;
      //   }

      //   if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
      //     this.imageError = 'Only Images are allowed ( JPG | PNG )';
      //     this.AddknowledgeForm.value.invalid = true;
      //     this.AddknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
      //     this.cardImageBase64 = null;
      //     this.isImageSaved = false;
      //   }
      //   const reader = new FileReader();
      //   reader.onload = (e: any) => {
      //     const image = new Image();
      //     image.src = e.target.files;
      //     image.onload = rs => {
      //       const img_height = rs.currentTarget['height'];
      //       const img_width = rs.currentTarget['width'];
      //       if (img_height > max_height && img_width > max_width) {
      //         this.imageError =
      //           'Maximum dimentions allowed ' +
      //           max_height +
      //           '*' +
      //           max_width +
      //           'px';
      //         this.AddknowledgeForm.value.invalid = true;
      //         this.AddknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
      //         this.cardImageBase64 = null;
      //         this.isImageSaved = false;
      //       } else {
      //         this.isImageSaved = true
      //         this.AddknowledgeForm.value.invalid = false;
      //        this.cardImageBase64 = e.target.result;
      //        this.upload_image = e.target.files[0];
      //        ;
      //       }
      //     };
      //   };

      //   reader.readAsDataURL(fileInput.target.files[0]);
      // }
    }
    if (this.AddknowledgeFormGeneric) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      this.image_names='';
      const allowed_types = ['image/png', 'image/jpeg'];

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.image_names='';
        this.imageError = "Only Images are allowed ( JPG | PNG  )"
        // this.toastr.error("Only Images are allowed ( JPG | PNG )")
        // this.myFiles.reduce(e.target.files)
        // this.productraise.value.invalid = true;
      }
      else {
        this.imageError = '';
        let fileList: FileList = fileInput.target.files[0];
        this.image_names = fileList;
      }
      // if (fileInput.target.files && fileInput.target.files[0]) {
      //   const max_size = 20971520;
      //   const allowed_types = ['image/png', 'image/jpeg'];
      //   const max_height = 15200;
      //   const max_width = 25600;

      //   if (fileInput.target.files[0].size > max_size) {
      //     this.imageError =
      //       'Maximum size allowed is ' + max_size / 1000 + 'Mb';
      //     this.AddknowledgeFormGeneric.value.invalid = true;
      //     this.AddknowledgeFormGeneric.controls['upload_image'].setErrors({ 'incorrect': true });
      //     this.cardImageBase64 = null;
      //     this.isImageSaved = false;
      //   }

      //   if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
      //     this.AddknowledgeFormGeneric.value.invalid = true;
      //     this.imageError = 'Only Images are allowed ( JPG | PNG )';
      //     this.AddknowledgeFormGeneric.controls['upload_image'].setErrors({ 'incorrect': true });
      //     this.cardImageBase64 = null;
      //     this.isImageSaved = false;
      //   }
      //   const reader = new FileReader();
      //   reader.onload = (e: any) => {
      //     const image = new Image();
      //     image.src = e.target.files;
      //     image.onload = rs => {
      //       const img_height = rs.currentTarget['height'];
      //       const img_width = rs.currentTarget['width'];
      //       if (img_height > max_height && img_width > max_width) {
      //         this.imageError =
      //           'Maximum dimentions allowed ' +
      //           max_height +
      //           '*' +
      //           max_width +
      //           'px';
      //         this.AddknowledgeFormGeneric.value.invalid = true;
      //         this.AddknowledgeFormGeneric.controls['upload_image'].setErrors({ 'incorrect': true });
      //         this.cardImageBase64 = null;
      //         this.isImageSaved = false;
      //       } else {
      //         this.AddknowledgeFormGeneric.value.invalid = false;
      //         this.cardImageBase64 = e.target.result;
      //         this.upload_image = e.target.files[0];
      //         this.isImageSaved = true;
      //       }
      //     };
      //   };

      //   reader.readAsDataURL(fileInput.target.files[0]);
      // }
    }
    if (this.editknowledgeForm) {

      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.editknowledgeForm.value.invalid = true;
          this.editknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.editknowledgeForm.value.invalid = true;
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.editknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        } 
        this.upload_image = fileInput.target.files[0];
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.files;
          this.isImageSaved = true;
          this.editknowledgeForm.value.invalid = false;
          const imgBase64Path = e.target.result;
          this.cardImageBase64 = imgBase64Path;



        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }


  }
  removeImage() {

    if (this.AddknowledgeForm) {
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
    }
    else if (this.AddknowledgeFormGeneric) {
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
    }
    else if (this.editknowledgeForm) {
      this.imageError='';
      this.editknowledgeForm.value.invalid = false;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
    }

  }
  validateFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'png') {
      return true;
    }
    else {
      return false;
    }
  }


  fileChangeEvent(event: any): void {
    this.AddknowledgeForm.value.invalid = true;
    this.AddknowledgeFormGeneric.value.invalid = true;
    this.editknowledgeForm.value.invalid = true;
    this.isImageSaved = false;
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper
    this.Filenames = event.target.files[0].name;
    this.ByDefault = true;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.validateFile(this.Filenames)
      this.AddknowledgeForm.value.invalid = true;
      this.AddknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
      this.AddknowledgeFormGeneric.controls['upload_image'].setErrors({ 'incorrect': true });
      this.editknowledgeForm.controls['upload_image'].setErrors({ 'incorrect': true });
      this.image_validation = "Only Image will be allowed ( JPG | PNG |JPEG )";
      this.imageChangedEvent = '';
      this.buttonmainedit = false;
    }
    else {

      // this.image_pattern = false;
      this.AddknowledgeForm.value.invalid = false;
      this.AddknowledgeFormGeneric.value.invalid = false;
      this.editknowledgeForm.value.invalid = false;
      this.image_validation = "";
      this.imageChangedEvent = event.target.files[0];
      this.imageChangedEvent = event;
      this.upload_image = event.target.files[0];
      this.ByDefault = true;
      this.buttonmainedit = true;
      this.isImageSaved = true;
      console.log(this.upload_image)
    }
  }
  croppedImage: any = '';
  imageCropped(event: any): void {
    this.imageChangedEvent = event;
    this.croppedImage = event.base64;
    this.imageChangedEvent = event.base64;
  }
  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }
  fileChangeVideo(event: any): void {
    this.AddknowledgeForm.value.invalid = true;
    this.AddknowledgeFormGeneric.value.invalid = true;
    this.editknowledgeForm.value.invalid = true;
    this.video_name = event.target.files[0].name;
    this.ByDefault1 = true
    if (event.target.files.length !== 1) {
      this.AddknowledgeForm.controls['upload_video'].setErrors({ 'incorrect': true });
      this.AddknowledgeFormGeneric.controls['upload_video'].setErrors({ 'incorrect': true });
      this.editknowledgeForm.controls['upload_video'].setErrors({ 'incorrect': true });
      this.toastr.warning("Multiple files not allowed", 'Warning');        //toastr message for 
    }

    // const allowed_types = ['video/mp4'];
    const allowed_types = ['video/mp4'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.AddknowledgeForm.value.invalid = true;
      this.AddknowledgeFormGeneric.value.invalid = true;
      this.editknowledgeForm.value.invalid = true;
      this.AddknowledgeForm.controls['upload_video'].setErrors({ 'incorrect': true });
      this.AddknowledgeFormGeneric.controls['upload_video'].setErrors({ 'incorrect': true });
      this.editknowledgeForm.controls['upload_video'].setErrors({ 'incorrect': true });
      this.video_validation = "Only Video will be allowed (mp4)"
    }
    else {
      this.AddknowledgeForm.value.invalid = false;
      this.AddknowledgeFormGeneric.value.invalid = false;
      this.editknowledgeForm.value.invalid = false;
      this.video_validation = "";
      this.imageChanged = event.target.files[0];
      this.upload_video = this.imageChanged
    }
  }

  onChangeLocation() {
    if (this.ProductLocation.value.product_id == "") {
      this.ProductLocation.value.product_id = null;
      this.ProductLocation.value.product_sub_id = null;
    }
    else {
      this.ProductLocation.value.product_id = this.ProductLocation.value.product_id;
    }
    if (this.ProductLocation.value.product_sub_id == "") {
      this.ProductLocation.value.product_sub_id = null;
    }
    else {
      this.ProductLocation.value.product_sub_id = this.ProductLocation.value.product_sub_id;
    }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.overlayRef.attach(this.LoaderComponentPortal);
      this.showTables = false;
      this.get_approved_knowledgebase();

    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.overlayRef.attach(this.LoaderComponentPortal);
      this.showTable = false;
      this.get_knowledge_details();
    }

  }
  get_approved_knowledgebase() {
    var url = 'get_approved_knowledge_details/';
    var data = this.ProductLocation.value;
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.knowledges = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTables = true;
        this.overlayRef.detach();
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {                                                         //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  delete_knowledge(knowledge_id) {
    Swal.fire({ //sweet a             
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        var url = 'delete_knowledgebase/';
        var data = { "knowledge_id": knowledge_id };
        this.ajax.postdata(url, data).subscribe((result) => {                          //if sucess
          if (result.response.response_code == "200") {
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.ProductLocation.value.product_id = null;
            this.ProductLocation.value.product_sub_id = null;
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.showTables = false;
            this.chRef.detectChanges();
            this.get_approved_knowledgebase();
          }
          else if (result.response.response_code == "400") {                //if failure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                         //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
      else {
        this.showTables = true;
        this.chRef.detectChanges();
      }
    })
  }

  //-----------------------------add knowledgebase based on product----------------------------//
  add_knowledgebase(find_type) {
    this.loadingAd = true;
    this.AddknowledgeForm.value.knowledge_type = "Product";
    this.AddknowledgeForm.value.upload_image = this.image_names;
    this.AddknowledgeForm.value.upload_video = this.upload_video;
    var val = this.AddknowledgeForm.value;
    // stop here if form is invalid  
    if (this.AddknowledgeForm.valid && this.image_validation == '' && this.video_validation == '') {
      // if () {
      const formData: FormData = new FormData();         //convert the formdata
      formData.append('knowledge_type', val.knowledge_type);
      formData.append('problem_desc', val.problem_desc);
      formData.append('solution', val.solution);
      formData.append('upload_image', val.upload_image);
      formData.append('upload_video', val.upload_video);
      formData.append('product_id', val.product_id);
      formData.append('product_sub_id', val.product_sub_id);
      var method = "post";
      // var data = formData;
      var url = 'add_knowledgebase/'
      //api url of add
      this.ajax.ajaxpost(formData, method, url).subscribe(data => {
        if (data['response'].response_code == "200") {
          // this.onChangeLocation();
          // this.showTables = false;
          // this.ProductLocation.value.product_id = null;
          // this.ProductLocation.value.product_sub_id = null;
          // this.get_approved_knowledgebase();
          this.tabvalue = "tab-selectbyid1"
          this.onChangeLocation();
          this.knowledge_reset();
          this.imageChangedEvent = '';
          this.activeId = "tab-selectbyid1";
          this.preId = "tab-selectbyid3";
          this.toastr.success(data['response'].message, 'Success');        //toastr message for success
          this.loadingAd = false;

        }
        else if (data['response'].response_code == "500") {
          this.error = data['response'].message       //toastr message for error
          this.loadingAd = false;
        }
        else {
          this.error = "Something went wrong"    //toastr message for error
          this.loadingAd = false;
        }

      }, (err) => {
        this.loadingAd = false;
        this.error = "Something went wrong"                                            //prints if it encounters an error
      });
      // }
    }
  }
  //-----------------------------add knowledgebase based on Generic----------------------------//
  add_generic(find_type) {
    this.loadingAdd = true;
    // this.submitted1=true;
    //  this.submitted=false;
    var val = this.AddknowledgeFormGeneric.value;
    const formData: FormData = new FormData();         //convert the formdata
    formData.append('knowledge_type', "Generic");
    formData.append('problem_desc', val.problem_desc);
    formData.append('solution', val.solution);
    formData.append('upload_image', this.image_names);
    formData.append('upload_video', this.upload_video);
    var method = "post";
    // stop here if form is invalid  
    if (this.AddknowledgeFormGeneric.valid && this.image_validation == '' && this.video_validation == '') {
      // else {
      var data = formData;
      var url = 'add_knowledgebase/'                                         //api url of add
      this.ajax.ajaxpost(formData, method, url).subscribe(data => {
        if (data['response'].response_code == "200") {
          // this.showTables = false;
          // this.ProductLocation.value.product_id = null;
          // this.ProductLocation.value.product_sub_id = null;
          // this.get_approved_knowledgebase();
          this.tabvalue = "tab-selectbyid1";
          this.onChangeLocation();
          this.knowledge_reset();
          this.imageChangedEvent = '';
          this.activeId = "tab-selectbyid1";
          this.preId = "tab-selectbyid3";
          this.toastr.success(data['response'].message, 'Success');        //toastr message for success
          this.loadingAdd = false;
        }
        else if (data['response'].response_code == "500") {
          this.error = data['response'].message      //toastr message for error
          this.loadingAdd = false;
        }
        else {
          this.error = data['response'].message      //toastr message for error
          this.loadingAdd = false;
        }
      }, (err) => {
        this.loadingAdd = false;
        this.error = "Something went wrong"                                          //prints if it encounters an error
      });

    }
  }

  knowledge_reset() {
    this.cardImageBase64 = '';
    this.spare_image = '';
    this.isImageSaved = false;
    this.imageError = null;
    this.buttonmainedit = false;
    this.AddknowledgeForm.reset();
    this.AddknowledgeFormGeneric.reset();
    this.error = '';
    this.upload_image = '';
    this.upload_video = '';
    this.Filenames = '';
    this.video_name = '';
    this.image_validation = '';
    this.video_validation = '';
    this.imageChangedEvent = '';
    this.AddknowledgeFormGeneric.controls['knowledge_type'].setValue(this.RadioGroupForm.value.knowledge_type);         //set the base64 in create product image
  }



  //Function to call the edit api
  updateKnowledge() {
    this.loadingUpdate = true;
    if (this.editknowledgeForm.value.product_id == null ||
      this.editknowledgeForm.value.product_id == '-' ||
      this.editknowledgeForm.value.product_id == '' ||
      this.editknowledgeForm.value.product_id == 0) {
      var type = "Generic";
      this.editknowledgeForm.controls['knowledge_type'].setValue(type);
      this.editknowledgeForm.controls['product_id'].setValue('-');
      this.editknowledgeForm.controls['product_sub_id'].setValue('-');
      if (this.editknowledgeForm.value.solution == '' ||
        this.editknowledgeForm.value.problem_desc == '') {
        return this.toastr.error("All fields must be valid and required", 'Error');        //toastr message for error
      }
    }
    else {
      var type = "Product";
      this.editknowledgeForm.controls['knowledge_type'].setValue(type);
      if (this.editknowledgeForm.value.product_id == '' ||
        this.editknowledgeForm.value.product_sub_id == '' ||
        this.editknowledgeForm.value.solution == '' ||
        this.editknowledgeForm.value.problem_desc == '') {
        return this.toastr.error("All fields must be valid and required", 'Error');        //toastr message for error
      }
    }
    if (this.editknowledgeForm.valid && this.image_validation == '' && this.video_validation == '') {

      var val = this.editknowledgeForm.value;
      const formData: FormData = new FormData(); //convert the formdata
      formData.append('knowledge_id', val.knowledge_id);
      formData.append('knowledge_type', val.knowledge_type);
      formData.append('problem_desc', val.problem_desc);
      formData.append('solution', val.solution);
      formData.append('product_id', val.product_id);
      formData.append('product_sub_id', val.product_sub_id);
      console.log(val.upload_image)
      if (this.upload_image == '' || this.upload_image == undefined) {
        // formData.append('upload_image',this.knowledgeData.upload_image)
        formData.append('upload_image', this.spare_image);
      }
      else {
        formData.append('upload_image', this.upload_image);
      }
      if (this.upload_video == '' || this.upload_video == undefined) {
        formData.append('upload_video', this.edit_video);
      }
      else {
        formData.append('upload_video', this.upload_video);
      }

      var method = "post";
      var data = formData;
      var url = 'edit_knowledgebase/' //api url of add
      this.ajax.ajaxpost(formData, method, url).subscribe(data => {
        if (data['response'].response_code == "200") {

          this.toastr.success(data['response'].message, 'Success'); //toastr message for success   
          this.loadingUpdate = false;
          this.editknowledgeForm.value.upload_image = '';
          this.editknowledgeForm.value.upload_video = '';
          this.overlayRef.attach(this.LoaderComponentPortal);
          this.ProductLocation.value.product_id = null;
          this.ProductLocation.value.product_sub_id = null;
          this.showTables = false;
          this.chRef.detectChanges();
          this.get_approved_knowledgebase();
          this.Filenames = '';
          this.video_name = '';
          this.image_validation = '';
          this.video_validation = '';
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (data['response'].response_code == "500") {
          this.error = data['response'].message//toastr message for error
          this.loadingUpdate = false;
        }
        else {
          this.error = data['response'].message//toastr message for error
          this.loadingUpdate = false;
        }
      }, (err) => {
        this.loadingUpdate = false;
        this.error = "Something went wrong"
      });
    }
  }
  approve_knowledgebase(knowledge_id) {
    var url = 'approve_knowledgebase/'; //API url for spare request approve
    var data = { "knowledge_id": knowledge_id }
    Swal.fire({ //sweet alert for accptance
      title: 'Are you sure?',
      text: "You want to accept it!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Accept',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {                         //if sucess
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.ProductLocation.value.product_id = null;
            this.ProductLocation.value.product_sub_id = null;
            this.showTable = false;
            this.chRef.detectChanges();
            this.get_knowledge_details();
          }
          else if (result.response.response_code == "400") {                //if failure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                         //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
      // else{
      //   this.showTable = true;
      //   this.chRef.detectChanges();
      // }
    })
  }


  reject_knowledgebase(knowledge_id) {
    var url = 'reject_knowledgebase/'; //API url for spare request approve
    var data = { "knowledge_id": knowledge_id }
    Swal.fire({ //sweet alert for accptance
      title: 'Are you sure?',
      text: "You want to reject it!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Reject',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {                         //if sucess
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.overlayRef.attach(this.LoaderComponentPortal);
            this.ProductLocation.value.product_id = null;
            this.ProductLocation.value.product_sub_id = null;
            this.showTable = false;
            this.chRef.detectChanges();
            this.get_knowledge_details();                                            //reloading the component
          }
          else if (result.response.response_code == "400") {                //if failure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                         //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
      else {
        this.showTable = true;
        this.chRef.detectChanges();
      }
    })
  }

  fetchNews(evt: any) {
    this.buttonmainedit = false;
    this.tabvalue = evt.nextId;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['product_sub_id'].setValue('');
    if (this.tabvalue == 'tab-selectbyid1') {
      this.ProductLocation.value.product_id = null;
      this.ProductLocation.value.product_sub_id = null;
      this.overlayRef.attach(this.LoaderComponentPortal);
      this.showTables = false;
      this.get_approved_knowledgebase();

    }
    else if (this.tabvalue == 'tab-selectbyid2') {
      this.ProductLocation.value.product_id = null;
      this.ProductLocation.value.product_sub_id = null;
      this.overlayRef.attach(this.LoaderComponentPortal);
      this.showTable = false;
      this.get_knowledge_details();

    }
    else if (evt.nextId == 'tab-selectbyid3') {
      this.knowledge_reset();
      this.activeId = "tab-selectbyid3";
      this.preId = "tab-selectbyid2";
      this.radiochange_value = 'Product';
      this.image_validation = '';
      this.video_validation = '';
      this.imageChangedEvent = '';
      this.error = '';
      this.role = '';
      this.maxChars = 512;
    }
  }
  imageClear() {
    this.ByDefault = false;
    this.imageChangedEvent = '';
    this.buttonmainedit = !this.buttonmainedit;
  }
}
export function fileextension(controlName, validationType): ValidationErrors | null {
  if (!_.includes(validationType, controlName.target.files[0].type)) {
    return { validLname: true };
  }
  return null;
}