import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketagereportComponent } from './ticketagereport.component';

describe('TicketagereportComponent', () => {
  let component: TicketagereportComponent;
  let fixture: ComponentFixture<TicketagereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketagereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketagereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
