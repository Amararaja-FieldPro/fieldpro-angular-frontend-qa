import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';  //loader
import { ComponentPortal } from '@angular/cdk/portal'; //loader
import { LoaderComponent } from '../../loader/loader.component';//loader
import { environment } from '../../../../../environments/environment';

declare var require: any
const FileSaver = require('file-saver');
// import * as objectPath from 'object-path';
@Component({
	selector: 'kt-sparerequest',
	templateUrl: './sparerequest.component.html',
	styleUrls: ['./sparerequest.component.scss']
})
export class SparerequestComponent implements OnInit {
	tab_name: any;
	reject_url: any;
	params: any;
	employee_code: any;
	// spare_request:any;
	spare_transfer_id: any;
	// spare_request_details:any;
	spares: any;
	technician_data: any;
	techId: any;
	accepted_spare_request: any;
	sapreRejectReason: FormGroup;
	// rejected_spare_request:any;
	ticket_data: any;
	currentJustify = 'end';
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	dataTable: any;
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	spare_request = [];
	rejected_spare_request = [];
	spare_request_details = [];
	showTableSpare: boolean = false;
	showTableApproved: boolean = false;
	showTableNew: boolean = false;
	showTableRejected: boolean = false;
	error: any;
	@ViewChild('dataTable', { static: true }) table;
	url: string;
	title: any;
	httplink: string;
	image_or_video: any;
	constructor(private formBuilder: FormBuilder,
		private fb: FormBuilder, private ajax: ajaxservice, private render: Renderer2, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.sapreRejectReason = new FormGroup({
			"reject_reason": new FormControl("", [Validators.required]),
		});
	}
	get log() {
		return this.sapreRejectReason.controls;
	}
	ngOnInit() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10,
			processing: false
		};
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.overlayRef.attach(this.LoaderComponentPortal);
		this.getSpareRequest();

	}

	getSpareRequest() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'new_sapre/?';                                  // api url for getting the Spare request
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_request = result.response.data;
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach(); //moving loader after response
				this.chRef.detectChanges();
				this.dtTrigger.next();
				// this.rerender(); 
			}
			else if (result.response.response_code == "500") {
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();
			}
			else {
				this.toastr.error(result.response.message, 'Error');
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getAcceptedSpareRequest() {
		this.overlayRef.attach(this.LoaderComponentPortal); //adding loader oncliking the tab
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'sapre_approved_details/?';                                  // api url for getting the Spare request approve details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.accepted_spare_request = result.response.data;
				this.showTableApproved = true; //table shown only after response
				this.overlayRef.detach(); //detaching the loader after response
				this.chRef.detectChanges();
				this.dtTrigger.next();
				// this.ngOnInit(); 
				// 	  alert(this.dataTable);
				// 	  document.createEvent('MouseEvents');
				// 	  this.render.setAttribute(this.dtOptions.processing, 'data-ktmenu-dropdown-timeout', objectPath.get(this.accepted_spare_request, 'aside.menu.submenu.dropdown.hover-timeout'));
			}
			else if (result.response.response_code == "500") {
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();
				this.error = result.response.message;
			}
			else {
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getRejectedSpareRequest() {
		this.overlayRef.attach(this.LoaderComponentPortal);  //adding loader oncliking the tab
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'spare_reject_details/?';                                  // api url for getting the Spare request reject 
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.rejected_spare_request = result.response.data;
				this.showTableRejected = true;
				this.overlayRef.detach();  //detaching the loader after response
				this.chRef.detectChanges();
				// this.rerender(); 
			}
			else if (result.response.response_code == "500") {
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();
				this.toastr.error(result.response.message, 'Error');

			}
			else {
				this.showTableNew = true; //table shown only after response
				this.overlayRef.detach();

			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});


	}
	spareApprove(ticket_id, spare_accept_reason) {
		this.sapreRejectReason.reset()
		Swal.fire({ //sweet alert for accptance
			title: 'Are you sure?',
			text: "You want to accept this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Accept'
		}).then((result) => {
			if (result.value) {				
				var employee_code =localStorage.getItem('employee_code'); // get employee code 
				var url = 'spare_approve/'; //API url for spare request approve
				var data = {//params for API
					"ticket_id": ticket_id,
					"employee_code": employee_code
				}	
				this.title="Spare accept reason"
			
				this.modalService.open(spare_accept_reason, {
					size: 'sm',
					// windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
				this.url = url;
				this.params = data
			}
		})
	}
	spareReject(ticket_id, spare_reject_reason) {
		this.sapreRejectReason.reset()
		Swal.fire({//swwet alert for reject or cancel
			title: 'Are you sure?',
			text: "You want to reject this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Reject'
		}).then((result) => {
			if (result.value) {
				var url = 'spare_reject/';
				var data = {
					"employee_code": localStorage.getItem('employee_code'),
					"ticket_id": ticket_id,
				}
				this.title="Spare reject reason"
				this.modalService.open(spare_reject_reason, {
					size: 'sm',
					// windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
				this.url = url;
				this.params = data
			}
		})
	}
	sparePopApp() {
		console.log(this.url)
		console.log(this.params)
		var url = this.url;
		var data = this.params;
		console.log(this.sapreRejectReason.value.reject_reason)
		data['reason'] = this.sapreRejectReason.value.reject_reason;
		console.log(data,"data")
		if (this.sapreRejectReason.valid) {
			this.ajax.postdata(url, data).subscribe((result) => {
				if (result.response.response_code == "200") {                          //if sucess
					// this.ngOnInit();                                               //reloading the component
					this.showTableNew= false;
					this.getSpareRequest();
					this.chRef.detectChanges();
					this.toastr.success(result.response.message, 'Success');        //toastr message for success
					this.modalService.dismissAll(); //to close the modal box                     
				}
				else if (result.response.response_code == "400") {                //if failure
					this.toastr.error(result.response.message, 'Error');            //toastr message for error
				}
				else {                                                         //if not sucess
					this.toastr.error(result.response.message, 'Error');        //toastr message for error
				}
			}, (err) => {
				console.log(err);                                        //prints if it encounters an error
			});

		}
	}
	
	fetchNews(evt: any) {
		if (evt.nextId == "tab-selectbyid1") {
			console.log(evt.nextId);
			this.showTableNew = false;
			this.getSpareRequest();
		}
		else if (evt.nextId == "tab-selectbyid2") {
			console.log(evt.nextId);
			this.showTableApproved = false;
			this.getAcceptedSpareRequest();
		}
		else if (evt.nextId == "tab-selectbyid3") {
			console.log(evt.nextId)
			this.showTableRejected = false;
			this.getRejectedSpareRequest();
		}
		console.log(evt); // has nextId that you can check to invoke the desired function
	}
	spareDetails(data, spareDetails) { //get Spare Details
		this.spares = data;
		var tk_id = this.spares.ticket_id;
		var ticket_id = "ticket_id";
		var url = 'spare_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_request_details = result.response.data; //bind result in this variable
				this.showTableSpare = true;
				this.modalService.open(spareDetails, {
					size: 'lg',
					// windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
				console.log(this.spare_request_details);
				// this.rerender(); 
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',
					// windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	techDetails(techId, tech_details) {
		this.techId = techId;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the technician data
		this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.technician_data = result.response.data; //bind results in this variable\
				console.log(this.technician_data.ticket_id);
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	  // function to download the setr downlad
	  downloadPdf(ticket_id) {
		this.overlayRef.attach(this.LoaderComponentPortal);

		var id = "ticket_id";
		var url = "setr_pdf/?";
		var value = ticket_id;
		this.ajax.getPdfDocument(url, id,value).subscribe((response) => {
		  console.log(response, "response")
		  // let file = new Blob([response], { type: 'application/pdf' });
		  let file = response.url
		  console.log(response.url, "fileURL")
		  // var fileURL = URL.createObjectURL(file);
		  // window.open(file);  
		  FileSaver.saveAs(file, value);
		  this.overlayRef.detach();

		}, (err) => {
			this.overlayRef.detach();

		  console.log(err);  //prints if it encounters an error
		});  
	  
	  }
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modalsm"                                  //specifies the size of the dialog box
		});
	}
	openSpare(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg'
		});
	}
	openmodal(view_content, image_or_video) {
		this.httplink = environment.image_static_ip;
		this.image_or_video = image_or_video
		console.log(this.image_or_video)
		this.modalService.open(view_content, {
		  size: 'lg',                                                        //specifies the size of the modal box
		  backdrop: 'static',              //popup outside click by Sowndarya
		  keyboard: false
		});
	  }
}
