import { AfterViewInit, Component, ElementRef, OnInit, ViewChild , ChangeDetectorRef} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';

//image cropper


@Component({
	selector: 'kt-newsubscription',
	templateUrl: './newsubscription.component.html',
	styleUrls: ['./newsubscription.component.scss']
})
export class NewsubscriptionComponent implements OnInit {
	stateInfo: any = [];
  countryInfo: any = [];
  cityInfo: any=[];

	@ViewChild('wizard', { static: true }) el: ElementRef;
	isLinear = true;
	companydetailsform: FormGroup;
	submitted = false;
	cropped_img = true;
	fileData: File = null;
	previewUrl:any = null;
	fileUploadProgress: string = null;
	uploadedFilePath: string = null;
	value: any;
	company_logo: any;
	url :string = "https://raw.githubusercontent.com/sagarshirbhate/Country-State-City-Database/master/Contries.json";
	country: any;
	com_pany: any;
	submit_btn: any;
	con_name: any;
	no_of_license: any;
	con_number:any;
	email_add: any;
	s_package: any;
	s_plan: any;
	full_address: any;
	pack_amt: any;
	country_disp :any;
	selectedcountry:any;
	selectedstate:any;
	selectedfile=true;

	constructor(private http:HttpClient,
		 private formBuilder: FormBuilder,
		 private fb: FormBuilder,
    private cd: ChangeDetectorRef, 

		 ) {

		this.companydetailsform = new FormGroup({
			"company_name": new FormControl("", [Validators.required]),
			"company_logo": new FormControl("", [Validators.required]),
			"contact_name": new FormControl("", [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]),
			"no_of_license": new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
			"contact_number": new FormControl("", [Validators.required, Validators.maxLength(10),Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
			"alt_number": new FormControl("", [Validators.maxLength(10),Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
			"email_address": new FormControl("", [Validators.required, Validators.email]),
			"subscription_package": new FormControl("", [Validators.required]),
			 "subscription_plan": new FormControl("", [Validators.required]),
			"door_no": new FormControl("", [Validators.required,Validators.maxLength(4)]),
			"street": new FormControl("", [Validators.required]),
			"town": new FormControl("", [Validators.required]),
			"land_mark": new FormControl(""),
			"city": new FormControl("", [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]),
			"country": new FormControl("", [Validators.required]),
			"state": new FormControl("", [Validators.required]),
			"code": new FormControl("", [Validators.required]),
			"package_amount": new FormControl(""),

		});
	

	}
	get log() {
		return this.companydetailsform.controls;
	}
	fileProgress(fileInput: any) {
		this.fileData = <File>fileInput.target.files[0];
		this.preview();
	}
	preview() {
		// Show preview 
		var mimeType = this.fileData.type;
		if (mimeType.match(/image\/*/) == null) {
		  return;
		}
	 
		var reader = new FileReader();      
		reader.readAsDataURL(this.fileData); 
		reader.onload = (_event) => { 
		  this.previewUrl = reader.result; 
		}
	}


	list_package1: any = ['1 month', '12 months', '6 months' , '24 months']; 
  


	next_step(){

	//Confirm details wizard 
	var com_name =  this.companydetailsform.value.company_name;
	this.com_pany =com_name;

	var license = this.companydetailsform.value.no_of_license;
	this.no_of_license = license;

	var cont_name = this.companydetailsform.value.contact_name;
	this.con_name = cont_name;

	var cont_num = this.companydetailsform.value.contact_number;
	this.con_number = cont_num;

	var ad_email = this.companydetailsform.value.email_address;
	this.email_add = ad_email;

	var pac_amt = this.companydetailsform.value.package_amount;
	this.pack_amt = pac_amt;


	var door_num = this.companydetailsform.value.door_no;
	var street_add = this.companydetailsform.value.street;
	var town_add = this.companydetailsform.value.town;
	var city_add = this.companydetailsform.value.city;
	var country_add = this.selectedcountry.CountryName;
	var state_add = this.selectedstate.StateName;
//To get full address
	this.full_address = door_num+","+street_add+","+town_add+","+city_add+","+country_add+","+state_add;

	var sub_pack = this.companydetailsform.value.subscription_package;
	if(sub_pack == "")
	{
		this.s_package = "Free trial";
	}
	else{
		this.s_package = sub_pack;
	}

	var sub_plan = this.companydetailsform.value.subscription_plan;
	this.s_plan = sub_plan;

//Button text based on subscription package 
	
	console.log(sub_pack);
	
		if(sub_pack == ""){
			this.submit_btn = "Submit";

		}
		else {
			this.submit_btn = "Proceed to pay";

		}

	}

//Functions for image cropper
imageChangedEvent: any = '';
croppedImage: any = '';
    
	fileChangeEvent(event: any): void {
		this.selectedfile = false; //show confirm button
		this.cropped_img = true; // image cropper 
		//console.log(event);
        this.imageChangedEvent = event;
	}
	
	// imageCropped(event: ImageCroppedEvent) {
    //     this.croppedImage = event.base64;
	// }
	
	imageLoaded() {
        // show cropper
	}
	
	cropperReady() {
        // cropper ready
	}
	
	loadImageFailed() {
        // show message
	}
	
upload_img(){ //button to clear the cropper
		this.cropped_img = false;
		this.selectedfile=true;
	}

	ngOnInit() {
 	this.getCountries();


	}
	allCountries(): Observable<any>{
		return this.http.get(this.url);
	  }
	getCountries(){
		 this.allCountries().
		 
		subscribe(

		  data2 => {
			this.countryInfo=data2.Countries;
			console.log(data2);
			console.log('Data:', this.countryInfo);
		  },
		  err => console.log(err),
		  () => console.log('complete')
		)
	  }
	//select country change event
	  onChangeCountry(countryValue) {
		  console.log(this.countryInfo[countryValue])
		  this.selectedcountry = this.countryInfo[countryValue];
		this.stateInfo=this.countryInfo[countryValue].States;
		
	  }
	//select state change event
	  onChangeState(stateValue) {
		  console.log(this.stateInfo[stateValue]);
		this.selectedstate = this.stateInfo[stateValue];
		this.cityInfo=this.stateInfo[stateValue].Cities;
		
	  }
		
	submitform()
	{
		if (this.companydetailsform.valid) {
			console.log("success");
			console.log(this.companydetailsform.value)
		}
		else
		{
			console.log(this.companydetailsform.value)
		}

	}
	
	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
		});

		// Validation before going to next page
		wizard.on('beforeNext', function (wizardObj) {
			

			
		});

		// Change event
		wizard.on('change', function (wizard) {
			setTimeout(function () {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	
	onSubmit() {
		
		this.submitted = true;
	}
}
