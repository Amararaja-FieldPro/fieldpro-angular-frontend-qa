import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { NgbModal, NgbDateStruct, NgbDate, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ToastrService } from 'ngx-toastr';// To use toastr
// import { RequestOptions, Headers } from '@angular/http';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
// import { Reset } from '@ngrx/store-devtools/src/actions';
// import { select } from '@ngrx/store';
import { ExcelService } from '../../../../excelservice';
declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'kt-new-tickets',
  templateUrl: './new-tickets.component.html',
  styleUrls: ['./new-tickets.component.scss']
})
export class NewTicketsComponent implements OnInit {
  [x: string]: any;
  expiry: string;
  servicedesk_id: any;
  batteryBankArray: any = [];
  FiscalYearStartDate: NgbDateStruct;
  tabvalue: any;
  tab_name: any;
  ProductLocation: FormGroup;
  ViewTechnicianDetails: FormGroup;
  EditDiffTicketForm: FormGroup;
  EditTicketstatus: FormGroup;
  products: any;
  locations: any;
  product_id: any
  location_id: any;
  showTable: boolean = false;
  avilable_technicians_details: any;
  ticket_id: any;
  edit_tiket_id: any;
  tickt_id: any;
  currentJustify = 'end';
  SpareTechTable: boolean = false;
  labelwarranty: string;
  Addbatterybank: FormGroup;
  expire: any;
  // assigned_tckt: any;
  // unassigned_tckt: any;
  // deffered_tckt: any;
  // accepted_tckt: any;
  ticket_details: any;
  technicians_details: any;
  file_name: FileList;
  employee_code: any;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  task_counts: any;                   //variable to store the task count for the technician
  // var nums:number[] = [1,2,3,3]
  dataTable: any;
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  unassigned_tckt = [];
  assigned_tckt = [];
  accepted_tckt = [];
  deffered_tckt = [];
  showTableAssigned: boolean = false;  //add a variable which will shown table only when a api response completed
  showTableAccepted: boolean = false;  //add a variable which will shown only when a api response completed
  showTableDiffered: boolean = false;  //add a variable which will shown only when a api response completed
  // @ViewChild('dataTable', { static: true }) table;
  product_details: any;
  sub_product: any;
  product_name: any;
  model_id: any;
  subi_d: any;
  warranty_id: any;
  country: any;
  states: any;
  cities: any[];
  location_ids: any;
  application: any;
  segment: any;
  site_details: any;
  serials: any;
  productdata: any[];
  name: any;
  rejected_tckt: any;
  showTables: boolean;
  sapreAcceptReason: FormGroup;
  sapreRejectReason:FormGroup;
  ticket_ids: any;
  Accept: any;
  Reject:any;
  showTableedit: boolean;
  display_spareAccept: string;
  editdate: boolean;
  datatable: string;
  site: number;
  serialNumbersdata: any;
  serial_error: string;
  flag: any;
  product: any;
  subproduct: any[];
  products_id: any;
  subid: any;
  warranty_type: any[];
  mfg_date: any;
  serial: any[];
  serialProduct: any[];
  title: string;
  work_type: any;
  call: any;
  prioritys: string;
  edit_ticket: any;
  ExpiryDate: any;
  ticket_details_export: any;
  settingsObj: any = {
    "order": [[1, "asc"]],
    deferRender: true,
    scrollY: 200,
    scrollCollapse: true,
    scroller: true
  }
  table: any;
  display_sparereject:string;
  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef, private calendar: NgbCalendar, private excelservice: ExcelService,
    public modalService: NgbModal, private overlay: Overlay,
    private toastr: ToastrService,) {
    this.sapreAcceptReason = new FormGroup({
      "accept_reason": new FormControl("", [Validators.required]),
    });
    this.sapreRejectReason = new FormGroup({
      "reject_reason": new FormControl("", [Validators.required]),
    });
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required)
    })
    this.EditTicketstatus = new FormGroup({
      "employee_code": new FormControl(this.employee_code),
      "customer_code": new FormControl(null),
      "customer_name": new FormControl('', Validators.required),
      "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "saf_reference_number": new FormControl(""),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_id": new FormControl(""),
      "contact_person_name": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl('', [Validators.required]),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
      "product_id": new FormControl('', [Validators.required]),
      "product_sub_id": new FormControl('', [Validators.required]),
      "model_no": new FormControl('', [Validators.required]),
      "serial_no": new FormControl('', [Validators.required]),
      "contract_duration": new FormControl('', [Validators.required]),
      "battery_bank_id": new FormControl(''),
      "sys_ah": new FormControl('', [Validators.required]),
      "voltage": new FormControl('', [Validators.required]),
      "segment_id": new FormControl('', [Validators.required]),
      "application_id": new FormControl('', [Validators.required]),
      "organization_name": new FormControl('', [Validators.required]),
      "expiry_day": new FormControl('', [Validators.required]),
      "mfg_date": new FormControl('', [Validators.required]),
      "warranty_type_id": new FormControl('', [Validators.required]),
      "ticket_status": new FormControl('', [Validators.required]),
      "image": new FormControl(""),
      "cust_preference_date": new FormControl(this.calendar.getToday(), [Validators.required,]),
      "proioritys": new FormControl(''),
      "call_category_id": new FormControl("", [Validators.required]),
      "work_type_id": new FormControl(""),
      "desc": new FormControl("", [Validators.required, this.noWhitespace])

    })
    this.EditDiffTicketForm = new FormGroup({
      "ticket_id": new FormControl('', Validators.required),
      "cust_preference_date": new FormControl('', Validators.required)
    })
    this.table = $('#datatables').DataTable();
  }
  get logaccept() {
    return this.sapreAcceptReason.controls;
  }
  get logreject() {
    return this.sapreRejectReason.controls;
  }
  sparePopRej() {
    this.Reject = this.sapreRejectReason.value.reject_reason;
  }
  // ticket_bulk_upload
  ngOnInit() {
    this.labelwarranty = "Select Warranty type";
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
      // processing: true
    }
    this.get_ticket_product_location();
    // this.getdetails();
    this.product_id = null
    this.location_id = null
    this.tabvalue = "tab-selectbyid1";
    this.employee_code = localStorage.getItem('employee_code');
    console.log(this.employee_code);
    this.servicedesk_id = localStorage.getItem('employee_code')
    this.get_unassigned_tickets();
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);


  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  getdetails() {
    var url = 'get_all_customer_contract/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.customerdetail = result.response.data;
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // Excel download
  exportAsXLSXdown(): void {
    this.excelservice.exportAsExcelFile(this.customerdetail, 'customer');
  }
  fileUpload(event) {
    this.ByDefault = true;
    this.error = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
  };
  // --------------------------------function for view the modal box------------------------------------------------------//
  openmodal(modal) {
    this.file_name = undefined;             //refresh the file
    this.Filename = '';                 //refresh the file name
    this.error = '';                      //refresh the error message
    this.bulk = ''                     //refresh the error from api
    this.modalService.open(modal, {
      size: 'lg',
      // windowClass: "center-modalsm",
    });
  }
  customerfilesubmit(bulkupload) {
    this.bulk = [];
    this.loadingUpload = true;
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.bulk = '';
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.bulk = '';
        this.error = "Only Excel File will be allowed ( xlsx )"
      }
      else if (validExts = this.type) {
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();
        formData.append('excel_file', file);
        var method = "post";
        var url = 'cus_ticket_bulkupload/'
        var i: number;
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
            this.loadingUpload = false;

            // alert('200');// this.file_name = undefined;             //refresh the file
            // this.Filename = '';                     //refersh the file name
            // if (data['success'].length > 0) {
            //   for (i = 0; i < data['success'].length; i++) {
            //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
            //     this.loadingUpload = false;
            //     // this.bulk = '';
            //     // this.modalService.dismissAll();                                //to close the modal box       
            //   }
            if (data['success'].length > 0) {
              // alert("success")
              this.loadingUpload = false;                    //sucess response
              for (i = 0; i < data['success'].length; i++) {
                // this.bulk = '';
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.loadingUpload = false;
                this.showTable = true;
                this.chRef.detectChanges();
                this.get_unassigned_tickets();                                //to close the modal box
              }
            }


            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );
                });
                this.bulk = a;

              }
            }
          }
          else if (data['response'].response_code == "500") {
            // this.bulk = [];
            console.log(data['response'].message)
            this.error = data['response'].message
            this.loadingUpload = false;
          }
          else {
            // this.bulk = [];
            this.error = "Something went wrong."      //toastr message for error
            this.loadingUpload = false;
          }
        },
          (err) => {
            // this.bulk = '';
            this.error = "Something went wrong"          //prints if it encounters an error
            this.loadingUpload = false;
          });

      }
    }
  }
  filesubmit(bulkupload) {
    this.bulk = [];
    this.loadingUpload = true;
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.bulk = '';
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.bulk = '';
        this.error = "Only Excel File will be allowed ( xlsx )"
      }
      else if (validExts = this.type) {
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();
        formData.append('excel_file', file);
        var method = "post";
        var url = 'ticket_bulk_upload/'
        var i: number;
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
            this.loadingUpload = false;
            // this.file_name = undefined;             //refresh the file
            // this.Filename = '';                     //refersh the file name
            // if (data['success'].length > 0) {
            //   for (i = 0; i < data['success'].length; i++) {
            //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
            //     this.loadingUpload = false;
            //     // this.bulk = '';
            //     // this.modalService.dismissAll();                                //to close the modal box       
            //   }
            if (data['success'].length > 0) {
              this.loadingUpload = false;                    //sucess response
              for (i = 0; i < data['success'].length; i++) {
                // this.bulk = '';
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.loadingUpload = false;
                this.showTable = true;
                this.chRef.detectChanges();
                this.get_unassigned_tickets();                                //to close the modal box
              }
            }


            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );
                });
                this.bulk = a;

              }
            }
          }
          else if (data['response'].response_code == "500") {
            // this.bulk = [];
            console.log(data['response'].message)
            this.error = data['response'].message
            this.loadingUpload = false;
          }
          else {
            // this.bulk = [];
            this.error = "Something went wrong."      //toastr message for error
            this.loadingUpload = false;
          }
        },
          (err) => {
            // this.bulk = '';
            this.error = "Something went wrong"          //prints if it encounters an error
            this.loadingUpload = false;
          });

      }
    }
  };
  allbattery_id() {
    this.batteryBankArray = "All";
  }
  batteryBankClick(event, item) {
    console.log(item)
    console.log(event)
    if (event.target.checked) {
      this.serials.push(item);
      console.log(this.serials);
    }
    else {
      this.serials = this.serials.filter(({ serial_no }) => serial_no !== item.serial_no);
      console.log('unchecked');
      console.log(this.serials);
    }
  }
  getcountry() {
    var url = 'get_country/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.country = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  //get state dropdown
  onChangeCountry(country_id) {
    if (country_id != '') {
      this.EditTicketstatus.controls['city_id'].setValue("");
      this.EditTicketstatus.controls['location_id'].setValue("");
      this.EditTicketstatus.controls['state_id'].setValue("");
      var country = +country_id; // country: number
      var url = 'get_state/?'; // api url for getting the details with using post params
      var id = "country_id";
      this.ajax.getdataparam(url, id, country).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.states = result.response.data;
          this.cities = [];
          this.location_ids = [];
        } else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue('null'); //set the base64 in create product image
          this.states = null;
          this.cities = [];
          this.location_ids = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
          this.cities = [];
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
          this.cities = [];
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.states = null;
      this.location_ids = [];
      this.cities = [];
    }

  }
  onChangeserial(value, item) {
    console.log(value);
    console.log(item);
    console.log(this.productdata);
    this.productdata.forEach(e => {
      if (e.serial_no == item.serial_no) {
        e.serial_no = value;
      }
    });
    console.log(this.productdata)
  }
  onChangeState(state_id) {
    if (state_id != '') {
      this.EditTicketstatus.controls['city_id'].setValue("");
      this.EditTicketstatus.controls['location_id'].setValue("");
      var state = +state_id; // state: number
      var id = "state_id";
      var url = 'get_city/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, state).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data; //storing the api response in the array
          this.locations = [];
        }
        else if (result.response.response_code == "400") {
          this.cities = [];
          this.locations = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {

      this.locations = [];
      this.cities = [];
    }
  }
  onChangecity(cty_id) {
    if (cty_id != '') {
      this.EditTicketstatus.controls['location_id'].setValue("");
      var city = +cty_id; // state: number
      var id = "city_id";
      var url = 'get_location_details/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, city).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.location_ids = result.response.data; //storing the api response in the array
        }
        else if (result.response.response_code == "400") {
          this.location_ids = null;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.location_ids = [];
    }
  }
  getproduct() {
    var url = 'get_product_details/';
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.product_details = result.response.data;                    //storing the api response in the array
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  onChangeProduct(product_id) {
    this.product_name = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, this.product_name).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.sub_product = result.response.data;
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }
  getmodel(id) {
    this.subid = id;
    var data = { "product_id": this.product_name, "product_sub_id": this.subid };// storing the form group value
    var url = 'get_models/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.model_id = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  // getwarranty(id) {
  //   console.log(id)
  //   // if (this.Addbatterybank.value.mfg_date == undefined || this.Addbatterybank.value.mfg_date == null || this.Addbatterybank.value.mfg_date == '') {
  //   //   var mfg_date = "";
  //   //   // this.EditTicketstatus.controls['cust_preference_date'].setValue('');
  //   // }
  //   // else {
  //   //   var years = this.Addbatterybank.value.mfg_date.year;
  //   //   var months = this.Addbatterybank.value.mfg_date.month;
  //   //   var days = this.Addbatterybank.value.mfg_date.day;
  //   //   mfg_date =  days + "-" + months + "-" +years;
  //   // }
  //   var data = { "product_id": this.product_name, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
  //   var url = 'get_warranty_type/'                                         //api url of remove license
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.warranty_id = result.response.data;
  //       this.warranty_id.forEach(e => {
  //         if (e.expiry_day) {
  //           var expdates = e.expiry_day.split("-", 3);
  //           var Eday: number = +expdates[0];
  //           var Emonth: number = +expdates[1];
  //           var Eyear: number = +expdates[2];
  //         }
  //         if (e.manufacture_date) {
  //           var mdates = e.manufacture_date.split("-", 3);
  //           var Myear: number = +mdates[0];
  //           var Mmonth: number = +mdates[1];
  //           var Mday: number = +mdates[2];
  //         }
  //         const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

  //         const EDate: NgbDate = new NgbDate(Eyear, Emonth, Eday);
  //         console.log(EDate);
  //         this.Addbatterybank.controls["expiry_day"].setValue(EDate);
  //         this.Addbatterybank.controls["mfg_date"].setValue(MDate);

  //         // this.Addbatterybank.controls["expiry_day"].setValue(e.expiry_day);
  //         this.Addbatterybank.controls["voltage"].setValue(e.voltage);
  //         this.Addbatterybank.controls["sys_ah"].setValue(e.sys_ah);
  //         this.warranty_type = e.warranty_type;
  //         this.labelwarranty = this.warranty_type[0].warranty_duration
  //         this.Addbatterybank.controls["warranty_type_id"].setValue(this.warranty_type[0].warranty_id);
  //         this.Addbatterybank.controls["contract_duration"].setValue(this.warranty_type[0].warranty_duration);

  //       })
  //       this.chRef.detectChanges();
  //     }

  //     else if (result.response.response_code == "500") {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                             //prints if it encounters an error
  //   });

  // }
  getwarranty(id) {
    console.log(id)
    // console.log(this.Addbatterybank.value.mfg_date)
    console.log(this.product_name);
    console.log(this.subid)
    if (id != '' && this.Addbatterybank.value.mfg_date != '') {
      // if (this.Addbatterybank.value.mfg_date) {
      var year = this.Addbatterybank.value.mfg_date.year;
      var month = this.Addbatterybank.value.mfg_date.month;
      var day = this.Addbatterybank.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
      // }

      var data = { "product_id": this.product_name, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
      var url = 'get_warranty_type/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.warranty_id = result.response.data;
          this.warranty_id.forEach(e => {
            console.log(e.expiry_day);
            if (e.expiry_day) {
              var expdates = e.expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }
            if (e.manufacture_date) {
              var mdates = e.manufacture_date.split("-", 3);
              var Myear: number = +mdates[0];
              var Mmonth: number = +mdates[1];
              var Mday: number = +mdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

            this.Addbatterybank.controls["expiry_day"].setValue(EDate);
            this.Addbatterybank.controls["mfg_date"].setValue(MDate);

            console.log(EDate);
            this.Addbatterybank.controls["voltage"].setValue(e.voltage);
            this.Addbatterybank.controls["sys_ah"].setValue(e.sys_ah);
            this.warranty_type = e.warranty_type;
            this.labelwarranty = this.warranty_type[0].warranty_duration
            this.Addbatterybank.controls["warranty_type_id"].setValue(this.warranty_type[0].warranty_id);
            this.Addbatterybank.controls["contract_duration"].setValue(this.warranty_type[0].warranty_duration);

          })
          this.chRef.detectChanges();
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });

    }

  }
  duration(item) {
    console.log(item)
    console.log(this.warranty_id)
    this.warranty_id.forEach(element => {
      if (element.warranty_id == item) {
        this.labelwarranty = element.warranty_duration;

      }
      else {
        this.labelwarranty = "Select Warranty type";
      }
    });
    if (this.Addbatterybank) {
      this.Addbatterybank.controls['contract_duration'].setValue(this.labelwarranty)
    }
    else {
      this.EditTicketstatus.controls['contract_duration'].setValue(this.labelwarranty)
    }
  }
  // duration(item) {
  //   this.warranty_type.forEach(element => {
  //     if (element.warranty_id == item) {
  //       this.labelwarranty = element.warranty_duration;


  //     }
  //     else {
  //       this.labelwarranty = "Select Warranty type";
  //     }
  //   });

  // }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  getcallcategory() {
    var url = 'get_call_category/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.call = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  editorg(event) {
    console.log(this.Addbatterybank.value.site_id)
    var org = event.target.value.trim();
    this.Addbatterybank.controls['organization_name'].setValue(org)
  }
  getapplication(segment_id) {
    console.log(segment_id)
    var data = { "segment_id": segment_id };// storing the form group value
    var url = 'get_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.application = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  getapplications(segment_id) {
    if (segment_id != null || segment_id != '') {
      var data = { "segment_id": segment_id };// storing the form group value
      var url = 'get_application/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.application = result.response.data;
          this.chRef.detectChanges();
          if (this.Addbatterybank) {
            this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['application_id'].setValue(null);
          }
        }

        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          if (this.Addbatterybank) {
            this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['application_id'].setValue(null);
          }
        }
        else {
          if (this.Addbatterybank) {
            this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
            this.Addbatterybank.controls['application_id'].setValue(null);
          }
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        if (this.Addbatterybank) {
          this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
          this.Addbatterybank.controls['application_id'].setValue(null);
        }
        console.log(err);                                             //prints if it encounters an error
      });

    }

    else {
      // alert("ss")
      this.application = [];
      this.Addbatterybank.controls['segment_id'].setErrors({ 'incorrect': true });
      this.Addbatterybank.controls['application_id'].setValue(null);
      this.Addbatterybank.controls['application_id'].setErrors({ 'incorrect': true });
    }

  }
  // editmodal(edit, data) {

  //   this.editdate = false;
  //   this.display_spareAccept = false;
  //   console.log(data);
  //   this.product_details = [];
  //   this.sub_product = [];
  //   this.country = [];
  //   this.states = [];
  //   this.cities = [];
  //   this.model_id = [];
  //   this.warranty_id = [];
  //   this.location_ids = [];
  //   this.segment = [];
  //   this.application = [];
  //   this.ticket_ids = data.ticket_id
  //   var form = { "ticket_id": this.ticket_ids };// storing the form group value
  //   var url = 'ticket_details/'                                         //api url of remove license
  //   this.ajax.postdata(url, form).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.site_details = result.response.data;
  //       this.chRef.detectChanges();
  //       console.log(this.site_details)

  //       console.log(this.site_details.expiry_day)
  //       console.log(this.site_details.battery_bank_id)
  //       // var res = this.site_details.expire_day.split("-");
  //       // var date = (res[2] + "-" + res[1] + "-" + res[0]);
  //       // var splitted = date.split("-", 3);
  //       // var year_date = splitted[2].split(",", 1)
  //       // var year: number = +year_date[0];
  //       // var day: number = +splitted[0];
  //       // var month: number = +splitted[1];
  //       // var time = {
  //       //   year: day,
  //       //   month: month,
  //       //   day: year,
  //       // }
  //       // console.log(time)
  //       if (this.site_details.generated_by == 1) {
  //         this.site = 1
  //       }
  //       else {
  //         this.site = 0
  //       }


  //       this.site_details.serial_no.forEach(element => {
  //         var serial = {
  //           "product_id": element.product.product_id,
  //           "product_sub_id": element.subproduct.product_sub_id,
  //           "model": element.model,
  //           "sys_ah": element.sys_ah,
  //           "voltage": element.voltage,
  //           "serial_no": element.serial_no,
  //           "invoice_number": element.invoice_number,
  //           "invoice_date": element.invoice_date,
  //           "mfg_date": element.mfg_date,
  //           "warranty_type_id": element.warranty.warranty_id,
  //           "expiry_date": element.expire_date,
  //         };
  //         this.serials = serial;
  //       })

  //       this.productdata = this.site_details.serial_no;              //storing the api response in the array
  //       console.log(this.productdata)
  //       console.log(this.serials)

  //       this.showTableedit = true;                            //show datas if after call API
  //       this.chRef.detectChanges();
  //       console.log(this.productdata, "this.productdata")
  //       console.log(this.productdata.length)
  //       if (this.productdata.length == 0) {

  //         this.datatable = "";
  //       }
  //       else {

  //         this.datatable = "Enable";
  //       }
  //       this.get_servicegroup();
  //       this.getcallcategory();
  //       this.prioritys = JSON.parse(localStorage.getItem('priority_details'))
  //       console.log(this.prioritys, "prioritys")
  //       this.modalService.open(edit, {
  //         size: 'lg',
  //       });


  //       this.getproduct();
  //       this.getcountry();
  //       this.onChangeCountry(this.site_details.country.country_id);
  //       this.onChangeState(this.site_details.state.state_id);
  //       this.onChangecity(this.site_details.city.city_id);
  //       this.getsegment();
  //       if(this.site_details.segment != null)
  //       {
  //         this.getapplication(this.site_details.segment.segment_id);
  //       }

  //       // console.log(this.site_details.cust_preference_date);

  //       var dateFormated = this.site_details.cust_preference_date.split("-", 3);
  //       var year = dateFormated[0];
  //       var month = dateFormated[1];
  //       var day = dateFormated[2];
  //       // this.FiscalYearStartDate ={"day": day,
  //       //   "month": month,
  //       //   "year": year}
  //       var cus_date = { "year": year, "month": month, "day": day }
  //       // var cuspre_date: any;
  //       // var cuspredate:any;
  //       // console.log(cus_date)
  //       // var rese = this.site_details.cust_preference_date.split("-");
  //       // cuspre_date = (rese[2] + "-" + rese[1] + "-" + rese[0])
  //       // var splitted = cuspre_date.split("-", 3);
  //       // var year_date = splitted[2].split(",", 1)
  //       // var year: number = +year_date[0];
  //       // var day: number = +splitted[0];
  //       // var month: number = +splitted[1];
  //       //  cuspredate = {
  //       //   year: day,
  //       //   month: month,
  //       //   day: year,
  //       // }
  //       // this.FiscalYearStartDate=cus_date;
  //       console.log(this.calendar.getToday())
  //       this.FiscalYearStartDate=this.calendar.getToday();
  //        console.log(this.FiscalYearStartDate)
  //        console.log(this.site_details.image)
  //       this.EditTicketstatus.patchValue({
  //         'customer_name': this.site_details.customer_name,
  //         'customer_code': this.site_details.customer_code,
  //         'email_id': this.site_details.email_id,
  //         'contact_number': this.site_details.contact_number,
  //         'alternate_number': this.site_details.alternate_number,
  //         'sap_reference_number': this.site_details.sap_reference_number,
  //         'site_id': this.site_details.site_id,
  //         'country_id': this.site_details.country.country_id,
  //         'state_id': this.site_details.state.state_id,
  //         'city_id': this.site_details.city.city_id,
  //         'location_id': this.site_details.location.location_id,
  //         'battery_bank_id': this.site_details.battery_bank_id,
  //         'ticket_status': this.site_details.current_status,
  //         'plot_number': this.site_details.plot_number,
  //         'street': this.site_details.street,
  //         'landmark': this.site_details.landmark,
  //         'post_code': this.site_details.post_code,
  //         'contact_person_name': this.site_details.contact_person_name,
  //         'contact_person_number': this.site_details.contact_person_number,
  //         // 'organization_name': this.site_details.organization_name,
  //         'work_type_id': this.site_details.work_type.work_type_id,
  //         'call_category_id': this.site_details.call_category.call_category_id,
  //         'proioritys': this.site_details.priority,
  //         'desc': this.site_details.problem_desc,
  //         'image': '',
  //         'cust_preference_date': cus_date,

  //       })
  //       // this.EditTicketstatus.controls["cust_preference_date"].setValue(this.cuspre_date)
  //       console.log(this.EditTicketstatus.value)

  //       if(this.site_details.product!=null){ 
  //         this.EditTicketstatus.controls['product_id'].setValue(this.site_details.product.product_id);
  //       this.EditTicketstatus.controls['product_sub_id'].setValue(this.site_details.sub_product.product_sub_id);
  //       this.EditTicketstatus.controls['warranty_type_id'].setValue(this.site_details.contract_type.contract_type_id);
  //       this.EditTicketstatus.controls['model_no'].setValue(this.site_details.model);
  //       this.EditTicketstatus.controls['segment_id'].setValue(this.site_details.segment.segment_id);
  //       this.EditTicketstatus.controls['application_id'].setValue(this.site_details.application.application_id);
  //       this.EditTicketstatus.controls['expiry_day'].setValue(this.site_details.expiry_day);
  //       this.EditTicketstatus.controls['mfg_date'].setValue(this.site_details.manufacture_date);
  //       this.EditTicketstatus.controls['sys_ah'].setValue(this.site_details.sys_ah);
  //       this.EditTicketstatus.controls['voltage'].setValue(this.site_details.voltage);
  //       this.onChangeProduct(this.site_details.product.product_id);
  //         this.getmodel(this.site_details.sub_product.product_sub_id);
  //         this.duration(this.site_details.contract_type.contract_type_id);
  //       }
  //      else{
  //       this.EditTicketstatus.controls['product_id'].setValue('');
  //       this.EditTicketstatus.controls['product_sub_id'].setValue('');
  //       this.EditTicketstatus.controls['warranty_type_id'].setValue('');
  //       this.EditTicketstatus.controls['model_no'].setValue('');
  //       this.EditTicketstatus.controls['segment_id'].setValue('');
  //       this.EditTicketstatus.controls['application_id'].setValue('');
  //       this.EditTicketstatus.controls['expiry_day'].setValue('');
  //       this.EditTicketstatus.controls['mfg_date'].setValue('');
  //       this.EditTicketstatus.controls['sys_ah'].setValue('');
  //       this.EditTicketstatus.controls['voltage'].setValue('');
  //      } 


  //     }
  //     else if (result.response.response_code == "500") {
  //       this.toastr.error(result.response.message, "Error")
  //     }
  //     else {
  //       this.toastr.error(result.response.message, "Error")
  //     }
  //   })
  // }
  editmodal(edit, data) {

    this.editdate = false;
    // this.display_spareAccept = false;
    this.display_spareAccept = "Disable";
    console.log(data);
    this.product_details = [];
    this.sub_product = [];
    this.country = [];
    this.states = [];
    this.cities = [];
    this.model_id = [];
    this.warranty_id = [];
    this.location_ids = [];
    this.segment = [];
    this.application = [];
    this.ticket_ids = data.ticket_id
    var form = { "ticket_id": this.ticket_ids };// storing the form group value
    var url = 'ticket_details/'                                         //api url of remove license
    this.ajax.postdata(url, form).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.site_details = result.response.data;
        console.log(this.site_details, "this.site_details")
        this.chRef.detectChanges();
        this.ExpiryDate = this.site_details.expiry_day
        // this.ManufactureDate = this.site_details.manufacture_date

        if (this.site_details.generated_by == 1) {
          this.site = 1
        }
        else {
          this.site = 0
        }


        this.site_details.serial_no.forEach(element => {

          console.log(element.voltage, " element.voltage")
          var serial = {
            "product_id": element.product.product_id,
            "product_sub_id": element.subproduct.product_sub_id,
            "model": element.model.model_id,
            "sys_ah": element.sys_ah,
            "voltage": element.voltage,
            "serial_no": element.serial_no,
            "invoice_number": element.invoice_number,
            "invoice_date": element.invoice_date,
            "mfg_date": element.mfg_date,
            "warranty_type_id": element.warranty.warranty_id,
            "expiry_date": element.expire_date,
          };
          this.serials = serial;
        })

        this.productdata = this.site_details.serial_no;              //storing the api response in the array
        console.log(this.productdata)
        console.log(this.serials)

        this.showTableedit = true;                            //show datas if after call API
        this.chRef.detectChanges();
        console.log(this.productdata, "this.productdata")
        console.log(this.productdata.length)
        if (this.productdata.length == 0) {

          this.datatable = "";
        }
        else {

          this.datatable = "Enable";
        }
        this.get_servicegroup();
        this.getcallcategory();
        this.prioritys = JSON.parse(localStorage.getItem('priority_details'))
        console.log(this.prioritys, "prioritys")
        this.modalService.open(edit, {
          size: 'lg', //specifies the size of the dialog box
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });


        this.getproduct();
        this.getcountry();
        this.onChangeCountry(this.site_details.country.country_id);
        this.onChangeState(this.site_details.state.state_id);
        this.onChangecity(this.site_details.city.city_id);
        this.getsegment();
        if (this.site_details.segment != null) {
          this.getapplication(this.site_details.segment.segment_id);
        }
        if (this.site_details.cust_preference_date) {
          var dateFormated = this.site_details.cust_preference_date.split("-", 3);
          var year = dateFormated[0];
          var month = dateFormated[1];
          var day = dateFormated[2];
          var cus_date = { "year": year, "month": month, "day": day }
        }
        else {
          cus_date = null;
        }
        this.FiscalYearStartDate = this.calendar.getToday();
        if (this.site_details.model)   //if model is there
        {
          var model_id = this.site_details.model.model_id
        } else {
          var model_id = null         //if there is no model
        }
        this.EditTicketstatus.patchValue({
          'customer_name': this.site_details.customer_name,
          'customer_code': this.site_details.customer_code,
          'email_id': this.site_details.email_id,
          'contact_number': this.site_details.contact_number,
          'alternate_number': this.site_details.alternate_number,
          'sap_reference_number': this.site_details.sap_reference_number,
          'site_id': this.site_details.site_id,
          'country_id': this.site_details.country.country_id,
          'state_id': this.site_details.state.state_id,
          'city_id': this.site_details.city.city_id,
          'location_id': this.site_details.location.location_id,
          'battery_bank_id': this.site_details.battery_bank_id,
          'model_no': model_id,

          // 'ticket_status': this.site_details.current_status,
          'plot_number': this.site_details.plot_number,
          'street': this.site_details.street,
          'landmark': this.site_details.landmark,
          'post_code': this.site_details.post_code,
          'contact_person_name': this.site_details.contact_person_name,
          'contact_person_number': this.site_details.contact_person_number,
          // 'organization_name': this.site_details.organization_name,
          'work_type_id': this.site_details.work_type.work_type_id,
          'call_category_id': this.site_details.call_category.call_category_id,
          'proioritys': this.site_details.priority,
          'desc': this.site_details.problem_desc,
          'image': '',
          'cust_preference_date': cus_date,

        })
        // this.EditTicketstatus.controls["cust_preference_date"].setValue(this.cuspre_date)
        console.log(this.EditTicketstatus.value)

        if (this.site_details.product != null) {
          this.EditTicketstatus.controls['product_id'].setValue(this.site_details.product.product_id);
          this.EditTicketstatus.controls['product_sub_id'].setValue(this.site_details.sub_product.product_sub_id);
          this.EditTicketstatus.controls['warranty_type_id'].setValue(this.site_details.contract_type.contract_type_id);
          this.EditTicketstatus.controls['model_no'].setValue(this.site_details.model);
          if (this.site_details.segment) {
            this.EditTicketstatus.controls['segment_id'].setValue(this.site_details.segment.segment_id);
          }
          if (this.site_details.application) {
            this.EditTicketstatus.controls['application_id'].setValue(this.site_details.application.application_id);
          }
          this.EditTicketstatus.controls['expiry_day'].setValue(this.site_details.expiry_day);
          this.EditTicketstatus.controls['mfg_date'].setValue(this.site_details.manufacture_date);
          this.EditTicketstatus.controls['sys_ah'].setValue(this.site_details.sys_ah);
          this.EditTicketstatus.controls['voltage'].setValue(this.site_details.voltage);
          this.onChangeProduct(this.site_details.product.product_id);
          this.getmodel(this.site_details.sub_product.product_sub_id);
          this.duration(this.site_details.contract_type.contract_type_id);
        }
        else {
          this.EditTicketstatus.controls['product_id'].setValue('');
          this.EditTicketstatus.controls['product_sub_id'].setValue('');
          this.EditTicketstatus.controls['warranty_type_id'].setValue('');
          this.EditTicketstatus.controls['model_no'].setValue('');
          this.EditTicketstatus.controls['segment_id'].setValue('');
          this.EditTicketstatus.controls['application_id'].setValue('');
          this.EditTicketstatus.controls['expiry_day'].setValue('');
          this.EditTicketstatus.controls['mfg_date'].setValue('');
          this.EditTicketstatus.controls['sys_ah'].setValue('');
          this.EditTicketstatus.controls['voltage'].setValue('');
        }


      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, "Error")
      }
      else {
        this.toastr.error(result.response.message, "Error")
      }
    })
  }
  openlarge(modal) {
    this.modalService.open(modal, {
      size: 'lg',
      // windowClass: "center-modalsm",
    });
  }
  getSerialNmber(bbid) {
    this.showTable = false;                            //show datas if after call API
    this.chRef.detectChanges();
    console.log(bbid);
    // this.battery_data = 'Enable';
    var data = { battery_bank_id: bbid };
    var url = 'get_serial_numbers/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.productdata = result.response.data;              //storing the api response in the array
        console.log(this.productdata)
        console.log(this.serials)

        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.productdata.forEach(i => {
          let a = this.serials.find(c => c.serial_no === i.serial_no);
          if (a === undefined) {
            console.log(a)
            i.select = true;
            this.productdata.push(i);
          } else {
            i.select = false;
            this.productdata.push(i);
          }
        });
        console.log(this.productdata)



        var seenNames = {};

        this.productdata = this.productdata.filter(function (currentObject) {
          if (currentObject.serial_no in seenNames) {
            return false;
          } else {
            seenNames[currentObject.serial_no] = true;
            return true;
          }
        });
        console.log(this.productdata)


      }
      else if (result.response.response_code == "500") {
        this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      // this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  ticketstatus($eve, reason) {
    console.log($eve.target.value);
    if ($eve.target.value == 1) {
      this.display_spareAccept = 'Enable';
      this.display_sparereject='Disable';
      // this.display_spareAccept = true;
    }
    // if ($eve.target.value == 1) {
    //   this.modalService.open(reason, {
    //     size: 'lg',
    //     windowClass: "center-modalsm",
    //     backdrop: 'static',                                    // modal will not close by outside click
    //     keyboard: false,                                       // modal will not close by keyboard click
    //   });
    // }
    else {
      this.display_spareAccept = "Disable";
      this.Accept = '';
      this.display_sparereject='Enable';
    }
  }
  sparePopApp() {
    this.Accept = this.sapreAcceptReason.value.accept_reason;
    // this.modalService.dismissAll();
  }
  // ergerter
  edit_serl() {
    console.log(this.serials);
    console.log(this.serials.length);
    console.log(this.EditTicketstatus.value);
    console.log(this.Addbatterybank.value)
    console.log(this.ticket_ids, "this.ticket_ids");
    console.log(this.employee_code, "this.employee_code")
    console.log(this.Accept, "reason foe accept")
    if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
      this.Addbatterybank.controls['invoice_date'].setValue('');
    }
    else {
      var year = this.Addbatterybank.value.invoice_date.year;
      var month = this.Addbatterybank.value.invoice_date.month;
      var day = this.Addbatterybank.value.invoice_date.day;
      var dateFormated = year + "-" + month + "-" + day;
      this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
    }


    var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
    var year = ids[0];
    var month = ids[1];
    var day = ids[2];
    var exdate = day + "-" + month + "-" + year;
    console.log(exdate)

    // this.Addbatterybank.controls['expiry_day'].setValue(exdate);

    var id = this.Addbatterybank.value.mfg_date.split("-", 3)
    var year = id[0];
    var month = id[1];
    var day = id[2];
    var mfdate = day + "-" + month + "-" + year;
    console.log(mfdate)
    if (this.site_details.contract_type != null) {
      var contract_type_id = this.site_details.contract_type.contract_type_id
    }
    else {
      contract_type_id = null
    }
    if (this.serials.length != 0) {
      var data = {
        "ticket_id": this.ticket_ids,
        "site_id": this.EditTicketstatus.value.site_id,
        "product_id": this.EditTicketstatus.value.product_id,
        "product_sub_id": this.EditTicketstatus.value.product_sub_id,
        "serial_no": this.serials,
        "country_id": this.EditTicketstatus.value.country_id,
        "state_id": this.EditTicketstatus.value.state_id,
        "city_id": this.EditTicketstatus.value.city_id,
        "location_id": this.EditTicketstatus.value.location_id,
        "current_status": this.EditTicketstatus.value.ticket_status,
        "employee_code": this.employee_code,
        "contact_person_name": this.EditTicketstatus.value.contact_person_name,
        "contact_person_number": this.EditTicketstatus.value.contact_person_number,
        "organization_name": this.EditTicketstatus.value.organization_name,
        "segment_id": this.EditTicketstatus.value.segment_id,
        "application_id": this.EditTicketstatus.value.application_id,
        "post_code": this.EditTicketstatus.value.post_code,
        "landmark": this.EditTicketstatus.value.landmark,
        "street": this.EditTicketstatus.value.street,
        "voltage": this.Addbatterybank.value.voltage,
        "sys_ah": this.Addbatterybank.value.sys_ah,
        "plot_number": this.EditTicketstatus.value.plot_number,
        "model_no": this.Addbatterybank.value.model.model_id,
        "contract_type_id": contract_type_id,
        "work_type_id": this.EditTicketstatus.value.work_type_id,
        "proioritys": this.EditTicketstatus.value.proioritys,
        "cust_preference_date": this.EditTicketstatus.value.cust_preference_date,
        "problem_desc": this.EditTicketstatus.value.problem_desc,
        "image": this.EditTicketstatus.value.image,
        "invoice_number": this.Addbatterybank.value.invoice_number,
        "manufacture_date": mfdate,
        "duration": this.Addbatterybank.value.duration,
        "invoice_date": this.Addbatterybank.value.invoice_date,
        "expiry_day": exdate,

      }
      console.log(data)
      if (data.current_status == 23) {
        data["approve_reason"] = this.Reject;
      }
      else {
        data["approve_reason"] = this.Accept
      }
      var url = 'edit_ticket/'                                         //api url for add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                 //if sucess
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();
          // this.loadingAdd = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.get_unassigned_tickets();
          this.showTables = false;
          this.get_rejected_tickets();
          this.modalService.dismissAll();                                 //to close the modal box

        }
        else if (result.response.response_code == "400") {
          // this.adderror = result.response.message;
          // this.loadingAdd = false;
        }
        else if (result.response.response_code == "500") {
          // this.adderror = result.response.message;
          // this.loadingAdd = false;
        }
        else {
          // this.adderror = "Something went wrong";
          // this.loadingAdd = false;
        }
      }, (err) => {                                                     //if error
        console.log(err);                                             //prints if it encounters an error
      });
    }
    else {
      this.toastr.warning("warning", "Please select the corresponding serial number")
    }
  }
  checkboxFunction(value) {
    return this.name.filter(checkvalue => checkvalue == value)
  }
  openedit(modal, ticket_id) {
    console.log(ticket_id)
    this.edit_tiket_id = ticket_id;
    this.modalService.open(modal, {
      size: 'lg'
    });
  }
  editticket(i, model) {
    this.display_spareAccept = "Disable";
    this.Accept = '';
    this.getproduct();
    this.onChangeProduct(i.product.product_id);
    this.product_name = i.product.product_id
    this.getmodel(i.subproduct.product_sub_id);
    this.subid = i.subproduct.product_sub_id;

    console.log(i, "i")
    console.log(this.site_details, "this.site_details")
    if (this.site_details.segment != null) {
      this.getapplication(this.site_details.segment.segment_id);
    } var rese = i.expiry_date.split("-");
    // this.expiry = (rese[2] + "-" + rese[1] + "-" + rese[0])
    // var splitted = this.expiry.split("-", 3);
    // var year_date = splitted[2].split(",", 1)
    // var year: number = +year_date[0];
    // var day: number = +splitted[0];
    // var month: number = +splitted[1];
    // this.expire = {
    //   year: day,
    //   month: month,
    //   day: year,
    // }

    //     console.log(this.expire)
    if (i.expiry_day) {
      var expdates = i.expiry_day.split("-", 3);
      var Eyear: number = +expdates[0];
      var Emonth: number = +expdates[1];
      var Eday: number = +expdates[2];
    }
    if (i.mfg_date) {
      var mdates = i.mfg_date.split("-", 3);
      var Myear: number = +mdates[0];
      var Mmonth: number = +mdates[1];
      var Mday: number = +mdates[2];
    }

    const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
    const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

    // this.Addbatterybank.controls["expiry_day"].setValue(EDate);
    // this.Addbatterybank.controls["mfg_date"].setValue(MDate);
    this.title = "Edit Serial Number";
    this.labelwarranty = "Select Warranty type";
    if (this.site_details.segment) {
      var segment_id = this.site_details.segment.segment_id
    }
    if (this.site_details.application) {
      var application_id = this.site_details.application.application_id
    }
    this.Addbatterybank = new FormGroup({
      "product_id": new FormControl(this.product_name, [Validators.required]),
      "product_sub_id": new FormControl(this.subid, [Validators.required]),
      "model_no": new FormControl(i.model.model_id, [Validators.required]),
      "serial_no": new FormControl(i.serial_no, [Validators.required]),
      "invoice_date": new FormControl(null),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
      "sys_ah": new FormControl(i.sys_ah, [Validators.required]),

      "segment_id": new FormControl(segment_id, [Validators.required]),
      "application_id": new FormControl(application_id, [Validators.required]),
      "organization_name": new FormControl(this.site_details.organization_name),
      "invoice_number": new FormControl(i.invoice_number),
      "expiry_day": new FormControl(EDate),
      "warranty_type_id": new FormControl(i.warranty.warranty_id, [Validators.required]),
      "mfg_date": new FormControl(MDate),
      "voltage": new FormControl(i.voltage, [Validators.required]),
      "ticket_status": new FormControl(this.site_details.current_status, [Validators.required]),

    })
    this.getwarranty(i.model.model_id);

    this.modalService.open(model, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });

  }

  addserial(model) {
    this.display_spareAccept = "Disable";
    this.Accept = '';
    this.labelwarranty = "Select Warranty type";
    this.title = "Add Serial Number";
    // this.Addbatterybank = new FormGroup({
    //   "product_id": new FormControl('', [Validators.required]),
    //   "product_sub_id": new FormControl('', [Validators.required]),
    //   "model_no": new FormControl('', [Validators.required]),
    //   "serial_no": new FormControl('', [Validators.required]),
    //   "invoice_date": new FormControl(''),
    //   "contract_duration": new FormControl(''),
    //   "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
    //   "sys_ah": new FormControl('', [Validators.required]),
    //   "segment_id": new FormControl('', [Validators.required]),
    //   "application_id": new FormControl('', [Validators.required]),
    //   "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
    //   "invoice_number": new FormControl(''),
    //   "expiry_day": new FormControl(''),
    //   "warranty_type_id": new FormControl('', [Validators.required]),
    //   "mfg_date": new FormControl(''),
    //   "voltage": new FormControl('', [Validators.required]),
    // })
    console.log(this.site_details.invoice_date, "invoice date")
    if (this.site_details.invoice_date != null) {
      var idates = this.site_details.invoice_date.split("-", 3);
      var iyear: number = +idates[0];
      var imonth: number = +idates[1];
      var iday: number = +idates[2];
      const IDate: NgbDate = new NgbDate(iday, imonth, iyear);
      console.log(IDate)
      var date = IDate;
    }
    else {
      date = null;
    }

    this.getsegment();
    if (this.site_details.segment != null) {
      var segment = this.site_details.segment.segment_id;
      var application = this.site_details.application.application_id
    }
    else {
      segment = this.site_details.segment
      application = this.site_details.application
    }
    this.Addbatterybank = new FormGroup({
      "product_id": new FormControl('', [Validators.required]),
      "product_sub_id": new FormControl('', [Validators.required]),
      "model_no": new FormControl('', [Validators.required]),
      "serial_no": new FormControl('', [Validators.required]),
      "invoice_date": new FormControl(date),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
      "sys_ah": new FormControl('', [Validators.required]),
      "segment_id": new FormControl(segment, [Validators.required]),
      "application_id": new FormControl(application, [Validators.required]),
      "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(''),
      "warranty_type_id": new FormControl('', [Validators.required]),
      "mfg_date": new FormControl(''),
      "voltage": new FormControl('', [Validators.required]),
      "ticket_status": new FormControl(this.site_details.current_status, [Validators.required]),
    })
    this.Addbatterybank.controls['product_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_id'].setValue(null);
    this.Addbatterybank.controls['product_sub_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['product_sub_id'].setValue(null);
    this.Addbatterybank.controls['model_no'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['model_no'].setValue(null);
    this.Addbatterybank.controls['warranty_type_id'].setErrors({ 'incorrect': true });
    this.Addbatterybank.controls['warranty_type_id'].setValue(null);
    this.modalService.open(model, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  // extractSerialnumber(serialno) {

  //   var serialno: any;
  //   console.log(this.Addbatterybank.value.serial_no);
  //   if (this.Addbatterybank.value.serial_no) {
  //     serialno = this.Addbatterybank.value.serial_no;
  //     var serialnum_length = serialno.toString().length;
  //     console.log(serialnum_length);
  //   }
  //   else {
  //     serialno = "";
  //   }
  //   if (serialno) {
  //     var serial_number = { 'serial_no': serialno };
  //     var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
  //     this.ajax.postdata(url, serial_number).subscribe((result) => {
  //       if (result.response.response_code == "200" || result.response.response_code == "400") {
  //         this.serial_error = '';
  //         this.flag = result.response.flag;
  //         if (this.flag == 0) {
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product = [];
  //           this.subproduct = [];
  //           this.model_id = [];
  //           this.warranty_type = [];
  //           this.product.push(this.serialNumbersdata.product);
  //           this.model_id.push(this.serialNumbersdata.model);
  //           this.warranty_type.push(this.serialNumbersdata.warranty_type);
  //           this.subproduct.push(this.serialNumbersdata.product_sub);
  //           this.products_id = this.serialNumbersdata.product.product_id
  //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
  //           this.mfg_date = this.serialNumbersdata.manufacture_date;
  //           console.log(this.serialNumbersdata);
  //           this.Addbatterybank.patchValue({
  //             'product_id': this.serialNumbersdata.product.product_id,
  //             'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
  //             'model_no': this.serialNumbersdata.model.model_id,
  //             "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
  //             "sys_ah": this.serialNumbersdata.sys_ah,
  //             "voltage": this.serialNumbersdata.voltage,
  //             "mfg_date": this.serialNumbersdata.manufacture_date,
  //             "expiry_day": this.serialNumbersdata.expiry_day,

  //           });

  //           this.getsegment();
  //           // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

  //           this.Addbatterybank.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)

  //         }
  //         else if (this.flag == 1) {
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product = [];
  //           this.subproduct = [];
  //           this.model_id = [];
  //           this.warranty_type = [];
  //           // this.segment = [];
  //           // this.application = [];
  //           this.product.push(this.serialNumbersdata.product);
  //           this.subproduct.push(this.serialNumbersdata.product_sub);
  //           this.products_id = this.serialNumbersdata.product.product_id
  //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
  //           this.serialNumbersdata.model.forEach(element => {
  //             this.model_id.push(element);
  //           });

  //           console.log(this.subid);
  //           this.Addbatterybank.patchValue({
  //             'product_id': this.products_id,
  //             'product_sub_id': this.subid,
  //             "mfg_date": this.serialNumbersdata.manufacture_date,
  //             "expiry_day": this.serialNumbersdata.expiry_day,
  //             "sys_ah": '',
  //             "segment_id": this.Addbatterybank.value.segment_id,
  //             "application_id": this.Addbatterybank.value.application_id,
  //             "organization_name": '',
  //             "invoice_number": '',
  //             "invoice_date": '',
  //             "warranty_type_id": '',
  //             "contract_duration": '',



  //           });
  //           this.getsegment();

  //         }
  //         else if (this.flag == 2) {
  //           // this.getproduct();
  //           this.Addbatterybank.patchValue({
  //             "mfg_date": result.response.manufacture_date[0].manufacture_date
  //           });
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product = [];
  //           this.subproduct = [];
  //           this.model_id = [];
  //           this.serialNumbersdata.product.forEach(element => {
  //             this.product.push(element);
  //           });
  //           console.log(this.product)
  //         }
  //         else if (this.flag == 3) {
  //           // this.product = [];
  //           // this.subproduct = [];
  //           // this.model_id = [];
  //           this.Addbatterybank.patchValue({
  //             "mfg_date": result.response.manufacture_date
  //           });
  //           // this.Addbatterybank.value.mfg_date = result.response.manufacture_date; //storing the api response in the array

  //           this.getsegment();
  //           this.getproduct();
  //         }
  //         // }

  //         this.chRef.detectChanges();
  //       }
  //       else if (result.response.response_code == "500") {
  //         this.toastr.error(result.response.message, 'Error')

  //         this.serial_error = result.response.message;
  //       }
  //       else {
  //         this.toastr.error("Something went wrong", 'Error')
  //         this.serial_error = "";
  //       }
  //     }, (err) => {
  //       console.log(err); //prints if it encounters an error
  //     });
  //   }



  // }
  // 2nd time
  // extractSerialnumber(serialno) {

  //   var serialno: any;
  //   console.log(this.Addbatterybank.value.serial_no);
  //   if (this.Addbatterybank.value.serial_no) {
  //     serialno = this.Addbatterybank.value.serial_no;
  //     var serialnum_length = serialno.toString().length;
  //     console.log(serialnum_length);
  //   }
  //   else {
  //     serialno = "";
  //   }
  //   if (serialno) {
  //     var serial_number = { 'serial_no': serialno };
  //     var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
  //     this.ajax.postdata(url, serial_number).subscribe((result) => {
  //       if (result.response.response_code == "200" || result.response.response_code == "400") {
  //         this.serial_error = '';
  //         this.flag = result.response.flag;
  //         if (this.flag == 0) {
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product = [];
  //           this.subproduct = [];
  //           this.model_id = [];
  //           this.warranty_type = [];
  //           this.product_details.push(this.serialNumbersdata.product);
  //           this.model_id.push(this.serialNumbersdata.model);
  //           this.warranty_type.push(this.serialNumbersdata.warranty_type);
  //           this.subproduct.push(this.serialNumbersdata.product_sub);
  //           this.products_id = this.serialNumbersdata.product.product_id
  //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
  //           // this.mfg_date = this.serialNumbersdata.manufacture_date;
  //           console.log(this.serialNumbersdata);
  //           this.Addbatterybank.patchValue({
  //             'product_id': this.serialNumbersdata.product.product_id,
  //             'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
  //             'model_no': this.serialNumbersdata.model.model_id,
  //             "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
  //             "sys_ah": this.serialNumbersdata.sys_ah,
  //             "voltage": this.serialNumbersdata.voltage,
  //             // "mfg_date": this.serialNumbersdata.manufacture_date,
  //             // "expiry_day": this.serialNumbersdata.expiry_day,

  //           });
  //           if (this.serialNumbersdata.expiry_day) {
  //             var expdates = this.serialNumbersdata.expiry_day.split("-", 3);
  //             var Eyear: number = +expdates[0];
  //             var Emonth: number = +expdates[1];
  //             var Eday: number = +expdates[2];
  //           }
  //           if (this.serialNumbersdata.manufacture_date) {
  //             var mdates = this.serialNumbersdata.manufacture_date.split("-", 3);
  //             var Myear: number = +mdates[0];
  //             var Mmonth: number = +mdates[1];
  //             var Mday: number = +mdates[2];
  //           }

  //           const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
  //           const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

  //           this.Addbatterybank.controls["expiry_day"].setValue(EDate);
  //           this.Addbatterybank.controls["mfg_date"].setValue(MDate);
  //           this.onChangeProduct(this.serialNumbersdata.product.product_id)
  //           this.getsegment();
  //           // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

  //           this.Addbatterybank.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)

  //         }
  //         else if (this.flag == 1) {
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product_details = [];
  //           this.sub_product = [];
  //           this.model_id = [];
  //           this.warranty_type = [];
  //           // this.segment = [];
  //           // this.application = [];
  //           this.product_details.push(this.serialNumbersdata.product);
  //           this.sub_product.push(this.serialNumbersdata.product_sub);
  //           this.products_id = this.serialNumbersdata.product.product_id

  //           this.subid = this.serialNumbersdata.product_sub.product_sub_id;
  //           this.serialNumbersdata.model.forEach(element => {
  //             this.model_id.push(element);
  //           });
  //           if (result.response.manufacture_date[0]) {
  //             var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
  //             var Eyear: number = +expdates[0];
  //             var Emonth: number = +expdates[1];
  //             var Eday: number = +expdates[2];
  //           }

  //           const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
  //           this.Addbatterybank.controls["mfg_date"].setValue(EDate);
  //           console.log(this.subid);
  //           // this.Addbatterybank.patchValue({
  //           //   'product_id': this.products_id,
  //           //   'product_sub_id': this.subid,
  //           //   // "mfg_date": this.serialNumbersdata.manufacture_date,
  //           //   // "expiry_day": this.serialNumbersdata.expiry_day,
  //           //   "sys_ah": '',
  //           //   "segment_id": this.Addbatterybank.value.segment_id,
  //           //   "application_id": this.Addbatterybank.value.application_id,
  //           //   "organization_name": '',
  //           //   "invoice_number": '',
  //           //   "invoice_date": '',
  //           //   "warranty_type_id": '',
  //           //   "contract_duration": '',



  //           // });
  //           // this.Addbatterybank.patchValue({
  //           //   'product_id': this.products_id,
  //           //   'product_sub_id': this.subid,
  //           //   // 'model_no': this.serialNumbersdata.model.model_id,
  //           //   // "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
  //           //   // "mfg_date": this.serialNumbersdata.manufacture_date,
  //           //   // "expiry_day": this.serialNumbersdata.expiry_day,

  //           // });
  //           this.Addbatterybank = new FormGroup({
  //             "product_id": new FormControl(this.products_id, [Validators.required]),
  //             "product_sub_id": new FormControl(this.subid, [Validators.required]),
  //             "model_no": new FormControl('', [Validators.required]),
  //             // "serial_no": new FormControl('', [Validators.required]),
  //             "invoice_date": new FormControl(''),
  //             "contract_duration": new FormControl(''),
  //             // "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
  //             "sys_ah": new FormControl('', [Validators.required]),
  //             "segment_id": new FormControl(this.Addbatterybank.value.segment_id, [Validators.required]),
  //             "application_id": new FormControl(this.Addbatterybank.value.application_id, [Validators.required]),
  //             "invoice_number": new FormControl(''),
  //             "expiry_day": new FormControl(''),
  //             "warranty_type_id": new FormControl('', [Validators.required]),
  //             "mfg_date": new FormControl(EDate),
  //             "voltage": new FormControl('', [Validators.required]),
  //             "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
  //           })
  //           this.getsegment();
  //           this.product_name = this.Addbatterybank.value.product_id
  //         }
  //         else if (this.flag == 2) {
  //           // this.getproduct();
  //           // this.Addbatterybank.patchValue({
  //           //   "mfg_date": result.response.manufacture_date[0].manufacture_date
  //           // });
  //           if (result.response.manufacture_date[0].manufacture_date) {
  //             var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
  //             var Eyear: number = +expdates[0];
  //             var Emonth: number = +expdates[1];
  //             var Eday: number = +expdates[2];
  //           }


  //           const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
  //           // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
  //           this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
  //           this.product_details = [];
  //           this.subproduct = [];
  //           this.model_id = [];
  //           this.serialNumbersdata.product.forEach(element => {
  //             this.product_details.push(element);
  //           });
  //           console.log(this.product_details)
  //           this.Addbatterybank = new FormGroup({
  //             "product_id": new FormControl('', [Validators.required]),
  //             "product_sub_id": new FormControl('', [Validators.required]),
  //             "model_no": new FormControl('', [Validators.required]),
  //             // "serial_no": new FormControl('', [Validators.required]),
  //             "invoice_date": new FormControl(''),
  //             "contract_duration": new FormControl(''),
  //             // "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
  //             "sys_ah": new FormControl('', [Validators.required]),
  //             "segment_id": new FormControl('', [Validators.required]),
  //             "application_id": new FormControl('', [Validators.required]),
  //             "invoice_number": new FormControl(''),
  //             "expiry_day": new FormControl(''),
  //             "warranty_type_id": new FormControl('', [Validators.required]),
  //             "mfg_date": new FormControl(EDate),
  //             "voltage": new FormControl('', [Validators.required]),
  //             "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
  //           })
  //         }

  //         else if (this.flag == 3) {
  //           // this.product = [];
  //           // this.subproduct = [];
  //           // this.model_id = [];
  //           if (result.response.manufacture_date) {
  //             var expdates = result.response.manufacture_date.split("-", 3);
  //             var Eyear: number = +expdates[0];
  //             var Emonth: number = +expdates[1];
  //             var Eday: number = +expdates[2];
  //           }


  //           const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
  //           // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
  //           // this.Addbatterybank.patchValue({
  //           //   'product_id': '',
  //           //   'product_sub_id': '',
  //           //   'model_no': '',
  //           //   "warranty_type_id": '',
  //           //   "sys_ah": '',
  //           //   "voltage": '',
  //           //   "invoice_date": '',
  //           //   "invoice_number": ''

  //           // });
  //           this.Addbatterybank = new FormGroup({
  //             "product_id": new FormControl('', [Validators.required]),
  //             "product_sub_id": new FormControl('', [Validators.required]),
  //             "model_no": new FormControl('', [Validators.required]),
  //             // "serial_no": new FormControl('', [Validators.required]),
  //             "invoice_date": new FormControl(''),
  //             "contract_duration": new FormControl(''),
  //             // "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
  //             "sys_ah": new FormControl('', [Validators.required]),
  //             "segment_id": new FormControl('', [Validators.required]),
  //             "application_id": new FormControl('', [Validators.required]),
  //             "invoice_number": new FormControl(''),
  //             "expiry_day": new FormControl(''),
  //             "warranty_type_id": new FormControl('', [Validators.required]),
  //             "mfg_date": new FormControl(EDate),
  //             "voltage": new FormControl('', [Validators.required]),
  //             "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
  //           })
  //           // this.Addbatterybank.patchValue({
  //           //   "mfg_date": result.response.manufacture_date
  //           // });
  //           // this.Addbatterybank.value.mfg_date = result.response.manufacture_date; //storing the api response in the array

  //           this.getsegment();
  //           this.getproduct();
  //         }


  //         this.chRef.detectChanges();
  //       }
  //       else if (result.response.response_code == "500") {
  //         this.toastr.error(result.response.message, 'Error')

  //         this.serial_error = result.response.message;
  //       }
  //       else {
  //         this.toastr.error("Something went wrong", 'Error')
  //         this.serial_error = "";
  //       }
  //     }, (err) => {
  //       console.log(err); //prints if it encounters an error
  //     });
  //   }



  // }

  extractSerialnumber(serialno) {

    var serialno: any;
    console.log(this.Addbatterybank.value);
    if (this.Addbatterybank.value.serial_no) {
      serialno = this.Addbatterybank.value.serial_no;
      var serialnum_length = serialno.toString().length;
      console.log(serialnum_length);
    }
    else {
      serialno = "";
    }
    if (serialno) {
      var serial_number = { 'serial_no': serialno };
      var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
      this.ajax.postdata(url, serial_number).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.serial_error = '';
          this.flag = result.response.flag;
          if (this.flag == 0) {
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.warranty_type = [];
            this.product_details.push(this.serialNumbersdata.product);
            this.model_id.push(this.serialNumbersdata.model);
            this.warranty_type.push(this.serialNumbersdata.warranty_type);
            this.subproduct.push(this.serialNumbersdata.product_sub);
            this.products_id = this.serialNumbersdata.product.product_id
            this.subid = this.serialNumbersdata.product_sub.product_sub_id;
            // this.mfg_date = this.serialNumbersdata.manufacture_date;
            console.log(this.serialNumbersdata);
            this.Addbatterybank.patchValue({
              'product_id': this.serialNumbersdata.product.product_id,
              'product_sub_id': this.serialNumbersdata.product_sub.product_sub_id,
              'model_no': this.serialNumbersdata.model.model_id,
              "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
              "sys_ah": this.serialNumbersdata.sys_ah,
              "voltage": this.serialNumbersdata.voltage.toUpperCase(),
              // "mfg_date": this.serialNumbersdata.manufacture_date,
              "ticket_status": this.Addbatterybank.value.ticket_status,

            });
            if (this.serialNumbersdata.expiry_day) {
              var expdates = this.serialNumbersdata.expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }
            if (this.serialNumbersdata.manufacture_date) {
              var mdates = this.serialNumbersdata.manufacture_date.split("-", 3);
              var Myear: number = +mdates[0];
              var Mmonth: number = +mdates[1];
              var Mday: number = +mdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

            this.Addbatterybank.controls["expiry_day"].setValue(EDate);
            this.Addbatterybank.controls["mfg_date"].setValue(MDate);
            this.onChangeProduct(this.serialNumbersdata.product.product_id)
            this.getsegment();
            // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

            this.Addbatterybank.controls['contract_duration'].setValue(this.serialNumbersdata.warranty_type.warranty_duration)

          }
          else if (this.flag == 1) {
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product_details = [];
            this.sub_product = [];
            this.model_id = [];
            this.warranty_type = [];
            // this.segment = [];
            // this.application = [];
            this.product_details.push(this.serialNumbersdata.product);
            this.sub_product.push(this.serialNumbersdata.product_sub);
            this.products_id = this.serialNumbersdata.product.product_id

            this.subid = this.serialNumbersdata.product_sub.product_sub_id;
            this.serialNumbersdata.model.forEach(element => {
              this.model_id.push(element);
            });
            if (result.response.manufacture_date[0]) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            console.log(this.subid);
            // this.Addbatterybank.patchValue({
            //   'product_id': this.products_id,
            //   'product_sub_id': this.subid,
            //   // "mfg_date": this.serialNumbersdata.manufacture_date,
            //   // "expiry_day": this.serialNumbersdata.expiry_day,
            //   "sys_ah": '',
            //   "segment_id": this.Addbatterybank.value.segment_id,
            //   "application_id": this.Addbatterybank.value.application_id,
            //   "organization_name": '',
            //   "invoice_number": '',
            //   "invoice_date": '',
            //   "warranty_type_id": '',
            //   "contract_duration": '',



            // });
            // this.Addbatterybank.patchValue({
            //   'product_id': this.products_id,
            //   'product_sub_id': this.subid,
            //   // 'model_no': this.serialNumbersdata.model.model_id,
            //   // "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
            //   // "mfg_date": this.serialNumbersdata.manufacture_date,
            //   // "expiry_day": this.serialNumbersdata.expiry_day,

            // });
            this.Addbatterybank = new FormGroup({
              "product_id": new FormControl(this.products_id, [Validators.required]),
              "product_sub_id": new FormControl(this.subid, [Validators.required]),
              "model_no": new FormControl('', [Validators.required]),
              "serial_no": new FormControl(this.Addbatterybank.value.serial_no, [Validators.required]),
              "invoice_date": new FormControl(null),
              "contract_duration": new FormControl(''),
              "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
              "sys_ah": new FormControl('', [Validators.required]),
              "segment_id": new FormControl(this.Addbatterybank.value.segment_id, [Validators.required]),
              "application_id": new FormControl(this.Addbatterybank.value.application_id, [Validators.required]),
              "invoice_number": new FormControl(''),
              "expiry_day": new FormControl(''),
              "warranty_type_id": new FormControl('', [Validators.required]),
              "mfg_date": new FormControl(EDate),
              "voltage": new FormControl('', [Validators.required]),
              "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
              "ticket_status": new FormControl(this.Addbatterybank.value.ticket_status, [Validators.required]),

            })
            this.getsegment();
            this.product_name = this.Addbatterybank.value.product_id
          }
          else if (this.flag == 2) {
            // this.getproduct();
            // this.Addbatterybank.patchValue({
            //   "mfg_date": result.response.manufacture_date[0].manufacture_date
            // });
            if (result.response.manufacture_date[0].manufacture_date) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            this.serialNumbersdata = result.response.manufacture_date[0]; //storing the api response in the array
            this.product_details = [];
            this.subproduct = [];
            this.model_id = [];
            this.serialNumbersdata.product.forEach(element => {
              this.product_details.push(element);
            });
            console.log(this.Addbatterybank.value)
            console.log(this.product_details)
            this.Addbatterybank = new FormGroup({
              "product_id": new FormControl('', [Validators.required]),
              "product_sub_id": new FormControl('', [Validators.required]),
              "model_no": new FormControl('', [Validators.required]),
              "serial_no": new FormControl(this.Addbatterybank.value.serial_no, [Validators.required]),
              "invoice_date": new FormControl(null),
              "contract_duration": new FormControl(''),
              "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
              "sys_ah": new FormControl('', [Validators.required]),
              "segment_id": new FormControl('', [Validators.required]),
              "application_id": new FormControl('', [Validators.required]),
              "invoice_number": new FormControl(''),
              "expiry_day": new FormControl(''),
              "warranty_type_id": new FormControl('', [Validators.required]),
              "mfg_date": new FormControl(EDate),
              "voltage": new FormControl('', [Validators.required]),
              "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
              "ticket_status": new FormControl(this.Addbatterybank.value.ticket_status, [Validators.required]),
            })
            console.log(this.Addbatterybank.value)

          }

          else if (this.flag == 3) {
            // this.product = [];
            // this.subproduct = [];
            // this.model_id = [];
            if (result.response.manufacture_date) {
              var expdates = result.response.manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            // this.Addbatterybank.patchValue({
            //   'product_id': '',
            //   'product_sub_id': '',
            //   'model_no': '',
            //   "warranty_type_id": '',
            //   "sys_ah": '',
            //   "voltage": '',
            //   "invoice_date": '',
            //   "invoice_number": ''

            // });
            this.Addbatterybank = new FormGroup({
              "product_id": new FormControl('', [Validators.required]),
              "product_sub_id": new FormControl('', [Validators.required]),
              "model_no": new FormControl('', [Validators.required]),
              "serial_no": new FormControl(this.Addbatterybank.value.serial_no, [Validators.required]),
              "invoice_date": new FormControl(null),
              "contract_duration": new FormControl(''),
              "battery_bank_id": new FormControl(this.site_details.battery_bank_id),
              "sys_ah": new FormControl('', [Validators.required]),
              "segment_id": new FormControl('', [Validators.required]),
              "application_id": new FormControl('', [Validators.required]),
              "invoice_number": new FormControl(''),
              "expiry_day": new FormControl(''),
              "warranty_type_id": new FormControl('', [Validators.required]),
              "mfg_date": new FormControl(EDate),
              "voltage": new FormControl('', [Validators.required]),
              "organization_name": new FormControl('', [Validators.required, this.noWhitespace]),
              "ticket_status": new FormControl(this.Addbatterybank.value.ticket_status, [Validators.required]),

            })
            // this.Addbatterybank.patchValue({
            //   "mfg_date": result.response.manufacture_date
            // });
            // this.Addbatterybank.value.mfg_date = result.response.manufacture_date; //storing the api response in the array

            this.getsegment();
            this.getproduct();
          }


          this.chRef.detectChanges();
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')

          this.serial_error = result.response.message;
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
          this.serial_error = "";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }



  }
  //get work type
  get_servicegroup() {
    var url = 'get_servicegroup/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.work_type = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  // addBatteryBankIdCustomerName(val) {
  //   console.log(this.productdata)
  //   // console.log(this.Addbatterybank.value);
  //   // console.log(this.Addbatterybank.value.invoice_date)
  //   if (this.Addbatterybank) {
  //     console.log("if")
  //     if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
  //       this.Addbatterybank.controls['invoice_date'].setValue('');
  //     }
  //     else {
  //       var year = this.Addbatterybank.value.invoice_date.year;
  //       var month = this.Addbatterybank.value.invoice_date.month;
  //       var day = this.Addbatterybank.value.invoice_date.day;
  //       var dateFormated = year + "-" + month + "-" + day;
  //       this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
  //     }
  //     console.log(this.Addbatterybank.value)
  //     var year = this.Addbatterybank.value.expiry_day.year;
  //     var month = this.Addbatterybank.value.expiry_day.month;
  //     var day = this.Addbatterybank.value.expiry_day.day;
  //     var exdate = year + "-" + month + "-" + day;
  //     // var ids = this.Addbatterybank.value.expiry_day.split("-", 3)
  //     // var year = ids[0];
  //     // var month = ids[1];
  //     // var day = ids[2];
  //     // var exdate = day + "-" + month + "-" + year;
  //     var year = this.Addbatterybank.value.mfg_date.year;
  //     var month = this.Addbatterybank.value.mfg_date.month;
  //     var day = this.Addbatterybank.value.mfg_date.day;
  //     // var id = this.Addbatterybank.value.mfg_date.split("-", 3)
  //     // var year = id[0];
  //     // var month = id[1];
  //     // var day = id[2];
  //     // var mfdate = day + "-" + month + "-" + year;
  //     var mfdate = year + "-" + month + "-" + day;

  //     this.Addbatterybank.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
  //     this.Addbatterybank.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
  //     this.serialProduct = [];
  //     var serial: any = {};
  //     this.serial = [];
  //     if (this.Addbatterybank.value.mfg_date == undefined || this.Addbatterybank.value.mfg_date == null || this.Addbatterybank.value.mfg_date == '') {
  //       var mfg_date = "";
  //       // this.EditTicketstatus.controls['cust_preference_date'].setValue('');
  //     }
  //     else {
  //       var years = this.Addbatterybank.value.mfg_date.year;
  //       var months = this.Addbatterybank.value.mfg_date.month;
  //       var days = this.Addbatterybank.value.mfg_date.day;
  //       mfg_date = years + "-" + months + "-" + days;
  //       var mfg_date1 =  days + "-" + months + "-" + years;
  //     }
  //     if (this.Addbatterybank.value.expiry_day == undefined || this.Addbatterybank.value.expiry_day == null || this.Addbatterybank.value.expiry_day == '') {
  //       var exp_date = "";
  //       // this.EditTicketstatus.controls['cust_preference_date'].setValue('');
  //     }
  //     else {
  //       var years = this.Addbatterybank.value.expiry_day.year;
  //       var months = this.Addbatterybank.value.expiry_day.month;
  //       var days = this.Addbatterybank.value.expiry_day.day;
  //       exp_date = years + "-" + months + "-" + days;
  //       var exp_date1 =  days + "-" + months + "-" + years;
  //     }
  //     serial = {
  //       "product_id": this.Addbatterybank.value.product_id,
  //       "product_sub_id": this.Addbatterybank.value.product_sub_id,
  //       "model": this.Addbatterybank.value.model_no['model_id'],
  //       "sys_ah": this.Addbatterybank.value.sys_ah,
  //       "voltage": this.Addbatterybank.value.voltage,
  //       "serial_no": this.Addbatterybank.value.serial_no,
  //       "invoice_number": this.Addbatterybank.value.invoice_number,
  //       "invoice_date": this.Addbatterybank.value.invoice_date,
  //       "mfg_date": mfg_date1,
  //       "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
  //       "expiry_date": exp_date1
  //     };
  //     console.log(serial)
  //     this.serial.push(serial);
  //     console.log(this.EditTicketstatus.value.cust_preference_date)
  //     if (this.EditTicketstatus.value.cust_preference_date == undefined || this.EditTicketstatus.value.cust_preference_date == null || this.EditTicketstatus.value.cust_preference_date == '') {
  //       var dateFormateds = "";
  //       // this.EditTicketstatus.controls['cust_preference_date'].setValue('');
  //     }
  //     else {
  //       var years = this.EditTicketstatus.value.cust_preference_date.year;
  //       var months = this.EditTicketstatus.value.cust_preference_date.month;
  //       var days = this.EditTicketstatus.value.cust_preference_date.day;
  //       dateFormateds = years + "-" + months + "-" + days;
  //     }

  //     if (this.EditTicketstatus.value.ticket_status == 23) {
  //       var approve_reason = '';
  //     }
  //     else {
  //       approve_reason = this.Accept
  //     }
  //     // if(this.Addbatterybank.value.warranty_type_id != null)
  //     // {
  //     //   var contract_type_id = this.site_details.contract_type.contract_type_id
  //     // }
  //     // else{
  //     //   contract_type_id = null
  //     // }
  //     // console.log(data)
  //     this.edit_ticket = ({
  //       "ticket_id": this.ticket_ids,
  //       "site_id": this.EditTicketstatus.value.site_id,
  //       "product_id": this.Addbatterybank.value.product_id,
  //       "product_sub_id": this.Addbatterybank.value.product_sub_id,
  //       "serial_no": this.serial,
  //       "country_id": this.EditTicketstatus.value.country_id,
  //       "state_id": this.EditTicketstatus.value.state_id,
  //       "city_id": this.EditTicketstatus.value.city_id,
  //       "location_id": this.EditTicketstatus.value.location_id,
  //       "current_status": this.EditTicketstatus.value.ticket_status,
  //       "employee_code": this.employee_code,
  //       "contact_person_name": this.EditTicketstatus.value.contact_person_name,
  //       "contact_person_number": this.EditTicketstatus.value.contact_person_number,
  //       "organization_name": this.Addbatterybank.value.organization_name,
  //       "segment_id": this.Addbatterybank.value.segment_id,
  //       "application_id": this.Addbatterybank.value.application_id,
  //       "plot_number": this.EditTicketstatus.value.plot_number,
  //       "street": this.EditTicketstatus.value.street,
  //       "landmark": this.EditTicketstatus.value.landmark,
  //       "post_code": this.EditTicketstatus.value.post_code,
  //       "work_type_id": this.EditTicketstatus.value.work_type_id,
  //       "callcategory_id": this.EditTicketstatus.value.call_category_id,
  //       "priority": this.EditTicketstatus.value.proioritys,
  //       "cust_preference_date": dateFormateds,
  //       "problem_desc": this.EditTicketstatus.value.desc,
  //       "model_no": this.Addbatterybank.value.model_no,
  //       "sys_ah": this.Addbatterybank.value.sys_ah,
  //       "voltage": this.Addbatterybank.value.voltage,
  //       "image": this.EditTicketstatus.value.image,
  //       "contract_type_id":this.Addbatterybank.value.warranty_type_id,
  //       "invoice_number": this.Addbatterybank.value.invoice_number,
  //       "manufacture_date": mfdate,
  //       "duration": this.Addbatterybank.value.contract_duration,
  //       "invoice_date": this.Addbatterybank.value.invoice_date,
  //       "expiry_day": exdate,
  //       "intimate_flag": val,
  //       "approve_reson": approve_reason
  //     })
  //   }
  //   else {
  //     console.log("else")
  //     // if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
  //     //   this.Addbatterybank.controls['invoice_date'].setValue('');
  //     // }
  //     // else {
  //     //   var year = this.Addbatterybank.value.invoice_date.year;
  //     //   var month = this.Addbatterybank.value.invoice_date.month;
  //     //   var day = this.Addbatterybank.value.invoice_date.day;
  //     //   var dateFormated = year + "-" + month + "-" + day;
  //     //   this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
  //     // }

  //     var ids = this.EditTicketstatus.value.expiry_day.split("-", 3)
  //     var year = ids[0];
  //     var month = ids[1];
  //     var day = ids[2];
  //     var exdate = day + "-" + month + "-" + year;
  //     var id = this.EditTicketstatus.value.mfg_date.split("-", 3)
  //     var year = id[0];
  //     var month = id[1];
  //     var day = id[2];
  //     var mfdate = day + "-" + month + "-" + year;
  //     // this.EditTicketstatus.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
  //     // this.EditTicketstatus.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
  //     this.serialProduct = [];
  //     var serial: any = {};
  //     this.serial = [];
  //     // serial = {
  //     //   "product_id": this.Addbatterybank.value.product_id,
  //     //   "product_sub_id": this.Addbatterybank.value.product_sub_id,
  //     //   "model": this.Addbatterybank.value.model_no,
  //     //   "sys_ah": this.Addbatterybank.value.sys_ah,
  //     //   "voltage": this.Addbatterybank.value.voltage,
  //     //   "serial_no": this.Addbatterybank.value.serial_no,
  //     //   "invoice_number": this.Addbatterybank.value.invoice_number,
  //     //   "invoice_date": this.Addbatterybank.value.invoice_date,
  //     //   "mfg_date": this.Addbatterybank.value.mfg_date,
  //     //   "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
  //     //   "expiry_date": this.Addbatterybank.value.expiry_day
  //     // };
  //     console.log(this.serials)
  //     this.serial.push(this.serials);
  //     console.log(this.EditTicketstatus.value.cust_preference_date)
  //     if (this.EditTicketstatus.value.cust_preference_date == undefined || this.EditTicketstatus.value.cust_preference_date == null || this.EditTicketstatus.value.cust_preference_date == '') {
  //       var dateFormateds = "";
  //     }addBatteryBankIdCustomerName

  //     if (this.EditTicketstatus.value.ticket_status == 23) {
  //       var approve_reason = '';
  //     }
  //     else {
  //       approve_reason = this.Accept
  //     }
  //     if(this.site_details.contract_type != null)
  //     {
  //       var contract_type_id = this.site_details.contract_type.contract_type_id
  //     }
  //     else{
  //       contract_type_id = null
  //     }
  //     // console.log(data)
  //     this.edit_ticket = ({
  //       "ticket_id": this.ticket_ids,
  //       "site_id": this.EditTicketstatus.value.site_id,
  //       "product_id": this.EditTicketstatus.value.product_id,
  //       "product_sub_id": this.EditTicketstatus.value.product_sub_id,
  //       "serial_no": this.serial,
  //       "country_id": this.EditTicketstatus.value.country_id,
  //       "state_id": this.EditTicketstatus.value.state_id,
  //       "city_id": this.EditTicketstatus.value.city_id,
  //       "location_id": this.EditTicketstatus.value.location_id,
  //       "current_status": this.EditTicketstatus.value.ticket_status,
  //       "employee_code": this.employee_code,
  //       "contact_person_name": this.EditTicketstatus.value.contact_person_name,
  //       "contact_person_number": this.EditTicketstatus.value.contact_person_number,
  //       "organization_name": this.EditTicketstatus.value.organization_name,
  //       "segment_id": this.EditTicketstatus.value.segment_id,
  //       "application_id": this.EditTicketstatus.value.application_id,
  //       "plot_number": this.EditTicketstatus.value.plot_number,
  //       "street": this.EditTicketstatus.value.street,
  //       "landmark": this.EditTicketstatus.value.landmark,
  //       "post_code": this.EditTicketstatus.value.post_code,
  //       "work_type_id": this.EditTicketstatus.value.work_type_id,
  //       "callcategory_id": this.EditTicketstatus.value.call_category_id,
  //       "priority": this.EditTicketstatus.value.proioritys,
  //       "cust_preference_date": dateFormateds,
  //       "problem_desc": this.EditTicketstatus.value.desc,
  //       "model_no": this.EditTicketstatus.value.model_no,
  //       "sys_ah": this.EditTicketstatus.value.sys_ah,
  //       "voltage": this.EditTicketstatus.value.voltage,
  //       "image": this.EditTicketstatus.value.image,
  //       "contract_type_id": contract_type_id,
  //       "invoice_number": this.site_details.invoice_number,
  //       "manufacture_date": mfdate,
  //       "duration": this.site_details.contract_type.contract_duration,
  //       "invoice_date": this.site_details.invoice_date,
  //       "expiry_day": exdate,
  //       "intimate_flag": val,
  //       "approve_reson": approve_reason
  //     })
  //   }
  //   var url = 'edit_ticket/'                                         //api url of add
  //   this.ajax.postdata(url, this.edit_ticket).subscribe((result) => {
  //     //if sucess
  //     if (result.response.response_code == "200") {
  //       this.modalService.dismissAll();
  //       this.toastr.success(result.response.message, 'Success');    //toastr message for success

  //     }
  //     //if error
  //     else if (result.response.response_code == "400" || result.response.response_code == "500") {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                             //prints if it encounters an error
  //   });


  // }
  addBatteryBankIdCustomerName(val) {
    console.log(this.productdata)
    // console.log(this.Addbatterybank.value);
    // console.log(this.Addbatterybank.value.invoice_date)
    if (this.Addbatterybank) {
      console.log(this.Addbatterybank.value.invoice_date)
      if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
        // this.Addbatterybank.controls['invoice_date'].setValue('');
        dateFormated = '';
      }
      else {
        var year = this.Addbatterybank.value.invoice_date.year;
        var month = this.Addbatterybank.value.invoice_date.month;
        var day = this.Addbatterybank.value.invoice_date.day;
        var dateFormated = year + "-" + month + "-" + day;
        // this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
      }
      console.log(this.Addbatterybank.value);
      if (this.Addbatterybank.value.expiry_day == undefined || this.Addbatterybank.value.expiry_day == null || this.Addbatterybank.value.expiry_day == '') {
        // this.Addbatterybank.controls['expiry_day'].setValue('');
        this.edit_exdate = null;
      }
      else {
        var year = this.Addbatterybank.value.expiry_day.year;
        var month = this.Addbatterybank.value.expiry_day.month;
        var day = this.Addbatterybank.value.expiry_day.day;
        var exdate_day = day + "-" + month + "-" + year;
        this.edit_exdate = year + "-" + month + "-" + day;
      }
      if (this.Addbatterybank.value.mfg_date == undefined || this.Addbatterybank.value.mfg_date == null || this.Addbatterybank.value.mfg_date == '') {
        // this.Addbatterybank.controls['mfg_date'].setValue('');
        this.edit_mfdate = null;
      }
      else {
        var year = this.Addbatterybank.value.mfg_date.year;
        var month = this.Addbatterybank.value.mfg_date.month;
        var day = this.Addbatterybank.value.mfg_date.day;
        var mfdate_day = day + "-" + month + "-" + year;
        this.edit_mfdate = year + "-" + month + "-" + day;
      }


      this.Addbatterybank.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
      this.Addbatterybank.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
      this.serialProduct = [];
      var serial: any = {};
      this.serial = [];
      serial = {
        "product_id": this.Addbatterybank.value.product_id,
        "product_sub_id": this.Addbatterybank.value.product_sub_id,
        "model": this.Addbatterybank.value.model_no,
        "sys_ah": this.Addbatterybank.value.sys_ah,
        "voltage": this.Addbatterybank.value.voltage,
        "serial_no": this.Addbatterybank.value.serial_no,
        "invoice_number": this.Addbatterybank.value.invoice_number,
        "invoice_date": dateFormated,
        "mfg_date": mfdate_day,
        "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
        "expiry_date": exdate_day
      };
      console.log(serial)
      this.serial.push(serial);
      console.log(this.EditTicketstatus.value.cust_preference_date)
      if (this.EditTicketstatus.value.cust_preference_date == undefined || this.EditTicketstatus.value.cust_preference_date == null || this.EditTicketstatus.value.cust_preference_date == '') {
        var dateFormateds = "";
        // this.EditTicketstatus.controls['cust_preference_date'].setValue('');
      }
      else {
        var years = this.EditTicketstatus.value.cust_preference_date.year;
        var months = this.EditTicketstatus.value.cust_preference_date.month;
        var days = this.EditTicketstatus.value.cust_preference_date.day;
        dateFormateds = years + "-" + months + "-" + days;
      }
      var approve_reason="";
      if (this.Addbatterybank.value.ticket_status == 23) {
        approve_reason = this.Reject;
      }
      else {
        approve_reason = this.Accept
      }
      // console.log(data)
      this.edit_ticket = ({
        "ticket_id": this.ticket_ids,
        "customer_code": this.EditTicketstatus.value.customer_code,
        "contact_number": this.EditTicketstatus.value.contact_number,
        "alternate_number": this.EditTicketstatus.value.alternate_number,
        "email_id": this.EditTicketstatus.value.email_id,
        "site_id": this.EditTicketstatus.value.site_id,
        "product_id": this.Addbatterybank.value.product_id,
        "product_sub_id": this.Addbatterybank.value.product_sub_id,
        "serial_no": this.serial,
        "country_id": this.EditTicketstatus.value.country_id,
        "state_id": this.EditTicketstatus.value.state_id,
        "city_id": this.EditTicketstatus.value.city_id,
        "location_id": this.EditTicketstatus.value.location_id,
        "current_status": this.Addbatterybank.value.ticket_status,
        "employee_code": this.employee_code,
        "contact_person_name": this.EditTicketstatus.value.contact_person_name,
        "contact_person_number": this.EditTicketstatus.value.contact_person_number,
        "organization_name": this.Addbatterybank.value.organization_name,
        "segment_id": this.Addbatterybank.value.segment_id,
        "application_id": this.Addbatterybank.value.application_id,
        "plot_number": this.EditTicketstatus.value.plot_number,
        "street": this.EditTicketstatus.value.street,
        "landmark": this.EditTicketstatus.value.landmark,
        "post_code": this.EditTicketstatus.value.post_code,
        "work_type_id": this.EditTicketstatus.value.work_type_id,
        "callcategory_id": this.EditTicketstatus.value.call_category_id,
        "priority": this.EditTicketstatus.value.proioritys,
        "cust_preference_date": dateFormateds,
        "problem_desc": this.EditTicketstatus.value.desc,
        "model_no": this.Addbatterybank.value.model_no,
        "sys_ah": this.Addbatterybank.value.sys_ah,
        "voltage": this.Addbatterybank.value.voltage,
        "image": this.EditTicketstatus.value.image,
        "contract_type_id": this.Addbatterybank.value.warranty_type_id,
        "invoice_number": this.Addbatterybank.value.invoice_number,
        "manufacture_date": this.edit_mfdate,
        "duration": this.Addbatterybank.value.contract_duration,
        "invoice_date": dateFormated,
        "expiry_day": this.edit_exdate,
        "intimate_flag": val,
        "approve_reson": approve_reason
      })
    }
    else {
      // if (this.Addbatterybank.value.invoice_date == undefined || this.Addbatterybank.value.invoice_date == null || this.Addbatterybank.value.invoice_date == '') {
      //   this.Addbatterybank.controls['invoice_date'].setValue('');
      // }
      // else {
      //   var year = this.Addbatterybank.value.invoice_date.year;
      //   var month = this.Addbatterybank.value.invoice_date.month;
      //   var day = this.Addbatterybank.value.invoice_date.day;
      //   var dateFormated = year + "-" + month + "-" + day;
      //   this.Addbatterybank.controls['invoice_date'].setValue(dateFormated);
      // }
      if (this.EditTicketstatus.value.expiry_day == undefined || this.EditTicketstatus.value.expiry_day == null || this.EditTicketstatus.value.expiry_day == '') {
        this.edit_exdate = null;
      }
      else {
        var ids = this.EditTicketstatus.value.expiry_day.split("-", 3)
        var year = ids[0];
        var month = ids[1];
        var day = ids[2];
        this.edit_exdate = day + "-" + month + "-" + year;
      }
      console.log(this.EditTicketstatus.value.mfg_date)
      if (this.EditTicketstatus.value.mfg_date == undefined || this.EditTicketstatus.value.mfg_date == null || this.EditTicketstatus.value.mfg_date == '') {
        this.edit_mfdate = null;
      }
      else {
        var id = this.EditTicketstatus.value.mfg_date.split("-", 3)
        var year = id[0];
        var month = id[1];
        var day = id[2];
        this.edit_mfdate = day + "-" + month + "-" + year;
      }

      // this.EditTicketstatus.controls['application_id'].setValue(+this.Addbatterybank.value.application_id);
      // this.EditTicketstatus.controls['segment_id'].setValue(+this.Addbatterybank.value.segment_id);
      this.serialProduct = [];
      var serial: any = {};
      this.serial = [];
      // serial = {
      //   "product_id": this.Addbatterybank.value.product_id,
      //   "product_sub_id": this.Addbatterybank.value.product_sub_id,
      //   "model": this.Addbatterybank.value.model_no,
      //   "sys_ah": this.Addbatterybank.value.sys_ah,
      //   "voltage": this.Addbatterybank.value.voltage,
      //   "serial_no": this.Addbatterybank.value.serial_no,
      //   "invoice_number": this.Addbatterybank.value.invoice_number,
      //   "invoice_date": this.Addbatterybank.value.invoice_date,
      //   "mfg_date": this.Addbatterybank.value.mfg_date,
      //   "warranty_type_id": this.Addbatterybank.value.warranty_type_id,
      //   "expiry_date": this.Addbatterybank.value.expiry_day
      // };
      console.log(this.serials)
      this.serial.push(this.serials);
      console.log(this.EditTicketstatus.value.cust_preference_date)
      if (this.EditTicketstatus.value.cust_preference_date == undefined || this.EditTicketstatus.value.cust_preference_date == null || this.EditTicketstatus.value.cust_preference_date == '') {
        var dateFormateds = "";
      }
      else {
        var years = this.EditTicketstatus.value.cust_preference_date.year;
        var months = this.EditTicketstatus.value.cust_preference_date.month;
        var days = this.EditTicketstatus.value.cust_preference_date.day;
        dateFormateds = years + "-" + months + "-" + days;
      }
      
      if (this.Addbatterybank.value.ticket_status == 23) {
        approve_reason = this.Reject;
      }
      else {
        approve_reason = this.Accept
      }
      if (this.site_details.contract_type == null || this.site_details.contract_type == '') {
        var contract_type = "";
      }
      else {
        contract_type = this.site_details.contract_type.contract_type_id
      }
      // console.log(data)
      this.edit_ticket = ({
        "ticket_id": this.ticket_ids,
        "customer_code": this.EditTicketstatus.value.customer_code,
        "contact_number": this.EditTicketstatus.value.contact_number,
        "alternate_number": this.EditTicketstatus.value.alternate_number,
        "email_id": this.EditTicketstatus.value.email_id,
        "site_id": this.EditTicketstatus.value.site_id,
        "product_id": this.EditTicketstatus.value.product_id,
        "product_sub_id": this.EditTicketstatus.value.product_sub_id,
        "serial_no": this.serial,
        "country_id": this.EditTicketstatus.value.country_id,
        "state_id": this.EditTicketstatus.value.state_id,
        "city_id": this.EditTicketstatus.value.city_id,
        "location_id": this.EditTicketstatus.value.location_id,
        "current_status": this.Addbatterybank.value.ticket_status,
        "employee_code": this.employee_code,
        "contact_person_name": this.EditTicketstatus.value.contact_person_name,
        "contact_person_number": this.EditTicketstatus.value.contact_person_number,
        "organization_name": this.EditTicketstatus.value.organization_name,
        "segment_id": this.EditTicketstatus.value.segment_id,
        "application_id": this.EditTicketstatus.value.application_id,
        "plot_number": this.EditTicketstatus.value.plot_number,
        "street": this.EditTicketstatus.value.street,
        "landmark": this.EditTicketstatus.value.landmark,
        "post_code": this.EditTicketstatus.value.post_code,
        "work_type_id": this.EditTicketstatus.value.work_type_id,
        "callcategory_id": this.EditTicketstatus.value.call_category_id,
        "priority": this.EditTicketstatus.value.proioritys,
        "cust_preference_date": dateFormateds,
        "problem_desc": this.EditTicketstatus.value.desc,
        "model_no": this.EditTicketstatus.value.model_no.model_id,
        "sys_ah": this.EditTicketstatus.value.sys_ah,
        "voltage": this.EditTicketstatus.value.voltage,
        "image": this.EditTicketstatus.value.image,
        "contract_type_id": contract_type,
        "invoice_number": this.site_details.invoice_number,
        "manufacture_date": this.edit_mfdate,
        "duration": this.site_details.contract_duration,
        "invoice_date": this.site_details.invoice_date,
        "expiry_day": this.edit_exdate,
        "intimate_flag": val,
        "approve_reson": approve_reason
      })
    }
    console.log(this.edit_ticket);
    // return this.edit_ticket;
    var url = 'edit_ticket/'                                         //api url of add
    this.ajax.postdata(url, this.edit_ticket).subscribe((result) => {
      //if sucess
      if (result.response.response_code == "200") {
        this.ProductLocation.controls['product_id'].setValue('');
        this.ProductLocation.controls['location_id'].setValue('');
        this.get_unassigned_tickets();             //call unassigned ticket api
        this.showTables = false;
        this.get_rejected_tickets();
        this.modalService.dismissAll();
        this.toastr.success(result.response.message, 'Success');    //toastr message for success

      }
      //if error
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });


  }
  // ---------------------------------function for getters----------------------------------------------------------------//
  get log() {
    return this.ProductLocation.controls;                       //error logs for productlocation 
  }
  get raise() {
    return this.EditTicketstatus.controls;                       //error logs for productlocation 
  }

  //-----------------------------------function for gettinf details for view----------------------------------------------//

  // function for getting the ticket details based on TicketId 
  get_ticket_details(tickt_id, view_tcktdetails) {
    console.log(this.tickt_id)
    var ticket_id = tickt_id; // country: number                                    
    var url = 'get_ticket_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    console.log(url, id, ticket_id);
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details = result.response.data;                                           //reloading the component
        this.modalService.open(view_tcktdetails, {
          size: 'lg',
          // windowClass: "center-modalsm",
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the Technician details based on TechnicianId 
  get_technician_details(technician_id, ticket_id, view_techniciandetails) {

    var ticket_id = ticket_id
    var technician_id = technician_id
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;
        this.modalService.open(view_techniciandetails, {
          size: 'lg',
          windowClass: "center-modalsm",
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //-----------------------------------------Function for passing location and Product------------------------------------//
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code };
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }

  //onchange functoin for getting the location id
  onChangeLocation(location_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.location_id = this.ProductLocation.value.location_id;
    this.product_id = this.ProductLocation.value.product_id;
    console.log(this.ProductLocation.value.location_id);
    if (this.location_id == "") {
      this.location_id = null;
    }
    else {
      this.location_id = this.ProductLocation.value.location_id;
    }
    if (this.product_id == "") {
      this.product_id = null;
    }
    else {
      this.product_id = this.ProductLocation.value.product_id;
    }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTable = false;

      this.get_unassigned_tickets();
    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableAssigned = false;
      this.get_assigned_tickets();
    }

    if (this.tabvalue == 'tab-selectbyid3') {
      this.showTableAccepted = false;
      this.accepted_tickets();
    }

    if (this.tabvalue == 'tab-selectbyid4') {
      this.showTableDiffered = false;
      this.deffered_tickets();
    }

  }
  onchangeforsearch() {
    console.log(this.ProductLocation.value)
  }
  //-------------------------------------------Unassigned Tickets functions starts----------------------------------------//
  // function for getting the unassigned ticket based on the product and location 
  // get_unassigned_tickets() {
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
  //   var url = 'unassigned_ticket_details/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.unassigned_tckt = result.response.data;      //storing the api response in the array
  //       this.showTable = true; // displaying the table
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();


  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTable = true; // displaying the table
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();
  //     }

  //     else {
  //       this.toastr.error(result.response.message, 'Error');
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();
  //     }
  //   }, (err) => {
  //     this.overlayRef.detach();
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }


  get_unassigned_tickets() {
    console.log("asdasd")
    // this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_product,"bfv")
    // console.log(this.name_location,"bfv")
    this.unassigned_tckt = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
    //  console.log(search_value)
    var search_value = ""
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
    var url = "unassigned_ticket_details/"
    //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.unassigned_tckt = result.response.data;      //storing the api response in the array
        // this.showTable = true; // displaying the table
        // this.overlayRef.detach();


        // this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {

          "deferLoading": result.response.recordsTotal,

          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',

          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            console.log(search_value, "search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming = "unassigned_ticket_details/"
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.unassigned_tckt = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();

            });

          },
          "columnDefs": [
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Segmnet", "targets": 2, "searchable": true },
            { "name": "Customer Organization", "targets": 3, "searchable": true },
            { "name": "End User", "targets": 4, "searchable": true },
            { "name": "Product Category-System Ah", "targets": 5, "searchable": true },
            { "name": "District-Town", "targets": 6, "searchable": true },
            { "name": "Raised Time", "targets": 7, "searchable": true },
          ],
        }

        $('#unassigned').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#unassigned').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.unassigned_tckt = [];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }



  get_rejected_tickets() {
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    var url = 'rejected_ticket_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.rejected_tckt = result.response.data;      //storing the api response in the array
        this.showTables = true; // displaying the table
        this.overlayRef.detach();
        this.chRef.detectChanges();


      }
      else if (result.response.response_code == "500") {
        this.showTables = true; // displaying the table
        this.overlayRef.detach();
      }

      else {
        this.toastr.error(result.response.message, 'Error');
        this.overlayRef.detach();
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  // function for getting the Available technician details based on TicketId 
  get_avilable_technicians(tickt_id, TechnicianTable) {
    // console.log(tickt_id)
    // this.tickt_id = tickt_id;
    // var url = 'avilable_technicians/?';             // api url for getting the details with using post params
    // var id = "ticket_id";
    // this.ajax.getdataparam(url, id, this.tickt_id).subscribe((result) => {
    //   if (result.response.response_code == "200" || result.response.response_code == "400") {
    //     this.avilable_technicians_details = result.response.data;
    //     this.SpareTechTable = true; // displaying the table
    //     this.modalService.open(TechnicianTable, {
    //       size: 'lg',
    //       // windowClass: "center-modalsm"                                  //specifies the size of the dialog box
    //       backdrop: 'static',                                    // modal will not close by outside click
    //       keyboard: false,                                       // modal will not close by keyboard click
    //     });
    //     this.chRef.detectChanges();
    //   }

    // }, (err) => {
    //   console.log(err);                                        //prints if it encounters an error
    // });
    this.tickt_id = tickt_id
    var ticket_id = tickt_id;
    var data = { "ticket_id": ticket_id, "employee_code": this.employee_code }
    var url = 'avilable_technicians/'                                           //api url of Assign Ticket                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.avilable_technicians_details = result.response.data; //storing the api response in the array    
          this.SpareTechTable = true;
          this.modalService.open(TechnicianTable, {
            size: 'lg',
            backdrop: 'static',              //popup outside click by Sowndarya
            keyboard: false
          });
        }
        else if (result.response.response_code == "500") {
          this.avilable_technicians_details = result.response.data; //storing the api response in the array 
          this.SpareTechTable = true;
          this.modalService.open(TechnicianTable, {
            size: 'lg',
            backdrop: 'static',              //popup outside click by Sowndarya
            keyboard: false
          });
        }
        else {
          this.toastr.error("Something went wrong");
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
  }

  //function for Assign the ticket to the technician based on the ticket id and technician id
  technician_assign(id_details) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to assign",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Assign!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        console.log(id_details)
        console.log(this.tickt_id)
        var data = { "servicedesk_id": this.servicedesk_id, "technician_id": id_details.technician_id, "ticket_id": this.tickt_id, "secondary_tec_id": null }
        var url = 'technician_assign/'                                           //api url of Assign Ticket                                    
        this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200")                           //if sucess
            {
              //reloading the component
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
              this.modalService.dismissAll();                                 //to close the modal box
            }
            else if (result.response.response_code == "400") {                  //if failure
              this.toastr.error(result.response.message, 'Error');            //toastr message for error
            }
            else {                                                                     //if not sucess
              this.toastr.error(result.response.message, 'Error');        //toastr message for error
            }
          }, (err) => {                                                           //if error
            console.log(err);                                                 //prints if it encounters an error
          });
      }
    })
  }


  //-------------------------------------------Unassigned Tickets functions end-------------------------------------------//

  //-------------------------------------------Assigned Tickets functions starts------------------------------------------//
  // function for getting the unassigned ticket based on the product and location 
  // get_assigned_tickets() {
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
  //   var url = 'assigned_ticket_details/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.assigned_tckt = result.response.data;
  //       this.showTableAssigned = true; // displaying the table

  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();

  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTableAssigned = true; // displaying the table
  //       this.overlayRef.detach();
  //     }

  //     else {
  //       this.toastr.error(result.response.message, 'Error');
  //     }
  //   }, (err) => {
  //     this.overlayRef.detach();
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }

  get_assigned_tickets() {
    // this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.assigned_tckt = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
    //  console.log(search_value)
    var search_value = ""
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
    var url = 'assigned_ticket_details/'
    //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        // this.assigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        // this.assigned_tckt = result.response.data;              //storing the api response in the array             
        // this.showTable = true;
        this.assigned_tckt = result.response.data;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {

          "deferLoading": result.response.recordsTotal,

          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',

          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            console.log(search_value, "search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming = 'assigned_ticket_details/'
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.assigned_tckt = result.response.data; this.return_draw = result.response.draw
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();

            });

          },
          "columnDefs": [
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Segmnet", "targets": 2, "searchable": true },
            { "name": "Customer Organization", "targets": 3, "searchable": true },
            { "name": "End User", "targets": 4, "searchable": true },
            { "name": "Technician Name", "targets": 5, "searchable": true },
            { "name": "Product Category-System Ah", "targets": 6, "searchable": true },
            { "name": "District-Town", "targets": 7, "searchable": true },
            { "name": "Ticket Raised Time", "targets": 8, "searchable": true },
            { "name": "Assigned Time", "targets": 9, "searchable": true },
          ],
        }

        $('#assigned').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#assigned').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.assigned_tckt = [];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();

      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }


  get_all_ticket_list() {
    var url = 'get_all_ticket_list/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details_export = result.response.data; //storing the api response in the array
        this.excelservice.exportAsExcelFile(this.ticket_details_export, 'Ticket export');

      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  exportAsXLSX(): void {
    this.excelservice.exportAsExcelFile(this.ticket_details_export, 'Unassigned Tickets');
  }
  //function  for reassign the tickets
  re_assign_technician(tickt_id) {
    this.tickt_id = tickt_id
    console.log(this.tickt_id)
    var ticket_id = tickt_id; // country: number                                    
    var url = 're_assign_technician/?';             // api url for getting the details with using post params
    // var id = "ticket_id";
    // console.log(url, id, ticket_id);
    var data = { "ticket_id": ticket_id, "employee_code": this.employee_code }
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.re_assign_technician = result.response.data;

        }

      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
  }
  //-------------------------------------------Assigned Tickets functions ends--------------------------------------------//

  //-------------------------------------------accepted_tickets Tickets functions starts----------------------------------//
  // function for getting the Accepted ticket based on the product and location 
  // accepted_tickets() {
  //   this.accepted_tckt = [];
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
  //   var url = 'accepted_ticket_details/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       // this.accepted_tckt = result.response.data;              //storing the api response in the array
  //       result.response.data.forEach(element => {
  //         var date = element.acceptance_date_time.split(",", 2);
  //         element.acceptance_date_time = date[0];
  //         this.accepted_tckt.push(element);
  //       })
  //       this.showTableAccepted = true;
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();
  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTableAccepted = true;
  //       this.overlayRef.detach();
  //     }

  //     else {
  //       this.toastr.error(result.response.message, 'Error');
  //     }

  //   }, (err) => {
  //     this.overlayRef.detach();
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }
  accepted_tickets() {
    // this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.accepted_tckt = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
    console.log(search_value)
    var search_value = ""
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
    var url = 'accepted_ticket_details/'
    //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array             
        // this.showTable = true;
        console.log(this.accepted_tckt, "this.accepted_tckt")
        // this.accepted_tckt = result.response.data;              //storing the api response in the array

        if (this.accepted_tckt != []) {
          result.response.data.forEach(element => {
            var date = element.acceptance_date_time.split(",", 2);
            element.acceptance_date_time = date[0];
            this.accepted_tckt.push(element);
          })
        }
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {

          "deferLoading": result.response.recordsTotal,

          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',

          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            console.log(search_value, "search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming = 'accepted_ticket_details/'
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.accepted_tckt = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();

            });

          },
          "columnDefs": [
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Customer Name", "targets": 2, "searchable": true },
            { "name": "Ticket Type", "targets": 3, "searchable": true },
            { "name": "Technician Id", "targets": 4, "searchable": true },
            { "name": "Product Category", "targets": 5, "searchable": true },
            { "name": "Town", "targets": 6, "searchable": true },
            { "name": "Sub Product Category", "targets": 7, "searchable": true },
            { "name": "Accepted Date", "targets": 8, "searchable": true },


          ],
        }

        $('#accepted').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#accepted').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.accepted_tckt = [];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {
        this.accepted_tckt = [];
        //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  //-------------------------------------------accepted_tickets Tickets functions ends------------------------------------//

  //-------------------------------------------Deffered Tickets functions starts------------------------------------------//
  deffered_tickets() {
    this.deffered_tckt = [];
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    var url = 'deffered_ticket_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // this.deffered_tckt = result.response.data;              //storing the api response in the array
        result.response.data.forEach(element => {
          var date = element.cust_preference_date.split(",", 2);
          element.cust_preference_date = date[0];
          this.deffered_tckt.push(element);
        })
        this.overlayRef.detach();
        this.showTableDiffered = true;
        this.chRef.detectChanges();
        this.dtTrigger.next();
      }
      else if (result.response.response_code == "500") {
        this.showTableDiffered = true;
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }



  //-------------------------------------------Deffered Tickets functions ends--------------------------------------------//
  //--------------------------------------------------------------------------------------------------//
  fetchNews(evt: any) {
    this.tabvalue = evt.nextId;
    console.log(evt.nextId);
    console.log(this.tabvalue);
    this.product_id = null;
    this.location_id = null;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['location_id'].setValue('');
    if (this.tabvalue == "tab-selectbyid2") {
      this.showTableAssigned = false;
      this.get_assigned_tickets();
      this.overlayRef.attach(this.LoaderComponentPortal);
    }
    else if (this.tabvalue == "tab-selectbyid3") {
      this.showTableAccepted = false;
      this.accepted_tickets();
      this.overlayRef.attach(this.LoaderComponentPortal);
    } else if (this.tabvalue == "tab-selectbyid4") {
      this.showTableDiffered = false;
      this.deffered_tickets();
      this.overlayRef.attach(this.LoaderComponentPortal);
    }
    else if (this.tabvalue == "tab-selectbyid1") {
      this.showTable = false;
      this.get_unassigned_tickets();
      this.overlayRef.attach(this.LoaderComponentPortal);
    }
    else if (this.tabvalue == "tab-selectbyid5") {
      this.showTables = false;
      this.get_rejected_tickets();
      this.overlayRef.attach(this.LoaderComponentPortal);
    }

    console.log(evt); // has nextId that you can check to invoke the desired function
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}