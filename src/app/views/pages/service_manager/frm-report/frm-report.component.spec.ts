import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FRMReportComponent } from './frm-report.component';

describe('FRMReportComponent', () => {
  let component: FRMReportComponent;
  let fixture: ComponentFixture<FRMReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FRMReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FRMReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
