import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianTrackingComponent } from './technician-tracking.component';

describe('TechnicianTrackingComponent', () => {
  let component: TechnicianTrackingComponent;
  let fixture: ComponentFixture<TechnicianTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
