import { Component, OnInit, ViewContainerRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'kt-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  warehousedetails: any;              //list of warehouse from get API
  warehousename: string;                   //fields for this component
  leadtime: string;                              //fields for this component
  Edit_warehouse_form: FormGroup;                //formgroup for edit warehouse
  Add_warehouse_form: FormGroup;
  edit_data: any;                             //get datas from edit API
  warehouse: any;                              //get warehouse id for delete 
  showTable: boolean = false;                  //loader show and hide
  overlayRef: OverlayRef;                       //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;  //loader component
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  loading: boolean = false;
  error: string;

  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router, private toastr: ToastrService, vcr: ViewContainerRef, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    //Defines the required services to be used 
    this.Add_warehouse_form = new FormGroup({
      "warehouse_name": new FormControl("", [Validators.required,this.noWhitespace]),
      "lead_time": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")])
    });
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    this.warehousename = "";
    this.leadtime = "";
    this.getwarehousedetails();
  }
  get f() { return this.Add_warehouse_form.controls; }
  get e() { return this.Edit_warehouse_form.controls; }
  //get Warehouse master from database
  getwarehousedetails() {
    this.error = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_warehouse/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.warehousedetails = result.response.data;                    //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.error = result.response.message;
      }
      else {
        this.error = "Something Went Wrong";
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //function for submit create warehouse
  addwarehouse() {
    this.error = '';
    this.loading = true;                                       //submit button with onclick loader
    var data = this.Add_warehouse_form.value;                  //Data to be passed for add api
    var url = 'add_warehouse/'                                         //api url of add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                //if sucess
        // this.ngOnInit();  
        this.loading = false;                                       //dismissing the submit button loader
        this.showTable = false;
        this.chRef.detectChanges();
        this.getwarehousedetails();                                       //reloading the component
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.modalService.dismissAll();
        this.Add_warehouse_form.reset();
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {               //if failiure
        this.loading = false;                                          //dismissing the submit button loader
        this.error = result.response.message;        //toastr message for error
      }
      else {                                                            //if not sucess
        this.loading = false;                                            //dismissing the submit button loader
        this.error = "Something Went Wrong";      //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  // Function to open the modal box 
  openLarge(content6) {
    this.Add_warehouse_form.reset();
    this.error = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                    // modal popup for resizing the popup                                                //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to store the line item selected to edit
  editpackage(data) {
    this.edit_data = data;
    //formgroup for edit Warehouse
    this.Edit_warehouse_form = new FormGroup({
      "warehouse_id": new FormControl(this.edit_data.warehouse_id, Validators.required),
      "warehouse_name": new FormControl(this.edit_data.warehouse_name, [Validators.required,this.noWhitespace]),
      "lead_time": new FormControl(this.edit_data.lead_time, [Validators.required, Validators.pattern("^[0-9]*$")])
    });                                         //Stores the data into the array variable
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to call the edit api 
  editwarehouse(edit_data) {
    this.loading = true;      //addig update button with spinner
    this.error = '';
    if (this.Edit_warehouse_form.invalid) {

    }
    else {
      var url = 'edit_warehouse/'                                           //api url of edit api                                    
      this.ajax.postdata(url, this.Edit_warehouse_form.value)
        .subscribe((result) => {
          if (result.response.response_code == "200")                   //if sucess
          {
            this.loading = false;      //dismissing update button  spinner  
            this.showTable = false;
            this.chRef.detectChanges();
            this.getwarehousedetails();                                       //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.modalService.dismissAll();                                 //to close the modal box
          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                 //if failiure
            this.loading = false;      //dismissing update button  spinner  
            this.error = result.response.message;        //toastr message for error
          }
          else {                                                             //if not sucess
            this.loading = false;      //dismissing update button  spinner  
            this.error = "Something Went Wrong";     //toastr message for error
          }
        }, (err) => {
          console.log(err);                                                 //prints if it encounters an error
        });
    }
  }
  // functon for delete warehouse
  trashwarehouse(data) {
    this.error = '';
    this.warehouse = data;
    var warehouse_id = this.warehouse.warehouse_id;           //Data to be passed for delete api    
    var warehouseid = +warehouse_id;
    var url = 'delete_warehouse/?';             // api url for delete warehouse
    var id = "warehouse_id";                       //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.getdataparam(url, id, warehouseid).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.showTable = false;
            this.chRef.detectChanges();
            this.getwarehousedetails();                                              //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                  //if failiure
            this.toastr.error(result.response.message, 'Error');        //toastr message for success
          }
          else {       
            this.toastr.error( "Something Went Wrong", 'Error');        //toastr message for success                                                   //if not sucess
            // this.error = "Something Went Wrong";        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  //--------------------------------------------------------------------------------------------------//


    //saranya22-dec-2020 for alphabet block in keyboard
    numberOnly(event): boolean {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }

}
