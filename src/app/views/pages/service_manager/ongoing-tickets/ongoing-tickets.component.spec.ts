import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OngoingTicketsComponent } from './ongoing-tickets.component';

describe('OngoingTicketsComponent', () => {
  let component: OngoingTicketsComponent;
  let fixture: ComponentFixture<OngoingTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngoingTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OngoingTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
