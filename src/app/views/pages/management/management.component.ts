// import { Component, OnInit, Input } from '@angular/core';
// import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
// import { ToastrService } from 'ngx-toastr'; 
// import Swal from 'sweetalert2' 
// import { DynamicGrid } from '../../../dynamicgrid.service'
 
// @Component({
//   selector: 'kt-management',
//   templateUrl: './management.component.html',
//   styleUrls: ['./management.component.scss']
// })
// export class ManagementComponent implements OnInit {
//   Manage: FormGroup; //form group
//   @Input() index: number; //index for s.no
//   plan: string;
//   package: string;
//   amount:number;
//   id : number;

//   constructor(private toastr: ToastrService) { 
   
//   }  
  
// // dynamicArray and newDynamic used for add and delete function
//   dynamicArray: Array<DynamicGrid> = [];  
//   newDynamic: any = {};  
//   // arrayItems used for view the input
//   arrayItems:Array<{plan: string, id: number, package: string, amount:number}> = []; 
//   ngOnInit(): void {  
//     this.arrayItems = [];
//     // dynamicArray from sevice
//       this.newDynamic = {Package: "", Plan: "",  Amount:''};  
//       this.dynamicArray.push(this.newDynamic); 

//       // validate for form control using form group
//       this.Manage = new FormGroup({
//         "package": new FormControl("", [Validators.required]),
//         "plan": new FormControl("", [Validators.required]),
//         "discount":new FormControl("", [Validators.required]),
//         // "discount":new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
//         "amount":new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
        
//         });
//   }  
// //  first table add icon function
//   addRow() {    

     
//     this.newDynamic = {Package: "", Plan: "", Discount:"", Amount:""};
 
//     this.dynamicArray.push(this.newDynamic);  
 
//     this.toastr.success('New row added successfully', 'New Row');  
//     console.log(this.newDynamic);  
//     return true;
    
  
// }  

// // first table delete icon function
// deleteRow(index) {  

//     if(this.dynamicArray.length ==1) {  
//       this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
//         return false;  
//     } else {  
//         this.dynamicArray.splice(index, 1);  
//         this.toastr.warning('Row deleted successfully', 'Delete row');  
//         return true;  
//     }  
// }
// // second table delete icon
// deleterow(index) {  
//   if(this.dynamicArray.length ==1) {  
//     Swal.fire({
//       title: 'Are you sure?',
//       text: 'You want to delete the package?',
//       icon: 'warning',
//       showCancelButton: true,
//       confirmButtonText: 'Yes, Delete',
//       cancelButtonText: 'No, Cancel'
//     }).then((result) => {
//       if (result.value) {
//         Swal.fire(
//           'Alert',
//           'Deleted Successfully.',
//           'success'
//         )
     
//       } else if (result.dismiss === Swal.DismissReason.cancel) {
//         Swal.fire(
//           'Cancelled',
//           'Package is safe :)',
//           'error'
//         )
//       }
//     })
//       return false;  
//   } else {  
//       this.dynamicArray.splice(index, 1);  
//       this.toastr.warning('Package  deleted successfully', 'Delete Package');  
//       return true;  
//   }  
// }

// //  afer validate to submit
// submitform(){
//   if (this.Manage.valid) {
//     console.log("success");
//     console.log(this.Manage.value)
//   }
//   else
//   {
//     console.log(this.Manage.value)
//   }
// // take the values of form group and push for view table
// var plan = this.Manage.value.plan;
// 	if(plan == "")
// 	{
// 		this.plan = "Monthly";
// 	}
// 	else{
// 		this.plan = plan;
// 	}

// 	var plan = this.Manage.value.plan;
// 	// this.plan = plan;
//    this.plan =this.Manage.value.plan;
//    this.package =this.Manage.value.package;
   
//    this.amount =this.Manage.value.amount;
//    this.id = 0; 
//    this.arrayItems.push({ plan: this.plan, id: this.id, package: this.package,  amount: this.amount });
//   console.log(this.arrayItems);


// }



// }
  



