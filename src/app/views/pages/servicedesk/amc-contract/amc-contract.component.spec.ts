import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmcContractComponent } from './amc-contract.component';

describe('AmcContractComponent', () => {
  let component: AmcContractComponent;
  let fixture: ComponentFixture<AmcContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmcContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmcContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
