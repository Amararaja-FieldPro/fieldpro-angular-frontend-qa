import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagercontractComponent } from './managercontract.component';

describe('ManagercontractComponent', () => {
  let component: ManagercontractComponent;
  let fixture: ComponentFixture<ManagercontractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagercontractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagercontractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
