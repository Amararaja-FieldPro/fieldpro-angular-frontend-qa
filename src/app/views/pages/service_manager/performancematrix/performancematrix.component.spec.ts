import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformancematrixComponent } from './performancematrix.component';

describe('PerformancematrixComponent', () => {
  let component: PerformancematrixComponent;
  let fixture: ComponentFixture<PerformancematrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerformancematrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformancematrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
