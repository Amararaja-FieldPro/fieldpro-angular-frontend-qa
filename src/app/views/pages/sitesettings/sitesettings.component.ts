import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
// import { ActivatedRoute, Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import * as _ from 'lodash';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: 'kt-sitesettings',
  templateUrl: './sitesettings.component.html',
  styleUrls: ['./sitesettings.component.scss']
})
export class SitesettingsComponent implements OnInit {
  cardImageBase64: string;
  imageError: string;
  isImageSaved: boolean;
  EditSite: FormGroup;
  selectedfile = true;
  filename: any; // file upload
  Filename: any; // file upload
  load: any;
  image_name: any; //variable to strore the image
  ByDefault: boolean = false; // file upload
  company_logo: any;
  submitEnable: any;
  error: any;
  overlayRef: OverlayRef;
  loading: boolean = false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  buttonClose: boolean = true;
  message: any;
  image: string;
  constructor(private chRef: ChangeDetectorRef, private modalService: NgbModal, private ajax: ajaxservice, private toastr: ToastrService, private overlay: Overlay,) {
    this.EditSite = new FormGroup({
      "company_id": new FormControl(''),
      "site_id": new FormControl(''),
      "copyright_info": new FormControl('', [Validators.required, this.noWhitespace]),
      "notification_elapse_time": new FormControl(0, [Validators.required]),
      'technician_radius': new FormControl('', [Validators.required]),
      'technician_assign_type': new FormControl('', [Validators.required]),
      'customer_prefix': new FormControl('', [Validators.required, this.noWhitespace, Validators.maxLength(6), Validators.minLength(2), Validators.pattern("^[A-Z a-z0-9%+-_]{2,6}$")]),
      'company_logo': new FormControl(''),
    });

  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  employee_code: string
  sitedata: any
  //node_id : IntegralUIEditorType
  ngOnInit() {
    this.message = "";
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getsitesetting();
  }
  get log() {
    return this.EditSite.controls;                       //error logs for create user
  }

  submit() {
  }
  fileChangeEvents(fileInput: any) {
    this.image = '';
    this.message = '';
    console.log(fileInput.target.files[0].name);
    this.ByDefault = true;
    this.Filename = fileInput.target.files[0].name;
    if (this.EditSite) {
      this.cardImageBase64 = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.EditSite.value.invalid = true;
          this.EditSite.controls['company_logo'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.EditSite.value.invalid = true;
          this.EditSite.controls['company_logo'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.EditSite.value.invalid = true;
              this.EditSite.controls['company_logo'].setErrors({ 'incorrect': true });
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.message = '';
              this.EditSite.value.invalid = false;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }

  }
  removeImage() {

    if (this.EditSite) {
      this.image = '';
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
      this.message = "please select the image";
      this.EditSite.value.invalid = true;
      this.EditSite.controls['company_logo'].setErrors({ 'incorrect': true });
    }


  }
  getsitesetting() {
    this.cardImageBase64 = null;
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.error = '';
    this.employee_code = localStorage.getItem('employee_code');
    // var employee_code = this.employee_code; // country: number                                    
    var url = 'get_site_settings/';             // api url for getting the details with using post params
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sitedata = result.response.data;
        this.sitedata.forEach(element => {
          this.company_logo = element.company_logo;
          this.EditSite.controls['company_id'].setValue(element.company_id);
          this.EditSite.controls['site_id'].setValue(element.site_id);
          this.EditSite.controls['copyright_info'].setValue(element.copyright_info);
          this.EditSite.controls['notification_elapse_time'].setValue(element.notification_elapse_time);
          this.EditSite.controls['technician_radius'].setValue(element.technician_radius);
          this.EditSite.controls['technician_assign_type'].setValue(element.technician_assign_type);
          this.EditSite.controls['customer_prefix'].setValue(element.customer_prefix);
          this.EditSite.controls['company_logo'].setValue('');
          this.image = environment.image_static_ip + element.company_logo;
          console.log(this.image);
          this.isImageSaved = true;
          this.ByDefault = true;
          this.Filename = "image";
          console.log(element.company_logo);
          this.overlayRef.detach();
        })
      }
      else if (result.response.response_code == "500") {
        this.error = result.response.message;
      }
      else {
        this.error = "Something went wrong"
      }
    }
    )
  }

  //Function to call the edit api 
  editsite() {

    var data = this.EditSite.value;
    console.log(data.company_logo);

    if (this.image == '' || this.image == undefined || data.company_logo == '') {

      data["company_logo"] = "";
    }
    else {
      data["company_logo"] = this.cardImageBase64;

    }
    if (data.site_id == '' || data.copyright_info == '' || data.notification_elapse_time == '' || data.technician_radius == '' || data.technician_assign_type == '') {
      this.submitEnable = 'False';
    }
    else {
      this.submitEnable = 'True';
    }
    var url = 'edit_site_settings/';   //api url of edit api 
    this.loading = true;
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                        //if sucess
        {
          this.loading = false;
          this.chRef.detectChanges();
          this.getsitesetting();                                                //reloading the component
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (result.response.response_code == "400") {                //if failure
          this.error = result.response.message
          this.loading = false;
          this.chRef.detectChanges();
        }
        else if (result.response.response_code == "500") {                //if failure
          this.error = result.response.message
          this.loading = false;
          this.chRef.detectChanges();
        }
        else {
          this.error = result.response.message
          this.loading = false;   
          this.chRef.detectChanges();                                                      //if not sucess
        }
      }, (err) => {
        this.loading = false;    
        this.chRef.detectChanges();                                                //if error
        console.log(err);                                                 //prints if it encounters an error
      });
  }


  //--------------------------------------------------------------------------------------------------// 

  //saranya22-dec-2020 for alphabet block in keyboard
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
