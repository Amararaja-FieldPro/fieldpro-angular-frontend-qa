import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'datatables.net';
import 'datatables.net-bs4';


@Component({
  selector: 'kt-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  country: any;
  states: any;
  cities: any;
  location: any;
  LocationAdd: FormGroup;
  LocationEdit: FormGroup;
  id_based_location: any;
  showTable: boolean = false;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  nrSelect = "daadddd";
  submitted = false;
  loadingAdd: boolean = false;
  loadingEdit: boolean = false;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  error: any;
  locate_hill: boolean = false;
  bulk: any;
  loadingBulk: boolean = false;
  filename: any;
  file_name: any;
  type: any;
  Filename: any;
  errors: any;
  ByDefault: boolean = false;
  settingsObj: any = {
    //"dom": 'Blfrtip',

    // buttons: [{
    //   extend: 'excel', text: 'Export',
    //   exportOptions: {
    //     columns: [2, 3, 4,5]
    //   },
    //   title: '',
    // }
    // ]
    "order": [[1, "asc"]],

    deferRender: true,
    scrollY: 200,
    scrollCollapse: true,
    scroller: true

  }
  checkall: any;
  return_draw: number;
  table: any;
  bulks: any;
  // dtOptions: DataTables.Settings = {};
  constructor(public ajax: ajaxservice, private modalService: NgbModal,
    private router: Router, private toastr: ToastrService, public activeModal: NgbActiveModal, private overlay: Overlay, public _location: Location, private chRef: ChangeDetectorRef) {
    this.LocationAdd = new FormGroup({

      "country_id": new FormControl(101),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location": new FormControl('', [Validators.required, this.noWhitespace]),
      "hill_area": new FormControl('')
    });
    this.table = $('#datatables').DataTable();

  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  areabased(event) {
    if (this.LocationAdd) {
      if (event.target.checked) {
        this.LocationAdd.controls['hill_area'].setValue(1);

      }
      else {
        this.LocationAdd.controls['hill_area'].setValue(0);

      }
    }
    if (this.LocationEdit) {
      if (event.target.checked) {
        this.LocationEdit.controls['hill_area'].setValue(1);

      }
      else {
        this.LocationEdit.controls['hill_area'].setValue(0);

      }
    }
  }
  // convenience getter for easy access to form fields
  get log() {
    return this.LocationAdd.controls;                       //error logs for create user
  }
  // convenience getter for easy access to form fields
  get log1() {
    return this.LocationEdit.controls;                       //error logs for create user
  }
  testString: string = 'This is test';

  ngOnInit() {

    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    // this.getcountry(); 
    // this.LocationAdd.controls['country_id'].setValue(101);
    // this.LocationAdd.patchValue({
    //   'country_id':101
    // })
    // this.LocationAdd.value;
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   responsive: true,
    //   serverSide: true,
    //   processing: true,
    //   ajax: (dataTablesParameters: any, callback) => {
    //     var method = "post";
    //         // var limit = data.length
    //         // var page_no = data.draw
    //         var url_forming = "get_all_locations/?limit="+10+"&page_no="+1
    //         console.log(url_forming,"url_forming")
    //         var url = url_forming;
    //         this.ajax.getdata(url).subscribe(resp => {
    //         this.location = resp.data;

    //         callback({
    //           recordsTotal: resp.recordsTotal,
    //           recordsFiltered: resp.recordsFiltered,
    //           data: []
    //         });
    //       });
    //   },
    //          columns: [   
    //         { "name": "Country"},
    //         { "name": "State" },
    //         { "name": "District" },
    //         { "name": "Town"},
    //         { "name": "Hill Area"},

    //       ],
    //   // columns: [{ data: 'id' }, { data: 'firstName', class: 'none' }, { data: 'lastName' }]
    // };
    this.onChangeCountry();
    this.get_all_locations()

  }

  onReset() {
    this.LocationAdd.reset();  // data clear by Sowndarya
  }
  open(modal) {
    this.LocationAdd.reset();
    this.error = '';
    this.modalService.open(modal, {
      size: "lg",
      windowClass: "center-modalsm",     //modal popup resize
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });


  }
  openLarge(modal) {
    this.errors = '';
    this.Filename = '';
    this.bulk = '';
    this.bulks = '';
    // this.file_name='';
    this.ByDefault = false;
    this.error = '';
    this.modalService.open(modal, {
      size: "lg",
      windowClass: "center-modalsm",     //modal popup resize
      backdrop: 'static',
      keyboard: false,
    });

  }
  //after submit the file for product
  bulkfilesubmit(bulkupload) {
    this.bulk = '';
    this.bulks = '';
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined || this.file_name == '') {
      // this.toastr.error("Please select file", 'Error');
      this.error = "Please select file";
      this.bulk = '';
      this.bulks = '';
      this.loadingBulk = false;
    }
    else if (this.file_name.length > 0) {                   //if file is not empty
      if (validExts[0] != this.type) {
        this.bulk = '';
        this.bulks = '';
        this.loadingBulk = false;
        // this.toastr.error("Please select correct extension", 'Error');
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) {
        this.loadingBulk = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        var gh = file;
        var method = "post";                                    //post method 
        var url = 'location_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
            this.loadingBulk = false;
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {                        //sucess response            
              for (i = 0; i < data['success'].length; i++) {
                // this.bulk = '';
                // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                var a = [];
                data['success'].forEach(value => {
                  a.push(value.success_message
                  );
                });
                this.bulks = a;
              // for (i = 0; i < data['success'].length; i++) {
              //   console.log(data['success'][i].success_message)
              //   this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.loadingBulk = false;
                this.showTable = false;
                this.chRef.detectChanges();
                this.get_all_locations();                               //to close the modal box
              // }
            }
          }
            if (data['response'].fail) {
            if (data['response'].fail.length > 0) {                                    //failure response 
              //if employee code or employee id is error to put this
              // console.log(data['response'].fail)
              // for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
              //   var a = [];
              //   data['response'].fail.forEach(value => {
              //     a.push(value.error_message);
              //   });

              //   this.bulk = a;
              var a = [];
              data['response'].fail.forEach(value => {
                a.push(value.error_message
                );
              });
              this.bulk = a;
              }
            }
            // this.error = '';

            // if (data['response'].fail.length > 0) {                                    //failure response 
            //   //if employee code or employee id is error to put this
            //   for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
            //     var a = [];
            //     data['response'].fail.forEach(value => {
            //       a.push(value.error_message
            //       );
            //     });

            //     this.bulk = a;
            //     // this.file_name = '';
            //     // this.ByDefault = false;
            //   }
            // }
            // //success response
            // //if newly add product for excisting employee code or employee id
            // if (data['success'].length > 0) {                         //sucess response
            //   for (i = 0; i < data['success'].length; i++) {
            //     // alert(data['success'][i])
            //     // this.bulk = '';
            //     this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
            //     this.showTable = false;
            //     this.chRef.detectChanges();
            //     this.get_all_locations();
            //     // this.file_name = '';
            //     // this.ByDefault = false;

            //   }
            // }
            // this.loadingBulk = false;
            this.loadingBulk = false;
          }

          //if other errors
          else if (data['response'].response_code == "500") {
            this.bulks=''
            this.bulk = [data['response'].message];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            // this.error = data['response'].message
          }
          else {
            this.bulks=''
            this.bulk = ['Something Went Wrong'];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            // this.error = "Something Went Wrong";                                                                  //if not sucess
          }
        },

          (err) => {
            this.bulks=''
            this.bulk = ["Something Went Wrong"];
            this.loadingBulk = false;
            // this.error = "Something Went Wrong";
            // this.file_name = '';
            this.ByDefault = false;                                                                 //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }

  };
  //get country dropdown
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onChangeCountry() {
    var country = +101; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    console.log(url, id, country);
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;
        this.LocationAdd.controls['state_id'].setValue(null);                                             //storing the result from the response
        this.LocationAdd.controls['city_id'].setValue(null);
        this.LocationAdd.value.valid = false;
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onchangecountryEdit(country_id) {
    var country = +country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    console.log(url, id, country);
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;
        this.LocationEdit.controls['state_id'].setValue(null);                                             //storing the result from the response
        this.LocationEdit.controls['city_id'].setValue(null);

        this.LocationEdit.value.valid = false;
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //select state change event
  //get city dropdown 
  onChangeState(state_id) {
    console.log(state_id);
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';
    console.log(url, id, state);                               // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                   
        this.LocationAdd.controls['city_id'].setValue(null);
        this.LocationAdd.value.valid = false;
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });




  }
  onChangeStateEdit(state_id) {
    console.log(state_id);
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';
    console.log(url, id, state);                               // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                   
        this.LocationEdit.controls['city_id'].setValue(null);
        this.LocationEdit.value.valid = false;
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  nextButtonClickEvent(): void {
    //do next particular records like  101 - 200 rows.
    //we are calling to api

    console.log('next clicked')
  }
  //get country dropdown
  get_all_locations1() {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var url = 'get_all_locations/?limit=1000&page_no=1';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.location = result.response.data;              //storing the api response in the array             
        this.showTable = true;

        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code == "400") {
        this.error = result.response.message;
        this.location = result.response.data;              //storing the api response in the array             
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code == "500") {
        this.overlayRef.detach();
        this.error = result.response.message;

      }
      else {
        this.error = "Something went wrong";       //warning message for error
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  submitloc(data) {
    this.submitted = true;
    this.loadingAdd = true;
    if (this.LocationAdd.invalid) {
      console.log("invalid");

    }
    else {
      console.log(this.LocationAdd.value)
      if (this.LocationAdd.value.hill_area == null) {
        this.LocationAdd.controls['hill_area'].setValue(0);
      }
      else {
        this.LocationAdd.controls['hill_area'].setValue(this.LocationAdd.value.hill_area);
      }
      this.LocationAdd.controls['country_id'].setValue(101);
      var data = this.LocationAdd.value;
      var url = 'add_location/'                                         //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {


          this.modalService.dismissAll();
          this.LocationAdd.reset();
          this.showTable = false;
          this.chRef.detectChanges();
          this.get_all_locations();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingAdd = false;
        }
        else if (result.response.response_code == "400") {
          this.error = result.response.message;
          this.loadingAdd = false;
        }
        else if (result.response.response_code == "500") {
          this.error = result.response.message;
          this.loadingAdd = false;
        }
        else {
          this.error = "Something went wrong";       //warning message for error
          this.loadingAdd = false;
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  //function for bulk upload in location
  fileUpload(event) {
    // this.bulk = '';
    // this.errors = '';
    //read the file
    // this.error = "";
    this.type = '';
    this.Filename = '';
    this.file_name = '';
    this.bulk = '';
    this.bulks = '';
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    this.ByDefault = true;
  };
  //function for add user
  locaion_add(data) {

  }
  //get country dropdown
  get_location_data(locatn_id, edit_location) {
    this.error = '';
    console.log(locatn_id)
    var location_id = +locatn_id; // country: number                                    
    var url = 'get_location_data/?';             // api url for getting the details with using post params
    var id = "location_id";
    console.log(url, id, location_id);
    this.ajax.getdataparam(url, id, location_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.id_based_location = result.response.data;

        this.onChangeCountry();
        this.onChangeState(this.id_based_location.state.state_id)
        this.LocationEdit = new FormGroup({
          "location_id": new FormControl(this.id_based_location.location_id),
          "country_id": new FormControl(this.id_based_location.country.country_name),
          "state_id": new FormControl(this.id_based_location.state.state_id, [Validators.required]),
          "city_id": new FormControl(this.id_based_location.city.city_id, [Validators.required]),
          "location": new FormControl(this.id_based_location.location_name, [Validators.required, this.noWhitespace]),
          "hill_area": new FormControl(this.id_based_location.hill_area)
        });
        this.modalService.open(edit_location, {
          size: "lg",
          windowClass: "center-modalsm",     //modal popup resize
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
        this.LocationEdit.value.valid = true;
        // if (this.id_based_location.hill_area == 1) {
        //   this.locate_hill = true;
        // }
        // else {
        //   this.locate_hill = false;
        // }


        console.log(this.LocationEdit.valid)
      }
      else if (result.response.response_code == "400") {
        this.showTable = false;
        this.error = result.response.message;
      }
      else if (result.response.response_code == "500") {
        this.showTable = false;
        this.error = result.response.message;
      }
      else {
        this.error = "something Went wrong";
      }
    });
  }
  locaion_edit() {
    this.submitted = true;
    this.loadingEdit = true;
    if (this.LocationEdit.invalid) {
      console.log("invalid");
    }
    else {
      this.LocationEdit.controls['country_id'].setValue(101);
      var data = this.LocationEdit.value;
      var url = 'edit_location/'                                         //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {

          this.showTable = false;
          this.chRef.detectChanges();
          this.get_all_locations();
          this.modalService.dismissAll();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingEdit = false;
        }
        else if (result.response.response_code == "400") {
          this.error = result.response.message;
          this.loadingEdit = false;
        }
        else if (result.response.response_code == "500") {
          this.error = result.response.message;
          this.loadingEdit = false;
        }
        else {
          this.error = "something Went wrong";
          this.loadingEdit = false;
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  trashlocation(data) {
    this.error = '';
    this.id_based_location = data;                                   //get datas from edit API
    var locationid = this.id_based_location.location_id;
    var location = +locationid;
    var url = 'delete_location/?';             // api url for getting the details with using post params
    var id = "location_id";                            //id for specific the servicegroup name
    var location_id = { "location_id": this.id_based_location.location_id };
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.showTable = false;
        this.ajax.postdata(url, location_id).subscribe((result) => {
          if (result.response.response_code == "200") {                 //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.get_all_locations();
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
          }
          else if (result.response.response_code == "400") {
            this.error = result.response.message;
          }
          else if (result.response.response_code == "500") {
            this.error = result.response.message;
          }
          else {
            this.error = "something Went wrong";
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
   //get country dropdown
   get_all_locations() {
    this.overlayRef.attach(this.LoaderComponentPortal);

    // var url = 'get_all_locations/?length=10&start=1';
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    var search_value = ""
    var url = "get_all_locations/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.location = result.response.data;              //storing the api response in the array             
        this.showTable = true;
        this.overlayRef.detach();
        this.settingsObj = {
          
          "deferLoading": result.response.recordsTotal,
      
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
       
          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            var country_name = data.columns['Country']
            var state_name = data.columns['State']
            var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']         
            var url_forming = "get_all_locations/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url = url_forming;
            this.ajax.getdata(url).subscribe(result => {
              this.location = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw          
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();
            
            });

          },
          "columnDefs": [           
            { "name": "Country", "targets": 1, "searchable": true },
            { "name": "State", "targets": 2, "searchable": true },
            { "name": "City", "targets": 3, "searchable": true },
            { "name": "Town", "targets": 4, "searchable": true },

          ],
        }

        $('#datatables').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#datatables').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.error = result.response.message;
        this.location = result.response.data;              //storing the api response in the array             
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code == "500") {
        this.overlayRef.detach();
        this.error = result.response.message;

      }
      else {
        this.error = "Something went wrong";       //warning message for error
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // get_all_locations() {
  //   this.checkall = false;
  //   this.return_draw = 0;
  //   this.overlayRef.attach(this.LoaderComponentPortal);
  //   var data = ""
  //   // var url = 'get_all_locations/?length=10&start=1';
  //   var length = 10
  //   var start = 0
  //   var draw = this.return_draw
  //   var search_value = ""
  //   var url = "get_all_locations/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value

  //   // var url = 'get_all_locations/';                                  // api url for getting the details

  //   this.ajax.getdata(url).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       this.location = result.response.data;              //storing the api response in the array             

  //       this.overlayRef.detach();
  //       // this.return_draw = result.response.draw

  //       this.settingsObj = {
  //         deferRender: true,
  //         scrollCollapse: true,
  //         scroller: true,
  //         responsive: true,
  //         "deferLoading": result.response.recordsTotal,
  //         serverSide: true,
  //         processing: true,
  //         'serverMethod': 'post',
  //         ajax: (dataTablesParameters: any, callback) => {

  //           var data = dataTablesParameters;

  //           var method = "post";
  //           var limit = data.limit
  //           var draw = data.draw
  //           var leng = data.length

  //           var length = (data.start) + (data.length)

  //           var start = data.start

  //           var search_value = data.search.value
  //           var country_name = data.columns['Country']
  //           var state_name = data.columns['State']
  //           var city_name = data.columns['City']
  //           var sort = data.order[0]['dir']
  //           var column = data.order[0]['column']
  //           var url_forming = "get_all_locations/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column

  //           var url = url_forming;
  //           this.ajax.getdata(url).subscribe(result => {
  //             this.location = result.response.data;              //storing the api response in the array             

  //             this.return_draw = result.response.draw

  //             callback({
  //               recordsTotal: result.response.recordsTotal,
  //               recordsFiltered: result.response.totalRecordswithFilter,
  //               data: [],

  //             });
  //             this.chRef.detectChanges();
  //           });

  //         },
  //         "columnDefs": [
  //           { "orderable": false, "targets": 0 },
  //           { "orderable": false, "targets": 1 },
  //           { "orderable": false, "targets": 6 },
  //           { "name": "Country", "targets": 2, "searchable": true },
  //           { "name": "State", "targets": 3, "searchable": true },
  //           { "name": "District", "targets": 4, "searchable": true },
  //           { "name": "Town", "targets": 5 },
  //           { "name": "Hill Area", "targets": 6 },

  //         ],
  //       }

  //       $('#datatables').DataTable().destroy();
  //       setTimeout(() => {
  //         this.table = $('#datatables').DataTable(

  //           this.settingsObj
  //         );
  //       }, 5);
  //     }

  //     // }
  //     else if (result.response.response_code == "400") {
  //       this.error = result.response.message;
  //       this.location = result.response.data;              //storing the api response in the array             
  //       this.showTable = true;
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();


  //     }
  //     else if (result.response.response_code == "500") {
  //       this.overlayRef.detach();
  //       this.error = result.response.message;

  //     }
  //     else {
  //       this.error = "Something went wrong";       //warning message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  //   //PAGE 

  // }

  // get_all_locations4() {
  //   console.log("i am calling")
  //   this.checkall = false;
  //   this.return_draw = 0;
  //   this.overlayRef.attach(this.LoaderComponentPortal);
  //   var data = ""
  //   var url = 'get_all_locations/?length=10&start=1';
  //   var length = 10
  //   var start = 1
  //   var draw = this.return_draw
  //   var url = "get_all_locations/"
  //   // var url = 'get_all_locations/';                                  // api url for getting the details

  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       this.location = result.response.data;              //storing the api response in the array             
  //       // this.showTable = true;
  //       console.log(location, "location")
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();
  //       this.return_draw = result.response.draw
  //       // $('#datatables').DataTable().destroy();
  //       // setTimeout(() => {
  //       //   this.table = $('#datatables').DataTable(

  //       //     this.settingsObj
  //       //   );
  //       // }, 5);
  //       console.log(result, "result")
  //       console.log(result.response)
  //       console.log(result.response.page_count)
  //       console.log(this.location, "this.location")
  //       this.table = $('#datatables').DataTable(
  //         this.settingsObj = {
  //           // paging: false,
  //           // pageLength : 4,
  //           // data: this.location,
  //           deferRender: true,
  //           // scrollY: 200,
  //           scrollCollapse: true,
  //           scroller: true,

  //           aData: this.location,
  //           // pagingType: 'full_numbers',
  //           responsive: true,
  //           "deferLoading": result.response.recordsTotal,
  //           serverSide: true,
  //           // processing: true,
  //           // 'processing': true,
  //           // 'serverSide': true,
  //           'serverMethod': 'post',

  //           ajax: (dataTablesParameters: any, callback) => {
  //             console.log("akjadasdh")
  //             var data = dataTablesParameters;
  //             console.log(data, "data")
  //             var method = "post";
  //             var limit = data.limit
  //             var draw = data.draw
  //             var length = (data.length) * (data.draw)
  //             var start = data.start
  //             // this.showTable = false;
  //             var url_forming = "get_all_locations/"
  //             console.log(url_forming, "url_forming")
  //             var url = url_forming;
  //             this.ajax.postdata(url, data).subscribe(result => {
  //               console.log(result)
  //               // resp.result.forEach(function (e) { e.select = false });   

  //               // this.location = result.response.data;
  //               // // this.rerender()
  //               // // this.selectalldata=resp.filterData;
  //               // this.chRef.detectChanges();
  //               // this.overlayRef.detach();
  //               // this.return_draw = result.response.draw;
  //               // // this.showTable = true;

  //               // // this.chRef.detectChanges();
  //               // // this.dtTrigger.next()
  //               // // this.overlayRef.detach();
  //               this.location = result.response.data;              //storing the api response in the array             
  //               console.log(location, "location")
  //               // this.rerender()
  //               // this.chRef.detectChanges();
  //               // this.overlayRef.detach();
  //               this.return_draw = result.response.draw
  //               callback({
  //                 recordsTotal: result.response.recordsTotal,
  //                 recordsFiltered: result.response.totalRecordswithFilter,
  //                 data: [],

  //               });

  //             });

  //           },
  //           "columnDefs": [
  //             { "orderable": false, "targets": 0 },
  //             { "orderable": false, "targets": 1 },
  //             { "orderable": false, "targets": 6 },
  //             { "name": "Country", "targets": 2, "searchable": true },
  //             { "name": "State", "targets": 3, "searchable": true },
  //             { "name": "District", "targets": 4, "searchable": true },
  //             { "name": "Town", "targets": 5 },
  //             { "name": "Hill Area", "targets": 6 },

  //           ],
  //         }

  //         // $('#datatables').DataTable().destroy();
  //         // setTimeout(() => {
  //         // this.table = $('#datatables').DataTable(

  //         // this.settingsObj
  //       );
  //       // }, 5);
  //       console.log(this.settingsObj, "this.settingsObj")

  //       //   }
  //       // )
  //     }

  //     // }
  //     else if (result.response.response_code == "400") {
  //       this.error = result.response.message;
  //       this.location = result.response.data;              //storing the api response in the array             
  //       this.showTable = true;
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();


  //     }
  //     else if (result.response.response_code == "500") {
  //       this.overlayRef.detach();
  //       this.error = result.response.message;

  //     }
  //     else {
  //       this.error = "Something went wrong";       //warning message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  //   //PAGE 

  // }

}
