import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';// To use toastr

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
declare var require: any
const FileSaver = require('file-saver');
// import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'kt-ongoing-tickets',
  templateUrl: './ongoing-tickets.component.html',
  styleUrls: ['./ongoing-tickets.component.scss']
})
export class OngoingTicketsComponent implements OnInit {
  ProductLocation: FormGroup;
  currentJustify = 'end';
  spare: any;
  employee_code: any;
  product_id: any;
  location_id: any;
  spare_details: any;
  esclated_tickets: any;
  working_in_progress_tckts: [];
  spare_requested_ticket: any;
  ticket_details: any;
  subproduct: any;
  products: any;
  locations: any;
  tab_type: any;
  technicians_details: any
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  CustomerfeedbackForm: FormGroup;
  showTable: boolean = false;
  ticket_id: any;
  avilable_technicians_details: any;
  spare_request_details: any;
  reassigned_technician: any;
  showTableEscalated: boolean = false;
  showTableSpare: boolean = false;
  SpareTable: boolean = false;
  dataTable: any;
  tabvalue: any;
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtOptions1: DataTables.Settings = {};
  dtOptions2: DataTables.Settings = {};
  dtOptions3: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  loadingSub: boolean = false;
  @ViewChild('dataTable', { static: true }) table;
  spare_requested_tickets: any;
  pdf: any;
  cust_feedbacks = [];                   //Customer feedback array        
  cus_ratings = [];                      //Customer rating array
  servicedesk_id: string;
  constructor(private ajax: ajaxservice, private chRef: ChangeDetectorRef, private overlay: Overlay, public modalService: NgbModal, private toastr: ToastrService) {
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required)
    });
    this.CustomerfeedbackForm = new FormGroup({
      "ticket_id": new FormControl(''),
      "customer_code": new FormControl(''),
      "customer_name": new FormControl(''),
      "contact_number": new FormControl(''),
      "email_id": new FormControl(''),
      'cust_comments': new FormControl('', Validators.required),

    })
  }



  ngOnInit() {
    this.servicedesk_id = localStorage.getItem('employee_code')
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
      // processing: true
    }

    this.tabvalue = 'tab-selectbyid1';
    this.employee_code = localStorage.getItem('employee_code');
    console.log(this.employee_code);
    this.get_ticket_product_location();
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    // this.overlayRef.attach(this.LoaderComponentPortal);

    this.product_id = null;
    this.location_id = null;
    this.cust_feedbacks = [
      { id: "P1", value: "Excellent" },
      { id: "P2", value: "Satisfied" },
      { id: "P3", value: "Very Good" },
      { id: "P4", value: "Average" },
      { id: "p5", value: "Bad" },
    ];
    //customer rating array
    this.cus_ratings = [
      { id: "1", value: "1" },
      { id: "2", value: "2" },
      { id: "3", value: "3" },
      { id: "4", value: "4" },
      { id: "5", value: "5" },
      { id: "6", value: "6" },
      { id: "7", value: "7" },
      { id: "8", value: "8" },
      { id: "9", value: "9" },
      { id: "10", value: "10" },
    ]

    this.get_working_in_progress();
  }
  downloadPdf(ticket_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var id = "ticket_id";
    var url = "setr_pdf/?";
    var value = ticket_id;
    this.ajax.getPdfDocument(url, id,value).subscribe((response) => {
      console.log(response, "response")
      // let file = new Blob([response], { type: 'application/pdf' });
      let file = response.url
      console.log(response.url, "fileURL")
      // var fileURL = URL.createObjectURL(file);
      // window.open(file);  

      FileSaver.saveAs(file, value);
      this.overlayRef.detach();

    }, (err) => {
      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });  
  
  }

 

  get log() {
    return this.ProductLocation.controls;                       //error logs for productlocation 
  }
  get feed() {
    return this.CustomerfeedbackForm.controls;                       //error logs for create user
  }
  customer_feedback_modal(modal, item) {
    //set the value to the customerfeedback formgroup
    this.CustomerfeedbackForm.patchValue({
      "ticket_id": item.ticket_id,
      "customer_code": item.customer_code,
      "customer_name": item.customer_name,
      "contact_number": item.contact_number,
      "email_id": item.email_id,
    })
    //open modal box
    this.modalService.open(modal, {
      size: 'lg',
      windowClass: "center-modalsm",
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  //Customer feedback 
  customer_feedback() {
    this.loadingSub = true;
    var data = this.CustomerfeedbackForm.value;
    data['flag'] = 1;
    console.log(data)
    var url = 'customer_feedback/' //api url of edit api
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") //if sucess
        {
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          // this.overlayRef.attach(this.LoaderComponentPortal);
          this.loadingSub = false;
          this.get_working_in_progress();
          this.showTable = false;
          this.chRef.detectChanges();
          this.modalService.dismissAll(); //to close the modal box
        }
        else if (result.response.response_code == "500") { //if failiure
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingSub = false;
        }
        else { //if not sucess
          this.toastr.error("Something went wrong", 'Error'); //toastr message for error
          this.loadingSub = false;
        }
      }, (err) => {
        //prints if it encounters an error
      });
  }
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onChangeLocation(tab_type) {
    console.log(tab_type)
    this.location_id = this.ProductLocation.value.location_id;
    this.product_id = this.ProductLocation.value.product_id;
    console.log(this.ProductLocation.value.location_id);
    if (this.location_id == "") {
      this.location_id = null;
    }
    else {
      this.location_id = this.ProductLocation.value.location_id;
    }
    if (this.product_id == "") {
      this.product_id = null;
    }
    else {
      this.product_id = this.ProductLocation.value.product_id;
    }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTable = false;
      this.get_working_in_progress();
    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableEscalated = false;
      this.get_esclated_tickets();
    }

    if (this.tabvalue == 'tab-selectbyid3') {
      this.showTableSpare = false;
      this.get_spare_requested_ticket();
    }


  }


  //get sub product details based on product id
  onChangeProduct(product_id) {
    this.subproduct = null;

    this.product_id = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var product = "product_id";
    console.log(url, product, this.product_id);
    this.ajax.getdataparam(url, product, this.product_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;                                                //reloading the component
        console.log(this.subproduct)
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onchangeforsearch() {
    console.log(this.ProductLocation.value)
  }
  openLarge(modal) {
    // this.ticket_id=ticket_id;
    this.modalService.open(modal, {
      size: 'lg',
      windowClass: "center-modalsm"     //modal popup resize
    });
  }
  openSpare(modal) {
    // this.ticket_id=ticket_id;
    this.modalService.open(modal, {
      size: 'lg',
    });
  }

  openTech(openTech, id: any) {
    this.ticket_id = id;
    this.modalService.open(openTech, {
      size: 'lg'
    });
  }

  // function for getting the work in progress ticket based on the product and location  and employee code
  get_working_in_progress() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.employee_code);
    console.log(this.product_id, "product_id")
    console.log(this.location_id, "location_id")
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    console.log(data);
    var url = 'in_progress_ticket_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.working_in_progress_tckts = result.response.data;              //storing the api response in the array

        result.response.data.forEach(el => {
          var date= el.schedule_next.split(",",2);
          el.schedule_next=date[0];
          console.log(el);
          // this.working_in_progress_tckts.push(el);
         })

        console.log(this.working_in_progress_tckts)
        this.showTable = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
        this.dtTrigger.next();


        
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.overlayRef.detach();
      }

      else {
        this.toastr.error(result.response.message, 'Error');
      }


    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the escalated ticket based on the product and location  and employee code
  get_esclated_tickets() {

    this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.employee_code);
    console.log(this.product_id, "product_id")
    console.log(this.location_id, "location_id")
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    console.log(data);
    var url = 'esclated_ticket_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.esclated_tickets = result.response.data;              //storing the api response in the array

        this.showTableEscalated = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
        this.dtTrigger.next();
      
      }
      else if (result.response.response_code == "500") {
        this.showTableEscalated = true;
        this.overlayRef.detach();
      }

      else {
        this.toastr.error(result.response.message, 'Error');
      }


    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  // function for getting the get requested tiket based on employee code
  get_spare_requested_ticket() {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var url = 'spare_request_ticket_details/';             // api url for getting the details with using post params
    var employee_code = "employee_code";
    var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code }
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.spare_requested_ticket = result.response.data;                                                //reloading the component
        console.log(this.spare_requested_ticket)
        this.showTableSpare = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
        this.dtTrigger.next();
       
      }
    
      else if (result.response.response_code == "500") {
        this.showTableSpare = true;
        this.overlayRef.detach();
      }

      else {
        this.toastr.error(result.response.message, 'Error');
      }

    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the get requested tiket based on employee code
  get_spare_details(ticket_id, view_requested_spare) {
    console.log(ticket_id)


    var url = 'spare_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    console.log(url, id, ticket_id);
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.spare_requested_tickets = result.response.data;                                                //reloading the component
        console.log(this.spare_requested_tickets)
        this.SpareTable = true;
        this.modalService.open(view_requested_spare, {
          size: 'lg',
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click

        });
      }
      else if (result.response.response_code == "500") {
        this.SpareTable = true;
        this.overlayRef.detach();
      }

      else {
        this.toastr.error(result.response.message, 'Error');
      }

    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  // function for getting the ticket details based on TicketId 
  get_ticket_details(tickt_id, view_tcktdetails) {
    console.log(tickt_id)
    var ticket_id = tickt_id; // country: number                                    
    var url = 'get_ticket_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    console.log(url, id, ticket_id);
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details = result.response.data;                                           //reloading the component
        this.modalService.open(view_tcktdetails, {
          size: 'lg',
          // windowClass: "center-modalsm",     //modal popup resize
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click

        });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the Technician details based on TechnicianId 
  get_technician_details(ticket_id, technician_id, view_techniciandetails) {

    var ticket_id = ticket_id
    var technician_id = technician_id
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;
        console.log(this.technicians_details);
        this.modalService.open(view_techniciandetails, {
          size: 'lg',
          windowClass: "center-modalsm",     //modal popup resize
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click

        });
      }
      else {
        console.log(result.response.console.error, "error");

      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }


  // function for getting the Available technician details based on TicketId 
  get_avilable_technicians(tickt_id) {
    var ticket_id = tickt_id; // country: number                                    
    var url = 'avilable_technicians/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    console.log(url, id, ticket_id);
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.avilable_technicians_details = result.response.data;
        console.log(this.avilable_technicians_details)
        // this.task_counts=this.avilable_technicians_details[0].task_count;
        // this.ViewTechnicianDetails.controls['task_count'].setValue(this.avilable_technicians_details[0].task_count);
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }

  //function  for reassign the tickets
  re_assign_technician(ticket_id) {
    // alert("function call")
    // console.log(this.ticket_id)
    var ticket_id = ticket_id; //ticket id                                   
    var url = 're_assign_technician/?';             // api url for getting the details with using post params
    // var id = "ticket_id";
    // console.log(url, id, ticket_id);
    var data = { "ticket_id": this.ticket_id, "employee_code": this.employee_code }
    this.ajax.postdata(url, data)
      .subscribe((result) => {
      if (result.response.response_code == "200") {
        this.reassigned_technician = result.response.data;
        console.log(this.reassigned_technician)                                               //reloading the component
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  technician_assign(id_details) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to assign",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Assign!'
    }).then((result) => {
      if (result.value) {
        console.log(id_details)
        console.log(this.ticket_id)
        var data = { "servicedesk_id": this.servicedesk_id,"technician_id": id_details.technician_id, "ticket_id": this.ticket_id, "secondary_tec_id": null }
        var url = 'technician_assign/'                                           //api url of Assign Ticket                                    
        this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200")                           //if sucess
            {
              this.ngOnInit();                                                //reloading the component
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
              this.modalService.dismissAll();                                 //to close the modal box
            }
            else if (result.response.response_code == "400") {                  //if failure
              this.toastr.error(result.response.message, 'Error');            //toastr message for error
            }
            else {                                                                     //if not sucess
              this.toastr.error(result.response.message, 'Error');        //toastr message for error
            }
          }, (err) => {                                                           //if error
            console.log(err);                                                 //prints if it encounters an error
          });
      }
    })
  }



  fetchNews($event) {
    this.tabvalue = $event.nextId;
    this.product_id = null;
    this.location_id = null;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['location_id'].setValue('');
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTable = false;
      this.get_working_in_progress();
    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableEscalated = false;
      this.get_esclated_tickets();
    }
    if (this.tabvalue == 'tab-selectbyid3') {
      this.showTableSpare = false;
      this.get_spare_requested_ticket();

    }






  }

}
