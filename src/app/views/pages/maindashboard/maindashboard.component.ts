import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import {
  // AccumulationChartComponent,
  // AccumulationChart,
  // AccumulationDataLabel,
  IAccLoadedEventArgs,
  AccumulationTheme
} from "@syncfusion/ej2-angular-charts";
import { Browser } from "@syncfusion/ej2-base";
import {
  ILoadedEventArgs,
  ChartTheme,
  MarkerSettingsModel
} from "@syncfusion/ej2-angular-charts";
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr

@Component({
  selector: 'kt-maindashboard',
  templateUrl: './maindashboard.component.html',
  styleUrls: ['./maindashboard.component.scss']
})
export class MaindashboardComponent implements OnInit {
  btn:any;
  public data3: Object[] = [{ x: "January", y: 0 }];
  public data4: Object[] = [
    { x: "February", y: 0 },
    { x: "March", y: 0 },
    { x: "April", y: 0 },
    { x: "May", y: 0 },
    { x: "June", y: 0 },
    { x: "July", y: 0 },
    { x: "August", y: 0 },
    { x: "September", y: 0 },
    { x: "October", y: 0 },
    { x: "November", y: 0 },
    { x: "December", y: 0 }
  ];
  public data5: Object[] = [
   
    { x: "25", y: 0 },
    { x: "27", y: 0 },
    { x: "29", y: 0 },
    { x: "31", y: 0 },
  ];
  public data8: Object[] = [
    { x: "01", y: 0 },
    { x: "03", y: 0 },
    { x: "05", y: 0 },
    { x: "07", y: 0 },
    { x: "09", y: 0 },
    { x: "11", y: 0 },
    { x: "13", y: 0 },
    { x: "15", y: 0 },
    { x: "17", y: 0 },
    { x: "19", y: 0 },
    { x: "21", y: 0 },
    { x: "23", y: 0 },
    { x: "25", y: 0 },
  
  ];
  public data6: Object[] = [
    { x: "Q1(JFM)", y: 0 },
    { x: "Q2(AMJ)", y: 0 },
    { x: "Q3(JAS)", y: 0 },
    { x: "Q4(OND)", y: 0 },
  
  ];
  public data7: Object[] = [
    { x: "Q1(JFM)", y: 0 },
 
  ];
    //Initializing Primary X Axis
    public primaryXAxis: Object = {
      majorGridLines: { width: 1 },
      minorGridLines: { width: 1 },
      majorTickLines: { width: 1 },
      minorTickLines: { width: 1 },
      interval: 1,
      lineStyle: { width: 0 },
      valueType: "Category"
    };
    public primaryXAxis1: Object = {
      majorGridLines: { width: 1 },
      minorGridLines: { width: 1 },
      majorTickLines: { width: 1 },
      minorTickLines: { width: 1 },
      interval: 1,
      lineStyle: { width: 0 },
      valueType: "Category"
    };
    public primaryXAxis2: Object = {
      majorGridLines: { width: 1 },
      minorGridLines: { width: 1 },
      majorTickLines: { width: 1 },
      minorTickLines: { width: 1 },
      interval: 1,
      lineStyle: { width: 0 },
      valueType: "Category"
    };
    //Initializing Primary Y Axis
    public primaryYAxis: Object = {
      title: "Revenue (Lakhs)",
      lineStyle: { width: 0 },
      minimum: 0,
      maximum: 10,
      interval: 2,
      majorTickLines: { width: 1 },
      majorGridLines: { width: 0 },
      minorGridLines: { width: 0 },
      minorTickLines: { width: 0 },
      labelFormat: "{value}"
    };
    
  public data: Object[] = [
    { x: "Available", y: 34, text: "34 license" },
    { x: "Registered", y: 28, text: "28 license" }
  ];
  //Initializing Legend
  public legendSettings: Object = {
    visible: true,
    position: "Top"
  };
  //Initializing DataLabel
  public dataLabel: Object = {
    visible: true,
    name: "text",
    position: "Inside",
    font: {
      fontWeight: "600",
      color: "#ffffff"
    }
  };
  public data1: Object[] = [
    { x: "Available", y: 26, text: "26 license" },
    { x: "Registered", y: 5, text: "5 license" }
  ];
  public data2: Object[] = [
    { x: "Total Calls", y: 481, text: "481 calls" }
  ];
  //Initializing Legend
  public legendSettings1: Object = {
    visible: true,
    position: "Top"
  };
  public legendSettings2: Object = {
    visible: true,
    position: "Top"
  };
  //Initializing DataLabel
  public dataLabel1: Object = {
    visible: true,
    name: "text",
    position: "Inside",
    font: {
      fontWeight: "600",
      color: "#ffffff"
    }
  };
  public dataLabel2: Object = {
    visible: true,
    name: "text",
    position: "Inside",
    font: {
      fontWeight: "600",
      color: "#ffffff"
    }
  };
  user_count: any;
  // custom code start
  public load(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split("/")[1];
    selectedTheme = selectedTheme ? selectedTheme : "Material";
    args.accumulation.theme = <AccumulationTheme>(
      (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(
        /-dark/i,
        "Dark"
      )
    );
  }
  public load1(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split("/")[1];
    selectedTheme = selectedTheme ? selectedTheme : "Material";
    args.accumulation.theme = <AccumulationTheme>(
      (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(
        /-dark/i,
        "Dark"
      )
    );
  }
  public load2(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split("/")[1];
    selectedTheme = selectedTheme ? selectedTheme : "Material";
    args.accumulation.theme = <AccumulationTheme>(
      (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(
        /-dark/i,
        "Dark"
      )
    );
  }
  // custom code end
  public startAngle: number = 0;
  public endAngle: number = 360;
  public tooltip: Object = { enable: true };
  public title: string = "Technician";
  public startAngle1: number = 0;
  public endAngle1: number = 360;
  public startAngle2: number = 0;
  public endAngle2: number = 360;
  public title1: string = "Service desks";
  public title2: string = "Total Calls";
  public tooltip1: Object = {
    enable: true
  };
  // custom code start
  public load3(args: ILoadedEventArgs): void {
    let selectedTheme: string = location.hash.split("/")[1];
    selectedTheme = selectedTheme ? selectedTheme : "Material";
    args.chart.theme = <ChartTheme>(
      (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(
        /-dark/i,
        "Dark"
      )
    );
  }
  // custom code end
  public title3: string = "Revenue Insight";
  public chartArea: Object = {
    border: {
      width: 0
    }
  };
  public width: string = Browser.isDevice ? "100%" : "90%";
  public marker: MarkerSettingsModel = { visible: true };
 
  constructor( private chRef: ChangeDetectorRef,public ajax: ajaxservice, private toastr: ToastrService,) { }

  ngOnInit() {
    this.btn=1;
    console.log(this.marker)
    this.get_dasboard_details()
  }
  drop(){
  this.data5 = [
    { x: "01", y: 0 },
    { x: "03", y: 0 },
    { x: "05", y: 0 },
    { x: "07", y: 0 },
    { x: "09", y: 0 },
    { x: "11", y: 0 },
    { x: "13", y: 0 },
    { x: "15", y: 0 },
    { x: "17", y: 0 },
    { x: "19", y: 0 },
    { x: "21", y: 0 },
    { x: "23", y: 0 },
    { x: "25", y: 0 },
    { x: "27", y: 0 },
    { x: "29", y: 0 },
    { x: "31", y: 0 },
  ];
  this.btn=1;
  this.chRef.detectChanges();
  }
  drop1(){
    this.data7=[ { x: "Q1(JFM)", y: 0 },],
    this.data6=[
      { x: "Q1(JFM)", y: 0 },
      { x: "Q2(AMJ)", y: 0 },
      { x: "Q3(JAS)", y: 0 },
      { x: "Q4(OND)", y: 0 },
    
    ];
    this.btn=2;
    this.chRef.detectChanges();
  }
  drop2(){
    this.data3= [{ x: "January", y: 0 }];
    this.data4 = [
      { x: "February", y: 0 },
      { x: "March", y: 0 },
      { x: "April", y: 0 },
      { x: "May", y: 0 },
      { x: "June", y: 0 },
      { x: "July", y: 0 },
      { x: "August", y: 0 },
      { x: "September", y: 0 },
      { x: "October", y: 0 },
      { x: "November", y: 0 },
      { x: "December", y: 0 }
    ];
    this.btn=3;
    this.chRef.detectChanges();
  }
  onChangetypes(type){
console.log(type);
console.log(this.btn)
  }
  onChangetype(type){
    console.log(type);
    if(type == 'Overall Revenue' ){
      this.data = [
        { x: "Available", y: 34, text: "34 license" },
        { x: "Registered", y: 28, text: "28 license" }
      ];
      this.data1 = [
        { x: "Available", y: 26, text: "26 license" },
        { x: "Registered", y: 5, text: "5 license" }
      ];
      this.data2 = [
        { x: "Total Calls", y: 481, text: "481 calls" }
      ];
    }
    else if(type == 'Kaspon Appliances'){
      this.data = [
        { x: "Available", y: 11, text: "11 license" },
        { x: "Registered", y: 9, text: "9 license" }
      ];
      this.data1 = [
        { x: "Available", y: 4, text: "4 license" },
        { x: "Registered", y: 0, text: "0 license" }
      ];
      this.data2 = [
        { x: "Total Calls", y: 371, text: "371 calls" }
      ];
    }
    else if (type== 'Samples'){
      this.data = [
        { x: "Available", y: 2, text: "2 license" },
        { x: "Registered", y: 1, text: "1 license" }
      ];
      this.data1 = [
        { x: "Available", y: 14, text: "14 license" },
        { x: "Registered", y: 1, text: "1 license" }
      ];
      this.data2 = [
        { x: "Total Calls", y: 0, text: "0 calls" }
      ];
    }
    else if(type=='Easy Homes'){
      this.data = [
        { x: "Available", y: 18, text: "18 license" },
        { x: "Registered", y: 1, text: "1 license" }
      ];
      this.data1 = [
        { x: "Available", y: 6, text: "6 license" },
        { x: "Registered", y: 1, text: "1 license" }
      ];
      this.data2 = [
        { x: "Total Calls", y: 3, text: "3 calls" }
      ];
    }
    else if(type == 'Kaspon techworks'){
      this.data = [
        { x: "Available", y: 3, text: "3 license" },
        { x: "Registered", y: 17, text: "17 license" }
      ];
      this.data1 = [
        { x: "Available", y: 2, text: "2 license" },
        { x: "Registered", y: 3, text: "3 license" }
      ];
      this.data2 = [
        { x: "Total Calls", y: 48, text: "48 calls" }
      ];
    }
  
      }

      
      get_dasboard_details() {
        var url = 'user_count_details/'; // api url for getting the details
        this.ajax.getdata(url).subscribe((result) => {
          if (result.response.response_code == "200" || result.response.response_code == "400") {
            this.user_count = result.response.data; //storing the api response in the array
          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error')
          }
          else {
            this.toastr.error("Something went wrong", 'Error')
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }


}
