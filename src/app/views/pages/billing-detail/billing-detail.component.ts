import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Http, RequestOptions, Headers } from '@angular/http';

const selectAnActiveTabById = {
		// beforeCodeTitle: 'Dashboard',	
		viewCode: ``,
		isCodeVisible: false
	};
@Component({
  selector: 'kt-billing-detail',
  templateUrl: './billing-detail.component.html',
  styleUrls: ['./billing-detail.component.scss'],
  
})
//export class
export class BillingDetailComponent implements OnInit {
	@Input() name;
	exampleSelectAnActiveTabById;
	Setcharges: FormGroup;
	error:any;
	file_name:any;
	type:any;
	isSubmitted = false;
	product1: any = ['Television', 'REFRIGIRATOR', 'AIR CONDITIONER', 'WASHING MACHINE', 'TELEVISION', 'MICROOVEN'];
	product2: any = ['', 'Washing Machine'];
	product3: any = ['Washing Machine', 'Microoven'];
	category1: any = ['single door', 'Double door', 'split AC', 'Front Load'];
	category2: any = ['split AC'];
	category3: any = ['Front load'];
	list_company1: any = ['Whirlpool', 'Jeeves', 'Easy home']; 
	company1: any = ['REFRIGERATORS', 'SINGLE DOOR', 0, 120, 0, 0, 0, 140, 0, 0];
	company2: any = ['Washing Machine', 'Top load234@#$@#', 0, 50, 0, 0, 0, 10, 0, 0];
	company3:any = ['Television', 'LED TV', 150, 0, 0, 0, 100, 0, 0, 0];
	selectedcompany:any="All";
	currentJustify="end";   //tabs right display
	filename:any;
  Filename:any;
  ByDefault:boolean= false;
	constructor(public ajax: ajaxservice,private toastr: ToastrService,config: NgbTabsetConfig, public fb: FormBuilder, private modalService: NgbModal, public activeModal: NgbActiveModal) {
	
	}
//reload the functions
	ngOnInit() {

		this.exampleSelectAnActiveTabById = selectAnActiveTabById;   //this for using tabset
		this.Setcharges = new FormGroup({
			"company": new FormControl("", [Validators.required]),
			"priority": new FormControl("", [Validators.required]),
			"product":new FormControl("", [Validators.required]),
			"category":new FormControl("", [Validators.required]),
			"sla_amt":new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
			"elapsed_amt":new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
			
		  });
	}
	// valid error
	get log() {	
		return this.Setcharges.controls;
	}

	public beforeChange($event: NgbTabChangeEvent) {
		if ($event.nextId === 'tab-preventchange2') {
			$event.preventDefault();
		}
	}
	// onchange for product
	changeproduct(e) {
		// console.log(e.value)
		this.product.setValue(e.target.value, {
		  onlySelf: true
		})
	  }
	  get product() {
		return this.Setcharges.get('product');
	  }
// onchange for subproduct	  
	  changecategory(e) {
		// console.log(e.value)
		this.category.setValue(e.target.value, {
			onlySelf: true
		  })
	
	  }
	  get category() {
		return this.Setcharges.get('category');
	  }
	  // onchange for company
	  changelist_company(e) {
		   
		// console.log(e)
		this.selectedcompany = e.target.value;
   
	
	  }
	  fileUpload(event) {
        //read the file
        this.error = "";
        let fileList: FileList = event.target.files;
        this.file_name = fileList;
        this.Filename = event.target.files[0].name;
        this.type = event.target.files[0].type;
        this.ByDefault = true;
        // console.log(this.file_name[0])
	};
	 //after submit the file for product
	 filesubmit(bulkupload) {
        var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");

        if (this.file_name == undefined) {
            // this.toastr.error("Please select file", 'Error');
            this.error = "Please select file";
        }
        else if (this.file_name.length > 0) {

            if (validExts[0] != this.type) {
                // this.toastr.error("Please select correct extension", 'Error');
                this.error = "Please select correct extension";

            }
            else if (validExts = this.type) { //if file is not empty
                let file: File = this.file_name[0];
                const formData: FormData = new FormData(); //convert the formdata
                formData.append('excel_file', file); //append the name of excel file
                let currentUser = localStorage.getItem('LoggedInUser');
                let headers = new Headers();
                let options = new RequestOptions({ headers: headers });
                var gh = file;
                var method = "post"; //post method
                var url = '' //post url
                var i: number; //i is the data
                this.ajax.ajaxpost(formData, method, url).subscribe(data => {
                    // console.log(data['response'].fail.length)
                    if (data['response'].response_code == "200") { //if sucess
                        if (data['response'].fail.length > 0) { //failure response
                            for (i = 0; i < data['response'].fail.length; i++) { //count the length
                                this.toastr.error(data['response'].fail[i].assessment, 'Error!'); //toastr message for error
                                this.modalService.dismissAll(); //to close the modal box
                            }
                        }
                        if (data['response'].sucess.length > 0) { //sucess response
                            for (i = 0; i < data['response'].sucess.length; i++) { //count the length
                                this.toastr.success(data['response'].sucess[i].Quation, 'Success!'); //toastr message for sucess
                                this.modalService.dismissAll(); //to close the modal box
                            }
                        }
                    }
                    else if (data['response'].response_codee == "400") { //if failure
                        this.toastr.error(data['response'].message, 'Error'); //toastr message for error
                    }
                    else { //if not sucess
                        this.toastr.error(data['response'].message, 'Error'); //toastr message for error
                    }

                },

                    (err) => { //if error
                        console.log(err); //prints if it encounters an error
                    }
                );

            }
        }
    };
	  get list_company() {
		return this.Setcharges.get('list_company');
	  }
	//   model for bulk upload
	  open2(content2) {
		this.modalService.open(content2, {
			size: 'lg',
			windowClass: "center-modalsm"
		});
	}
// model for edit button
	  openLarge(content) {
		this.modalService.open(content, {
			size: 'lg',
			windowClass: "center-modal"
		});
	}
	// Submit for setcharges
	  submitform()
	{
	
		if (this.Setcharges.valid) {
			// console.log("success");
			// console.log(this.Setcharges.value)
		}
		else
		{
			// console.log(this.Setcharges.value)
		}

	}

	// function for reset button
	resetForm() {
		this.Setcharges.reset();
	  }
	//   submi valid
		onSubmit() {
		this.isSubmitted = true;
		
	
	  }
}