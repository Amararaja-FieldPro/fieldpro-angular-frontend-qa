import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TatTrendComponent } from './tat-trend.component';

describe('TatTrendComponent', () => {
  let component: TatTrendComponent;
  let fixture: ComponentFixture<TatTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TatTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TatTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
