import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ToastrService } from 'ngx-toastr';// To use toastr
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
declare var require: any
const FileSaver = require('file-saver');
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-servicesparerequest',
  templateUrl: './servicesparerequest.component.html',
  styleUrls: ['./servicesparerequest.component.scss']
})
export class ServicesparerequestComponent implements OnInit {
  ProductLocation: FormGroup;
  UpdateSpareQuantity: FormGroup;
  UpdateImprestSpareQuantity: FormGroup;
  employee_code: any;
  products: any;
  locations: any;
  sparerequests: any;
  product_id: any;
  location_id: any;
  imprest_sparerequests: any;
  updated_spare_request: any;
  imprest_details: any;
  spare_details: any;
  ticket_id: any;
  id: any;
  tabvalue: any
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  showTable: boolean = false;
  showTableImprest: boolean = false;
  UptSpreQuan: boolean = false;
  showTableQuan: boolean = false;
  loadingSub: boolean = false;
  error_spare_code: any;
  approve_error: string
  newsparedetails: Array<items> = [];
  sparedetails = [];
  imprestsparedetails = [];
  currentJustify = 'end';

  // @ViewChild(DataTableDirective, { static: false })
  // dtElement: DataTableDirective;
  // dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject<any> = new Subject();


  newsimprestparedetails: Array<item> = [];
  error: string;
  imprest_sparerequest: any;
  ticket_data: any;
  technicians_details: any;
  ticket: any;
  imprest_spare_id: any;
  httplink: string;
  image_or_video: any;
  constructor(public ajax: ajaxservice,
    public modalService: NgbModal,
    private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required),
      "employee_code": new FormControl('', Validators.required),
    })
    this.UpdateSpareQuantity = new FormGroup({
      "spare_quantity": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),

    })
    this.UpdateImprestSpareQuantity = new FormGroup({
      "spare_quantity": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),

    })
  }
  get log2() {
    return this.UpdateSpareQuantity.controls;
  }
  
  ngOnInit() {
    this.ProductLocation.value.location_id = null
    this.ProductLocation.value.product_id = null
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.tabvalue = 'tab-selectbyid1'
    this.employee_code = localStorage.getItem('employee_code');
    console.log(this.employee_code);
    this.get_ticket_product_location();                                  //get all product and location                          
    this.showTableImprest = false;
    this.UpdateImprestSpareQuantity = new FormGroup({
      "spare_quantity": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),

    })
    this.get_allimprest_spare();
    // this.get_spare_request();
    // this.sparedetails = [
    //   {ticket_id: "",spare_code:"",approved_quantity:''}


    // ];
  }
  // ---------------------------------function for getters----------------------------------------------------------------//
  get log() {
    return this.ProductLocation.controls;                       //error logs for productlocation 
  }
  get log1() {
    return this.UpdateImprestSpareQuantity.controls;                       //error logs for create sms
  }
  //-----------------------------------------Function for passing location and Product------------------------------------//
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code };
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array
        console.log(this.products)
        console.log(this.locations)
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //---------------------------------------------MODAL_POPUP----------------------------------------------------------//
  openmodal(modal, id) {
    this.id = id
    console.log(this.id)
    console.log(this.ticket_id)
    this.modalService.open(modal, {
      size: 'lg',
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  onChangeLocation() {
    if (this.ProductLocation.value.location_id == "") {
      this.ProductLocation.value.location_id = null;
    }
    else {
      this.ProductLocation.value.location_id = this.ProductLocation.value.location_id;
    }
    if (this.ProductLocation.value.product_id == "") {
      this.ProductLocation.value.product_id = null;
    }
    else {
      this.ProductLocation.value.product_id = this.ProductLocation.value.product_id;
    }
    this.showTable = false;

    this.get_spare_request()
  }

  // function for getting the Technician details based on TechnicianId 
  get_technician_details(technician_id, view_techniciandetails) {
    var techId = technician_id;
    var url = 'get_technician_data/?';             // api url for getting the details with using get params
    var technician_code = "technician_code";
    this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;
        console.log(this.technicians_details);
        this.modalService.open(view_techniciandetails, {
          size: 'lg',                                                       //specifies the size of the modal box
          windowClass: "center-modalsm",     //modal popup custom size     
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      } else {
        this.toastr.error("Something went wrong", 'Error');
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  ticketDetails(ticket_id, ticket_details) {
    var tk_id = ticket_id;
    var tickets_id = "ticket_id";
    var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
    this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_data = result.response.data; //bind result in this variable
        this.modalService.open(ticket_details, {
          size: 'lg',                                                       //specifies the size of the modal box
          // windowClass: "center-modalsm",      //modal popup custom size    
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'error');

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }

  onchangeimprestquantity(item, approved_quantity) {
    this.approve_error = '';
    console.log(item);
    console.log(approved_quantity)
    this.error = "";
    const DigitValidation = /^[0-9]*$/;
    var approvedcspare_quan = DigitValidation.test(String(approved_quantity));
    console.log(approvedcspare_quan);
    if (approvedcspare_quan) {
      // this.approve_error = ""
      var data = { "imprest_spare_id": item.imprest_spare_id, "spare_code": item.spare_code, "approved_quantity": approved_quantity }
      console.log(data)
      console.log(item, "item");
      item.approved_quantity = data.approved_quantity;
      console.log(item, "after insert");
      // check whether id exists
      var index = this.newsimprestparedetails.findIndex(items => items.spare_code === data.spare_code);
      if (index > -1) {
        console.log("if")
        // check quantity to determin whether replace the exist one or not
        console.log(this.newsimprestparedetails[index].approved_quantity, "1")
        if (item.approved_quantity != this.newsimprestparedetails[index].approved_quantity) {
          // update quantity for the exist one
          console.log(this.newsimprestparedetails[index].approved_quantity, "2")
          this.newsimprestparedetails[index].approved_quantity = data.approved_quantity;
          this.newsimprestparedetails[index].approved_quantity = item.approved_quantity;
          this.newsimprestparedetails.push(item);
        }
      } else {
        console.log("else")
        // add the new item which dosen't exist
        this.newsimprestparedetails.push(item);
      }
      console.log(this.newsimprestparedetails);
    }
    else {
      this.approve_error = "Please enter only digits"
    }
  }
  //-------------------------------------------Spare requset functions starts----------------------------------------//
  // function for getting the Spare request based on the employee code, product and location 
  get_spare_request() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.employee_code);
    console.log(this.ProductLocation.value)
    this.ProductLocation.value.employee_code = this.employee_code
    var data = this.ProductLocation.value
    console.log(data);
    var url = 'spare_request/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sparerequests = result.response.data;              //storing the api response in the array
        this.showTable = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.overlayRef.detach();
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {                                                         //if not sucess
        this.toastr.error("something went wrong", 'Error');        //toastr message for error


      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  get_spare_details(ticket_id, UpdateSpareQuan) {
    this.UpdateSpareQuantity.reset();
    this.error = '';
    this.approve_error = ''
    this.newsparedetails = [];
    this.sparedetails = [];
    console.log(ticket_id)
    this.ticket = ticket_id;
    var data = ticket_id
    var url = 'spare_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    console.log(url, id, data);
    this.ajax.getdataparam(url, id, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.showTableQuan = true;
        this.spare_details = result.response.data;              //storing the api response in the array
        console.log(this.spare_details);
        this.modalService.open(UpdateSpareQuan, {
          size: 'lg',
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onchangequantity(item, approved_quantity) {
    console.log(item)
    console.log(this.newsparedetails)
    // alert("calling onchange")
    this.approve_error = '';
    this.error = '';
    const DigitValidation = /^[0-9]*$/;
    var approvedcspare_quan = DigitValidation.test(String(approved_quantity));
    if (approvedcspare_quan) {
      var data = { "ticket_id": this.id, "spare_details_id": item.spare_details_id, "approved_quantity": approved_quantity }
      item.approved_quantity = data.approved_quantity;
      // check whether id exists
      var index = this.newsparedetails.findIndex(items =>
        // console.log(items)
        items.spare_details_id === data.spare_details_id
      );
      console.log(index, "index")
      if (index > -1) {
        console.log("if")
        // check quantity to determin whether replace the exist one or not
        if (item.approved_quantity != this.newsparedetails[index].approved_quantity) {
          console.log("inside if")
          //   return;
          // } else {
          // console.log("inside else")
          console.log(data.approved_quantity, "data.approved_quantity")
          console.log(item.approved_quantity, "item.approved_quantity")
          // this.newsparedetails[index].approved_quantity = data.approved_quantity;
          this.newsparedetails[index].approved_quantity = item.approved_quantity;
          this.newsparedetails.push(item);
        }
        else {
          console.log("inside else")
        }
      } else {
        console.log("else")

        // add the new item which dosen't exist
        this.newsparedetails.push(item);
      }
      console.log(this.newsparedetails);
    }
    else {
      this.error_spare_code = item.spare_details_id;
      this.UptSpreQuan = false
      this.approve_error = "Please enter only digits"
    }
  }

  update_spare_quantity() {
    // alert("calling submit")
    this.sparedetails = [];
    var err = [];
    var stack_err = [];
    console.log(this.newsparedetails);
    console.log(this.spare_details);
    const checkRoleExistence = roleParam => this.newsparedetails.some(({ approved_quantity }) => approved_quantity == roleParam)
    console.log(checkRoleExistence(0));
    if (checkRoleExistence(0)) {
      this.approve_error = "Minimum quantity must be greater than one";
    }
    else {
      this.newsparedetails.forEach(element => {
        if (this.spare_details.length == this.newsparedetails.length) {

          if (element.spare_quantity < element.approved_quantity) {
            this.error = 'Approved quantity is too large'
            err.push(this.error)
          }
          else {
            // alert("d")
            var json = {
              "ticket_id": this.ticket,
              "spare_code": element.spare_code,
              "approved_quantity": +element.approved_quantity
            }
            this.sparedetails.push(json);
          }
        }
        else {
          this.approve_error = "Stock Quantiy is required"
          stack_err.push(this.approve_error)

        }
      });
      console.log(err.length, this.error.length)
      console.log(stack_err, stack_err.length)
      if (err.length === 0 && stack_err.length === 0) {
        console.log(this.sparedetails);
        console.log("can go")
        var data = {
          "update_spare": this.sparedetails,
          "employee_code": localStorage.getItem('employee_code')
        };
        var url = 'update_spare_quantity/';                                  // api url for getting the details
        console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {
            // this.updated_spare_request = result.response.data;              //storing the api response in the array
            console.log(this.updated_spare_request)
            this.toastr.success(result.response.message, 'Success');    //toastr message for success    
            this.UpdateSpareQuantity.reset();
            this.approve_error = ""
            this.showTable = false;
            this.ProductLocation.value.location_id = null;
            this.ProductLocation.value.product_id = null;
            this.get_spare_request()
            this.chRef.detectChanges();
            this.modalService.dismissAll();                                 //to close the modal box
          }
          else if (result.response.response_code == "400") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
      else {
        console.log("cant update")
      }
    }

  }
  // function for getting the imprest Spare request based on the employee code
  get_allimprest_spare() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    console.log(this.employee_code)
    var data = this.employee_code
    // var url = 'imprest_spare/?';             // api url for getting the details with using post params
    var url = 'imprest_spare_approved_details/?';             // api url for getting the details with using post params
    var id = "employee_code";
    console.log(url, id, data);
    this.ajax.getdataparam(url, id, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.imprest_sparerequests = result.response.data;              //storing the api response in the array
        this.showTableImprest = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {                                                         //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.showTableImprest = true;
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("something went wrong", 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_imprest_spare(imprest_spare_id, UpdateImprestSpareQuan) {
    this.approve_error = '';
    this.loadingSub = false;
    this.approve_error = '';
    this.UpdateImprestSpareQuantity.reset();
    console.log(imprest_spare_id);
    this.imprest_spare_id = imprest_spare_id;
    var data = imprest_spare_id
    var url = 'get_imprest_spare/?';             // api url for getting the details with using post params
    var id = "imprest_spare_id";
    console.log(url, id, data);
    this.ajax.getdataparam(url, id, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.imprest_sparerequest = result.response.data;              //storing the api response in the array
        console.log(this.imprest_sparerequest);
        this.showTable=true;
        this.modalService.open(UpdateImprestSpareQuan, {
          size: 'lg',
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("something went wrong", 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }

  update_imprest_spare_quantity() {
    this.loadingSub = true;
    this.imprestsparedetails = [];
    var err_imp = [];
    var stack_err_imp = [];
    console.log(this.newsimprestparedetails);
    const checkRoleExistence = roleParam => this.newsimprestparedetails.some(({ approved_quantity }) => approved_quantity == roleParam)
    if (checkRoleExistence(0)) {
      this.approve_error = "Minimum quantity must be 1";
    }
    else {
      console.log(this.newsimprestparedetails, "this.newsimprestparedetails")
      console.log(this.imprest_sparerequest.length, "this.imprest_sparerequest.length")
      this.newsimprestparedetails.forEach(element => {
        console.log(element, "element")
        // if (this.newsimprestparedetails.length == this.imprest_sparerequest.length) {
          if (element.request_quantity < element.approved_quantity) {
            this.error = 'Approved quantity is too large'
            err_imp.push(this.error)

          }
          else {
            var json = {
              "imprest_spare_id": this.imprest_spare_id,
              "spare_code": element.spare_code,
              "spare_location_id": +element.spare_location.spare_location_id,
              "approved_quantity": +element.approved_quantity
            }
            this.imprestsparedetails.push(json);
          }
        // }
        // else {
        //   this.loadingSub = false;
        //   this.approve_error = "Stock Quantiy is required"
        //   stack_err_imp.push(this.approve_error)
        // }
      });
      console.log(err_imp.length, this.error.length)
      console.log(stack_err_imp, stack_err_imp.length)
      if (err_imp.length === 0 && stack_err_imp.length === 0) {
        console.log(this.imprestsparedetails);
        console.log("can go")
        var data = { "update_imprest_spare": this.imprestsparedetails };
        var url = 'update_imprest_spare_quantity/';
        console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {
            console.log(result.response.response_code)
            // this.updated_spare_request = result.response.data;              //storing the api response in the array
            console.log(this.updated_spare_request)
            this.toastr.success(result.response.message, 'Success');    //toastr message for success   
            this.loadingSub = false;
            this.UpdateImprestSpareQuantity.reset()
            this.approve_error = ""
            this.showTableImprest = false;
            this.get_allimprest_spare();
            this.chRef.detectChanges();
            this.modalService.dismissAll();                                 //to close the modal box
          }
          else if (result.response.response_code == "400") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
            this.loadingSub = false;

          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
            this.loadingSub = false;
          }
          else {
            this.toastr.error("Something went wrong", 'Error');        //toastr message for error
            this.loadingSub = false;
          }
        }, (err) => {
          this.loadingSub = false;
          console.log(err);                                        //prints if it encounters an error
        });
      }
      // else {
      //   alert("dont go")
      // }
    }
  }

  findtab(evt: any) {
    console.log(evt)
    this.tabvalue = evt.nextId;
    if (evt.nextId == 'tab-selectbyid1') {
      this.ProductLocation.controls['location_id'].setValue('');
      this.ProductLocation.controls['product_id'].setValue('');
      this.onChangeLocation()
    }
    if (evt.nextId == 'tab-selectbyid2') {
      this.showTableImprest = false;
      this.get_allimprest_spare();

    }


  }
  // function to download the setr downlad
  downloadPdf(ticket_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var id = "ticket_id";
    var url = "setr_pdf/?";
    var value = ticket_id;
    this.ajax.getPdfDocument(url, id, value).subscribe((response) => {
      console.log(response, "response")
      // let file = new Blob([response], { type: 'application/pdf' });
      let file = response.url
      console.log(response.url, "fileURL")
      // var fileURL = URL.createObjectURL(file);
      // window.open(file);  
      FileSaver.saveAs(file, value);
      this.overlayRef.detach();

    }, (err) => {
      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });

  }
  openimage(view_content, image_or_video) {
    this.httplink = environment.image_static_ip;
    this.image_or_video = image_or_video
    console.log(this.image_or_video)
    this.modalService.open(view_content, {
      size: 'lg',                                                        //specifies the size of the modal box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
}
export class items {
  ticket_id: string;
  spare_code: string;
  approved_quantity: number;
  spare_quantity: number;
  spare_details_id: any;
}
export class item {
  imprest_spare_id: number;
  spare_code: string;
  approved_quantity: number;
  request_quantity: any;
  spare_location: any;
}