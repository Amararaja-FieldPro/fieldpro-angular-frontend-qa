import { Component, OnInit } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
//image cropper
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { ToastrService } from 'ngx-toastr';// To use toastr
import Swal from 'sweetalert2';
import { Router } from '@angular/router'; // To enable routing for this component
import { Location } from '@angular/common';
@Component({
  selector: 'kt-sparemaster',
  templateUrl: './sparemaster.component.html',
  styleUrls: ['./sparemaster.component.scss']
})
export class SparemasterComponent implements OnInit {
  sparemasterdetails: any;
  selectedfile = true;
  cropped_img = true;
  public isCollapsed = false;
  Editsparemaster:FormGroup;
  Addsparemaster: FormGroup;              //form group for create subscription
  product_details: any;
  warehousedetails: any;
  subproduct_details:any
  edit_data: any;
  edit_spare:any;
  subproductdatas: any;
    file_name: any;
  spare: any;
  constructor(private modalService: NgbModal,private router: Router, public _location: Location, public ajax: ajaxservice, private toastr: ToastrService,private http: Http) {
     //create subscription form group
     this.Addsparemaster = new FormGroup({
      "spare_code": new FormControl(0, [Validators.required]),
      "spare_name": new FormControl("", [Validators.required, Validators.maxLength(48)]),
      "spare_image": new FormControl('', [Validators.required]),
      "spare_desc": new FormControl("", [Validators.required, Validators.pattern("^[a-z A-Z ]*$"), Validators.maxLength(48)]),
     "product_id": new FormControl("", [Validators.required] ),
        "product_sub_id": new FormControl("", [Validators.required] ),
        "warehouse_id": new FormControl("", [Validators.required] ),
         "quantity_purchased": new FormControl(0, [Validators.required]),
      "quantity_used": new FormControl(0, [Validators.required]),
      "purchased_date": new FormControl(0, [Validators.required]),
     "expiry_date" :new FormControl(0, [Validators.required]),
    });
   }

  ngOnInit() { 
    this.getsparemasterdetails();
    this.getproductmasterdata();
    this.getwarehousedetails();
    this.purshasedate();
   
  }
  
  purshasedate(){
    var today = new Date();
    console.log(today);
    this.Addsparemaster.controls['purchased_date'].setValue(today);
    this.Addsparemaster.controls['expiry_date'].setValue(today);
  }
  getwarehousedetails()
  {
  
    var url = 'get_warehouse/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {            
      if (result.response.response_code == "200") 
      {           
        this.warehousedetails = result.response.data;                    //storing the api response in the array
    
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //Functions for image cropper
  imageChangedEvent: any = '';
  croppedImage: any = '';
  fileChangeEvent(event: any): void {
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper 
    this.imageChangedEvent = event;
  }
  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }
 //image cropping
//  imageCropped(event: ImageCroppedEvent) {
//   this.croppedImage = event.base64;
// }
  upload_img() { //button to clear the cropper
    this.cropped_img = false;
    this.selectedfile = true;
  }
    // Function for modal box 
    openLarge(content6) {
      this.modalService.open(content6, {
        size: 'lg'                                                        //specifies the size of the modal box
      });
    }
    
    getproductmasterdata() {

      var url = 'get_product_details/';                                   // api url for getting the details
      this.ajax.getdata(url).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.product_details = result.response.data;                    //storing the api response in the array
  
        }
      }, (err) => {
        console.log(err);                                                 //prints if it encounters an error
      });
      this.getsubproductdetails();
  
    }
  getsparemasterdetails()
  {
    
    var url = 'get_spare_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {            
      if (result.response.response_code == "200") 
      {           
        this.sparemasterdetails = result.response.data;                    //storing the api response in the array
       console.log(this.sparemasterdetails);
       
                                                  
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  getsubproductdetails()
  {
    var url = 'get_allsubproduct_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {            
      if (result.response.response_code == "200") 
      {           
        this.subproduct_details = result.response.data;                    //storing the api response in the array
      
     }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  onchangeproduct(value){
    console.log(value);
  }
  
  addsparemaster() {
    if(this.Addsparemaster.invalid )                                                   //Check if the package type is empty
    { 
      this.toastr.error("Enter spare name!", 'Error');                //If empty display the toast error message 
    }
  
    else
    {
      console.log(this.Addsparemaster.value);
      var data = this.Addsparemaster.value;                  //Data to be passed for add api
      var url = 'add_spare/'                                         //api url for add
      this.ajax.postdata(url, data).subscribe((result) =>         
      {
        if (result.response.response_code == "200") {
        
          this.ngOnInit();                                            //reloading the component
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();                                 //to close the modal box
        }
      else{
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  editspare(data) {
    this.edit_spare = data;
    console.log(data);
    this.Editsparemaster = new FormGroup({
      "spare_id": new FormControl(this.edit_spare.spare_id, [Validators.required]),
      "spare_code": new FormControl(this.edit_spare.spare_code, [Validators.required]),
      "spare_name": new FormControl(this.edit_spare.spare_name, [Validators.required, Validators.maxLength(48)]),
      "spare_image": new FormControl(this.edit_spare.spare_image, [Validators.required]),
      "spare_desc": new FormControl(this.edit_spare.spare_desc, [Validators.required, Validators.pattern("^[a-z A-Z ]*$"), Validators.maxLength(48)]),
     "product_id": new FormControl(this.edit_spare.product_id, [Validators.required] ),
        "product_sub_id": new FormControl(this.edit_spare.product_sub_id, [Validators.required] ),
        "warehouse_id": new FormControl(this.edit_spare.warehouse_id, [Validators.required] ),
         "quantity_purchased": new FormControl(this.edit_spare.quantity_purchased, [Validators.required]),
      "quantity_used": new FormControl(this.edit_spare.quantity_used, [Validators.required]),
      "purchased_date": new FormControl(this.edit_spare.purchased_date, [Validators.required]),
     "expiry_date" :new FormControl(this.edit_spare.expiry_date, [Validators.required]),
     
    });
    console.log(this.edit_spare.value);                                   //Stores the data into the array variable
  }
  editsubproduct(edit_spare) {
    var url = 'edit_spare/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.edit_spare.value)
      .subscribe((result) => {
        if (result.response.response_code == "200") {
          this.ngOnInit();                                                //reloading the component
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error');            //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                                 //prints if it encounters an error
      });
  }
  fileUpload(event) {
   
    let fileList: FileList = event.target.files;
    this.file_name=fileList; 
    // this.filesubmit(event);
  };
  filesubmit(bulkupload) {
    if(this.file_name.length > 0) {
    let file: File = this.file_name[0]; 
    const formData: FormData = new FormData();
    formData.append('excel_file', file);
    console.log(formData);
    console.log(file);
    let currentUser = localStorage.getItem('LoggedInUser');
    let headers = new Headers();
    // headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));
    let options = new RequestOptions({ headers: headers });
 
  var gh = file; 
  console.log(gh);
  var method = "post";
  var url = 'spare_bulk_upload/'
  var i: number; 
  this.ajax.ajaxpost( formData,method,url) .subscribe(data => {
console.log(data['response'].fail.length)
      if (data['response'].response_code == "200") {
        console.log(data['response'].fail);
        console.log(data['response'].fail.length)                          //if sucess
      if (data['response'].fail.length > 0) {                                    //failure response 
      //if employee code or employee id is error to put this
        for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
          this.toastr.error(data['response'].fail[i].error_message, 'Error!');   //toastr message for error
          // this.toastr.error(data['response'].fail[i].email_id, 'Error!'); 
          // this.toastr.error(data['response'].fail[i].contact_number, 'Error!'); 
          console.log(data['response'].fail[i].error_message);
          this.modalService.dismissAll();                                 //to close the modal box
          this.router.navigateByUrl("/location", { skipLocationChange: true }).then(() => {
            console.log(decodeURI(this._location.path()));
            this.router.navigate([decodeURI(this._location.path())]);
          });
        }
      }
      //success response
      //if newly add product for excisting employee code or employee id
      console.log(data['sucess']);
      if (data['sucess'].length > 0) {                         //sucess response
        for (i = 0; i < data['sucess'].length; i++) {    
        
          this.ngOnInit();               //count the length
          this.toastr.success(data['sucess'][i].success_message, 'Success!');  //toastr message for sucess
          this.modalService.dismissAll();                                 //to close the modal box
          this.router.navigateByUrl("/location", { skipLocationChange: true }).then(() => {
            console.log(decodeURI(this._location.path()));
            this.router.navigate([decodeURI(this._location.path())]);
          });
        }
      }
  }
 
  

     },
     
     error => this.toastr.error(error) 
     );
     } 
};
subfilesubmit(){
  if(this.file_name.length > 0) {
    let file: File = this.file_name[0]; 
    const formData: FormData = new FormData();
    formData.append('excel_file', file);
    console.log(formData);
    console.log(file);
    let currentUser = localStorage.getItem('LoggedInUser');
    let headers = new Headers();
   
    let options = new RequestOptions({ headers: headers });
 
  var gh = file; 
  console.log(gh);
  var method = "post";
  var url = 'subproduct_bulk_upload/'
  var i: number; 
  this.ajax.ajaxpost( formData,method,url) .subscribe(data => {
// console.log(data['response'].fail.length)
      if (data['response'].response_code == "200") {
      if (data['response'].fail.length>0) {
        for(i=0;i<data['response'].fail.length;i++){
        this.toastr.error(data['response'].fail[i].Product_non_exist, 'Error!'); 
        this.modalService.dismissAll();                                 //to close the modal box
      }
    }
      if (data['response'].sucess.length>0) {
        for(i=0;i<data['response'].sucess.length;i++){
        this.toastr.success(data['response'].sucess[i].sub_product_save, 'Success!'); 
        this.modalService.dismissAll();                                 //to close the modal box
      }
    }
  }
 
  else {
    this.toastr.error(data['response'], 'Error');        //toastr message for error
    // this.toastr.error(data['response'].Product_non_exist, 'Error');        //toastr message for error
  }

     },
     
     error => this.toastr.error(error) 
     );
     } 
}
trashspare(data){
  this.spare=data;
      var spare_id=this.spare.spare_id;
      console.log(spare_id);
    
      var spareid = +spare_id;                                    
      var url = 'delete_spare/?';             // api url for getting the details with using post params
      var id = "spare_id";
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
      this.ajax.getdataparam(url, id, spareid).subscribe((result) => {
        if (result.response.response_code == "200") {
        this.ngOnInit();                                                //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
                                   
        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error');            //toastr message for error
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }
    })
    }
}
