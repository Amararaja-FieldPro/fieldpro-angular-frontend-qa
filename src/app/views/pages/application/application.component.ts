import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
@Component({
  selector: 'kt-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {
  AddApplication: FormGroup;
  EditApplication: FormGroup;
  error: any;
  application: [];
  segment:[];
  prioritys = [];
  showTables: boolean = false;
  //loader
  loadingAdd: boolean;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(public ajax: ajaxservice, private toastr: ToastrService, private overlay: Overlay, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) {
    this.AddApplication = new FormGroup({
      "priority":new FormControl("", [Validators.required, ]),
      "segment_id": new FormControl("", [Validators.required, ]),
      "application_name": new FormControl("", [Validators.required,  this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
    })
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  get log() {
    return this.AddApplication.controls;
  }
  get logedit() {
    return this.EditApplication.controls;
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.getapplication();
    this.getsegment();
    this.prioritys = [
      { priority_id: "P1", priority_name: "P1" },
      { priority_id: "P2", priority_name: "P2" },
      { priority_id: "P3", priority_name: "P3" },
      { priority_id: "P4", priority_name: "P4" },
    ];

  }
  getapplication() {
    var data = {};// storing the form group value
    var url = 'get_all_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.application = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  openLarge(content6){
    this.error = '';
    this.AddApplication.reset();
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  addapplication(){
    this.loadingAdd=true;
    var data = this.AddApplication.value;// storing the form group value
    var url = 'add_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
       this.showTables = false;
        this.chRef.detectChanges();
        this.getapplication();
        this.modalService.dismissAll();
        this.loadingAdd=false;
        this.toastr.success(result.response.message, 'Sucess');  
      }

      else if (result.response.response_code == "500") {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd=false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  editapplicationmodal(item,edit){
    // console.log(item);
    this.EditApplication = new FormGroup({
      "application_id": new FormControl(item.application_id, [Validators.required, ]),
      "priority":new FormControl(item.priority, [Validators.required, ]),
      "segment_id": new FormControl(item.segment.segment_id, [Validators.required, ]),
      "application_name": new FormControl(item.application_name, [Validators.required,  this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
   });
    this.modalService.open(edit, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  editapplication(){
    this.loadingAdd=true;
    var data = this.EditApplication.value;// storing the form group value
    var url = 'edit_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
       this.showTables = false;
        this.chRef.detectChanges();
        this.getapplication();
        this.modalService.dismissAll();
        this.loadingAdd=false;
        this.toastr.success(result.response.message, 'Sucess');  
      }

      else if (result.response.response_code == "500") {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd=false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd=false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  detele(item){
    console.log(item)
    var data = { "application_id": item };
    var url = 'delete_application/'; // api url for delete warehouse
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showTables = false;
            this.chRef.detectChanges();
            this.getapplication();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
}
