import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormControl, FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
@Component({
  selector: 'kt-compensation',
  templateUrl: './compensation.component.html',
  styleUrls: ['./compensation.component.scss']
})
export class CompensationComponent implements OnInit {
  loadingAdd: boolean = false;
  AddCompensate: FormGroup;
  EditCompensate: FormGroup;
  compensate: any;
  showTables: boolean = false;
  location_type: any;
  range: { range: string; }[];
  replaced: { replaced: string; }[];
    //loader
    overlayRef: OverlayRef;
    LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(private formBuilder: FormBuilder, private overlay: Overlay, public ajax: ajaxservice, private toastr: ToastrService, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) {

    this.AddCompensate = new FormGroup({
      "location_type_id": new FormControl('', [Validators.required]),
      "ah_range": new FormControl("", [Validators.required]),
      "number_bc_replaced": new FormControl("", [Validators.required]),
      "amount": new FormControl("", [Validators.required]),
      "voltage": new FormControl("", [Validators.required]),
    });
  }

  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.getcompensate();
  }
  addcompensate() {
    this.loadingAdd = true;
    var data = this.AddCompensate.value;
    var url = 'add_compensation/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {


        this.showTables = false;
        this.chRef.detectChanges();
        this.getcompensate();
        this.modalService.dismissAll();
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingAdd = false;
      }

      else if (result.response.response_code == "400") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingAdd = false;
      }
      else if (result.response.response_code == "500") {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd = false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  getcompensate() {
    var data = {};// storing the form group value
    var url = 'get_compensation/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.compensate = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  // getlocation() {
  //   var data = {};// storing the form group value
  //   var url = 'get_location_type/'                                         //api url of remove license
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.location_type = result.response.data;

  //     }

  //     else if (result.response.response_code == "500") {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                             //prints if it encounters an error
  //   });


  // }
  get log() {
    return this.AddCompensate.controls;
  }
  get logs() {
    return this.EditCompensate.controls;
  }
  onChangevoltageEdit(voltage) {
    if (voltage == '2v') {
      this.range = [{ 'range': 'below 450' }, { 'range': 'above 450' }];
      this.replaced = [{ 'replaced': '0' }, { 'replaced': '1-2' }, { 'replaced': '3-4' }, { 'replaced': '4+' }]
    }
    else if (voltage == '12v') {
      this.range = [{ 'range': '26-84' }, { 'range': '100-200' }];
      this.replaced = [{ 'replaced': '0' }, { 'replaced': '1' }, { 'replaced': '2' }, { 'replaced': '3-4' }, { 'replaced': '5+' }]
    }
    this.EditCompensate.controls["ah_range"].setValue('');
    this.EditCompensate.controls["number_bc_replaced"].setValue('');
    var data = { "voltage": voltage }
    var url = 'get_voltage_locations/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.location_type = result.response.data;

      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  onChangevoltage(voltage) {
    if (voltage == '2v') {
      this.range = [{ 'range': 'below 450' }, { 'range': 'above 450' }];
      this.replaced = [{ 'replaced': '0' }, { 'replaced': '1-2' }, { 'replaced': '3-4' }, { 'replaced': '4+' }]
    }
    else if (voltage == '12v') {
      this.range = [{ 'range': '26-84' }, { 'range': '100-200' }];
      this.replaced = [{ 'replaced': '0' }, { 'replaced': '1' }, { 'replaced': '2' }, { 'replaced': '3-4' }, { 'replaced': '5+' }]
    }

    var data = { "voltage": voltage }
    var url = 'get_voltage_locations/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.location_type = result.response.data;

      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });


  }
  openLarge(content6) {
    this.AddCompensate.reset();
    // this.getlocation();
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });

  }
  edit_compensate() {
    this.loadingAdd = true;
    var data = this.EditCompensate.value;// storing the form group value
    var url = 'edit_compensation/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {

        this.showTables = false;
        this.chRef.detectChanges();
        this.getcompensate();
        this.modalService.dismissAll();
        this.toastr.success(result.response.message, 'Sucess');
        this.loadingAdd = false;
      }

      else if (result.response.response_code == "500") {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingAdd = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingAdd = false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  editcompensatemodal(item, edit) {
    // console.log(item)
    // this.getlocation();
    this.onChangevoltage(item.voltage);
    this.EditCompensate = new FormGroup({
      "compensation_id": new FormControl(item.compensation_id, [Validators.required]),
      "location_type_id": new FormControl(item.location_type.location_type_id, [Validators.required]),
      "ah_range": new FormControl(item.ah_range, [Validators.required]),
      "number_bc_replaced": new FormControl(item.number_bc_replaced, [Validators.required]),
      "amount": new FormControl(item.amount, [Validators.required]),
      "voltage": new FormControl(item.voltage, [Validators.required]),
    });
    this.modalService.open(edit, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  detele(item) {
    // console.log(item)
    var data = { "compensation_id": item };
    var url = 'delete_compensation/'; // api url for delete warehouse
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        // console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showTables = false;
            this.chRef.detectChanges();
            this.getcompensate();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
