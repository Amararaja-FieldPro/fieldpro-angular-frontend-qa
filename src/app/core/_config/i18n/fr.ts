// France
export const locale = {
	lang: 'fr',
	data: {
		TRANSLATOR: {
			SELECT: 'choisissez votre langue',
		},
		MENU: {
			NEW: 'Nouveau',
			ACTIONS: 'Actes',
			CREATE_POST: 'Créer un nouveau Post',
			PAGES: 'Pages',
			FEATURES: 'Fonctionnalités',
			APPS: 'Applications',
			DASHBOARD: 'Tableau de Bord',
			NEW_SUBSCRIPTION:'NOUVEL ABONNEMENT',
			MANAGE_SUBSCRIPTION:'gérer labonnement',
			PRODUCT_MASTER:'produit maître',
			SLA_MAPPING:'Sla Mapping',
			BILLING_DETAILS:'DÉTAILS DE LA FACTURATION',
			PACKAGE_MANAGEMENT:'GESTION DES PAQUETS',
			PACKAGE_PLAN:'plan de forfait',
			PACKAGE_CONFIGURATION:'CONFIGURATION DU FORFAIT',
			WAREHOUSE:'maître dentrepôt',
			SPARE:'Maître de rechange',
			SERVICE:'',
			AMC:'',
		},
		AUTH: {
			GENERAL: {
				OR: 'Ou',
				SUBMIT_BUTTON: 'Soumettre',
				NO_ACCOUNT: 'Ne pas avoir de compte?',
				SIGNUP_BUTTON: 'Registre',
				FORGOT_BUTTON: 'Mot de passe oublié',
				BACK_BUTTON: 'Back',
				PRIVACY: 'Privacy',
				LEGAL: 'Legal',
				CONTACT: 'Contact',
			},
			LOGIN: {
				TITLE: 'Créer un compte',
				BUTTON: 'Sign In',
			},
			FORGOT: {
				TITLE: 'mot de passe oublié ?',
				DESC: 'Enter your email to reset your password',
				SUCCESS: 'Your account has been successfully reset.'
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'Your account has been successfuly registered.'
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Mot de passe',
				CONFIRM_PASSWORD: 'Confirm Password',
				USERNAME: 'Nom d\'utilisateur'
			},
			VALIDATION: {
				INVALID: '{{name}} n\'est pas valide',
				REQUIRED: '{{name}} est requis',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: 'The login detail is incorrect',
				REQUIRED_FIELD: 'Required field',
				MIN_LENGTH_FIELD: 'Minimum field length:',
				MAX_LENGTH_FIELD: 'Maximum field length:',
				INVALID_FIELD: 'Field is not valid',
			}
		},
		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Nombre d\'enregistrements sélectionnés: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'éCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Les clients',
				CUSTOMERS_LIST: 'Liste des clients',
				NEW_CUSTOMER: 'Nouveau client',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Suppression du client',
					DESCRIPTION: 'Êtes-vous sûr de supprimer définitivement ce client?',
					WAIT_DESCRIPTION: 'Le client est en train de supprimer ...',
					MESSAGE: 'Le client a été supprimé'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Supprimer les clients',
					DESCRIPTION: 'Êtes-vous sûr de supprimer définitivement les clients sélectionnés?',
					WAIT_DESCRIPTION: 'Les clients suppriment ...',
					MESSAGE: 'Les clients sélectionnés ont été supprimés'
				},
				UPDATE_STATUS: {
					TITLE: 'Le statut a été mis à jour pour les clients sélectionnés',
					MESSAGE: 'Le statut des clients sélectionnés a été mis à jour avec succès'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Le client a été mis à jour',
					ADD_MESSAGE: 'Le client a été créé'
				}
			}
		},
		PRODUCT_MASTER:{
		PARA:'Une machine à laver est une machine qui lave les vêtements sales ... Ce baril est rempli deau, puis tourne très rapidement pour que leau élimine la saleté des vêtements. La plupart des machines à laver sont conçues de manière à ce que les détergents (liquides ou poudres) puissent être introduits dans la machine. Ceux-ci peuvent aider à rendre le vêtement plus propre.',
		PARA1:'Une machine à laver est une machine qui lave les vêtements sales ... Ce baril est rempli deau, puis tourne très rapidement pour que leau élimine la saleté des vêtements. La plupart des machines à laver sont conçues de manière à ce que du détergent (liquides ou poudres) puisse être introduit dans la machine. Ceux-ci peuvent aider à rendre le vêtement plus propre.',
	},
	NEW_SUBSCRIPTION:{
		COMPANY_INFO:'INFORMATION DENTREPRISE',
		ADDRESS:'adresse',
		CONFIRM_DETAILS:'confirmer les détails',
		ENTER:'Entrer les détails de lentreprise',
		ADD:'adresse',
DET:'Confirmer les détails',
			}
}
};