// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';// RxJS
import { Observable, Subject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Auth
import { ToastrService } from 'ngx-toastr';// To use toastr
import { AuthNoticeService, AuthService } from '../../../../core/auth';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
// Layout
// import {  MenuAsideService } from '../../../../core/_base/layout';
import { MenuConfig } from '../../../../core/_config/menu.config';

// Services
import { MenuConfigService } from '../../../../core/_base/layout/services/menu-config.service';
import { MenuHorizontalService } from '../../../../core/_base/layout/services/menu-horizontal.service';

/**
 * ! Just example => Should be removed in development
 */
// Object-Path
import * as objectPath from 'object-path';
const DEMO_PARAMS = {
	EMAIL: '',
	PASSWORD: ''
};
@Component({
	selector: 'kt-customer-login',
	templateUrl: './customer-login.component.html',
	styleUrls: ['./customer-login.component.scss'],
	// encapsulation: ViewEncapsulation.
})
export class CustomerLoginComponent implements OnInit {

	// Public params
	loginForm: FormGroup;
	forgotPasswordForm: FormGroup;
	loadingAdd: boolean = false;
	error_msgs: any; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	// loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];
	menu: any = []; // array push
	submenu: any = []; // array push
	today: number = Date.now();
	stop$ = new Subject();
	private unsubscribe: Subject<any>;

	private returnUrl: any;
	// Private properties
	private menuConfig: MenuConfig;
	aside: any;
	private headerMenus: any;
	private asideMenus: any;
	role_id: any;
	employee_code: any;
	error_msg: any;
	loading: boolean;
	fieldTextType: boolean;
	login: boolean = true;
	forgot: boolean = false;
	loaderclass: string;
	loaderclasssubmit: string;
	role_name: string;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 * @param route
	 * @param menuConfigService: MenuConfigService
	 * @param MenuHorizontalService:MenuHorizontalService
	 * @param authService
	
	 
	 */
	constructor(private chRef: ChangeDetectorRef,
		public menuConfigService: MenuConfigService,
		private router: Router,
		private MenuHorizontalService: MenuHorizontalService,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		public ajax: ajaxservice,
		public toastr: ToastrService,
		private authService: AuthService,


		// public menuAsideService: MenuAsideService,
	) {
		this.unsubscribe = new Subject();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {

		this.initLoginForm();
		this.initRegistrationForm();
		this.loaderclass = "kt-spinner--right kt-spinner--md ";
		this.loaderclasssubmit = "kt-spinner--right kt-spinner--md ";
		// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'] || 'dashboard';
		});
	}
	initRegistrationForm() {
		this.forgotPasswordForm = this.fb.group({
			email: ['', Validators.compose([
				Validators.required,
				Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			]
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {

		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		// demo message to show
		if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			const initialNotice = `Use account
			<strong>${DEMO_PARAMS.EMAIL}</strong> and password
			<strong>${DEMO_PARAMS.PASSWORD}</strong> to continue.`;
			this.authNoticeService.setNotice(initialNotice, 'info');
		}

		this.loginForm = this.fb.group({
			email: [DEMO_PARAMS.EMAIL, Validators.compose([
				Validators.required,
				// Validators.email, 
				Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			],
			password: [DEMO_PARAMS.PASSWORD, Validators.compose([
				Validators.required,
				Validators.minLength(6),
				Validators.maxLength(100)
			])
			]
		});
	}

	toggleFieldTextType() {
		this.fieldTextType = !this.fieldTextType;
	}

	/**
	 * Form Submit
	 */
	loginclick() {
		this.login = false;
		this.forgot = true;
	}
	forgotclick() {
		this.forgotPasswordForm.reset();
		this.login = true;
		this.forgot = false;
	}

	submit() {
		this.error_msg = "";
		this.loading = true;
		const controls = this.loginForm.controls;
		this.loaderclass = "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--login signin-btn': loading";


		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		let password = ".FGTkgmLE234/@##$#%@ZvWK;l";
		// let salt = "Pdg4523#$@72nxgl;56i";   
		let salt = "523#$@72nxgl;56i"
		let iterations = 128;
		let bytes = CryptoJS.PBKDF2(password, salt, { keySize: 48, iterations: iterations });
		let iv = CryptoJS.enc.Hex.parse(bytes.toString().slice(0, 32));   //iv  
		let key = CryptoJS.enc.Hex.parse(bytes.toString().slice(32, 96)); //secret key 	
		let cipher_password = CryptoJS.AES.encrypt(controls['password'].value, key, { iv: iv }); //cipher text for password
		// let cipher_username = CryptoJS.AES.encrypt(controls['email'].value, key, { iv: iv });      //cipher text for username
		// console.log(controls['email'].value, "controls['email']")
		// console.log(cipher_password, "cipher_password")


		// code for decryption
		// let decrypt = (CryptoJS.AES.decrypt(cipher_password, key, { iv: iv })).toString(CryptoJS.enc.Utf8);
		// console.log(cipher_username.toString(),"encrypted format")   
		// console.log(decrypt,"decrypted format")

		const authData = {
			username: controls['email'].value,
			password: cipher_password.toString()
		};
		// console.log(authData, 'authData')
		var url = 'web_customer_login/'
		this.ajax.postdata(url, authData).subscribe((result) => {
			if (result) {
				if (result.response.response_code == "200") {
					localStorage.setItem('user_name', result.response.customer_details.customer_name);
					localStorage.setItem('user_role', result.response.customer_details.role.role_name);
					localStorage.setItem('role_id', result.response.customer_details.role.role_id);
					localStorage.setItem('email_id', result.response.customer_details.email_id);
					localStorage.setItem('customer_code',result.response.customer_details.customer_code)
					this.role_id = localStorage.getItem('role_id');
					this.role_name = localStorage.getItem('user_role');
					this.headerMenus = objectPath.get(this.menuConfigService.getMenus(), 'header');
					this.asideMenus = objectPath.get(this.menuConfigService.getMenus(), 'aside');
					localStorage.setItem('asideMenus', this.asideMenus);
					if (this.role_id == 7) {
						this.router.navigate(["/raiseticket"]);
					}			
				}
				else {
					this.error_msg = result.response.message;
					// console.log(this.error_msg);
					this.loaderclass = "kt-spinner--right kt-spinner--md kt-spinner--login ";
					this.chRef.detectChanges();
				}
			}
		});
	}


	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
	isControlHasError1(controlName: string, validationType: string): boolean {
		const control = this.forgotPasswordForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

}
