import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';

import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';


@Component({
	selector: 'kt-spare-report',
	templateUrl: './spare-report.component.html',
	styleUrls: ['./spare-report.component.scss']
})
export class SpareReportComponent implements OnInit {

	from: NgbDateStruct;
	to: NgbDateStruct;
	service_group: any;
	techId: any;
	assesment_data: any;
	technician_code: any;
	from_date: any;
	to_date: any;
	spare_report: any;
	employee_code: any;
	filterSpare: FormGroup;    //form group 
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	spares: any;
	spare_request_details: any;
	product_id: any;
	today: any;
	location_id: any;
	showmodelTable: boolean = false;
	overlayRef: OverlayRef;
	ticket_data: any;
	loadingSub: boolean = false;    // button spinner by Sowndarya
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	technicians_details: any;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTableModal: boolean = false;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		this.filterSpare = new FormGroup({
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to, Validators.required),
			"technician_code": new FormControl('', Validators.required),
			"product_id": new FormControl('', Validators.required),
			"location_id": new FormControl('', Validators.required),
		})

	}

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);

		var dob = new Date();
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		this.today = Fdate;
		// this.from_date=Fdate;
		// this.to_date=Fdate;
		var params = {
			"technician_code": null,
			"product_id": null,
			"to_date": Fdate,
			"from_date": Fdate,
			'location_id': null
		}
		this.getTech();
		this.getSpare(params);
		this.get_ticket_product_location();

	}

	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.spare_report, 'spare_reports');
	}
	getTech() {
		var url = 'get_alltechnician_details/';                                  // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.alltech = result.response.data;              //storing the api response in the array
				//storing the api response in the array     
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	spareDetails(data, spare_required) { //get Spare Details
		this.spares = data;
		console.log(data);
		var tk_id = this.spares.ticket_id;
		var ticket_id = "ticket_id";
		var url = 'spare_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_request_details = result.response.data; //bind result in this variable
				this.showTableModal = true;
				console.log(this.spare_request_details);
				this.modalService.open(spare_required, {
					size: 'lg',
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	get_ticket_product_location() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var data = { "employee_code": emp_code };
		var url = 'get_ticket_product_location/';                                  // api url for getting the details
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.products = result.response.product;              //storing the api response in the array
				this.locations = result.response.location;              //storing the api response in the array     
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterSpareF() {
		this.loadingSub = true;
		this.from_date = this.filterSpare.value.from_date;
		this.to_date = this.filterSpare.value.to_date;
		this.technician_code = this.filterSpare.value.technician_code;
		this.product_id = this.filterSpare.value.product_id;
		this.location_id = this.filterSpare.value.location_id;
		console.log(this.filterSpare.value);

		if (this.filterSpare.value.from_date.year == undefined) {
			this.filterSpare.value.from_date = this.filterSpare.value.from_date;
			console.log(this.filterSpare.value.from_date);
		}
		else {
			console.log(this.filterSpare.value.from_date)
			var Fyear = this.filterSpare.value.from_date.year;
			var Fmonth = this.filterSpare.value.from_date.month;
			var Fday = this.filterSpare.value.from_date.day;
			this.filterSpare.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
		}
		if (this.filterSpare.value.to_date.year == undefined) {
			this.filterSpare.value.to_date = this.filterSpare.value.to_date;
			console.log(this.filterSpare.value.to_date);
		}
		else {
			var Tyear = this.filterSpare.value.to_date.year;
			var Tmonth = this.filterSpare.value.to_date.month;
			var Tday = this.filterSpare.value.to_date.day;
			this.filterSpare.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;

		}
		if (this.product_id == "") {
			this.filterSpare.value.product_id = null;
		}
		else {
			this.filterSpare.value.product_id = this.filterSpare.value.product_id;
		}
		if (this.location_id == "") {
			this.filterSpare.value.location_id = null;
		}
		else {
			this.filterSpare.value.location_id = this.filterSpare.value.location_id;
		}
		if (this.technician_code == "") {
			this.filterSpare.value.technician_code = null;
		}
		else {
			this.filterSpare.value.technician_code = this.filterSpare.value.technician_code;
		}
		this.showmodelTable = false;
		this.getSpare(this.filterSpare.value);
	}
	// --------------------------------function for view the modal box------------------------------------------------------//
	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',                                                       //specifies the size of the modal box
					windowClass: "center-modalsm",      //modal popup custom size     
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');            //toastr message for error
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getSpare(data) {

		var url = 'spare_reports/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.loadingSub = false;
				this.spare_report = result.response.data;
				this.showmodelTable = true;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				this.overlayRef.detach();


			}
			else if (result.response.response_code == "500") {
				this.showmodelTable = true;
				this.loadingSub = false;
				// this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			this.loadingSub = false;
			// this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(content6) {
		this.modalService.open(content6, {
			size: 'lg'
		});
	}
	// function for getting the Technician details based on TechnicianId 
	get_technician_details(ticket_id, technician_id, view_techniciandetails) {
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.ticket_data = result.response.data; //bind result in this 		
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');            //toastr message for error
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
		var ticket_id = ticket_id;
		var technician_id = technician_id;
		var url = 'get_technician_details/?';             // api url for getting the details with using get params
		var id = "ticket_id";
		var id1 = "technician_id";
		this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technicians_details = result.response.data;
				console.log(this.technicians_details);
				this.modalService.open(view_techniciandetails, {
					size: 'lg',                                                       //specifies the size of the modal box
					windowClass: "center-modalsm",      //modal popup custom size 
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click   
				});
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');            //toastr message for error
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
}
