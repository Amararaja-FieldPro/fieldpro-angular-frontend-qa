import { Component, OnInit, ɵConsole, ViewContainerRef, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormArray, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
// import { Http, RequestOptions, Headers } from '@angular/http';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { MatRadioChange } from '@angular/material';
// import moment from 'moment' //find the difference b/w date


@Component({
  selector: 'kt-amc-contract',
  templateUrl: './amc-contract.component.html',
  styleUrls: ['./amc-contract.component.scss'],
  providers: [
    NgbTabset
  ]
})
export class AmcContractComponent implements OnInit {
  //url navigations
  asideMenus: any;
  current_url: any;
  exist: any;
  show: boolean = true;
  AddButton: Boolean = false;
  states: any;                                                    //variable to store the state
  cities: any;                                                    //variable to store the city
  country: any;
  locations: any;                                                 //variable to store the country
  RenewCustomerInfo: FormGroup;
  RenewProductInfo: FormGroup;
  RenewContract: FormGroup;
  NewContract: FormGroup;                                 //FormGroup variable for creating new customer 
  ReCustomerInfo: FormGroup;
  ReProductInfo: FormGroup;
  form: any;
  customer_details: any;                                           //variable to store the customer data getting from the api call
  contract_details: any;                                           //variable to store the  contract data getting from the api call
  contract_detail: any;
  customer_code: any;                                              //variable to get the customer code and pass it to get the customer details
  contact_number: any;                                            //variable to get the contact number and pass it to get the customer details
  product: any;
  subproduct: any;
  work_type: any;
  call_category: any;
  selectedfile = true;
  // image:any = '';
  cropped_img = true;
  contract_id: any;
  raiseticket_details: any;
  contrac_type: any;
  employee_id: any;
  contract_data: any
  isShow: any;
  submitted = false;
  contact_no: any;
  model: NgbDateStruct;
  activeId: any;
  preId: any;
  activerId: any;
  prerId: any;
  nextrId: any;
  prefId: any;
  nextId: any;
  evt: any;
  serail_nos: any;
  serial_validation: any;
  // multiserial_validation: any;
  save_validation: any;
  serial_valida_success: any;
  private tabSet: ViewContainerRef;
  // serial_noFor_validation: any;
  EnableAddButton: boolean = false;
  RemoveBtnDisable: boolean = false;
  radiochange_value: any;
  firstDate: any;
  secondDate: any;
  expiry_day: any;
  @ViewChild('div', { static: true }) div: ElementRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  save: any;
  item: any;
  // form: FormGroup;
  serial_no: FormArray;
  forms: any;
  serialnum = [];
  no: string;
  serial_empty: string;
  duplicate: string;
  count: any;
  arr_serial: any[];
  error: string;
  api_response: any;
  renew_error: any;
  contract_error: any;
  customer_error: string;
  serial_pattern_validation: string;
  errors: string;
  loadingSub: boolean = false;
  valid_empty_serial_no: string;
  constructor(private ajax: ajaxservice, private formBuilder: FormBuilder, private modalService: NgbModal, private router: Router, private fb: FormBuilder, private overlay: Overlay,
    private renderer: Renderer2, private el: ElementRef,
    private toastr: ToastrService, public _location: Location, private calendar: NgbCalendar, public tabset: NgbTabset) {
    this.model = this.calendar.getToday();
    this.RenewCustomerInfo = new FormGroup({
      "emoployee_id": new FormControl(this.employee_id),
      "lable_type": new FormControl(""),
      "ticket_id": new FormControl(""),
      "contract_id": new FormControl("", [Validators.required]),
      "customer_code": new FormControl("", [Validators.required]),
      "customer_name": new FormControl('', [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "plot_number": new FormControl("", [Validators.required, Validators.maxLength(32), Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(''),
      "post_code": new FormControl('', [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('null', [Validators.required]),
      "state_id": new FormControl('null', [Validators.required]),
      "city_id": new FormControl('null', [Validators.required]),
      "location_id": new FormControl('null', [Validators.required]),
    })
    this.RenewProductInfo = new FormGroup({
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required]),
      "serial_no": new FormControl("", [Validators.required]),
      "contract_type": new FormControl("", [Validators.required]),
      "cust_preference_date": new FormControl(this.model, [Validators.required]),
      "quantity": new FormControl(1),
      "contract_period": new FormControl('', [Validators.required])
    });
    this.ReCustomerInfo = new FormGroup({
      //------------------------formcontrol for customer details--------------------------------------//
      "lable_type": new FormControl(""),
      "customer_name": new FormControl("", this.noWhitespace),
      "customer_code": new FormControl("", [Validators.required]),
      "ticket_id": new FormControl(""),
      "customer_id": new FormControl(""),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "plot_number": new FormControl("", [Validators.required, Validators.maxLength(32), Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(""),
      "post_code": new FormControl('', [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
    });
    this.ReProductInfo = new FormGroup({
      //------------------------formcontrol for contract details--------------------------------------//
      "product_id": new FormControl("", [Validators.required]),
      "product_sub_id": new FormControl("", [Validators.required]),// product_sub_id
      "model_no": new FormControl("", [Validators.required, this.noWhitespace]),
      "serial_no": this.fb.array([this.fb.control(null)], [Validators.required]),
      "cust_preference_date": new FormControl(this.model, [Validators.required]),
      "contract_period": new FormControl("", [Validators.required]),
      "contract_type": new FormControl("", [Validators.required]),
      "priority": new FormControl('', Validators.required),
    });
    this.createEventForm();
  }
  createEventForm() {
    this.form = this.formBuilder.group({
      packages: this.serial_no
    });
  }
  // quantities(): FormArray {
  //   var textbox_length = this.ReProductInfo.get("serial_no") as FormArray
  //   if (textbox_length.length < 2) {
  //     this.RemoveBtnDisable = false;
  //     this.serial_valida_success = '';
  //     this.multiserial_validation = "Minimum one serial number is required";
  //   }
  //   else {
  //     this.multiserial_validation = ""
  //     return this.ReProductInfo.get("serial_no") as FormArray
  //   }

  // }

  // convenience getter for easy access to form fields
  get log() {
    return this.RenewCustomerInfo.controls;                       //error logs for create user
  }
  // convenience getter for easy access to form fields
  get log2() {
    return this.ReProductInfo.controls;                       //error logs for create user
  }
  get log1() {
    return this.ReCustomerInfo.controls;                       //error logs for create user
  }
  get log3() {
    return this.RenewProductInfo.controls;                       //error logs for create user
  }

  ngOnInit() {
    this.asideMenus = localStorage.getItem('asideMenus');
    console.log(typeof (JSON.parse(this.asideMenus)));
    console.log(this.router.url);
    this.current_url = this.router.url;
    const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    console.log(result.length)
    if (result.length <= 0) {
      // alert("u cannpot go")
      localStorage.clear();

      this.router.navigate(["/login"]);
    } else {
      // alert("else")
      this.duplicate = '';
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      // this.overlayRef.attach(this.LoaderComponentPortal);
      this.employee_id = localStorage.getItem('employee_id');

      this.form = new FormGroup({
        serial_no: new FormArray([this.initSection()])
      });
    }
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  onchangeserial_no() {
    this.serial_valida_success = '';     //refresh the success message for validating the message after saved the value if any changes in that
  }

  saveserial(serial_no) {
    console.log(serial_no, "saveserial")
    this.arr_serial = [];
    this.serial_valida_success = '';
    this.duplicate = '';
    console.log(valid_serial_no)
    if (this.ReProductInfo.value.product_id == null || this.ReProductInfo.value.product_id == '' || this.ReProductInfo.value.product_sub_id == '' ||
      this.ReProductInfo.value.product_sub_id == null || this.ReProductInfo.value.model_no == '' || this.ReProductInfo.value.model_no == null) {
      this.error = "Please select Product, Sub Product,Model No. Before select Serial no";   //toastr message for success
      this.AddButton = false;
    }
    else {
      this.error = ''
      this.serialnum = [];
      console.log(this.form.value.serial_no, "array")
      for (var j = 0; j < this.form.value.serial_no.length; j++) {
        if (this.form.value.serial_no[j].serial_no == '' || this.form.value.serial_no[j].serial_no == null || this.form.value.serial_no[j].serial_no == undefined) {
          this.AddButton = false;
          this.serial_empty = "please enter serial number"
        }
        else {
          this.serial_empty = '';
          var serial_num = this.form.value.serial_no[j].serial_no
          console.log(serial_num, "serial_num")
          const serial_pattern = /[a-zA-Z0-9\s]+/;
          // const empty_sapce = /^[A-Za-z0-9?,_-](\s?[A-Za-z0-9?,_-]+)*$/;
          // const empty_sapce=serial_num.trim().isEmpty()
          const empty_sapce = /.*[^ ].*/
          console.log(empty_sapce)
          var valid_serial_no = serial_pattern.test(String(serial_num));
          var valid_empty_serial_no = empty_sapce.test(String(serial_num));
          console.log(valid_empty_serial_no)
          console.log(valid_serial_no)
          if (valid_serial_no) {
            console.log("if")
            // this.AddButton = false;
            this.serial_pattern_validation = ''
            if (valid_empty_serial_no) {
              this.valid_empty_serial_no = ''
              console.log(" 2 nd emptyif")
              this.serialnum.push(this.form.value.serial_no[j].serial_no);
              var datas = {
                "customer_code": this.ReCustomerInfo.value.customer_code,
                "product_id": this.ReProductInfo.value.product_id,
                "product_sub_id": this.ReProductInfo.value.product_sub_id,
                "model_no": this.ReProductInfo.value.model_no,
                "serial_no": serial_num,
                "priority": this.ReCustomerInfo.value.priority,
              }
              var url = 'validate_serial_no/'    //api url of add
              var success_response = [];
              var failure_response = [];
              var error_responase = [];
              this.ajax.postdata(url, datas)
                .subscribe((data) => {
                  if (data.response.response_code == "200") {
                    this.serial_validation = '';
                    this.error = '';
                    this.save_validation = '';
                    this.save_validation = '';
                    this.serial_pattern_validation = ''
                    this.valid_empty_serial_no = ''
                    if (this.serial_empty == '' && this.error == '' && this.serial_validation == '' && this.duplicate == '' && this.serial_pattern_validation == '' && this.valid_empty_serial_no == '') {
                      this.AddButton = true;
                      var serial_valida_success_msg = 'Serial number added Successfully';
                      success_response.push(serial_valida_success_msg)
                    }
                  }
                  else if (data.response.response_code == "400" || data.response.response_code == "500") {
                    console.log(serial_num, "serial_num")
                    this.AddButton = false;
                    this.serial_validation = data.response.message + serial_num;
                    failure_response.push(this.serial_validation)

                  }
                  // else if (data.response.response_code == "500") {
                  //   this.AddButton = false;
                  //   this.serial_validation = data.response.message + serial_num;
                  //   success_response.push(this.serial_validation)
                  // }
                  else {
                    this.AddButton = false;
                    this.serial_validation = data.response.message + serial_num;
                    failure_response.push(this.serial_validation)
                  }

                }, (err) => {
                  this.error = "Something went wrong";   //prints if it encounters an error
                });
              console.log(success_response, "success_response")
              console.log(failure_response, "failure_response")
              if (failure_response.length == 0) {
                this.serial_valida_success = 'Serial number added Successfully';
              }
              else {
                this.serial_valida_success = ''
              }
            }
            else {
              // if (serial_valida_success_msg != '') {
              this.AddButton = false;
              // }
              this.valid_empty_serial_no = "Please enter the valid serial number it should be alpha or numberic or alpha numeric";
            }
          }
          else {
            // if (serial_valida_success_msg != '') {
            this.AddButton = false;
            // }
            this.serial_pattern_validation = "Please enter the valid serial number it should be alpha or numberic or alpha numeric"
          }
        }
      }
      console.log(this.serialnum)
    }
    var arr = this.serialnum
    this.arr_serial = arr
    console.log(arr)
    function getDuplicateArrayElements(arr) {
      var sorted_arr = arr.slice().sort();
      var results = [];
      for (var i = 0; i < sorted_arr.length - 1; i++) {
        if (sorted_arr[i + 1] === sorted_arr[i]) {
          results.push(sorted_arr[i]);
        }
      }
      return results;
    }
    var duplicateColors = getDuplicateArrayElements(arr);
    var length = duplicateColors.length;
    if (length != 0) {
      this.AddButton = false;
      // alert("inside condition")
      this.duplicate = "Serial number is already exists";
    }
    else {
      this.duplicate = '';
      this.AddButton = true;
    }
  }

  initSection() {
    return new FormGroup({
      serial_no: new FormControl("")
    });
  }
  initItemRows() {
    return this.formBuilder.group({
      itemname: [""]
    });
  }

  public addPackageRow() {
    this.serial_valida_success = '';
    this.serial_empty = '';
    var i = 0;
    this.AddButton = false
    if (this.ReProductInfo.value.product_id == null || this.ReProductInfo.value.product_id == '' || this.ReProductInfo.value.product_sub_id == '' ||
      this.ReProductInfo.value.product_sub_id == null || this.ReProductInfo.value.model_no == '' || this.ReProductInfo.value.model_no == null) {
      this.error = "Please select Product, Sub Product,Model No. Before select Serial no" //toastr message for success
    }
    else if (this.form.value.serial_no[i].serial_no == '' || this.form.value.serial_no[i].serial_no == null || this.form.value.serial_no[i].serial_no == undefined) {
      this.serial_empty = "Please enter serial number";
    }
    else {
      const control = <FormArray>this.form.get("serial_no");
      control.push(this.initSection());
    }
  }

  initOptions() {
    return new FormGroup({
      optionTitle: new FormControl("")
    });
  }

  addSection() {
    const control = <FormArray>this.form.get("serial_no");
    control.push(this.initSection());
  }

  getSections(form) {
    return form.controls.serial_no.controls;
  }

  public removeSection(i) {
    console.log(i)
    console.log(this.serialnum)
    this.serialnum.splice(i, 1); //delete data from array
    console.log(this.serialnum)
    this.serial_valida_success = '';
    // this.serial_empty='';
    const control = <FormArray>this.form.get("serial_no");
    control.removeAt(i);
  }
  customer_address_validation() {

    if (this.RenewCustomerInfo.valid) {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
  }
  customer_address_validationn() {
    if (this.ReCustomerInfo.valid) {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
  }
  tabBack(preId) {
    this.activeId = this.preId;
    this.preId = this.activeId
  }
  check_validation() {
    this.submitted = true;
    this.activerId = "tab-selectbyid2";
    this.prerId = "tab-selectbyid1";
  }
  product_validation() {
    if (this.RenewProductInfo.valid) {
      this.activeId = "tab-selectbyid3";
      this.preId = "tab-selectbyid2";
    }
  }
  product_validationn() {
    if (this.ReProductInfo.valid && this.serial_empty == '' && this.error == '' && this.serial_validation == '' && this.duplicate == '' && this.serial_valida_success != '') {
      this.save_validation = ''
      this.activeId = "tab-selectbyid3";
      this.preId = "tab-selectbyid2";
    }
    else {
      this.save_validation = "Click save button before next step";
    }
  }

  tabBackR(prerId) {
    this.activerId = this.prerId;
    this.prerId = this.activerId
  }
  tabBackF(prefId) {
    this.activeId = this.prefId
    this.preId = this.activerId
  }

  // function for modal box
  openLarge(raise_ticket) {
    this.getcountry()
    this.getproduct()
    this.get_servicegroup()
    this.get_call_category()
    this.getamc()
    console.log(this.radiochange_value)

    this.ReCustomerInfo.controls['lable_type'].setValue(this.radiochange_value);
    this.RenewCustomerInfo.controls['lable_type'].setValue(this.radiochange_value);
    // this.raise_ticket_reset();
    // console.log(this.RenewCustomerInfo.value.lable_type)
    // console.log(this.ReCustomerInfo.value.lable_type)
    // this.radiochange_value='1'    
    // this.RenewCustomerInfo.controls['lable_type'].setValue(this.radiochange_value);
    // this.ReCustomerInfo.controls['lable_type'].setValue(this.radiochange_value);
    this.contract_error = '';              //refersh the error message
    // this.multiserial_validation = ''
    this.activeId = 'tab-selectbyid1';
    this.modalService.open(raise_ticket, {
      size: 'lg',                                  //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  // opencontract(new_customer)
  // {
  //   this.activeId = 'tab-selectbyid1';
  //   this.modalService.open(new_customer, {
  //     size: 'lg'                                  //specifies the size of the dialog box
  //   });
  // }

  //get country dropdown
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }
  //get state dropdown 
  onChangeCountry(country_id) {
    console.log(country_id, "country_id")
    var country = +country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";

    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;
        //reloading the component
      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  //get city dropdown 
  onChangeState(state_id) {
    console.log(state_id, "state_id")
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                  

      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  onChangecity(cty_id) {
    console.log(cty_id, "cty_id")
    var city = +cty_id; // state: number
    var id = "city_id";
    var url = 'get_location_details/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, city).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.locations = result.response.data;              //storing the api response in the array                  
      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  onchangeradio(event: MatRadioChange) {
    this.radiochange_value = event.value;
  }
  //get amc duration based on amc id
  onchangeContractType(amc_id) {
    this.ReProductInfo.controls['contract_period'].setValue(amc_id);
    this.contrac_type.forEach(element => {
      if (element.amc_id == amc_id) {
        this.RenewProductInfo.controls['contract_period'].setValue(element.duration);
      } else {
      }

    });
  }
  //get product details
  getproduct() {
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.product = result.response.data;                    //storing the api response in the array
      }
    }, (err) => {                                                      //if error
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  //get sub product details based on product id
  onChangeProduct(product_id) {
    var product = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";

    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;                                                //reloading the component

      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  //get work type
  get_servicegroup() {
    var url = 'get_servicegroup/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.work_type = result.response.data;                    //storing the api response in the array

      }
    }, (err) => {                                                      //if error
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }

  //get call category
  get_call_category() {
    var url = 'get_call_category/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.call_category = result.response.data;                    //storing the api response in the array

      }
    }, (err) => {                                                      //if error
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }
  //get all the amc contract type
  getamc() {
    var url = 'get_amc_details/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.contrac_type = result.response.data;

      }
    }, (err) => {
      // this.error="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
    });
  }
  //-------------------------------------Function for image upload------------------------------------//
  //Functions for image cropper
  imageChangedEvent: any = '';
  croppedImage: any = '';
  fileChangeEvent(event: any): void {
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper 
    this.imageChangedEvent = event;
  }
  //image cropping
  // imageCropped(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;

  //   this.croppedImage = event.base64;                   //convert base64
  //   this.RenewProductInfo.controls['image'].setValue(this.imageChangedEvent);         //set the base64 in create product image

  // }

  index: any;
  //onchange function for getting the customer code
  passcustomercode(customer_code) {
    this.customer_code = customer_code

  }
  //onchange function for getting the contact number
  passcontact_no(contact_number) {
    this.contact_number = contact_number

  }
  //function for get all customer and contract details based on contact or customer
  get_customer_contract_details(contact_customer_value, type_value: any) {
    console.log(contact_customer_value, "contact_customer_value")
    console.log(type_value, "type_value")

    if (type_value == 1 && contact_customer_value != undefined) {
      // alert("if")
      this.contact_no = contact_customer_value.length;
    }
    else {
      // alert("else")
      this.contact_no = 10;
    }
    if (contact_customer_value == '' || contact_customer_value == null || this.contact_no < 10) {
      Swal.fire({
        title: 'Oops!',
        text: 'Please provide valid phone number or customer id',
        icon: 'info',
        confirmButtonText: 'ok',
        allowOutsideClick: false,   //alert outside click by sowndarya
      })
    }
    else {
      // alert("taking contact_number")
      console.log(this.customer_code, "this.customer_code")
      console.log(this.contact_number, "this.contact_number")
      if ((this.customer_code === undefined && type_value == 1) || this.customer_code == '') {

        // if ((this.customer_code === undefined && type_value == 1)) {
        // if (this.customer_code == '') {
        //   // if (this.customer_details.customer_code !== '' && type_value == 2) {
        //   // console.log(this.customer_details.contact_number)
        //   var contact_number = this.customer_details.contact_number;
        // }
        // else {
        var contact_number = this.contact_number;
        // }
        var id = "contact_number";
        var parameter = contact_number;
      }
      else if ((this.contact_number === undefined && type_value == 2) || (type_value == 2)) {
        // alert("taking customer_code")

        if (this.customer_code !== '' && type_value !== '') {
          var customer_code = this.customer_code;
        }

        else if (this.customer_details.customer_code !== '' && type_value == 2) {
          var customer_code = this.customer_details.customer_code;
        }
        var id = "customer_code";
        var parameter = customer_code;
      }
      this.overlayRef.attach(this.LoaderComponentPortal);
      var url = 'get_amc_contract_details/?';
      this.ajax.getdataparam(url, id, parameter).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.customer_details = result.response.customer_details;
          this.contract_details = result.response.contract_details[0];
          this.contract_detail = result.response.contract_details;
          this.contact_number = this.customer_details.contact_number;
          // this.customer_code = this.customer_details.ticket_id;
          this.isShow = false;
          this.get_expiredate(this.contract_detail)
          this.RenewCustomerInfo.patchValue({
            'customer_code': this.customer_details.customer_code,
            'ticket_id': this.customer_details.ticket_id,
            'customer_name': this.customer_details.customer_name,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': this.customer_details.plot_number,
            'street': this.customer_details.street,
            'landmark': this.customer_details.landmark,
            'post_code': this.customer_details.post_code,
            'country_id': this.customer_details.country.country_id,
            'state_id': this.customer_details.state.state_id,
            'city_id': this.customer_details.city.city_id,
            'location_id': this.customer_details.location.location_id,
            'contract_id': this.contract_details.contract_id,
          })
          this.RenewProductInfo.patchValue({
            'product_id': this.contract_details.product.product_id,
            'contract_type': this.contract_details.contract.amc_id,
            'cust_preference_date': this.model,
          })
          this.onChangeProduct(this.contract_details.product.product_id);
          this.ReCustomerInfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_number': this.customer_details.alternate_number,
            'plot_number': this.customer_details.plot_number,
            'street': this.customer_details.street,
            'landmark': this.customer_details.landmark,
            'post_code': this.customer_details.post_code,
            'country_id': this.customer_details.country.country_id,
            'state_id': this.customer_details.state.state_id,
            'city_id': this.customer_details.city.city_id,
            'location_id': this.customer_details.location.location_id
          })
          this.getcountry();
          this.onChangeCountry(this.customer_details.country.country_id);
          this.onChangeState(this.customer_details.state.state_id);
          this.onChangecity(this.customer_details.city.city_id);
          this.overlayRef.detach();
        }
        else if (result.response.response_code == "404") {
          this.customer_details = result.response.customer_details;
          // this.contract_details = result.response.contract_details[0];
          var contract_detail = result.response.contract_details;
          console.log(result.response.contract_details, "contract_details")
          if (contract_detail.length == '0') {
            this.errors = "There is no Contract for this customer";
            this.RenewCustomerInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_contact_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.ReCustomerInfo.patchValue({
              'customer_id': this.customer_details.customer_id,
              'customer_code': this.customer_details.customer_code,
              'customer_name': this.customer_details.customer_name,
              'ticket_id': this.customer_details.ticket_id,
              'email_id': this.customer_details.email_id,
              'contact_number': this.customer_details.contact_number,
              'alternate_number': this.customer_details.alternate_number,
              'plot_number': '',
              'street': '',
              'landmark': '',
              'post_code': '',
              'country_id': '',
              'state_id': '',
              'city_id': '',
              'location_id': ''
            })
            this.overlayRef.detach();

          }
          else {
            this.error = ''
            this.isShow = false;
            this.RenewProductInfo.patchValue({
              'product_id': this.contract_details.product.product_id,
              'contract_type': this.contract_details.contract.amc_id,
              'cust_preference_date': this.model,
            })
            this.overlayRef.detach();
            // this.chRef.detectChanges();
          }
        }
        else if (result.response.response_code == "500") {
          this.amc_ticket_reset();
          var default_radiovalue = '1';
          this.radiochange_value = '1';
          this.RenewCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
          this.ReCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
          this.overlayRef.detach();
          Swal.fire(
            'Sorry!',
            'No Details Found',
            'error'
          )
        } (err) => {
          // this.customer_error="Something went wrong,Please check your Internet connnection";//prints if it encounters an error
          this.overlayRef.detach();
        }
      });
    }
  }
  //change the expire date as text if date is less than today
  get_expiredate(contract_data) {
    contract_data.forEach((element) => {
      var today_date = this.calendar.getToday()
      var year = today_date.year;
      var month = today_date.month;
      var day = today_date.day;
      this.firstDate = year + "-" + month + "-" + day;
      this.secondDate = element.expiry_day;
      if (new Date(this.firstDate).getTime() > new Date(this.secondDate).getTime()) {
        element.expiry_day = "Contract Expired";

      }
      else {
        element.expiry_day = element.expiry_day

      }
    })
  }
  //Function for getting the customer,product details based on the contract id
  get_contract_info(contrat_id, type: any) {
    console.log(contrat_id)
    var contract_id = +contrat_id;
    var url = 'get_contract_info/?';             // api url for getting the details with using post params
    var id = "contract_id";                        //id for making url
    this.ajax.getdataparam(url, id, contract_id).subscribe((result) => {
      if (result.response.response_code == "200") {                //if sucess
        this.contract_error = '';
        this.raiseticket_details = result.response.data;
        console.log(this.raiseticket_details, " this.raiseticket_details")
        this.RenewCustomerInfo.patchValue({
          'contract_id': this.raiseticket_details.contract_id,
          'customer_code': this.raiseticket_details.customer_code,
          'customer_name': this.raiseticket_details.customer_name,
          'ticket_id': this.raiseticket_details.ticket_id,
          'email_id': this.raiseticket_details.email_id,
          'contact_number': this.raiseticket_details.contact_number,
          'alternate_contact_number': this.raiseticket_details.alternate_number,
          'plot_number': this.raiseticket_details.plot_number,
          'street': this.raiseticket_details.street,
          'landmark': this.raiseticket_details.landmark,
          'post_code': this.raiseticket_details.post_code,
          'country_id': this.raiseticket_details.country.country_id,
          'state_id': this.raiseticket_details.state.state_id,
          'city_id': this.raiseticket_details.city.city_id,
          'location_id': this.raiseticket_details.location.location_id,
        })
        this.RenewProductInfo.patchValue({
          'product_id': this.raiseticket_details.product.product_id,
          'product_sub_id': this.raiseticket_details.sub_product.product_sub_id,
          'model_no': this.raiseticket_details.model_no,
          'serial_no': this.raiseticket_details.serial_no,
          'contract_type': this.raiseticket_details.contract.contract_type_id,
          'contract_period': this.raiseticket_details.contract.contract_period,
          'cust_preference_date': this.raiseticket_details.expiry_day,
        })
        this.onChangeCountry(this.raiseticket_details.country.country_id);
        this.onChangeState(this.raiseticket_details.state.state_id);
        this.onChangecity(this.raiseticket_details.city.city_id)
        this.onChangeProduct(this.raiseticket_details.product.product_id);
        this.onchangeContractType(this.raiseticket_details.contract.contract_type_id)
      }
      else if (result.response.response_code == "400") {                //if failure
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      }
      else if (result.response.response_code == "500") {                //if failure
        this.contract_error = result.response.message         //toastr message for error
      }
      else {                                                            //if not sucess
        this.contract_error = result.response.message       //toastr message for error
      }
    }, (err) => {                                                       //if error
      // this.contract_error="Something went wrong,Please check your Internet connnection";                                                 //prints if it encounters an error
    });


  }
  //Function to reset the raiseticket field value
  amc_ticket_reset() {
    this.RenewCustomerInfo.reset();
    this.ReCustomerInfo.reset();
    this.ReProductInfo.reset();
    this.contact_number = '';
    this.customer_code = '';
    this.errors = '';
    this.radiochange_value = '1'
    this.ReCustomerInfo.controls['lable_type'].setValue(this.radiochange_value);
    this.RenewCustomerInfo.controls['lable_type'].setValue(this.radiochange_value); // this.radiochange_value
    this.isShow = true;
  }
  //comment

  //raising the ticket
  renewal_ticket() {
    console.log(this.RenewProductInfo.value, "this.RenewProductInfo.value")
    this.submitted = true;
    var year = this.RenewProductInfo.value.cust_preference_date.year;
    var month = this.RenewProductInfo.value.cust_preference_date.month;
    var day = this.RenewProductInfo.value.cust_preference_date.day;
    var dateFormated = year + "-" + month + "-" + day;
    var location_id = this.raiseticket_details.location.location_id;
    this.RenewProductInfo.value.image = this.imageChangedEvent

    this.RenewProductInfo.value.cust_preference_date = dateFormated;
    this.RenewCustomerInfo.value.location_id = location_id;
    this.RenewCustomerInfo.value.employee_id = this.employee_id;


    if (this.RenewProductInfo.invalid) {
      this.toastr.error("All fields must be valid and required2", 'Error'); //toastr message for error
    }
    else {

      this.form = {
        "ticket_id": this.RenewCustomerInfo.value.ticket_id,
        "contract_id": this.RenewCustomerInfo.value.contract_id,
        "alternate_contact_number": this.RenewCustomerInfo.value.alternate_contact_number,
        "city_id": this.RenewCustomerInfo.value.city_id,
        "contact_number": this.RenewCustomerInfo.value.contact_number,
        "country_id": this.RenewCustomerInfo.value.country_id,
        "customer_code": this.RenewCustomerInfo.value.customer_code,
        "customer_id": this.RenewCustomerInfo.value.customer_id,
        "customer_name": this.RenewCustomerInfo.value.customer_name,
        "email_id": this.RenewCustomerInfo.value.email_id,
        "landmark": this.RenewCustomerInfo.value.landmark,
        "location_id": +this.RenewCustomerInfo.value.location_id,
        "plot_number": this.RenewCustomerInfo.value.plot_number,
        "post_code": this.RenewCustomerInfo.value.post_code,
        "state_id": this.RenewCustomerInfo.value.state_id,
        "street": this.RenewCustomerInfo.value.street,
        "contract_period": +this.RenewProductInfo.value.contract_period,
        "contract_type": this.RenewProductInfo.value.contract_type,
        "cust_preference_date": this.RenewProductInfo.value.cust_preference_date,
        "model_no": this.RenewProductInfo.value.model_no,
        "product_id": this.RenewProductInfo.value.product_id,
        "product_sub_id": this.RenewProductInfo.value.product_sub_id,
        "quantity": this.RenewProductInfo.value.quantity,
        "serial_no": this.RenewProductInfo.value.serial_no,
        "work_type_id": this.RenewProductInfo.value.work_type_id,
      }
      var url = 'renew_contract/'                                           //api url of edit api                                    
      this.ajax.postdata(url, this.form)
        .subscribe((result) => {
          if (result.response.response_code == "200")                   //if sucess
          {

            this.amc_ticket_reset();
            var default_radiovalue = '1';
            this.RenewCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
            this.ReCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
            this.radiochange_value = "1";
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.ngOnInit();
            this.modalService.dismissAll();                                //to close the modal box     

          }
          else if (result.response.response_code == "400") {                 //if failiure
            this.renew_error = result.response.message           //toastr message for error
          }
          else if (result.response.response_code == "500") {                                                            //if not sucess
            this.renew_error = result.response.message   //toastr message for error
          }
          else {
            this.renew_error = "Something went wrong"      //toastr message for error

          }
        }, (err) => {
          // this.renew_error="Something went wrong,Please check your Internet connnection";                                                 //prints if it encounters an error
        });
    }
  }

  create_new_customer() {

    var year = this.ReProductInfo.value.cust_preference_date.year;
    var month = this.ReProductInfo.value.cust_preference_date.month;
    var day = this.ReProductInfo.value.cust_preference_date.day;
    var dateFormated = year + "-" + month + "-" + day;

    this.ReProductInfo.value.cust_preference_date = dateFormated;

    var serial_number = this.form.get('serial_no') as FormArray
    var arr = this.serialnum
    function getDuplicateArrayElements(arr) {
      var sorted_arr = arr.slice().sort();
      var results = [];
      for (var i = 0; i < sorted_arr.length - 1; i++) {
        if (sorted_arr[i + 1] === sorted_arr[i]) {
          results.push(sorted_arr[i]);
        }
      }
      return results;
    }

    var duplicateColors = getDuplicateArrayElements(this.serialnum);
    console.log(this.serialnum, " Submit this.serialnum")
    this.forms = {
      "employee_id": this.employee_id,
      "alternate_number": this.ReCustomerInfo.value.alternate_number,
      "city_id": this.ReCustomerInfo.value.city_id,
      "contact_number": this.ReCustomerInfo.value.contact_number,
      "country_id": this.ReCustomerInfo.value.country_id,
      "customer_code": this.ReCustomerInfo.value.customer_code,
      "customer_id": this.ReCustomerInfo.value.customer_id,
      "customer_name": this.ReCustomerInfo.value.customer_name,
      "email_id": this.ReCustomerInfo.value.email_id,
      "landmark": this.ReCustomerInfo.value.landmark,
      "location_id": +this.ReCustomerInfo.value.location_id,
      "plot_number": this.ReCustomerInfo.value.plot_number,
      "post_code": this.ReCustomerInfo.value.post_code,
      "state_id": this.ReCustomerInfo.value.state_id,
      "street": this.ReCustomerInfo.value.street,
      "ticket_id": this.ReCustomerInfo.value.ticket_id,
      "contract_period": +this.ReProductInfo.value.contract_period,
      "contract_type": this.ReProductInfo.value.contract_type,
      "cust_preference_date": this.ReProductInfo.value.cust_preference_date,
      "model_no": this.ReProductInfo.value.model_no,
      "product_id": this.ReProductInfo.value.product_id,
      "product_sub_id": this.ReProductInfo.value.product_sub_id,
      "serial_no": this.serialnum,
      "priority": this.ReProductInfo.value.priority,
    }
    var data = this.forms
    this.loadingSub = true;
    var url = 'add_amc_contract/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                   //if sucess
        {
          this.amc_ticket_reset();
          this.modalService.dismissAll();
          var default_radiovalue = '1';
          this.RenewCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
          this.ReCustomerInfo.controls['lable_type'].setValue(default_radiovalue);
          this.radiochange_value = '1;'
          this.serial_valida_success = '';
          if (this.serialnum.length > 1) {
            // this.multiserial_validation = ''
            var i: number;
            i = 0;
            for (i = this.serialnum.length; i >= 1; i--) {
              this.removeSection(i);
            }
          }
          this.ngOnInit();
          // this.form.reset();              //reset the value from the multiple serial number array
          // this.createEventForm();
          this.arr_serial = [];    //refresh the multiple serial number
          this.toastr.success(result.response.Message, 'Success');        //toastr message for success
          this.loadingSub = false;
        }
        else if (result.response.response_code == "500") {                 //if failiure
          this.api_response = result.response.Message            //toastr message for error
          this.loadingSub = false;
        }
        else {                                                             //if not sucess
          this.api_response = result.response.Message       //toastr message for error
          this.loadingSub = false;
        }
      }, (err) => {
        // this.api_response="Something went wrong,Please check your Internet connnection";   //prints if it encounters an error
      });
  }

  fetchNews(evt: any) {
    this.activeId = evt.nextId;
    this.evt = evt; // has nextId that you can check to invoke the desired function
  }

}


