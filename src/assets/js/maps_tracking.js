﻿jQuery(document).ready(function ($) {
    'use strict';
    // var routeSelect = document.getElementById('routeSelect');
    var routeSelect = document.getElementById('routeSelect');
    var map = new google.maps.Map(document.getElementById('map-canvas'));
    var map = document.getElementById('map-canvas');
    var autoRefresh = false;
    var intervalID = 0;
    var sessionIDArray = [];
    var viewingAllRoutes = false;
    var routeSelect = document.getElementById('routeSelect');
    routeSelect.selectedIndex = 0;
    console.log(routeSelect);
    getAllRoutesForMap();
    loadRoutesIntoDropdownBox();

    $("#routeSelect").change(function () {
        console.log(routeSelect.selectedIndex)
        getRouteForMap();
        console.log('inside of routeselect onchange');
        if (hasMap()) {
            console.log('has inside of routeselect onchange');
            viewingAllRoutes = false;

            getRouteForMap();
        }
    });

    $("#refresh").click(function () {
        if (viewingAllRoutes) {
            getAllRoutesForMap();
        } else {
            if (hasMap()) {
                getRouteForMap();
            }
        }
    });

    $("#delete").click(function () {
        deleteRoute();
    });

    $('#autorefresh').click(function () {
        if (autoRefresh) {
            turnOffAutoRefresh();
        } else {
            turnOnAutoRefresh();
        }
    });

    $("#viewall").click(function () {
        getAllRoutesForMap();
    });

    function getAllRoutesForMap() {
        var data = {
            "employee_code": localStorage.getItem('employee_code')
        }
        // var data={ "locations": [{ "latitude":"13.0808080", "longitude":"80.2476000", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Kavin", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba6aa", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsaaa", "accuracy":"74", "extraInfo":"Purasawalkam","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0186110", "longitude":"80.1688970", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Manoj", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba59c", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsdfg", "accuracy":"74", "extraInfo":"Alandur","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0090990", "longitude":"80.2119950", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Ravi", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba5ed", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsdyy", "accuracy":"74", "extraInfo":"Guindy ","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0737770", "longitude":"80.1943410", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Rahul", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba5fg", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsfff", "accuracy":"74", "extraInfo":"Aminjikkarai","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0391640", "longitude":"80.2181980", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Gokul", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba6dd", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsgdd", "accuracy":"74", "extraInfo":"Mambalam","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0763230", "longitude":"80.2558630", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Arun", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba6tg", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgsggg", "accuracy":"74", "extraInfo":"Egmore","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0259490", "longitude":"80.1588410", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Naveen", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba6jj", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgslll", "accuracy":"74", "extraInfo":"Madhuravoyal","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0410270", "longitude":"80.2652940", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Ramesh", "phoneNumber":"a277c95f-8df8-44a0-94a0-615ea28ba4rr", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92asdgswww", "accuracy":"74", "extraInfo":"Mylapore","eventType":"Kandanchavadi Chennai" },{ "latitude":"13.0119920", "longitude":"80.2215070", "speed":"0", "direction":"0", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:23AM", "userName":"Rakesh", "phoneNumber":"a277c95f-8df8-44a0-94a0-6ert354ba4rr", "sessionID":"9b0d45c0-66f8-4689-b2bc-b92aser345ww", "accuracy":"74", "extraInfo":"Guindy","eventType":"Kandanchavadi Chennai" },{ "latitude":"12.9718243", "longitude":"80.2490452", "speed":"0", "direction":"321", "distance":"0.1", "locationMethod":"Free", "gpsTime":"Nov 12 2019 06:06PM", "userName":"Mahendran", "phoneNumber":"6721ced1-eeb0-413f-87ef-170da2e5bd2a", "sessionID":"9fa86970-298e-41f7-a622-6c8ad457f790", "accuracy":"81", "extraInfo":"Kaspon Techworks","eventType":"Adyar" },{ "latitude":"13.0006423", "longitude":"80.2132615", "speed":"0", "direction":"204", "distance":"0.0", "locationMethod":"Free", "gpsTime":"Nov 14 2019 08:13PM", "userName":"ramkumar", "phoneNumber":"3fb01596-d74e-4d45-9fe4-6057217f9fa8", "sessionID":"924043f4-2794-4c0d-a56c-37814f4b36e9", "accuracy":"82", "extraInfo":"Maduvankarai ","eventType":"android" },{ "latitude":"12.9717650", "longitude":"80.2489994", "speed":"0", "direction":"0", "distance":"0.0", "locationMethod":"Free", "gpsTime":"Feb 4 2020 11:47AM", "userName":"Rajesh", "phoneNumber":"5edbea5c-a95c-473b-87e4-9c3d1fe2def8", "sessionID":"3502d96c-9921-48ad-aef7-e66d0c837b79", "accuracy":"72", "extraInfo":"-286","eventType":"android" },{ "latitude":"12.9878141", "longitude":"77.7319763", "speed":"0", "direction":"0", "distance":"0.0", "locationMethod":"Free", "gpsTime":"Sep 7 2020 12:53PM", "userName":"Babu", "phoneNumber":"695b3776-3c9a-4413-9f47-2ddf0608437a", "sessionID":"1374efd8-9120-4a36-9969-9cc6aaea6e80", "accuracy":"69", "extraInfo":"2665","eventType":"android" }] }
        // loadGPSLocations(data)
        // viewingAllRoutes = true;
        // // routeSelect.selectedIndex = 0;
        // showPermanentMessage('Please select a route below');


        $.ajax({
            url: 'https://fieldpro.kaspontech.com/djadmin/get_all_technician_tracking/',
            // url: 'https://fieldpro.kaspontech.com/djadmin_qa/get_all_technician_tracking/',
            type: 'post',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (data) {
                console.log(data);
                loadGPSLocations(data.response.data);
                loadRoutes(data.response);
            },
            error: function (xhr, status, errorThrown) {
                console.log("error status: " + xhr.status);
                console.log("errorThrown: " + errorThrown);
            }
        });
    }

    function loadRoutesIntoDropdownBox() {
        var data = {
            "technician_code": ""
        }
        // var data={ "routes": [{ "sessionID": "1374efd8-9120-4a36-9969-9cc6aaea6e80", "userName": "Babu", "times": "(Sep 7 2020 12:53PM - Sep 7 2020 12:53PM)" },{ "sessionID": "3502d96c-9921-48ad-aef7-e66d0c837b79", "userName": "Rajesh", "times": "(Feb 4 2020 11:47AM - Feb 4 2020 11:47AM)" },{ "sessionID": "924043f4-2794-4c0d-a56c-37814f4b36e9", "userName": "ramkumar", "times": "(Nov 14 2019 08:13PM - Nov 14 2019 08:13PM)" },{ "sessionID": "9fa86970-298e-41f7-a622-6c8ad457f790", "userName": "Mahendran", "times": "(Nov 12 2019 06:06PM - Nov 12 2019 06:06PM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsaaa", "userName": "Kavin", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsggg", "userName": "Arun", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsfff", "userName": "Rahul", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgswww", "userName": "Ramesh", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsgdd", "userName": "Gokul", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgslll", "userName": "Naveen", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsdyy", "userName": "Ravi", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsdfg", "userName": "Manoj", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92aser345ww", "userName": "Rakesh", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" }] };
        // loadRoutes(data);
        $.ajax({
            url: 'https://fieldpro.kaspontech.com/djadmin/technician_tracking_location/',
            // url: 'https://fieldpro.kaspontech.com/djadmin_qa/technician_tracking_location/',
            type: 'post',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (data) {

                // data= { "routes": [{ "sessionID": "3e578e9b-e1b3-4007-b7a0-d983b8e4f541", "userName": "Pradeep ", "times": "(Jan 29 2021 12:03PM - Jan 29 2021 12:03PM)" },{ "sessionID": "b8540812-1d5e-4708-8240-ea3c6bd53b21", "userName": "Senthil", "times": "(Nov 23 2020 01:06PM - Nov 23 2020 01:06PM)" },{ "sessionID": "1374efd8-9120-4a36-9969-9cc6aaea6e80", "userName": "Babu", "times": "(Sep 7 2020 12:53PM - Sep 7 2020 12:53PM)" },{ "sessionID": "3502d96c-9921-48ad-aef7-e66d0c837b79", "userName": "Rajesh", "times": "(Feb 4 2020 11:47AM - Feb 4 2020 11:47AM)" },{ "sessionID": "924043f4-2794-4c0d-a56c-37814f4b36e9", "userName": "ramkumar", "times": "(Nov 14 2019 08:13PM - Nov 14 2019 08:13PM)" },{ "sessionID": "9fa86970-298e-41f7-a622-6c8ad457f790", "userName": "Mahendran", "times": "(Nov 12 2019 06:06PM - Nov 12 2019 06:06PM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgswww", "userName": "Ramesh", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsgdd", "userName": "Gokul", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgslll", "userName": "Naveen", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsdyy", "userName": "Ravi", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsdfg", "userName": "Manoj", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92aser345ww", "userName": "Rakesh", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsaaa", "userName": "Kavin", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsggg", "userName": "Arun", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" },{ "sessionID": "9b0d45c0-66f8-4689-b2bc-b92asdgsfff", "userName": "Rahul", "times": "(Nov 12 2019 06:23AM - Nov 12 2019 06:23AM)" }] }
                // loadRoutes(data.response.data);
            },
            error: function (xhr, status, errorThrown) {
                console.log("status: " + xhr.status);
                console.log("errorThrown: " + errorThrown);
            }
        });
    }

    function loadRoutes(json) {
        console.log(json);
        if (json.length == 0) {
            showPermanentMessage('There are no routes available to view');
        }
        else {
            // create the first option of the dropdown box
            // $('#routeSelect').reset()
            var option = document.createElement('option');
            option.setAttribute('value', '0');
            routeSelect.innerHTML = '';
            option.innerHTML = 'Select Route...';
            routeSelect.appendChild(option);

            // when a user taps on a marker, the position of the sessionID in this array is the position of the route
            // in the dropdown box. it's used below to set the index of the dropdown box when the map is changed
            sessionIDArray = [];
            console.log(json);
            // iterate through the routes and load them into the dropdwon box.
            $(json.data).each(function (key, value) {
                // console.log(value);
                var option = document.createElement('option');
                option.setAttribute('value', $(this).attr('technician_code'));

                sessionIDArray.push($(this).attr('gps_location_id'));

                // option.innerHTML = $(this).attr('technician_code') + " " + $(this).attr('gps_time');
                option.innerHTML = $(this).attr('technician_name') + "(" + $(this).attr('technician_code') + ")";

                routeSelect.appendChild(option);
            });
            console.log(option);
            // need to reset this for firefox
            routeSelect.selectedIndex = 0;

            showPermanentMessage('Please select a route below');
        }
    }

    function getRouteForMap() {
        console.log('inside of routeselect function');
        var value = $('#routeSelect').val();
        console.log(value);

        // if (hasMap()) {
        console.log($("#routeSelect").prop("selectedIndex"));

        var url = 'https://fieldpro.kaspontech.com/djadmin/technician_tracking_location/';
        // var url = 'https://fieldpro.kaspontech.com/djadmin_qa/technician_tracking_location/';
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify({ 'technician_code': $('#routeSelect').val() }),
            dataType: 'json',
            success: function (data) {
                console.log(data.response.data)
                loadGPSLocations(data.response.data);
            },
            error: function (xhr, status, errorThrown) {
                console.log("status: " + xhr.status);
                console.log("errorThrown: " + errorThrown);
            }
        });

        // }
    }

    function loadGPSLocations(json) {
        // console.log(JSON.stringify(json));
        console.log(json);

        // var directionsDisplay;
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var directionsService = new google.maps.DirectionsService();
        //var map;
        if (json.length == 0) {
            showPermanentMessage('There is no tracking data to view');
            map.innerHTML = '';
        }
        else {
            if (map.id == 'map-canvas') {
                // clear any old map objects
                document.getElementById('map-canvas').outerHTML = "<div id='map-canvas'></div>";

                // use leaflet (http://leafletjs.com/) to create our map and map layers
                var gpsTrackerMap = new L.map('map-canvas');

                var openStreetMapsURL = ('https:' == document.location.protocol ? 'https://' : 'http://') +
                    '{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
                var openStreetMapsLayer = new L.TileLayer(openStreetMapsURL,
                    { attribution: '&copy;2014 <a href="http://openstreetmap.org">OpenStreetMap</a> contributors' });

                // need to get your own bing maps key, http://www.microsoft.com/maps/create-a-bing-maps-key.aspx
                // var bingMapsLayer = new L.BingLayer("GetAKey");
                var googleMapsLayer = new L.Google('ROADMAP');

                // this fixes the zoom buttons from freezing
                // https://github.com/shramov/leaflet-plugins/issues/62
                L.polyline([[0, 0],]).addTo(gpsTrackerMap);

                // this sets which map layer will first be displayed
                gpsTrackerMap.addLayer(googleMapsLayer);

                // this is the switcher control to switch between map types
                gpsTrackerMap.addControl(new L.Control.Layers({
                    // 'Bing Maps':bingMapsLayer,
                    'Google Maps': googleMapsLayer,
                    'OpenStreetMaps': openStreetMapsLayer
                }, {}));
            }

            var finalLocation = false;
            var counter = 0;
            var locationArray = [];

            // iterate through the locations and create map markers for each location
            $(json).each(function (key, value) {

                var latitude = $(this).attr('latitude');
                var longitude = $(this).attr('longitude');
                var tempLocation = new L.LatLng(latitude, longitude);

                locationArray.push(tempLocation);
                counter++;

                // want to set the map center on the last location
                if (counter == $(json).length) {
                    //gpsTrackerMap.setView(tempLocation, zoom); if using fixed zoom
                    finalLocation = true;
                    if (!viewingAllRoutes) {
                        displayCityName(latitude, longitude);
                    }
                }
                console.log(longitude);
                var marker = createMarker(
                    latitude,
                    longitude,
                    $(this).attr('speed'),
                    $(this).attr('direction'),
                    $(this).attr('distance'),
                    $(this).attr('Current_status'),
                    $(this).attr('Current_location'),
                    $(this).attr('technician_name'),
                    $(this).attr('gps_time'),

                    $(this).attr('accuracy'),
                    $(this).attr('extraInfo'),
                    gpsTrackerMap, finalLocation);
                // console.log(marker);

            });


            // fit markers within window
            var bounds = new L.LatLngBounds(locationArray);
            gpsTrackerMap.fitBounds(bounds);

            // Draw the route
            var start = new google.maps.LatLng(12.9719182, 80.2465589);
            //var end = new google.maps.LatLng(38.334818, -181.884886);
            var end = new google.maps.LatLng(13.0059592, 80.2375489);
            var request = {
                origin: start,
                destination: end,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(gpsTrackerMap);
                } else {
                    alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                }
            });
            //////////////////////////////////////////////////////////////////////

            // restarting interval here in case we are coming from viewing all routes
            if (autoRefresh) {
                restartInterval();
            }
        }
    }
    function binddata(data) {
        console.log('inside of bind data');
        localStorage.setItem("latitude", data.latlng.lat);
        localStorage.setItem("longitude", data.latlng.lon);
        console.log(data);
    }
    function createMarker(latitude, longitude, speed, direction, distance, locationMethod, gpsTime,
        userName, sessionID, accuracy, extraInfo, map, finalLocation) {
        var iconUrl;
        console.log(latitude, longitude, speed, direction, distance, locationMethod, gpsTime,
            userName, sessionID, accuracy, extraInfo, map, finalLocation)
        if (finalLocation) {
            iconUrl = 'assets/images/coolred_small.png';
        } else {
            iconUrl = 'assets/images/coolgreen2_small.png';
        }
        var markerIcon = new L.Icon({
            iconUrl: iconUrl,
            shadowUrl: 'assets/images/coolshadow_small.png',
            iconSize: [12, 20],
            shadowSize: [22, 20],
            iconAnchor: [6, 20],
            shadowAnchor: [6, 20],
            popupAnchor: [-3, -25]
        });

        var lastMarker = "</td></tr>";

        // when a user clicks on last marker, let them know it's final one
        if (finalLocation) {
            // lastMarker = "</td></tr><tr><td align=left>&nbsp;</td><td><b>Final location</b></td></tr>";
            lastMarker = "";

        }

        // convert from meters to feet
        accuracy = parseInt(accuracy * 3.28);
        var compImg = '/assets/images/';
        var link = $('<a href="/manager_livetracking" target="_blank">Track</a>').click(function () {
            alert("test");
        })[0];
        var popupWindowText = "<table border=0 style=\"font-size:95%;font-family:arial,helvetica,sans-serif;color:#000;\">" +
            "<tr><td align=right>Technician Name:&nbsp;</td><td>" + userName + "</td><td>&nbsp;</td></tr>" +
            "<tr><td align=right>Location:&nbsp;</td><td colspan=2>" + gpsTime + "</td></tr>" +
            "<tr><td align=right>Status:&nbsp;</td><td>" + locationMethod + "</td><td>&nbsp;</td></tr><tr><td align=right>&nbsp;</td><td>&nbsp;</td></tr></table>";
        // var popupWindowText = "<table border=0 style=\"font-size:95%;font-family:arial,helvetica,sans-serif;color:#000;\">" +
        // "<tr><td align=right>&nbsp;</td><td>&nbsp;</td><td rowspan=2 align=right>" +
        // "<img src="+compImg+ getCompassImage(direction) + ".jpg alt= />" + lastMarker +
        // "<tr><td align=right>Name:&nbsp;</td><td>" + userName + "</td><td>&nbsp;</td></tr>" +
        // "<tr><td align=right>Location:&nbsp;</td><td colspan=2>" + gpsTime + "</td></tr>" +
        // "<tr><td align=right>Status:&nbsp;</td><td>" + locationMethod + "</td><td>&nbsp;</td></tr><tr><td align=right><a href="+link+" target='_blank'>Track</a>&nbsp;</td><td>&nbsp;</td></tr></table>";
        console.log('create marker');

        // console.log(popupWindowText);
        var gpstrackerMarker;
        var title = userName + " - " + gpsTime;
        // var popup = L.popup()
        // .setLatLng(latitude)
        // .setContent('<p>Hello world!<br />This is a nice popup.</p>')
        // .openOn(map);
        // L.marker.bindPopup(popup).openPopup();

        // make sure the final red marker always displays on top
        if (finalLocation) {
            gpstrackerMarker = new L.marker(new L.LatLng(latitude, longitude), { title: title, icon: markerIcon, zIndexOffset: 999 }).bindPopup(popupWindowText).addTo(map).on("click", binddata);
        } else {
            gpstrackerMarker = new L.marker(new L.LatLng(latitude, longitude), { title: title, icon: markerIcon }).bindPopup(popupWindowText).addTo(map).on("click", binddata);
        }
        // google.maps.event.addListener(marker, "click", function(event){
        // that.onVehiculeClick(event)
        // });
        // if we are viewing all routes, we want to go to a route when a user taps on a marker instead of displaying popupWindow
        if (viewingAllRoutes) {
            gpstrackerMarker.unbindPopup();

            gpstrackerMarker.on("click", function () {
                var url = 'https://fieldpro.kaspontech.com/djadmin/get_route/?sessionid=' + sessionID;
                // var url = 'https://fieldpro.kaspontech.com/djadmin_qa/get_route/?sessionid=' + sessionID;

                viewingAllRoutes = false;

                var indexOfRouteInRouteSelectDropdwon = sessionIDArray.indexOf(sessionID) + 1;
                routeSelect.selectedIndex = indexOfRouteInRouteSelectDropdwon;

                if (autoRefresh) {
                    restartInterval();
                }

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        loadGPSLocations(data);
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log("status: " + xhr.status);
                        console.log("errorThrown: " + errorThrown);
                    }
                });
            }); // on click
        }
    }

    function getCompassImage(azimuth) {
        if ((azimuth >= 337 && azimuth <= 360) || (azimuth >= 0 && azimuth < 23))
            return "compassN";
        if (azimuth >= 23 && azimuth < 68)
            return "compassNE";
        if (azimuth >= 68 && azimuth < 113)
            return "compassE";
        if (azimuth >= 113 && azimuth < 158)
            return "compassSE";
        if (azimuth >= 158 && azimuth < 203)
            return "compassS";
        if (azimuth >= 203 && azimuth < 248)
            return "compassSW";
        if (azimuth >= 248 && azimuth < 293)
            return "compassW";
        if (azimuth >= 293 && azimuth < 337)
            return "compassNW";

        return "";
    }

    // check to see if we have a map loaded, don't want to autorefresh or delete without it
    function hasMap() {
        // if (routeSelect.maps.js == 0) { // means no map
        // return false;
        // }
        // else {
        // return true;
        // }
    }

    function displayCityName(latitude, longitude) {
        var lat = parseFloat(latitude);
        var lng = parseFloat(longitude);
        var latlng = new google.maps.LatLng(lat, lng);
        var reverseGeocoder = new google.maps.Geocoder();
        reverseGeocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                // results[0] is full address
                if (results[1]) {
                    var reverseGeocoderResult = results[1].formatted_address;
                    showPermanentMessage(reverseGeocoderResult);
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }

    function turnOffAutoRefresh() {
        showMessage('Auto Refresh Off');
        $('#autorefresh').val('Auto Refresh Off');

        autoRefresh = false;
        clearInterval(intervalID);
    }

    function turnOnAutoRefresh() {
        showMessage('Auto Refresh On (1 min)');
        $('#autorefresh').val('Auto Refresh On');
        autoRefresh = true;

        restartInterval();
    }

    function restartInterval() {
        // if someone is viewing all routes and then switches to a single route
        // while autorefresh is on then the setInterval is going to be running with getAllRoutesForMap
        // and not getRouteForMap

        clearInterval(intervalID);

        if (viewingAllRoutes) {
            intervalID = setInterval(getAllRoutesForMap, 15 * 1000); // one minute
        } else {
            intervalID = setInterval(getRouteForMap, 15 * 1000);
        }
    }

    function deleteRoute() {
        if (hasMap()) {

            // comment out these two lines to get delete working
            // confirm("Disabled here on test website, this works fine.");
            // return false;

            var answer = confirm("This will permanently delete this route\n from the database. Do you want to delete?");
            if (answer) {
                var url = 'deleteroute.php' + $('#routeSelect').val();

                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function () {
                        deleteRouteResponse();
                        getAllRoutesForMap();
                    }
                });
            }
            else {
                return false;
            }
        }
        else {
            alert("Please select a route before trying to delete.");
        }
    }

    function deleteRouteResponse() {
        routeSelect.length = 0;

        document.getElementById('map-canvas').outerHTML = "<div id='map-canvas'></div>";

        $.ajax({
            url: 'getroutes.php',
            type: 'GET',
            success: function (data) {
                loadRoutes(data);
            }
        });
    }

    // message visible for 7 seconds
    function showMessage(message) {
        // if we show a message like start auto refresh, we want to put back our current address afterwards
        var tempMessage = $('#messages').html();

        $('#messages').html(message);
        setTimeout(function () {
            $('#messages').html(tempMessage);
        }, 7 * 1000); // 7 seconds
    }

    function showPermanentMessage(message) {
        $('#messages').html(message);
    }

    // for debugging, console.log(objectToString(map));
    function objectToString(obj) {
        var str = '';
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str += p + ': ' + obj[p] + '\n';
            }
        }
        return str;
    }

    function setTheme() {
        //var bodyBackgroundColor = $('body').css('backgroundColor');
        //$('.container').css('background-color', bodyBackgroundColor);
        //$('body').css('background-color', '#ccc');
        // $('head').append('<link rel="stylesheet" href="style2.css" type="text/css" />');
    }

});
