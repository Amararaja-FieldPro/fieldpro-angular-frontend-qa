import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
	selector: 'kt-attendance-report',
	templateUrl: './attendance-report.component.html',
	styleUrls: ['./attendance-report.component.scss']
})
export class AttendanceReportComponent implements OnInit {
	from: NgbDateStruct;
	to: NgbDateStruct;
	service_group: any;
	techId: any;
	assesment_data: any;
	technician_code: any;
	from_date: any;
	to_date: any;
	call_report: any;
	employee_code: any;
	filterAttendance: FormGroup;    //form group 
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	today: any;
	attendance_report: any;
	showTable: boolean = false;
	overlayRef: OverlayRef;
	customer_data: any;
	cusId: any;
	technician_id: any;
	technician_details: any;
	loadingSub: boolean = false;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		this.filterAttendance = new FormGroup({
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to),
			"technician_code": new FormControl('', Validators.required),
		})

		// this.from_date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
		// this.to_date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');

	}

	ngOnInit() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10
			// processing: true
		};
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);

		var dob = new Date();
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		this.today = Fdate;
		// console.log(Fdate);

		var data = {
			"technician_code": null,
			"to_date": Fdate,
			"from_date": Fdate,

		}
		this.filterAttendance.controls['from_date'].setValue(Fdate);
		this.getTech();
		this.getAttendance(data);

	}

	get log() {
		return this.filterAttendance.controls;                       //error logs for create user
	}
	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.attendance_report, 'attendance_report');
	}
	getTech() {
		var url = 'get_alltechnician_details/';                                  // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.alltech = result.response.data;              //storing the api response in the array
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterAttendanceF() {
		this.loadingSub = true;
		// console.log(this.filterAttendance.value.from_date);
		// var from=this.filterAttendance.value.from_date;
		// var splitted = from.split("-", 3);
		// console.log(this.filterAttendance.value.from_date.year);

		// console.log(splitted);
		this.from_date = this.filterAttendance.value.from_date;
		this.to_date = this.filterAttendance.value.to_date;
		this.technician_code = this.filterAttendance.value.technician_code;

		if (this.technician_code == "") {
			this.filterAttendance.value.technician_code = null;
		}
		else {
			this.filterAttendance.value.technician_code = this.filterAttendance.value.technician_code;
		}
		if (this.filterAttendance.value.from_date.year == undefined) {
			this.filterAttendance.value.from_date = this.filterAttendance.value.from_date;
			// console.log(this.filterAttendance.value.from_date);
		}
		else {
			// console.log(this.filterAttendance.value.from_date)
			var Fyear = this.filterAttendance.value.from_date.year;
			var Fmonth = this.filterAttendance.value.from_date.month;
			var Fday = this.filterAttendance.value.from_date.day;
			this.filterAttendance.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
		}
		if (this.filterAttendance.value.to_date.year == undefined) {
			this.filterAttendance.value.to_date = this.filterAttendance.value.to_date;
			// console.log(this.filterAttendance.value.to_date);
		}
		else {
			var Tyear = this.filterAttendance.value.to_date.year;
			var Tmonth = this.filterAttendance.value.to_date.month;
			var Tday = this.filterAttendance.value.to_date.day;
			this.filterAttendance.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;
		}
		this.showTable = false;
		this.getAttendance(this.filterAttendance.value);
	}

	getAttendance(data) {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var url = 'get_attendence_details/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.attendance_report = result.response.data;
				this.loadingSub = false;
				this.showTable = true;
				this.chRef.detectChanges();
				// this.dtTrigger.next();
				this.overlayRef.detach();

			}
			else if (result.response.response_code == "500") {
				this.loadingSub = false;
				this.showTable = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			this.loadingSub = false;
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	customerDetails(technician_id, tech_details) {
		this.techId = technician_id;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, technician_code, technician_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_details = result.response.data; //bind results in this variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				}

				)
			}
			else if (result.response.response_code == "400") {
				this.technician_details = result.response.data; //bind results in this variable
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal"                                  //specifies the size of the dialog box
		});
	}
}
