import { Component, Input, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ajaxservice } from './../../../ajaxservice';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router'; // To enable routing for this component
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';




@Component({
	selector: 'kt-slamapping',
	templateUrl: './slamapping.component.html',
	styleUrls: ['./slamapping.component.scss']
})
export class SlamappingComponent implements OnInit {
	
	@Input() name;
	list_company1: any = ['Whirlpool', 'Jeeves', 'Easy home'];
	selectedcompany: any = "All";
	@ViewChild('div', { static: true }) div: ElementRef;
	images: any;
	httplink:any;
	organ: any;
	show: boolean = true;
	product_data: any;                                   //variable to store the product data
	org_data: any;                                    //variable to store the organization data
	subproduct_data: any;                              //variable to store the subproduct data
	sla_data: any;                             //variable to store the sla data
	UpdateSla: FormGroup
	EditSla: FormGroup
	prod_id: any;
	sub_id: any;
	product_name: any;
	product_id: any;
	submitted = false;
	showTable: boolean = false;
	overlayRef: OverlayRef;
	subproduct_id: any;
	sub_name: any;
	activeId: any;
	response_miniute: any
	sub_data_value: string;
	error: any;
	showTables: boolean = false;
	loadingNext: boolean = false;
	loadingSubmit: boolean = false;
	loadingSub: boolean = false;
	currentJustify = 'end';
	location_type: any;
	goToBottom() {
		window.scrollTo(0, document.body.scrollHeight);
	}
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	model: any = {

	};
	edit_data: any;
	organization_logo: any;
	organization_name: any;
	constructor(config: NgbTabsetConfig, private modalService: NgbModal, private toastr: ToastrService,
		private router: Router, public activeModal: NgbActiveModal, public ajax: ajaxservice, public _location: Location, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.UpdateSla = new FormGroup({
			"product_id": new FormControl(''),
			"priority": new FormControl('', [Validators.required]),
			"subproduct_id": new FormControl(''),
			"response_day": new FormControl('', [Validators.required]),
			"response_time": new FormControl('', [Validators.required]),
			"response_miniute": new FormControl('', [Validators.required]),
			"resolution_day": new FormControl('', [Validators.required]),
			"resolution_time": new FormControl('', [Validators.required]),
			"resolution_miniute": new FormControl('', [Validators.required]),
			"acceptance_day": new FormControl('', [Validators.required]),
			"acceptance_time": new FormControl('', [Validators.required]),
			"acceptance_miniute": new FormControl('', [Validators.required]),
			"SLA_Compliance_Target": new FormControl('', [Validators.required]),
		});
	}
	get UpdateSlalog() {
		return this.UpdateSla.controls;                       //error logs for create user
	}
	get log() {
		return this.EditSla.controls;                       //error logs for create user
	}

	ngOnInit() {
		this.httplink=environment.image_static_ip;
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

		this.getimagedetails()
		this.get_sla_details();


	}

	getlocation() {	
		var data = { "voltage": '12v' }
		var url = 'get_voltage_locations/'                                         //api url of remove license
		this.ajax.postdata(url, data).subscribe((result) => {
		  if (result.response.response_code == "200" || result.response.response_code == "400") {
			this.location_type = result.response.data;	
		  }
			else if (result.response.response_code == "500") {
			this.toastr.error(result.response.message, 'Error');        //toastr message for error
		  }
		  else {
			this.toastr.error(result.response.message, 'Error');        //toastr message for error
		  }
		}, (err) => {
		  console.log(err);                                             //prints if it encounters an error
		});
	
	
	  }
	getimagedetails() {
		this.error = '';
		var url = 'get_org_product/';                                   // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.org_data = result.response.org_data;                    //storing the api response in the array
				this.organization_logo = this.org_data.organization_logo
				this.organization_name = this.org_data.organization_name
				this.product_data = result.response.product_data;
				this.chRef.detectChanges();

			}
			else if (result.response.response_code == "500") {
				this.error = result.response.message;
			}
			else {
				this.error = "Something Went wrong";
			}
		}, (err) => {
			console.log(err);                                                 //prints if it encounters an error
		});
	}
	getproduct() {
		console.log(this.org_data);
		console.log(this.product_data);
		this.product_name = "All Categories";
		this.sub_name = "All Sub Categories";
		this.prod_id = "All Categories";
		this.sub_id = "All Sub Categories";

	}
	getsubimage_details(product_id) {
		this.error = '';
		var productid = product_id;
		var product = +productid;                        //get the product id for identify the  product name                     
		var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
		var id = "product_id";
		this.sub_name = "All Sub Categories";                       //id use for url
		this.ajax.getdataparam(url, id, product).subscribe((result) => {
			if (result.response.response_code == "200") {               //if sucess
				this.subproduct_data = result.response.data;
				console.log(this.subproduct_data);
				console.log(this.subproduct_data[0].product.product_name);
				this.product_name = this.subproduct_data[0].product.product_name;
				this.sub_data_value = "";
				this.showTable = true;
				this.chRef.detectChanges();
			}
			else if (result.response.response_code == "400") {
				this.subproduct_data = result.response.data;
				this.sub_data_value = "0 No data found";
			}
			else if (result.response.response_code == "500") {
				this.error = result.response.message;
			}
			else {
				this.error = "Something Went wrong";
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});

	}
	//for edit option
	openLarge(content6) {
		this.error = '';
		this.modalService.open(content6, {
			size: 'lg',
			windowClass: "center-modalsm"                                    // modal popup for resizing the popup
		});
	}



	//get all sla details



	onchangeproduct(product_id) {
		this.prod_id = product_id
		this.sub_id = 'All Sub Categories';
		this.sub_name = "All Sub Categories";
	}
	onchangesubproduct(product_sub_id, product_sub_name) {
		this.sub_id = product_sub_id
		console.log(this.sub_id);
		this.sub_name = product_sub_name;
		console.log(this.sub_name);
		this.subproduct_id = product_sub_id;
	}

	update_sla(upload_sla) {
		this.UpdateSla.reset();
		this.error = '';
		if ((this.prod_id == '' && this.sub_id == '') || (this.prod_id == null && this.sub_id == null)) {
			
			Swal.fire({
				title: 'Select product before uploading sla',
				icon: 'error',
				// showCancelButton: true,
				cancelButtonText: 'OK'
			})
		}
		else {
			this.loadingNext = true;
			if (this.sub_id == '') {
				this.sub_id = "All Sub Categories";

			}
			var data = {
				"product_id": this.prod_id,
				"subproduct_id": this.sub_id
			}
			
			var url = 'sla_product/'
			console.log(data)                                     //api url of add
			this.ajax.postdata(url, data).subscribe((result) => {
				if (result.response.response_code == "200") {
					this.loadingNext = false;
					this.chRef.detectChanges();
					this.modalService.open(upload_sla, {
						size: 'lg',
						// windowClass: "center-modalsm",                                    // modal popup for resizing the popup
						backdrop: 'static',                                    // modal will not close by outside click
						keyboard: false,                                       // modal will not close by keyboard click

					});
					
				}
				else if (result.response.response_code == "500") {
					this.error = result.response.message;
					this.loadingNext = false;
					this.chRef.detectChanges();
					
				}
				else {
					this.error = "Something Went wrong";
					this.loadingNext = false;
				}
				this.activeId = "tab-selectbyid1";

			}, (err) => {
				console.log(err);                                             //prints if it encounters an error
			});



		}

	}
	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
		this.ngOnInit();
	}
	sla_update() {
		this.error = '';
		this.submitted = true;
		this.loadingSubmit = true;
		console.log('update sla');
		console.log(this.UpdateSla.value)

		// stop here if form is invalid
		if (this.UpdateSla.invalid) {
			console.log("error");
			// return;
		}
		// else{
		console.log(this.prod_id, "this.prod_id")
		console.log(this.sub_id, "this.subid")
		console.log(this.UpdateSla.value.subproduct_id)
		if (this.sub_id == '' || this.sub_id == null) {
			this.UpdateSla.value.subproduct_id = "All Sub Categories"
		}
		else {
			this.UpdateSla.value.subproduct_id = this.sub_id
		}
		this.UpdateSla.value.resolution_time = this.UpdateSla.value.resolution_time + ":" + this.UpdateSla.value.resolution_miniute;
		this.UpdateSla.value.response_time = this.UpdateSla.value.response_time + ":" + this.UpdateSla.value.response_miniute;
		this.UpdateSla.value.acceptance_time = this.UpdateSla.value.acceptance_time + ":" + this.UpdateSla.value.acceptance_miniute;
		console.log(this.UpdateSla.value.resolution_time);
		console.log(this.UpdateSla.value.response_time);
		console.log(this.UpdateSla.value.acceptance_time);
		this.submitted = true;
		this.UpdateSla.value.product_id = this.prod_id
		var str = "02:03";
		var splitted = str.split(":", 1);
		console.log(splitted, "splitted")
		var data = this.UpdateSla.value;          //Data to be passed for add api	
		var url = 'update_sla/'
		console.log(data)                                     //api url of add
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200") {

				this.toastr.success(result.response.message, 'Success');    //toastr message for success
				this.loadingSubmit = false;
				this.modalService.dismissAll();
				//to close the modal box
			}
			else if (result.response.response_code == "500") {
				this.error = result.response.message;
				this.loadingSubmit = false;
			}
			else {
				this.error = "Something Went wrong";
				this.loadingSubmit = false;
			}
		}, (err) => {
			console.log(err);                                             //prints if it encounters an error
		});
		// }


		// }
	}


	get_sla_details() {
		this.error = '';
		this.overlayRef.attach(this.LoaderComponentPortal);
		var url = 'get_sla_details/';                                   // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.sla_data = result.response.data;   //storing the api response in the array              
				this.showTables = true;
				this.chRef.detectChanges();

				// this.sla_data.forEach(element => {
				// 	this.EditSla = new FormGroup({
				// 		"location_type_id": new FormControl(element.location_type.location_type_id),
				// 		"sla_mapping_id": new FormControl(element.sla_mapping_id),
				// 		"response_day": new FormControl(element.response_day),
				// 		"response_time": new FormControl(element.response_time),
				// 		"resolution_day": new FormControl(element.resolution_day),
				// 		"resolution_time": new FormControl(element.resolution_time),
				// 		"acceptance_day": new FormControl(element.acceptance_day),
				// 		"acceptance_time": new FormControl(element.acceptance_time),
				// 		"SLA_Compliance_Target": new FormControl(element.SLA_Compliance_Target),

				// 	}
				// 	);
				// });
				this.overlayRef.detach();

			}
			else if (result.response.response_code == "500") {
				this.error = result.response.message;
			}
			else {
				this.error = "Something Went wrong";
			}
		}, (err) => {
			console.log(err);                                                 //prints if it encounters an error
		});
	}

	sla_edit_data(sla_id, edit_sla) {
		this.getlocation();
		this.error = '';
		// this.submitted = true;

		// // stop here if form is invalid
		// if (this.EditSla.invalid) {
		// 	// console.log(error);
		// return;
		// }
		var sla_id = sla_id;
		var sla_mapping_id = "sla_mapping_id";
		var url = 'get_sla_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, sla_mapping_id, sla_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.edit_data = result.response.data;
				this.chRef.detectChanges();
			
				var response_time = this.edit_data.response_time;
				var splitted_response = response_time.split(":", 3);
				var resolution_time = this.edit_data.resolution_time;
				var splitted_resolution = resolution_time.split(":", 3);
				console.log(splitted_response[1])
				var acceptance = this.edit_data.acceptance_time;
				var splitted_acceptance = acceptance.split(":", 3);
				console.log(this.edit_data.product_name);
				this.product_id = this.edit_data.product_name;
				this.subproduct_id = this.edit_data.subproduct_name;
				this.response_miniute = splitted_response[1];
				console.log(this.edit_data)				
				this.EditSla = new FormGroup({
					"sla_mapping_id": new FormControl(this.edit_data.sla_mapping_id),
					"response_day": new FormControl(this.edit_data.response_day),
					"response_time": new FormControl(splitted_response[0]),
					"resolution_day": new FormControl(this.edit_data.resolution_day),
					"resolution_miniute": new FormControl(splitted_resolution[1]),
					"response_miniute": new FormControl(this.response_miniute),
					"acceptance_miniute": new FormControl(splitted_acceptance[1]),
					"resolution_time": new FormControl(splitted_resolution[0]),
					"acceptance_day": new FormControl(this.edit_data.acceptance_day),
					"acceptance_time": new FormControl(splitted_acceptance[0]),
					"location_type_id": new FormControl(this.edit_data.location_type.location_type_id),
					"SLA_Compliance_Target": new FormControl(this.edit_data.SLA_Compliance_Target),
				});
				console.log(this.EditSla.value)
				this.modalService.open(edit_sla, {
					size: 'lg',
					windowClass: "center-modallg",                                    // modal popup for resizing the popup
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "500") {
				this.error = result.response.message;
			}
			else {
				this.error = "Something Went wrong";
			}
			this.activeId = "tab-selectbyid2";
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}

	sla_edit() {
		this.error = '';
		this.submitted = true;
		this.loadingSub = true;
		console.log('edit add form');
		// stop here if form is invalid
		if (this.EditSla.invalid) {
			// console.log(error);
			return;
		} else {
			console.log(this.EditSla.value);
			this.EditSla.value.resolution_time = this.EditSla.value.resolution_time + ":" + this.EditSla.value.resolution_miniute;
			this.EditSla.value.response_time = this.EditSla.value.response_time + ":" + this.EditSla.value.response_miniute;
			this.EditSla.value.acceptance_time = this.EditSla.value.acceptance_time + ":" + this.EditSla.value.acceptance_miniute;
			console.log(this.EditSla.value.response_time);
			var data = this.EditSla.value;           //Data to be passed for add api
			var url = 'edit_sla/'                                         //api url of add
			this.ajax.postdata(url, data).subscribe((result) => {
				if (result.response.response_code == "200") {
					this.showTables = false;
					this.chRef.detectChanges();
					this.get_sla_details();
					this.toastr.success(result.response.message, 'Success');    //toastr message for success
					this.loadingSub = false;
					this.modalService.dismissAll();
					//to close the modal box
				}
				else if (result.response.response_code == "500") {
					this.error = result.response.message;
					this.loadingSub = false;
				}
				else {
					this.error = "Something Went wrong";
					this.loadingSub = false;
				}
				this.activeId = "tab-selectbyid2";
				console.log(this.activeId);
			}, (err) => {
				console.log(err);                                             //prints if it encounters an error
			});
		}

	}


	// function for delete the content
	detele_sla(sla_ma_id) {
		this.error = '';
		var sla_mapping_id = { "sla_mapping_id": sla_ma_id };
		var url = 'delete_sla/';             // api url for delete warehouse
		var id = "sla_mapping_id";                        //id for identify warehouse name
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, sla_mapping_id).subscribe((result) => {
					if (result.response.response_code == "200") {               //if sucess

						this.showTables = false;
						this.chRef.detectChanges();
						this.get_sla_details();
						this.toastr.success(result.response.message, 'Success');        //toastr message for success

					}
					else if (result.response.response_code == "500") {
						this.error = result.response.message;
					}
					else {
						this.error = "Something Went wrong";
					}
					this.activeId = "tab-selectbyid2";
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}

}
