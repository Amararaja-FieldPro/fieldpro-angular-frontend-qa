import { Component, ViewEncapsulation, OnInit, ViewChild, ChangeDetectorRef,ElementRef } from '@angular/core';
import { filter } from 'rxjs/operators';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { ChartComponent } from '@syncfusion/ej2-angular-charts';
import { FormControl } from '@angular/forms';
import * as xlsx from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
// import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
// import { Browser } from '@syncfusion/ej2-base';
let series1: any[] = [];
let yValue = [7.66, 8.03, 8.41, 8.97, 8.77, 8.20, 8.16, 7.89, 8.68, 9.48, 10.11, 11.36, 12.34, 12.60, 12.95,
  13.91, 16.21, 17.50, 22.72, 28.14, 31.26, 31.39, 32.43, 35.52, 36.36,
  41.33, 43.12, 45.00, 47.23, 48.62, 46.60, 45.28, 44.01, 45.17, 41.20, 43.41, 48.32, 45.65, 46.61, 53.34, 58.53];
let point1; let i; let j = 0;
for (i = 1973; i <= 2013; i++) {
  point1 = { x: i, y: yValue[j] };
  series1.push(point1); j++;
}
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;

@Component({
  selector: 'kt-replacement-battery',
  templateUrl: './replacement-battery.component.html',
  styleUrls: ['./replacement-battery.component.scss']
})
export class ReplacementBatteryComponent implements OnInit {

  public chartData: Object[] = [
    { "Months": "Months", "25_30": 600, "31_36": 800 },
    // { "Months":"Months","31_36": "31 To 36 Months", y: 800 }, 
    // { x: new Date(2006, 0, 1), y: 24 },
    // { x: new Date(2007, 0, 1), y: 36 }, { x: new Date(2008, 0, 1), y: 38 },
    // { x: new Date(2009, 0, 1), y: 54 }, { x: new Date(2010, 0, 1), y: 57 },
    // { x: new Date(2011, 0, 1), y: 70 }
  ];
  public data: Object[] = series1;
  public primaryYAxis: Object = {
    title: '',
    interval: 200, lineStyle: { width: 0 }, majorTickLines: { width: 0 }
  };
  public primaryXAxis: Object = {
    title: 'Months',

  };
  public chartArea: Object = {
    border: { width: 0 }
  };
  public tooltip: Object;
  public title: string = 'Battery Age Report';
  public primaryXAxis1: Object;
  // public chartData: Object[];
  public title1: string;
  public primaryYAxis1: Object;
  public palette: string[];
  // public chartData1: Object[];

  public marker: Object;

  ChartTrendlines: {
    //...
    //Customize the Trendline styles
    fill: string; width: number; opacity: number; dashArray: string;
  }[];
  @ViewChild('chartid', { static: true })
  public chartObj: ChartComponent;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  start_date: any;
  mfg_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  currentYear = moment().year();
  currentMonth = moment().month();
  dtOptions: DataTables.Settings = {};
  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef, private calendar: NgbCalendar) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 25, 50],
      processing: true
    };
    setTimeout(function () {
      $('#datatable').DataTable(this.dtOptions)
    }, 2000);//wait 2 seconds
    this.marker = {
      dataLabel: { visible: true }
    };
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    this.start_date = moment(this.fromdates.value._d).format("YYYY-MM-DD"); //change the date format
    // this.end_date=moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    this.start_date = moment([this.currentYear - 1, 3, 1]).format("YYYY-MM-DD");
    this.end_date = moment([this.currentYear, 2, 28]).format("YYYY-MM-DD");
    this.mfg_date = moment([this.currentYear, this.currentMonth, 1]).format("YYYY-MM-DD");
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    if (Fmonth > 3) {
      var finyear = Fyear + "-" + (Fyear + 1)
    }
    else {
      finyear = (Fyear - 1) + "-" + Fyear
    }

    this.start_date = Fyear + "-" + Fmonth + "-" + 1;
    var datas = {
      "employee_code": emp_code,
      "start_date": this.start_date,
      "end_date": this.end_date,
      "service_desk": [],
      "state": [],
      "technician": [],
      "customer": '',
      "segment": [],
      "mfg_date": this.mfg_date,
      "product": [],
      "product_sub_id": [],
      "model_no": [],
      "region": [],
      "call_catgory": [],
      "finance_year": finyear,
      "flag": 1,
      "fin_flag": "year",
      "application": []
    };
    this.batterry_replace_report(datas)
    this.primaryXAxis = { valueType: 'Category' };

    // this.title = 'Battery Age Report';
  }
  export() {
    this.chartObj.exportModule.export('PNG', 'export');

  }
  export_table() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
  const wb: xlsx.WorkBook = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
  xlsx.writeFile(wb, 'BatteryAgeWiseReplaceReport.xlsx');
  }
  updateDataMain(datas) {
    this.batterry_replace_report(datas)

  }
  public batterry_replace_report(data) {
    var url = 'battery_replace_report/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.chartData = result.response.data;              //storing the api response in the array
        //     this.chartData1 = result.response.TAT;
        this.dtOptions = {
          pagingType: 'full_numbers',
          searching: false,
          pageLength: 15,
          lengthMenu: [15, 25, 50],
          data: result.response.data,
          search: false,
          processing: true,
          columns: [{ data: 'BatteryAge', orderable: false }, { data: '0_6' }, { data: '7_12' }, { data: '13_18' }, { data: '19_24' }, { data: '25_30' }, { data: '31_36' }, { data: 'Above36' }],
          "columnDefs": [
            {
              "targets": [], //first column / numbering column
              "orderable": false, //set not orderable
              "visible": false,
              "searchable": false,
            },
          ],
          order: [],
          destroy: true

        };
        console.log(result.response.Financial_Year[0],"result.response.Finance_title")
        // this.Export = result.response.Table;
        $('#datatable').DataTable(this.dtOptions)
        this.chRef.detectChanges();
        this.title = 'Battery Age Wise Complaints Replacement Report' + " " + "(" + result.response.Financial_Year[0] + "-" + result.response.Financial_Year[1] + ")";
      }
      else if (result.response.response_code == "500") {
        // this.toastr.error(result.response.message, 'Error')
        // this.showTable = 'true';
        this.chRef.detectChanges();
        // this.overlayRef.detach();
      }
      else {
        // this.toastr.error("Something went wrong", 'Error')
        // this.showTable = 'true';
        this.chRef.detectChanges();
        // this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      // this.overlayRef.detach();
    })
  }

}
