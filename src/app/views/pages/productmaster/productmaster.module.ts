import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatStepperModule,MatFormFieldModule,MatInputModule ,MatRadioModule } from '@angular/material';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule
  ]
})
export class ProductmasterModule { }
