import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Router } from '@angular/router';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post

import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
import { TatTrendComponent } from '../tat-trend/tat-trend.component';   // for call the api function from this component
import { CsiAnalysisComponent } from '../csi-analysis/csi-analysis.component';
import { CSLTrendComponent } from '../csltrend/csltrend.component';   // for call the api function from this component
import { FinancialReportComponent } from '../financial-report/financial-report.component';   // for call the api function from this component
import { FinanceReplacementComponent } from '../finance-replacement/finance-replacement.component';   // for call the api function from this component
import { FtrreportComponent } from '../ftrreport/ftrreport.component';
import { ComplaintsummeryComponent } from '../complaintsummery/complaintsummery.component';
import { CompetitorreportComponent } from '../competitorreport/competitorreport.component';
import { PerformanceReportComponent } from '../performance-report/performance-report.component'
@Component({
  selector: 'kt-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    CSLTrendComponent,
    TatTrendComponent,
    FinancialReportComponent,
    FinanceReplacementComponent,
    CsiAnalysisComponent,
    FtrreportComponent,
    ComplaintsummeryComponent,
    CompetitorreportComponent,
    PerformanceReportComponent
  ],
})
export class FiltersComponent implements OnInit {
  // two dates are moment function
  // fromdates = new FormControl(moment().startOf('month').format('YYYY-MM-DD'));
  fromdates = new FormControl(moment());
  mfg_date = new FormControl();
  todates = new FormControl(moment());
  finance_year = new FormControl();
  // it will call parent component filter api
  @Output() updateDataRequest = new EventEmitter<any>();
  datas: any = [];
  // public date_mfg: Date;
  // the bellow settings are values shows and set for api in the dropdowns
  dropdownSettings: any = {};
  dropdownSettingsservice: any = {};
  dropdownSettingstechnician: any = {};
  dropdownSettingscustomer: any = {};
  dropdownSettingssegment: any = {};
  ShowFilter = true; //for role based dropdowns
  FilterData: FormGroup;  //for dropdowns
  state: any;  //data from api
  employee_code: string; // from local storage
  assignstates: any; // data from api
  avilable_service_details: any = []; //data from api
  service_desk: any = [];  // for local storage array
  service_desk_code: any = [];  // for local storage array
  avilable_technician_details: any;  //data from api
  technician: any = [];  //for local storage
  state_customer: any = [];  // for local storage store the customer code based on state
  CustomerName: any = []; // data from api
  segment: any; //data from api
  select_service: any[]; // for visible only
  select_customer: any[]; //for visible only
  select_technician: any[]; //for visible only
  select_segment: any[];  //for visible only
  segment_id: any = [];  // for local storage in segment id
  role_id: string; // from local storage get item
  path: string; // windows url for graph
  state_service: string;  // string for filter dropdowns is  needed or not
  start_date: any;  // store the date format
  end_date: any; // store the date format
  mfg_dates: any;
  page: string;
  @ViewChild('element', { static: true }) element: ElementRef;
  loadingUpd: boolean = false;
  product: any;
  subproduct: any;
  product_service: string;
  ftr_service: string;
  model_id: any;
  callcategorydetails: any;
  tech_service: string;
  replace_service: string;
  application: any;
  replace_service_analyse: string;
  battery_service: string;
  competitor_service: string;
  reject_reason: string;
  location_wise: string;
  finance_service: string;
  minDate: Moment;
  maxDate: Moment;
  maxDates: Moment;
  date_time: string;
  public Filtershow: boolean = true;
  date_month: string;
  finanace_year: string;
  message: string;
  last_months: string;
  ftr_service_filter: string;
  tat_report_service: string;
  finreplace_report_service: string;
  fin_report_service: string;
  performance_service: string;
  segment_service: string;
  selectdrop: string;
  selecttype: string;
  complaint_service: string;
  ticket_service: string;
  selectyear: string;
  fin_flag: any;
  customer_type: any;
  reject_reason_service: string;
  complaint_service_desk: string;
  constructor(private router: Router, public ajax: ajaxservice,
    private tat_trend: TatTrendComponent,  // child component for call the api for gaph in tat report
    private csl_trend: CSLTrendComponent,
    private finance_report: FinancialReportComponent,
    private finace_replace: FinanceReplacementComponent,
    private csi_analysis: CsiAnalysisComponent,
    private ftr_report: FtrreportComponent,
    private competitor: CompetitorreportComponent,
    private Performance_report: PerformanceReportComponent
  ) { }
  toggle() {
    this.Filtershow = !this.Filtershow;
  }
  ngOnInit() {
    this.finanace_year = 'Yes';
    this.date_time = "None";
    this.date_month = "None";
    this.service_desk = [];  // set empty
    this.service_desk_code = [];
    this.state = [];  //set empty
    this.employee_code = localStorage.getItem('employee_code');  //get from login 
    this.role_id = localStorage.getItem('role_id'); //get  from login
    this.customer_type = JSON.parse(localStorage.getItem('customer_type'))
    this.start_date = moment().startOf('month').format('YYYY-MM-DD'); //change the date format
    this.end_date = moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    this.mfg_dates = moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    this.path = window.location.pathname.split('/')[1]; //get the web url // 2-dev 1-live //need to change
    // this.selecttype = '0';
    this.selecttype = '6';
    this.get_customer_type(6);
    if (this.path == 'tat_trend') {
      this.selectdrop = '2';
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.page = 'tat';
      if (this.role_id == '5') {
        this.segment_service = "enable";
        // 5 is manager. In manager all filters are needed
        this.state_service = "enable";      // set the value
        this.finance_service = "disable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.tat_report_service = "enable";
        // this.state_service = "disable";  // set the value
        // this.finance_service = "disable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
    }
    else if (this.path == 'csl_trend') {
      this.getsegment();
      this.selectdrop = '2';
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.finance_service = "disable";  // set the value
      this.page = 'csl';
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.state_service = "enable";      // set the value
        this.segment_service = "enable";
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        this.segment_service = "enable";
        this.tat_report_service = "enable";
        // For service desk. In this state and service desk are not needed
        // this.state_service = "disable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
    }
    else if (this.path == 'csi_analyse') {
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.selectdrop = '2';
      this.finance_service = "disable";  // set the value
      this.page = 'csl_analysis';
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.replace_service_analyse = "enable";
        this.segment_service = "enable";
        this.state_service = "enable";      // set the value

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.tat_report_service = "enable";
        // this.state_service = "disable";  // set the value
        this.replace_service_analyse = "enable";

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);


    }
    else if (this.path == 'finance_report') {
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.selectdrop = '2';
      this.page = 'finance';
      this.getproduct();
      this.get_callcategory();
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.tech_service = "enable";      // set the value
        // this.state_service="enable";      // set the value
        this.finance_service = "enable";  // set the value
        this.product_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl([]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "mfg_date": new FormControl(''),
          "flag": new FormControl(2),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        var data_segment = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_segment); // technician and  get from api based on service desk employee code
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        this.fin_report_service = "enable";
        this.finance_service = "enable";  // set the value
        // For service desk. In this state and service desk are not needed
        // this.state_service = "disable";  // set the value
        // this.product_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl(''),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl([]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "mfg_date": new FormControl(''),
          "flag": new FormControl(2),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      // this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['call_catgory'].setErrors({ 'incorrect': true });
      this.FilterData.controls['call_catgory'].setValue(null);
      // this.FilterData.controls['region'].setValue(null);
      this.FilterData.controls['model_no'].setValue(null);
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setValue(null);

    }
    else if (this.path == 'finance_replace') {
      this.selectdrop = '2';
      this.page = 'replace';
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.getproduct();
      this.get_callcategory();
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.tech_service = "enable";      // set the value
        this.replace_service = "enable";
        this.finance_service = "enable";  // set the value
        this.product_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "voltage": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(2),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        var data_segment = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_segment); // technician and  get from api based on service desk employee code
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        this.finreplace_report_service = "enable";
        this.finance_service = "enable";  // set the value
        // For service desk. In this state and service desk are not needed
        // this.product_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "voltage": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(2),
          "mfg_date": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['call_catgory'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['voltage'].setErrors({ 'incorrect': true });
      this.FilterData.controls['voltage'].setValue(null);
      this.FilterData.controls['application'].setValue(null);
      this.FilterData.controls['call_catgory'].setValue(null);
      this.FilterData.controls['region'].setValue(null);
      this.FilterData.controls['model_no'].setValue(null);
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setValue(null);
    }
    else if (this.path == 'battery-age') {
      this.page = 'battery-age';
      // this.last_months = 'yes';
      // this.selectdrop = '3';
      // this.selectyear = "2020-2022";
      // this.finanace_year = 'Yes';
      // this.date_month = "No";
      // this.date_time = "No";
      // this.onChangereporttype(3);
      this.selectdrop = '2';
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.finance_service = "disable";  // set the value
      this.fin_flag = "year";
      this.getproduct();
      this.finance_service = "enable";  // set the value
      // this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {          // 5 is manager. In manager all filters are needed
        this.replace_service_analyse = "enable";
        this.battery_service = "enable";  // set the value
        // this.state_service = "enable";      // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
          "mfg_date": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        // this.state_service = "disable";  // set the value
        this.replace_service_analyse = "enable";
        this.finance_service = "enable";  // set the value
        // this.product_service = "enable";  // set the value
        this.battery_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
          "mfg_date": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      var data_service = { "employee_code": this.employee_code, "role": 5 };
      this.service_technician(data_service);
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setValue(null);
      this.FilterData.controls['model_no'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);


    }
    else if (this.path == 'battery-replace') {
      // this.last_months = 'yes';
      // this.selectdrop = '3';
      // this.selectyear = "2020-2022";
      // this.finanace_year = 'Yes';
      // this.date_month = "No";
      // this.date_time = "No";
      // this.onChangereporttype(3);
      this.selectdrop = '2';
      this.last_months = 'no';
      this.selectyear = "2022-2023";
      this.finance_service = "disable";  // set the value
      this.fin_flag = "year";
      this.page = 'battery-replace';
      this.getproduct();
      this.finance_service = "enable";  // set the value
      // this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {          // 5 is manager. In manager all filters are needed
        this.replace_service_analyse = "enable";
        this.battery_service = "enable";  // set the value
        // this.state_service = "enable";      // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
          "mfg_date": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        // this.state_service = "disable";  // set the value
        this.replace_service_analyse = "enable";
        this.finance_service = "enable";  // set the value
        // this.product_service = "enable";  // set the value
        this.battery_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
          "mfg_date": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      var data_service = { "employee_code": this.employee_code, "role": 5 };
      this.service_technician(data_service);
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setValue(null);
      this.FilterData.controls['model_no'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);


    }
    else if (this.path == 'ftr') {
      this.selectdrop = '3';
      this.finanace_year = 'No';
      this.last_months == 'no'
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(3);
      // const currentYear = moment().year();
      // const currentMonth = moment().month();
      // if (currentMonth > 3) {
      //   this.date_time = "Yes";
      //   this.minDate = moment([currentYear, currentMonth - 2, 1]);
      //   this.maxDate = moment([currentYear, currentMonth, 28]);
      // }
      // else {
      //   this.date_time = "No";
      //   if (currentMonth == 0) {
      //     this.minDate = moment([currentYear - 1, currentMonth + 10, 1]);
      //     this.maxDate = moment([currentYear, currentMonth, 28]);
      //   }
      //   else if (currentMonth == 1) {
      //     this.minDate = moment([currentYear - 1, currentMonth + 9, 1]);
      //     this.maxDate = moment([currentYear, currentMonth, 28]);
      //   }
      //   else if (currentMonth == 2) {
      //     this.minDate = moment([currentYear - 1, currentMonth + 8, 1]);
      //     this.maxDate = moment([currentYear, currentMonth, 28]);
      //   }
      // }
      // this.maxDates = moment([currentYear+1, 3, 28]);
      this.page = 'ftr';
      this.last_months = 'yes';
      // this.date_month = "Yes";
      this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.ftr_service = "enable";      // set the value

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl(''),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        this.ftr_service_filter = "enable";
        // For service desk. In this state and service desk are not needed
        // this.ftr_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl(''),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['region'].setValue(null);
    }
    else if (this.path == 'competitor') {
      this.page = 'competitor';
      this.last_months = 'yes';
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(4);
      this.page = 'complaint';
      this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.competitor_service = "enable";      // set the value

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl(''),
          "region": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        // var data = { "employee_code": this.employee_code }
        // this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
        var data_service = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.competitor_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl(''),
          "region": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        // var datas = { "employee_code": this.employee_code, "role": 4 }
        // this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);
      this.FilterData.controls['region'].setValue(null);
    }
    else if (this.path == 'reject_reason') {
      this.page = 'reject_reason';
      this.last_months = 'yes';
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      // this.selectdrop = '2';
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(4);
      this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.reject_reason = "enable";      // set the value

        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
        var data_service = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.reject_reason_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl(''),
          "region": new FormControl('', [Validators.required]),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);
      this.FilterData.controls['region'].setValue(null);
    }
    else if (this.path == 'locationwise_report') {
      this.page = 'locationwise_report';
      this.last_months = 'yes';
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(4);
      this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.location_wise = "enable";      // set the value

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl('',),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
        var data_service = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.location_wise = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl('',),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setErrors({ 'incorrect': true });
      this.FilterData.controls['segment'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);
      this.FilterData.controls['region'].setValue(null);
    }
    else if (this.path == 'ticket_age') {
      this.last_months = 'yes';
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(4);
      this.page = 'ticket_age';
      this.finance_service = "disable";  // set the value
      this.getproduct();
      if (this.role_id == '5') {          // 5 is manager. In manager all filters are needed
        // this.replace_service_analyse = "enable";
        this.ticket_service = "enable";  // set the value
        // this.state_service = "enable";      // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl(''),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        // this.state_service = "disable";  // set the value
        // this.replace_service_analyse = "enable";
        // this.product_service = "enable";  // set the value
        this.ticket_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "service_desk": new FormControl('', [Validators.required]),
          "technician": new FormControl('', [Validators.required]),
          "customer": new FormControl('', [Validators.required]),
          "segment": new FormControl('', [Validators.required]),
          "application": new FormControl('', [Validators.required]),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "call_catgory": new FormControl('', [Validators.required]),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      var data_service = { "employee_code": this.employee_code, "role": 5 };
      this.service_technician(data_service);
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setValue(null);
      this.FilterData.controls['model_no'].setValue(null);
      this.FilterData.controls['application'].setErrors({ 'incorrect': true });
      this.FilterData.controls['application'].setValue(null);
      this.FilterData.controls['segment'].setErrors({ 'incorrect': true });


    }
    else if (this.path == 'complaint_summery') {
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(2);
      this.page = 'complaint';
      this.last_months = 'yes';
      this.getproduct();
      // this.date_month = "Yes";
      // this.finance_service = "disable";  // set the 
      // this.replace_service="disable";
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        // this.replace_service_analyse = "enable";
        this.complaint_service = "enable";  // set the value
        // this.ftr_service = "enable";      // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "customer": new FormControl(''),
          "application": new FormControl(''),
          "segment": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
      }
      else {
        // this.ftr_service_filter="enable";
        // For service desk. In this state and service desk are not needed
        // this.replace_service_analyse = "enable";
        this.complaint_service_desk = "enable";  // set the value
        this.FilterData = new FormGroup({
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl(''),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "product": new FormControl('', [Validators.required]),
          "product_sub_id": new FormControl('', [Validators.required]),
          "model_no": new FormControl('', [Validators.required]),
          "customer": new FormControl(''),
          "application": new FormControl(''),
          "segment": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['product'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product'].setValue(null);
      this.FilterData.controls['product_sub_id'].setErrors({ 'incorrect': true });
      this.FilterData.controls['product_sub_id'].setValue(null);
      this.FilterData.controls['model_no'].setErrors({ 'incorrect': true });
      this.FilterData.controls['model_no'].setValue(null);
    }
    else if (this.path == 'performance_report') {
      this.page = 'performance_report';
      this.last_months = 'yes';
      this.selectdrop = '4';
      this.selectyear = "2022-2023";
      // this.selectdrop = '2';
      this.finanace_year = 'Yes';
      this.date_month = "No";
      this.date_time = "No";
      this.onChangereporttype(4);
      this.finance_service = "disable";  // set the value
      if (this.role_id == '5') {
        // 5 is manager. In manager all filters are needed
        this.performance_service = "enable";      // set the value

        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl('',),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.states(); // states get from api based on manager employee code
        var data = { "employee_code": this.employee_code }
        this.get_avilable_servicedesk(data); // customer and service desk get from api based on manager employee code
        var data_service = { "employee_code": this.employee_code, "role": 5 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
      }
      else {
        // For service desk. In this state and service desk are not needed
        this.performance_service = "enable";  // set the value
        this.FilterData = new FormGroup({
          // "fromdates": new FormControl(moment()),
          // "todates": new FormControl(moment()),
          "state": new FormControl('', [Validators.required]),
          "region": new FormControl('',),
          "service_desk": new FormControl(''),
          "technician": new FormControl(''),
          "customer": new FormControl('', [Validators.required]),
          "customer_type": new FormControl('6', [Validators.required]),
          "segment": new FormControl(''),
          "application": new FormControl(''),
          "flag": new FormControl(''),
        });
        this.state = [];
        this.service_desk = [];
        this.service_desk_code = [];
        var data_service = { "employee_code": this.employee_code, "role": 4 }
        this.service_technician(data_service); // technician and  get from api based on service desk employee code
        var datas = { "employee_code": this.employee_code, "role": 4 }
        this.get_avilable_servicedesk(datas); // customer get from api based on  employee code
      }
      this.FilterData.controls['region'].setErrors({ 'incorrect': true });
      this.FilterData.controls['region'].setValue(null);
    }
    else {
      this.page = 'no';

    }
    this.segment = [];
    this.CustomerName = [];
    // below settitng for state
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'state_id',
      textField: 'state_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    // below settitng for service desk
    this.dropdownSettingsservice = {
      singleSelection: false,
      idField: 'service_employee_code',
      textField: 'service_desk_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    // below settitng for technician
    this.dropdownSettingstechnician = {
      singleSelection: false,
      idField: 'technician_code',
      textField: 'technician_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    };
    // below settitng for Customer
    this.dropdownSettingscustomer = {
      singleSelection: false,
      idField: 'customer_code',
      textField: 'customer_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter
    }
    // below settitng for Segment
    this.dropdownSettingssegment = {
      singleSelection: false,
      idField: 'segment_id',
      textField: 'segment_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter

    }
  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  // on change function for customer typoe
  get_customer_type(customer_type_id) {
    this.state_customer = [];
    this.select_customer = [];
    this.CustomerName = []; //empty the array while changing the customer type
    var datas = { "employee_code": this.employee_code, "role": 4, "customer_type": customer_type_id }
    if (customer_type_id != '6') {  // 6 is null
      this.customerName(datas)
    }
    else {
      this.state_customer = [];
    }
  }
  // type based customers
  customerName(data) {
    var url = 'get_customer_reports/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.CustomerName = result.response.data;              //storing the api response in the array
        console.log(this.CustomerName, "this.CustomerName")
      }

    }, (err) => {
    });
  }

  get_callcategory() {
    var url = 'get_call_category/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.callcategorydetails = result.response.data;                   //storing the api response in the array
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  getproduct() {
    var url = 'get_product_details_reports/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.product = result.response.data; //storing the api response in the array
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  //for get subproduct in array based on product
  onChangeProduct(product_id) {
    this.model_id = [];
    this.FilterData.controls['product_sub_id'].setValue("null")
    if (product_id == null || product_id == 'null') {
      this.subproduct = [];
    }
    else {
      var product = +product_id; // country: number                                    
      var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
      var id = "product_id";
      this.ajax.getdataparam(url, id, product).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.subproduct = result.response.data;
        }
      }, (err) => {
        console.log(err);                                    //prints if it encounters an error
      });
    }


  }
  getmodel(id) {
    if (this.FilterData.value.product_sub_id == null || this.FilterData.value.product_sub_id == 'null') {
      this.model_id = [];
    }
    else {
      var data = { "product_id": this.FilterData.value.product, "product_sub_id": this.FilterData.value.product_sub_id };// storing the form group value
      var url = 'get_models/'                                         //api url of remove license
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.model_id = result.response.data;
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }


  }
  //states api employee code based 
  states() {
    var data = {
      "employee_code": this.employee_code,
      "country_id": 101
    }
    var state = [];
    var url = 'service_desk_locations/'
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.assignstates = result.response.data;
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.assignstates = []
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onChangereporttype(label) {
    if (label == 1) {
      this.fin_flag = "Month";
      this.finanace_year = 'No';
      this.date_month = "No";
      this.date_time = "No";
      const currentYear = moment().year();
      const currentMonth = moment().month();
      console.log(currentMonth, "currentMonth")
      if (currentMonth > 1) {
        this.date_time = "Yes";
        this.minDate = moment([currentYear, currentMonth - 2, 1]);
        this.maxDate = moment([currentYear, currentMonth, 28]);
        this.start_date = currentYear + "-" + (currentMonth - 1) + "-" + 1;
        this.end_date = currentYear + "-" + (currentMonth + 1) + "-" + 28;
      }
      else {
        this.date_time = "No";
        if (currentMonth == 0) {
          this.minDate = moment([currentYear - 1, currentMonth + 10, 1]);
          this.maxDate = moment([currentYear, currentMonth, 28]);
          this.start_date = (currentYear - 1) + "-" + (currentMonth + 10) + "-" + 1
          this.end_date = currentYear + "-" + (currentMonth + 1) + "-" + 28;
        }
        else if (currentMonth == 1) {
          this.minDate = moment([currentYear - 1, currentMonth + 9, 1]);
          this.maxDate = moment([currentYear, currentMonth, 28]);
          this.start_date = (currentYear - 1) + "-" + (currentMonth + 9) + "-" + 1
          this.end_date = currentYear + "-" + (currentMonth + 1) + "-" + 28;
        }
        else if (currentMonth == 2) {
          this.minDate = moment([currentYear - 1, currentMonth + 8, 1]);
          this.maxDate = moment([currentYear, currentMonth, 28]);
          this.start_date = (currentYear - 1) + "-" + (currentMonth + 8) + "-" + 1
          this.end_date = currentYear + "-" + (currentMonth + 1) + "-" + 28;
        }
      }

      // this.maxDates = moment([currentYear + 1, currentMonth, 28]);
      this.fromdates.setValue(this.minDate);
    }
    else if (label == 2) {
      this.fin_flag = "year";
      this.finanace_year = "Yes";
      this.date_time = "yes";
      this.date_month = 'yes';
      const currentYear = moment().year();
      const currentMonth = moment().month();
      var startYear = (currentYear - 1)
      var startmonth = 1
      var startdate = 1
      // this.date_month = "Yes";
      // this.start_date = "2021-04-01";
      // this.end_date = "2022-03-31";
      this.start_date = currentYear + "-" + 4 + "-" + 1
      this.end_date = (currentYear + 1) + "-" + 3 + "-" + 1
      // this.date_time="No";
      // const currentYear = moment().year();
      this.minDate = moment([currentYear - 1, 4, 1]);
      this.maxDate = moment([currentYear, 3, 28]);
      this.maxDates = moment([currentYear + 1, 3, 28]);
    }
    else if (label == 4) {
      this.fin_flag = "year";
      this.finanace_year = 'no';
      this.date_month = "no";
      this.date_time = "no";
      const currentYear = moment().year();
      const currentMonth = moment().month();
      if (currentMonth > 3) {
        var future = currentYear + 1
        date = currentYear + "-" + future

      }
      else {
        var pre = currentYear - 1
        var date = pre + "-" + currentYear
      }
      this.finance_year.setValue(date)
    }
    else if (label == 3) {
      console.log(label, "label")
      this.fin_flag = "Month";
      this.finanace_year = 'No';
      this.date_month = "No";
      this.date_time = "No";
      const currentYear = moment().year();
      const currentMonth = moment().month();
      this.minDate = moment([currentYear, currentMonth, 1]);
      this.maxDate = moment([currentYear, currentMonth, 28]);
      this.start_date = currentYear + "-" + currentMonth + "-" + 1;
      this.end_date = currentYear + "-" + currentMonth + "-" + 28;
      if (currentMonth > 3) {
        var future = currentYear + 1
        date = currentYear + "-" + future

      }
      else {
        var pre = currentYear - 1
        var date = pre + "-" + currentYear
      }
      this.finance_year.setValue(date)
      this.fromdates.setValue(this.minDate);
      this.todates.setValue(this.maxDate);
      this.end_date = moment(this.maxDate).format("YYYY-MM-DD")
      this.start_date = moment(this.minDate).format("YYYY-MM-DD")
      console.log(moment(this.maxDate).format("YYYY-MM-DD"), "this.maxDate")
    }
    console.log(this.fin_flag, "this.fin_flag ")
  }
  //choose year for start date
  fromchosenYearHandlerfinance(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValuefin = this.fromdates.value;
    ctrlValuefin.year(normalizedMonth.year());
    this.fromdates.setValue(ctrlValuefin);
    this.start_date = moment(this.fromdates.value._d).format("YYYY-MM-DD"); // change to date format
    datepicker.close(); // close the filter
  }
  //choose year for end date
  tochosenYearHandlerfin(normalizedMonthfin: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue1fin = this.todates.value;
    ctrlValue1fin.year(normalizedMonthfin.year());
    // this.todates.setValue(ctrlValue1fin);
    this.todates.setValue(ctrlValue1fin);
    this.end_date = moment(this.todates.value._d).format("YYYY-MM-DD"); // change to date format
    if (this.start_date <= this.end_date) {
      this.message = "";
    }
    else {
      this.message = "To Year must be greater than or equal to from Year";

    }
    datepicker.close(); // close the filter
  }
  //choose year for end date
  fromchosenYearHandler(normalizedYearf: Moment) {
    const ctrlValue5 = this.fromdates.value;
    ctrlValue5.year(normalizedYearf.year());
    this.fromdates.setValue(ctrlValue5);
  }
  //choose year for mfg date
  mfgchosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue2 = this.mfg_date.value;
    ctrlValue2.year(normalizedYear.year());
    this.mfg_date.setValue(ctrlValue2);


  }
  //choose month for mfg date
  mfgchosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue2 = this.mfg_date.value;
    ctrlValue2.month(normalizedMonth.month());
    this.mfg_date.setValue(ctrlValue2);
    this.mfg_dates = moment(this.mfg_date.value._d).format("YYYY-MM-DD"); // change to date format
    // this.date_mfg=this.mfg_dates;
    datepicker.close(); // close the filter
  }
  //choose year for end date
  tochosenYearHandler(normalizedYear1: Moment) {
    const ctrlValue1 = this.todates.value;
    ctrlValue1.year(normalizedYear1.year());
    this.todates.setValue(ctrlValue1);
  }
  //choose month for from date
  fromchosenMonthHandler(normalizedMonthf: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValueM = this.fromdates.value;
    ctrlValueM.month(normalizedMonthf.month());
    this.fromdates.setValue(ctrlValueM);
    this.start_date = moment(this.fromdates.value._d).format("YYYY-MM-DD"); // change to date format
    datepicker.close(); // close the filter
  }
  //choose month for to date
  tochosenMonthHandler(normalizedMonth1: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue1 = this.todates.value;
    ctrlValue1.month(normalizedMonth1.month());
    this.todates.setValue(ctrlValue1);
    this.end_date = moment(this.todates.value._d).format("YYYY-MM-DD"); ///change to date format
    datepicker.close(); // close the filter
  }
  //state select if single or multiple
  onItemSelectState(item: any) {
    var isPresent = this.state.some(function (el) { return el === item.state_id });
    if (isPresent) {
    } else {
      this.state.push(item.state_id);
    }
    this.select_service = [];
    this.select_customer = [];
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_service_details = [];
    this.CustomerName = [];
    this.selecttype = '6';
    this.get_customer_type(6);
    var data = { "employee_code": this.employee_code, "state_id": this.state }
    this.get_avilable_servicedesk(data); //api for service desk and customer are state based
    this.service_desk = [];
    this.technician = [];
    this.state_customer = [];
  }
  onSelectAllState(items: any) {
    var isPresent = this.state.some(function (el) { return el === items.state_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.state.push(value.state_id);
      })
    }
    this.select_service = [];
    this.select_customer = [];
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_service_details = [];
    this.CustomerName = [];
    this.selecttype = '6';
    this.get_customer_type(6);
    var data = { "employee_code": this.employee_code, "state_id": this.state }
    this.get_avilable_servicedesk(data); //api for service desk and customer are state based
    this.service_desk = [];
    this.technician = [];
    this.state_customer = [];
  }
  onItemDeSelectState(item: any) {
    const index: number = this.state.indexOf(item.state_id);
    if (index !== -1) {
      this.state.splice(index, 1); //remove the particular id
    }
    this.select_service = [];
    this.select_customer = [];
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_service_details = [];
    this.CustomerName = [];
    this.selecttype = '6';
    this.get_customer_type(6);
    var data = { "employee_code": this.employee_code, "state_id": this.state }
    this.get_avilable_servicedesk(data); //api for service desk and customer are state based
    this.service_desk = [];
    this.technician = [];
    this.state_customer = [];
  }
  onItemDeSelectAllState(items: any) {
    this.state = []
    this.select_service = [];
    this.select_customer = [];
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_service_details = [];
    this.CustomerName = [];
    this.selecttype = '6';
    this.get_customer_type(6);
    // var data = { "employee_code": this.employee_code, "state_id": [] }
    // this.get_avilable_servicedesk(data); //api for service desk and customer are state based
    this.service_desk = [];
    this.technician = [];
    this.state_customer = [];
  }
  onItemDeSelectAllService(items: any) {
    this.service_desk = [];
    this.select_technician = [];
    this.service_desk_code = [];
    this.select_segment = [];
    this.avilable_technician_details = [];
    var data = { "employee_code": [] }
    this.service_technician(data); // api for technician and segment based on service desk
    this.technician = [];
  }
  onItemDeSelectAllTechnician(items: any) {
    this.technician = [];
  }
  onItemDeSelectService(item: any) {
    const index: number = this.service_desk.indexOf(item.service_employee_code);
    const index1: number = this.service_desk_code.indexOf(item.service_desk_name.split("-")[1]);
    if (index !== -1) {
      this.service_desk.splice(index, 1);  // remove that particular service desk
    }
    if (index1 !== -1) {
      this.service_desk_code.splice(index, 1);  // remove that particular service desk
      // this.service_desk_code.forEach(value => {
      //   // var servicedesk=value.service_desk_name.split("-")
      //    this.service_desk_code.push(value)
      //    console.log(this.service_desk_code,"2")
      //  })
    }
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_technician_details = [];
    var data = { "employee_code": this.service_desk }
    this.service_technician(data); // api for technician and segment based on service desk
    this.technician = [];
  }
  onItemDeSelectTechnician(item: any) {
    const index: number = this.technician.indexOf(item.technician_code);
    if (index !== -1) {
      this.technician.splice(index, 1);  //remove particular technician
    }
  }
  onSelectAllService(items: any) {
    // var service_desk_code=[]
    var isPresent = this.service_desk.some(function (el) { return el === items.service_employee_code });
    var isPresent1 = this.service_desk_code.some(function (el) { return el === items.service_desk_name });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.service_desk.push(value.service_employee_code);
      })
    }
    if (isPresent1) {
    } else {
      items.forEach(value => {
        var servicedesk = value.service_desk_name.split("-")
        this.service_desk_code.push(servicedesk[1]);
      })
    }
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_technician_details = [];
    var data = { "employee_code": this.service_desk }
    this.service_technician(data);
    this.technician = [];

  }
  onSelectAllTechnician(items: any) {
    this.technician = [];
    var isPresent = this.technician.some(function (el) { return el === items.technician_code });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.technician.push(value.technician_code);
      })
    }
  }
  onItemSelectService(item: any) {
    // var service_desk_code=[]
    var isPresent = this.service_desk.some(function (el) { return el === item.service_employee_code });
    var isPresent1 = this.service_desk_code.some(function (el) { return el === item.service_desk_name });
    if (isPresent) {
    } else {
      this.service_desk.push(item.service_employee_code);
    }
    if (isPresent1) {
    } else {
      var servicedesk = item.service_desk_name
      this.service_desk_code.push(servicedesk);
    }
    this.select_technician = [];
    this.select_segment = [];
    this.avilable_technician_details = [];
    var data = { "employee_code": this.service_desk }
    this.service_technician(data);
    this.technician = [];
  }
  onItemSelectTechnician(item: any) {
    var isPresent = this.technician.some(function (el) { return el === item.technician_code });
    if (isPresent) {
    } else {
      this.technician.push(item.technician_code);
    }
  }
  onItemSelectCustomer(item: any) {
    var isPresent = this.state_customer.some(function (el) { return el === item.customer_code });                  // for multi select customer name
    if (isPresent) {
    } else {
      this.state_customer.push(item.customer_code);
    }
    var data = { "employee_code": this.state_customer }                                                             //passed the data in array
    this.getSiteDetails(data);
    // this.state_customer = item.customer_code;
    // console.log(this.state_customer,"this.state_customer")
    // this.getSiteDetails(this.state_customer);
  }
  getSiteDetails(data) {
    var data = data;
    var url = 'get_site_details/';                                  // api url for getting the details
    // this.ajax.postdata(url, data).subscribe((result) => {
    //   if (result.response.response_code == "200" || result.response.response_code == "400") {
    //     this.sitedetails = result.response.data;              //storing the api response in the array
    //     console.log(this.sitedetails)
    //   }
    //   else if (result.response.response_code == "500") {
    //     this.toastr.error(result.response.message, 'Error');        //toastr message for error
    //   }
    //   else {
    //     this.toastr.error("Something went wrong");
    //   }
    // }, (err) => {
    //   console.log(err)      //prints if it encounters an error
    // });
  }
  onItemDeSelectCustomer(item: any) {
    const index: number = this.state_customer.indexOf(item.customer_code);
    if (index !== -1) {
      this.state_customer.splice(index, 1);
    }
    var data = { "employee_code": this.state_customer }
    this.getSiteDetails(data);
  }
  onSelectAllCustomer(items: any) {
    this.state_customer = [];
    var isPresent = this.state_customer.some(function (el) { return el === items.customer_code });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.state_customer.push(value.customer_code);
      })
    }
    var data = { "employee_code": this.state_customer }
    this.getSiteDetails(data);
  }
  onItemDeSelectAllCustomer(items: any) {
    this.state_customer = [];
    var data = { "employee_code": this.state_customer }
    this.getSiteDetails(data);
  }
  onSelectAllSegment(items: any) {
    this.segment_id = [];
    this.application = [];
    this.FilterData.controls['application'].setValue("null");
    var isPresent = this.segment_id.some(function (el) { return el === items.segment_id });
    if (isPresent) {
    } else {
      items.forEach(value => {
        this.segment_id.push(value.segment_id);
      })
    }
    this.getapplication(this.segment_id);
    this.FilterData.value.application = '';
  }
  onItemSelectSegment(item: any) {
    this.FilterData.controls['application'].setValue("null");
    this.application = [];
    var isPresent = this.segment_id.some(function (el) { return el === item.segment_id });
    if (isPresent) {
    } else {
      this.segment_id.push(item.segment_id);
    }
    this.getapplication(this.segment_id);
    this.FilterData.value.application = '';
  }
  onItemDeSelectSegment(item: any) {
    this.FilterData.controls['application'].setValue("null");
    this.application = [];
    const index: number = this.segment_id.indexOf(item.segment_id);
    if (index !== -1) {
      this.segment_id.splice(index, 1);
    }
    this.getapplication(this.segment_id);
    this.FilterData.value.application = '';
  }
  onItemDeSelectAllSegment(items: any) {
    this.FilterData.controls['application'].setValue("null");
    this.segment_id = [];
    this.application = [];
    // this.getapplication(this.segment_id);
    this.FilterData.value.application = '';
  }
  getapplication(segment_id) {
    var data = { "segment_id": segment_id, "flag": 1 };// storing the form group value
    var url = 'get_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.application = result.response.data;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  //api for segment and technician based on service desk
  service_technician(data) {
    var url = 'get_service_technicians/'                                           //api url of Assign Ticket                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.avilable_technician_details = result.response.data; //storing the api response in the array  
          this.segment = result.response.segment;  //storing the api response in the array
        }
        else if (result.response.response_code == "500") {
          this.avilable_technician_details = result.response.data; //storing the api response in the array 
          this.segment = result.response.segment;  //storing the api response in the array
        }
        else {
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
  }

  get_avilable_servicedesk(data) {
    var url = 'get_sd_service/'                                           //api url of Assign Ticket                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.avilable_service_details = result.response.data; //storing the api response in the array    
          // this.CustomerName = result.response.Customer_data;
        }
        else if (result.response.response_code == "500") {
          this.avilable_service_details = result.response.data; //storing the api response in the array
          // this.CustomerName = result.response.Customer_data;

        }
        else {
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
  }
  report() {
    if ((this.FilterData.value.product == 'null' || this.FilterData.value.product == null || this.FilterData.value.product == undefined || this.FilterData.value.product == '' || this.FilterData.value.product == "")) {
      this.FilterData.value.product = [];
      this.FilterData.value.product_sub_id = [];
      this.FilterData.value.model_no = [];
    }
    else {
      this.FilterData.value.product = this.FilterData.value.product;
      if ((this.FilterData.value.product == [] || this.FilterData.value.product == 'null' || this.FilterData.value.product_sub_id == 'null' || this.FilterData.value.product_sub_id == null || this.FilterData.value.product_sub_id == undefined || this.FilterData.value.product_sub_id == '' || this.FilterData.value.product_sub_id == "")) {
        this.FilterData.value.product_sub_id = [];
        this.FilterData.value.model_no = [];
      }
      else {
        this.FilterData.value.product_sub_id = this.FilterData.value.product_sub_id;
        if ((this.FilterData.value.product == [] || this.FilterData.value.product_sub_id == [] || this.FilterData.value.model_no == 'null' || this.FilterData.value.model_no == null || this.FilterData.value.model_no == undefined || this.FilterData.value.model_no == '' || this.FilterData.value.model_no == "")) {
          this.FilterData.value.model_no = [];
        }
        else {
          this.FilterData.value.model_no = this.FilterData.value.model_no;
        }
      }

    }

    if ((this.FilterData.value.call_catgory == null || this.FilterData.value.call_catgory == undefined || this.FilterData.value.call_catgory == '' || this.FilterData.value.call_catgory == "" || this.FilterData.value.call_catgory == "null")) {
      this.FilterData.value.call_catgory = [];
    }
    else {
      this.FilterData.value.call_catgory = this.FilterData.value.call_catgory;
    }
    if ((this.FilterData.value.application == null || this.FilterData.value.application == "null" || this.FilterData.value.application == undefined || this.FilterData.value.application == '' || this.FilterData.value.application == "")) {
      this.FilterData.value.application = [];
    }
    else {
      this.FilterData.value.application = this.FilterData.value.application;
    }
    if ((this.FilterData.value.voltage == 'null' || this.FilterData.value.voltage == null || this.FilterData.value.voltage == undefined || this.FilterData.value.voltage == '' || this.FilterData.value.voltage == "")) {
      this.FilterData.value.voltage = [];
    }
    else {
      this.FilterData.value.voltage = this.FilterData.value.voltage;
    }
    if (this.FilterData.value.mfg_date == 'null' || this.FilterData.value.mfg_date == null || this.FilterData.value.mfg_date == undefined || this.FilterData.value.mfg_date == "") {
      this.FilterData.value.mfg_date = moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    }
    else {
      this.FilterData.value.mfg_date == this.FilterData.value.mfg_date
    }
    if (this.finance_year.value == null) {
      const currentYear = moment().year();
      const currentMonth = moment().month();
      console.log(currentMonth, 'currentMonth')
      if (currentMonth > 4) {
        var previous_Year = (currentYear + 1)
        var finance_year = currentYear + "-" + previous_Year
      }
      else {
        var previous_Year = (currentYear - 1)
        var finance_year = previous_Year + "-" + currentYear
      }

    }
    else {
      finance_year = this.finance_year.value
    }
    var start = this.start_date.split("-", 3);
    var start_date = start[0] + "-" + start[1] + "-" + 1
    var end = this.end_date.split("-", 3);
    var end_month = end[1]
    // if end_month=='12':
    var end_date = end[0] + "-" + end[1] + "-" + 28
    console.log(this.state_customer, "this.state_customer")
    console.log(this.FilterData.value.customer_type, "this.FilterData.value.customer_type")
    if (this.state_customer == [] || this.state_customer == "null" || this.state_customer == "") {
      console.log("dfsdf")
      console.log(this.FilterData.value.customer_type, "this.FilterData.value.customer_type")
      if (this.FilterData.value.customer_type != 6) {
        console.log("tyjt")
        console.log(this.CustomerName, "this.CustomerName")
        this.CustomerName.forEach(e => {
          this.state_customer.push(e.customer_code);
        })
      }
    }
    console.log(this.state_customer, "this.sdddasdas")
    this.datas = {
      "fin_flag": this.fin_flag,
      // "finance_year": finance_year,
      "start_date": start_date,
      "end_date": end_date,
      "state": this.state,
      "service_desk": this.service_desk,
      "technician": this.technician,
      "customer": this.state_customer,
      "application": this.FilterData.value.application,
      "segment": this.segment_id,
      "employee_code": this.employee_code,
      "product": this.FilterData.value.product,
      "product_sub_id": this.FilterData.value.product_sub_id,
      "model_no": this.FilterData.value.model_no,
      "region": this.FilterData.value.region,
      "call_catgory": this.FilterData.value.call_catgory,
      "flag": 2,
      "voltage": this.FilterData.value.voltage,
      "mfg_date": this.mfg_dates
    }
    this.loadingUpd = true;
    if (this.page == 'tat') {
      var fin_split = finance_year.split("-", 2);
      var fin_previous = +fin_split[0] - 1
      this.datas["finance_year"] = fin_previous + "-" + fin_split[1]
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'csl') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'finance') {
      var fin_split = finance_year.split("-", 2);
      var fin_previous = +fin_split[0] - 1
      this.datas["finance_year"] = fin_previous + "-" + fin_split[1]
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'replace') {
      var fin_split = finance_year.split("-", 2);
      var fin_previous = +fin_split[0] - 1
      this.datas["finance_year"] = fin_previous + "-" + fin_split[1]
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'complaint') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'csl_analysis') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'ftr') {
      this.datas["finance_year"] = finance_year
      this.ftr_report.ftrreportdaata(this.datas);
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'competitor') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'reject_reason') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'locationwise_report') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'performance_report') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'ticket_age') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);

    }
    else if (this.page == 'battery-age') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);
    }
    else if (this.page == 'battery-replace') {
      this.datas["finance_year"] = finance_year
      this.updateDataRequest.emit(this.datas);
    }
    setTimeout(() => {
      this.loadingUpd = false;
    }, 3);
  }
}
