import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReimbursmentformComponent } from './reimbursmentform.component';

describe('ReimbursmentformComponent', () => {
  let component: ReimbursmentformComponent;
  let fixture: ComponentFixture<ReimbursmentformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReimbursmentformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReimbursmentformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
