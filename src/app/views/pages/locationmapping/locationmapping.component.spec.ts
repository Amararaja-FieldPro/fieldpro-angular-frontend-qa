import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationmappingComponent } from './locationmapping.component';

describe('LocationmappingComponent', () => {
  let component: LocationmappingComponent;
  let fixture: ComponentFixture<LocationmappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationmappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationmappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
