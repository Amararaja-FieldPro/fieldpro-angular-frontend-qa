import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { NgbModal, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import * as _ from 'lodash';
import { Http, RequestOptions, Headers } from '@angular/http';

@Component({
  selector: 'kt-spare',
  templateUrl: './spare.component.html',
  styleUrls: ['./spare.component.scss']
})
export class SpareComponent implements OnInit {
  sparedetails: any;
  Editsparecode_details: any;
  sparecode_details: any;
  message: string;
  disabled: boolean = false;
  exp_error_add: any;
  exp_error: any;
  spare_image: any;
  edit_expireddate: any;
  expiry_date: any;
  year: number;
  day: number;
  month: number;
  file_name: any;                        //To use bulk upload
  selectedfile = true;                            //selected image
  cropped_img = true;                             //cropping
  sparemasterdetails: any;                       //spare details from get API
  product_details: any;                       //products from gt API
  warehousedetails: any;                              //warehouse from gt API
  subproduct_details: any                                //subproducts from gt API
  Addsparemaster: FormGroup;              //form group for create sparemaster
  Editsparemaster: FormGroup;                                //form group for update sparemaster
  spare: any;                                        //get data for delete API
  edit_spare: any;                                        //get data foe edit API
  submitted = false;
  spareImage: any;
  showTable: boolean = false;                  //loader show and hide
  overlayRef: OverlayRef;                       //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;  //loader component  
  Purchasedate: any;
  load: any;
  filename: any;
  Filename: any;
  ByDefault: boolean = false;
  ByDefaults: Boolean = false;
  type: any;
  Purchase: any;
  edit_purchasedate: any;
  edit_expiredate: any;
  error: any;
  loading: boolean = false;
  loadingAdd: boolean = false;
  loadingBulk: boolean = false;
  loadingEdit: boolean = false;
  errors: any;
  from: any;
  to: any;
  purchase: any;
  expire: any;
  maxChars: number;
  chars: number;
  role: any;
  bulk: any;
  bulks: any;
  today: any;
  purch_error: string;
  purchases: any;
  expires: any;
  epurchase: string;
  expiry: string;
  buttonVisibleEdit: boolean = true;
  buttonAdd: boolean = false;
  buttonEdit: boolean = false;
  imageError: string;
  isImageSaved: boolean = false;
  isImageSaveds: boolean = false;
  cardImageBase64: string;
  Filenames: string;
  dash: string;
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef, private calendar: NgbCalendar) {
    this.from = this.calendar.getToday();
    this.to = this.calendar.getToday();
    this.purchases = this.calendar.getToday();
    this.expires = this.calendar.getToday();
    //create subscription form group
    this.Addsparemaster = new FormGroup({
      "spare_code": new FormControl("", [Validators.required]),
      "spare_name": new FormControl("", [Validators.required]),
      "product_name": new FormControl("", [Validators.required]),
      "subproduct_name": new FormControl("", [Validators.required]),
      "warehouse_id": new FormControl("", [Validators.required]),
      "quantity_purchased": new FormControl("", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.min(1)]),
      "image": new FormControl(""),
      "purchase_date": new FormControl(this.from, [Validators.required]),
      "expiry_date": new FormControl(this.to, [Validators.required]),
      "spare_model": new FormControl("", [Validators.required]),
      "price": new FormControl("", [Validators.required, Validators.min(1)])
    });
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    this.maxChars = 1024;
    this.role = '';
    this.chars = 0;
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getsparemasterdetails();
    this.getproductmasterdata();
    this.getwarehousedetails();
    this.purshasedate();
  }
  // -----------------------------------------------------------------------------------------------------------//
  getsparemasterdetails() {
    this.errors = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_stocks/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200")                             //if sucess
      {
        this.sparedetails = result.response.data;                    //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.errors = result.response.message;
      }
      else {
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.errors = result.response.message;
      }
    }, (err) => {                                                        //if error
      console.log(err);
      this.overlayRef.detach();                                       //prints if it encounters an error
    });
  }
  purshasedate() {
    var today = this.calendar.getToday();
    this.Purchasedate = today.year + "-" + today.month + "-" + today.day;
    this.Addsparemaster.controls['purchase_date'].setValue(this.Purchasedate);          //set the date for purchase  date
  }
  getwarehousedetails() {
    this.errors = '';
    var url = 'get_warehouse/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400")                           //if sucess
      {
        this.warehousedetails = result.response.data;                    //storing the api response in the array
      }
      else if (result.response.response_code == "500") {

        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                  //prints if it encounters an error
    });
  }
  getproductmasterdata() {
    this.errors = '';
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {                       //if sucess
        this.product_details = result.response.data;                    //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {                                                          //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // -------------------------------------------------------------------------------------------------------------------------------------------------//
  // Function for modal box with spare image
  openlargeimage(content6, image_value) {
    this.errors = '';
    this.spareImage = image_value;
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                   // modal popup for resizing the popup                                                //specifies the size of the modal box                                                //specifies the size of the modal box
      backdrop: 'static',
      keyboard: false,
    });

  }
  // --------------------------------------------------------------------------------------------------------------------------------------------//
  // -------------------------------------------------------------------------------------------------------------------------------------------------//
  // Function for modal box 
  openLarge(content6) {
    this.sparemaster();
    this.ByDefault = false;
    this.isImageSaved = false;
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.imageError = '';
    this.message = '';
    this.buttonAdd = false;
    this.buttonEdit = true;
    this.load = "";
    this.from = this.calendar.getToday();
    this.to = this.calendar.getToday();
    this.exp_error_add = "";
    this.purch_error = "";
    this.errors = '';
    this.Addsparemaster.reset();
    this.purshasedate();
    this.Filename = '';
    this.Filenames = '';
    this.modalService.open(content6, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
    });
  }
  openAdd(content6) {
    this.errors = '';
    this.modalService.open(content6, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      windowClass: "center-modalheight"                                    // modal popup for resizing the popup                                                //specifies the size of the modal box                                                //specifies the size of the modal box
    });
  }
  openBulk(content6) {
    this.ByDefault=false;
    this.error = "";
    this.loadingBulk = false;
    this.disabled = false;
    this.bulk = '';
    this.bulks = '';
    this.errors = '';
    // this.file_name = undefined;
    // this.Filename = '';
    this.modalService.open(content6, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      windowClass: "center-modalsm"                                    // modal popup for resizing the popup                                                //specifies the size of the modal box                                                //specifies the size of the modal box
    });
  }
  // --------------------------------------------------------------------------------------------------------------------------------------------//

  // ---------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // convenience getter for easy access to form fields
  get log() {
    return this.Editsparemaster.controls;          //error controls for create spare
  }
  get add() {
    return this.Addsparemaster.controls;          //error controls for create spare
  }
  sparemaster() {
    var url = 'get_spare_master/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200")                             //if sucess
      {
        this.sparemasterdetails = result.response.data;                    //storing the api response in the array

      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error(result.response.message, 'Error')
        this.errors = result.response.message;
      }
    }, (err) => {
      this.toastr.error('Something went wrong', 'Error')                                                        //if error
      console.log(err);                                     //prints if it encounters an error
    });
  }
  //Add sparemaster using API 
  addspare() {
    console.log(this.Addsparemaster)
    console.log(this.Addsparemaster.value)
    this.errors = '';
    this.loadingAdd = true //adding the spinner in submit button
    var year = this.to.year;                  //separate the year
    var month = this.to.month;                         //separate the month
    var day = this.to.day;                    //separate the day
    var Expiredate = year + "-" + month + "-" + day;                                    //date which is used in India 
    var years = this.from.year;                  //separate the year
    var months = this.from.month;                         //separate the month
    var days = this.from.day;                    //separate the day
    var Purchaserdate = years + "-" + months + "-" + days;                                    //date which is used in India 
    var data = this.Addsparemaster.value;                  //Data to be passed for add api
    data["spare_image"] = this.cardImageBase64;                 //set the image
    data["purchase_date"] = Purchaserdate;
    data["expiry_date"] = Expiredate;
    if (this.load == '') {
      var today = this.calendar.getToday();
      var tyears = today.year;                  //separate the year
      var tmonths = today.month;                         //separate the month
      var tdays = today.day;                    //separate the day
      this.today = tyears + "-" + tmonths + "-" + tdays;
      console.log(new Date(this.Addsparemaster.value.expiry_date).getTime(), "today date");
      console.log(new Date(this.today).getTime());
      console.log (data," data)")
      if (new Date(this.Addsparemaster.value.purchase_date).getTime() == new Date(this.today).getTime() || new Date(this.Addsparemaster.value.purchase_date).getTime() < new Date(this.today).getTime()) {
        this.purch_error = "";
        if (new Date(this.Addsparemaster.value.expiry_date).getTime() > new Date(this.Addsparemaster.value.purchase_date).getTime()) {
          this.exp_error_add = "";
          var url = 'add_stocks/';                                       //api url for add
          console.log(data);
          this.ajax.postdata(url, data).subscribe((result) => {
            if (result.response.response_code == "200") {                   //if sucess
              this.loadingAdd = false //dismissing  the spinner in submit button
              this.showTable = false;
              this.chRef.detectChanges();                                         //reloading the component
              this.getsparemasterdetails();
              this.toastr.success(result.response.message, 'Success');    //toastr message for success
              this.modalService.dismissAll();
              this.purshasedate();
              this.errors = "";
            }
            else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
              this.loadingAdd = false //dismissing  the spinner in submit button
              this.errors = result.response.message;                        //toastr message for error
            }
            else {                                                           //if not sucess
              this.loadingAdd = false //dismissing  the spinner in submit button
              this.errors = result.response.message;     //toastr message for error
            }
          }, (err) => {                                                        //if error
            console.log(err);                                             //prints if it encounters an error
          });
        }
        else {
          this.loadingAdd = false
          this.exp_error_add = "expire date is greater than purchase date";
        }
      }
      else {

        this.purch_error = "purchase date is must be less than or equal to today date"
        this.loadingAdd = false
      }
    }
    else {
      this.loadingAdd = false

    }
  }
  expValid(data) {
    console.log(data);
  }
  purValid(data) {
    console.log(data)
  }
  fileChangeEvents(fileInput: any) {

    if (this.Editsparemaster) {
      this.ByDefaults = true;
      this.dash = fileInput.target.files[0].name
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaveds = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.Editsparemaster.controls['image'].setErrors({ 'incorrect': true });
          this.Editsparemaster.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaveds = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.Editsparemaster.controls['image'].setErrors({ 'incorrect': true });
          this.Editsparemaster.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaveds = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.Editsparemaster.controls['image'].setErrors({ 'incorrect': true });
              this.Editsparemaster.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaveds = false;
            } else {
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaveds = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
    if (this.Addsparemaster) {
      this.Filename = fileInput.target.files[0].name;
      this.ByDefault = true;
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.Addsparemaster.controls['image'].setErrors({ 'incorrect': true });
          this.Addsparemaster.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.Addsparemaster.controls['image'].setErrors({ 'incorrect': true });
          this.Addsparemaster.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.Addsparemaster.controls['image'].setErrors({ 'incorrect': true });
              this.Addsparemaster.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.Addsparemaster.value.invalid = false;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }



  }

  //function for bulk upload in sparemaster
  fileUpload(event) {
    this.type = '';
    // this.Filename = '';
    // this.file_name = '';
    this.ByDefault = true;
    this.error = "";
    // this.bulk = [];
    // this.errors = '';
    //read the file
    // this.error = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    console.log(this.Filename)
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];

  };

  //after submit the file for sparemaster
  filesubmit(bulkupload) {
    this.bulk = [];
    this.bulks = [];
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined || this.file_name == '') {
      // this.toastr.error("Please select file", 'Error');
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {                   //if file is not empty
      if (validExts[0] != this.type) {
        // this.toastr.error("Please select correct extension", 'Error');
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) {
        this.loadingBulk = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        var gh = file;
        var method = "post";                                    //post method 
        var url = 'spare_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
           
            if (data['response'].fail.length > 0) {                                    //failure response 
              //if employee code or employee id is error to put this
              for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );
                });

                this.bulk = a;
                // this.file_name = '';
                // this.ByDefault = false;
              }
            }
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {                         //sucess response
              for (i = 0; i < data['success'].length; i++) {
                // alert(data['success'][i])
                // this.bulk = '';
                var a = [];
                data['success'].forEach(value => {
                  a.push(value.success_message
                  );
                });
                this.bulks = a;
                this.loadingBulk = false;
                // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.showTable = false;
                this.chRef.detectChanges();
                this.getsparemasterdetails();
                // this.file_name = '';
                // this.ByDefault = false;

              }
            }
            this.loadingBulk = false;
          }

          //if other errors
          else if (data['response'].response_code == "500") {
            this.bulk = [];
            this.bulks = [];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = data['response'].message
          }
          else {
            this.bulk = [];
            this.bulks = [];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = "Something Went Wrong";                                                                  //if not sucess
          }
        },

          (err) => {
            this.bulk = [];
            this.bulks = [];
            // this.file_name = '';
            // this.ByDefault = false;                                                                 //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }
   
  }
  // ----------------------------------------------------------------------------------------------------------------------------//
  //edit function for spare
  editspare(data) {
    this.spare_image = '';
    this.dash = "Image"
    this.Filenames = 'Image';
    this.errors = '';
    this.edit_spare = data;
    console.log(this.edit_spare)
    this.chRef.detectChanges();
    
    // this.isImageSaveds = true;
    this.cardImageBase64 = "";
    console.log(this.edit_spare.image)
    if(this.edit_spare.image==null){
      this.isImageSaveds = false;
      this.ByDefaults = false;
    }
    else{
      this.isImageSaveds = true;
      this.ByDefaults = true;
    }
    if (this.edit_spare.expiry_date == null || this.edit_spare.expiry_date == undefined) {
      var today = this.calendar.getToday();
      var year = today.year;                  //separate the year
      var month = today.month;                         //separate the month
      var day = today.day;                    //separate the day
      var expire = year + "-" + month + "-" + day;

      this.expire = {
        year: year,
        month: month,
        day: day,
      }
    } else {
      if (this.edit_spare.expiry_date) {
        var rese = this.edit_spare.expiry_date.split("-");
        this.expiry = (rese[2] + "-" + rese[1] + "-" + rese[0])
        var splitted = this.expiry.split("-", 3);
        var year_date = splitted[2].split(",", 1)
        var year: number = +year_date[0];
        var day: number = +splitted[0];
        var month: number = +splitted[1];
        this.expire = {
          year: day,
          month: month,
          day: year,
        }
      }      // return date;
      console.log(this.expire)
    }

    if (this.edit_spare.purchase_date) {
      var res = this.edit_spare.purchase_date.split("-");
      this.epurchase = (res[2] + "-" + res[1] + "-" + res[0]);
      var splitted = this.epurchase.split("-", 3);
      var year_date = splitted[2].split(",", 1)
      var year: number = +year_date[0];
      var day: number = +splitted[0];
      var month: number = +splitted[1];
      this.purchase = {
        year: day,
        month: month,
        day: year,
      }
      console.log(this.purchase)
    }
    console.log(this.ByDefaults)
    console.log(this.Filenames)
    console.log(this.edit_spare.image)
    this.spare_image = environment.image_static_ip + this.edit_spare.image;
    console.log(this.spare_image)
    this.Editsparemaster = new FormGroup({
      "stock_id": new FormControl(this.edit_spare.stock_id, [Validators.required]),
      "spare_code": new FormControl(this.edit_spare.spare_code, [Validators.required, this.noWhitespace]),
      "spare_name": new FormControl(this.edit_spare.spare_name, [Validators.required, this.noWhitespace, Validators.maxLength(48)]),
      "product_name": new FormControl(this.edit_spare.product_name, [Validators.required]),
      "subproduct_name": new FormControl(this.edit_spare.subproduct_name, [Validators.required]),
      "warehouse_id": new FormControl(this.edit_spare.warehouse.warehouse_id, [Validators.required]),
      "quantity_purchased": new FormControl(this.edit_spare.quantity_purchased, [Validators.required, Validators.min(1), Validators.pattern("^[0-9]*$")]),
      "purchase_date": new FormControl(this.purchase, [Validators.required]),
      "expiry_date": new FormControl(this.expire, [Validators.required]),
      "image": new FormControl(""),
      "spare_model": new FormControl(this.edit_spare.spare_model, [Validators.required]),
      "price": new FormControl(this.edit_spare.price, [Validators.required, Validators.min(1)]),

    });
  }
  removeImage() {

    if (this.Editsparemaster) {
// alert("gg")
      // this.Editsparemaster.controls['image'].setErrors({ 'incorrect': true });
      // this.Editsparemaster.value.invalid = true;
      this.isImageSaveds = false;
      this.ByDefaults = false;
      this.cardImageBase64 = null;
    }
    else if (this.Addsparemaster) {
      // this.Addsparemaster.controls['spare_image'].setErrors({ 'incorrect': true });
      // this.Addsparemaster.value.invalid = true;  
      this.isImageSaved = false;     
      this.ByDefault = false;
      this.cardImageBase64 = null;

    }
  }
  editsparemaster() {
    this.loadingEdit = true;
    console.log(this.expire);
    console.log(this.Editsparemaster.value.expiry_date)
    this.errors = '';
    this.exp_error = '';
    this.loading = true;    //adding spinner in submit button 
    var year = this.Editsparemaster.value.expiry_date.year;                  //separate the year
    var month = this.Editsparemaster.value.expiry_date.month;                         //separate the month
    var day = this.Editsparemaster.value.expiry_date.day;                    //separate the day
    var Expiredate = year + "-" + month + "-" + day;                                    //date which is used in India 
    var years = this.Editsparemaster.value.purchase_date.year;                  //separate the year
    var months = this.Editsparemaster.value.purchase_date.month;                         //separate the month
    var days = this.Editsparemaster.value.purchase_date.day;                    //separate the day
    var Purchaserdate = years + "-" + months + "-" + days;                                    //date which is used in India 
    // var data = this.Editsparemaster.value;                  //Data to be passed for add api
    if (this.cardImageBase64 == '' || this.cardImageBase64 == '' || this.cardImageBase64 == undefined || this.spare_image == this.Editsparemaster.value.image) {
      this.Editsparemaster.value.image = '';
    }
    else {
      this.Editsparemaster.value.image = this.cardImageBase64;
    }
    var data = this.Editsparemaster.value;
    data["purchase_date"] = Purchaserdate;
    data["expiry_date"] = Expiredate;
    if (this.load == '') {
      var today = this.calendar.getToday();
      var tyears = today.year;                  //separate the year
      var tmonths = today.month;                         //separate the month
      var tdays = today.day;                    //separate the day
      this.today = tyears + "-" + tmonths + "-" + tdays;
      console.log(new Date(this.Editsparemaster.value.expiry_date).getTime(), "today date");
      console.log(new Date(this.today).getTime());
      if (new Date(this.Editsparemaster.value.purchase_date).getTime() == new Date(this.today).getTime() || new Date(this.Editsparemaster.value.purchase_date).getTime() < new Date(this.today).getTime()) {
        this.purch_error = "";
        // alert("go purchase")
        if (new Date(this.Editsparemaster.value.expiry_date).getTime() > new Date(this.Editsparemaster.value.purchase_date).getTime()) {
          //  alert("go")
          this.exp_error_add = "";
          var url = 'edit_stocks/';                                       //api url for add
          console.log(data);
          this.ajax.postdata(url, data).subscribe((result) => {
            if (result.response.response_code == "200") {                   //if sucess
              this.loadingEdit = false //dismissing  the spinner in submit button
              this.showTable = false;
              this.chRef.detectChanges();                                         //reloading the component
              this.getsparemasterdetails();
              this.toastr.success(result.response.message, 'Success');    //toastr message for success
              this.loadingEdit = false;
              this.modalService.dismissAll();
              this.purshasedate();
              this.errors = "";
            }
            else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
              this.loadingEdit = false //dismissing  the spinner in submit button
              this.errors = result.response.message;                        //toastr message for error
              this.loadingEdit = false;
            }
            else {                                                           //if not sucess
              this.loadingEdit = false //dismissing  the spinner in submit button
              this.errors = result.response.message;     //toastr message for error
              this.loadingEdit = false;
            }
          }, (err) => {                                                        //if error
            console.log(err);                                             //prints if it encounters an error
          });
        }
        else {
          this.loadingEdit = false
          this.exp_error_add = "expire date is greater than purchase date";
        }
      }
      else {
        this.loadingEdit = false
        this.purch_error = "purchase date is must be less than or equal to today date";
      }
    }
    else {
      this.loadingEdit = false;

    }
  }
  // ------------------------------------------------------------------------------------------------------------//
  //delete function for spare master
  trashspare(data) {
    this.errors = '';
    this.spare = data;   
    console.log(this.spare)                                    //get the data
    var stock_id = this.spare.stock_id;                       //compare id    
    var stockid = { "stock_id": stock_id };                            //get the spare id                                 
    var url = 'stock_delete/?';             // api url for getting the details with using post params
    var id = "stock_id";                        //id for API url
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.showTable = false;
        this.ajax.postdata(url, stockid).subscribe((result) => {
          if (result.response.response_code == "200") {                     //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.getsparemasterdetails();
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                   //if failure
            this.showTable = true;
            this.errors = result.response.message;           //toastr message for error
          }
          else {                                                           //if not sucess
            this.showTable = true;
            this.errors = result.response.message;          //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  onChangeSpareEdit(spare_code) {
    console.log(spare_code)
    var data = { "spare_code": spare_code };
    var url = 'get_spare_master_details/?'; // api url for getting the details with using post params
    var id = "spare_code";
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.Editsparecode_details = result.response.data;
        this.Editsparemaster.patchValue({
          "spare_name": this.Editsparecode_details.spare_name,
          "spare_model": this.Editsparecode_details.spare_model,
          "product_name": this.Editsparecode_details.product.product_name,
          "subproduct_name": this.Editsparecode_details.subproduct.subproduct_name,


        });
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error(result.response.message, 'Error')

      }
    }, (err) => {
      this.toastr.error('Something Went Wrong', 'Error')
      console.log(err); //prints if it encounters an error
    });
  }
  onChangespare(spare_code) {
    console.log(spare_code)
    var data = { "spare_code": spare_code };
    var url = 'get_spare_master_details/?'; // api url for getting the details with using post params
    var id = "spare_code";
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.sparecode_details = result.response.data;
        this.onChangeProduct(this.sparecode_details.product.product_id);

        this.Addsparemaster = new FormGroup({
          "spare_code": new FormControl(spare_code, [Validators.required]),
          "spare_name": new FormControl(this.sparecode_details.spare_name, [Validators.required]),
          "product_name": new FormControl(this.sparecode_details.product.product_name, [Validators.required]),
          "subproduct_name": new FormControl(this.sparecode_details.subproduct.subproduct_name, [Validators.required]),
          "warehouse_id": new FormControl(this.Addsparemaster.value.warehouse_id, [Validators.required]),
          "quantity_purchased": new FormControl(this.Addsparemaster.value.quantity_purchased, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.min(1)]),
          "image": new FormControl(this.Addsparemaster.value.image),
          "purchase_date": new FormControl(this.Addsparemaster.value.purchase_date, [Validators.required]),
          "expiry_date": new FormControl(this.Addsparemaster.value.expiry_date, [Validators.required]),
          "spare_model": new FormControl(this.sparecode_details.spare_model, [Validators.required]),
          "price": new FormControl(this.Addsparemaster.value.price, [Validators.required, Validators.min(1)])
        });



      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error(result.response.message, 'Error')
      }
    }, (err) => {
      this.toastr.error('Somethink Went Wrong', 'Error')
      console.log(err); //prints if it encounters an error
    });
  }
  onChangeProduct(product_id) {
    this.errors = '';
    var product = +product_id; // country: number
    var url = 'get_subproduct_details/?'; // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.subproduct_details = result.response.data;
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //saranya24-dec-2020 for alphabet block in keyboard
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    // console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
