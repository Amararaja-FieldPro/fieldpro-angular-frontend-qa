import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingscoreComponent } from './trainingscore.component';

describe('TrainingscoreComponent', () => {
  let component: TrainingscoreComponent;
  let fixture: ComponentFixture<TrainingscoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingscoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingscoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
