import { Component, OnInit, ViewContainerRef, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { NgbModal, NgbDateStruct, NgbDate, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes

// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ExcelService } from './../../../excelservice';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import * as _ from 'lodash';
import { Location } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { stringify } from '@angular/compiler/src/util';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
declare var require: any
const FileSaver = require('file-saver');
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Component({
  selector: 'kt-customermanagemant',
  templateUrl: './customermanagemant.component.html',
  styleUrls: ['./customermanagemant.component.scss']
})
export class CustomermanagemantComponent implements OnInit {
  subproducts_data: any;
  validation_data_contact: any;
  contract_details: any       //variable for contract details from get API
  customer_details: any;   //variable for customer details from get API
  customer_detail: any;   //variable for customer details from get API
  contract_detail: any;//variable for customer details from get API based on contract_id
  file_name: any;   //variable for bulk upload
  Edit_customer: FormGroup;  //variable for edit customer details
  Edit_contract_form: FormGroup;  //Formgroup variable for edit contract details
  EmailGroup: FormGroup; //Formgroup variable for generate login
  UploadCertificate: FormGroup //Formgroup variable for upload performance certificate
  edit_data: any;
  id: any;                        //variable to store the row id 
  states: any;
  type: any;
  cities: any;
  country: any;
  node_value: any;
  product: any;
  subproduct: any;
  contrac_type: any;               //variable to store all the amc contract type
  customerdetail: any;
  submitted = false;
  showTable: boolean = false;      // table hide and show
  showTables: boolean = false;
  overlayRef: OverlayRef;         // for loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  filename: any;
  Filename: any;
  end_date: any;
  ByDefault: boolean = false;
  ByDefault1: boolean = false;
  error: string;
  activeId: any;
  preId: any;
  activerId: any;
  prerId: any;
  evt: any;
  bulks:any;
  params: any;
  validation_data: any;
  bulk: any;
  errors: any;
  customer_code: any;
  // bulks: any;
  dtOptions1: DataTables.Settings = {};
  contract_message: any;
  loadingBulk: boolean = false;
  alternate: string;
  contact: string;
  bulkboolean: boolean = false;
  loadingUpdate: boolean = false;
  dropdownProductSettings: any = {};
  ShowFilter = true;
  dropdownSettings: any = {};
  products: any = [];
  select: any = [];
  productsall: any = [];
  dropdownsubProductSettings: any = {};
  // sub_product: any;
  disabled = false;
  product_label: string;
  product_name: any;
  value_disabled: boolean;
  video_word: any;
  pdf: any;
  error_type: string;
  uploadname: any;
  size: number;
  perf_certi: any;
  login_flag: any;
  video_word_name: any;
  upload_disable: boolean = false;
  battery_bank_details: any;
  multi_details: any;
  SerialDetails: FormGroup;
  application: any;
  segment: any;
  model_id: any[];
  serial_error: string;
  flag: any;
  warranty_type: any[];
  products_id: any;
  subid: any;
  mfg_date: any;
  contract_data: any;
  warranty_id: any;
  labelwarranty: any;
  invice_date: NgbDateStruct;
  AddressDetails: FormGroup;
  form: any;
  employee_code: string;
  serial: any;
  productdata: any;
  serialProduct: any;
  battery_bank_id: any;
  site_id: any;
  locations: any;
  site_flag: any;
  contract_id: any;
  batteryBankArray: any = [];
  serial_data: any;
  scontract_id: any;
  contract_details_view: any;
  index: any;
  private element: any;
  IsmodelShow: boolean = false; 
  httplink: string;
  customerexcel: string;
  site_code: any;
  return_draw: number;
  settingsObj: { deferLoading: any; processing: boolean; serverSide: boolean; serverMethod: string; ajax: (dataTablesParameters: any, callback: any) => void; columnDefs: { name: string; targets: number; searchable: boolean; }[]; };
  @ViewChild('dataTable', { static: true }) table;

  constructor(private modalService: NgbModal,private activeModalService: NgbActiveModal ,
    public ajax: ajaxservice,
    private toastr: ToastrService,
    vcr: ViewContainerRef, private el: ElementRef,
    private excelservice: ExcelService, private config: NgbDatepickerConfig, public activeModal: NgbActiveModal, private router: Router, public _location: Location, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    this.dtOptions1 = {
      pagingType: 'full_numbers',
      pageLength: 5,
      lengthMenu: [5, 10, 15]
    };

    this.element = el.nativeElement;
    this.Edit_customer = new FormGroup({
      "customer_id": new FormControl(""),
      "customer_code": new FormControl(""),
      "customer_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.maxLength(48)]),
      "email_id": new FormControl("", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl("", [this.noWhitespace]),
    });
    // this.Edit_contract_form = new FormGroup({
    //   "contract_type_id": new FormControl("", [Validators.required]),
    //   "contract_id": new FormControl("", [Validators.required]),
    //   "plot_number": new FormControl("", [Validators.required, this.noWhitespace]),
    //   "contract_duration": new FormControl("", [Validators.required]),
    //   "model_no": new FormControl("", [Validators.required, this.noWhitespace]),
    //   "serial_no": new FormControl("", [Validators.required, this.noWhitespace]),
    //   "product_id": new FormControl("", [Validators.required]),
    //   "product_sub_id": new FormControl("", [Validators.required]),
    //   "location_id": new FormControl("", [Validators.required]),
    //   "street": new FormControl("", [Validators.required, this.noWhitespace]),
    //   "landmark": new FormControl(''),
    //   "post_code": new FormControl("", [Validators.required]),
    //   "country_id": new FormControl("", [Validators.required]),
    //   "state_id": new FormControl("", [Validators.required]),
    //   "city_id": new FormControl("", [Validators.required]),

    // });
    this.SerialDetails = new FormGroup({
      "product_id": new FormControl('', [Validators.required]),
      "product_sub_id": new FormControl('', [Validators.required]),
      "model_no": new FormControl('', [Validators.required]),
      "serial_no": new FormControl(''),
      // "invoice_date": new FormControl(null),
      "contract_duration": new FormControl(''),
      "battery_bank_id": new FormControl('', [Validators.required]),
      "sys_ah": new FormControl('', [Validators.required]),
      "segment_id": new FormControl('', [Validators.required]),
      "application_id": new FormControl('', [Validators.required]),
      "organization_name": new FormControl('', [Validators.required]),
      "invoice_number": new FormControl(''),
      "expiry_day": new FormControl(''),
      "warranty_type_id": new FormControl('', [Validators.required]),
      "mfg_date": new FormControl('', [Validators.required]),
      "voltage": new FormControl("", [Validators.required]),
      "site_id": new FormControl("", [Validators.required]),
      'contract_id': new FormControl(""),
      'flag': new FormControl(""),
      "serial_no_details": new FormControl("")
    });

    this.AddressDetails = new FormGroup({
      "employee_code": new FormControl(""),
      "customer_code": new FormControl(""),
      "customer_name": new FormControl('', [Validators.required, this.noWhitespace]),
      "email_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_number": new FormControl('', [this.noWhitespace]),
      "sap_reference_number": new FormControl(""),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl('', [Validators.required]),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required]),
      "site_id": new FormControl(""),
      'contract_id': new FormControl(""),
      'flag': new FormControl(""),
    })

    this.EmailGroup = new FormGroup({
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "customer_code": new FormControl("", [Validators.required])
    })
    this.UploadCertificate = new FormGroup({
      "performance_certificate": new FormControl('', [Validators.required]),
      "performance_certificate_type": new FormControl('', [Validators.required]),
    })
  }
  get serial_log() {
    return this.SerialDetails.controls;
  }
  get add_log() {
    return this.AddressDetails.controls;
  }
  // Excel download
  exportAsXLSX(): void {
    this.getdetails();
    
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  //select country change event
  //get state dropdown 
  onChangeCountry(country_id) {
    this.AddressDetails.controls['state_id'].setErrors({ 'incorrect': true });
    this.AddressDetails.controls['city_id'].setErrors({ 'incorrect': true });
    this.AddressDetails.controls['state_id'].setValue(null);
    this.AddressDetails.controls['city_id'].setValue(null);
    var country = +country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;

      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.AddressDetails.controls['state_id'].setValue("null");                                               //store the response
        this.AddressDetails.controls['city_id'].setValue("null");
        this.AddressDetails.value.invalid = true;
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onChangecity(cty_id) {
    if (cty_id != '') {
      this.AddressDetails.controls['location_id'].setValue("");
      var city = +cty_id; // state: number
      var id = "city_id";
      var url = 'get_location_details/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, city).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.locations = result.response.data; //storing the api response in the array
        }
        else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue(null); //set the base64 in create product image
          this.locations = null;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.locations = [];
    }
  }
  
  //select state change event
  //get city dropdown 
  onChangeState(state_id) {
    this.AddressDetails.controls['city_id'].setErrors({ 'incorrect': true });
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                   

      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.AddressDetails.controls['city_id'].setValue("null");
        this.AddressDetails.value.invalid = true;
      }
    }, (err) => {
    });

  }

  ngOnInit() {
    this.dropdownProductSettings = {
      singleSelection: false,
      idField: 'product_id',
      textField: 'product_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: this.ShowFilter
    };
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    this.get_all_customers();    //load get API
    this.getcountry();
    this.getproduct();
    // this.getdetails();
    this.getsegment();

    // this.flag=0;
  }
  get log() {
    return this.Edit_contract_form.controls;                       //error logs for create user
  }
  get emaillog() {
    return this.EmailGroup.controls;
  }

  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
  }
  getamc() {
    var url = 'get_amc_details/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.warranty_type = result.response.data;
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const ec = (r, c) => {
      return XLSX.utils.encode_cell({ r: r, c: c })
    }
    const delete_row = (ws, row_index) => {
      let range = XLSX.utils.decode_range(ws["!ref"])
      for (var R = row_index; R < range.e.r; ++R) {
        for (var C = range.s.c; C <= range.e.c; ++C) {
          ws[ec(R, C)] = ws[ec(R + 1, C)]
        }
      }
      range.e.r--
      ws['!ref'] = XLSX.utils.encode_range(range.s, range.e)
    }
    delete_row(worksheet, 0)
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  getdetails() {
    var url = 'get_all_customer_contract/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.customerdetail = result.response.data;
        this.exportAsExcelFile(this.customerdetail, 'customer Contract');
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  getwarrantyproduct(id, flag) {
    // console.log(this.SerialDetails.value.mfg_date, "manufacture")
    // if (this.SerialDetails.value.mfg_date) {
    var year = this.SerialDetails.value.mfg_date.year;
    var month = this.SerialDetails.value.mfg_date.month;
    var day = this.SerialDetails.value.mfg_date.day;
    var mfg_date = day + "-" + month + "-" + year;
    // }
    // else {
    //   var mfg_date = '';
    // }
    // console.log(mfg_date)
    var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
    var url = 'get_warranty_type/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.warranty_id = result.response.data;
        // console.log(this.warranty_id)
        this.chRef.detectChanges();
        this.warranty_id.forEach(e => {
          if (flag == 1) {
            this.warranty_type = e.warranty_type;
            this.SerialDetails.controls["warranty_type_id"].setValue(this.warranty_type[0].warranty_id);
            this.SerialDetails.controls["contract_duration"].setValue(this.warranty_type[0].warranty_duration);
          } else {


            this.SerialDetails.controls["expiry_day"].setValue(e.expiry_day);
            this.SerialDetails.controls["voltage"].setValue(e.voltage);
            this.SerialDetails.controls["sys_ah"].setValue(e.sys_ah);
            this.warranty_type = e.warranty_type;
            this.SerialDetails.controls["warranty_type_id"].setValue(this.warranty_type[0].warranty_id);
            this.SerialDetails.controls["contract_duration"].setValue(this.warranty_type[0].warranty_duration);
          }

        })
        // console.log(this.warranty_type, "warranty_type")
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  getmodel(id) {
    this.model_id = [];
    this.subid = id;
    var data = { "product_id": this.products_id, "product_sub_id": this.subid };// storing the form group value
    // var data = { "product_id": this.SerialDetails.value.product_id, "product_sub_id": this.SerialDetails.value.sub_product_id };// storing the form group value
    var url = 'get_models/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.model_id = result.response.data;
        // console.log(this.model_id)
        // this.SerialDetails.controls['model_no'].setValue("null")
        this.chRef.detectChanges();
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }

  getproduct() {
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.product = result.response.data;                    //storing the api response in the array

      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  onChangeProduct(product_id) {
    var product = +product_id; // product: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproducts_data = result.response.data;                              //reloading the component
        this.product_label = 'subproduct';
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.subproducts_data = "";
        this.product_label = '';
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // onChangeProduct(product_id) {
  //   console.log(product_id)
  //   var product = product_id; // product: number                                    
  //   var url = 'multi_subproduct_details/?';             // api url for getting the details with using post params
  //   var id = "product_id";
  //   console.log(product)
  //   this.ajax.postdata(url, product_id).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       this.subproducts_data = result.response.data;                              //reloading the component
  //       this.product_label = 'subproduct';
  //       this.Edit_contract_form.controls['product_sub_id'].setValue('');

  //     }
  //     else if (result.response.response_code == "400") {
  //       this.subproducts_data = [];
  //       this.product_label = '';
  //     }
  //     else if (result.response.response_code == "500") {
  //       this.subproducts_data = [];
  //       this.product_label = '';
  //     }
  //   }, (err) => {
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }
  onchangeMdate(id) {
    // console.log(this.SerialDetails.value);
  }
  onChangeMFGSite(event) {
    console.log(event);

    // console.log(this.SerialDetails.value);
    // this.onchangetsiteMFG(this.SerialDetails.value.model_no);
  }
  //  onchangetsiteMFG(id) {
  //   console.log(this.SerialDetails.value.mfg_date,"this.SerialDetails.value.mfg_date")
  //   // if (this.SerialDetails.value.mfg_date) {
  //   //   var year = this.SerialDetails.value.mfg_date.year;
  //   //   var month = this.SerialDetails.value.mfg_date.month;
  //   //   var day = this.SerialDetails.value.mfg_date.day;
  //   //   var mfg_date = day + "-" + month + "-" + year;
  //   // }
  //   // else {
  //     var  mfg_date = this.SerialDetails.value.mfg_date;
  //   // }
  //   console.log(mfg_date)
  //   var data = { "product_id": this.products_id, "product_sub_id": this.subid, "model_id": id, "manufacture_date": mfg_date };// storing the form group value
  //   var url = 'get_warranty_type/'                                         //api url of remove license
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.warranty_id = result.response.data;
  //       this.warranty_id.forEach(e => {
  //         console.log(e.expiry_day);
  //         if (e.expiry_day) {
  //           var expdates = e.expiry_day.split("-", 3);
  //           var Eyear: number = +expdates[0];
  //           var Emonth: number = +expdates[1];
  //           var Eday: number = +expdates[2];
  //         }


  //         const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);

  //         this.SerialDetails.controls["expiry_day"].setValue(EDate);


  //       })
  //       this.chRef.detectChanges();
  //     }

  //     else if (result.response.response_code == "500") {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //   }, (err) => {
  //     console.log(err);                                             //prints if it encounters an error
  //   });
  // }
  extractSerialnumber(serial) {
    var serialno: any;
    // console.log(this.SerialDetails.value.serial_no);
    // if (serial) {
    serialno = this.SerialDetails.value.serial_no;
    //   var serialnum_length = serialno.toString().length;
    //   console.log(serialnum_length);
    // }
    // else {
    //   serialno = "";
    // }
    if (serialno) {
      var serial_number = { 'serial_no': serialno };
      var url = 'serial_extraction/'; // api url for getting the details                                 // api url for getting the details
      // this.ajax.postdata(url, serial_number).subscribe((result) => {
      //   if (result.response.response_code == "200" || result.response.response_code == "400") {
      //     this.serial_error = '';
      //     this.flag = result.response.flag;
      //     if (this.flag == 0) {
      //       var serial_data = result.response.manufacture_date[0]; //storing the api response in the array
      //       this.product = [];
      //       this.subproducts_data = [];
      //       this.model_id = [];
      //       this.warranty_type = [];
      //       this.product.push(serial_data.product);
      //       this.model_id.push(serial_data.model);
      //       this.warranty_type.push(serial_data.warranty_type);
      //       this.subproducts_data.push(serial_data.product_sub);
      //       this.products_id = serial_data.product.product_id
      //       this.subid = serial_data.product_sub.product_sub_id;
      //       this.mfg_date = serial_data.manufacture_date;
      //       console.log(serial_data);
      //       this.SerialDetails.patchValue({
      //         'product_id': serial_data.product.product_id,
      //         'product_sub_id': serial_data.product_sub.product_sub_id,
      //         'model_no': serial_data.model.model_id,
      //         "warranty_type_id": serial_data.warranty_type.warranty_id,
      //         "sys_ah": serial_data.sys_ah,
      //         "voltage": serial_data.voltage,
      //         "mfg_date": serial_data.manufacture_date,
      //         "expiry_day": serial_data.expiry_day,
      //         "serial_no_details": JSON.stringify(this.batteryBankArray)
      //       });

      //       this.getsegment();
      //       // this.Addbatterybank.controls['warranty_type_id'].setValue(serial_data.warranty_type.warranty_id)

      //       this.SerialDetails.controls['contract_duration'].setValue(serial_data.warranty_type.warranty_duration)

      //     }
      //     else if (this.flag == 1) {
      //       serial_data = result.response.manufacture_date[0]; //storing the api response in the array
      //       this.product = [];
      //       this.subproducts_data = [];
      //       this.model_id = [];
      //       this.warranty_type = [];
      //       // this.segment = [];
      //       // this.application = [];
      //       this.product.push(serial_data.product);
      //       this.subproducts_data.push(serial_data.product_sub);
      //       this.products_id = serial_data.product.product_id
      //       this.subid = serial_data.product_sub.product_sub_id;
      //       serial_data.model.forEach(element => {
      //         this.model_id.push(element);
      //       });

      //       console.log(this.subid);
      //       this.SerialDetails.patchValue({
      //         'product_id': this.products_id,
      //         'product_sub_id': this.subid,
      //         "mfg_date": serial_data.manufacture_date,
      //         "expiry_day": serial_data.expiry_day,
      //         "sys_ah": '',
      //         "segment_id": this.SerialDetails.value.segment_id,
      //         "application_id": this.SerialDetails.value.application_id,
      //         "organization_name": '',
      //         "invoice_number": '',
      //         "invoice_date": '',
      //         "warranty_type_id": '',
      //         "contract_duration": '',
      //       });
      //       this.getsegment();
      //     }
      //     else if (this.flag == 2) {
      //       // this.getproduct();
      //       this.SerialDetails.patchValue({
      //         "mfg_date": result.response.manufacture_date[0].manufacture_date
      //       });
      //       serial_data = result.response.manufacture_date[0]; //storing the api response in the array
      //       this.product = [];
      //       this.subproducts_data = [];
      //       this.model_id = [];
      //       serial_data.product.forEach(element => {
      //         this.product.push(element);
      //       });
      //       console.log(this.product)
      //     }
      //     else if (this.flag == 3) {
      //       // this.product = [];
      //       // this.subproduct = [];
      //       // this.model_id = [];
      //       if (result.response.manufacture_date) {
      //         var mrfdates = result.response.manufacture_date.split("-", 3);
      //         var Myear: number = +mrfdates[0];
      //         var Mmonth: number = +mrfdates[1];
      //         var Mday: number = +mrfdates[2];
      //       }

      //       const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
      //       console.log(MDate)
      //       this.SerialDetails.controls["mfg_date"].setValue(MDate);
      //       // this.SerialDetails.patchValue({
      //       //   "mfg_date": result.response.manufacture_date
      //       // });
      //       // this.Addbatterybank.value.mfg_date = result.response.manufacture_date; //storing the api response in the array
      //       console.log(this.SerialDetails.value)
      //       this.getsegment();
      //       this.getproduct();
      //     }
      //     // }

      //     this.chRef.detectChanges();
      //   }
      //   else if (result.response.response_code == "500") {
      //     this.toastr.error(result.response.data, 'Error')

      //     this.serial_error = result.response.message;
      //   }
      //   else {
      //     this.toastr.error("Something went wrong", 'Error')
      //     this.serial_error = "";
      //   }
      // }, (err) => {
      //   console.log(err); //prints if it encounters an error
      // });
      this.ajax.postdata(url, serial_number).subscribe((result) => {
        if (result.response.response_code == "200" || result.response.response_code == "400") {
          this.serial_error = '';
          this.flag = result.response.flag;
          // console.log(this.flag, "flag")
          if (this.flag == 0) {
            this.serial_data = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.warranty_type = [];
            this.product.push(this.serial_data.product);
            this.model_id.push(this.serial_data.model);
            this.warranty_type.push(this.serial_data.warranty_type);
            this.subproducts_data.push(this.serial_data.product_sub);
            this.products_id = this.serial_data.product.product_id
            this.subid = this.serial_data.product_sub.product_sub_id;
            // this.mfg_date = this.serialNumbersdata.manufacture_date;
            // console.log(this.serial_data);
            this.SerialDetails.patchValue({
              'product_id': this.serial_data.product.product_id,
              'product_sub_id': this.serial_data.product_sub.product_sub_id,
              'model_no': this.serial_data.model.model_id,
              "warranty_type_id": this.serial_data.warranty_type.warranty_id,
              "sys_ah": this.serial_data.sys_ah,
              "voltage": this.serial_data.voltage.toUpperCase(),

            });
            if (this.serial_data.expiry_day) {
              var expdates = this.serial_data.expiry_day.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }
            if (this.serial_data.manufacture_date) {
              var mdates = this.serial_data.manufacture_date.split("-", 3);
              var Myear: number = +mdates[0];
              var Mmonth: number = +mdates[1];
              var Mday: number = +mdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

            this.SerialDetails.controls["expiry_day"].setValue(EDate);
            this.SerialDetails.controls["mfg_date"].setValue(MDate);
            this.onChangeProduct(this.serial_data.product.product_id)
            this.getsegment();
            // this.Addbatterybank.controls['warranty_type_id'].setValue(this.serialNumbersdata.warranty_type.warranty_id)

            this.SerialDetails.controls['contract_duration'].setValue(this.serial_data.warranty_type.warranty_duration)

          }
          else if (this.flag == 1) {
            this.serial_data = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.warranty_type = [];
            // this.segment = [];
            // this.application = [];
            this.product.push(this.serial_data.product);
            this.subproduct.push(this.serial_data.product_sub);
            this.products_id = this.serial_data.product.product_id

            this.subid = this.serial_data.product_sub.product_sub_id;
            this.serial_data.model.forEach(element => {
              this.model_id.push(element);
            });
            if (result.response.manufacture_date[0]) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }

            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            this.SerialDetails.controls["mfg_date"].setValue(EDate);
            // console.log(this.subid);
            // this.Addbatterybank.patchValue({
            //   'product_id': this.products_id,
            //   'product_sub_id': this.subid,
            //   // "mfg_date": this.serialNumbersdata.manufacture_date,
            //   // "expiry_day": this.serialNumbersdata.expiry_day,
            //   "sys_ah": '',
            //   "segment_id": this.Addbatterybank.value.segment_id,
            //   "application_id": this.Addbatterybank.value.application_id,
            //   "organization_name": '',
            //   "invoice_number": '',
            //   "invoice_date": '',
            //   "warranty_type_id": '',
            //   "contract_duration": '',



            // });
            // this.Addbatterybank.patchValue({
            //   'product_id': this.products_id,
            //   'product_sub_id': this.subid,
            //   // 'model_no': this.serialNumbersdata.model.model_id,
            //   // "warranty_type_id": this.serialNumbersdata.warranty_type.warranty_id,
            //   // "mfg_date": this.serialNumbersdata.manufacture_date,
            //   // "expiry_day": this.serialNumbersdata.expiry_day,

            // });
            // console.log(this.SerialDetails.value.site_id)
            this.SerialDetails.patchValue({
              "contract_id": this.contract_id,
              "product_id": this.products_id,
              "product_sub_id": this.subid,
              "model_no": '',
              "serial_no": this.SerialDetails.value.serial_no,
              "invoice_date": '',
              "contract_duration": '',
              "battery_bank_id": this.SerialDetails.value.battery_bank_id,
              "sys_ah": '',
              "segment_id": this.SerialDetails.value.segment_id,
              "application_id": this.SerialDetails.value.application_id,
              "invoice_number": '',
              "expiry_day": '',
              "warranty_type_id": '',
              "mfg_date": EDate,
              "voltage": '',
              "organization_name": '',
              "flag": 2,
              "site_id": this.SerialDetails.value.site_id,
            })



            this.getsegment();
            this.product_name = this.SerialDetails.value.product_id
          }
          else if (this.flag == 2) {
            // this.getproduct();
            // this.Addbatterybank.patchValue({
            //   "mfg_date": result.response.manufacture_date[0].manufacture_date
            // });
            if (result.response.manufacture_date[0].manufacture_date) {
              var expdates = result.response.manufacture_date[0].manufacture_date.split("-", 3);
              var Eyear: number = +expdates[0];
              var Emonth: number = +expdates[1];
              var Eday: number = +expdates[2];
            }


            const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            this.serial_data = result.response.manufacture_date[0]; //storing the api response in the array
            this.product = [];
            this.subproduct = [];
            this.model_id = [];
            this.serial_data.product.forEach(element => {
              this.product.push(element);
            });
            // console.log(this.SerialDetails.value)
            // console.log(this.product)
            this.SerialDetails.patchValue({
              "contract_id": this.contract_id,
              "product_id": '',
              "product_sub_id": '',
              "model_no": '',
              "serial_no": this.SerialDetails.value.serial_no,
              "invoice_date": '',
              "contract_duration": '',
              "battery_bank_id": this.SerialDetails.value.battery_bank_id,
              "sys_ah": '',
              "segment_id": '',
              "application_id": '',
              "invoice_number": '',
              "expiry_day": '',
              "warranty_type_id": '',
              "mfg_date": EDate,
              "voltage": '',
              "organization_name": '',
              "ticket_status": this.SerialDetails.value.ticket_status,
              "flag": 2,
              "site": this.SerialDetails.value.site_id

            })
            // console.log(this.SerialDetails.value)

          }

          else if (this.flag == 3) {
            // this.product = [];
            // this.subproduct = [];
            // this.model_id = [];
            // if (result.response.manufacture_date) {
            //   var expdates = result.response.manufacture_date.split("-", 3);
            //   var Eyear: number = +expdates[0];
            //   var Emonth: number = +expdates[1];
            //   var Eday: number = +expdates[2];
            // }
            var mrfdates = result.response.manufacture_date;
            var Myear: number = +mrfdates[2];
            var Mmonth: number = +mrfdates[1];
            var Mday: number = +mrfdates[0];

            const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
            // console.log(MDate)

            // const EDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);
            // this.Addbatterybank.controls["mfg_date"].setValue(EDate);
            // this.Addbatterybank.patchValue({
            //   'product_id': '',
            //   'product_sub_id': '',
            //   'model_no': '',
            //   "warranty_type_id": '',
            //   "sys_ah": '',
            //   "voltage": '',
            //   "invoice_date": '',
            //   "invoice_number": ''

            // });
            this.serial_data = new FormGroup({
              "product_id": new FormControl('', [Validators.required]),
              "product_sub_id": new FormControl('', [Validators.required]),
              "model_no": new FormControl('', [Validators.required]),
              "serial_no": new FormControl(this.serial_data.value.serial_no, [Validators.required]),
              "invoice_date": new FormControl(''),
              "contract_duration": new FormControl(''),
              "battery_bank_id": new FormControl(this.serial_data.value.battery_bank_id),
              "sys_ah": new FormControl('', [Validators.required]),
              "segment_id": new FormControl('', [Validators.required]),
              "application_id": new FormControl('', [Validators.required]),
              "invoice_number": new FormControl(''),
              "expiry_day": new FormControl(''),
              "warranty_type_id": new FormControl('', [Validators.required]),
              "mfg_date": new FormControl(MDate),
              "voltage": new FormControl('', [Validators.required]),
              "organization_name": new FormControl('', [this.noWhitespace]),

            })

            this.getsegment();
            this.getproduct();
          }


          this.chRef.detectChanges();
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')

          this.serial_error = result.response.message;
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
          this.serial_error = "";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }



  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  //--------------------------------------------------------------------------------------------------//
  onvalidate() {
    if (this.customer_detail.email_id == this.Edit_customer.value.email_id) {
    }
    else {
      this.params = {
        "email_id": this.Edit_customer.value.email_id,
      }
      var url = 'cus_validation/?';                                  // api url for getting the cities
      this.ajax.postdata(url, this.params).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.validation_data = '';
          this.Edit_customer.value.invalid = false;
          this.Edit_customer.controls['email_id'].setValue(this.Edit_customer.value.email_id);

        }
        else if (result.response.response_code == "500") {
          this.validation_data = result.response.message;
          this.Edit_customer.value.invalid = true;
          this.Edit_customer.controls['email_id'].setErrors({ 'incorrect': true });


        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }


  }
  oncontactvalidate() {
    this.Edit_customer.value.invalid = true;
    if (this.customer_detail.contact_number == this.Edit_customer.value.contact_number) {

    }
    else {

      this.params = {
        "contact_number": this.Edit_customer.value.contact_number
      }
      var url = 'cus_validation/?';                                  // api url for getting the cities
      this.ajax.postdata(url, this.params).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.validation_data_contact = '';
          this.Edit_customer.value.invalid = false;
          this.Edit_customer.controls['contact_number'].setValue(this.Edit_customer.value.contact_number)

        }
        else if (result.response.response_code == "500") {
          this.validation_data_contact = result.response.message;
          this.Edit_customer.value.invalid = true;
          this.Edit_customer.controls['contact_number'].setErrors({ 'incorrect': true });


        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });
    }


  }
  editalternatecontact() {
    if (this.Edit_customer.value.contact_number == this.Edit_customer.value.alternate_number) {
      this.alternate = "Contact number and alternate contact number are same.";
      this.Edit_customer.controls['contact_number'].setErrors({ 'incorrect': true });
      this.Edit_customer.value.invalid = true;

    }
    else {
      this.alternate = '';
      this.Edit_customer.controls['contact_number'].setValue(this.Edit_customer.value.contact_number)
      this.Edit_customer.value.invalid = false;
    }
  }
  editcontact() {
    if (this.Edit_customer.value.alternate_number == this.Edit_customer.value.contact_number) {
      this.contact = "Contact and alternate are same.";
      // this.Edit_customer.value.invalid = true;
      // this.Edit_customer.controls['contact_number'].setErrors({'incorrect': true });

    }
    else {
      this.contact = '';
      // this.Edit_customer.controls['contact_number'].setValue(this.Edit_customer.value.contact_nummber);
      // this.Edit_customer.value.invalid = false;
    }
  }
  //To Get the customer details from API
  get_all_customers() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    var sort = ""
    var column = ""
    var url = 'get_all_customers/';  
    var search_value = "";
    var data = {  "length":length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }                                 // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customer_details = result.response.data;                    //storing the api response in the array 
        this.showTables = true;
        this.chRef.detectChanges();
        this.settingsObj = {

          "deferLoading": result.response.recordsTotal,

          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',

          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            var leng = data.length
            var length = data.length

            var start = data.start
            var search_value = data.search.value
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']
            var data1 = {  "length":length, "start": start, "draw": draw, "search_value": search_value, "sort": sort, "column": column }
            var url_forming = "get_all_customers/"
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.customer_details = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();

            });

          },
          "columnDefs": [           
					  { "name": "Ticket_id", "targets": 1, "searchable": true },
					  { "name": "Customer_name", "targets": 2, "searchable": true },
					  { "name": "ticket_type", "targets": 3, "searchable": true },
					  { "name": "Product", "targets": 4, "searchable": true },
            { "name": "Location", "targets": 5, "searchable": true },
					  { "name": "Sub_product", "targets": 6, "searchable": true },
					 
		  
					],
        }

        $('#datatables').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#datatables').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
        this.overlayRef.detach();
      }
      else {
        this.errors = "Something Went Wrong";
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
      this.overlayRef.detach();
    });
  }

  //To Get the contract details from API
  getcontractdata(contract_table, row) {
    this.contract_message = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.customer_code = row; // customer_code                                    
    var url = 'get_contract_details/?';             // api url for getting the details with using post params
    var id = "customer_code";
    this.ajax.getdataparam(url, id, this.customer_code).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.contract_details = result.response.data;                    //storing the api response in the array
        this.end_date = this.contract_details.expiry_day
        this.showTable = true;
        this.chRef.detectChanges();
        this.modalService.open(contract_table, {
          //specifies the size of the modal box
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,
          windowClass: "center-modalextralg"                                  // modal will not close by keyboard click
        });
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.contract_details = result.response.message;
        // this.errors = result.response.message;
        this.overlayRef.detach();
      }
      else {
        this.contract_details = "Something Went Wrong";
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  // Function to add and edit the modal box 
  openLarge(upload) {
    this.ByDefault = false;
    this.validation_data = '';
    this.bulkboolean = true;
    this.bulk = '';
    this.loadingBulk = false;
    this.bulks = '';
    this.error = "";
    this.file_name = undefined;   //refresh the file 
    this.Filename = '';             //refresh the file name
    this.httplink = environment.image_static_ip;
    this.customerexcel='media/customer_bulk_usermanual.xlsx';
    this.modalService.open(upload, {
      size: 'lg',                                                       //specifies the size of the modal box
      // windowClass: "center-modalsm",      //modal popup custom size  
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click   
    });
  }
  opencontract(upload, row) {
    this.id = row;
    this.modalService.open(upload, {
      size: 'lg',                                                        //specifies the size of the modal box
      windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  openviewLarge(upload, row) {

    this.id = row;
    this.modalService.open(upload, {
      size: 'lg',                                                       //specifies the size of the modal box
      windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  //--------------------------------------------------------------------------------------------------//
  //functions for bulk upload
  fileUpload(event) {
    this.type = '';
    this.Filename = '';
    this.file_name = '';
    this.bulk = '';
    this.bulks = '';
    this.error = "";
    //read the file
    let fileList: FileList = event.target.files;  //store that binary file
    this.file_name = fileList;
    this.Filename = event.target.files[0].name;
    this.type = event.target.files[0].type;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    this.ByDefault = true;

  };

  //  bulk upload for customer details
  filesubmit(bulkupload) {
    this.bulk = '';
    this.bulks = '[]';
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.bulk = '';
      this.bulks = '';
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.bulk = '';
        this.bulks = '';
        this.error = "Please select correct extension";
      }
      else if (validExts[0] = this.type) {                 //if file is not empty

        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        var gh = file;
        var method = "post";                                    //post method 
        var url = 'cus_ticket_bulkupload/'                         //post url
        var i: number;                                             //i is the data
        this.loadingBulk = true;
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {

          //refresh the file name
          if (data['response_code'] == "200") {    //if sucess
            this.error = '';
            this.loadingBulk = false;
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {                        //sucess response            
              for (i = 0; i < data['success'].length; i++) {
                // this.bulk = '';
                // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                var a = [];
                data['success'].forEach(value => {
                  a.push(value.success_message
                  );
                });
                this.bulks = a;
              // for (i = 0; i < data['success'].length; i++) {
              //   console.log(data['success'][i].success_message)
              //   this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.loadingBulk = false;
                this.showTables = false;
                this.chRef.detectChanges();
                this.get_all_customers();                               //to close the modal box
              // }
            }
          }
            if (data['response'].fail) {
            if (data['response'].fail.length > 0) {                                    //failure response 
              //if employee code or employee id is error to put this
              // console.log(data['response'].fail)
              // for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
              //   var a = [];
              //   data['response'].fail.forEach(value => {
              //     a.push(value.error_message);
              //   });

              //   this.bulk = a;
              var a = [];
              data['response'].fail.forEach(value => {
                a.push(value.error_message
                );
              });
              this.bulk = a;
              }
            }
          }
            
          else if (data['response'].response_code == "500") {
            this.bulk = '';
            this.loadingBulk = false;
            this.error = data['response'].message
          }

          else {
            this.bulk = '';
            this.loadingBulk = false;                                                       //if not sucess
            this.error = "Something Went Wrong";        //toastr message for error
          }

        },

          (err) => {                                                                     //if error
            this.loadingBulk = false;
            this.error = "Something Went Wrong";
            console.log(err);                                                 //prints if it encounters an error
          }
        );

      }
    }
  };
  get edit() {
    return this.Edit_customer.controls;                       //error logs for create user
  }
  //Function to store the line item selected to edit
  showcustomer_detail(data, edit_customer) {
    this.validation_data_contact = '';
    this.validation_data = '';
    this.alternate = '';
    this.contact = '';
    var customer_code = data.customer_code; // customer_code                                    
    var url = 'get_customer_details/?';             // api url for getting the details with using post params
    var id = "customer_code";
    this.ajax.getdataparam(url, id, customer_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.customer_detail = result.response.data;                  //storing the api response in the array
        this.chRef.detectChanges();
        this.modalService.open(edit_customer, {
          size: 'lg',                                                       //specifies the size of the modal box
          windowClass: "center-modalsm",      //modal popup custom size 
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click    
        });
        this.Edit_customer.setValue({
          "customer_id": this.customer_detail.customer_id,
          "customer_code": this.customer_detail.customer_code,
          "customer_name": this.customer_detail.customer_name,
          "email_id": this.customer_detail.email_id,
          "contact_number": this.customer_detail.contact_number,
          "alternate_number": this.customer_detail.alternate_number,
        });


      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to call the customer edit api 
  edit_customer_details() {
    this.submitted = true;
    console.log(this.Edit_customer, "this.Edit_customer")
    if (this.Edit_customer.invalid) {
      this.toastr.error("Fields are missing", 'Error');
    }
    else {
      var data = this.Edit_customer.value
      data["customer_code"] = this.customer_detail.customer_code
      data["customer_id"] = this.customer_detail.customer_id
      var url = 'edit_customer/'                                           //api url of edit api                                    
      this.ajax.postdata(url, data)
        .subscribe((result) => {
          if (result.response.response_code == "200")                           //if sucess
          {
            this.modalService.dismissAll();                                 //to close the modal box
            this.showTables = false;
            this.chRef.detectChanges();
            this.get_all_customers();                                                //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "500" || result.response_cod == "400") {
            this.errors = result.response.message;

          }
          else {
            this.errors = "Something Went Wrong";

          }
        }, (err) => {                                                           //if error
          console.log(err);                                                 //prints if it encounters an error
        });
    }
  }
  add_con_detail(evt: any) {
    if (this.Edit_contract_form.value.product_sub_id == '' || this.Edit_contract_form.value.product_id == '' || this.Edit_contract_form.value.serial_no == '' || this.Edit_contract_form.value.model_no == '' || this.Edit_contract_form.value.contract_type_id == '') {
    }
    else {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
  }
  editserialValidation() {
    this.params = {
      "customer_code": this.Edit_contract_form.value.customer_code,
      "product_id": this.Edit_contract_form.value.product_id,
      "product_sub_id": this.Edit_contract_form.value.product_sub_id,
      "model_no": this.Edit_contract_form.value.model_no,
      "serial_no": this.Edit_contract_form.value.serial_no
    }
    this.userValidation(this.params)
  }
  userValidation(param) {
    this.params = param;
    var url = 'validate_serial_no/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = '';
      }
      if (result.response.response_code == "500") {
        this.validation_data = result.response.message;
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  //--------------------------------------------------------------------------------------------------------//
  // function to show the contract detail based on id for view
  show_serial_list(contract_id, serialList) {
    this.scontract_id = contract_id; // customer_code                                    
    var url = 'get_contract_info/?';             // api url for getting the details with using post params
    var id = "contract_id";
    this.ajax.getdataparam(url, id, this.scontract_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // console.log(result.response)
        this.contract_id = contract_id;
        // this.AddressDetails.patchValue({
        //   'contract_id': contract_id,
        // });
        this.contract_data = result.response.contract_details;
        // console.log(this.contract_data)
        this.battery_bank_details = result.response.data;

        this.modalService.open(serialList, {
          // size: 'lg',                                                        //specifies the size of the modal box
          // windowClass: "center-modalsm",      //modal popup custom size 
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
          windowClass: "center-modalextralg"
        });
        this.getamc();
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = "Something Went Wrong";
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }


  // function to show the contract detail based on id for view
  showcontract_detail(item, view_contract) {
    // console.log(item)
    // console.log(this.scontract_id)
    this.loadingUpdate = false;
    this.subproducts_data = [];
    this.activeId = "tab-selectbyid1";
    var contract_id = contract_id; // customer_code                                    
    var url = 'get_contract_info/?';             // api url for getting the details with using post params
    var id = "contract_id";
    this.ajax.getdataparam(url, id, this.scontract_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.battery_bank_details = result.response.data;
        this.contract_details_view = result.response.contract_details;
        this.modalService.open(view_contract, {
          size: 'lg',                                                        //specifies the size of the modal box
          // windowClass: "center-modalsm",      //modal popup custom size 
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
        // this.onChangeCountry(this.contract_detail.country.country_id)
        // this.onChangeState(this.contract_detail.state.state_id)
        // this.onChangeProduct(this.contract_detail.product.product_id);
        this.getamc();
        this.chRef.detectChanges();
        this.overlayRef.detach();
        // this.Edit_contract_form.setValue({
        //   "contract_type_id": this.contract_detail.contract.contract_type_id,
        //   "contract_id": this.contract_detail.contract_id,
        //   "plot_number": this.contract_detail.plot_number,
        //   "contract_duration": this.contract_detail.contract_duration,
        //   "model_no": this.contract_detail.model_no,
        //   "serial_no": this.contract_detail.serial_no,
        //   "product_id": this.contract_detail.product.product_id,
        //   "product_sub_id": this.contract_detail.sub_product.product_sub_id,
        //   "location_id": this.contract_detail.location.location_id,
        //   "street": this.contract_detail.street,
        //   "landmark": this.contract_detail.landmark,
        //   "post_code": this.contract_detail.post_code,
        //   "country_id": this.contract_detail.country.country_id,
        //   "state_id": this.contract_detail.state.state_id,
        //   "city_id": this.contract_detail.city.city_id,

        // });
        // if (this.contract_detail.sub_product.product_sub_id != '') {
        //   this.product_label = 'subproduct';
        // }
        // else {
        //   this.product_label = '';
        // }
        // this.select = this.contract_detail.product;
        //   console.log(this.select, "this.select")
        // //   if(this.select){
        //     this.select.forEach((product) => {
        //       this.products.push(product.product_id);
        //     })
        //   }
        //  else{
        // this.products.push(this.contract_detail.product.product_id);

        //  }
        // var datas = { "product_id": this.products }
        // this.Edit_contract_form.setValue({"product_id": this.contract_detail.product.product_id})
        // console.log(this.contract_detail.product.product_id, "this.contract_detail.product.product_id")
        // this.subproduct.forEach((subproduct) => {
        //   this.subproduct.push(subproduct.product_sub_id);
        // }); 
        // console.log(this.subproduct,"this.subproduct")


      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = "Something Went Wrong";
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

  // addcustomer_name(val) {

  //   if (this.SerialDetails.value.invoice_date) {
  //     var year = this.SerialDetails.value.invoice_date.year;
  //     var month = this.SerialDetails.value.invoice_date.month;
  //     var day = this.SerialDetails.value.invoice_date.day;
  //     var dateFormated = year + "-" + month + "-" + day;
  //     this.SerialDetails.controls['invoice_date'].setValue(dateFormated);
  //     var ids = this.SerialDetails.value.invoice_date.split("-", 3);
  //     var year = ids[0];
  //     var month = ids[1];
  //     var day = ids[2];
  //     var date = year + "-" + month + "-" + day;
  //     this.SerialDetails.controls["invoice_date"].setValue(date);
  //   }
  //   else {
  //     this.SerialDetails.controls['invoice_date'].setValue('');
  //   }
  //   if (this.SerialDetails.value.battery_bank_id == null) {
  //     this.SerialDetails.controls['battery_bank_id'].setValue('');
  //   }
  //   else {
  //     this.SerialDetails.value.battery_bank_id = this.SerialDetails.value.battery_bank_id;
  //   }
  //   if (this.AddressDetails.value.site_id == null) {
  //     this.AddressDetails.controls['site_id'].setValue('');
  //   }
  //   else {
  //     this.AddressDetails.value.site_id = this.AddressDetails.value.site_id;

  //   }
  //   this.employee_code = localStorage.getItem('employee_code');
  //   this.AddressDetails.controls['employee_code'].setValue(this.employee_code);
  //   this.SerialDetails.controls['voltage'].setValue(this.SerialDetails.value.voltage);

  //   console.log(this.SerialDetails.value.expiry_day)
  //   var ids = this.SerialDetails.value.expiry_day.split("-", 3)
  //   var year = ids[0];
  //   var month = ids[1];
  //   var day = ids[2];
  //   var exdate = day + "-" + month + "-" + year;
  //   this.SerialDetails.controls["expiry_day"].setValue(exdate);
  //   var ids1 = this.SerialDetails.value.mfg_date.split("-", 3)
  //   var year1 = ids1[0];
  //   var month1 = ids1[1];
  //   var day1 = ids1[2];
  //   var mgdate = day1 + "-" + month1 + "-" + year1;
  //   this.SerialDetails.controls["mfg_date"].setValue(mgdate);
  //   console.log(exdate)
  //   var serial: any = [];
  //   this.serial = [];
  //   var serial_num = '';
  //   serial = {
  //     "product_id": this.SerialDetails.value.product_id,
  //     "product_sub_id": this.SerialDetails.value.product_sub_id,
  //     "model": this.SerialDetails.value.model_no,
  //     "sys_ah": this.SerialDetails.value.sys_ah,
  //     "serial_no": this.SerialDetails.value.serial_no,
  //     "invoice_number": this.SerialDetails.value.invoice_number,
  //     "invoice_date": this.SerialDetails.value.invoice_date,
  //     "mfg_date": this.SerialDetails.value.mfg_date,
  //     "warranty_type_id": this.SerialDetails.value.warranty_type_id,
  //     "expiry_date": this.SerialDetails.value.expiry_day,
  //     "voltage": this.SerialDetails.value.voltage,
  //   };
  //   if (val == 1) {

  //     this.AddressDetails.patchValue({
  //       'battery_bank_id': '',
  //     });
  //     console.log(serial)
  //     this.serial.push(serial);
  //     var serial_num = JSON.stringify(this.serial);


  //   }

  //   else {
  //     this.productdata.forEach(obj => {
  //       var item = {
  //         "product_id": obj.product.product_id,
  //         "product_sub_id": obj.subproduct.product_sub_id,
  //         "model": obj.model.model_id,
  //         "voltage": obj.voltage,
  //         "sys_ah": obj.sys_ah,
  //         "serial_no": obj.serial_no,
  //         "invoice_number": obj.invoice_number,
  //         "invoice_date": obj.invoice_date,
  //         "mfg_date": obj.mfg_date,
  //         "warranty_type_id": obj.warranty.warranty_id,
  //         "expiry_date": obj.expiry_date
  //       }
  //       this.serialProduct.push(item)
  //     })
  //     console.log(this.serialProduct);

  //     this.serialProduct.push(serial);

  //     var serial_num = JSON.stringify(this.serialProduct);

  //   }

  //   if (this.SerialDetails.value.battery_bank_id != '' || this.SerialDetails.value.battery_bank_id != null || this.SerialDetails.value.battery_bank_id != undefined) {
  //     this.battery_bank_id = this.SerialDetails.value.battery_bank_id
  //   }
  //   else {
  //     this.battery_bank_id = ""
  //   }
  //   if (this.AddressDetails.value.site_id != '' || this.AddressDetails.value.site_id != null || this.AddressDetails.value.site_id != undefined) {
  //     console.log("if")
  //     this.site_id = this.AddressDetails.value.site_id
  //   }
  //   else {
  //     console.log("else")
  //     this.site_id = ""
  //   }

  //   var data = this.AddressDetails.value
  //   console.log(this.AddressDetails.value.customer_type)

  //   console.log(data)
  //   this.form = ({
  //     "employee_code": this.employee_code,
  //     "site_id": this.AddressDetails.value.site_id,
  //     "customer_code": this.AddressDetails.value.customer_code,
  //     "customer_name": this.AddressDetails.value.customer_name,
  //     "customer_type": this.AddressDetails.value.customer_type,
  //     "email_id": this.AddressDetails.value.email_id,
  //     "contact_number": this.AddressDetails.value.contact_number,
  //     "alternate_number": this.AddressDetails.value.alternate_number,
  //     "sap_reference_number": this.AddressDetails.value.sap_reference_number,
  //     "plot_number": this.AddressDetails.value.plot_number,
  //     "contact_person_name": this.AddressDetails.value.contact_person_name,
  //     "contact_person_number": this.AddressDetails.value.contact_person_number,
  //     "street": this.AddressDetails.value.street,
  //     "landmark": this.AddressDetails.value.landmark,
  //     "post_code": this.AddressDetails.value.post_code,
  //     "country_id": this.AddressDetails.value.country_id,
  //     "state_id": this.AddressDetails.value.state_id,
  //     "city_id": this.AddressDetails.value.city_id,
  //     "location_id": this.AddressDetails.value.location_id,
  //     "product_id": this.SerialDetails.value.product_id,
  //     "product_sub_id": this.SerialDetails.value.product_sub_id,
  //     "model_no": this.SerialDetails.value.model_no,
  //     "serial_no": this.SerialDetails.value.serial_no,
  //     "invoice_date": this.SerialDetails.value.invoice_date,
  //     "contract_duration": this.SerialDetails.value.contract_duration,
  //     "battery_bank_id": this.SerialDetails.value.battery_bank_id,
  //     "sys_ah": this.SerialDetails.value.sys_ah,
  //     "segment_id": this.SerialDetails.value.segment_id,
  //     "application_id": this.SerialDetails.value.application_id,
  //     // "organization_name": this.SerialDetails.value.organization_name,
  //     "invoice_number": this.SerialDetails.value.invoice_number,
  //     "expiry_day": this.SerialDetails.value.expiry_day,
  //     "warranty_type_id": this.SerialDetails.value.warranty_type_id,
  //     "mfg_date": this.SerialDetails.value.mfg_date,
  //     "voltage": this.SerialDetails.value.voltage,
  //     "serial_no_details": serial_num
  //   });
  //   console.log(this.form)
  //   if (serial_num != '') {
  //     var url = 'add_customer_contract/'                                         //api url of add
  //     this.ajax.postdata(url, this.form).subscribe((result) => {
  //       //if sucess
  //       if (result.response.response_code == "200") {

  //         this.toastr.success(result.response.message, 'Success');    //toastr message for success

  //         this.overlayRef.detach();

  //       }
  //       else if (result.response.response_code == "404") {
  //         this.customer_details = result.response.customer_details;

  //         this.toastr.error(result.response.message, "Error");
  //       }
  //       else if (result.response.response_code == "400") {

  //         this.toastr.error(result.response.message, "Error");


  //         this.overlayRef.detach();



  //       }
  //       else if (result.response.response_code == "500") {
  //         this.overlayRef.detach();
  //         this.toastr.error(result.response.message, "Error");
  //       }
  //       else {
  //         this.overlayRef.detach();
  //         this.error = "Something went wrong";
  //         this.toastr.error(this.error, "Error");
  //       }
  //       (err) => {
  //         this.overlayRef.detach();
  //         console.log(err); //prints if it encounters an error
  //       }
  //     });
  //   }




  // }
  edit_serial_details(data, modalname, siteflag, index) {
    this.index = index
    // console.log(index, "index")
    // console.log(data)
    // console.log(data.invoice_date, "data.invoice_date")
    if (data.invoice_date != null) {

      var invoice = data.invoice_date.split("-", 3);;
      var Iyear: number = +invoice[0];
      var Imonth: number = +invoice[1];
      var Iday: number = +invoice[2];
    }
    const IDDate: NgbDate = new NgbDate(Iday, Imonth, Iyear);
    // console.log(data.mfg_date, "data.mfg_date")
    if (data.mfg_date) {

      var mdates = data.mfg_date.split("-", 3);
      var Myear: number = +mdates[0];
      var Mmonth: number = +mdates[1];
      var Mday: number = +mdates[2];
    }

    const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);

    // var mrfdates = data.mfg_date;
    // var Myear: number = +mrfdates[2];
    // var Mmonth: number = +mrfdates[1];
    // var Mday: number = +mrfdates[0];

    // const MDate: NgbDate = new NgbDate(Mday, Mmonth, Myear);
    // console.log(MDate)


    // console.log(data.expiry_date, "data.expiry_date")
    if (data.expiry_date != null) {
      var expiry = data.expiry_date.split("-", 3);;
      var Eyear: number = +expiry[0];
      var Emonth: number = +expiry[1];
      var Eday: number = +expiry[2];
      // var expirydate = EXPDate
    }
    const EXPYDate: NgbDate = new NgbDate(Eday, Emonth, Eyear);

    // else {
    //   var invoicedeate ='';
    // }
    // console.log(this.contract_data, " this.contract_data.")
    // console.log(data.warranty.warranty_duration, "this.contract_id")
    // console.log(data.subproduct.product_sub_id, "sdfsdf")
    this.SerialDetails.patchValue({
      "contract_id": this.contract_id,
      "product_id": data.product.product_id,
      "product_sub_id": data.subproduct.product_sub_id,
      "model_no": data.model.model_id,
      "serial_no": data.serial_no,
      "invoice_date": IDDate,
      "contract_duration": data.warranty.warranty_duration,
      "battery_bank_id": this.contract_data.battery_bank_id,
      "sys_ah": data.sys_ah,
      "segment_id": this.contract_data['segment_id'],
      "application_id": this.contract_data['application_id'],
      "organization_name": this.contract_data.organization_name,
      "invoice_number": data.invoice_number,
      "expiry_day": EXPYDate,
      "warranty_type_id": data.warranty.warranty_type_id,
      "mfg_date": MDate,
      "voltage": data.voltage.toUpperCase(),
      "site_id": this.contract_data.site_id,
      "flag": siteflag,
      "serial_no_details": "",
    });
    // console.log(this.SerialDetails.value, "SerialDetails1")
    // this.SerialDetails.controls["mfg_date"].setValue(MDate);
    //  this.SerialDetails.patchValue({
    //   "product_id": data.product.product_id,
    //   "product_sub_id": data.subproduct.product_sub_id,
    //   "model_no": data.model.model_id,
    //   "serial_no": data.serial_no,
    //   // "invoice_date": IDate,
    //   // "contract_duration": data.contract.amc_id,
    //   "battery_bank_id": this.contract_data.battery_bank_id,
    //   "sys_ah": data.sys_ah,
    //   "segment_id": this.contract_data['segment_id'],
    //   "application_id": this.contract_data['application_id'],
    //   "organization_name": this.contract_data.organization_name,
    //   "invoice_number": data.invoice_number,
    //   "expiry_day": data.expiry_date,
    //   "warranty_type_id": data.warranty.warranty_type_id,
    //   "mfg_date": MDate,
    //   "voltage": data.voltage,
    //   "site_id": this.contract_data.site_id,
    //   "contract_id": this.contract_id,
    //   "flag": siteflag,
    // });
    this.modalService.open(modalname, {
      size: 'lg',                                                        //specifies the size of the modal box
      // windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });

    // this.SerialDetails.controls["invoice_date"].setValue(IDate);
    // console.log(data.serial_no)
    // console.log(this.SerialDetails.value)
    // this.extractSerialnumber(data.serial_no)

    this.serial_data = data;
    this.subid = data.subproduct.product_sub_id
    this.products_id = data.product.product_id
    this.getmodel(data.subproduct.product_sub_id);
    this.onChangeProduct(data.product.product_id);
    this.getapplication(this.contract_data.segment_id)
    // this.getamc();
    var flag = 1
    this.getwarrantyproduct(data.model.model_id, flag)
    // console.log(this.SerialDetails.value, "SerialDetails2")
  }
  // function to show the contract detail based on id for view
  show_serial_details(contract_id, edit_serial_number, siteflag) {
    this.scontract_id = contract_id; // customer_code                                    
    var url = 'get_contract_info/?';             // api url for getting the details with using post params
    var id = "contract_id";
    this.ajax.getdataparam(url, id, this.scontract_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // console.log(result.response)
        this.contract_data = result.response.contract_details;
        // console.log(this.contract_data);
        this.onChangeCountry('101');
        this.onChangeState(this.contract_data.state.state_id);
        this.onChangecity(this.contract_data.city.city_id)
        // console.log(this.contract_data)
        this.battery_bank_details = result.response.data;
        this.modalService.open(edit_serial_number, {
          size: 'lg',                                                        //specifies the size of the modal box
          // windowClass: "center-modalsm",      //modal popup custom size 
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
        this.site_flag = siteflag
        this.AddressDetails.patchValue({
          "customer_code": this.contract_data.customer_code,
          "customer_name": this.contract_data.customer_name,
          "email_id": this.contract_data.email_id,
          "contact_number": this.contract_data.contact_number,
          "alternate_number": this.contract_data.alternate_number,
          "sap_reference_number": this.contract_data.sap_reference_number,
          "plot_number": this.contract_data.plot_number,
          "contact_person_name": this.contract_data.contact_person_name,
          "contact_person_number": this.contract_data.contact_person_number,
          "street": this.contract_data.street,
          "landmark": this.contract_data.landmark,
          "post_code": this.contract_data.post_code,
          "country_id": this.contract_data.country.country_id,
          "state_id": this.contract_data.state.state_id,
          "city_id": this.contract_data.city.city_id,
          "location_id": this.contract_data.location.location_id,
          "site_id": this.contract_data.site_id,
          "contract_id": contract_id,
          "flag": siteflag
        })
        // console.log(this.AddressDetails.value, "this.AddressDetails.")
        this.getamc();
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = "Something Went Wrong";
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
    // console.log(multi_details)
    // console.log(this.contract_data)
    // if (multi_details.invoice_date != '') {
    //   if(multi_details.invoice_date){
    //     var ids = multi_details.invoice_date.split("-", 3);
    //     var year = ids[0];
    //     var month = ids[1];
    //     var day = ids[2];
    //     // var invoice_date = day + "-" + month + "-" + year;
    //     this.invice_date = {
    //       year: year,
    //       day: day,
    //       month: month
    //     }

    //     console.log(this.invice_date, "invoice_date")
    //   }
    // console.log(invoice_date)

    // this.SerialDetails.controls["mfg_date"].setValue(date);
    // }
    // console.log(manufacture_date)
    // this.SerialDetails.patchValue({
    //   "product_id": multi_details.product.product_id,
    //   "product_sub_id": multi_details.sub_product.product_sub_id,
    //   "model_no": multi_details.model_no,
    //   "serial_no": multi_details.serial_no,
    //   "invoice_date": this.invice_date,
    //   "contract_duration": multi_details.contract.amc_id,
    //   "battery_bank_id": multi_details.battery_bank_id,
    //   "sys_ah": multi_details.sys_ah,
    //   // "segment_id": this.contract_data['segment_id'],
    //   // "application_id": this.contract_data['application_id'],
    //   // "organization_name": this.contract_data.organization_name,
    //   // "invoice_number": multi_details.invoice_number,
    //   // "expiry_day": multi_details.expiry_date,
    //   // "warranty_type_id": multi_details.warranty.warranty_type_id,
    //   // "mfg_date": multi_details.mfg_date,
    //   // "voltage": multi_details.voltage,
    //   "site_id":multi_details.site_id
    // });
    // console.log(this.contract_data);
    // this.onChangeCountry(this.contract_data.country.country_id);
    // this.onChangeState(this.contract_data.country.country_id);
    // console.log(this.contract_data.value);
    this.AddressDetails.patchValue({
      "customer_code": this.contract_data.customer_code,
      "customer_name": this.contract_data.customer_name,
      "email_id": this.contract_data.email_id,
      "contact_number": this.contract_data.contact_number,
      "organization_name": this.contract_data.organization_name,
      "sap_reference_number": this.contract_data.sap_reference_number,
      "plot_number": this.contract_data.plot_number,
      "contact_person_name": this.contract_data.contact_person_name,
      "contact_person_number": this.contract_data.contact_person_number,
      "street": this.contract_data.street,
      "landmark": this.contract_data.landmark,
      "post_code": this.contract_data.landmark,
      "country_id": this.contract_data.country.country_id,
      "state_id": this.contract_data.state.state_id,
      "city_id": this.contract_data.city.city_id,
      "location_id": this.contract_data.location.location_id,
      "site_id": this.contract_data.site_id,
    })
    // console.log(this.SerialDetails.value)
    this.modalService.open(edit_serial_number, {
      size: 'lg',                                                        //specifies the size of the modal box
      // windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
    // this.getamc();
    // this.subid = multi_details.subproduct.product_sub_id
    // this.products_id = multi_details.product.product_id
    // this.getmodel(multi_details.subproduct.product_sub_id);
    // this.onChangeProduct(multi_details.product.product_id);
    // this.getapplication(this.contract_data.segment_id)
    // this.getwarrantyproduct(multi_details.model.model_id)
    // console.log(this.SerialDetails.value, "this.multi_details.")
    this.chRef.detectChanges();
    this.overlayRef.detach();
    // console.log(this.SerialDetails.value)
    // console.log(this.AddressDetails.value)
  }
  getapplication(segment_id) {
    // console.log(segment_id)
    // console.log(segment_id)
    var data = { "segment_id": segment_id };// storing the form group value
    var url = 'get_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.application = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }

  duration(item) {
    this.warranty_type.forEach(element => {
      if (element.warranty_id == item) {
        this.labelwarranty = element.warranty_duration;
        this.SerialDetails.controls['contract_duration'].setValue(this.labelwarranty);
      }
      else {
        this.labelwarranty = "Select Warranty type";
      }
    });

  }
  next(evt: any) {
    if (this.preId = "tab-selectbyid2") {
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
    else {
      this.activeId = "tab-selectbyidb";
      this.preId = "tab-selectbyida";
    }

  }
  previous(evt: any) {
    if (this.preId = "tab-selectbyid1") {
      this.activeId = "tab-selectbyid1";
      this.preId = "tab-selectbyid2";
    }
    else {
      this.activeId = "tab-selectbyida";
      this.preId = "tab-selectbyidb";
    }
  }
  previous1() {
    this.activeId = "tab-selectbyidb";
    this.preId = "tab-selectbyida";
  }
  update_serial(update_serial) {
    this.loadingUpdate = true;
    // console.log(this.SerialDetails.value, "hfhfhhfh")
    // if (this.SerialDetails.invalid) {
    //   this.toastr.error("Please check the Form", 'Error');
    // }
    // else {
    if (this.SerialDetails.value.mfg_date == undefined || this.SerialDetails.value.mfg_date == null || this.SerialDetails.value.mfg_date == '' || this.SerialDetails.value.mfg_date == "undefined-undefined-" || this.SerialDetails.value.invoice_date == "undefined-undefined-undefined") {
      this.SerialDetails.controls['mfg_date'].setValue('');
    }
    else {
      var year = this.SerialDetails.value.mfg_date.year;
      var month = this.SerialDetails.value.mfg_date.month;
      var day = this.SerialDetails.value.mfg_date.day;
      var mfg_date = day + "-" + month + "-" + year;
      var mfg_date_1 = year + "-" + month + "-" + day;
      this.SerialDetails.controls['mfg_date'].setValue(mfg_date_1);
    }

    // this.SerialDetails.value.mfg_date = mfg_date_1 

    if (this.SerialDetails.value.invoice_date == undefined || this.SerialDetails.value.invoice_date == null || this.SerialDetails.value.invoice_date == '' || this.SerialDetails.value.invoice_date == "undefined-undefined-" || this.SerialDetails.value.invoice_date == "undefined-undefined-undefined") {
      this.SerialDetails.controls['invoice_date'].setValue('');
    }
    else {
      var in_year = this.SerialDetails.value.invoice_date.year;
      var in_month = this.SerialDetails.value.invoice_date.month;
      var in_day = this.SerialDetails.value.invoice_date.day;
      var inv_date = in_day + "-" + in_month + "-" + in_year;
      var inv_date_1 = in_year + "-" + in_month + "-" + in_day;
      this.SerialDetails.controls['invoice_date'].setValue(inv_date_1);
    }
    if (this.SerialDetails.value.expiry_day == undefined || this.SerialDetails.value.expiry_day == null || this.SerialDetails.value.expiry_day == '' || this.SerialDetails.value.expiry_day == "undefined-undefined-" || this.SerialDetails.value.invoice_date == "undefined-undefined-undefined") {
      this.SerialDetails.controls['expiry_day'].setValue('');
    }
    else {
      // console.log(this.SerialDetails.value.expiry_day)
      var ex_year = this.SerialDetails.value.expiry_day.year;
      var ex_month = this.SerialDetails.value.expiry_day.month;
      var ex_day = this.SerialDetails.value.expiry_day.day;
      var ex_date = ex_day + "-" + ex_month + "-" + ex_year;
      var ex_date_1 = ex_year + "-" + ex_month + "-" + ex_day;
      this.SerialDetails.controls['expiry_day'].setValue(ex_date_1);
    }
    // this.SerialDetails.value.invoice_date = inv_date_1

    this.battery_bank_details.forEach((element, index) => {
      // console.log(this.index, (index), "adgajsg")
      if ((this.index - 1) == index) {
        // console.log("if")
        // if (this.SerialDetails.value.serial_no == element.serial_no) {  
        //   console.log("its amtiching")   
        //   console.log(this.SerialDetails.value.serial_no)
        //   var serial = {
        //     "product_id": this.SerialDetails.value.product_id,
        //     "product_sub_id": this.SerialDetails.value.product_sub_id,
        //     "model": this.SerialDetails.value.model_no,
        //     "sys_ah": this.SerialDetails.value.sys_ah,
        //     "voltage": this.SerialDetails.value.voltage,
        //     "serial_no": this.SerialDetails.value.serial_no,
        //     "invoice_number": this.SerialDetails.value.invoice_number,
        //     "invoice_date": inv_date,
        //     "mfg_date": mfg_date,
        //     "warranty_type_id": this.SerialDetails.value.warranty_type_id,
        //     "expiry_date": ex_date,
        //   };
        //   console.log(serial);
        //   this.batteryBankArray.push(serial);
        // // }

        // else {
        //   console.log(element, " element.expiry_daywotoiwer")
        //   serial = {
        //     "product_id": element.product.product_id,
        //     "product_sub_id": element.subproduct.product_sub_id,
        //     "model": element.model.model_id,
        //     "sys_ah": element.sys_ah,
        //     "voltage": element.voltage,
        //     "serial_no": element.serial_no,
        //     "invoice_number": element.invoice_number,
        //     "invoice_date": element.invoice_date,
        //     "mfg_date": element.mfg_date,
        //     "warranty_type_id": element.warranty.warranty_id,
        //     "expiry_date": element.expiry_date,
        //   };
        //   console.log(serial);
        //   this.batteryBankArray.push(serial);
        // }

        element.product_id = this.SerialDetails.value.product_id,
          element.product_sub_id = this.SerialDetails.value.product_sub_id,
          element.model = this.SerialDetails.value.model_no,
          element.sys_ah = this.SerialDetails.value.sys_ah,
          element.voltage = this.SerialDetails.value.voltage.toUpperCase(),
          element.serial_no = this.SerialDetails.value.serial_no,
          element.invoice_number = this.SerialDetails.value.invoice_number,
          element.invoice_date = inv_date,
          element.mfg_date = mfg_date,
          element.warranty_type_id = this.SerialDetails.value.warranty_type_id,
          element.expiry_date = ex_date
      }
      else {
        element.product_id = element.product.product_id,
          element.product_sub_id = element.subproduct.product_sub_id,
          element.model = element.model.model_id,
          element.sys_ah = element.sys_ah,
          element.voltage = element.voltage.toUpperCase(),
          element.serial_no = element.serial_no,
          element.invoice_number = element.invoice_number,
          element.invoice_date = element.invoice_date,
          element.mfg_date = element.mfg_date,
          element.warranty_type_id = element.warranty.warranty_id,
          element.expiry_date = element.expiry_date
      }
    })
    // console.log(this.battery_bank_details, "this.battery_bank_details")
    // console.log(this.contract_data.site_id)
    this.SerialDetails.patchValue({
      "serial_no_details": JSON.stringify(this.battery_bank_details),
      "mfg_date": mfg_date,
      "site_id": this.contract_data.site_id
    });
    // [{"product_id":10,"product_sub_id":45,"model":"367","sys_ah":"425Ah","voltage":"2V","serial_no":"IK21920250123456","invoice_number":"dfgdfg","invoice_date":{"year":2021,"month":4,"day":9},"mfg_date":"30-11-2019","warranty_type_id":1,"expiry_date":"28-02-2022"}]"
    // console.log(this.batteryBankArray);
    // console.log(this.SerialDetails.value);
    // this.AddressDetails.controls['flag'].setValue(this.site_flag);
    this.showTable = false;
    this.chRef.detectChanges();
    var data = this.SerialDetails.value;
    data['site_id'] = this.contract_data.site_id
    // console.log(data)
    var url = 'edit_contract/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                           //if sucess
        {
          this.overlayRef.attach(this.LoaderComponentPortal);
          var customer_code = this.customer_code; // customer_code  
          //  var customer_code =+"cu_0010"; // customer_code                                    
          var url = 'get_contract_details/?';             // api url for getting the details with using post params
          var id = "customer_code";
          this.ajax.getdataparam(url, id, customer_code).subscribe((result1) => {
            // this.modalService.dismissAll();
            if (result1.response.response_code == "200" || result1.response.response_code == "400") {
              this.contract_details = result1.response.data;                    //storing the api response in the array
              this.end_date = this.contract_details.expiry_day
              this.loadingUpdate = false;
              this.showTable = true;
              this.chRef.detectChanges();
              this.modalService.dismissAll();
              // this.modalService.open(update_serial, {
              //   backdrop: 'static',                                    // modal will not close by outside click
              //   keyboard: false,                                       // modal will not close by keyboard click
              //   windowClass: "center-modalextralg"                                  // modal will not close by keyboard click
              // });
              this.overlayRef.detach();
            }
            else if (result1.response.response_code == "500") {
              // console.log("500")
              this.contract_details = [];
              this.contract_message = result1.response.message;
              this.loadingUpdate = false;
              this.showTable = true;
              this.chRef.detectChanges();
              // console.log(this.contract_message)

              // this.modalService.open(view_contract, {
              //   size: 'lg',                                                        //specifies the size of the modal box
              //   backdrop: 'static',                                    // modal will not close by outside click
              //   keyboard: false,                                       // modal will not close by keyboard click
              // });
              this.overlayRef.detach();
            }
            else {
              this.contract_details = [];
              this.contract_message = "Something Went Wrong";
              this.loadingUpdate = false;
              this.showTable = true;
              this.chRef.detectChanges();
              // this.modalService.open(view_contract, {
              //   size: 'lg',                                                        //specifies the size of the modal box
              //   backdrop: 'static',                                    // modal will not close by outside click
              //   keyboard: false,                                       // modal will not close by keyboard click
              // });
              this.overlayRef.detach();
            }
          }, (err) => {
            console.log(err);                                        //prints if it encounters an error
            this.loadingUpdate = false;
          }); this.toastr.success(result.response.message, 'Success');        //toastr message for success
          //to close the modal box
        }
        else if (result.response.response_code == "500") {
          // this.errors = result.response.message;
          this.loadingUpdate = false;
          this.toastr.error(result.response.message, 'Error');
        }
        else {
          this.loadingUpdate = false;
          this.errors = "Something Went Wrong";

        }
      }, (err) => {                                                           //if error
        this.loadingUpdate = false;
        console.log(err);                                                 //prints if it encounters an error
      });
  }
  // }
  // function to call contract edit api
  // edit_contract_details(contract_table) {
  //   this.loadingUpdate = true;
  //   if (this.Edit_contract_form.invalid) {
  //     this.toastr.error("Please check the Form", 'Error');
  //   }
  //   else {
  //     this.showTable = false;
  //     this.chRef.detectChanges();
  //     var data = this.Edit_contract_form.value;
  //     var url = 'edit_contract/'                                           //api url of edit api                                    
  //     this.ajax.postdata(url, data)
  //       .subscribe((result) => {
  //         if (result.response.response_code == "200")                           //if sucess
  //         {
  //           this.overlayRef.attach(this.LoaderComponentPortal);
  //           var customer_code = this.customer_code; // customer_code  
  //           //  var customer_code =+"cu_0010"; // customer_code                                    
  //           var url = 'get_contract_details/?';             // api url for getting the details with using post params
  //           var id = "customer_code";
  //           this.ajax.getdataparam(url, id, customer_code).subscribe((result1) => {
  //             this.modalService.dismissAll();
  //             if (result1.response.response_code == "200" || result1.response.response_code == "400") {
  //               this.contract_details = result1.response.data;                    //storing the api response in the array
  //               this.end_date = this.contract_details.expiry_day
  //               this.loadingUpdate = false;
  //               this.showTable = true;
  //               this.chRef.detectChanges();
  //               this.modalService.open(contract_table, {
  //                 size: 'lg',                                                       //specifies the size of the modal box
  //                 backdrop: 'static',                                    // modal will not close by outside click
  //                 keyboard: false,                                       // modal will not close by keyboard click
  //               });
  //               this.overlayRef.detach();
  //             }
  //             else if (result1.response.response_code == "500") {
  //               this.contract_details = [];
  //               this.contract_message = result1.response.message;
  //               this.loadingUpdate = false;
  //               this.showTable = true;
  //               this.chRef.detectChanges();
  //               this.modalService.open(contract_table, {
  //                 size: 'lg',                                                        //specifies the size of the modal box
  //                 backdrop: 'static',                                    // modal will not close by outside click
  //                 keyboard: false,                                       // modal will not close by keyboard click
  //               });
  //               this.overlayRef.detach();
  //             }
  //             else {
  //               this.contract_details = [];
  //               this.contract_message = "Something Went Wrong";
  //               this.loadingUpdate = false;
  //               this.showTable = true;
  //               this.chRef.detectChanges();
  //               this.modalService.open(contract_table, {
  //                 size: 'lg',                                                        //specifies the size of the modal box
  //                 backdrop: 'static',                                    // modal will not close by outside click
  //                 keyboard: false,                                       // modal will not close by keyboard click
  //               });
  //               this.overlayRef.detach();
  //             }
  //           }, (err) => {
  //             console.log(err);                                        //prints if it encounters an error
  //             this.loadingUpdate = false;
  //           }); this.toastr.success(result.response.message, 'Success');        //toastr message for success
  //           //to close the modal box
  //         }
  //         else if (result.response.response_code == "500") {
  //           this.errors = result.response.message;
  //           this.loadingUpdate = false;
  //         }
  //         else {
  //           this.loadingUpdate = false;
  //           this.errors = "Something Went Wrong";

  //         }
  //       }, (err) => {                                                           //if error
  //         this.loadingUpdate = false;
  //         console.log(err);                                                 //prints if it encounters an error
  //       });
  //   }
  // }



  edit_contract_details(edit_contract_details) {
    // console.log(this.AddressDetails, "this.AddressDetails.i")
    this.loadingUpdate = true;
    if (this.AddressDetails.invalid) {
      this.toastr.error("Please check the Form", 'Error');
    }
    else {
      // console.log(this.AddressDetails.value);
      // this.AddressDetails.controls['flag'].setValue(this.site_flag);
      this.showTable = false;
      this.chRef.detectChanges();
      var data = this.AddressDetails.value;
      var url = 'edit_contract/'                                           //api url of edit api                                    
      this.ajax.postdata(url, data)
        .subscribe((result) => {
          if (result.response.response_code == "200")                           //if sucess
          {
            this.overlayRef.attach(this.LoaderComponentPortal);
            var customer_code = this.customer_code; // customer_code  
            //  var customer_code =+"cu_0010"; // customer_code                                    
            var url = 'get_contract_details/?';             // api url for getting the details with using post params
            var id = "customer_code";
            this.ajax.getdataparam(url, id, customer_code).subscribe((result1) => {
              // this.modalService.dismissAll();
              if (result1.response.response_code == "200" || result1.response.response_code == "400") {
                this.contract_details = result1.response.data;                    //storing the api response in the array
                this.end_date = this.contract_details.expiry_day
                this.loadingUpdate = false;
                this.showTable = true;
                this.chRef.detectChanges();
                this.modalService.dismissAll();
                this.modalService.open(edit_contract_details, {
                  backdrop: 'static',                                    // modal will not close by outside click
                  keyboard: false,
                  windowClass: "center-modalextralg"                                       // modal will not close by keyboard click
                });
                this.overlayRef.detach();
                // this.element.style.display = 'none';
                // document.body.classList.remove('jw-modal-open');
                // console.log("before close")
                // this.modalService.close(edit_site)
                // this.activeModalService.close(edit_site);
                // this.IsmodelShow=true;// set false while you need open your model popup

                // console.log("adfter close")
                // close(result?: any): void;
                // private _dismiss;
              }
              else if (result1.response.response_code == "500") {
                this.contract_details = [];
                this.contract_message = result1.response.message;
                this.loadingUpdate = false;
                this.showTable = true;
                this.chRef.detectChanges();
                // this.modalService.open(view_contract, {
                //   size: 'lg',                                                        //specifies the size of the modal box
                //   backdrop: 'static',                                    // modal will not close by outside click
                //   keyboard: false,                                       // modal will not close by keyboard click
                // });
                this.overlayRef.detach();
              }
              else {
                this.contract_details = [];
                this.contract_message = "Something Went Wrong";
                this.loadingUpdate = false;
                this.showTable = true;
                this.chRef.detectChanges();
                // this.modalService.open(view_contract, {
                //   size: 'lg',                                                        //specifies the size of the modal box
                //   backdrop: 'static',                                    // modal will not close by outside click
                //   keyboard: false,                                       // modal will not close by keyboard click
                // });
                this.overlayRef.detach();
              }
            }, (err) => {
              console.log(err);                                        //prints if it encounters an error
              this.loadingUpdate = false;
            }); this.toastr.success(result.response.message, 'Success');        //toastr message for success
            //to close the modal box
          }
          else if (result.response.response_code == "500") {
            this.errors = result.response.message;
            this.loadingUpdate = false;
          }
          else {
            this.loadingUpdate = false;
            this.errors = "Something Went Wrong";

          }
        }, (err) => {                                                           //if error
          this.loadingUpdate = false;
          console.log(err);                                                 //prints if it encounters an error
        });
    }
  }
  test(data) {
    this.node_value = data;
    if (this.node_value.level == '1') {
      this.onChangeCountry(this.node_value.country);
      this.Edit_contract_form.controls['country_id'].setValue(data.country);
      this.Edit_contract_form.controls['location_id'].setValue(this.node_value.country);
      this.Edit_contract_form.controls['location_label'].setValue("country")
      this.activeModal.close()
    }
    else if (this.node_value.level == '2') {
      var country_id = this.node_value.country;
      var country = +country_id; // state: number
      var id = "country_id";
      var url = 'get_location/?';
      var node_id = "node_id";
      var node = +this.node_value.id;                                  // api url for getting the cities
      this.ajax.getdatalocation(url, id, country, node, node_id).subscribe((result) => {
        if (result.response.response_code == "200" || result.response_code == "400") {
          this.states = result.response.data;              //storing the api response in the array                   
          this.Edit_contract_form.controls['state_id'].setValue(this.node_value.state);             //view city name in view modal box
          this.Edit_contract_form.controls['location_id'].setValue(this.node_value.state);
          this.Edit_contract_form.controls['location_label'].setValue("state")
          this.activeModal.close()
        }
        else if (result.response.response_code == "500") {
          this.errors = result.response.message;
          this.overlayRef.detach();
        }
        else {
          this.errors = "Something Went Wrong";
          this.overlayRef.detach();
        }
      }, (err) => {
        console.log(err);                                        //prints if it encounters an error
      });

    }
    else if (this.node_value.level == '3') {
      var stateid = this.node_value.state;
      var state = +stateid; // state: number
      var id = "state_id";
      var url = 'get_location/?';
      var node_id = "node_id";
      var node = +this.node_value.id;                                  // api url for getting the cities
      this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
        if (result.response.response_code == "200" || result.response_code == "400") {
          this.cities = result.response.data;              //storing the api response in the array
          this.Edit_contract_form.controls['city_id'].setValue(this.node_value.city);             //view city name in view modal box
          this.Edit_contract_form.controls['location_id'].setValue(this.node_value.city);
          this.Edit_contract_form.controls['location_label'].setValue("city");
          this.activeModal.close()
        }
        else if (result.response.response_code == "500") {
          this.errors = result.response.message;
          this.overlayRef.detach();
        }
        else {
          this.errors = "Something Went Wrong";
          this.overlayRef.detach();
        }
      },
        (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
    }
  }
  fetchNews(evt: any) {
    this.activeId = evt.nextId;
    this.evt = evt; // has nextId that you can check to invoke the desired function
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }




  generate_login(customer) {
    this.EmailGroup.setValue({
      "email_id": customer.email_id,
      "customer_code": customer.customer_code
    })
    // control = new FormControl({ value: email_id, disabled: true })
    if (customer.email_id == "" || customer.email_id == null || customer.email_id == undefined) {
      // console.log(customer.email_id)
      this.value_disabled = true
      // console.log(customer.email_id)
    }
    else {
      this.value_disabled = false
      // console.log(customer.email_id)
    }

  }
  open_email_update(email_modal, flag) {
    this.login_flag = flag
    this.modalService.open(email_modal, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      windowClass: "center-modalsm",
      keyboard: false
    });
  }
  generate_customer_login() {
    var data = this.EmailGroup.value
    // console.log(data)
    var url = 'cus_login_generation/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                           //if sucess
        {
          this.toastr.success(result.response.message, 'Success!');  //toastr message for sucess
          this.modalService.dismissAll()
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error!');  //toastr message for sucess
        }
        else {
          this.toastr.error(result.response.message, 'Error!');  //toastr message for sucess
        }
      }, (err) => {                                                           //if error
        // this.loadingUpdate = false;
        console.log(err);                                                 //prints if it encounters an error
      });
    // }
  }
  view_performance_site(per_modal, customer_code,site) {
    this.customer_code = customer_code;
    this.site_code=site;
    this.ByDefault1 = false;
    this.error_type = "";
    // console.log("wesasdfsadsdf")
    this.modalService.open(per_modal, {
      size: 'lg', //specifies the size of the dialog box      
      windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }

  perfr_cert_upload_site() {
    console.log(this.site_code)
    // console.log(this.UploadCertificate.value)
    // var certificate = this.UploadCertificate.value
    // console.log(this.video_word, " this.video_word ")
    // console.log(this.pdf, "this.pdf ")
    const formData: FormData = new FormData(); //convert the formdata
    formData.append('performance_certificate', this.perf_certi); //append the name of excel file
    formData.append('performance_certificate_type', this.pdf); //append the name of excel file
    formData.append('customer_code', this.customer_code); //append the name of excel file
    formData.append('site_id', this.site_code);
    var method = "post";
    var data = formData;
    var url = 'upload_perf_certificate_site/' //api url of add
    this.ajax.ajaxpost(data, method, url).subscribe(data => {
      if (data['response'].response_code == "200") {
        this.toastr.success(data['response'].message, 'Success'); //toastr message for success
        this.modalService.dismissAll();                                 //to close the modal box
        this.showTables = false;
        this.chRef.detectChanges();
        this.get_all_customers();                                                //reloading the component
      }

      else if (data['response'].response_code == "400") {
        this.toastr.error(data['response'].message, 'Success');

      }
      else if (data['response'].response_code == "500") {
        this.toastr.error(data['response'].message, 'Success');

      }
      else {
        this.toastr.error(data['response'].message, 'Success');

      }
    }, (err) => {


      console.log(err); //prints if it encounters an error
    });
  }
  view_performance(per_modal, customer_code) {
    this.customer_code = customer_code;
    this.ByDefault1 = false;
    this.error_type = "";
    // console.log("wesasdfsadsdf")
    this.modalService.open(per_modal, {
      size: 'lg', //specifies the size of the dialog box      
      windowClass: "center-modalsm",      //modal popup custom size 
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }

  perfr_cert_upload() {
    // console.log(this.UploadCertificate.value)
    // var certificate = this.UploadCertificate.value
    // console.log(this.video_word, " this.video_word ")
    // console.log(this.pdf, "this.pdf ")
    const formData: FormData = new FormData(); //convert the formdata
    formData.append('performance_certificate', this.perf_certi); //append the name of excel file
    formData.append('performance_certificate_type', this.pdf); //append the name of excel file
    formData.append('customer_code', this.customer_code); //append the name of excel file
    var method = "post";
    var data = formData;
    var url = 'upload_perf_certificate/' //api url of add
    this.ajax.ajaxpost(data, method, url).subscribe(data => {
      if (data['response'].response_code == "200") {
        this.toastr.success(data['response'].message, 'Success'); //toastr message for success
        this.modalService.dismissAll();                                 //to close the modal box
        this.showTables = false;
        this.chRef.detectChanges();
        this.get_all_customers();                                                //reloading the component
      }

      else if (data['response'].response_code == "400") {
        this.toastr.error(data['response'].message, 'Success');

      }
      else if (data['response'].response_code == "500") {
        this.toastr.error(data['response'].message, 'Success');

      }
      else {
        this.toastr.error(data['response'].message, 'Success');

      }
    }, (err) => {


      console.log(err); //prints if it encounters an error
    });
  }


  downloadPdf(customer_name, pdfUrl: string) {

    var pdfName = customer_name
    // console.log(pdfName, "pdfName")
    // console.log(pdfUrl, "pdfUrl")
    // var pdf = "https://" + pdfUrl
    var pdf = environment.image_static_ip + pdfUrl
    // console.log(pdf)
    FileSaver.saveAs(pdf, pdfName);
  }


  //functions for image/video upload
  certificateUpload(event) {
    // console.log("inside the function")
    this.ByDefault1 = false;

    this.video_word_name = event.target.files[0].name;
    // console.log(event.target.files[0].type, "event.target.files[0].type")
    this.ByDefault1 = true;
    // if (event.target.files[0].type == 'pdf') {
    // console.log("pdf")
    this.pdf = event.target.files[0].type;
    // console.log(this.pdf)
    var validExts = new Array("application/pdf", ".pdf");
    // console.log("word")
    // this.UploadCertificate.value.performance_certificate_type = event.target.files[0].type;
    this.pdf = event.target.files[0].type;
    // console.log(this.pdf)
    const allowed_types = ['application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword'];
    if (this.pdf == undefined) {
      this.ByDefault1 = true;
      this.upload_disable = false

      this.error_type = "Please select file";
      // this.UploadCertificate.controls['performance_certificate'].setValue('');
    }
    else if (this.pdf.length > 0) {
      this.ByDefault1 = true;
      this.upload_disable = false

      if (validExts[0] != this.pdf) {
        this.upload_disable = false

        this.error_type = "Please select correct extension";
        // this.UploadCertificate.controls['performance_certificate'].setValue('');
      }
      else {
        this.upload_disable = false

        this.pdf = "pdf"
        this.error_type = "";
      }
    }
    else if (!_.includes(allowed_types, this.pdf)) {
      this.ByDefault1 = true;
      this.upload_disable = false

      this.error_type = "Only Video will be allowed ( docs,doc )";
      // this.UploadCertificate.controls['performance_certificate'].setValue('');
    }
    else if (this.pdf == undefined) {
      this.ByDefault1 = true;
      this.upload_disable = false

      this.error_type = "Please select file";
      // this.UploadCertificate.controls['performance_certificate'].setValue('');
    }
    else if (this.pdf.length > 0) {
      this.ByDefault1 = true;
      this.upload_disable = false

      if (this.size > 15728640) {
        this.upload_disable = false

        this.error_type = "Maximum size is 20 MB";
        // this.UploadCertificate.controls['performance_certificate'].setValue('');
      }
      else {
        this.upload_disable = false
        this.pdf = "word"
        this.error_type = "";
        // this.UploadCertificate.controls['performance_certificate'].setValue('');

      }
    }
    if (this.pdf == 'pdf' || this.pdf == 'word') {
      this.upload_disable = true

      // console.log("pdf or word")
      let fileList: FileList = event.target.files[0];
      this.perf_certi = fileList;
      this.video_word = fileList;
      // var per_cer_file = this.UploadCertificate.value.performance_certificate
      // console.log(this.video_word, "per_cer_file")
      // console.log(this.pdf, "this.pdf")
      // this.UploadCertificate.controls['performance_certificate'].setValue(this.video_word);
      // this.UploadCertificate.controls['performance_certificate_type'].setValue(this.pdf);
      // this.UploadCertificate.value.valid = true
      // console.log(this.UploadCertificate)

    }
    else {
      this.upload_disable = false
      this.error_type = "Extension should be word or pdf"
      // this.UploadCertificate.controls['performance_certificate'].setValue('');

    }


  }

}