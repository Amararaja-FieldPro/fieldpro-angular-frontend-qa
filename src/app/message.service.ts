// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFirestore } from '@angular/fire/firestore';
// // import { auth } from 'firebase/auth';
// import * as firebase from 'firebase/app';


// // Device detector
// import { DeviceDetectorService } from 'ngx-device-detector';

// import { BehaviorSubject } from 'rxjs';

// @Injectable({
//     providedIn: 'root'
// })
// export class AuthenticationService {
//     private _user: any = new BehaviorSubject(null);
//     messaging: any;
//     constructor(
//         private afAuth: AngularFireAuth,
//         private _afs: AngularFirestore,
//         private deviceService: DeviceDetectorService
//     ) {

//         this.afAuth.user.subscribe(async user => {
//             try {
//                 this.setUser(user);
//                 const token = return this.messaging.requestPermission().then(() => {
//                     return this.messaging.getToken();
//                 });
//                 this.alert.this.messaging.onMessage((payload: any) => {
//                     this.showSuccess(
//                         `${payload.notification.title} : ${payload.notification.body}`
//                     );
//                 });
//                 this.updatePushToken(token);
//             } catch (error) {
//                 console.log("Error Message ", error);
//             }

//         });
//     }
//     private setUser(user: any) {
//         this._user.next(user);
//     }

//     public get user(): BehaviorSubject<any> {
//         return this._user;
//     }


//     /**
//    * Update the User's push token
//    * @param token string
//    */
//     public async updatePushToken(token: string) {
//         try {
//             const devices = await this._afs.firestore.collection('Devices').where('token', '==', token).get();

//             if (devices.empty) {
//                 const deviceInfo = this.deviceService.getDeviceInfo();
//                 const data = {
//                     token: token,
//                     userId: this._user._value.uid,
//                     deviceType: 'web',
//                     deviceInfo: {
//                         browser: deviceInfo.browser,
//                         userAgent: deviceInfo.userAgent
//                     },
//                     // createdAt: firestore.FieldValue.serverTimestamp()
//                 };

//                 await this._afs.firestore.collection('Devices').add(data);
//                 console.log('New Device Added');
//             } else {
//                 console.log('Already existing Device');
//             }
//         } catch (error) {
//             console.log("Error Message", error);
//         }
//     }


// }




// import { Injectable } from '@angular/core';
// import { AngularFireMessaging } from '@angular/fire/messaging';
// import { BehaviorSubject } from 'rxjs'
// @Injectable()
// export class MessagingService {
//     currentMessage = new BehaviorSubject(null);
//     constructor(private angularFireMessaging: AngularFireMessaging) {
//         this.angularFireMessaging.messaging.subscribe(
//             (_messaging) => {
//                 _messaging.onMessage = _messaging.onMessage.bind(_messaging);
//                 _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
//                 console.log(_messaging.onTokenRefresh,"_messaging.onTokenRefresh")
//             }
//         )
//     }
//     requestPermission() {
//         console.log(this.angularFireMessaging.requestToken, "Tokens")
//         this.angularFireMessaging.requestToken.subscribe(
//             (token) => {
//                 console.log(token,"token");
//             },
//             (err) => {
//                 console.error('Unable to get permission to notify.', err);
//             }
//         );
//     }
//     receiveMessage() {
//         this.angularFireMessaging.messages.subscribe(
//             (payload) => {
//                 console.log("new message received. ", payload);
//                 this.currentMessage.next(payload);
//             })
//     }
// }


import firebase from "firebase/app";
import "firebase/messaging";
export class MessagingService {
 currentMessage: any;
 getMessagingObject() {
    //  console.log("get message")
  // [START messaging_get_messaging_object]
  const messaging = firebase.messaging();
  // [END messaging_get_messaging_object]
}

 receiveMessage() {
  // alert("this.receiveMessage")
  const messaging = firebase.messaging();
  // [START messaging_receive_message]
  // Handle incoming messages. Called when:
  // - a message is received while the app has focus
  // - the user clicks on an app notification created by a service worker
  //   `messaging.onBackgroundMessage` handler.
  messaging.onMessage((payload) => {
    // console.log('Message received. ', payload);
    this.currentMessage.next(payload);
    // ...
  });
  // [END messaging_receive_message]
}

 getToken() {
  //  alert("get token")
  const messaging = firebase.messaging();
  // [START messaging_get_token]
  // Get registration token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  // console.log(messaging.getToken({ vapidKey: 'BCbFtfz8hrrUOt1kBrKU5ChJ-5_jHxC7CoCjdrXNei1CwFTK_3BY9txZg4GO0nRaz6rX1TOHdw3TzEkaugY8Ekw' }).then((currentToken)=>{}),'augasdad')
  messaging.getToken({ vapidKey: 'BCbFtfz8hrrUOt1kBrKU5ChJ-5_jHxC7CoCjdrXNei1CwFTK_3BY9txZg4GO0nRaz6rX1TOHdw3TzEkaugY8Ekw' }).then((currentToken) => {
    // console.log("11111")
    if (currentToken) {
        // console.log(currentToken,"currentToken")
         localStorage.setItem('current_token',currentToken)

        // Send the token to your server and update the UI if necessary
      // ...
    } else {
      // Show permission request UI
      // console.log('No registration token available. Request permission to generate one.');
      // ...
    }
  }).catch((err) => {
    // console.log('An error occurred while retrieving token. ', err);
    // ...
  });
  // [END messaging_get_token]
}

 requestPermission() {
  // alert("requestPermission")

  // [START messaging_request_permission]
  Notification.requestPermission().then((permission) => {
    // alert(permission)
    if (permission === 'granted') {
      // console.log('Notification permission granted.');
      // TODO(developer): Retrieve a registration token for use with FCM.
      // ...
    } else {
      // console.log('Unable to get permission to notify.');
    }
  });
  // [END messaging_request_permission]
}

 deleteToken() {
  // alert("deleteToken")
  const messaging = firebase.messaging();

  // [START messaging_delete_token]
  messaging.deleteToken().then(() => {
    // console.log('Token deleted.');
    // ...
  }).catch((err) => {
    // console.log('Unable to delete token. ', err);
  });
  // [END messaging_delete_token]
}
}