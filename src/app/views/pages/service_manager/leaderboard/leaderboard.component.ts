import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
	selector: 'kt-leaderboard',
	templateUrl: './leaderboard.component.html',
	styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
	SearchLevel: FormGroup;
	// leaderboard_all_data:any;
	technicians_details: any;
	leaderboard: any;
	level: any;
	Level: any;
	showTable: boolean = false;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	dataTable: any;
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	leaderboard_all_data = [];

	@ViewChild('dataTable', { static: true }) table;

	constructor(private ajax: ajaxservice, private renderer: Renderer2, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.SearchLevel = new FormGroup({
			"level": new FormControl('', Validators.required)
		})
	}

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10
			// processing: true
		};
		this.level = "";
		this.getRewardDetails();
		// this.getLeaderBoard();

	}

	onChangeLevel() {
		this.level = this.SearchLevel.value.level;
		this.showTable = false;
		this.getRewardDetails();
	}
	get_tech(tech_id, view_techniciandetails) {

		var tech_code = tech_id; // country: number                                    
		var url = 'get_technician_data/?';             // api url for getting the details with using post params
		var id = "technician_code";
		console.log(url, id, tech_code);
		this.ajax.getdataparam(url, id, tech_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.technicians_details = result.response.data;
				this.modalService.open(view_techniciandetails, {
					size: 'lg',
					windowClass: "center-modalsm",     //modal popup resize
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});                                       //reloading the component
			}

		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getRewardDetails() {

		this.overlayRef.attach(this.LoaderComponentPortal);

		var level = this.level; // get level
		var Level = "level";
		var url = 'get_reward_details/?';                               // api url for getting the cities
		this.ajax.getdataparam(url, Level, level).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.leaderboard_all_data = result.response.data;
				this.showTable = true;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				this.overlayRef.detach();

			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getLeaderBoard() {
		var url = 'get_reward/';                                  // api url for getting the cities
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.leaderboard = result.response.data;
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}

	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
	}

}
