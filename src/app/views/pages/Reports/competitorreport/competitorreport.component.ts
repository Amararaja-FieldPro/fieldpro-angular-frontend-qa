import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { FormControl } from '@angular/forms';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
// import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { environment } from '../../../../../environments/environment';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { ExcelService } from '../../../../excelservice';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
declare var $: any;
import * as xlsx from 'xlsx';
@Component({
  selector: 'kt-competitorreport',
  templateUrl: './competitorreport.component.html',
  styleUrls: ['./competitorreport.component.scss']
})
export class CompetitorreportComponent implements OnInit {
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  showTable: string;
  dtOptions: DataTables.Settings = {};
  overlayRef: OverlayRef;
  // @ViewChild(DataTableDirective, { static: false })
  // dtElement: DataTableDirective;
  // dtTrigger: Subject<any> = new Subject();
  // dtInstance: Promise<DataTables.Api>;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  competitor_data: any;
  competitorExport: any;
  constructor(private excelservice: ExcelService, public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private overlay: Overlay, private toastr: ToastrService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 50,
      lengthMenu: [50,100,500],
      processing: true
    };
    $('#datatables').DataTable().destroy();
    setTimeout(function () {
      $('#datatables').DataTable(this.dtOptions)
    }, 2000);//wait 2 seconds
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    // this.title = 'CSI Response Analysis - Percentage';
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
    this.end_date = Fyear + "-" + '03' + "-" + '28';
    if (this.start_date.month>4){
      // this.start_date = Fyear + "-" + '04' + "-" + '01';
      // this.end_date = (Fyear+1) + "-" + '03' + "-" + '31';
      var finance=Fyear + "-" + (Fyear+1)
    }
    else{
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      // this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance=(Fyear - 1) + "-" + Fyear
    }
    var data = {
      "employee_code": emp_code,
      "start_date": this.start_date,
      "end_date": this.end_date,
      "state": [],
      "product": [],
      "product_sub_id": [],
      "model_no": [],
      "service_desk": [],
      "technician": [],
      "application": [],
      "segment": [],
      "customer": [],
      "finance_year": finance,
      "fin_flag": "Year"
    };

    this.competitor_report(data)
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const ec = (r, c) => {
      return XLSX.utils.encode_cell({ r: r, c: c })
    }
    const delete_row = (ws, row_index) => {
      let range = XLSX.utils.decode_range(ws["!ref"])
      for (var R = row_index; R < range.e.r; ++R) {
        for (var C = range.s.c; C <= range.e.c; ++C) {
          ws[ec(R, C)] = ws[ec(R + 1, C)]
        }
      }
      range.e.r--
      ws['!ref'] = XLSX.utils.encode_range(range.s, range.e)
    }
    delete_row(worksheet, 0)
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  exportToExcel() {
    // const ws: xlsx.WorkSheet =
    //   xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    // const wb: xlsx.WorkBook = xlsx.utils.book_new();
    // xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    // xlsx.writeFile(wb, 'CompetitorReport.xlsx');
    this.excelservice.exportAsExcelFile(this.competitorExport, 'Competitor Reports');
  }
  updateDataMain(datas) {
    this.competitor_report(datas)

  }
  public competitor_report(data) {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.showTable = 'false';
    var url = 'competitor_report/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.competitor_data = result.response.data;              //storing the api response in the array
        this.competitorExport=result.response.Export
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 50,
          lengthMenu: [50,100,500],
          // data: result.response.data,
          processing: true,
          //   columns: [{ data: 'SNO' },{ data: 'Batterymake' }, { data: 'BatteryRating' },{ data: 'ManufacturingDate' },{ data: 'I&CDate' },{ data: 'SYSAH' },{ data: 'Application' },
          //   { data: 'battery_cell' },{ data: 'source_charging' },{ data: 'rating_charging' },{ data: 'system_voltage' },{ data: 'no_of_bc_bank' },{ data: 'no_of_bc_per_bank' },{ data: 'mn_ah' },{ data: 'load_battery' },{ data: 'capture_complaints' },{ data: 'failed_quantity' },
          //   { data: null,     
          //     render: function (data, type, row) {
          //     return `
          //     <div class="text-center">
          //     <a style="cursor: pointer;"
          //     onclick="editMember(data[0])"><i class="fas fa-download"
          //       style="color:#242fb7;cursor: pointer;" aria-hidden="true"></i></a>
          //     </div>
          // `;
          // }, "width": "5%" }
          //   ],
          // order: [],
          // destroy: true
        };
        $('#datatables').DataTable().destroy();
        setTimeout(function () {
          $('#datatables').DataTable(this.dtOptions)
        }, 2000);//wait 2 seconds
        this.overlayRef.detach();
        this.showTable = 'true';
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      this.overlayRef.detach();
    });
  }
  downloadimage(image) {
    // if (image != "") {
    var pdf = environment.image_static_ip + image
    FileSaver.saveAs(pdf, "Battery");

    // }
  }
}
