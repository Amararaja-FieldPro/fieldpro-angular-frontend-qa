import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { FormControl } from '@angular/forms';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';// To use toastr
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { ExcelService } from '../../../../excelservice'
import * as xlsx from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
declare var $: any;
import { FiltersComponent } from '../filters/filters.component';

@Component({
  selector: 'kt-ftrreport',
  templateUrl: './ftrreport.component.html',
  styleUrls: ['./ftrreport.component.scss']
})
export class FtrreportComponent implements OnInit {
  ftr_data: any;
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  showTable: string;
  dtOptions: DataTables.Settings = {};
  overlayRef: OverlayRef;
  ftr_data_export: any;
  export_data: any[];
  data_field: any;
  value_data: any;
  export_input: { employee_code: string; start_date: any; end_date: any; service_desk: any[]; state: any[]; technician: any[]; customer: string; segment: any[]; product: any[]; product_sub_id: any[]; model_no: any[]; region: any[]; call_catgory: any[]; flag: number; fin_flag: string; };
  constructor(public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private overlay: Overlay, private toastr: ToastrService, private excelservice: ExcelService) { }
  @ViewChild('div', { static: true }) div: ElementRef;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;

  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      responsive: true,
      lengthMenu: [10, 25, 50]
    };
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    this.end_date = moment(this.todates.value._d).format("YYYY-MM-DD"); //change the date format
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    this.start_date = Fyear + "-" + Fmonth + "-" + 1;
    var datas = {
      "employee_code": emp_code, "start_date": this.start_date,
      "end_date": this.end_date, "service_desk": [], "state": [],
      "technician": [],
      "customer": '',
      "segment": [],
      "product": [],
      "product_sub_id": [],
      "model_no": [],
      "region": [],
      "call_catgory": [],
      "flag": 1,
      "fin_flag": "Month"
    };
    this.export_input=datas
    // this.ftrreportdaata(datas)
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    // this.overlayRef.attach(this.LoaderComponentPortal);
    this.ftrreportdaata(datas)

  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const ec = (r, c) => {
      return XLSX.utils.encode_cell({ r: r, c: c })
    }
    const delete_row = (ws, row_index) => {
      let range = XLSX.utils.decode_range(ws["!ref"])
      for (var R = row_index; R < range.e.r; ++R) {
        for (var C = range.s.c; C <= range.e.c; ++C) {
          ws[ec(R, C)] = ws[ec(R + 1, C)]
        }
      }
      range.e.r--
      ws['!ref'] = XLSX.utils.encode_range(range.s, range.e)
    }
    delete_row(worksheet, 0)
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  public ftrreportdaata(data) {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.showTable = 'false';
    var url = 'ftr_analyse/';
    this.data_field = data;
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        // this.export_data = result.response.data
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10,
          lengthMenu: [10, 25, 50],
          data: result.response.data,
          columns: [{ data: 'SNO' }, { data: 'TicketId' }, { data: 'CustomerName' }, { data: 'SiteId' }, { data: 'Address' },{ data: 'EndUserOrganizationName' }, { data: 'ContactPersonName' }, { data: 'ContactPersonNumber' }, { data: 'AssignedBy' },{ data: 'Source' }, { data: 'Destination' },
          { data: 'ModeOfTravel' }, { data: 'LocationType' }, { data: 'SysAh' }, { data: 'Voltage' }, { data: 'Noofkmtravelled' },
          { data: 'ClaimAmount' },
          // { data: 'OtherExpenses' },
          { data: 'TotalAmount' },
          { data: 'TicketStatus' },
          { data: 'TicketEndDate' }
          ],
          destroy: true
        };

        //storing the api response in the array
        $('#datatables').DataTable(this.dtOptions)
        this.overlayRef.detach();
        this.showTable = 'true';
        this.chRef.detectChanges();

      }
   

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      this.overlayRef.detach();
    });


  }
  updateDataMain(datas) {
    this.value_data=datas;
  }
  export_ftrdata() {
    this.export_data = [];
    var url = 'ftr_analyse/';
    if (this.value_data==[] || this.value_data==undefined || this.value_data==""){
      var data=this.export_input
    }
    else{
      data=this.value_data
    }
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.export_data = result.response.data
        this.excelservice.exportAsExcelFile(this.export_data, 'FTR Reports');
      }
      else if (result.response.response_code == "400") {
        this.toastr.error("There is no data", 'Error')
        this.chRef.detectChanges();
      }
       else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.chRef.detectChanges();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')

        this.chRef.detectChanges();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    } 
    
 
}
