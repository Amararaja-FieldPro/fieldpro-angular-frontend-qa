import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {PackagetypeComponent} from './packagetype.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    // PackagetypeComponent
  ],
  imports: [
    CommonModule,
   FormsModule,
   RouterModule
  ]
})
export class PackagetypeModule { }
