import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { ActivatedRoute, Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables'; // datatable
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'kt-smtpsettings',
  templateUrl: './smtpsettings.component.html',
  styleUrls: ['./smtpsettings.component.scss']
})
export class SmtpsettingsComponent implements OnInit {
  smtpsetting_details: any;
  submitted = false;
  edit_data: any;
  Addsmtpplan: FormGroup;     //form group for add site setting plan
  Editsmtpplan: FormGroup;     //form group for edit site setting plan
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  overlayRef: OverlayRef;
  showTable: boolean = false;
  loadingAdd: boolean = false;
  loadingUpdate: boolean = false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  error: any;

  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router,
    private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    //form group for add site settings
    this.Addsmtpplan = new FormGroup({
      "smtp_security_type": new FormControl("", [Validators.required]),
      "smtp_hostname": new FormControl("", [Validators.required,this.noWhitespace]),
      "smtp_port": new FormControl("", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(4), Validators.minLength(2)]),
      "smtp_username": new FormControl("", [Validators.required,  Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "smtp_password": new FormControl("", [Validators.required, Validators.minLength(6)]),
    })
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getsmtpsetting();

  }
  getsmtpsetting() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_smtp_settings/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.smtpsetting_details = result.response.data;                    //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();

      }

      else if (result.response.response_code == "500") {              //if failure                                               //if not sucess
        this.smtpsetting_details = result.response.data;                    //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.error = result.response.message;
        this.overlayRef.detach();
      }
      else {                                                            //if not sucess
        this.error = "something went wrong";
        this.overlayRef.detach();
      }

    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  openLarge(content6) {
    this.error = '';
    this.Addsmtpplan.reset();
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  // convenience getter for easy access to form fields
  get log() {
    return this.Addsmtpplan.controls;
  }         //error logs for create smtp
  get log1() {
    return this.Editsmtpplan.controls;
  }
  addsiteplan() {
    var data = this.Addsmtpplan.value;                  //Data to be passed for add api
    this.loadingAdd = true;
    var url = 'create_smtp_settings/'                                         //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                 //if sucess
        this.showTable = false;
        this.chRef.detectChanges();
        this.getsmtpsetting();
        //reloading the component
        this.Addsmtpplan.reset();
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingAdd = false;
        this.modalService.dismissAll();                                 //to close the modal box

      }
      else if (result.response.response_code == "400") {              //if failure                                               //if not sucess
        this.error = result.response.message;
        this.loadingAdd = false;
      }
      else if (result.response.response_code == "500") {              //if failure                                               //if not sucess
        this.error = result.response.message;
        this.loadingAdd = false;
      }
      else {                                                            //if not sucess
        this.error = "something went wrong";
        this.loadingAdd = false;
      }
    }, (err) => {                                                     //if error
      console.log(err);                                             //prints if it encounters an error
    });

  }
  //Function to store the line item selected to edit
  editsmtpsetting(data) {
    this.edit_data = data;
    console.log(this.edit_data);
    this.Editsmtpplan = new FormGroup({
      "smtp_id": new FormControl(this.edit_data.smtp_id, [Validators.required]),
      "smtp_security_type": new FormControl(this.edit_data.smtp_security_type, [Validators.required]),
      "smtp_hostname": new FormControl(this.edit_data.smtp_hostname, [Validators.required,this.noWhitespace]),
      "smtp_port": new FormControl(this.edit_data.smtp_port, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(4),Validators.minLength(2)]),
      "smtp_username": new FormControl(this.edit_data.smtp_username, [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "smtp_password": new FormControl(this.edit_data.smtp_password, [Validators.required, Validators.minLength(6)]),
      "default": new FormControl(this.edit_data.default, Validators.required),
    });                                         //Stores the data into the array variable
  }
  //Function to call the edit api 
  editsmtp() {
    this.showTable = false;
    this.loadingUpdate = true;
    var url = 'edit_smtp_settings/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.Editsmtpplan.value)
      .subscribe((result) => {
        if (result.response.response_code == "200")                        //if sucess
        {
          this.showTable = false;
          this.chRef.detectChanges();
          this.getsmtpsetting();                                                //reloading the component
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.loadingUpdate = false;
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (result.response.response_code == "400") {              //if failure                                               //if not sucess
          this.error = result.response.message;
          this.loadingUpdate = false;
        }
        else if (result.response.response_code == "500") {              //if failure                                               //if not sucess
          this.error = result.response.message;
          this.loadingUpdate = false;
        }
        else {                                                            //if not sucess
          this.error = "something went wrong";
          this.loadingUpdate = false;
        }
      }, (err) => {                                                        //if error
        console.log(err);                                                 //prints if it encounters an error
      });
  }
  //only number allowed in input field saranya 17/12/2020
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
