// Angular
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { GestureConfig, MatMenuModule, MatProgressSpinnerModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from "@mat-datetimepicker/core";
import {MatDialogModule} from '@angular/material';

// import { AccumulationChartModule } from '@syncfusion/ej2-angular-charts';
import {
	CategoryService, BarSeriesService, ColumnSeriesService, LineSeriesService, LegendService, DataLabelService, MultiLevelLabelService, SelectionService, PieSeriesService, AccumulationLegendService, AccumulationTooltipService, AccumulationAnnotationService,
	AccumulationDataLabelService
} from '@syncfusion/ej2-angular-charts';
import { HttpModule } from '@angular/http';
// Angular in memory
// import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// Perfect Scroll bar
// import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// SVG inline
// import { InlineSVGModule } from 'ng-inline-svg';
// Env
// import { environment } from '../environments/environment';
// Hammer JS
// import 'hammerjs';
// import { IntegralUIModule } from '@lidorsystems/integralui-web/bin/integralui/integralui.module';
// NGX Permissions
import { NgxPermissionsModule } from 'ngx-permissions';
// import { LocalService } from './local.service';
// import { CountriesService } from './countries.service';
import { ToastrModule } from 'ngx-toastr';
// NGRX
import { StoreModule } from '@ngrx/store';
// import { EffectsModule } from '@ngrx/effects';
// import { StoreRouterConnectingModule } from '@ngrx/router-store';
// import { StoreDevtoolsModule } from '@ngrx/store-devtools';
// State
// import { metaReducers, reducers } from './core/reducers';
// Copmponents
import { AppComponent } from './app.component';
// Modules
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { ThemeModule } from "./views/theme/theme.module";
// Partials
import { PartialsModule } from './views/partials/partials.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import {MatStepperModule} from '@angular/material';
// import { MatFormFieldModule,MatInputModule,MatRadioModule} from '@angular/material';
// import {MatTableModule} from '@angular/material/table';
// Layout Services
import {
	// DataTableService,
	// FakeApiService,
	KtDialogService,
	LayoutConfigService,
	LayoutRefService,
	MenuAsideService,
	MenuConfigService,
	MenuHorizontalService,
	PageConfigService,
	// SplashScreenService,
	// SubheaderService
} from './core/_base/layout';
// Auth
import { AuthModule } from './views/pages/auth/auth.module';
import { AuthService } from './core/auth';
// CRUD
import { HttpUtilsService, LayoutUtilsService, TypesUtilsService } from './core/_base/crud';
// Config
// import { LayoutConfig } from './core/_config/layout.config';
// Highlight JS
import { HIGHLIGHT_OPTIONS, HighlightLanguage } from 'ngx-highlightjs';
import * as typescript from 'highlight.js/lib/languages/typescript';
import * as scss from 'highlight.js/lib/languages/scss';
import * as xml from 'highlight.js/lib/languages/xml';
import * as json from 'highlight.js/lib/languages/json';

//import { NewsubscriptionComponent } from './views/pages/newsubscription/newsubscription.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// import { Checklistdatabase } from './checklistdatabase.service';
// import { NgxOrgChartModule } from 'ngx-org-chart';
// import { OrgChartModule } from '@mondal/org-chart';ToastrModule
//Excel download
import { ExcelService } from './excelservice';
// import { ReimbursmentformComponent } from '../app/views/pages/reimbursmentform/reimbursmentform.component';
// import { MaindashboardComponent } from './maindashboard/maindashboard.component';
import { LoaderComponent } from './views/pages/loader/loader.component';

import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { MessagingService } from './message.service';

// import { FeedbackComponent } from './feedback/feedback.component';

// import {CustomerLoginComponent} from '../app/views/pages/auth/customer-login/customer-login.component'

// import { TestraiseticketComponent } from './views/pages/testraiseticket/testraiseticket.component';
// import { FilterPipe } from './filter.pipe';
// import { ProfileComponent } from './profile/profile.component';
// import { LocationComponent } from './location/location.component';
// import { CompletedticketsComponent } from './completedtickets/completedtickets.component';
// import { OngoingticketsComponent } from './ongoingtickets/ongoingtickets.component';
// import { ConfigurationComponent } from './configuration/configuration.component';
// import { NewticketsComponent } from './newtickets/newtickets.component';
// import { RaiseticketComponent } from './views/pages/raiseticket/';

// import { ChartsModule } from 'ng2-charts';DataTablesModule layout config
// 	return () => {
// 		if (appConfig.getConfig() ===SplashScreenService null) {
// 			appConfig.loadConfigs(new LayoutConfig().configs);
// 		}
// 	};
// }

export function hljsLanguages(): HighlightLanguage[] {
	return [
		{ name: 'typescript', func: typescript },
		{ name: 'scss', func: scss },
		{ name: 'xml', func: xml },
		{ name: 'json', func: json }
	];
}

@NgModule({
	declarations: [AppComponent],
	imports: [
		NgMultiSelectDropDownModule.forRoot(),
		BrowserModule,
		//  AccumulationChartModule,
		// ChartsModule,BaseComponent
		BrowserAnimationsModule,
		ReactiveFormsModule,
		BrowserModule,
		CommonModule,
		FormsModule,
		RouterModule,
		AppRoutingModule,
		HttpModule,
		HttpClientModule,
		ThemeModule,
		// environment.isMockEnabled ? HttpClientInMemoryWebApiModule.forRoot(FakeApiService, {
		// 	passThruUnknownUrl: true,
		// 	dataEncapsulation: false
		// }) : [],
		NgxPermissionsModule.forRoot(),
		PartialsModule,
		CoreModule,
		OverlayModule,
		StoreModule.forRoot({}),
		// EffectsModule.forRoot([]),
		// StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
		// StoreDevtoolsModule.instrument(),
		AuthModule.forRoot(),
		// DataTableService,
		//MatTableModule,
		//MatRadioModule,
		//MatMenuModule,
		MatDialogModule,
		MatDatetimepickerModule,
		ToastrModule.forRoot(), // ToastrModule added
		// OrgChartModule
		AngularFireDatabaseModule,
		AngularFireAuthModule,
		AngularFireMessagingModule,
		AngularFireModule.initializeApp(environment.firebaseConfig),
	],
	exports: [],
	providers: [
		AuthService,		
		PieSeriesService,
		AccumulationLegendService,
		// AccumulationTooltipService,
		AccumulationDataLabelService,
		// AccumulationAnnotationService,
		CategoryService,
		BarSeriesService,
		ColumnSeriesService,
		LineSeriesService,
		LegendService,
		DataLabelService,
		MultiLevelLabelService,
		SelectionService,
		LayoutConfigService,
		LayoutRefService,
		KtDialogService,
		// CountriesService,
		// Checklistdatabase,
		NgbActiveModal,
		NgbTabset,
		// SplashScreenService,
		// {
		// 	provide: PERFECT_SCROLLBAR_CONFIG,
		// 	useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		// },
		// {
		// 	provide: HAMMER_GESTURE_CONFIG,
		// 	useClass: GestureConfig
		// },
		// {
		// 	layout config initializer
		// 	provide: APP_INITIALIZER,
		// 	useFactory: initializeLayoutConfig,
		// 	deps: [LayoutConfigService], multi: true
		// },
		{
			provide: HIGHLIGHT_OPTIONS,
			useValue: { languages: hljsLanguages }
		},
		// template services
		// SubheaderService,
		MenuHorizontalService,
		PageConfigService,
		MenuConfigService,
		MenuAsideService,
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService,
		ExcelService,
		MessagingService,
		AsyncPipe
	],
	bootstrap: [AppComponent],
	entryComponents: [LoaderComponent]
})
export class AppModule {
}