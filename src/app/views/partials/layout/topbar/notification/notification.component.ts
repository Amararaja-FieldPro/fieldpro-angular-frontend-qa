// Angular
import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { ajaxservice } from '../../../../../ajaxservice';// Common API service for both get and post

@Component({
	selector: 'kt-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['notification.component.scss']
})
export class NotificationComponent {

	// Show dot on top of the icon
	@Input() dot: string;

	// Show pulse on icon
	@Input() pulse: boolean;

	@Input() pulseLight: boolean;

	// Set icon class name
	@Input() icon: string = 'flaticon2-bell-alarm-symbol';
	@Input() iconType: '' | 'success';

	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;

	// Set bg image path
	@Input() bgImage: string;

	// Set skin color, default to light
	@Input() skin: 'light' | 'dark' = 'light';

	@Input() type: 'brand' | 'success' = 'success';
	employee_code: string;
	notifiy: any;
	ticket_notify: boolean;
	role_id: any;
	/**
	 * Component constructor
	 *
	 * @param sanitizer: DomSanitizer
	 */
	constructor(private chRef: ChangeDetectorRef, private sanitizer: DomSanitizer, private ajax: ajaxservice, private toastr: ToastrService,) {
		this.employee_code = localStorage.getItem('employee_code');
	}
	ngOnInit() {
		this.role_id = localStorage.getItem('role_id')
		console.log(this.role_id)
		if (this.role_id != '7' || this.role_id != 7)
		{
			this.ticket_notify = false;
			// this.getnotify();
		}
	
	}
	getnotify() {
		var url = 'all_notification/?';
		var id = "employee_code"
		var code = this.employee_code;
		this.ajax.getdataparam(url, id, code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {                    //if sucess
				this.notifiy = result.response.data;                    //storing the api response in the array
				this.ticket_notify = true;
				this.chRef.detectChanges();
				console.log(this.notifiy);
			}
			else if (result.response.response_code == "500") {
				this.ticket_notify = false;
				this.toastr.error(result.response.message, 'Error');
			}
			else {
				this.ticket_notify = false;
				this.toastr.error(result.response.message, 'Error');
			}
		}, (err) => {
			this.ticket_notify = false;                                              //if error
			console.log(err);
		});
	}

	notification() {
		this.getnotify();
	}
	backGroundStyle(): string {
		if (!this.bgImage) {
			return 'none';
		}

		return 'url(' + this.bgImage + ')';
	}
}
