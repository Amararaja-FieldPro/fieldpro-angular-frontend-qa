import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsiAnalysisComponent } from './csi-analysis.component';

describe('CsiAnalysisComponent', () => {
  let component: CsiAnalysisComponent;
  let fixture: ComponentFixture<CsiAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsiAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsiAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
