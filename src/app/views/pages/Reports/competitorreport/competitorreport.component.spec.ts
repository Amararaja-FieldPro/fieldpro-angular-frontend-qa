import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitorreportComponent } from './competitorreport.component';

describe('CompetitorreportComponent', () => {
  let component: CompetitorreportComponent;
  let fixture: ComponentFixture<CompetitorreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitorreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitorreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
