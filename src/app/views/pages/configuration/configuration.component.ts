import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';

@Component({
  selector: 'kt-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {
  submitted = false;
  AllowanceGroup: FormGroup;
  TaxGroup: FormGroup;
  CallCategory: FormGroup;
  EditCallCategory: FormGroup;
  companydetails: any;
  taxdetails: any;
  allowancedetails: any;
  locationData:any;
  callcategorydetails: any;
  showTable: boolean = false;
  loadingSub: boolean = false; // btn spinner
  loadingTax: boolean = false;
  loadingCon: boolean = false;
  loadingSubmit: boolean = false;
  dtTrigger: Subject<any> = new Subject();
  currentJustify = 'end';
  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef,
    public toastr: ToastrService,
    private modalService: NgbModal) {


    this.CallCategory = new FormGroup({
      "company_id": new FormControl(''),
      "location_type_id": new FormControl('null', Validators.required),
      "voltage":new FormControl('null', Validators.required),
      "call_category": new FormControl('', Validators.required),
      "mttr": new FormControl('00:00', Validators.required)
    });

    this.AllowanceGroup = new FormGroup({
      "company_id": new FormControl('', Validators.required),
      "two_wheeler_per_km": new FormControl('', Validators.required),
      "four_wheeler_per_km": new FormControl('', Validators.required)
    });
    this.TaxGroup = new FormGroup({
      "company_id": new FormControl('', Validators.required),
      "product_gst": new FormControl('', Validators.required),
      "service_gst": new FormControl('', Validators.required)
    });
  }
  ngOnInit() {
    this.get_companyid()
    this.get_callcategory()
    // this.locationType();
  }
  // -----------------------------------------function for open the modal box for edit call category ------------------------------------------//

  openLarge(add) {
    this.modalService.open(add, {
      size: 'lg',
      windowClass: "center-modalsm",                              //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  // -----------------------------------------function for getters ------------------------------------------//

  get log() {
    return this.AllowanceGroup.controls;
  }
  get log1() {
    return this.TaxGroup.controls;
  }
  get log2() {
    return this.CallCategory.controls;
  }
  get log3() {
    return this.EditCallCategory.controls;
  }
  // -----------------------------------------function for getting the company id from sitesettings ------------------------------------------//
  get_companyid() {
    var url = 'get_site_settings/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.companydetails = result.response.data;                    //storing the api response in the array
        this.get_allowence(this.companydetails[0].company_id)
        this.get_tax(this.companydetails[0].company_id)
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // locationType() {
  //   this.submitted = true;
  //   this.loadingSub = true;
  //   var data={}                          //set the expire date
  //   var url = 'get_location_type/'                                         //api url for update
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     this.locationData = result.response.data;                    //storing the api response in the array
  //   }, (err) => {                                                        //if error
  //     console.log(err);                                             //prints if it encounters an error
  //   });
  // }

  onChangevoltageEdit(voltage)
{
  var data = { "voltage": voltage }
  var url = 'get_voltage_locations/'                                         //api url of remove license
  this.ajax.postdata(url, data).subscribe((result) => {
    if (result.response.response_code == "200" || result.response.response_code == "400") {
      this.locationData = result.response.data;

    }

    else if (result.response.response_code == "500") {
      this.toastr.error(result.response.message, 'Error');        //toastr message for error
    }
    else {
      this.toastr.error(result.response.message, 'Error');        //toastr message for error
    }
  }, (err) => {
    console.log(err);                                             //prints if it encounters an error
  });
}
// onChangevoltageEdit()
// {
  
// }

  // ----------------------------------------function for getting the allowance details  based on company id ------------------------------------------//
  get_allowence(compny_id) {
    var company_id = compny_id;                            //get the company id                                 
    var url = 'get_allowence/?';             // api url for getting the details with using post params
    var id = "company_id";                        //id for API url

    this.ajax.getdataparam(url, id, company_id).subscribe((result) => { // api url for getting the details
      if (result.response.response_code == "200") {                    //if sucess
        this.allowancedetails = result.response.data;                   //storing the api response in the array
        this.chRef.detectChanges();
        this.AllowanceGroup = new FormGroup({
          "company_id": new FormControl(company_id, Validators.required),
          "two_wheeler_per_km": new FormControl(this.allowancedetails.two_wheeler_per_km, Validators.required),
          "four_wheeler_per_km": new FormControl(this.allowancedetails.four_wheeler_per_km, Validators.required)
        });

      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // -----------------------------------------function for update Allowance ------------------------------------------//
  update_allowance() {
    this.submitted = true;
    this.loadingSub = true;
    var data = this.AllowanceGroup.value;                  //Data to be passed for update api
    data["company_id"] = this.companydetails[0].company_id;                           //set the expire date
    var url = 'update_allowence/'                                         //api url for update
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                   //if sucess

        this.ngOnInit();                                            //reloading the component
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingSub = false;

      }
      else if (result.response.response_code == "400") {                         //if failure
        this.toastr.error(result.response.message, 'Error');                         //toastr message for error
        this.loadingSub = false;
      }
      else {                                                           //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingSub = false;
      }
    }, (err) => {                                                        //if error
      console.log(err);                                             //prints if it encounters an error
    });
  }
  // -----------------------------------------function for getting the tax details based on company id ------------------------------------------//
  get_tax(compny_id) {
    var company_id = compny_id;                            //get the company id                                 
    var url = 'get_tax/?';             // api url for getting the details with using post params
    var id = "company_id";                        //id for API url

    this.ajax.getdataparam(url, id, company_id).subscribe((result) => { // api url for getting the details
      if (result.response.response_code == "200") {                    //if sucess
        this.taxdetails = result.response.data;                   //storing the api response in the array
        this.TaxGroup = new FormGroup({
          "company_id": new FormControl(company_id, Validators.required),
          "product_gst": new FormControl(this.taxdetails.product_gst, Validators.required),
          "service_gst": new FormControl(this.taxdetails.service_gst, Validators.required)
        });

      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // -----------------------------------------function for update tax ------------------------------------------//
  update_tax() {
    this.submitted = true;
    this.loadingTax = true;
    var data = this.TaxGroup.value;                  //Data to be passed for update api
    data["company_id"] = this.companydetails[0].company_id;                           //set the company_id
    var url = 'update_tax/'                                         //api url for update
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                   //if sucess

        this.ngOnInit();           //reloading the component
        this.modalService.dismissAll()        //close the modal box                         
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingTax = false;

      }
      else if (result.response.response_code == "400") {                         //if failure
        this.toastr.error(result.response.message, 'Error');                         //toastr message for error
        this.loadingTax = false;
      }
      else {                                                           //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingTax = false;
      }
    }, (err) => {                                                        //if error
      console.log(err);                                             //prints if it encounters an error
    });
  }
  // -----------------------------------------function for add call category ------------------------------------------//
  add_callcategory() {
    this.submitted = true;
    this.loadingCon = true;
    var data = this.CallCategory.value;                  //Data to be passed for add api
    data["company_id"] = this.companydetails[0].company_id;                           //set the expire date
    var url = 'add_call_category/'                                         //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                   //if sucess                                       //reloading the component
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingCon = false;
        this.CallCategory.reset();
        this.showTable = false;
        this.chRef.detectChanges();
        this.get_callcategory();
        // this.locationData = ''

      }
      else if (result.response.response_code == "400") {                         //if failure
        this.toastr.error(result.response.message, 'Error');                         //toastr message for error
        this.loadingCon = false;
      }
      else {                                                           //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingCon = false;
      }
    }, (err) => {                                                        //if error
      console.log(err);                                             //prints if it encounters an error
    });

  }
  // -----------------------------------------function for getting the call category details based on company id ------------------------------------------//

  get_callcategory() {
    var url = 'get_call_category/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.callcategorydetails = result.response.data;                   //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // -----------------------------------------function for show call category details for edit  ------------------------------------------//

  show_callcategory(edit_calldata) {
    this.EditCallCategory = new FormGroup({
      "voltage":new FormControl(edit_calldata.voltage, Validators.required),
      "location_type_id":new FormControl(edit_calldata.location_type.location_type_id, Validators.required),
      "call_category_id": new FormControl(edit_calldata.call_category_id, Validators.required),
      "call_category": new FormControl(edit_calldata.call_category, Validators.required),
      "mttr": new FormControl(edit_calldata.mttr, Validators.required)
    });
    this.onChangevoltageEdit(edit_calldata.voltage)
  }
  // -----------------------------------------function for edit call category  ------------------------------------------//
  edit_callcategory() {
    this.submitted = true;
    this.loadingSubmit = true;
    var data = this.EditCallCategory.value;                  //Data to be passed for edit api   
    var url = 'edit_call_category/'                                         //api url for edit
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                   //if sucess
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.loadingSubmit = false;
        this.modalService.dismissAll();
        this.showTable = false;
        this.chRef.detectChanges();
        this.get_callcategory();
      }
      else if (result.response.response_code == "400") {                         //if failure
        this.toastr.error(result.response.message, 'Error');                         //toastr message for error
        this.loadingSubmit = false;
      }
      else {                                                           //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingSubmit = false;
      }
    }, (err) => {                                                        //if error
      console.log(err);                                             //prints if it encounters an error
    });
  }
  // -----------------------------------------function for delete call category  ------------------------------------------//
  delete_callcategory(callcategory_id) {
    var call_category_id = { "call_category_id": callcategory_id };
    var url = 'delete_call_category/';             // api url for delete warehouse
    var id = "call_category_id";                        //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, call_category_id).subscribe((result) => {
          if (result.response.response_code == "200") {               //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.get_callcategory()                                       //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success      
          }
          else if (result.response.response_code == "400") {                //if failure
            this.toastr.error(result.response.message, 'Error');            //toastr message for error
          }
          else {                                                            //if not sucess
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  fetchNews($event) {

    // console.log($event);

  }
}