import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Router } from '@angular/router';
//pdf view
import { environment } from '../../../../../environments/environment';

declare var require: any
const FileSaver = require('file-saver');

// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
// import { DataTableDirective } from 'angular-datatables';
// import { Subject } from 'rxjs';

@Component({
  selector: 'kt-completedtickets',
  templateUrl: './completedtickets.component.html',
  styleUrls: ['./completedtickets.component.scss']
})
export class CompletedticketsComponent implements OnInit {
  ticket_id: any;
  csi_detials: any;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  loadingSubmit: boolean;
  //url navigations
  asideMenus: any;
  current_url: any;

  // year: number;
  // day: number;
  // month: number;
  // from: NgbDateStruct;

  // Formgroup 
  currentJustify = 'end';
  Feedback: FormGroup;
  ProductLocation: FormGroup;

  // Filter variables
  products: any;
  locations: any;
  product_id: any;
  location_id: any;
  date_format: any;

  //variable to store the data from master
  ticket_details: any;                   //Ticket details view
  technicians_details: any;              //Technician details view
  completed_tickets: any;                //Completed Tickets
  closed_tickets: any;                   //Closed Ticket
  cust_feedbacks = [];                   //Customer feedback array        
  cus_ratings = [];                      //Customer rating array
  // avilable_technicians_details: any;
  // ticket_id: any;
  // edit_tiket_id: any;
  // tickt_id: any;
  // assigned_tckt: any;
  // unassigned_tckt: any;
  // deffered_tckt: any;
  // accepted_tckt: any;
  // EditDiffTicketForm: FormGroup;



  // file_name: FileList;
  // Filename: any;

  //local storage 
  employee_code: any;
  tabvalue: any;                       //Store tab id
  // todayString: string = new Date().toDateString();
  // todayISOString: string = new Date().toISOString();
  // task_counts: any;                   //variable to store the task count for the technician
  // reassign_technician: any;

  //Loader

  //Table
  showTable: boolean = false;
  showTableClose: boolean = false;
  loadingSub: boolean = false; // button spinner by Sowndarya
  pdf: any;
  httplink: string;
  image_or_video: any;
  return_draw: number;

  // model: NgbDateStruct;
  // edit_model: NgbDateStruct;
  // submitted = false;
  // type: any;
  // error: any;
  // ByDefault: boolean = false;
  // @ViewChild(DataTableDirective, { static: false })
  // dtElement: DataTableDirective;
  // dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject<any> = new Subject();

  // ViewTechnicianDetails: FormGroup;
  // ViewTicketDetails: FormGroup;

  settingsObj: any = {
  
    "order": [[1, "asc"]],

    deferRender: true,
    scrollY: 200,
    scrollCollapse: true,
    scroller: true

  }
  table: any;
  // var nums:number[] = [1,2,3,3] NgbDateStruct, NgbCalendar   public _location: Location,private router: Router, private calendar: NgbCalendar, 
  constructor(public ajax: ajaxservice,
    public modalService: NgbModal,
    private toastr: ToastrService,
    private overlay: Overlay, private chRef: ChangeDetectorRef, private router: Router) {
    // this.model = this.calendar.getToday();
    // this.from = this.calendar.getToday();
    //Initialize FormGroup
    this.table = $('#datatables').DataTable();

    //Filter
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required),
      "date_format": new FormControl('', Validators.required)
    })

    //Customer Feedback
    // this.Feedback = new FormGroup({
    //   "ticket_id": new FormControl(""),
    //   "customer_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[a-z A-Z]*$")]),
    //   "customer_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
    //   "email_id": new FormControl(""),
    //   "referrence_number": new FormControl(""),
    //   "rating1": new FormControl('', [Validators.required]),
    //   "rating2": new FormControl('', [Validators.required]),
    //   "rating3": new FormControl('', [Validators.required]),
    //   "rating4": new FormControl('', [Validators.required]),
    //   "rating5": new FormControl('', [Validators.required]),
    //   "rating6": new FormControl('', [Validators.required]),
    //   "rating7": new FormControl('', [Validators.required]),
    //   "rating8": new FormControl('', [Validators.required]),
    //   "rating9": new FormControl('', [Validators.required]),
    //   "rating10": new FormControl('', [Validators.required]),
    //   "rating11": new FormControl('', [Validators.required]),
    //   "remarks": new FormControl('',)
    // })
    this.Feedback = new FormGroup({
      "ticket_id": new FormControl(""),
      "customer_name": new FormControl(""),
      "customer_number": new FormControl(""),
      "email_id": new FormControl(""),
      "referrence_number": new FormControl(""),
      "rating1": new FormControl(0),
      "rating2":new FormControl(0),
      "rating3":new FormControl(0),
      "rating4":new FormControl(0),
      "rating5":new FormControl('',[Validators.required]),
      "rating6":new FormControl('',[Validators.required]),
      "rating7":new FormControl('',[Validators.required]),
      "rating8":new FormControl('',[Validators.required]),
      "rating9":new FormControl('',[Validators.required]),
      "rating12":new FormControl('',[Validators.required]),
      "rating10":new FormControl(0),
      "rating11":new FormControl(0),
      "remarks":new FormControl('',)
    });
    // this.ViewTicketDetails = new FormGroup({
    //   "ticket_id": new FormControl(""),
    //   "customer_name": new FormControl(""),
    //   "contact_number": new FormControl(""),
    //   "location": new FormControl(""),
    //   "product": new FormControl(""),
    //   "subproduct": new FormControl(""),
    //   "call_category": new FormControl(""),
    //   "service_category": new FormControl(""),
    //   "problem": new FormControl(""),
    //   "priority": new FormControl(""),
    //   "work_type": new FormControl(""),
    // })

    // this.ViewTechnicianDetails = new FormGroup({
    //   "technician_id": new FormControl(""),
    //   "email_id": new FormControl(""),
    //   "full_name": new FormControl(""),
    //   "contact_number": new FormControl(""),
    //   "location": new FormControl(""),
    //   "skill_level": new FormControl(""),
    //   "task_count": new FormControl("")
    // })

  }
  get log() {
    return this.Feedback.controls;                       //error logs for create user
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    //   processing: true
    // }
    this.asideMenus = localStorage.getItem('asideMenus');
    console.log(this.asideMenus)
    console.log(typeof (JSON.parse(this.asideMenus)));
    console.log(typeof (this.asideMenus), "type")
    console.log(this.router.url);
    this.current_url = this.router.url;
    // this.exist = this.asideMenus.find(t => t.page === this.current_url);
    const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    console.log(result, "result")
    console.log(result.length, "result")

    // this.exist = this.asideMenus.filter(this.current_url);
    if (result.length <= 0) {
      // alert("u cannpot go")
      localStorage.clear();

      this.router.navigate(["/login"]);
    } else {
      // alert("else")
      this.get_ticket_product_location()
      this.product_id = null
      this.location_id = null
      this.date_format = null
      this.tabvalue = 'tab-selectbyid1'
      this.employee_code = localStorage.getItem('employee_code');
      //customer feedback array
      this.cust_feedbacks = [
        { id: "P1", value: "Excellent" },
        { id: "P2", value: "Satisfied" },
        { id: "P3", value: "Very Good" },
        { id: "P4", value: "Average" },
        { id: "p5", value: "Bad" },
      ];
      //customer rating array
      this.cus_ratings = [
        { id: "1", value: "1" },
        { id: "2", value: "2" },
        { id: "3", value: "3" },
        { id: "4", value: "4" },
        { id: "5", value: "5" },
        { id: "6", value: "6" },
        { id: "7", value: "7" },
        { id: "8", value: "8" },
        { id: "9", value: "9" },
        { id: "10", value: "10" },
      ]
      //Loader
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      this.get_completed_tickets(); //call completed ticket api
    }
  }
  // --------------------------------function for view the modal box------------------------------------------------------//
  openmodal(view_content, image_or_video) {
    this.httplink = environment.image_static_ip;
    this.image_or_video = image_or_video
    this.modalService.open(view_content, {
      size: 'lg',                                                        //specifies the size of the modal box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  customer_feedback_modal(modal, item) {
    this.Feedback.reset();
    //set the value to the customerfeedback formgroup
    // console.log(btoa(item.ticket_id))
    var ticket = btoa(item.ticket_id);

    var data = { "ticket_id": ticket }
    var url = 'get_csi_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.csi_detials = result.response.data;              //storing the api response in the array
        this.Feedback.patchValue({
          "customer_name": this.csi_detials.customer_name,
          "email_id": this.csi_detials.email_id,
          "referrence_number": this.csi_detials.sap_reference_number,
          "customer_number": this.csi_detials.contact_number,
          "ticket_id": this.csi_detials.ticket_id,
        })
        console.log(this.csi_detials, "this.csi_detials")
        console.log(this.Feedback.value)
        // this.overlayRef.detach();
        this.modalService.open(modal, {
          size: 'lg',
          // windowClass: "center-modal",
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
    //open modal box

  }

  // ---------------------------------function for getters----------------------------------------------------------------//
  get log1() {
    return this.ProductLocation.controls;                       //error logs for productlocation 
  }
  //-----------------------------------function for getting Ticket and Technician details for view Starts---------------------------------------------//

  // get log2() {
  //   return this.ViewTicketDetails.controls;                       //error logs for Ticket details
  // }
  // get log3() {
  //   return this.ViewTechnicianDetails.controls;                       //error logs for Ticket details
  // }
  // get log3()
  // {
  //   return this.EditDiffTicketForm.controls;
  // }
  //----------------------------------------- Functions for Ticket and Technician details  view Starts------------------------------------//

  // Ticket details based on TicketId 
  get_ticket_details(tickt_id, view_tcktdetails) {
    var ticket_id = tickt_id; // country: number                                    
    var url = 'get_ticket_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details = result.response.data;
        this.modalService.open(view_tcktdetails, {
          size: 'lg',
          // windowClass: "center-modalsm",
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      //prints if it encounters an error
    });
  }

  // Technician details based on TechnicianId 
  get_technician_details(ticket_id, technician_id, view_techniciandetails) {
    var ticket_id = ticket_id
    var technician_id = technician_id
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;

        this.modalService.open(view_techniciandetails, {
          size: 'lg',
          windowClass: "center-modalsm",
          backdrop: 'static',              //popup outside click by Sowndarya
          keyboard: false
        });
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      //prints if it encounters an error
    });
  }
  //----------------------------------------- Functions for Ticket and Technician details view Ends------------------------------------//


  //----------------------------------------- Function for Filter Starts-------------------------------------//
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      //prints if it encounters an error
    });
  }
  //onchange functoin for getting the product id
  // onChangeProduct(product_id) {
  //   this.product_id = product_id
  //   this.location_id = null;
  //   this.showTable = false;
  //   this.get_completed_tickets()
  // }

  filter_tickets() {
    this.date_format = this.ProductLocation.value.date_format;
    this.product_id = this.ProductLocation.value.product_id;
    this.location_id = this.ProductLocation.value.location_id;
    if (this.date_format == "") {
      this.date_format = null;
    }
    else {
      this.date_format = this.ProductLocation.value.date_format;
    }
    if (this.location_id == "") {
      this.location_id = null;
    }
    else {
      this.location_id = this.ProductLocation.value.location_id;
    }
    if (this.product_id == "") {
      this.product_id = null;
    }
    else {
      this.product_id = this.ProductLocation.value.product_id;
    }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTable = false;
      this.get_completed_tickets();                //call completed tickets api
    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableClose = false;
      this.get_closed_tickets();                   //call closed tickets api
    }

  }
  //----------------------------------------- Function for Filter Starts-------------------------------------//

  //------------------------------------------ Table View Starts----------------------------------------//
  // Completed ticket based on the product, location and date 
  // get_completed_tickets() {
  //   this.overlayRef.attach(this.LoaderComponentPortal); //attach loader
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, "date": this.date_format }
  //   var url = 'completed_tickets/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.completed_tickets = result.response.data;              //storing the api response in the array
  //       this.showTable = true;                            //show datas if after call API
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();
  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTable = true;                                                       //if not sucess
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error("Something went wrong");
  //     }
  //   }, (err) => {
  //     this.overlayRef.detach();
  //     //prints if it encounters an error
  //   });
  // }

  get_completed_tickets() {
    this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.completed_tickets = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    console.log(draw,"11111")
    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
  //  console.log(search_value)
  var search_value = ""
   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      
    var url = "completed_tickets/"
   //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        this.completed_tickets = result.response.data;              //storing the api response in the array             
        // this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {
          
          "deferLoading": result.response.recordsTotal,
      
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
       
          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            console.log(draw,"draw22222")
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            console.log(search_value,"search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']   
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      

            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming = "completed_tickets/" 
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.completed_tickets = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw     
              console.log(this.return_draw,"33333")     
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();
            
            });

          },
          "columnDefs": [           
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Technician ID", "targets": 2, "searchable": true },
            { "name": "Product Category", "targets": 3, "searchable": true },
            { "name": "Sub Product Category", "targets": 4, "searchable": true },
            { "name": "Town", "targets": 5, "searchable": true },
            { "name": "Completed Time", "targets": 6, "searchable": true },
            { "name": "Resolution Image", "targets": 7, "searchable": false },
            { "name": "SETR", "targets": 8, "searchable": false },
          ],
        }

        $('#datatables').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#datatables').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.completed_tickets =[];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }


  //-------------------------------------------Assigned Tickets functions starts------------------------------------------//
  // Closed ticket based on the product, location and date 
  // get_closed_tickets() {
  //   this.overlayRef.attach(this.LoaderComponentPortal); //attach loader
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, "date": this.date_format }
  //   var url = 'closed_tickets/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.closed_tickets = result.response.data;              //storing the api response in the array
  //       this.showTableClose = true;
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTableClose = true;
  //       this.overlayRef.detach();
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.overlayRef.detach();
  //       this.toastr.error("Something went wrong", 'Error')
  //     }
  //   }, (err) => {
  //     //prints if it encounters an error
  //   });
  // }


  get_closed_tickets() {
    this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.closed_tickets = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    console.log(draw,"11111")

    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
  //  console.log(search_value)
  var search_value = ""

   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      
    var url = 'closed_tickets/';  
   //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        this.closed_tickets = result.response.data;              //storing the api response in the array
        // this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {
          
          "deferLoading": result.response.recordsTotal,
      
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
       
          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            console.log(draw,"222222")

            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            console.log(search_value,"search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']   
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      

            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming ='closed_tickets/';  
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.closed_tickets = result.response.data;              //storing the api response in the array
              this.return_draw = result.response.draw   
              console.log(this.return_draw ,"3333")
       
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();
            
            });

          },
          "columnDefs": [           
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Technician ID", "targets": 2, "searchable": true },
            { "name": "Product Category", "targets": 3, "searchable": true },
            { "name": "Sub Product Category", "targets": 4, "searchable": true },
            { "name": "Town", "targets": 5, "searchable": true },
            { "name": "Assigned Time", "targets": 6, "searchable": true },
            { "name": "Closed Time", "targets": 7, "searchable": true },
            { "name": "Customer Rating", "targets": 8, "searchable": true },
            { "name": "Resolution Image", "targets": 9, "searchable": false },
            { "name": "SETR", "targets": 10, "searchable": false },
            { "name": "Service Report", "targets": 11, "searchable": false },
          ],
        }

        $('#closed_tickets').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#closed_tickets').DataTable(

            this.settingsObj
          );
        }, 5);
        console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.closed_tickets =[];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  //------------------------------------------ Table View Ends----------------------------------------//



  customer_feedback() {
    console.log(this.Feedback.value);
    var data = {
      // "feedback_details": [{ 
      "q1":+this.Feedback.value.rating5,
      "q2":+this.Feedback.value.rating6,
      "q3":+this.Feedback.value.rating7,
      "q4":+this.Feedback.value.rating8,
      "q5":+this.Feedback.value.rating9,
      "q6":+this.Feedback.value.rating12,
      // }],
     
      "cust_comments": this.Feedback.value.remarks,
      "ticket_id":this.Feedback.value.ticket_id,
      "flag":1
    }
    console.log(data)
    this.loadingSubmit = true;
    // data["ticket_id"] = this.Feedback.value.ticket_id
    console.log(data)
    var url = 'update_feedback/';
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200")                           //if sucess
        {
          this.loadingSubmit = false;
          this.get_completed_tickets();
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.showTable = false;
          this.chRef.detectChanges();
          
          this.modalService.dismissAll();
          //to close the modal box
        }
        else if (result.response.response_code == "400") {
          this.loadingSubmit = false;                  //if failure
          this.toastr.error(result.response.message, 'Error');            //toastr message for error
        }
        else if (result.response.response_code == "500") {
          this.loadingSubmit = false;
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else {
          this.loadingSubmit = false;                                                                 //if not sucess
          this.toastr.error("Something went wrong", 'Error');        //toastr message for error
        }
      }, (err) => {
        this.loadingSubmit = false;                                                          //if error
        console.log(err);   //prints if it encounters an error
      });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  //Pdf download
  downloadPdf(pdfName: string, pdfUrl: string) {
    var pdf = environment.image_static_ip + pdfUrl
    // var pdf = "https://" + pdfUrl
    FileSaver.saveAs(pdfUrl, pdfName);
  }
  downloadPdfs(item) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    var id = "ticket_id";
    var url = "setr_pdf/?";
    var value = item;
    this.ajax.getPdfDocument(url, id, value).subscribe((response) => {
      console.log(response, "response")
      let file = response.url
      console.log(response.url, "fileURL")
      FileSaver.saveAs(file, value);
      this.overlayRef.detach();

    }, (err) => {     
       this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });
  }
  findtab(evt: any) {
    this.tabvalue = evt.nextId;
    this.product_id = null;
    this.location_id = null;
    this.date_format = null;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['location_id'].setValue('');
    this.ProductLocation.controls['date_format'].setValue('');
    if (evt.nextId == 'tab-selectbyid1') {
      this.showTable = false;
      this.get_completed_tickets();
      // this.overlayRef.attach(this.LoaderComponentPortal);
    }
    if (evt.nextId == 'tab-selectbyid2') {
      this.showTableClose = false;
      this.get_closed_tickets();
      // this.overlayRef.attach(this.LoaderComponentPortal);
    }
  }
  service_report_pdf(ticket_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    console.log(ticket_id,"ticket_id")
    var id = "ticket_id";
    var url = "generate_pdf/?";
    var value = btoa(ticket_id); //base 64 encode the ticket id
    console.log(value,"value")
    this.ajax.getPdfDocument(url, id,value).subscribe((response) => {
      console.log(response, "response")
      let file = response.url
      console.log(response.url, "fileURL")   ;  
      FileSaver.saveAs(file, ticket_id);
      this.overlayRef.detach();

    },(err) => {
      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });  
  }
}
