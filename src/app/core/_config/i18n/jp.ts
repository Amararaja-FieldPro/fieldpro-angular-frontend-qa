// Japan
export const locale = {
	lang: 'jp',
	data: {
		TRANSLATOR: {
			SELECT: 'あなたが使う言語を選んでください',
		},
		MENU: {
			NEW: '新しい',
			ACTIONS: '行動',
			CREATE_POST: '新しい投稿を作成',
			PAGES: 'ページ',
			FEATURES: '特徴',
			APPS: 'アプリ',
			DASHBOARD: 'ダッシュボード',
			NEW_SUBSCRIPTION:'新しいサブスクリプション',
			MANAGE_SUBSCRIPTION:'サブスクリプションの管理',
			PRODUCT_MASTER:'製品マスター',
			SLA_MAPPING:'Slaマッピング',
			BILLING_DETAILS:'支払明細',
			PACKAGE_PLAN:'パッケージプラン',
			PACKAGE_MANAGEMENT:'パッケージ管理',
			PACKAGE_CONFIGURATION:'	パッケージ構成',
			WAREHOUSE:'	倉庫マスター',
			SPARE:'予備マスター',
			SERVICE:'',
			AMC:'',
		},
		AUTH: {
			GENERAL: {
				OR: 'または',
				SUBMIT_BUTTON: '提出する',
				NO_ACCOUNT: 'アカウントを持っていない？',
				SIGNUP_BUTTON: 'サインアップ',
				FORGOT_BUTTON: 'パスワードをお忘れですか',
				BACK_BUTTON: 'バック',
				PRIVACY: 'プライバシー',
				LEGAL: '法的',
				CONTACT: '接触',
			},
			LOGIN: {
				TITLE: 'Create Account',
				BUTTON: 'Sign In',
			},
			FORGOT: {
				TITLE: 'パスワードをお忘れですか ？',
				DESC: 'Enter your email to reset your password',
				SUCCESS: 'Your account has been successfully reset.'
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'Your account has been successfuly registered.'
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Confirm Password',
				USERNAME: 'ユーザー名'
			},
			VALIDATION: {
				INVALID: '{{name}} is not valid',
				REQUIRED: '{{name}} is required',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: 'The login detail is incorrect',
				REQUIRED_FIELD: 'Required field',
				VALID_ADDRESS:'有効なメールアドレスを入力してください',
				MIN_LENGTH_FIELD: 'Minimum field length:',
				MAX_LENGTH_FIELD: 'Maximum field length:',
				INVALID_FIELD: 'Field is not valid',
			}
		},
		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Selected records count: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'eCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Customers',
				CUSTOMERS_LIST: 'Customers list',
				NEW_CUSTOMER: 'New Customer',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Customer Delete',
					DESCRIPTION: 'Are you sure to permanently delete this customer?',
					WAIT_DESCRIPTION: 'Customer is deleting...',
					MESSAGE: 'Customer has been deleted'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Customers Delete',
					DESCRIPTION: 'Are you sure to permanently delete selected customers?',
					WAIT_DESCRIPTION: 'Customers are deleting...',
					MESSAGE: 'Selected customers have been deleted'
				},
				UPDATE_STATUS: {
					TITLE: 'Status has been updated for selected customers',
					MESSAGE: 'Selected customers status have successfully been updated'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Customer has been updated',
					ADD_MESSAGE: 'Customer has been created'
				}
			}
		},
		PRODUCT_MASTER:{
			PARA:'洗濯機は、汚れた衣服を洗う機械です。...この樽は水で満たされ、非常に素早く回転して、衣服から汚れを除去します。ほとんどの洗濯機は、洗剤（液体または粉末）を洗濯機に入れることができるように作られています。これらは衣服をきれいにするのに役立ちます。',
			PARA1:'洗濯機は、汚れた衣服を洗う機械です。...この樽は水で満たされ、非常に素早く回転して、衣服から汚れを除去します。ほとんどの洗濯機は、洗剤（液体または粉末）を洗濯機に入れることができるように作られています。これらは衣服をきれいにするのに役立ちます。',
		},
		NEW_SUBSCRIPTION:{
			COMPANY_INFO:'会社情報 ',
			ADDRESS:'住所',
			CONFIRM_DETAILS:'詳細を確認 ',
			ENTER:'会社の詳細を入力してください',
			ADD:'住所',
			DET:'詳細を確認',
				}
	}
};