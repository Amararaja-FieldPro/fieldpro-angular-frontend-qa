import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferspareComponent } from './transferspare.component';

describe('TransferspareComponent', () => {
  let component: TransferspareComponent;
  let fixture: ComponentFixture<TransferspareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferspareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferspareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
