import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
import { ExcelService } from '../../../../excelservice';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';

@Component({
	selector: 'kt-call-report',
	templateUrl: './call-report.component.html',
	styleUrls: ['./call-report.component.scss']
})
export class CallReportComponent implements OnInit {
	ShowTable:boolean=false;

	service_group: any;
	techId: any;
	assesment_data: any;
	service_id: any;
	call_report: any;
	employee_code: any;
	filterCall: FormGroup;    //form group 
	technician_details: any;
	loadingGo: boolean = false;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	total: any;	
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;

	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private excelservice: ExcelService,private overlay: Overlay,private chRef: ChangeDetectorRef) {
		this.filterCall = new FormGroup({
			"service_id": new FormControl(''),

		})
	}

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
			  });
			  this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		//   this.overlayRef.attach(this.LoaderComponentPortal);
	  
		var data = {
			"service_id": null,
		}
		this.getCall(data);
		this.getServices();
	}
	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.call_report, 'call_report');
	}
	getServices() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'get_servicegroup/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.service_group = result.response.data;
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}

	filterService() {
		this.loadingGo = true; 
		// alert("AFadf");
		this.service_id = this.filterCall.value.service_id;
		if (this.service_id == "") {
			this.service_id = null;
		}
		else {
			this.service_id = this.filterCall.value.service_id;
		}
		var data = {
			'service_id': this.service_id,
		}
		this.ShowTable=false;
		this.getCall(data);
	}

	getCall(data) {
		this.overlayRef.attach(this.LoaderComponentPortal);

		// console.log(data);
	
		
		var url = 'call_report/?';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.loadingGo = false;
				this.call_report = result.response.data;
				this.total=result.response.total;
				this.ShowTable=true;
				this.overlayRef.detach();  //detaching the loader after response
				this.chRef.detectChanges();
				this.dtTrigger.next();
			
				
			}
			else if (result.response.response_code == "500") {
				this.ShowTable=true;
				this.overlayRef.detach();
				this.loadingGo = false;
			}
			
			else {
				this.toastr.error( result.response.message, 'Error');
				this.loadingGo = false;
			}
		}, (err) => {
			this.loadingGo = false;
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal",                                  //specifies the size of the dialog box
			// backdrop : 'static', 
			// keyboard : false,
		});
	}

}
