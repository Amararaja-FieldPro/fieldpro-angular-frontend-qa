import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
// import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
@Component({
	selector: 'kt-transferspare',
	templateUrl: './transferspare.component.html',
	styleUrls: ['./transferspare.component.scss']
})
export class TransferspareComponent implements OnInit {
	employee_code: any;
	spare_transfer: any;
	spare_transfer_id: any;
	spare_required_details: any;
	spares: any;
	technician_data: any;
	techId: any;
	accepted_spare_transfer: any;
	rejected_spare_transfer: any;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	dataTable: any;
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTable: boolean = false;
	showTableModal: boolean = false;
	showTableAccept: boolean = false;
	showTableReject: boolean = false;
	currentJustify = 'end';
	@ViewChild('dataTable', { static: true }) table;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private chRef: ChangeDetectorRef, private overlay: Overlay, private router: Router, private toastr: ToastrService) { }
	ngOnInit() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10
		}
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.getSpareTransfer();

	}
	getSpareTransfer() {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'spare_transfer/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_transfer = result.response.data;
				this.showTable = true;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				this.overlayRef.detach();
			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error("Something Went Wrong", "Error")
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getAcceptedSpareTransfer() {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'spare_transfer_approve_details/?';                                  // api url for getting the Spare transfer approve details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.accepted_spare_transfer = result.response.data;
				this.showTableAccept = true;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				this.overlayRef.detach();
			}
			else if (result.response.response_code == "500") {
				this.showTableAccept = true;
				this.overlayRef.detach();
			}
			else {
				this.toastr.error("Something Went Wrong", "Error")
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getRejectedSpareTransfer() {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'spare_transfer_reject_details/?';                                  // api url for getting the Spare reject details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.rejected_spare_transfer = result.response.data;
				this.showTableReject = true;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				this.overlayRef.detach();
			}
			else if (result.response.response_code == "500") {
				this.showTableReject = true;
				this.overlayRef.detach();
			}
			else {
				this.toastr.error("Something Went Wrong", "Error")
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	spareApprove(spare_transfer_id) {
		var url = 'spare_transfer_approve/'; //API url for spare transfer approve
		var data = {//params for API
			"spare_transfer_id": spare_transfer_id
		}
		Swal.fire({ //sweet alert for accptance
			title: 'Are you sure?',
			text: "You want to accept this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Accept',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {                          //if sucess
						this.ngOnInit();                                                //reloading the component
						this.toastr.success(result.response.message, 'Success');        //toastr message for success

					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	spareReject(spare_transfer_id) {
		var url = 'spare_transfer_reject/';
		var data = {
			"spare_transfer_id": spare_transfer_id
		}
		Swal.fire({//swwet alert for reject or cancel
			title: 'Are you sure?',
			text: "You want to reject this spare !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Reject',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {                          //if sucess
						this.ngOnInit();                                                //reloading the component
						this.toastr.success(result.response.message, 'Success');        //toastr message for success
					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
		console.log(evt.nextId);
		if (evt.nextId == 'tab-selectbyid2') {
			this.showTableAccept = false;
			this.getAcceptedSpareTransfer();
		}
		else if (evt.nextId == 'tab-selectbyid3') {
			this.showTableReject = false;
			this.getRejectedSpareTransfer();
		}
		else if (evt.nextId == 'tab-selectbyid1') {
			this.showTable = false;
			this.getSpareTransfer();
		}
	}
	spareDetails(data, spare_required) { //get Spare Details
		this.spares = data;
		var tran_id = this.spares.spare_trans_id;
		var transfer_id = "transfer_id";
		var url = 'get_spare_transfer_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, transfer_id, tran_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_required_details = result.response.data; //bind result in this variable
				this.showTableModal = true;
				this.modalService.open(spare_required, {
					size: 'lg',
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	techDetails(techId, tech_details) {
		this.techId = techId;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_data = result.response.data; //bind results in this variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modalsm"                                  //specifies the size of the dialog box
		});
	}
	openModal(add, row) { // Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal"                                  //specifies the size of the dialog box
		});
	}
}
