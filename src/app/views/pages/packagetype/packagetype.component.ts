import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import {  Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'kt-packagetype',
  templateUrl: './packagetype.component.html',
  styleUrls: ['./packagetype.component.scss']
})
export class PackagetypeComponent implements OnInit {
  package_details: any;                                                 // array to store the package details from get method api
  edit_data: any;                                                       // array to store the line item selected to edit 
  myInput:string;                                                       // ngmodel value of the textbox package name
  Edit_package_form: FormGroup;
  dtOptions: DataTables.Settings = {};
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router, private toastr: ToastrService, vcr: ViewContainerRef) {
    //Defines the required services to be used 
  }
  ngOnInit() {
    this.myInput = "";    
    this.getdata();                                                     // calling function on page load
  
  }
  //--------------------------------------------------------------------------------------------------//

  //To Get the package details from API
  getdata() {
    var url = 'get_package_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {            
      if (result.response.response_code == "200") 
      {           
        this.package_details = result.response.data;                    //storing the api response in the array
                                                     //reloading the component
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

   //--------------------------------------------------------------------------------------------------//
  // Function to open the modal box 
  openLarge(content6) {
    this.modalService.open(content6, {
      size: 'lg'                                                        //specifies the size of the modal box
    });
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to store the line item selected to edit
  editpackage(data) {
    this.edit_data = data;    
    this.Edit_package_form = new FormGroup({
      "package_id": new FormControl(this.edit_data.package_id, Validators.required),
      "package_name": new FormControl(this.edit_data.package_name,Validators.required),
      "status": new FormControl(this.edit_data.status, Validators.required)
    });                                         //Stores the data into the array variable
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to call the edit api 
  editform(edit_data) {    
    var url = 'edit_package/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.Edit_package_form.value)
    .subscribe((result) => 
    {
      if (result.response.response_code == "200") 
      {
        this.ngOnInit();                                                //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
        this.modalService.dismissAll();                                 //to close the modal box
      }
     else if (result.response.response_code == "400") {
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      }
      else{
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

   //--------------------------------------------------------------------------------------------------//

  //Function to call the add api 
  addpackage(frmdata) {
    if(frmdata == '')                                                   //Check if the package type is empty
    { 
      this.toastr.error("Enter Package Name", 'Error');                //If empty display the toast error message 
    }
    else
    {
      var data = { package_name: frmdata, status: 1 }                  //Data to be passed for add api
      var url = 'add_package/'                                         //api url of add
      this.ajax.postdata(url, data).subscribe((result) =>         
      {     
        if (result.response.response_code == "200") {
       
          this.ngOnInit();                                            //reloading the component
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          // this.ngOnInit();                                            //reloading the component
        
        }
      else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
        else{
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
}
