import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicegroupComponent } from './servicegroup.component';

describe('ServicegroupComponent', () => {
  let component: ServicegroupComponent;
  let fixture: ComponentFixture<ServicegroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicegroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicegroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
