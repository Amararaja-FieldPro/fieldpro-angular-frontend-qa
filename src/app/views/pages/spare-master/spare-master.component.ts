import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { NgbModal, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import * as _ from 'lodash';
import { Http, RequestOptions, Headers } from '@angular/http';
@Component({
  selector: 'kt-spare-master',
  templateUrl: './spare-master.component.html',
  styleUrls: ['./spare-master.component.scss']
})
export class SpareMasterComponent implements OnInit {

  message: string;
  disabled: boolean = false;
  exp_error_add: any;
  exp_error: any;
  spare_image: any;
  edit_expireddate: any;
  expiry_date: any;
  year: number;
  day: number;
  month: number;
  file_name: any;                        //To use bulk upload
  sparemasterdetails: any;                       //spare details from get API
  product_details: any;                       //products from gt API
  warehousedetails: any;                              //warehouse from gt API
  subproduct_details: any                                //subproducts from gt API
  Addsparemaster: FormGroup;              //form group for create sparemaster
  Editsparemaster: FormGroup;                                //form group for update sparemaster
  spare: any;
  type: any;                                       //get data for delete API
  edit_spare: any;                                        //get data foe edit API
  submitted = false;
  showTable: boolean = false;                  //loader show and hide
  overlayRef: OverlayRef;                       //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;  //loader component  
  load: any;
  filename: any;
  Filename: any;
  ByDefault: boolean = false;
  error: any;
  loading: boolean = false;
  loadingAdd: boolean = false;
  loadingBulk: boolean = false;
  loadingEdit: boolean = false;
  errors: any;
  from: any;
  to: any;
  maxChars: number;
  chars: number;
  role: any;
  bulk: any;
  today: any;
  buttonVisibleEdit: boolean = true;
  buttonAdd: boolean = false;
  buttonEdit: boolean = false;
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef, private calendar: NgbCalendar) {
    this.from = this.calendar.getToday();
    this.to = this.calendar.getToday();
    //create subscription form group
    this.Addsparemaster = new FormGroup({
      "spare_code": new FormControl("", [Validators.required, this.noWhitespace]),
      "spare_model": new FormControl("", [Validators.required, this.noWhitespace]),
      "spare_name": new FormControl("", [Validators.required, this.noWhitespace, Validators.maxLength(48)]),
      "product_id": new FormControl("", [Validators.required]),
      "subproduct_id": new FormControl("", [Validators.required]),
    });
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  ngOnInit() {
    this.maxChars = 1024;
    this.role = '';
    this.chars = 0;
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getsparemasterdetails();
    this.getproductmasterdata();
  }
  //after submit the file for product
  bulkfilesubmit(bulkupload) {
    this.bulk = [];
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined || this.file_name == '') {
      // this.toastr.error("Please select file", 'Error');
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {                   //if file is not empty
      if (validExts[0] != this.type) {
        // this.toastr.error("Please select correct extension", 'Error');
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) {
        this.loadingBulk = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        let options = new RequestOptions({ headers: headers });
        var gh = file;
        var method = "post";                                    //post method 
        var url = 'sparemaster_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
           
            if (data['response'].fail.length > 0) {                                    //failure response 
              //if employee code or employee id is error to put this
              for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );
                });

                this.bulk = a;
                this.loadingBulk = false;
                // this.file_name = '';
                // this.ByDefault = false;
              }
            }
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {                         //sucess response
              for (i = 0; i < data['success'].length; i++) {
                // alert(data['success'][i])
                // this.bulk = '';
                var a = [];
                data['success'].forEach(value => {
                  a.push(value.success_message
                  );
                });
                this.bulk = a;
                this.loadingBulk = false;
                // this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                this.showTable = false;
                this.chRef.detectChanges();
                this.getsparemasterdetails();
                // this.file_name = '';
                // this.ByDefault = false;

              }
            }
            this.loadingBulk = false;
          }

          //if other errors
          else if (data['response'].response_code == "500") {
            this.bulk = [];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = data['response'].message
          }
          else {
            this.bulk = [];
            // this.file_name = '';
            // this.ByDefault = false;
            this.loadingBulk = false;
            this.error = "Something Went Wrong";                                                                  //if not sucess
          }
        },

          (err) => {
            this.bulk = [];
            // this.file_name = '';
            // this.ByDefault = false;                                                                 //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }
    // this.bulk = [];
    // var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    // if (this.file_name == undefined || this.file_name == '') {
    //   // this.toastr.error("Please select file", 'Error');
    //   this.error = "Please select file";
    // }
    // else if (this.file_name.length > 0) {                   //if file is not empty
    //   if (validExts[0] != this.type) {
    //     // this.toastr.error("Please select correct extension", 'Error');
    //     this.error = "Please select correct extension";
    //   }
    //   else if (validExts = this.type) {
    //     this.loadingBulk = true;
    //     let file: File = this.file_name[0];
    //     const formData: FormData = new FormData();         //convert the formdata
    //     formData.append('excel_file', file);                     //append the name of excel file
    //     let currentUser = localStorage.getItem('LoggedInUser');
    //     let headers = new Headers();
    //     let options = new RequestOptions({ headers: headers });
    //     var gh = file;
    //     var method = "post";                                    //post method 
    //     var url = 'sparemaster_bulk_upload/'                         //post url
    //     var i: number;                                             //i is the data
    //     this.ajax.ajaxpost(formData, method, url).subscribe(data => {
    //       if (data['response_code'] == "200") {
    //         this.error = '';
    //         this.loadingBulk = false;
    //         if (data['response'].fail.length > 0) {                                    //failure response 
    //           //if employee code or employee id is error to put this
    //           for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
    //             var a = [];
    //             data['response'].fail.forEach(value => {
    //               a.push(value.error_message
    //               );
    //             });

    //             this.bulk = a;
    //             // this.file_name = '';
    //             // this.ByDefault = false;
    //           }
    //         }
    //         //success response
    //         //if newly add product for excisting employee code or employee id
    //         if (data['success'].length > 0) {                         //sucess response
    //           for (i = 0; i < data['success'].length; i++) {
    //             this.bulk = '';
    //             this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
    //             this.showTable = false;
    //             this.chRef.detectChanges();
    //             this.getsparemasterdetails();
    //             // this.file_name = '';
    //             // this.ByDefault = false;

    //           }
    //         }
    //       }

    //       //if other errors
    //       else if (data['response'].response_code == "500") {
    //         this.bulk = [];
    //         this.file_name = '';
    //         // this.ByDefault = false;
    //         this.loadingBulk = false;
    //         this.error = data['response'].message
    //       }
    //       else {
    //         this.bulk = [];
    //         this.file_name = '';
    //         // this.ByDefault = false;
    //         this.loadingBulk = false;
    //         this.error = "Something Went Wrong";                                                                  //if not sucess
    //       }
    //     },

    //       (err) => {
    //         this.bulk = [];
    //         this.file_name = '';
    //         // this.ByDefault = false;                                                                 //if error
    //         console.log(err);                                                 //prints if it encounters an error
    //       }
    //     );
    //   }
    // }

  };
  // -----------------------------------------------------------------------------------------------------------//
  getsparemasterdetails() {
    this.errors = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_spare_master/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200")                             //if sucess
      {
        this.sparemasterdetails = result.response.data;                    //storing the api response in the array
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.errors = result.response.message;
      }
      else {
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.errors = result.response.message;
      }
    }, (err) => {                                                        //if error
      console.log(err);
      this.overlayRef.detach();                                       //prints if it encounters an error
    });
  }


  getproductmasterdata() {
    this.errors = '';
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {                       //if sucess
        this.product_details = result.response.data;                    //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {                                                          //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // -------------------------------------------------------------------------------------------------------------------------------------------------//
  // Function for modal box with spare image

  // --------------------------------------------------------------------------------------------------------------------------------------------//
  // -------------------------------------------------------------------------------------------------------------------------------------------------//
  // Function for modal box 
  openLarge(content6) {
    this.ByDefault = false;
    this.errors = '';
    this.error = '';
    this.bulk = [];
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.message = '';
    this.buttonAdd = false;
    this.buttonEdit = true;
    this.load = "";
    this.from = this.calendar.getToday();
    this.to = this.calendar.getToday();
    this.exp_error_add = "";
    this.errors = '';
    this.Addsparemaster.reset();
    // this.Filename = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false, 
    });
  }
  openAdd(content6) {
    this.errors = '';
    this.modalService.open(content6, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      windowClass: "center-modalheight"                                    // modal popup for resizing the popup                                                //specifies the size of the modal box                                                //specifies the size of the modal box
    });
  }
  openBulk(content6) {
    this.error = "";
    this.loadingBulk = false;
    this.disabled = false;
    this.bulk = '';
    this.errors = '';
    this.file_name = undefined;
    this.Filename = ''
    this.modalService.open(content6, {
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      windowClass: "center-modalsm"                                    // modal popup for resizing the popup                                                //specifies the size of the modal box                                                //specifies the size of the modal box
    });
  }
  // --------------------------------------------------------------------------------------------------------------------------------------------//

  // ---------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // convenience getter for easy access to form fields
  get log() {
    return this.Editsparemaster.controls;          //error controls for create spare
  }
  get add() {
    return this.Addsparemaster.controls;          //error controls for create spare
  }
  //Add sparemaster using API 
  addspare() {
    this.errors = '';
    this.loadingAdd = true //adding the spinner in submit button
    //date which is used in India 
    var data = this.Addsparemaster.value;                  //Data to be passed for add api

    if (this.load == '') {

      this.exp_error_add = "";
      var url = 'add_spare_master/';                                       //api url for add
      console.log(data);
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                   //if sucess
          this.loadingAdd = false //dismissing  the spinner in submit button
          this.showTable = false;
          this.chRef.detectChanges();                                         //reloading the component
          this.getsparemasterdetails();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();
          this.errors = "";
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
          this.loadingAdd = false //dismissing  the spinner in submit button
          this.errors = result.response.message;                        //toastr message for error
        }
        else {                                                           //if not sucess
          this.loadingAdd = false //dismissing  the spinner in submit button
          this.errors = result.response.message;     //toastr message for error
        }
      }, (err) => {                                                        //if error
        console.log(err);                                             //prints if it encounters an error
      });
    }
    else {
      this.loadingAdd = false
    }
  }
  expValid(data) {
    console.log(data);
  }
  purValid(data) {
    console.log(data)
  }


  //function for bulk upload in sparemaster
  fileUpload(event) {
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    this.ByDefault = true;
  };

  //after submit the file for sparemaster
  filesubmit(bulkupload) {
    this.errors = '';
    this.loadingBulk = true;    //adding spinner in submit button
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.loadingBulk = false;    //dismissing spinner in submit button 
      // this.toastr.error("Please select file", 'Error');
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {
      this.loadingBulk = false;    //dismissing spinner in submit button 
      if (validExts[0] != this.type) {
        this.error = "Please select correct extension";
      }                      //if file is not empty
      else if (validExts = this.type) {
        this.loadingBulk = true;    //dismissing spinner in submit button 
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();                     //convert the formdata
        formData.append('excel_file', file);                       //append the name of excel file
        var method = "post";                                       //post method 
        var url = 'spare_bulk_upload/'                               //post url
        var i: number;                                              //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data) {
            if (data['response_code'] == "200") {   //if error
              this.loadingBulk = false;    //dismissing spinner in submit button 
              if (data['response'].fail) {
                if (data['response'].fail.length > 0) {                                    //failure response 

                  var a = [];
                  data['response'].fail.forEach(value => {
                    a.push(value.error_message
                    );
                  });

                  this.bulk = a;

                }
              }
              //success response
              //if newly add product for excisting employee code or employee id
              // if (data['success']) {
              // if (data['success'].length >= 0) {                         //success response
              //   for (i = 0; i < data['success'].length; i++) {
              //     this.ngOnInit();               //count the length
              //     this.loadingBulk = false;    //dismissing spinner in submit button 
              //     this.toastr.success(data['sucess'][i].success_message, 'Success!');  //toastr message for sucess
              //     // this.modalService.dismissAll();                                 //to close the modal box

              //   }
              // }
              if (data['success'].length >= 0) {
                for (i = 0; i < data['success'].length; i++) {
                  this.bulk = '';
                  this.error = '';
                  this.loadingBulk = false;
                  this.showTable = false;
                  this.chRef.detectChanges();
                  this.getsparemasterdetails();
                  this.toastr.success(data['success'][i].Success_message, 'Success!');  //toastr message for Success
                }
              }
              // }
            }

            //if other errors
            else if (data['response_code'] == "400" || data['response_code'] == "500") {                   //if failure
              this.loadingBulk = false;    //dismissing spinner in submit button 
              this.error = data['response'].message;          //toastr message for error
            }
            else {                                                                 //if not sucess
              this.loadingBulk = false;    //dismissing spinner in submit button 
              this.error = data['response'].message;        //toastr message for error
            }
          }
        },
          error => this.toastr.error(error)
        );
      }
    }
  }
  // ----------------------------------------------------------------------------------------------------------------------------//
  //edit function for spare
  editspare(data) {
    this.errors = '';
    this.edit_spare = data;

    this.onChangeProduct(this.edit_spare.product.product_id);
    this.Editsparemaster = new FormGroup({
      "spare_master_id": new FormControl(this.edit_spare.spare_master_id, [Validators.required]),
      "spare_code": new FormControl(this.edit_spare.spare_code, [Validators.required, this.noWhitespace]),
      "spare_model": new FormControl(this.edit_spare.spare_model, [Validators.required, this.noWhitespace]),
      "spare_name": new FormControl(this.edit_spare.spare_name, [Validators.required, this.noWhitespace, Validators.maxLength(48)]),
      "product_id": new FormControl(this.edit_spare.product.product_id, [Validators.required]),
      "subproduct_id": new FormControl(this.edit_spare.subproduct.subproduct_id, [Validators.required]),
    });
    console.log(this.edit_spare.product.product_id)
    console.log("sparecode",this.edit_spare.spare_code)
    console.log("Totaleditspare",this.edit_spare);
  }

  editsparemaster() {
    this.loadingEdit = true;
    this.errors = '';
    this.exp_error = '';
    this.loading = true;    //adding spinner in submit button 
    var data = this.Editsparemaster.value;                  //Data to be passed for add api

    if (this.load == '') {
      //  alert("go")
      this.exp_error_add = "";
      var url = 'edit_spare_master/';                                       //api url for add
      console.log(data);
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                   //if sucess
          this.loadingEdit = false //dismissing  the spinner in submit button
          this.showTable = false;
          this.chRef.detectChanges();                                         //reloading the component
          this.getsparemasterdetails();
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingEdit = false;
          this.modalService.dismissAll();
          this.errors = "";
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
          this.loadingEdit = false //dismissing  the spinner in submit button
          this.errors = result.response.message;                        //toastr message for error
          this.loadingEdit = false;
        }
        else {                                                           //if not sucess
          this.loadingEdit = false //dismissing  the spinner in submit button
          this.errors = result.response.message;     //toastr message for error
          this.loadingEdit = false;
        }
      }, (err) => {                                                        //if error
        console.log(err);                                             //prints if it encounters an error
      });

    }
    else {
      this.loadingEdit = false;
      // alert("don't go image")

    }
  }
  // ------------------------------------------------------------------------------------------------------------//
  //delete function for spare master
  trashspare(data) {
    this.errors = '';
    this.spare = data;                                       //get the data
    var spare_code = { "spare_code": this.spare.spare_code };                       //compare id    
    var url = 'delte_spare_master/?';             // api url for getting the details with using post params
    var id = "spare_code";                        //id for API url
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, spare_code).subscribe((result) => {
          if (result.response.response_code == "200") {                     //if sucess
            this.showTable = false;
            this.chRef.detectChanges();
            this.getsparemasterdetails();
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                   //if failure
            // this.errors = result.response.message;           //toastr message for error
            this.toastr.error(result.response.message, 'Error');
          }
          else {                                                           //if not sucess
            //  this.errors = result.response.message;          //toastr message for error
            this.toastr.error(result.response.message, 'Error');
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }

  onChangeProduct(product_id) {
    this.errors = '';
    var product = +product_id; // country: number
    var url = 'get_subproduct_details/?'; // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.subproduct_details = result.response.data;
        this.Addsparemaster.controls["subproduct_id"].setValue('');
        // this.Editsparemaster.controls["subproduct_id"].setValue('');
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  onChangeProductEdit(product_id) {
    this.errors = '';
    var product = +product_id; // country: number
    var url = 'get_subproduct_details/?'; // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.subproduct_details = result.response.data;
        this.Editsparemaster.controls["subproduct_id"].setValue('');
        // this.Editsparemaster.controls["subproduct_id"].setValue('');
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = result.response.message;
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //saranya24-dec-2020 for alphabet block in keyboard
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    // console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


}
