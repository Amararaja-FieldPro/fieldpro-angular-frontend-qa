import { Component, OnInit } from '@angular/core';


import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';// To use toastr
// import { ajaxservice } from '../../app/ajaxservice';
import { ajaxservice } from '../../../../../app/ajaxservice';

import {ActivatedRoute} from '@angular/router';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
// import { LoaderComponent } from '../views/pages/loader/loader.component';
import { LoaderComponent } from '../../../../views/pages/loader/loader.component';


@Component({
  selector: 'kt-customerfeedback',
  templateUrl: './customerfeedback.component.html',
  styleUrls: ['./customerfeedback.component.scss']
})
export class CustomerfeedbackComponent implements OnInit {
  Feedback: FormGroup;
  ticket_id: any;
  csi_detials: any;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  loadingSubmit: boolean;
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  constructor(public ajax: ajaxservice, private toastr: ToastrService,
    private overlay: Overlay,private activatedRoute: ActivatedRoute) {
      this.ticket_id=localStorage.getItem('ticket_id');
      // console.log(this.ticket_id);
  //   this.activatedRoute.queryParams.subscribe(params => {
  //     console.log(params);
  //     this.ticket_id = params['ticket_id'];
  //     console.log(this.ticket_id); // Print the parameter to the console. 
  // });
    this.Feedback = new FormGroup({
      "ticket_id": new FormControl(""),
      "customer_name": new FormControl("", [Validators.required,this.noWhitespace, Validators.pattern("^[a-z A-Z]*$")]),
      "customer_number": new FormControl("", [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "email_id": new FormControl("", [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-zA-Z]{2,4}$")]),
      "referrence_number": new FormControl(""),
      "rating1": new FormControl(0),
      "rating2":new FormControl(0),
      "rating3":new FormControl(0),
      "rating4":new FormControl(0),
      "rating5":new FormControl('',[Validators.required]),
      "rating6":new FormControl('',[Validators.required]),
      "rating7":new FormControl('',[Validators.required]),
      "rating8":new FormControl('',[Validators.required]),
      "rating9":new FormControl('',[Validators.required]),
      "rating10":new FormControl(0),
      "rating11":new FormControl(0),
      "remarks":new FormControl('',)
    });
  }

  ngOnInit() {
    this.Feedback.reset();
    this.get_csi_details();
  }
  get log() {
    return this.Feedback.controls;                       //error logs for create user
  }
  get_csi_details()
  {
// console.log("hihi")
    // this.overlayRef.attach(this.LoaderComponentPortal); //attach loader
    var data = { "ticket_id": this.ticket_id}
    var url = 'get_csi_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.csi_detials = result.response.data;              //storing the api response in the array
       this.Feedback.patchValue({
        "customer_name": this.csi_detials.customer_name,    
        "email_id":this.csi_detials.email_id,
        "referrence_number": this.csi_detials.sap_reference_number,
        "customer_number":this.csi_detials.contact_number,
        "ticket_id":this.csi_detials.ticket_id,
       })
        // console.log(this.csi_detials,"this.csi_detials")
        // console.log(this.Feedback.value)
        // this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {   
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  customer_feedback() {
    // console.log(this.Feedback.value);
    var data = {   
      "feedback_details": [{
        "product_performance": [{
          "question ": "Are you satisfied with Product Performance?",
          "rating": +this.Feedback.value.rating1
        },
        {
          "question ": "Are you satisfied with the product look and feel?",
          "rating": +this.Feedback.value.rating2
        },
        {
          "question ": " Are you satisfied with the life of the Product?",
          "rating": +this.Feedback.value.rating3
        },
        {
          "question ": "How do you rate our Product compare to our competitor?",
          "rating": +this.Feedback.value.rating4
        }
        ],
        "sales_service": [{
          "question": "Are you satisfied with the response given by the technician?",
          "rating": +this.Feedback.value.rating5
        },
        {
          "question ": " Has the technician attended the site within a reasonable time as committed",
          "rating": +this.Feedback.value.rating6
        },
        {
          "question": " Are you satisfied with the professional conduct of the Service Personnel?",
          "rating": +this.Feedback.value.rating7
        },
        {
          "question ": "Did you find the technician competent enough to resolve the problem",
          "rating": +this.Feedback.value.rating8
        },
        {
          "question ": " Are you satisfied with the resolution Provided?",
          "rating": +this.Feedback.value.rating9
        },
        {
          "question ":"Have you read & understood the safety precautions pasted on cell/battery?",
          "rating": +this.Feedback.value.rating12
        }
        ],
        "saftey": [{
          "question": "Is the training given by manufacturer helping you in handling the battery safely and accident free",
          "rating": +this.Feedback.value.rating10
        },
        {
          "question ": " Can you able to execute installation & commissioning of VRLA batteries easily?",
          "rating": +this.Feedback.value.rating11
        },
        {
          "question ": "In you opinion on all the above factors who is your nearest choice (Competitor)?",
          "rating": 2
        }

        ]

      }],
      "remarks":this.Feedback.value.remarks
    }
    // console.log(data)
    this.loadingSubmit=true;
    data["ticket_id"] =this.Feedback.value.ticket_id
    // console.log(data)
    var url = 'customer_satisfaction/';
    this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200")                           //if sucess
            {
              this.loadingSubmit=false;
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
             
                                       //to close the modal box
            }
            else if (result.response.response_code == "400") {
              this.loadingSubmit=false;                  //if failure
              this.toastr.error(result.response.message, 'Error');            //toastr message for error
            }
            else if (result.response.response_code == "500") {
              this.loadingSubmit=false;
              this.toastr.error(result.response.message, 'Error');        //toastr message for error
            }
            else {    
              this.loadingSubmit=false;                                                                 //if not sucess
              this.toastr.error("Something went wrong", 'Error');        //toastr message for error
            }
          }, (err) => { 
            this.loadingSubmit=false;                                                          //if error
            console.log(err);   //prints if it encounters an error
          });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
