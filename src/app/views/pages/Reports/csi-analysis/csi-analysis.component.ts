import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IAccLoadedEventArgs, IAccTooltipRenderEventArgs, IPointEventArgs } from '@syncfusion/ej2-angular-charts';
import { ChartComponent } from '@syncfusion/ej2-angular-charts';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { FormControl } from '@angular/forms';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { AccumulationChartComponent, AccumulationChart, AccumulationDataLabel } from '@syncfusion/ej2-angular-charts';
@Component({
  selector: 'kt-csi-analysis',
  templateUrl: './csi-analysis.component.html',
  styleUrls: ['./csi-analysis.component.scss']
})
export class CsiAnalysisComponent implements OnInit {
  public piedata: Object[];
  public datalabel: Object;
  public tooltip: Object;
  public title: String;
  @ViewChild(('chart'), { static: false })
  public chartObj: ChartComponent;
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  public pointClick(args: IPointEventArgs): void {
    document.getElementById("lbl").innerText = "X : " + args.point.x + "\nY : " + args.point.y;
  };
  constructor(public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.datalabel = { visible: true };
    this.tooltip = { enable: true };
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    this.title = 'CSI Response Analysis - Percentage';
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    if (this.start_date.month > 4) {
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear + 1) + "-" + '03' + "-" + '31';
      var finance = Fyear + "-" + (Fyear + 1)
    }
    else {
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance = (Fyear - 1) + "-" + Fyear
    };
    var data = {
      "employee_code": emp_code,
      "start_date": this.start_date,
      "end_date": this.end_date,
      "state": [],
      "service_desk": [],
      "technician": [],
      "application": [],
      "segment": [],
      "customer": [],
      "finance_year": finance

    };

    this.analyschartreport(data)

  }

  export() {
    this.chartObj.exportModule.export('PNG', 'CSI Analysis');


  }
  updateDataMain(datas) {
    this.analyschartreport(datas)

  }
  public analyschartreport(data) {
    this.piedata = []
    var url = 'csl_analyse/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.piedata = result.response.data;              //storing the api response in the array
        this.title = 'CSI Response Analysis - Percentage for the period of' + " " + "(" + result.response.Financial_Year[0] + "-" + result.response.Financial_Year[1] + ")";
        this.chRef.detectChanges();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
}
