import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';


@Component({
	selector: 'kt-technician-report',
	templateUrl: './technician-report.component.html',
	styleUrls: ['./technician-report.component.scss']
})
export class TechnicianReportComponent implements OnInit {

	from: NgbDateStruct;
	to: NgbDateStruct;
	service_group: any;
	techId: any;
	assesment_data: any;
	technician_code: any;
	from_date: any;
	to_date: any;
	call_report: any;
	employee_code: any;
	filterTechnician: FormGroup;    //form group 
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	today: any;
	technician_report: any;
	ticket_data: any;
	overlayRef: OverlayRef;
	loadingSub: boolean = false;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTable: boolean = false;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		this.filterTechnician = new FormGroup({
			"from_date": new FormControl('2020-09-28', Validators.required),
			"to_date": new FormControl(''),
			"technician_code": new FormControl('', Validators.required),
		})

	}

	ngOnInit() {

		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		// this.overlayRef.attach(this.LoaderComponentPortal);
		var dob = new Date();
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		this.today = Fdate;
		console.log(Fdate);
		var data = {
			"technician_code": null,
			"to_date": Fdate,
			"from_date": Fdate,

		}
		this.filterTechnician.controls['from_date'].setValue(Fdate);
		this.getTech();
		this.getTechnician(data);
		this.overlayRef.detach();

	}

	get log() {
		return this.filterTechnician.controls;                       //error logs for create user
	}
	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',                                                       //specifies the size of the modal box
					windowClass: "center-modalsm",      //modal popup custom size     
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "400") {
				this.ticket_data = result.response.data; //bind result in this variable
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.technician_report, 'technician_report');
	}
	getTech() {
		var url = 'get_alltechnician_details/';                                  // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.alltech = result.response.data;              //storing the api response in the array
				//storing the api response in the array     

			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterTechF() {
		this.loadingSub = true;
		console.log(this.filterTechnician.value.from_date);
		this.from_date = this.filterTechnician.value.from_date;
		this.to_date = this.filterTechnician.value.to_date;
		this.technician_code = this.filterTechnician.value.technician_code;

		if (this.technician_code == "") {
			this.filterTechnician.value.technician_code = null;
		}
		else {
			this.filterTechnician.value.technician_code = this.filterTechnician.value.technician_code;
		}
		if (this.filterTechnician.value.from_date.year == undefined) {
			this.filterTechnician.value.from_date = this.filterTechnician.value.from_date;
			console.log(this.filterTechnician.value.from_date);
		}
		else {
			console.log(this.filterTechnician.value.from_date)
			var Fyear = this.filterTechnician.value.from_date.year;
			var Fmonth = this.filterTechnician.value.from_date.month;
			var Fday = this.filterTechnician.value.from_date.day;
			this.filterTechnician.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
		}
		if (this.filterTechnician.value.to_date.year == undefined) {
			this.filterTechnician.value.to_date = this.filterTechnician.value.to_date;
			console.log(this.filterTechnician.value.to_date);
		}
		else {
			var Tyear = this.filterTechnician.value.to_date.year;
			var Tmonth = this.filterTechnician.value.to_date.month;
			var Tday = this.filterTechnician.value.to_date.day;
			this.filterTechnician.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;

		}
		this.showTable = false;
		this.getTechnician(this.filterTechnician.value);
	}

	getTechnician(data) {
		this.overlayRef.attach(this.LoaderComponentPortal);

		var url = 'technician_reports/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.technician_report = result.response.data;
				this.showTable = true;
				this.loadingSub = false;
				this.chRef.detectChanges();
				this.overlayRef.detach();
				
			}
		}, (err) => {
			this.loadingSub = false;
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openlarge(ticket_details) {

		this.modalService.open(ticket_details, {
			size: 'lg',                                                       //specifies the size of the modal box
			windowClass: "center-modalsm",      //modal popup custom size     
			backdrop: 'static',					//Popup outside click restricted
			keyboard: false,
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal",                                  //specifies the size of the dialog box
			backdrop: 'static',					//Popup outside click restricted
			keyboard: false,
		});
	}
}
