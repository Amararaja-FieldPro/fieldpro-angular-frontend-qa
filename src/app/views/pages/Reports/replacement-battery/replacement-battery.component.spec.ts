import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplacementBatteryComponent } from './replacement-battery.component';

describe('ReplacementBatteryComponent', () => {
  let component: ReplacementBatteryComponent;
  let fixture: ComponentFixture<ReplacementBatteryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplacementBatteryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementBatteryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
