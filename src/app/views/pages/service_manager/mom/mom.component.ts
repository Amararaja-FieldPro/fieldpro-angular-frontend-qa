import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { NgbModal, NgbCalendar, NgbTabset } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
// import { MatRadioChange } from '@angular/material'; //for matradio button
import * as _ from 'lodash';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
// import moment from 'moment';
// import { values } from 'lodash';
declare var require: any
const FileSaver = require('file-saver');
// import { environment } from '../../../../../environments/environment';
// import { map, catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import moment from 'moment';

@Component({
  selector: 'kt-mom',
  templateUrl: './mom.component.html',
  styleUrls: ['./mom.component.scss'],
})
export class MomComponent implements OnInit {
  overlayRef: OverlayRef;
  customerCode: any;          // variable to store the all customer code
  CustomerName: any;           // variable to store the all customer name
  DetailsGroup: FormGroup;      //FormGroup for the mom details add 
  ParticipantListGroup: any;
  MomPointsGroup: any;
  EditDetailsGroup: FormGroup;            //FormGroup for the mom details edit
  EditParticipantListGroup: FormGroup;
  EditMomPointsGroup: FormGroup;
  sitedetails: any;           //store the customer name based sitedetails
  siteSelect: any = [];
  customerSelect: any;
  activeId: any;
  error: string;
  showTables: boolean;
  mom_details: any;
  form: any;
  date: { year: number; month: number; day: number; };
  mom_points: any;
  participant_array: any[];
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private overlay: Overlay,
    private toastr: ToastrService, public _location: Location, private calendar: NgbCalendar, public tabset: NgbTabset,
    private chRef: ChangeDetectorRef, private fb: FormBuilder, private http: HttpClient, private config: NgbDatepickerConfig) {
    var today = new Date();
    var today_date = this.calendar.getToday()
    var year = today_date.year;
    var month = today_date.month;
    var day = today_date.day;
    this.config.maxDate = {
      day: day,
      month: month,
      year: year,
    };
    this.DetailsGroup = new FormGroup({
      "customer_code": new FormControl(""),
      "customer_name": new FormControl("", [Validators.required]),
      "site_id": new FormControl(''),
      "meeting_type": new FormControl("", [Validators.required]),
      "date": new FormControl(today_date, [Validators.required]),
      "from_time": new FormControl("", [Validators.required]),
      "from_am_pm": new FormControl("", [Validators.required]),
      "to_time": new FormControl("", [Validators.required]),
      "to_am_pm": new FormControl("", [Validators.required]),
      "note_taker": new FormControl("", [Validators.required]),
    });
    this.ParticipantListGroup = new FormGroup({
      "participant_name": this.fb.array([this.fb.control(null)], [Validators.required])
    })
    this.MomPointsGroup = new FormGroup({
      "mom_points": this.fb.array([this.fb.control(null)], [Validators.required])
    })
    this.EditDetailsGroup = new FormGroup({
      "mom_id": new FormControl("", [Validators.required]),
      "customer_code": new FormControl("", [Validators.required]),
      "customer_name": new FormControl("", [Validators.required]),
      "site_id": new FormControl('', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "meeting_type": new FormControl("", [Validators.required]),
      "date": new FormControl("", [Validators.required]),
      "from_time": new FormControl("", [Validators.required]),
      "from_am_pm": new FormControl("", [Validators.required]),
      "to_time": new FormControl("", [Validators.required]),
      "to_am_pm": new FormControl("", [Validators.required]),
      "note_taker": new FormControl("", [Validators.required]),
    });
    this.EditParticipantListGroup = new FormGroup({
      // "participant_name": this.fb.array([this.fb.control(null)], [Validators.required])
      "participant_name": new FormArray([])

    })
    this.EditMomPointsGroup = new FormGroup({
      // "mom_points": this.fb.array([this.fb.control(null)], [Validators.required])
      "mom_points": new FormArray([])
    })

    // this.EditMomPointsGroup = this.fb.group({
    //   "mom_points": new FormArray([])
    // });
  }
  get log1() { return this.DetailsGroup.controls; } //error logs for create user
  get log2() { return this.EditDetailsGroup.controls; }
  get log3() { return this.EditMomPointsGroup.controls; }
  get log4() { return this.log3.mom_points as FormArray; }
  get log5() { return this.EditParticipantListGroup.controls; }
  get log6() { return this.log5.participant_name as FormArray }

  // discussion_point:new FormControl (element.discussion_point),
  //     scope: new FormControl (element.scope),
  //     target_date: new FormControl (element.target_date)
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.get_mom_details()
    this.customerName()
    this.ParticipantListGroup = new FormGroup({
      participant_name: new FormArray([this.initSection()])
    });
    this.MomPointsGroup = new FormGroup({
      mom_points: new FormArray([this.init_mompoint_Section()])
    })
  }
  get_mom_details() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var data = {};// storing the form group value
    var url = 'get_mom/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.mom_details = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }

  get_mom_pdf(mom_id) {
    console.log(mom_id)
    var mom_id = mom_id;                            //get the company id                                 
    var url = 'mom_pdf/?';             // api url for getting the details with using post params
    var id = "mom_id";                        //id for API url
    {
      this.ajax.getPdfDocument(url, id, mom_id).subscribe((response) => {
        console.log(response, "response")
        // let file = new Blob([response], { type: 'application/pdf' });
        let file = response.url
        console.log(response.url, "fileURL")
        // var fileURL = URL.createObjectURL(file);
        // window.open(file);  
        FileSaver.saveAs(file, mom_id);
      });
    }
  }

  openLarge(mom_modal, mom_temp_type) {
    this.activeId = "tab-selectbyid1"
    this.error = ""
    if (mom_temp_type == 1) {
      this.DetailsGroup.reset();
      this.ParticipantListGroup.reset();
      this.MomPointsGroup.reset();
    }
    // else if (mom_temp_type == 2) {
    //   this.EditDetailsGroup.reset();
    //   this.EditParticipantListGroup.reset();
    //   this.EditMomPointsGroup.reset();
    // }
    this.modalService.open(mom_modal, {
      size: 'lg',
      // windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  validation() {
    // var sites = this.siteSelect.customer_site;
    // var splitted = sites.split("-", 3);
    // console.log(splitted);
    // var customer_code = splitted[0];
    // var site_id = splitted[1];
    // console.log(site_id)
    // this.DetailsGroup.controls['customer_code'].setValue(customer_code)
    // this.DetailsGroup.controls['site_id'].setValue(site_id)
    // // this.DetailsGroup.controls['customer_name'].setValue(this.customerSelect)
    console.log(this.DetailsGroup.value)
    console.log(this.DetailsGroup)
    console.log(this.DetailsGroup.valid)
  }
  edit_validation() {
    console.log(this.EditDetailsGroup.value)
    console.log(this.EditDetailsGroup)
    console.log(this.EditDetailsGroup.valid)
  }
  participant_submit(part_type) {
    if (part_type == 1) {
      console.log(this.ParticipantListGroup.value)
      console.log(this.ParticipantListGroup.value.participant_name.length)
      if (this.ParticipantListGroup.value.participant_name.length == 1) {
        console.log(this.ParticipantListGroup.value.participant_name[0].participant_name)
        if (this.ParticipantListGroup.value.participant_name[0].participant_name == '' || this.ParticipantListGroup.value.participant_name[0].participant_name == null || this.ParticipantListGroup.value.participant_name[0].participant_name == undefined) {
          this.error = "Participant should not be empty"
          var tab_id = "tab-selectbyid2"
          this.nextTab(tab_id)
        }
        else {
          this.error = ""
          var tab_id = "tab-selectbyid3"
          this.nextTab(tab_id)
        }
      }
      else if (this.ParticipantListGroup.value.participant_name.length > 1) {
        this.error = ""
        var tab_id = "tab-selectbyid3"
        this.nextTab(tab_id)
      }
    }
    else if (part_type == 2) {
      console.log(this.EditParticipantListGroup.value)
      console.log(this.EditParticipantListGroup.value.participant_name.length)
      if (this.EditParticipantListGroup.value.participant_name.length == 1) {
        console.log(this.EditParticipantListGroup.value.participant_name[0].participant_name)
        if (this.EditParticipantListGroup.value.participant_name[0].participant_name == '' || this.EditParticipantListGroup.value.participant_name[0].participant_name == null || this.EditParticipantListGroup.value.participant_name[0].participant_name == undefined) {
          this.error = "Participant should not be empty"
          var tab_id = "tab-selectbyid2"
          this.nextTab(tab_id)
        }
        else {
          this.error = ""
          var tab_id = "tab-selectbyid3"
          this.nextTab(tab_id)
        }
      }
      else if (this.EditParticipantListGroup.value.participant_name.length > 1) {
        this.error = ""
        var tab_id = "tab-selectbyid3"
        this.nextTab(tab_id)
      }
    }
  }
  mom_submit(mom_type) {
    console.log(mom_type)
    if (mom_type == 1) {
      console.log(this.MomPointsGroup.value.mom_points)
      if (this.MomPointsGroup.value.mom_points.length == 1) {
        console.log(this.MomPointsGroup.value.mom_points[0].discussion_point)
        if ((this.MomPointsGroup.value.mom_points[0].discussion_point == '' || this.MomPointsGroup.value.mom_points[0].discussion_point == null || this.MomPointsGroup.value.mom_points[0].discussion_point == undefined)
          || (this.MomPointsGroup.value.mom_points[0].scope == '' || this.MomPointsGroup.value.mom_points[0].scope == null || this.MomPointsGroup.value.mom_points[0].scope == undefined) ||
          (this.MomPointsGroup.value.mom_points[0].target_date == '' || this.MomPointsGroup.value.mom_points[0].target_date == null || this.MomPointsGroup.value.mom_points[0].target_date == undefined)) {
          this.error = "Mom points should not be empty"
          var tab_id = "tab-selectbyid3"
          this.nextTab(tab_id)
        }
        else {
          this.error = ""
          // var tab_id = "tab-selectbyid3"
          // this.nextTab(tab_id)
          this.create_mom()
        }
      }
      else if (this.MomPointsGroup.value.mom_points.length > 1) {
        this.error = ""
        this.create_mom()
      }
    }

    else if (mom_type == 2) {
      console.log("i am here")
      console.log(this.EditMomPointsGroup.value.mom_points)
      if (this.EditMomPointsGroup.value.mom_points.length == 1) {
        console.log("i am here 1")
        console.log(this.EditMomPointsGroup.value.mom_points[0].discussion_point)
        if ((this.EditMomPointsGroup.value.mom_points[0].discussion_point == '' || this.EditMomPointsGroup.value.mom_points[0].discussion_point == null || this.EditMomPointsGroup.value.mom_points[0].discussion_point == undefined)
          || (this.EditMomPointsGroup.value.mom_points[0].scope == '' || this.EditMomPointsGroup.value.mom_points[0].scope == null || this.EditMomPointsGroup.value.mom_points[0].scope == undefined) ||
          (this.EditMomPointsGroup.value.mom_points[0].target_date == '' || this.EditMomPointsGroup.value.mom_points[0].target_date == null || this.EditMomPointsGroup.value.mom_points[0].target_date == undefined)) {
          this.error = "Mom points should not be empty"
          var tab_id = "tab-selectbyid3"
          this.nextTab(tab_id)
        }
        else {
          console.log("i am here 2")
          this.error = ""
          // var tab_id = "tab-selectbyid3"
          // this.nextTab(tab_id)
          this.update_mom()
        }
      }
      else if (this.EditMomPointsGroup.value.mom_points.length > 1) {
        console.log("i am here 3")
        this.error = ""
        this.update_mom()
      }
    }
  }
  public onFilterChange(item: any) {
    console.log(item);
  }

  public onDropDownClose(item: any) {
    // console.log(item);
  }
  public onItemSelect(item: any) {
    console.log(item);
    // this.customerSelect = item.customer_name;
    console.log(item);
    var id = item.city_location_ids.split("-", 5);
    console.log(id, "id")
    var name = id[0];
    var city = id[1];
    var location = id[2];
    var landmark = id[3];
    var customer_code = id[4];     
      this.customerSelect = { "customer_name": name, "city_id": city, "location_id": location, "landmark": landmark, "customer_code": customer_code };
    console.log(this.customerSelect, "customer_details");
    this.getSiteDetails(this.customerSelect);
  }
  public onSiteIdSelect(item: any) {
    console.log(item);
    this.siteSelect = item;

  }
  customername_settings = {
    singleSelection: true,
    idField: 'city_location_ids',
    textField: 'customer_name',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search customer name',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customerName() {
    var data = {}
    var url = 'get_customer_name/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.CustomerName = result.response.data;              //storing the api response in the array
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  siteid_settings = {
    singleSelection: true,
    idField: 'customer_site',
    textField: 'customer_site',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search Site id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };




  // getSiteDetails(item: any) {
  //   console.log(item);
  //   var id = item.city_location_ids.split("-", 5);
  //   console.log(id, "id")
  //   var name = id[0];
  //   var city = id[1];
  //   var location = id[2];
  //   var landmark = id[3];
  //   var customer_code = id[4];
  //     var url = 'get_site_details/';   
  //   var data = { "customer_name": name, "city_id": city, "location_id": location, "landmark": landmark, "customer_code": customer_code };
  //   console.log(data, "customer_details");
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.sitedetails = result.response.data;              //storing the api response in the array
  //       // this.showTable = true;                            //show datas if after call API
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();
  //     }
  //     else if (result.response.response_code == "500") {
  //       // this.showTable = true;                                                       //if not sucess
  //       this.toastr.error(result.response.message, 'Error');        //toastr message for error
  //     }
  //     else {
  //       this.toastr.error("Something went wrong");
  //     }
  //   }, (err) => {
  //     this.overlayRef.detach();
  //     //prints if it encounters an error
  //   });
  // }





  getSiteDetails(data) {
    // this.sitedetails =""
    // console.log(cus_name, "customer_name")
    // var ids = cus_name.split("-", 3);
    // var customer_name = ids[0];
    // console.log(customer_name);
    // var data = { "customer_name": customer_name };
    var url = 'get_site_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sitedetails = result.response.data;              //storing the api response in the array
        // this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        // this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  getSections(ParticipantListGroup) {
    return ParticipantListGroup.controls.participant_name.controls;
  }
  getMomPointSections(MomPointsGroup) {
    return MomPointsGroup.controls.mom_points.controls;
  }
  initSection() {
    return new FormGroup({
      participant_name: new FormControl("")
    });
  }
  edit_initSection() {
    return new FormGroup({
      participant_name: new FormControl("")
    });
  }

  public addPackageRow() {
    // this.serial_valida_success = '';
    // this.serial_empty = '';
    var i = 0;
    // this.AddButton = false
    // if (this.ReProductInfo.value.product_id == null || this.ReProductInfo.value.product_id == '' || this.ReProductInfo.value.product_sub_id == '' ||
    //   this.ReProductInfo.value.product_sub_id == null || this.ReProductInfo.value.model_no == '' || this.ReProductInfo.value.model_no == null) {
    //   this.error = "Please select Product, Sub Product,Model No. Before select Serial no" //toastr message for success
    // }
    // else if (this.form.value.serial_no[i].serial_no == '' || this.form.value.serial_no[i].serial_no == null || this.form.value.serial_no[i].serial_no == undefined) {
    //   this.serial_empty = "Please enter serial number";
    // }
    // else {
    const control = <FormArray>this.ParticipantListGroup.get("participant_name");
    control.push(this.initSection());
    // }
  }
  public editPackageRow() {
    // this.serial_valida_success = '';
    // this.serial_empty = '';
    var i = 0;
    // this.AddButton = false
    // if (this.ReProductInfo.value.product_id == null || this.ReProductInfo.value.product_id == '' || this.ReProductInfo.value.product_sub_id == '' ||
    //   this.ReProductInfo.value.product_sub_id == null || this.ReProductInfo.value.model_no == '' || this.ReProductInfo.value.model_no == null) {
    //   this.error = "Please select Product, Sub Product,Model No. Before select Serial no" //toastr message for success
    // }
    // else if (this.form.value.serial_no[i].serial_no == '' || this.form.value.serial_no[i].serial_no == null || this.form.value.serial_no[i].serial_no == undefined) {
    //   this.serial_empty = "Please enter serial number";
    // }
    // else {
    const control = <FormArray>this.EditParticipantListGroup.get("participant_name");
    control.push(this.edit_initSection());
    // }
  }


  public removeSection(i) {
    console.log(i)
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete the participant",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        const control = <FormArray>this.ParticipantListGroup.get("participant_name");
        control.removeAt(i);
      }
    })
  }
  public edit_removeSection(i) {
    console.log(i)
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete the participant",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        const control = <FormArray>this.EditParticipantListGroup.get("participant_name");
        control.removeAt(i);
      }
    })
  }
  getSections_edit(EditParticipantListGroup) {
    console.log(EditParticipantListGroup)
    return EditParticipantListGroup.controls.participant_name.controls;
  }
  getMomPointSections_edit(EditMomPointsGroup) {
    return EditMomPointsGroup.controls.mom_points.controls;
  }
  init_mompoint_Section() {
    return new FormGroup({
      discussion_point: new FormControl(""),
      scope: new FormControl(""),
      target_date: new FormControl("")
    });
  }
  public add_momponits_Row() {
    var i = 0;
    const control = <FormArray>this.MomPointsGroup.get("mom_points");
    control.push(this.init_mompoint_Section());
  }
  public edit_momponits_Row() {
    var i = 0;
    const control = <FormArray>this.EditMomPointsGroup.get("mom_points");
    control.push(this.init_mompoint_Section());
  }
  public remove_mom_points(i) {
    console.log(i)
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete the participant",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        const control = <FormArray>this.MomPointsGroup.get("mom_points");
        control.removeAt(i);
      }
    })
  }
  public edit_remove_mom_points(i) {
    console.log(i)
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete the participant",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,   //alert outside click by sowndarya
    }).then((result) => {
      if (result.value) {
        const control = <FormArray>this.EditMomPointsGroup.get("mom_points");
        control.removeAt(i);
      }
    })
  }
  fetchNews() {
    console.log("fetching news")
  }
  nextTab(id) {
    this.activeId = id;
  }
  create_mom() {
    this.participant_array = [];
    var sites = this.siteSelect.customer_site;
    console.log(sites)
    var splitted = sites.split("-", 3);
    var customer_code = splitted[0];
    var site_id = splitted[1];

    this.ParticipantListGroup.value.participant_name.forEach(element => {
      var participant_name = element.participant_name
      this.participant_array.push(participant_name)
    });
    // formatting the target date as yyyy-mm-dd
    this.MomPointsGroup.value.mom_points.forEach(element => {
      console.log(element.target_date, "target date")
      let targetdate = moment(element.target_date).format("YYYY-MM-DD");
      console.log(targetdate);
      // var target_date = element.target_date
      // var year = element.target_date.year;
      // var month = element.target_date.month;
      // var day = element.target_date.day;
      // element.target_date = year + "-" + month + "-" + day;
      element.target_date = targetdate
    });
    // console.log(this.participant_array)
    var year = this.DetailsGroup.value.date.year;
    var month = this.DetailsGroup.value.date.month;
    var day = this.DetailsGroup.value.date.day;
    var dateFormated = year + "-" + month + "-" + day;
    console.log(this.customerSelect)
    // var cus_name = this.customerSelect.split("-", 3);
    // var customer_name = cus_name[0];
    var customer_name = this.customerSelect.customer_name;
 
    this.form = {
      "customer_code": customer_code,
      "customer_name": customer_name,
      "site_id": site_id,
      "meeting_type": this.DetailsGroup.value.meeting_type,
      "date": dateFormated,
      "from_time": this.DetailsGroup.value.from_time + " " + this.DetailsGroup.value.from_am_pm,
      "to_time": this.DetailsGroup.value.to_time + " " + this.DetailsGroup.value.to_am_pm,
      "note_taker": this.DetailsGroup.value.note_taker,
      "participant_name": this.participant_array,
      "mom_points": this.MomPointsGroup.value.mom_points
    }
    console.log(this.form)
    var data = this.form; //Data to be passed for add api
    var url = 'add_mom/' //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.showTables = false;
        this.chRef.detectChanges();
        this.get_mom_details();
        var i: number;
        console.log(this.ParticipantListGroup.value.participant_name.length)
        this.ParticipantListGroup.value.participant_name.forEach(element => {
          console.log(this.ParticipantListGroup.value.participant_name.length)
          if (this.ParticipantListGroup.value.participant_name.length != 1) {
            const control = <FormArray>this.ParticipantListGroup.get("participant_name");
            control.removeAt(i);
          }
        })
        console.log(this.MomPointsGroup.value)

        console.log(this.MomPointsGroup.value.mom_points)
        console.log(this.MomPointsGroup.value.mom_points.length)
        this.MomPointsGroup.value.mom_points.forEach(element => {
          console.log(this.MomPointsGroup.value.mom_points.length)
          if (this.MomPointsGroup.value.mom_points.length != 1) {
            const control = <FormArray>this.MomPointsGroup.get("mom_points");
            control.removeAt(i);
          }
        })
        this.toastr.success(result.response.message, 'Success'); //toastr message for success
        this.modalService.dismissAll(); //to close the modal box
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.error = result.response.message;
      }
      else { //if not sucess
        this.error = "Somethng Went Wrong"; //toastr message for error
      }
    });
  }
  remove_dynamic_field(i) {
    this.log6.removeAt(i);
  }
  editmom(mom_data) {
    this.EditDetailsGroup.reset();
    // refresh the dynmic participant and mom points fields
    var i: number;
    console.log(this.EditParticipantListGroup.value.participant_name.length)
    this.EditParticipantListGroup.value.participant_name.forEach(element => {
      console.log(this.EditParticipantListGroup.value.participant_name.length)
      const control = <FormArray>this.EditParticipantListGroup.get("participant_name");
      control.removeAt(i);
    })
    console.log(this.EditMomPointsGroup.value)
    console.log(this.EditMomPointsGroup.value.mom_points)
    console.log(this.EditMomPointsGroup.value.mom_points.length)
    this.EditMomPointsGroup.value.mom_points.forEach(element => {
      console.log(this.EditMomPointsGroup.value.mom_points.length)
      const control = <FormArray>this.EditMomPointsGroup.get("mom_points");
      control.removeAt(i);
    })


    console.log(mom_data)
    // set the value for edit the mom
    console.log(mom_data.date, mom_data.date)
    var res = (mom_data.date.split("-"));
    var date = (res[2] + "-" + res[1] + "-" + res[0]);
    var year_date = date.split("-", 3);
    var splitted = year_date[2].split(",", 1)
    var year: number = +year_date[0];
    var day: number = +splitted[0];
    var month: number = +year_date[1];
    this.date = {
      year: day,
      month: month,
      day: year,
    }
    console.log(mom_data.from_time)
    console.log(mom_data.to_time)

    var from_tm = (mom_data.from_time.split(" "));
    console.log(from_tm)

    var frm_time = (from_tm[0]);
    console.log(frm_time)
    var frm_am_pm = (from_tm[1]);
    console.log(frm_am_pm)


    var to_tm = (mom_data.to_time.split(" "));
    console.log(to_tm)
    var to_time = (to_tm[0]);
    console.log(to_time)
    var to_am_pm = (to_tm[1]);
    console.log(to_am_pm)
    // var year_date = date.split("-", 3);
    // var splitted = year_date[2].split(",", 1)

    console.log(this.date)
    this.EditDetailsGroup.setValue({
      "mom_id": mom_data.mom_id,
      "customer_code": mom_data.customer.customer_code,
      "customer_name": mom_data.customer.customer_name,
      "site_id": mom_data.site_id,
      "meeting_type": mom_data.meeting_type,
      "date": this.date,
      "from_time": frm_time,
      "from_am_pm": frm_am_pm,
      "to_time": to_time,
      "to_am_pm": to_am_pm,
      "note_taker": mom_data.note_taker,
    })
    console.log(mom_data.participant_name)
    var momname_data = mom_data.participant_name
    momname_data.forEach(element => {
      console.log(element)
      this.log6.push(this.fb.group({
        participant_name: [element],
      }))
    });

    console.log(this.EditMomPointsGroup.value)
    console.log(this.EditParticipantListGroup.value)
    var momdata = mom_data.mom_points
    momdata.forEach(element => {
      console.log(element.target_date)
      let Dates = new Date(element.target_date);
      console.log(Dates)
      // var res = (element.target_date.split("-"));
      // var date = (res[2] + "-" + res[1] + "-" + res[0]);
      // var year_date = date.split("-", 3);
      // var splitted = year_date[2].split(",", 1)
      // var year: number = +year_date[0];
      // var day: number = +splitted[0];
      // var month: number = +year_date[1];
      // var target_date = {      //formatting the date to calender format for displaying the target date
      //   year: day,
      //   month: month,
      //   day: year,
      // }
      this.log4.push(this.fb.group({
        discussion_point: [element.discussion_point],
        scope: [element.scope],
        target_date: [Dates]
      }))

    });
    console.log(this.EditMomPointsGroup.value)

  }

  update_mom() {
    console.log(this.EditDetailsGroup.value)
    console.log(this.EditParticipantListGroup.value)
    console.log(this.EditMomPointsGroup.value)
    console.log(this.EditMomPointsGroup.value.mom_points)
    this.participant_array = [];
    this.EditParticipantListGroup.value.participant_name.forEach(element => {
      if (element.participant_name != null) {
        var participant_name = element.participant_name
        this.participant_array.push(participant_name)
      }

    });
    var year = this.EditDetailsGroup.value.date.year;
    var month = this.EditDetailsGroup.value.date.month;
    var day = this.EditDetailsGroup.value.date.day;
    var dateFormated = year + "-" + month + "-" + day;
    console.log(this.EditDetailsGroup)
    console.log(this.EditMomPointsGroup.value)
    // formatting the target date
    this.EditMomPointsGroup.value.mom_points.forEach(element => {
      console.log(element.target_date, "target date")
      let targetdate = moment(element.target_date).format("YYYY-MM-DD");
      console.log(targetdate);
      // var target_date = element.target_date
      // console.log(target_date, "target date")
      // var year = element.target_date.year;
      // var month = element.target_date.month;
      // var day = element.target_date.day;
      // element.target_date = year + "-" + month + "-" + day;
      element.target_date = targetdate

    });
    this.form = {
      "mom_id": this.EditDetailsGroup.value.mom_id,
      "customer_code": this.EditDetailsGroup.value.customer_code,
      "customer_name": this.EditDetailsGroup.value.customer_name,
      "site_id": this.EditDetailsGroup.value.site_id,
      "meeting_type": this.EditDetailsGroup.value.meeting_type,
      "date": dateFormated,
      "from_time": this.EditDetailsGroup.value.from_time + " " + this.EditDetailsGroup.value.from_am_pm,
      "to_time": this.EditDetailsGroup.value.to_time + " " + this.EditDetailsGroup.value.to_am_pm,
      "note_taker": this.EditDetailsGroup.value.note_taker,
      "participant_name": this.participant_array,
      "mom_points": this.EditMomPointsGroup.value.mom_points
    }
    console.log(this.form)
    var data = this.form; //Data to be passed for add api
    var url = 'edit_mom/' //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.showTables = false;
        this.chRef.detectChanges();
        this.get_mom_details()
        var i: number;
        for (i = this.participant_array.length; i > 1; i--) {
          this.remove_dynamic_field(i)
          // for (i = this.participant_array.length; i > 1; i--) {
          //   const control = <FormArray>this.EditParticipantListGroup.get("participant_name");
          //   control.removeAt(i);
          // };
        };
        this.toastr.success(result.response.message, 'Success'); //toastr message for success
        this.modalService.dismissAll(); //to close the modal box
      }
      else if (result.response.response_code == "400" || result.response.response_code == "500") {
        this.error = result.response.message;
      }
      else { //if not sucess
        this.error = "Somethng Went Wrong"; //toastr message for error
      }
    });
  }
}
