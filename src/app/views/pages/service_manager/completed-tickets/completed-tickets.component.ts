import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ToastrService } from 'ngx-toastr';// To use toastr
// import { RequestOptions, Headers } from '@angular/http';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router'; // To enable routing for this component
import { environment } from '../../../../../environments/environment';

declare var require: any
const FileSaver = require('file-saver');
@Component({
  selector: 'kt-completed-tickets',
  templateUrl: './completed-tickets.component.html',
  styleUrls: ['./completed-tickets.component.scss']
})
export class CompletedTicketsComponent implements OnInit {
  //url navigations
  asideMenus: any;
  current_url: any;
  ProductLocation: FormGroup;
  tabvalue: any;
  closed_ticket_data: any;
  completed_ticket_data: [];
  tab_name: any;
  url_search: any;
  locations: any;
  product_id: any;
  location_id: any;
  date_format: any;
  avilable_technicians_details: any;
  currentJustify = 'end';
  ticket_id: any;
  tickt_id: any;
  deffered_tckt: any;
  accepted_tckt: any;
  ticket_details: any;
  technicians_details: any;
  employee_code: any;
  task_counts: any;
  products: any;             //variable to store the task count for the technician
  overlayRef: OverlayRef;
  // var nums:number[] = [1,2,3,3] 
  showTable: boolean = false;
  dataTable: any;
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  showTableClosed: boolean = false;
  showTableCompleted: boolean = false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @ViewChild('dataTable', { static: true }) table;
  pdf: any;
  servicedesk_id: string;
  httplink: string;
  image_or_video: any;
  settingsObj: any = {
  
    "order": [[1, "asc"]],

    deferRender: true,
    scrollY: 200,
    scrollCollapse: true,
    scroller: true

  }
  // table: any;
  return_draw: any;
  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef, private overlay: Overlay, private router: Router,
    public modalService: NgbModal,
    private toastr: ToastrService,) {
    this.ProductLocation = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required),
      "date_format": new FormControl('', Validators.required)
    })

  }
  // ticket_bulk_upload
  ngOnInit() {
    this.servicedesk_id = localStorage.getItem('employee_code');
    this.asideMenus = localStorage.getItem('asideMenus');
    // console.log(this.asideMenus)
    var asidemenu = JSON.parse(this.asideMenus);
    // console.log(typeof (this.asideMenus), "type")
    // console.log(this.router.url);
    var current_url = this.router.url;
    // console.log(this.current_url, "current_url")
    // var submenu = this.asideMenus.map((i)=>{
    //   return {
    //     budget: i.budget,
    //   }
    // })
    // var val= this.asideMenus.map((i)=>{
    //   return {
    //     group: submenu.filter((x) => i.page === this.current_url)
    //   }
    // })

    function check(current_url) {
      asidemenu.forEach(menuItem => {
        // console.log(menuItem, "menuItem")
        if (menuItem.submenu) {
          for (let submenu of menuItem.submenu) {
            // console.log(current_url, "current_url")
            // console.log(submenu.page, "submenu.page")
            // console.log(current_url)
            if (submenu.page == current_url) {
              // console.log(submenu.page, "Matched")
              return true;
            }
          }
        }
      });
      return false;
    }

    let result = check(current_url)
    // console.log(result)
    // console.log(result, "val")

    if (result) {
      // alert("u cannpot go")
      localStorage.clear();
      // window.location.reload(); 

      // alert("cleared")
      this.router.navigate(["/login"]);
    } else {
      // alert("else")
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10
        // processing: true
      }
      this.tabvalue = "tab-selectbyid1";
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      // this.overlayRef.attach(this.LoaderComponentPortal);
      this.get_ticket_product_location()
      this.product_id = null
      this.location_id = null
      this.date_format = null
      this.employee_code = localStorage.getItem('employee_code');
      this.url_search = 'closed_ticket_details/';
      // console.log(this.employee_code);
      // this.get_closed_tickets()
      this.get_completed_tickets()
    }
  }
  // rerender(): void {
  //   this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     // Destroy the table first
  //     dtInstance.destroy();
  //     // Call the dtTrigger to rerender again
  //     this.dtTrigger.next();
  //   });
  // }
  // --------------------------------function for view the modal box------------------------------------------------------//
  openmodal(modal) {
    this.modalService.open(modal, {
      size: 'lg',
      windowClass: "center-modalsm"     //modal popup resize
    });
  }
  openmodalview(view_content, image_or_video) {
    this.httplink = environment.image_static_ip;
    this.image_or_video = image_or_video
    this.modalService.open(view_content, {
      size: 'lg',                                                        //specifies the size of the modal box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  // openedit(modal,ticket_id) {
  //   console.log(ticket_id)
  //   this.edit_tiket_id=ticket_id;
  //   this.modalService.open(modal, {
  //     size: 'lg'
  //   });
  // }
  // ---------------------------------function for getters----------------------------------------------------------------//



  // function for getting the ticket details based on TicketId 
  get_ticket_details(tickt_id, view_tcktdetails) {
    // console.log(this.tickt_id)
    var ticket_id = tickt_id; // country: number                                    
    var url = 'get_ticket_details/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    // console.log(url, id, ticket_id);
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.ticket_details = result.response.data;                                           //reloading the component
        this.modalService.open(view_tcktdetails, {
          size: 'lg',
          // windowClass: "center-modalsm",     //modal popup resize
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click

        });
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the Technician details based on TechnicianId 
  get_technician_details(ticket_id, technician_id, tech_details) {
    var ticket_id = ticket_id
    var technician_id = technician_id
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.technicians_details = result.response.data;
        this.modalService.open(tech_details, {
          size: 'lg',
          windowClass: "center-modalsm",     //modal popup resize
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click

        });
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //-----------------------------------------Function for passing location and Product------------------------------------//
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //onchange functoin for getting the product id
  onChangeSearch(location_id) {
    // console.log(location_id);
    this.date_format = this.ProductLocation.value.date_format;
    this.product_id = this.ProductLocation.value.product_id;
    this.location_id = this.ProductLocation.value.location_id;
    if (this.date_format == "") {
      this.date_format = null;
    }
    else {
      this.date_format = this.ProductLocation.value.date_format;
    }
    if (this.location_id == "") {
      this.location_id = null;
    }
    else {
      this.location_id = this.ProductLocation.value.location_id;
    }
    if (this.product_id == "") {
      this.product_id = null;
    }
    else {
      this.product_id = this.ProductLocation.value.product_id;
    }
    if (this.tabvalue == 'tab-selectbyid1') {
      this.showTableCompleted = false;
      this.get_completed_tickets();
    }
    if (this.tabvalue == 'tab-selectbyid2') {
      this.showTableClosed = false;
      this.get_closed_tickets();
    }


    // this.fetchNews(this.ProductLocation.value);

  }
  // //onchange functoin for getting the location id
  // onChangeLocation(location_id) {
  //   this.location_id = location_id
  //   this.product_id = null
  //   this.fetchNews(this.ProductLocation.value);
  //   // this.get_completed_tickets()
  //   console.log(this.ProductLocation.value);
  // }
  onchangeforsearch() {
    // console.log(this.ProductLocation.value)
  }
  //-------------------------------------------Unassigned Tickets functions starts----------------------------------------//
  // function for getting the unassigned ticket based on the product and location 
  // get_closed_tickets() {
  //   this.overlayRef.attach(this.LoaderComponentPortal);

  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, "date": this.date_format }
  //   var url = 'closed_ticket_details/';                                // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.closed_ticket_data = result.response.data;
  //       this.showTableClosed = true;
  //       this.overlayRef.detach();
  //       this.chRef.detectChanges();
  //       this.dtTrigger.next();
  //       // this.overlayRef.detach();

  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTableClosed = true;
  //       this.overlayRef.detach();
  //     }

  //     else {
  //       this.toastr.error(result.response.message, 'Error');
  //     }

  //   }, (err) => {
  //     this.overlayRef.detach();
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }



  
  get_closed_tickets() {
    this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.closed_ticket_data = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    // console.log(draw,"11111")

    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
  //  console.log(search_value)
  var search_value = ""

   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      
    var url = 'closed_ticket_details/';  
   //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        this.closed_ticket_data = result.response.data;              //storing the api response in the array
        // this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {
          
          "deferLoading": result.response.recordsTotal,
      
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
       
          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            // console.log(draw,"222222")

            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            // console.log(search_value,"search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']   
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      

            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming ='closed_ticket_details/';  
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.closed_ticket_data = result.response.data;              //storing the api response in the array
              this.return_draw = result.response.draw   
              // console.log(this.return_draw ,"3333")
       
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();
            
            });

          },
          "columnDefs": [           
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Technician ID", "targets": 2, "searchable": true },
            { "name": "Product Category", "targets": 3, "searchable": true },
            { "name": "Sub Product Category", "targets": 4, "searchable": true },
            { "name": "Town", "targets": 5, "searchable": true },
            { "name": "Assigned Time", "targets": 6, "searchable": true },
            { "name": "Closed Time", "targets": 7, "searchable": true },
            { "name": "Customer Rating", "targets": 8, "searchable": true },
            { "name": "Resolution Image", "targets": 9, "searchable": false },
            { "name": "SETR", "targets": 10, "searchable": false },
            { "name": "Service Report", "targets": 11, "searchable": false },
          ],
        }

        $('#closed_tickets').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#closed_tickets').DataTable(

            this.settingsObj
          );
        }, 5);
        // console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.closed_ticket_data =[];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }

  // function for getting the Available technician details based on TicketId 
  get_avilable_technicians(tickt_id) {
    this.tickt_id = tickt_id
    var ticket_id = tickt_id; // country: number                                    
    var url = 'avilable_technicians/?';             // api url for getting the details with using post params
    var id = "ticket_id";
    this.ajax.getdataparam(url, id, ticket_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.avilable_technicians_details = result.response.data;
        this.task_counts = this.avilable_technicians_details[0].task_count;
        // this.ViewTechnicianDetails.controls['task_count'].setValue(this.avilable_technicians_details[0].task_count);
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }

  //function for Assign the ticket to the technician based on the ticket id and technician id
  technician_assign(id_details) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to assign",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Assign!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        // console.log(id_details)
        // console.log(this.tickt_id)
        var data = {"servicedesk_id": this.servicedesk_id, "technician_id": id_details.technician_id, "ticket_id": this.tickt_id, "secondary_tec_id": null }
        var url = 'technician_assign/'                                           //api url of Assign Ticket                                    
        this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200")                           //if sucess
            {
              this.ngOnInit();                                                //reloading the component
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
              this.modalService.dismissAll();                                 //to close the modal box
            }
            else if (result.response.response_code == "400") {                  //if failure
              this.toastr.error(result.response.message, 'Error');            //toastr message for error
            }
            else {                                                                     //if not sucess
              this.toastr.error(result.response.message, 'Error');        //toastr message for error
            }
          }, (err) => {                                                           //if error
            console.log(err);                                                 //prints if it encounters an error
          });
      }
    })
  }

  //-------------------------------------------Unassigned Tickets functions end-------------------------------------------//

  //-------------------------------------------Assigned Tickets functions starts------------------------------------------//
  // function for getting the completed ticket based on the product and location 
  // get_completed_tickets() {
  //   this.overlayRef.attach(this.LoaderComponentPortal);
  //   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code, "date": this.date_format }
  //   var url = 'completed_ticket_details/';                                  // api url for getting the details
  //   this.ajax.postdata(url, data).subscribe((result) => {
  //     if (result.response.response_code == "200" || result.response.response_code == "400") {
  //       this.completed_ticket_data = result.response.data;
  //       this.showTableCompleted = true;
  //       this.chRef.detectChanges();
  //       this.overlayRef.detach();
  //       // this.dtTrigger.next();

  //     }
  //     else if (result.response.response_code == "500") {
  //       this.showTableCompleted = true;
  //       this.overlayRef.detach();
  //     }
  //     else {
  //       this.toastr.error(result.response.message, 'Error');
  //     }


  //   }, (err) => {
  //     this.overlayRef.detach();
  //     console.log(err);                                        //prints if it encounters an error
  //   });
  // }

  get_completed_tickets() {
    this.overlayRef.attach(this.LoaderComponentPortal);        //attach the loader
    // console.log(this.name_location,"bfv")
    this.completed_ticket_data = [];
    this.return_draw = 0;
    var length = 10
    var start = 0
    var draw = this.return_draw
    // console.log(draw,"11111")
    var sort = ""
    var column = ""
    // if(this.name_product==null||this.name_product==undefined||this.name_product==''){
    //   var search_value = "";
    // }
    // else{
    //    search_value = this.name_product;
    // }
  //  console.log(search_value)
  var search_value = ""
   var data = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      
    var url = "completed_ticket_details/"
   //var url = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value                              
    // api url for getting the details                                  

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        // this.unassigned_tckt = result.response.data;              //storing the api response in the array    
        // this.showTable = true;
        // this.overlayRef.detach();           //detach the loader
        // this.chRef.detectChanges();         //detech the changes 
        this.completed_ticket_data = result.response.data;              //storing the api response in the array             
        // this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.settingsObj = {
          
          "deferLoading": result.response.recordsTotal,
      
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
       
          ajax: (dataTablesParameters: any, callback) => {
            var data = dataTablesParameters;
            var method = "post";
            var limit = data.limit
            var draw = data.draw
            // console.log(draw,"draw22222")
            var leng = data.length
            var length = (data.start) + (data.length)

            var start = data.start
            var search_value = data.search.value
            // console.log(search_value,"search_value")
            // var country_name = data.columns['Country']
            // var state_name = data.columns['State']
            // var city_name = data.columns['City']
            var sort = data.order[0]['dir']
            var column = data.order[0]['column']   
            var data1 = { "product_id": this.product_id, "location_id": this.location_id, "employee_code": this.employee_code,length , "date": this.date_format,"start" :start ,"draw": draw ,"search_value": search_value , "sort" : sort , "column" : column }      

            // var url_forming = "unassigned_tickets/?length=" + length + "&start=" + start + "&draw=" + draw + "&search_value=" + search_value + "&sort=" + sort + "&column=" + column
            var url_forming = "completed_ticket_details/" 
            var url = url_forming;
            this.ajax.postdata(url, data1).subscribe(result => {
              this.completed_ticket_data = result.response.data;              //storing the api response in the array             
              this.return_draw = result.response.draw     
              // console.log(this.return_draw,"33333")     
              callback({
                recordsTotal: result.response.recordsTotal,
                recordsFiltered: result.response.totalRecordswithFilter,
                data: [],

              });
              this.chRef.detectChanges();
            
            });

          },
          "columnDefs": [           
            { "name": "Ticket Id", "targets": 1, "searchable": true },
            { "name": "Technician ID", "targets": 2, "searchable": true },
            { "name": "Product Category", "targets": 3, "searchable": true },
            { "name": "Sub Product Category", "targets": 4, "searchable": true },
            { "name": "Town", "targets": 5, "searchable": true },
            { "name": "Completed Time", "targets": 6, "searchable": true },
            { "name": "Resolution Image", "targets": 7, "searchable": false },
            { "name": "SETR", "targets": 8, "searchable": false },
          ],
        }

        $('#datatables').DataTable().destroy();
        setTimeout(() => {
          this.table = $('#datatables').DataTable(

            this.settingsObj
          );
        }, 5);
        // console.log(this.settingsObj)
      }
      else if (result.response.response_code == "400") {
        this.completed_ticket_data =[];
        // this.toastr.error(result.response.message, 'Error');          
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();


      }
      else if (result.response.response_code = "500") {                                                         //if not Success
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                 //detach the loader
      }
      else {                                                         //if not Success
        this.toastr.error("Some thing went wrong");        //toastr message for error
        this.chRef.detectChanges();
        this.overlayRef.detach();                    //detach the loader
      }
    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  fetchNews(evt: any) {
    this.tabvalue = evt.nextId;
    this.product_id = null;
    this.location_id = null;
    this.date_format = null;
    this.ProductLocation.controls['product_id'].setValue('');
    this.ProductLocation.controls['location_id'].setValue('');
    this.ProductLocation.controls['date_format'].setValue('');
    if (this.tabvalue == "tab-selectbyid1") {
      this.showTableCompleted = false;
      this.get_completed_tickets();

    }
    else if (this.tabvalue == "tab-selectbyid2") {
      this.showTableClosed = false;

      this.get_closed_tickets();
    }
    // console.log(evt); // has nextId that you can check to invoke the desired function
  }

  downloadPdf(pdfName: string, pdfUrl: string) {
    // const pdfUrl = pdfUrl;
    // var pdf = "https://" + pdfUrl
    // console.log(environment.image_static_ip)
    var pdf = environment.image_static_ip + pdfUrl
    FileSaver.saveAs(pdfUrl, pdfName);
  }
  downloadPdfs(ticket_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var id = "ticket_id";
    var url = "setr_pdf/?";
    var value = ticket_id;
    this.ajax.getPdfDocument(url, id,ticket_id).subscribe((response) => {
      // console.log(response, "response")
      let file = response.url
      // console.log(response.url, "fileURL")   ;  
      FileSaver.saveAs(file, ticket_id);
      this.overlayRef.detach();

    },(err) => {
      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });  
  }
  service_report_pdf(ticket_id) {
    this.overlayRef.attach(this.LoaderComponentPortal);

    // console.log(ticket_id,"ticket_id")
    var id = "ticket_id";
    var url = "generate_pdf/?";
    var value = btoa(ticket_id); //base 64 encode the ticket id
    // console.log(value,"value")
    this.ajax.getPdfDocument(url, id,value).subscribe((response) => {
      // console.log(response, "response")
      let file = response.url
      // console.log(response.url, "fileURL")   ;  
      FileSaver.saveAs(file, ticket_id);
      this.overlayRef.detach();

    },(err) => {
      this.overlayRef.detach();

      console.log(err);  //prints if it encounters an error
    });  
  }
}
