import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { FormControl } from '@angular/forms';
import { NgbModal, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';// To use toastr
import * as xlsx from 'xlsx';
import { environment } from '../../../../../environments/environment';
import * as FileSaver from 'file-saver';
import { ExcelService } from '../../../../excelservice';
@Component({
  selector: 'kt-performance-report',
  templateUrl: './performance-report.component.html',
  styleUrls: ['./performance-report.component.scss']
})
export class PerformanceReportComponent implements OnInit {
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  showTable: string;
  dtOptions: DataTables.Settings = {};
  // dtOptions1: DataTables.Settings = {};
  overlayRef: OverlayRef;
  // @ViewChild(DataTableDirective, { static: false })
  // dtElement: DataTableDirective;
  // dtTrigger: Subject<any> = new Subject();
  // dtInstance: Promise<DataTables.Api>;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @ViewChild('perfortbl', { static: false }) perfortbl: ElementRef;
  performance_data: any;
  contract_details: any;
  showTable_contract: string;
  performance_export: any;
  // dtOptions1: { pagingType: string; pageLength: number; lengthMenu: number[]; processing: boolean; };

  constructor(private excelservice: ExcelService, private modalService: NgbModal, public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private overlay: Overlay, private toastr: ToastrService) { }

  ngOnInit() {

    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    //   lengthMenu: [10, 25, 50],
    //   processing: true
    // };
    // $('#datatables').DataTable().destroy();
    // setTimeout(function () {
    //   $('#datatables').DataTable(this.dtOptions)
    // }, 2000);//wait 2 seconds
    // this.dtOptions1 = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    //   lengthMenu: [10, 25, 50],
    //   processing: true
    // };
    // $('#datatablesshort').DataTable().destroy();
    // setTimeout(function () {
    //   $('#datatablesshort').DataTable(this.dtOptions1)
    // }, 2000);//wait 2 seconds
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    // this.title = 'Performance Report - Percentage';
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    console.log(this.start_date.month, "this.start_date.month")
    if (this.start_date.month > 4) {
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear + 1) + "-" + '03' + "-" + '31';
      var finance = Fyear + "-" + (Fyear + 1)
    }
    else {
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance = (Fyear - 1) + "-" + Fyear
    }

    var data = {
      "employee_code": emp_code,
      "start_date": this.start_date,
      "end_date": this.end_date,
      "state": [],
      "product": [],
      "product_sub_id": [],
      "model_no": [],
      "service_desk": [],
      "technician": [],
      "application": [],
      "segment": [],
      "customer": [],
      "finance_year": finance,
      "fin_flag": "Year"

    };
    this.performance_report(data)
  }
  openLarge(performance, customer_code, site_id) {
    var data = { "customer_code": customer_code, "site_id": site_id }
    this.get_contract(data);
    this.modalService.open(performance, {
      size: 'lg',
      windowClass: "center-modallg",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,
    });
  }
  get_contract(data) {
    this.showTable_contract = 'false';
    var url = 'perform_getcontract/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.contract_details = result.response.data;              //storing the api response in the array
        // this.dtOptions1 = {
        //   pagingType: 'full_numbers',
        //   pageLength: 10,
        //   lengthMenu: [10, 25, 50],
        //   processing: true,
        // };
        $('#datatablesshort').DataTable().destroy();
        setTimeout(function () {
          $('#datatablesshort').DataTable(this.dtOptions)
        }, 2000);//wait 2 seconds
        this.showTable_contract = 'true';
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.showTable_contract = 'true';
        this.chRef.detectChanges();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.showTable_contract = 'true';
        this.chRef.detectChanges();
      }
    }, (err) => {
      console.log(err);
    });
  }
  exportToExcel() {
    this.excelservice.exportAsExcelFile(this.performance_export, 'Performance Report');
    // const ws: xlsx.WorkSheet =   
    // xlsx.utils.table_to_sheet(this.perfortbl.nativeElement);
    // const wb: xlsx.WorkBook = xlsx.utils.book_new();
    // xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    // xlsx.writeFile(wb, 'PerformanceReport.xlsx');
  }
  updateDataMain(datas) {
    // this.performance_data = [];
    this.performance_report(datas)

  }
  export_contract(data) {
    var url = 'perform_export/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.performance_export = result.response.data;

      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      // this.overlayRef.detach();
    });
  }
  public performance_report(data) {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.showTable = 'false';
    var url = 'perform_report/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.performance_data = [];
        $('#datatables').DataTable().destroy();
        this.performance_data = result.response.data;              //storing the api response in the array
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10,
          lengthMenu: [10, 25, 50],
          // data: result.response.data,
          processing: true,
          //      columns: [{ data: 'SNO' },{ data: 'Batterymake' }, { data: 'BatteryRating' },{ data: 'ManufacturingDate' },{ data: 'I&CDate' },{ data: 'SYSAH' },{ data: 'Application' },
          //   { data: 'battery_cell' },{ data: 'source_charging' },{ data: 'rating_charging' },{ data: 'system_voltage' },{ data: 'no_of_bc_bank' },{ data: 'no_of_bc_per_bank' },{ data: 'mn_ah' },{ data: 'load_battery' },{ data: 'capture_complaints' },{ data: 'failed_quantity' },
          //   { data: null,     
          //     render: function (data, type, row) {
          //     return `
          //     <div class="text-center">
          //     <a style="cursor: pointer;"
          //     onclick="editMember(data[0])"><i class="fas fa-download"
          //       style="color:#242fb7;cursor: pointer;" aria-hidden="true"></i></a>
          //     </div>
          // `;
          // }, "width": "5%" }
          //   ],
          // destroy: true
        };

        this.overlayRef.detach();
        this.showTable = 'true';
        this.export_contract(data);
        this.chRef.detectChanges();

        setTimeout(function () {
          $('#datatables').DataTable(this.dtOptions)
        }, 2000);//wait 2 seconds


      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      this.overlayRef.detach();
    });
  }
  downloadimage(image) {
    if (image != "" || image != undefined || image != null || image != "None" || image != '') {
      // var pdf = "assets/images/assignIcon19.png"
      var pdf = environment.image_static_ip + image
      FileSaver.saveAs(pdf, "PerformanceReport");

    }
  }
}
