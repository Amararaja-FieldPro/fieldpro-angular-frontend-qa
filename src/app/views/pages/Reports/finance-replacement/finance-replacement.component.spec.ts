import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceReplacementComponent } from './finance-replacement.component';

describe('FinanceReplacementComponent', () => {
  let component: FinanceReplacementComponent;
  let fixture: ComponentFixture<FinanceReplacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceReplacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceReplacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
