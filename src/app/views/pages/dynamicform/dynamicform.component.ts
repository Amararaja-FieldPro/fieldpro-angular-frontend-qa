import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators  } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice';
import { ToastrService } from 'ngx-toastr';
// import { StringNullableChain } from 'lodash';
@Component({
  selector: 'kt-dynamicform',
  templateUrl: './dynamicform.component.html',
  styleUrls: ['./dynamicform.component.scss']
})
export class DynamicformComponent implements OnInit {
  package_management_details:any;
  dtOptions: DataTables.Settings = {};
  addcont:string;
  package_details:any;
  package_plan_details:any;
  Addcont: FormGroup; //form group for Add button
  Edcont: FormGroup; //form group for Edit button
  isSubmitted = false;
  discount_type:String;
  licenseamount:string;
  discount_value:BigInteger;
  discount : string = '0';  //used for onchange function for discount
  package_plan: string;  // data type of package plan
  package_type_id: string;  //input type of package type
  per_license_cost:number;  //input type of perlicense cost
  constructor(private fb: FormBuilder, public ajax: ajaxservice,  private toastr: ToastrService, private modalService: NgbModal, public activeModal: NgbActiveModal,  vcr: ViewContainerRef) {
     //Defines the required services to be used 
   }
 
  index:any;
  edit_data: any;
  minNum = 15;
  arrayItems:Array<{package_plan: string, package_type_id: string,  per_license_cost:number}> = []; // array push
  ngOnInit() {
    this.addcont = "";
    this.arrayItems = [];
    //form group for Add button
    this.Addcont = new FormGroup({
			"package_type_id": new FormControl("", [Validators.required]),
			"package_plan_type_id": new FormControl("", [Validators.required]),
      "organization_type":new FormControl("",[Validators.required]),
      "discount_type":new FormControl('',[Validators.required]),
      "status":new FormControl(1, [Validators.required]),
			"minimum_license":new FormControl( '',[Validators.required,Validators.pattern("^[0-9]*$")]),
      "per_license_cost":new FormControl('', [Validators.required,Validators.pattern("^[0-9]*$")]),
      "discount_value":new FormControl("", ),      
      "discount_amount":new FormControl({disabled: true},),      
      }),

    
    this.getpackagedata();                          // calling function on page load
    this.getdata(); 
    this.getpackageplandata();                          // calling function on page load
  }
    //--------------------------------------------------------------------------------------------------//

   //To Get the package details from API
   getdata() {
    var url = 'get_package_management_details/';                        // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.package_management_details = result.response.data;                 //storing the api response in the array
      
      }
    }, (err) => {
      console.log(err);                         //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  // function for per license cost
  onlicenseChange(amount)
  {
    this.licenseamount=amount;
    if(this.discount == '0')                                          //if type is no
    {
    
      this.Addcont.controls['discount_amount'].setValue(this.licenseamount);    //set the value of total price in add subscription
    }
    if(this.discount == '1')                                  // set input is 1
    {
      if(amount > this.licenseamount)                          //if amount greater than than per license cost
      {
        this.toastr.error("Discount amount cannot be greater than license cost") 
        this.Addcont.controls['discount_amount'].setValue('');                         //for Add subscription
        this.Edcont.controls['discount_amount'].setValue('');      //toastr message for error
      }
      else
      {
        var value =  +this.licenseamount - amount;                            //if value calculate new amount
        this.Addcont.controls['discount_amount'].setValue(value);               //for add subscription
        this.Edcont.controls['discount_amount'].setValue(value);               //for edit subscription
      }
    }
    else if (this.discount == '2')                                                 //set the input is 2
    {
      if(amount > 100)                                                                 //if amount greater than 100
      {
        this.toastr.error("Discount percentage cannot be greater than 100")  
        this.Addcont.controls['discount_amount'].setValue('');                         //for Add subscription
        this.Edcont.controls['discount_amount'].setValue('');               //toastr message for error
      }
      else
      {
        var value =  Math.round((amount/100) * +this.licenseamount);  
        var newvalue = +this.licenseamount - value;                             //if percentage calculate new amount
        this.Addcont.controls['discount_amount'].setValue(newvalue);                         //for Add subscription
        this.Edcont.controls['discount_amount'].setValue(newvalue);                           //for edit subscription
      }
    }
  }
     //--------------------------------------------------------------------------------------------------//
    //  function for calculate discount amount
  onSearchChange(amount)
  {
    if(this.discount == '1')                                                                  //set input value is 1
    {
      if(amount > this.licenseamount)                                                           //if amount greater than than per license cost
      {
        console.log(amount);
        console.log( this.licenseamount);
        this.toastr.error("Discount amount cannot be greater than license cost")            //toastr message for error
      }
      else
      {
        var value =  +this.licenseamount - +amount;                                    //if value calculate new amount
        this.Addcont.controls['discount_amount'].setValue(value);                         //for Add subscription
        this.Edcont.controls['discount_amount'].setValue(value);                          //for edit subscription
      }
    }
    else if (this.discount == '2')                                                           //set input value is 2
    {
      if(amount > 100)                                                                 //if amount greater than 100
      {
        this.toastr.error("Discount percentage cannot be greater than 100")               //toastr message for error
      }
      else
      {
        var value =  Math.round((amount/100) * +this.licenseamount);                         //if percentage calculate amount   
        var newvalue = +this.licenseamount - value;                             //if percentage calculate new amount
        this.Addcont.controls['discount_amount'].setValue(newvalue);                         //for Add subscription
        this.Edcont.controls['discount_amount'].setValue(newvalue);                              //for edit subscription
      }
    }
   
  }
   //--------------------------------------------------------------------------------------------------//
  getpackagedata() {
                              // intialising datatable        
    var url = 'get_package_details/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.package_details = result.response.data;              //storing the api response in the array
        
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    
  }
  //-----------------------------------------------------------------------------------------------------//
  getpackageplandata() {
                               // intialising datatable        
    var url = 'get_package_plan_type/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.package_plan_details = result.response.data;              //storing the api response in the array
       
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
     //--------------------------------------------------------------------------------------------------//
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else {
      return `with: ${reason}`;
    }
  }
   //--------------------------------------------------------------------------------------------------//

  //Function to call the add api 
  addpackage(addcont) {
 
    if(this.Addcont.invalid)                             //Check if the form is valid
    {                                    
      this.toastr.error("Enter Correct Data", 'Error');    //If empty display the toast error message
    }
    else
    {
      var url = 'add_package_management/'                        //api url of add
      this.ajax.postdata(url, this.Addcont.value ).subscribe((result) => {
        if (result.response.response_code == "200") {                           //reconstructing the table with new datas with a time out function to wait for the table destroy
          this.ngOnInit();                                                         //reloading the component
          this.toastr.success(result.response.message, 'Success');                //toastr message for success
          this.modalService.dismissAll();     
          this.discount = '0'; 
        }
      else{
          this.toastr.error(result.response.message, 'Error');                //toastr message for error
        }
      }, (err) => {
        console.log(err);                                                 //prints if it encounters an error
      });
    }
  }
  //--------------------------------------------------------------------------------------------------//

  //Function to store the line item selected to edit

  editpackage(data) {
    console.log(data);
    this.edit_data = data;
    this.discount=this.edit_data.discount_type;
    this.licenseamount=this.edit_data.per_license_cost;
    this.Edcont = new FormGroup({
      "package_type_id": new FormControl(this.edit_data.package_type.package_id, [Validators.required]),
      "package_plan_type_id": new FormControl(this.edit_data.package_plan_id, [Validators.required]),
      "package_plan": new FormControl(this.edit_data.package_plan, [Validators.required]),
      "organization_type":new FormControl(this.edit_data.organization_type,[Validators.required]),
      "discount_type":new FormControl(this.edit_data.discount_type,[Validators.required]),
      "status":new FormControl(this.edit_data.status, [Validators.required]),
      "minimum_license":new FormControl( this.edit_data.minimum_license,[Validators.required,Validators.pattern("^[0-9]*$")]),
      "per_license_cost":new FormControl(this.edit_data.per_license_cost, [Validators.required,Validators.pattern("^[0-9]*$")]),
      "discount_value":new FormControl(this.edit_data.discount_value, [Validators.pattern("^[0-9]*$")]),      
      "discount_amount":new FormControl(this.edit_data.discount_amount, [Validators.pattern("^[0-9]*$")]),        
      
      });
       //Stores the data into the array variable
      this.Edcont.controls['discount_amount'].setValue(this.edit_data.discount_amount);
  }
    //--------------------------------------------------------------------------------------------------//

  //Function to call the edit api 
  editform(edit_data) {
    var url = 'edit_package_management/'                                      //api url of edit api
    this.ajax.postdata(url, this.Edcont.value).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.ngOnInit();                                                       //reloading the component
        this.toastr.success(result.response.message, 'Success');                 //toastr message for success
        this.modalService.dismissAll();                                           //to close the modal box
      }
     else if (result.response.response_code == "400") {
        this.toastr.error(result.response.message, 'Error');                         //toastr message for error
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  //onchange for discount
  onChangeofOptions(amount) {
    // console.log(amount);
    this.discount = amount;
}
//--------------------------------------------------------------------------------------------------//
  // Function for modal box 
  
//modal box for add button
  open2(content2) {
	
		this.modalService.open(content2, {
			size: 'lg'                      //specifies the size of the modal box
		});
  }
  //modal box for edit icon
  Open1(content1) {
	
		this.modalService.open(content1, {
			size: 'lg'                      //specifies the size of the modal box
		});
  }
 //--------------------------------------------------------------------------------------------------//
 
  //submit buttton for add button
  submitform()
	{
	
		if (this.Addcont.valid) {
			console.log("success");
			console.log(this.Addcont.value)
		}
		else
		{
			console.log(this.Addcont.value)
    }
  
    this.package_plan =this.Addcont.value.package_plan;
    this.package_type_id =this.Addcont.value.package_type_id;
   
    this.per_license_cost =this.Addcont.value.per_license_cost;
  
  
   console.log(this.arrayItems);
 

  }


}