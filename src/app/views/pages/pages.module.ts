
// Angular
import { RouterModule } from '@angular/router';
import { NgbAlertConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialPreviewModule } from '../../views/partials/content/general/material-preview/material-preview.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import { Checklistdatabase } from '../../checklistdatabase.service';
// for data table
import { DataTablesModule } from 'angular-datatables';
import { IntegralUIModule } from '@lidorsystems/integralui-web/bin/integralui/integralui.module';
// import { NgxSpinnerModule } from "ngx-spinner";
// import { WizardComponent } from'../pages/wizard/wizard/wizard.component';
// import { Wizard1Component } from '../pages/wizard/wizard1/wizard1.component';
import { Wizard2Component } from '../pages/wizard/wizard2/wizard2.component';
// import { Wizard3Component } from '../pages/wizard/wizard3/wizard3.component';
// import { Wizard4Component } from'../pages/wizard/wizard4/wizard4.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { AccumulationChartModule } from '@syncfusion/ej2-angular-charts';
// import { ChartModule} from '@syncfusion/ej2-angular-charts';

import {  CategoryService, BarSeriesService, ColumnSeriesService, LineSeriesService,LegendService, DataLabelService, MultiLevelLabelService, SelectionService,PieSeriesService, AccumulationLegendService, AccumulationTooltipService, AccumulationAnnotationService,
	AccumulationDataLabelService,StackingAreaSeriesService,StackingBarSeriesService,StackingColumnSeriesService,ExportService} from '@syncfusion/ej2-angular-charts';
import {
	MatAutocompleteModule,
	MatNativeDateModule,
	MatCardModule,
	MatChipsModule,
	MatSelectModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatSliderModule,
	MatPaginatorModule,
	MatSortModule,
	MatSidenavModule,
	MatSnackBarModule,
	MatToolbarModule,
	MatDividerModule,
	MatTabsModule,
	MatTableModule,
	MatTooltipModule,
	MatListModule,
	MatGridListModule,
	MatButtonToggleModule,
	MatBottomSheetModule,
	MatMenuModule,
	MatTreeModule,
	MatDatepickerModule	

} from '@angular/material';

import { MatCheckboxModule } from '@angular/material/checkbox';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
// import { MailModule } from './apps/mail/mail.module';
// import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
// import { MyPageComponent } from './my-page/my-page.component';
import { NewsubscriptionComponent } from './newsubscription/newsubscription.component';
import { BillingDetailComponent } from './billing-detail/billing-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatStepperModule,MatFormFieldModule,MatInputModule ,MatRadioModule } from '@angular/material';
import { ManagesubscriptionComponent } from './managesubscription/managesubscription.component';
import { ProductmasterComponent } from './productmaster/productmaster.component';
import { HttpModule } from '@angular/http';
//image cropper
//buttons
import { MatButtonModule } from '@angular/material';
//expansion bar
import { MatExpansionModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';

// Pages
// import { SlamappingComponent } from './slamapping/slamapping.component';
// import { ManagementComponent } from './management/management.component';
// import { ConfigurationComponent } from './configuration/configuration.component';
import { DynamicformComponent } from './dynamicform/dynamicform.component';
import { PackagetypeComponent } from './packagetype/packagetype.component';
import { ajaxservice } from '../../ajaxservice';
import { ExcelService } from './../../excelservice';
// import {ReimbursmentformService} from './reimbursmentform';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { SparemasterComponent } from './sparemaster/sparemaster.component';
import { MaindashboardComponent } from './maindashboard/maindashboard.component';
import { AmcComponent } from './amc/amc.component';
import { ServicegroupComponent } from './servicegroup/servicegroup.component';
import { SpareComponent } from './spare/spare.component';
//import { TreeComponent } from './tree/tree.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { SmtpsettingsComponent } from './smtpsettings/smtpsettings.component';
import { SmsComponent } from './sms/sms.component';
import { ContentmanagementComponent } from './contentmanagement/contentmanagement.component';
import { CustomermanagemantComponent } from './customermanagemant/customermanagemant.component';
import { SlaComponent } from './service_manager/sla/sla.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { NgxOrgChartModule } from 'ngx-org-chart';
import { LocationmappingComponent } from './locationmapping/locationmapping.component';
import { OrgstructureComponent } from './orgstructure/orgstructure.component';
import {RaiseticketComponent} from './servicedesk/raiseticket/raiseticket.component';
import { NewticketsComponent } from './servicedesk/newtickets/newtickets.component';
import {ConfigurationComponent} from './configuration/configuration.component';
import{ OngoingticketsComponent } from './servicedesk/ongoingtickets/ongoingtickets.component';
import {CompletedticketsComponent} from './servicedesk/completedtickets/completedtickets.component';
import{ LocationComponent } from './location/location.component';
import{ KnowledgebaseComponent } from './servicedesk/knowledgebase/knowledgebase.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { LeaderboardComponent } from './service_manager/leaderboard/leaderboard.component';
// import { GetticketComponent } from './getticket/getticket.component';
import { SparerequestComponent } from './service_manager/sparerequest/sparerequest.component';
import { ManagercontractComponent } from './managercontract/managercontract.component';
import { TrainingscoreComponent } from './service_manager/trainingscore/trainingscore.component';
import { ReimbursementComponent } from './service_manager/reimbursement/reimbursement.component';
import { ReportsComponent } from './reports/reports.component';
import { ImprestspareComponent } from './service_manager/imprestspare/imprestspare.component';
import { TransferspareComponent } from './transferspare/transferspare.component';
import {LoaderComponent} from '../pages/loader/loader.component';
import { ReimbursmentformComponent } from './reimbursmentform/reimbursmentform.component';
import { PerformancematrixComponent } from './service_manager/performancematrix/performancematrix.component';
import { ServicesparerequestComponent } from './servicedesk/servicesparerequest/servicesparerequest.component';
import { NewTicketsComponent } from './service_manager/new-tickets/new-tickets.component';
import { OngoingTicketsComponent } from './service_manager/ongoing-tickets/ongoing-tickets.component';
import { CompletedTicketsComponent } from './service_manager/completed-tickets/completed-tickets.component';
import { TechnicianTrackingComponent } from './service_manager/technician-tracking/technician-tracking.component';
import { SpareReportComponent } from './service_manager/spare-report/spare-report.component';
import { SummaryReportComponent } from './service_manager/summary-report/summary-report.component';
import { FRMReportComponent } from './service_manager/frm-report/frm-report.component';
import { TechnicianReportComponent } from './service_manager/technician-report/technician-report.component';
import { CallReportComponent } from './service_manager/call-report/call-report.component';
import { AttendanceReportComponent } from './service_manager/attendance-report/attendance-report.component';
import { AmcContractComponent } from './servicedesk/amc-contract/amc-contract.component';
import { SlamappingComponent } from './slamapping/slamapping.component';
import { RevenueComponent } from './service_manager/revenue/revenue.component';
import { ProductivityComponent } from './service_manager/productivity/productivity.component';
import { SatComponent } from './service_manager/sat/sat.component';
import { LiveTrackingComponent } from './service_manager/live-tracking/live-tracking.component';
import { ManagerDashboardComponent } from './service_manager/manager-dashboard/manager-dashboard.component';
import { DashboardServicedeskComponent } from './servicedesk/dashboard-servicedesk/dashboard-servicedesk.component';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from "@mat-datetimepicker/core";
import { ManageSubscriptionComponent } from './sass_admin/manage-subscription/manage-subscription.component';
import { PackagePlanComponent } from './sass_admin/package-plan/package-plan.component';
import { SpareMasterComponent } from './spare-master/spare-master.component';
import { CustomerTrackingComponent } from './customer-tracking/customer-tracking.component';
import { SegmentComponent } from './segment/segment.component';
import { ApplicationComponent } from './application/application.component';
import { ModalComponent } from './modal/modal.component';
import { PopupsComponent } from './popups/popups.component';
import { RaiseComponent } from './service_manager/raise/raise.component';
import { CompensationComponent } from './compensation/compensation.component';
import { CustomerRaisecallComponent } from './customer/customer-raisecall/customer-raisecall.component';
import { CsiComponent } from './customer/csi/csi.component';
import { MomComponent } from './service_manager/mom/mom.component';
import { TatTrendComponent } from './Reports/tat-trend/tat-trend.component';
import { FiltersComponent } from './Reports/filters/filters.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {  ScatterSeriesService, DateTimeService, TrendlinesService} from '@syncfusion/ej2-angular-charts';
import { CSLTrendComponent } from './Reports/csltrend/csltrend.component';
import { ChartModule, ChartAllModule } from '@syncfusion/ej2-angular-charts';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { FinancialReportComponent } from './Reports/financial-report/financial-report.component';
import { FinanceReplacementComponent } from './Reports/finance-replacement/finance-replacement.component';
import { CsiAnalysisComponent } from './Reports/csi-analysis/csi-analysis.component';
import { BatteryAgeReportComponent } from './Reports/battery-age-report/battery-age-report.component';
import { ReplacementBatteryComponent } from './Reports/replacement-battery/replacement-battery.component';
import { FtrreportComponent } from './Reports/ftrreport/ftrreport.component';
import { CompetitorreportComponent } from './Reports/competitorreport/competitorreport.component';
import { RejectreasonreportComponent } from './Reports/rejectreasonreport/rejectreasonreport.component';
import { LocationwisereportComponent } from './Reports/locationwisereport/locationwisereport.component';
import { TicketagereportComponent } from './Reports/ticketagereport/ticketagereport.component';
import { ComplaintsummeryComponent } from './Reports/complaintsummery/complaintsummery.component';
import { PerformanceReportComponent } from './Reports/performance-report/performance-report.component';
// import { NgChartsModule } from 'ng2-charts';
// var ng2Charts = require("ng2-charts")

//import { AuthModule } from '../../views/pages/auth/auth.module';
// import { NgSelectModule } from '@ng-select/ng-select';

// import { DemoComponent } from './demo/demo.component';
// const appRoutes: Routes = [
// 	{
// 		path: '',
// 		pathMatch: 'full',
// 		component: ManagesubscriptionComponent
// 	},
// 	];
@NgModule({
	declarations: [
		// MyPageComponent, 
		NewsubscriptionComponent,
		Wizard2Component,
		SlaComponent, 
		BillingDetailComponent, 
		ManagesubscriptionComponent, 
		ProductmasterComponent, 
		// ManagementComponent, 
		ConfigurationComponent, 
		DynamicformComponent, 
		PackagetypeComponent, 
		WarehouseComponent, 
		SparemasterComponent,
		MaindashboardComponent,
		AmcComponent,
		ServicegroupComponent,
		SpareComponent,
		//TreeComponent,
		SitesettingsComponent,
		SmtpsettingsComponent,
		SmsComponent,
		ContentmanagementComponent,
		CustomermanagemantComponent,		
		UsermanagementComponent,		
		LocationmappingComponent,
		OrgstructureComponent,
		RaiseticketComponent,
		NewticketsComponent,
		OngoingticketsComponent,
		CompletedticketsComponent,
		LocationComponent,
		KnowledgebaseComponent,
		ProfileComponent,
		ChangepasswordComponent,
		LeaderboardComponent,
		// GetticketComponent,
		SparerequestComponent,
		ManagercontractComponent,
		TrainingscoreComponent,
		ReimbursementComponent,
		ReportsComponent,
		ImprestspareComponent,
		TransferspareComponent,
		LoaderComponent,
		ReimbursmentformComponent,
		PerformancematrixComponent,
		ServicesparerequestComponent,
		NewTicketsComponent,
		OngoingTicketsComponent,
		CompletedTicketsComponent,
		TechnicianTrackingComponent,
		AmcContractComponent,
		SpareReportComponent,
		SummaryReportComponent,
		FRMReportComponent,
		TechnicianReportComponent,
		CallReportComponent,
		AttendanceReportComponent,
		SlamappingComponent,
		RevenueComponent,
		ProductivityComponent,
		SatComponent,
		LiveTrackingComponent,
		ManagerDashboardComponent,
		DashboardServicedeskComponent,
		ManageSubscriptionComponent,
		PackagePlanComponent,
		SpareMasterComponent,
		CustomerTrackingComponent,
		SegmentComponent,
		ApplicationComponent,
		ModalComponent,
		PopupsComponent,
		RaiseComponent,
		CompensationComponent,
		CustomerRaisecallComponent,
		CsiComponent,
		MomComponent,
		TatTrendComponent,
		FiltersComponent,
		CSLTrendComponent,
		FinancialReportComponent,
		FinanceReplacementComponent,
		CsiAnalysisComponent,
		BatteryAgeReportComponent,
		ReplacementBatteryComponent,
		FtrreportComponent,
		CompetitorreportComponent,
		RejectreasonreportComponent,
		LocationwisereportComponent,
		TicketagereportComponent,
		ComplaintsummeryComponent,
		PerformanceReportComponent,
		// DemoComponent,
		
	],
	exports: [RouterModule],
	imports: [
		BsDatepickerModule.forRoot() ,
		// NgSelectModule,
	 	NgMultiSelectDropDownModule.forRoot(),
		NgxOrgChartModule,
		ChartModule,
		BrowserModule, AccumulationChartModule,
		// NgxSpinnerModule,
		IntegralUIModule,
		DataTablesModule,
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		// MailModule,
		// ECommerceModule,
		ReactiveFormsModule,
		MatStepperModule,
		MatFormFieldModule,
		MatInputModule,
		MatRadioModule,
		MatButtonModule,
		MatExpansionModule,
		MatIconModule,
		MatDialogModule,
		NgbModule,
		CoreModule,
		TranslateModule.forRoot(),
		MaterialPreviewModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		HttpModule,
		PerfectScrollbarModule,
		MatButtonModule,
		MatExpansionModule,
		MatIconModule,
	// 	MatAutocompleteModule,
	// MatNativeDateModule,
	MatCheckboxModule,
	MatCardModule,
	// MatChipsModule,
	// MatSelectModule,
	// MatProgressBarModule,
	MatProgressSpinnerModule,
	// MatSliderModule,
	// MatPaginatorModule,
	// MatSortModule,
	// MatSidenavModule,
	// MatSnackBarModule,
	// MatToolbarModule,
	MatDividerModule,
	// MatTabsModule,
	// MatTableModule,
	// MatTooltipModule,
	// MatListModule,
	// MatGridListModule,
	// MatButtonToggleModule,
	// MatBottomSheetModule,
	// MatMenuModule,
	// MatTreeModule,
	MatDatepickerModule,
	// MatBottomSheetRef,
	MatDatetimepickerModule,
	MatNativeDatetimeModule,
	ButtonModule, ChartAllModule
	// NgChartsModule
	//AuthModule
	],
	providers: [NgbAlertConfig,ajaxservice,ExcelService,PieSeriesService,AccumulationLegendService,AccumulationTooltipService,AccumulationDataLabelService,	AccumulationAnnotationService,
		CategoryService,
		BarSeriesService,
		ColumnSeriesService,
		LineSeriesService,LegendService,
		DataLabelService, MultiLevelLabelService,
		SelectionService,StackingAreaSeriesService,StackingBarSeriesService,
		StackingColumnSeriesService,ExportService,
		ScatterSeriesService, DateTimeService, TrendlinesService ],
	entryComponents: [
	]
	
})
export class PagesModule {
}
