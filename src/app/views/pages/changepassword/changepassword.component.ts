import { Component, OnInit } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
// import { Router } from '@angular/router'; // To enable routing for this component
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';// To use toastr
@Component({
  selector: 'kt-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  changepassword: FormGroup;

  constructor(private ajax:ajaxservice,private toastr :ToastrService) { 
    this.changepassword = new FormGroup({
      "email": new FormControl('', [Validators.required, Validators.email]),
      "role_name": new FormControl('', [Validators.required]),
      "old_password": new FormControl('', [Validators.required]),
      "new_password": new FormControl('', [Validators.required]),
      "confirm_password": new FormControl('', [Validators.required]),

    });
         
  }

  employee_code : string
  user: any
  //node_id : IntegralUIEditorType
  ngOnInit() {
    this.getuser();
  }
  get log() {
    return this.changepassword.controls;                       //error logs for create user
  }
  getuser() {
    this.employee_code =localStorage.getItem('employee_code');
    var employee_code = this.employee_code; // country: number                                    
    var url = 'get_userdetails/?';             // api url for getting the details with using post params
    var id = "employee_code";
    // console.log(url, id, employee_code);
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.user = result.response.data;
        this.user.forEach(element => {
          this.changepassword.controls['email'].setValue(element.email_id);
          this.changepassword.controls['role_name'].setValue(element.role.role_name);

        })}})}
        change_password() {
          // console.log(this.changepassword.value);
          var url = 'web_change_password/'                                           //api url of edit api                                    
          this.ajax.postdata(url, this.changepassword.value)
            .subscribe((result) => {
              if (result.response.response_code == "200")                   //if sucess
              {
                // this.ngOnInit();                                                //reloading the component
                this.toastr.success(result.response.message, 'Success');        //toastr message for success
              }
              else if (result.response.response_code == "400") {                 //if failiure
                this.toastr.error(result.response.message, 'Error');            //toastr message for error
              }
              else {                                                             //if not sucess
                this.toastr.error(result.response.message, 'Error');        //toastr message for error
              }
            }, (err) => {
              console.log(err);                                                 //prints if it encounters an error
            });
        }

}
