import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectreasonreportComponent } from './rejectreasonreport.component';

describe('RejectreasonreportComponent', () => {
  let component: RejectreasonreportComponent;
  let fixture: ComponentFixture<RejectreasonreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectreasonreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectreasonreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
