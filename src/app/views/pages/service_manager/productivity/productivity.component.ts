import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'kt-productivity',
  templateUrl: './productivity.component.html',
  styleUrls: ['./productivity.component.scss'],

})
export class ProductivityComponent implements OnInit {
  productive_data: any;
  total: any;
  sla: any;
  Year: any;
  month: any;
  Date: FormGroup;
  esclated_data: any;
  calldata: any;
  callclosure: any;
  call: any;
  totalcount: any;
  years: any[];
  year: any;
  select: string;
  loadingSub: boolean = false;   //button spinner by Sowndarya
  // date = new FormControl(moment());
  constructor(private ajax: ajaxservice, private chRef: ChangeDetectorRef) {
    const currentYear = (new Date()).getFullYear();
    const range = (start, stop, step) => Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + (i * step));
    this.years = range(currentYear - 1, currentYear + 5, 1)
    console.log(this.years);
    this.select = "select the month";
    for (var i = 0; i < this.years.length; i++) {
      this.year = this.years[i];
      console.log(this.year);
    }
    this.Date = new FormGroup({
      "month": new FormControl('', Validators.required),
      "year": new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.Year = new Date().getFullYear();
    console.log(this.Year);
    this.Date.controls['year'].setValue(this.Year);
    this.month = new Date().getMonth();
  }
  get log() {
    return this.Date.controls;
  }

  date() {
    this.loadingSub = true;
    this.select = '';

    console.log(this.Date.value);

    this.getProductive();
  }
  getProductive() {
    console.log(this.Date.value.month);
    console.log(this.Date.value.year);
    console.log(this.Date.value);
    var url = 'productive_details/';
    var data = { "month": this.Date.value.month, "year": this.Date.value.year };                               // api url for getting the cities
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.loadingSub = false;
        this.esclated_data = result.response.call_esclated;
        this.call = result.response.call_closed;
        this.sla = result.response.sla.SLA_Compliance_Target;
        this.totalcount = result.response.sla.total_count;
        this.callclosure = result.response.call_closure;
        this.chRef.detectChanges();

      }
    }, (err) => {
      this.loadingSub = false;
      console.log(err);                                        //prints if it encounters an error
    });
  }

}  
