import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesparerequestComponent } from './servicesparerequest.component';

describe('ServicesparerequestComponent', () => {
  let component: ServicesparerequestComponent;
  let fixture: ComponentFixture<ServicesparerequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesparerequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesparerequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
