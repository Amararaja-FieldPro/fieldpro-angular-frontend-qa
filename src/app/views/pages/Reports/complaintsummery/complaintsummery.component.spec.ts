import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintsummeryComponent } from './complaintsummery.component';

describe('ComplaintsummeryComponent', () => {
  let component: ComplaintsummeryComponent;
  let fixture: ComponentFixture<ComplaintsummeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintsummeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintsummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
