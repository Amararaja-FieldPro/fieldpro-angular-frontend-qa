import { Component, ViewEncapsulation, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { filter } from 'rxjs/operators';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { ChartComponent } from '@syncfusion/ej2-angular-charts';
import { FormControl } from '@angular/forms';
import * as FileSaver from 'file-saver';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
// import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
// import { Browser } from '@syncfusion/ej2-base';
let series1: any[] = [];
let yValue = [7.66, 8.03, 8.41, 8.97, 8.77, 8.20, 8.16, 7.89, 8.68, 9.48, 10.11, 11.36, 12.34, 12.60, 12.95,
  13.91, 16.21, 17.50, 22.72, 28.14, 31.26, 31.39, 32.43, 35.52, 36.36,
  41.33, 43.12, 45.00, 47.23, 48.62, 46.60, 45.28, 44.01, 45.17, 41.20, 43.41, 48.32, 45.65, 46.61, 53.34, 58.53];
let point1; let i; let j = 0;
for (i = 1973; i <= 2013; i++) {
  point1 = { x: i, y: yValue[j] };
  series1.push(point1); j++;
}
import * as xlsx from 'xlsx';

import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject } from 'rxjs';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ExcelService } from '../../../../excelservice'

@Component({
  selector: 'kt-tat-trend',
  templateUrl: './tat-trend.component.html',
  styleUrls: ['./tat-trend.component.scss'],
  providers: [ChartComponent]
  // encapsulation: ViewEncapsulation.None

})
export class TatTrendComponent implements OnInit {
  public data: Object[] = series1;
  public primaryYAxis: Object = {
    title: '',
    interval: 100, lineStyle: { width: 0 }, majorTickLines: { width: 0 }
  };
  public primaryXAxis: Object = {
    title: 'Months',

  };
  public chartArea: Object = {
    border: { width: 0 }
  };
  public tooltip: Object;
  public title: string = 'TAT Trend %';
  public primaryXAxis1: Object;
  public chartData: Object[];
  public title1: string;
  public primaryYAxis1: Object;
  public palette: string[];
  public chartData1: Object[];

  public marker: Object;

  ChartTrendlines: {
    //...
    //Customize the Trendline styles
    fill: string; width: number; opacity: number; dashArray: string;
  }[];
  @ViewChild('chart', { static: false }) chartObj: ChartComponent;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  // public chartObj: ChartComponent;
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  currentYear = moment().year();
  Description: Object[];
  TATHeaders: Object[];
  TAT_1: Object[];
  total: Object[];
  Total_total: Object[];
  export_table_data: any;
  value: any;
  dtOptions: DataTables.Settings = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  Export: any[];
  current_month: { [x: string]: any; };
  chartdata_TAT1: any;
  chartdata_TAT2: string;
  chartdata_preTAT1: string;
  chartdata_preTAT2: string;
  targettat1: string;
  targettat2: string;
  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef, private calendar: NgbCalendar, private excelservice: ExcelService) {
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15,
      processing: true,
      responsive: true,
      lengthMenu: [15, 25, 50]
    };
    this.marker = {
      dataLabel: { visible: true }
    };
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    this.start_date = moment(this.fromdates.value._d).format("YYYY-MM-DD"); //change the date forma
    // this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
    // this.end_date = Fyear + "-" + '03' + "-" + '31';
    console.log(Fmonth, "Fmonth")
    if (Fmonth > 4) {
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear + 1) + "-" + '03' + "-" + '31';
      var finance = Fyear + "-" + (Fyear + 1)
    }
    else {
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance = (Fyear - 1) + "-" + Fyear
    }
    var datas = {
      "employee_code": emp_code, "start_date": this.start_date,
      "end_date": this.end_date, "service_desk": [], "state": [],
      "technician": [],
      "customer": [],
      "segment": [],
      "finance_year": finance
    };
    this.chartreport(datas)
    this.primaryXAxis = { valueType: 'Category' };
    this.title = 'TAT Trend %' + " " + "(" + finance + ")";
  }
  export() {
    this.chartObj.exportModule.export('PNG', 'TAT Trend');

  }
  exportToExcel() {
    const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'TatReport.xlsx');
  }
  updateDataMain(datas) {
    this.chartreport(datas)

  }
  public chartreport(data) {
    var url = 'tat_report/';
    this.ajax.postdata(url, data).subscribe((result) => {
      console.log(result);
      if (result.response.response_code == "200") {
        this.chartData = result.response.data;              //storing the api response in the array
        this.chartData1 = result.response.TAT;
        this.Description = result.response.Description;
        this.TATHeaders = result.response.TATHeaders;
        this.TAT_1 = result.response.row;
        this.Total_total = result.response.Total;
        this.export_table_data = result.response.Export;
        var data_difference=result.response.Finance_title[1]-result.response.Finance_title[0];
        if (data_difference==1){
          this.chartdata_TAT1 = "TAT1% " + result.response.Finance_title[1];
          this.chartdata_TAT2 = "TAT2% " + result.response.Finance_title[1];
          this.chartdata_preTAT1 = "TAT1% " + result.response.Finance_title[0];
          this.chartdata_preTAT2 = "TAT2% " + result.response.Finance_title[0];
          this.targettat1 = "Target TAT 1%";
          this.targettat2 = "Target TAT 2%";
        }
        else{
          var before=result.response.Finance_title[1]-1
          var after=+(result.response.Finance_title[0])+1
          this.chartdata_TAT1 = "TAT1% " + "( "+before +"-"+result.response.Finance_title[1]+")";
          this.chartdata_TAT2 = "TAT2% " +"( "+ before +" - "+result.response.Finance_title[1]+" )";
          this.chartdata_preTAT1 = "TAT1% " + "( "+ result.response.Finance_title[0]+"-"+after+")";
          this.chartdata_preTAT2 = "TAT2% " +"( "+ result.response.Finance_title[0]+"-"+after+")";
          this.targettat1 = "Target TAT 1%";
          this.targettat2 = "Target TAT 2%";
        }
       
        console.log(result.response.Finance_title[1]-result.response.Finance_title[0]);
        console.log(this.chartData, "this.chartData")
        this.dtOptions = {
          pagingType: 'full_numbers',
          searching: false,
          pageLength: 15,
          lengthMenu: [15, 25, 50],
          data: result.response.Table,
          search: false,
          processing: true,
          columns: [{ data: 'Description', orderable: false }, { data: 'Year' }, { data: 'Apr' }, { data: 'May' }, { data: 'Jun' }, { data: 'Jul' }, { data: 'Aug' }, { data: 'Sep' }, { data: 'Oct' }, { data: 'Nov' }, { data: 'Dec' }, { data: 'Jan' }, { data: 'Feb' }, { data: 'Mar' }, { data: 'Average' }, { data: 'Total_calls' }, { data: 'Ytd' }
          ],

          "columnDefs": [
            {
              "targets": [], //first column / numbering column
              "orderable": false, //set not orderable
              "visible": false,
              "searchable": false,
            },
          ],
          order: [],
          destroy: true

        };
        this.Export = result.response.Table;
        $('#datatable').DataTable(this.dtOptions)
        this.chRef.detectChanges();

        this.title = 'TAT Trend % for the period of' + " " + "(" + result.response.Finance_title[0] + "-" + result.response.Finance_title[1] + ")";
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }



}
