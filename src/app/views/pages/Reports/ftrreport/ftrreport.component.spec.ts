import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FtrreportComponent } from './ftrreport.component';

describe('FtrreportComponent', () => {
  let component: FtrreportComponent;
  let fixture: ComponentFixture<FtrreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FtrreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FtrreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
