import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
@Component({
	selector: 'kt-frm-report',
	templateUrl: './frm-report.component.html',
	styleUrls: ['./frm-report.component.scss']
})
export class FRMReportComponent implements OnInit {
	from: NgbDateStruct;
	to: NgbDateStruct;
	frm: any;
	service_group: any;
	techId: any;
	assesment_data: any;
	technician_code: any;
	from_date: any;
	to_date: any;
	frm_reports: any;
	employee_code: any;
	filterFRM: FormGroup;    //form group 
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	today: any;
	frm_details: any;
	length: any;
	frm_data: any;
	technicians_details: any;
	ticket_data: any;
	loadingSub: boolean = false;
	model: NgbDateStruct;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTable: boolean = false;
	ShowService: boolean = false;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;

	constructor(public modalService: NgbModal, private ajax: ajaxservice, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private overlay: Overlay) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		var date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		this.filterFRM = new FormGroup({
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to, Validators.required),
			"technician_code": new FormControl('', Validators.required),
		})

	}

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		//   this.overlayRef.attach(this.LoaderComponentPortal);
		var dob = new Date();
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		this.today = Fdate;
		// console.log(Fdate);
		var data = {
			"technician_code": null,
			"to_date": Fdate,
			"from_date": Fdate,

		}
		this.getTech();
		this.getFRM(data);

	}

	get log() {
		return this.filterFRM.controls;                       //error logs for create user
	}

	// Excel download
	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.frm_data, 'frm_data');
	}
	getTech() {
		var url = 'get_alltechnician_details/';                                  // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.alltech = result.response.data;              //storing the api response in the array
				this.overlayRef.detach();  //detaching the loader after response
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterFRMF() {
		this.loadingSub = true;
		// console.log(this.filterFRM.value.from_date);
		this.from_date = this.filterFRM.value.from_date;
		this.to_date = this.filterFRM.value.to_date;
		this.technician_code = this.filterFRM.value.technician_code;

		if (this.technician_code == "") {
			this.filterFRM.value.technician_code = null;
		}
		else {
			this.filterFRM.value.technician_code = this.filterFRM.value.technician_code;
		}
		if (this.filterFRM.value.from_date.year == undefined) {
			this.filterFRM.value.from_date = this.filterFRM.value.from_date;
		}
		else {
			// console.log(this.filterFRM.value.from_date)
			var Fyear = this.filterFRM.value.from_date.year;
			var Fmonth = this.filterFRM.value.from_date.month;
			var Fday = this.filterFRM.value.from_date.day;
			this.filterFRM.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
		}
		if (this.filterFRM.value.to_date.year == undefined) {
			this.filterFRM.value.to_date = this.filterFRM.value.to_date;
		}
		else {
			var Tyear = this.filterFRM.value.to_date.year;
			var Tmonth = this.filterFRM.value.to_date.month;
			var Tday = this.filterFRM.value.to_date.day;
			this.filterFRM.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;

		}
		this.showTable = false;
		this.getFRM(this.filterFRM.value);
	}
	FRMDetails(data, frm_required) {
		this.frm = data;
		// console.log(data);
		var tk_id = this.frm.ticket_id;
		var ticket_id = "ticket_id";
		var url = 'frm_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.frm_details = result.response.data; //bind result in this variable
				this.ShowService = true;
				
				this.modalService.open(frm_required, {
					size: 'lg',
					// windowClass: "center-modalsm"                                    // modal popup for resizing the popup
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
				// this.chRef.detectChanges();
			}
		}, (err) => {
		
			console.log(err);                                        //prints if it encounters an error
		});

	}
	getFRM(data) {
		// this.overlayRef.attach(this.LoaderComponentPortal); 
		var url = 'frm_reports/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.frm_reports = result.response.data;
				this.showTable = true;
				this.loadingSub = false;
				this.chRef.detectChanges();
				this.dtTrigger.next();
				// this.overlayRef.detach();
			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.loadingSub = false;
				// this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			this.loadingSub = false;
			// this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getdataFRM() {

		var url = 'frm_data/';                                  // api url for getting the cities
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.frm_data = result.response.data;

			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',                                                       //specifies the size of the modal box
					windowClass: "center-modalsm",      //modal popup custom size     
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');            //toastr message for error
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal"                                  //specifies the size of the dialog box
		});
	}
	openmodal(modal) {
		this.modalService.open(modal, {
			size: 'lg',
			windowClass: "center-modalsm"                                    // modal popup for resizing the popup
		});
	}
	// function for getting the Technician details based on TechnicianId 
	get_technician_details(ticket_id, technician_id, view_techniciandetails) {
		var ticket_id = ticket_id;
		var technician_id = technician_id;
		var url = 'get_technician_details/?';             // api url for getting the details with using get params
		var id = "ticket_id";
		var id1 = "technician_id";
		this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technicians_details = result.response.data;
				// console.log(this.technicians_details);
				this.modalService.open(view_techniciandetails, {
					size: 'lg',                                                       //specifies the size of the modal box
					windowClass: "center-modalsm",      //modal popup custom size     
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.message, 'Error');            //toastr message for error
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
}
