import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsubscriptionComponent } from './newsubscription.component';

describe('NewsubscriptionComponent', () => {
  let component: NewsubscriptionComponent;
  let fixture: ComponentFixture<NewsubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
