import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
@Component({
  selector: 'kt-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  // Declare form group of create new
  AddModal: FormGroup;
  // Declare form group of update Exsisting
  EditModal: FormGroup;
  //for showing error message
  error: any;
  // array for product details
  product_details: any;
  //array for subproduct details
  subproduct: any;
  //array for  warranty
  amc_details: any;
  //array for modal from response
  modal: any;
  //boolean for datatable
  showTables: boolean = false;
  loadingAdd: boolean = false;
  type: string;
  Filename: string;
  ByDefault: boolean;
  loading: boolean;
  bulk: any;
  file_name: any;
  error_type: string;
  overlayRef: OverlayRef;  
  loadingEdit: boolean = false;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;  //loader component
  constructor(public ajax: ajaxservice, private toastr: ToastrService,  private overlay: Overlay, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) {
    //parameters for create modal
    this.AddModal = new FormGroup({
      "product_id": new FormControl("", [Validators.required,]),
      "product_sub_id": new FormControl("", [Validators.required,]),
      "warranty_id": new FormControl("", [Validators.required,]),
      "model": new FormControl("", [Validators.required, this.noWhitespace]),
      "voltage": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
      "sys_ah": new FormControl("", [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
      "product_code": new FormControl('', [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),

    })
  }
  //reduct empty space
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  //error logs for create formgroup
  get log() {
    return this.AddModal.controls;
  }
  //error logs for update form group
  get logedit() {
    return this.EditModal.controls;
  }
  //single time reload
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.getmodal();
  }
  //for open popup
  openLarge(content6) {
    this.ByDefault=false;
    this.bulk = '';
    this.error = '';
    this.loadingAdd = false;
    this.error = '';
    //refresh the create form
    this.AddModal.reset();
    //call product api
    this.getproduct();
    //call warranty api
    this.getamc();
    //Call service 
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  // action edit button click time call this function
  editmodalmodal(item, edit) {
    console.log(item);
    //set the values in edit modal form
    this.EditModal = new FormGroup({
      "model_id": new FormControl(item.model_id, [Validators.required,]),
      "product_id": new FormControl(item.product.product_id, [Validators.required,]),
      "product_sub_id": new FormControl(item.product_sub.product_sub_id, [Validators.required,]),
      "warranty_id": new FormControl(item.warranty_type.warranty_id, [Validators.required,]),
      "model": new FormControl(item.model, [Validators.required, this.noWhitespace]),
      "voltage": new FormControl(item.voltage, [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
      "sys_ah": new FormControl(item.sys_ah, [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),
      "product_code": new FormControl(item.product_code, [Validators.required, this.noWhitespace, Validators.pattern("^[A-Za-z 0-9]{1,32}$")]),

    });
    this.onChangeProduct(item.product.product_id)
    this.loadingAdd = false;
    //call open modal service
    this.modalService.open(edit, {
      size: 'lg',
      windowClass: "center-modalsm",                                                      //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
    //call product api
    this.getproduct();
    //call warranty api
    this.getamc();
  }
  //For get all product  in array
  getproduct() {
    var url = 'get_product_details/';
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                    //if sucess
        this.product_details = result.response.data;
        this.chRef.detectChanges();                    //storing the api response in the array
      }
    }, (err) => {                                                      //if error
      console.log(err);                                                 //prints if it encounters an error
    })
  }
  onChangeProductEdit(product_id){
    var product = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
    this.EditModal.controls["product_sub_id"].setValue('');
  }
  //for get subproduct in array based on product
  onChangeProduct(product_id) {
    var product = +product_id; // country: number                                    
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;
      }
    }, (err) => {
      console.log(err);                                    //prints if it encounters an error
    });
    if(this.AddModal){
      this.AddModal.controls["product_sub_id"].setValue('');
    }
 
  }
  //for get warranty details in array
  getamc() {
    var url = 'get_amc_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") //if sucess
      {
        this.amc_details = result.response.data; //storing the api response in the array


      }
      else if (result.response.response_code == "500") {
        this.error = result.response.message;
      }


    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  //get all modal details in array
  getmodal() {
    var data = {};// storing the form group value
    var url = 'get_all_models/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.modal = result.response.data;
        this.showTables = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });
  }
  //create model newly based on addmodel form group
  addmodal() {
    this.loadingAdd = true;
    var data = this.AddModal.value;// storing the form group value
    var url = 'create_model/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
       
        //reset the datatable before filling new entrt
        this.showTables = false;
        this.chRef.detectChanges();
        this.getmodal();
        this.modalService.dismissAll();
        this.toastr.success(result.response.message, 'Sucess');
        this.loadingAdd = false;
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingAdd = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingAdd = false;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
      this.loadingAdd = false;
    });
  }
  editmodel() {
    this.loadingEdit = true;
    var data = this.EditModal.value;// storing the form group value
    var url = 'edit_model/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
       
        this.showTables = false;
        this.chRef.detectChanges();
        this.getmodal();
        this.modalService.dismissAll();
        this.toastr.success(result.response.message, 'Sucess');
        this.loadingEdit = false;
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingEdit = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingEdit = false;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
      this.loadingEdit = false;
    });
  }
  detele(item) {
    console.log(item)
    var data = { "model_id": item };
    var url = 'delete_model/'; // api url for delete warehouse
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        console.log(data)
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showTables = false;
            this.chRef.detectChanges();
            this.getmodal();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
//functions for bulk upload
fileUpload(event) {
  this.type = '';
  // this.Filename = '';
  this.file_name = '';
  this.bulk =[];
  //read the file
  this.error = "";
  let fileList: FileList = event.target.files;
  this.file_name = fileList;
  this.Filename = event.target.files[0].name;
  this.type = event.target.files[0].type;
  this.ByDefault = true;
};
filesubmit(bulkupload) {
  this.bulk = [];
  // console.log(this.file_name)
  var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
  if (this.file_name == undefined || this.file_name == '') {
    // this.toastr.error("Please select file", 'Error');
    this.error_type = "Please select file";
  }
  else if (this.file_name.length > 0) {                   //if file is not empty
    if (validExts[0] != this.type) {
      // this.toastr.error("Please select correct extension", 'Error');
      this.error_type = "Please select correct extension";
    }
    else if (validExts[0] = this.type) {
      this.loading = true;
      let file: File = this.file_name[0];
      const formData: FormData = new FormData();         //convert the formdata
      formData.append('excel_file', file);                     //append the name of excel file
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
      var gh = file;
      var method = "post";                                    //post method 
      var url = 'model_bulk_upload/'                         //post url
      var i: number;                                             //i is the data
      this.ajax.ajaxpost(formData, method, url).subscribe(data => {
        if (data['response_code'] == "200") {
          this.error_type = '';
         
          if (data['response'].fail.length > 0) {                                    //failure response 
            //if employee code or employee id is error to put this
            for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
              var a = [];
              data['response'].fail.forEach(value => {
                a.push(value.error_message
                );
              });

              this.bulk = a;
              // this.file_name = '';
              // this.ByDefault = false;
            }
          }
          //success response
          //if newly add product for excisting employee code or employee id
          if (data['success'].length > 0) {                         //sucess response
            for (i = 0; i < data['success'].length; i++) {
              // alert(data['success'][i])
              // this.bulk = '';
              this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
              this.showTables = false;
              this.chRef.detectChanges();
              this.getmodal();

            }
          }
          this.loading = false;
        }

        //if other errors
        else if (data['response'].response_code == "500") {
          this.bulk = [];
          // this.file_name = '';
          // this.ByDefault = false;
          this.loading = false;
          this.error_type = data['response'].message
        }
        else {
          this.bulk = [];
          // this.file_name = '';
          // this.ByDefault = false;
          this.loading = false;
          this.error_type = "Something Went Wrong";                                                                  //if not sucess
        }
      },

        (err) => {
          this.bulk = [];
          // this.file_name = '';
          // this.ByDefault = false;                                                                 //if error
          console.log(err);                                                 //prints if it encounters an error
        }
      );
    }
  }

};




}
