import { TestBed } from '@angular/core/testing';

import { ReimbursmentformService } from './reimbursmentform.service';

describe('ReimbursmentformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReimbursmentformService = TestBed.get(ReimbursmentformService);
    expect(service).toBeTruthy();
  });
});
