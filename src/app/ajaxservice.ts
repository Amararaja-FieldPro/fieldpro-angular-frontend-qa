import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
/* import 'rxjs/add/Observable/throw'; */
import { environment } from '../environments/environment';
import { Http, ResponseContentType } from '@angular/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
const httpOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/pdf'
  })
};
@Injectable()
export class ajaxservice {
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  constructor(private http: HttpClient) { }

  getdata(url): Observable<any> {
    return this.http.get(environment.api_url + url).pipe(
      map(this.extractData));
  }
  get(url, data): Observable<any> {
    console.log(data);
    // console.log("JSON.stringify(data)"+(data));
    return this.http.get<any>(environment.api_url + url, data).pipe(
      tap((data) => console.log(`get product w/ id=${data}`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }
  getdataparam(url, id, value): Observable<any> {
    return this.http.get(environment.api_url + url + id + "=" + value).pipe(
      map(this.extractData));
  }

  getPdfDocument(url, id, value): any {
    let headers = new HttpHeaders({ 'Content-Type': 'application/arraybuffer' });
    return this.http
      .get(environment.api_url + url + id + "=" + value,
        { headers: headers, responseType: 'blob' as 'arraybuffer', observe: 'response' as 'body' }
      );     
  }

  getdatalocation(url, id, value, node, node_id): Observable<any> {
    return this.http.get(environment.api_url + url + id + "=" + value + "&" + node_id + "=" + node).pipe(
      map(this.extractData));
  }
  getdatareimbursement(url, id, value, node, node_id,start_date,start,end_date,end): Observable<any> {
    return this.http.get(environment.api_url + url + id + "=" + value + "&" + node + "=" + node_id+ "&" + start_date + "=" + start + "&" + end_date + "=" + end ).pipe(
      map(this.extractData));
  }
  // getdatalocation(url,id,value,id1,value1): Observable<any> {
  //   return this.http.get(environment.api_url + url +id+"="+value+"&"+id1+"="+value1,httpOptions).pipe(
  //   map(this.extractData));
  // }    
  postdata(url, data): Observable<any> {
    console.log(data);
    console.log("JSON.stringify(data)" + (data));
    return this.http.post<any>(environment.api_url + url, JSON.stringify(data), httpOptions).pipe(
      tap((data) => console.log(`added product w/ id=${data}`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }

  postdata1(url, data): Promise<any> {
    console.log(data);
    console.log("JSON.stringify(data)" + (data));
    
    // return this.http.post<any>(environment.api_url + url, JSON.stringify(data), httpOptions).pipe(
    //   tap((data) => console.log(`added product w/ id=${data}`)),
    //   catchError(this.handleError<any>('addProduct'))
    // );

    return this.http.post<any>(environment.api_url + url, JSON.stringify(data), httpOptions).toPromise()

  }

  public ajaxpost(data, methods, url) {
    var obj = {
      methods: methods,
      submitUrl: url,
      data: data,
      dataType: "JSON"
    }
    return this.http.post(environment.api_url + obj.submitUrl, obj.data)
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}