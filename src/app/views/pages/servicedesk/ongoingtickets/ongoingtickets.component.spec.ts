import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OngoingticketsComponent } from './ongoingtickets.component';

describe('OngoingticketsComponent', () => {
  let component: OngoingticketsComponent;
  let fixture: ComponentFixture<OngoingticketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngoingticketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OngoingticketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
