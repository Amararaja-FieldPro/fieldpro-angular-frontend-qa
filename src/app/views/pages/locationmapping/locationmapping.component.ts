// import { State } from './../material/layout/default-forms/default-forms.component';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import { IntegralUITreeView } from '@lidorsystems/integralui-web/bin/integralui/components/integralui.treeview';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'kt-locationmapping',
  templateUrl: './locationmapping.component.html',
  styleUrls: ['./locationmapping.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class LocationmappingComponent implements OnInit {

  Edit_node: FormGroup;
  edit_data: any;
  getNode: any;
  getorgnode: any;
  states: number;
  country: number;
  cities: number;
  // editProject:this.getNode=this.getdata();
  editIndex: number = null;
  value: any;
  locations: any;
  ngOnInit() {
    this.getdata();
    this.getlocationdata();
    this.getcountry();
    this.getlocationstructure();

  }
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private toastr: ToastrService, public activeModal: NgbActiveModal, private http: Http) {

  }
  //--------------------------------------------------------------------------------------------------//
  //get country dropdown
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  getdata() {
    // var url = 'get_org_structure/';          // api url for getting the details
    var url = 'get_org_str_data/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getNode = result.response.nodes;
        console.log(this.getNode);
      }
    });
  }
  getlocationdata() {
    var url = 'get_location/';          // api url for getting the details
    // var url = 'get_org_str_data/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getorgnode = result.response.nodes;
        console.log(this.getorgnode);
      }
    });
  }
  getlocationstructure() {
    var url = 'get_org_structure/';          // api url for getting the details
    // var url = 'get_org_str_data/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getorgnode = result.response.nodes;
        console.log(this.getorgnode);
      }
    });
  }
  onEditClick(event, index: number) {
    this.getNode.id = this.getNode[index].id;
    this.getNode.node = this.getNode[index].node;

    this.editIndex = index;
  }
  // function for modal boxes
  openLarge(content6) {
    this.modalService.open(content6)
    }

  onChangeCountry(country_id) {
    var country = +country_id; // country: number        
    console.log(country)
    var url = 'get_location/?';             // api url for getting the details with using post params
    var id = "country_id";
    var node_id = "node_id";
    var node = +this.edit_data.id;
    console.log(url, id, country, node);
    this.ajax.getdatalocation(url, id, country, node, node_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;
        console.log(this.states)                                                   //reloading the component
        this.Edit_node.controls['state_id'].setValue(this.edit_data.state.state_id);     //view state name in view modal box
           }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    // }
  }
  onChangestate(state_id) {
    console.log(state_id)
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_location/?';
    var node_id = "node_id";
    var node = +this.edit_data.id;                                  // api url for getting the cities
    this.ajax.getdatalocation(url, id, state, node, node_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                   
        console.log(this.cities);
        this.Edit_node.controls['city_id'].setValue(this.edit_data.cities.city_id);             //view city name in view modal box
        this.Edit_node.controls['city_id'].setValue(this.edit_data.cities.city_id);             //view city name in edit modal box
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  onChangecity(cty_id) {
    console.log(cty_id)
    var city_id = +cty_id; // state: number
    var id = "city_id";
    var url = 'get_location/?';
    var node_id = "node_id";
    var node = +this.edit_data.id;                                  // api url for getting the cities
    this.ajax.getdatalocation(url, id, city_id, node, node_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array                   
        console.log(this.cities);
        this.Edit_node.controls['location_id'].setValue(this.edit_data.location_id);             //view city name in view modal box
        this.Edit_node.controls['location_id'].setValue(this.edit_data.location_id);             //view city name in edit modal box
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }

  //Function to call the edit api 
  editform(edit_data) {
    var url = 'update_location/'
    var data = this.Edit_node.value;
    //console.log(data);  
    if (data.state_id == null) {
      data.state_id = "";
    }
    if (data.city_id == null) {
      data.city_id = "";
    }
    if (data.location_id == null) {
      data.location_id = "";
    }
    this.value = {
      "node_id": data.id,
      "country_id": data.country_id,
      "state_id": data.state_id,
      "city_id": data.city_id,
      "location_id":data.location_id
    };
    // this.value={"node_id" : edit_data.id,"country_id" :data.country_id,"state_id" : data.state_id,"city_id" :data.city_id };
    console.log(this.value);                                  //api url of edit api                                    
    this.ajax.postdata(url, this.value).subscribe((result) => {
      console.log(result);
      if (result.response.response_code == "200")                           //if sucess
      {
        this.ngOnInit();                                                //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
        this.modalService.dismissAll();                                 //to close the modal box
      }
      else if (result.response.response_code == "400") {                  //if failure
        this.toastr.error(result.response.message, 'Error');            //toastr message for error
      }
      else {                                                                     //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {                                                           //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  test(data) {
    this.edit_data = data;
    console.log(data);
      this.Edit_node = new FormGroup({
      "id": new FormControl(this.edit_data.id, Validators.required),
      "country_id": new FormControl(this.edit_data.country, Validators.required),
      "parent_id": new FormControl(this.edit_data.parent_id),
      "state_id": new FormControl(this.edit_data.state),
      "city_id": new FormControl(this.edit_data.city),
     "level": new FormControl(this.edit_data.level),
    });
    if (this.edit_data.level == '1') {
      console.log(this.edit_data.country);
      this.onChangeCountry(this.edit_data.country);
    }
    else if (this.edit_data.level == '2') {
      console.log(this.edit_data.state);
        var country_id = this.edit_data.country;
        var country = +country_id; // state: number
        var id = "country_id";
        var url = 'get_location/?';
        var node_id = "node_id";
        var node = +this.edit_data.id;                                  // api url for getting the cities
        this.ajax.getdatalocation(url, id, country, node_id, node).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.states = result.response.data;              //storing the api response in the array                   
            console.log( this.states," this.states")
            this.Edit_node.controls['state_id'].setValue(this.edit_data.state);             //view city name in view modal box
          }

        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
       
    }
    else if (this.edit_data.level == '3') {
      console.log(this.edit_data.state);
        var stateid = this.edit_data.state;
        var state = +stateid; // state: number
        var id = "state_id";
        var url = 'get_location/?';
        var node_id = "node_id";
        var node = +this.edit_data.id;                                  // api url for getting the cities
        this.ajax.getdatalocation(url, id, state,node_id, node).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.cities = result.response.data;              //storing the api response in the array                   
            this.Edit_node.controls['city_id'].setValue(this.edit_data.city);             //view city name in view modal box
          }

        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
       
    }
    else if (this.edit_data.level == '4') {
      console.log(this.edit_data.city);
        var cityid = this.edit_data.city;
        var city = +cityid; // state: number
        var id = "city_id";
        var url = 'get_location/?';
        var node_id = "node_id";
        var node = +this.edit_data.id;                                  // api url for getting the cities
        this.ajax.getdatalocation(url, id, city, node_id, node).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.locations = result.response.data;              //storing the api response in the array                   
            this.Edit_node.controls['location_id'].setValue(this.edit_data.location);             //view city name in view modal box
          }

        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
       
    }
   
  }

}
