// Angular
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
// NGRX
import { select, Store } from '@ngrx/store';
// Auth reducers and selectors
import { AppState } from '../../../core/reducers/';
import { isLoggedIn } from '../_selectors/auth.selectors';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store<AppState>, private router: Router) { }

    // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
    //     return this.store
    //         .pipe(
    //             select(isLoggedIn),
    //             tap(loggedIn => {
    //                 if (!loggedIn) {
    //                  //  this.router.navigateByUrl('/auth/login/dashboard');
    //                 }
    //             })
    //         );
    // }
    // CanActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    //     let employee_code=localStorage.getItem('employee_code');
    //     console.log(employee_code,"employee code") 
    //     if(employee_code!="" && employee_code!=null)
    //     {
    //        // console.log('true')
    //         return true;
    //     }
    //     else
    //     {
    //        // console.log('false')
    //        this.router.navigateByUrl('auth/login');
    //        // return false;
    //     }
    // }




    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let employee_code = localStorage.getItem('employee_code');
        let role_name = localStorage.getItem('user_role')
        let customer_code = localStorage.getItem('customer_code')
        // console.log(role_name, "rolenmae")
        // console.log(employee_code)
        if (role_name == "Customer") {
            if (customer_code != "" && customer_code != null) {
                // logged in so return true
                return true;
            }
            this.router.navigateByUrl('customer/customer_login');
            return false;
        }
        else {
            // const currentUser = this.authenticationService.currentUserValue;
            if (employee_code != "" && employee_code != null) {
                // logged in so return true
                return true;
            }

            // not logged in so redirect to login page with the return url
            // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            this.router.navigateByUrl('login');
            return false;
        }

    }














}


// Angular
// import { Injectable } from '@angular/core';
// import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// // RxJS
// import { Observable } from 'rxjs';
// import { tap } from 'rxjs/operators';
// // NGRX
// import { select, Store } from '@ngrx/store';
// // Auth reducers and selectors
// import { AppState} from '../../../core/reducers/';
// import { isLoggedIn } from '../_selectors/auth.selectors';

// @Injectable()
// export class AuthGuard implements CanActivate {
//     constructor(private store: Store<AppState>, private router: Router) { }

//   /*  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
//         return this.store
//             .pipe(
//                 select(isLoggedIn),
//                 tap(loggedIn => {
//                     if (!loggedIn) {
//                         this.router.navigateByUrl('/auth/login');
//                     }
//                 })
//             );
//     }*/
//     CanActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
//         let user_email=localStorage.getItem('email');
//         if(user_email!="" && user_email!=null)
//         {
//            // console.log('true')
//             return true;
//         }
//         else
//         {
//            // console.log('false')
//            this.router.navigateByUrl('auth/login');
//            // return false;
//         }
//     }
// }