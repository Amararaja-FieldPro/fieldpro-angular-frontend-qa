import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardServicedeskComponent } from './dashboard-servicedesk.component';

describe('DashboardServicedeskComponent', () => {
  let component: DashboardServicedeskComponent;
  let fixture: ComponentFixture<DashboardServicedeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardServicedeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardServicedeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
