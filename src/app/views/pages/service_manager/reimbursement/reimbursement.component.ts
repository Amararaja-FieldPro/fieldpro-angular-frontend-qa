import { Component, OnInit, Input, Output, EventEmitter, Injectable, ChangeDetectorRef, ViewChild } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import {ReimbursmentformComponent} from '../reimbursmentform/reimbursmentform.component';
// import {ReimbursmentformService} from '../reimbursmentform/reimbursmentform.service';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ExcelService } from './../../../../excelservice';
@Component({
	selector: 'kt-reimbursement',
	templateUrl: './reimbursement.component.html',
	styleUrls: ['./reimbursement.component.scss']
})
@Injectable()

export class ReimbursementComponent implements OnInit {
	dtOptions1: DataTables.Settings = {};
	reimbursement: any;
	employee_code: any;
	reimbursment_data: any;
	reimbursement_view_form: any;
	val: any;
	technician_data: any;
	techId: any;
	accepted_reimbursment: any;
	rejected_reimbursment: any;
	data: any = {};
	currentJustify = 'end';
	reimbursement_view_detaildata: any;
	ShowtableDate: boolean = false;
	showtableReject: boolean = false;
	showtableApprove: boolean = false;
	tech: any;
	start: any;
	end: any;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	showTable: boolean = false;
	reimbusement_data_details: any;
	showmodelTable: boolean;
	export_down: number;
	// public idval:[];

	constructor(private excelservice: ExcelService, private ajax: ajaxservice, private chRef: ChangeDetectorRef, private modalService: NgbModal, private toastr: ToastrService, private overlay: Overlay
	) {
		this.dtOptions1 = {
			pagingType: 'full_numbers',
			pageLength: 5,
			lengthMenu: [5, 10, 15]
		};
	}

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.overlayRef.attach(this.LoaderComponentPortal);
		this.getReimbursment();
		// this.getAcceptedReimbursment();
		// this.getRejectedReimbursment();

	}
	exportAsXLSX(): void {
		this.export_down = 1;
		this.get_reimbursement();

	}
	exportAsXLSX_ticket(): void {
		this.export_down = 2;
		this.get_reimbursement();

	}
	get_reimbursement() {
		// this.loadingSub = false;
		// this.overlayRef.attach(this.LoaderComponentPortal);                          // intialising datatable
		var url = 'reimbursement_down/?';                                   // api url for getting the details
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		if (this.export_down == 2) {
			var technician_code = "technician_code";
			var start_date = "start_date";
			var end_date = "end_date";
			var tech_code = this.tech; // get technician code 
			this.ajax.getdatareimbursement(url, employee_code, emp_code,  technician_code,tech_code,start_date,this.start,end_date,this.end).subscribe((result) => {
				if (result.response.response_code == "200" || result.response.response_code == "400") {
					this.reimbusement_data_details = result.response.data;                    //storing the api response in the array
					console.log(this.reimbusement_data_details, "hhj")
					this.excelservice.exportAsExcelFile(this.reimbusement_data_details, 'Reimbursement');
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();

				}
				else if (result.response.response_code == "500") {
					this.toastr.error(result.response.message, "Error");
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();
				}
				else {
					this.toastr.error("Something Went Wrong", "Error");
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();
				}


			}, (err) => {
				this.showmodelTable = true;
				console.log(err);                                                 //prints if it encounters an error
			});
		}
		else {
			this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
				if (result.response.response_code == "200" || result.response.response_code == "400") {
					this.reimbusement_data_details = result.response.data;                    //storing the api response in the array
					console.log(this.reimbusement_data_details, "hhj")
					this.excelservice.exportAsExcelFile(this.reimbusement_data_details, 'Reimbursement');
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();

				}
				else if (result.response.response_code == "500") {
					this.toastr.error(result.response.message, "Error");
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();
				}
				else {
					this.toastr.error("Something Went Wrong", "Error");
					this.showmodelTable = true;
					this.chRef.detectChanges();
					// this.overlayRef.detach();
				}


			}, (err) => {
				this.showmodelTable = true;
				console.log(err);                                                 //prints if it encounters an error
			});
		}
		// var url = 'reimbursment/?';                                  // api url for getting the cities

	}
	getReimbursment() {

		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'reimbursment/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.reimbursment_data = result.response.data;
				this.ShowtableDate = true;
				this.overlayRef.detach();  //detaching the loader after response
				this.chRef.detectChanges();

			}
			else if (result.response.response_code == "500") {
				this.ShowtableDate = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getAcceptedReimbursment() {

		this.overlayRef.attach(this.LoaderComponentPortal);  //adding loader oncliking the tab

		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'reimbursment_approve_details/?';                                  // api url for getting the Spare transfer approve details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.accepted_reimbursment = result.response.data;
				this.showtableApprove = true;
				this.overlayRef.detach();  //detaching the loader after response
				this.chRef.detectChanges();

			}
			else if (result.response.response_code == "500") {
				this.showtableApprove = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
			}

		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getRejectedReimbursment() {
		this.overlayRef.attach(this.LoaderComponentPortal);  //adding loader oncliking the tab

		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'reimbursment_reject_details/?';                                  // api url for getting the reimbursment reject details
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.rejected_reimbursment = result.response.data;
				this.showtableReject = true;
				this.overlayRef.detach();  //detaching the loader after response
				this.chRef.detectChanges();

			}
			else if (result.response.response_code == "500") {
				this.showtableReject = true;
				this.overlayRef.detach();
			}

			else {
				this.toastr.error(result.response.message, 'Error');
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}
	reimbursmentApprove(reimburse_request_id) {
		var url = 'reimbursment_aprove/'; //API url for reimbursment approve
		var data = {//params for API
			"reimburse_request_id": reimburse_request_id
		}
		Swal.fire({ //sweet alert for accptance
			title: 'Are you sure?',
			text: "Reimbursement accepted Successfully !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Accept',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {                          //if sucess
						this.ngOnInit();                                                //reloading the component
						this.toastr.success(result.response.message, 'Success');        //toastr message for success

					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	reimbursmenteReject(reimburse_request_id) {
		var url = 'reimbursment_reject/';
		var data = {
			"reimburse_request_id": reimburse_request_id
		}
		Swal.fire({//swwet alert for reject or cancel
			title: 'Are you sure?',
			text: "You Want to Reject this Claim !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Reject',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {                          //if sucess
						this.ngOnInit();                                                //reloading the component
						this.toastr.success(result.response.message, 'Success');        //toastr message for success

					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}
	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
		if (evt.nextId == "tab-selectbyid1") {
			this.ShowtableDate = false;
			this.getReimbursment();
		}
		else if (evt.nextId == "tab-selectbyid2") {
			this.showTable = false;
			this.chRef.detectChanges();
			this.showtableApprove = false;
			this.getAcceptedReimbursment();
		}
		else if (evt.nextId == "tab-selectbyid3") {
			this.showtableReject = false;
			this.getRejectedReimbursment();

		}
	}
	viewForm(tech, start, end) { //get Spare Details
		this.showTable = false;
		this.chRef.detectChanges();
		this.tech = tech;
		this.start = start;
		this.end = end;
		var url = 'reimbursment_details/'; //API url for View Form
		var data = {//params for API
			"technician_id": this.tech,
			"start_date": this.start,
			"end_date": this.end
		}
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.reimbursement_view_form = result.response.data; //bind result in this variable
				this.reimbursement_view_detaildata = result.response.details_data;
				this.showTable = true;
				this.chRef.detectChanges();

			}
			else if (result.response.response_code == "500") {
				this.showTable = true;
				this.chRef.detectChanges();

			}
			else {
				this.showTable = true;
				this.chRef.detectChanges();
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	techDetails(techId, tech_details) {
		this.techId = techId;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the technician
		this.ajax.getdataparam(url, technician_code, techId).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_data = result.response.data; //bind results in this variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modalsm"                                  //specifies the size of the dialog box
		});
	}
	openspare(add) { //Modal popup
		this.modalService.open(add, {
			// size: 'lg',
			backdrop: 'static',                                    // modal will not close by outside click
			keyboard: false,                                       // modal will not close by keyboard click
			windowClass: "center-modalextralg"                                  //specifies the size of the dialog box

		});
	}
}
