// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { BaseComponent } from './views/theme/base/base.component';
import { ErrorPageComponent } from './views/theme/content/error-page/error-page.component';
// Auth
import { AuthGuard } from './core/auth';
import { NewsubscriptionComponent } from './views/pages/newsubscription/newsubscription.component';
import { BillingDetailComponent } from './views/pages/billing-detail/billing-detail.component';
import { SlaComponent } from './views/pages/service_manager/sla/sla.component';
import { ProductmasterComponent } from './views/pages/productmaster/productmaster.component';
// import { ManagementComponent } from './views/pages/management/management.component';
// import {ConfigurationComponent} from './views/pages/configuration/configuration.component';
import { LoginComponent } from './views/pages/auth/login/login.component';
import { DynamicformComponent } from './views/pages/dynamicform/dynamicform.component';
import { PackagetypeComponent } from './views/pages/packagetype/packagetype.component';
import { AmcComponent } from './views/pages/amc/amc.component';
import { ServicegroupComponent } from './views/pages/servicegroup/servicegroup.component';
import { WarehouseComponent } from './views/pages/warehouse/warehouse.component';
import { SparemasterComponent } from './views/pages/sparemaster/sparemaster.component';
import { SpareComponent } from './views/pages/spare/spare.component';
import { SitesettingsComponent } from './views/pages/sitesettings/sitesettings.component';
import { SmtpsettingsComponent } from './views/pages/smtpsettings/smtpsettings.component';
import { SmsComponent } from './views/pages/sms/sms.component';
import { ContentmanagementComponent } from './views/pages/contentmanagement/contentmanagement.component';
import { CustomermanagemantComponent } from './views/pages/customermanagemant/customermanagemant.component';
import { UsermanagementComponent } from './views/pages/usermanagement/usermanagement.component';
// import { Wizard2Component } from './views/pages/wizard/wizard2/wizard2.component';
import { LocationmappingComponent } from './views/pages/locationmapping/locationmapping.component';
import { OrgstructureComponent } from './views/pages/orgstructure/orgstructure.component';
import { RaiseticketComponent } from './views/pages/servicedesk/raiseticket/raiseticket.component';
import { NewticketsComponent } from './views/pages/servicedesk/newtickets/newtickets.component';
import { ConfigurationComponent } from './views/pages/configuration/configuration.component';
import { OngoingticketsComponent } from './views/pages/servicedesk/ongoingtickets/ongoingtickets.component';
import { CompletedticketsComponent } from './views/pages/servicedesk/completedtickets/completedtickets.component';
import { LocationComponent } from './views/pages/location/location.component';
import { MaindashboardComponent } from './views/pages/maindashboard/maindashboard.component';
import { ProfileComponent } from './views/pages/profile/profile.component';
// import { GetticketComponent } from './views/pages/getticket/getticket.component';
import { AuthModule } from './views/pages/auth/auth.module';
import { ChangepasswordComponent } from './views/pages/changepassword/changepassword.component';
import { LeaderboardComponent } from './views/pages/service_manager/leaderboard/leaderboard.component';
import { SparerequestComponent } from './views/pages/service_manager/sparerequest/sparerequest.component';
import { ManagercontractComponent } from './views/pages/managercontract/managercontract.component';
import { TrainingscoreComponent } from './views/pages/service_manager/trainingscore/trainingscore.component';
import { ReimbursementComponent } from './views/pages/service_manager/reimbursement/reimbursement.component';
import { ReportsComponent } from './views/pages/reports/reports.component';
import { ImprestspareComponent } from './views/pages/service_manager/imprestspare/imprestspare.component';
import { TransferspareComponent } from './views/pages/transferspare/transferspare.component';
import { ReimbursmentformComponent } from './views/pages/reimbursmentform/reimbursmentform.component';
import { PerformancematrixComponent } from './views/pages/service_manager/performancematrix/performancematrix.component';
import { NewTicketsComponent } from './views/pages/service_manager/new-tickets/new-tickets.component';
import { OngoingTicketsComponent } from './views/pages/service_manager/ongoing-tickets/ongoing-tickets.component';
import { CompletedTicketsComponent } from './views/pages/service_manager/completed-tickets/completed-tickets.component';
import { TechnicianTrackingComponent } from './views/pages/service_manager/technician-tracking/technician-tracking.component';
import { SpareReportComponent } from './views/pages/service_manager/spare-report/spare-report.component';
import { SummaryReportComponent } from './views/pages/service_manager/summary-report/summary-report.component';
import { FRMReportComponent } from './views/pages/service_manager/frm-report/frm-report.component';
import { TechnicianReportComponent } from './views/pages/service_manager/technician-report/technician-report.component';
import { CallReportComponent } from './views/pages/service_manager/call-report/call-report.component';
import { AttendanceReportComponent } from './views/pages/service_manager/attendance-report/attendance-report.component';
import { RevenueComponent } from './views/pages/service_manager/revenue/revenue.component';
import { ProductivityComponent } from './views/pages/service_manager/productivity/productivity.component';
import { SatComponent } from './views/pages/service_manager/sat/sat.component';

import { SlamappingComponent } from './views/pages/slamapping/slamapping.component';
import { ServicesparerequestComponent } from './views/pages/servicedesk/servicesparerequest/servicesparerequest.component';
import { AmcContractComponent } from './views/pages/servicedesk/amc-contract/amc-contract.component';
import { KnowledgebaseComponent } from './views/pages/servicedesk/knowledgebase/knowledgebase.component';
import { LiveTrackingComponent } from './views/pages/service_manager/live-tracking/live-tracking.component';
import { ManagerDashboardComponent } from './views/pages/service_manager/manager-dashboard/manager-dashboard.component';
import { DashboardServicedeskComponent } from './views/pages/servicedesk/dashboard-servicedesk/dashboard-servicedesk.component';
import { ManageSubscriptionComponent } from './views/pages/sass_admin/manage-subscription/manage-subscription.component';
import { PackagePlanComponent } from './views/pages/sass_admin/package-plan/package-plan.component';
import { SpareMasterComponent } from './views/pages/spare-master/spare-master.component';
import { CustomerTrackingComponent } from './views/pages/customer-tracking/customer-tracking.component';
import { SegmentComponent } from './views/pages/segment/segment.component';
import { ApplicationComponent } from './views/pages/application/application.component';
import { ModalComponent } from './views/pages/modal/modal.component';
import { PopupsComponent } from './views/pages/popups/popups.component';
import { RaiseComponent } from './views/pages/service_manager/raise/raise.component';
import { CompensationComponent } from './views/pages/compensation/compensation.component';
import { CustomerLoginComponent } from './views/pages/auth/customer-login/customer-login.component';
import { CustomerRaisecallComponent } from './views/pages/customer/customer-raisecall/customer-raisecall.component';
import { AuthComponent } from '../app/views/pages/auth/auth.component';
import { CustomerfeedbackComponent } from '../app/views/pages/auth/customerfeedback/customerfeedback.component';
import { MomComponent } from '../app/views/pages/service_manager/mom/mom.component';
import { TatTrendComponent } from '../app/views/pages/Reports/tat-trend/tat-trend.component';
import { CSLTrendComponent } from '../app/views/pages/Reports/csltrend/csltrend.component';
import { CsiAnalysisComponent } from '../app/views/pages/Reports/csi-analysis/csi-analysis.component';
import { FinancialReportComponent } from '../app/views/pages/Reports/financial-report/financial-report.component';
import { FinanceReplacementComponent } from '../app/views/pages/Reports/finance-replacement/finance-replacement.component'
import { BatteryAgeReportComponent } from '../app/views/pages/Reports/battery-age-report/battery-age-report.component'
import { ReplacementBatteryComponent } from'../app/views/pages/Reports/replacement-battery/replacement-battery.component'
import { FtrreportComponent } from '../app/views/pages/Reports/ftrreport/ftrreport.component';
import { CompetitorreportComponent } from '../app/views/pages/Reports/competitorreport/competitorreport.component';
import { RejectreasonreportComponent }from '../app/views/pages/Reports/rejectreasonreport/rejectreasonreport.component';
import { LocationwisereportComponent } from '../app/views/pages/Reports/locationwisereport/locationwisereport.component';
import { TicketagereportComponent } from '../app/views/pages/Reports/ticketagereport/ticketagereport.component';
import { ComplaintsummeryComponent } from '../app/views/pages/Reports/complaintsummery/complaintsummery.component';
import { PerformanceReportComponent } from '../app/views/pages/Reports/performance-report/performance-report.component';
const routes: Routes = [
	{
		// path: 'customer_feedback/:ticket_id',
		// component: CustomerfeedbackComponent
		path: 'customer',
		component: AuthComponent,
		children: [
			{
				path: 'customer_login',
				component: CustomerLoginComponent,
			},
			{
				path: 'customer_feedback/:ticket_id',
				component: CustomerfeedbackComponent,
			},
		]
	},
	
	// {
	// 	path: 'customer_login',
	// 	component: CustomerLoginComponent,
	// },
	
	{
		// path:'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule)},
		

		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'pop',
				component: PopupsComponent
			},

			{
				path: 'dashboard',
				// component:BillingDetailComponent,
				component: MaindashboardComponent,
			},
			{
				path: 'compensation',
				component: CompensationComponent,
			},
			{
				path: 'transfer_spare',
				component: TransferspareComponent
			},
			{
				path: 'imprest_spare',
				component: ImprestspareComponent,
			},
			{
				path: 'manager/raise',
				component: RaiseComponent,
			},

			{
				path: 'reports',
				component: ReportsComponent,
			},
			{
				path: 'reimbursement',
				component: ReimbursementComponent,
			},
			{
				path: 'reimbursmentform',
				component: ReimbursmentformComponent,
			},
			{
				path: 'spare_request',
				component: SparerequestComponent,
			},
			{
				path: 'training_score',
				component: TrainingscoreComponent,
			},
			{
				path: 'performance_matrix',
				component: PerformancematrixComponent,
			},
			{
				path: 'manager_contract',
				component: ManagercontractComponent
			},
			// {
			// 	path: 'getticket',
			// 	component: GetticketComponent,
			// },
			{
				path: 'leaderboard',
				component: LeaderboardComponent,
			},
			{
				path: 'changepassword',
				component: ChangepasswordComponent,
			},
			{
				path: 'profile',
				component: ProfileComponent,
			},
			// {
			// 	path:'newsubscription',
			// 	component:NewsubscriptionComponent
			// },

			{
				path: 'sms', component: SmsComponent
			},
			{
				path: 'billing-detail', component: BillingDetailComponent
			},
			{
				path: 'sass_admin/managesubscription', component: ManageSubscriptionComponent
			},
			{
				path: 'productmaster', component: ProductmasterComponent
			},
			{
				path: 'sass_admin/packageplan', component: PackagePlanComponent
			},
			{
				path: 'management', component: DynamicformComponent
			},
			// {
			// 	path:'configuration',component:ConfigurationComponent
			// },
			// {
			// 	path: 'subscription', component: SubscriptionComponent
			// },
			{
				path: 'amc', component: AmcComponent
			},
			{
				path: 'servicegroup', component: ServicegroupComponent
			},
			{
				path: 'customermanagement', component: CustomermanagemantComponent
			},
			{
				path: 'content', component: ContentmanagementComponent
			},
			{
				path: 'spare-master', component: SpareMasterComponent
			},
			{
				path: 'spares', component: SpareComponent
			},
			{
				path: 'sitesettings', component: SitesettingsComponent
			},

			{
				path: 'org', component: OrgstructureComponent
			},

			{
				path: 'locationmapping', component: LocationmappingComponent
			},
			{
				path: 'sla', component: SlaComponent
			},
			{
				path: 'sms', component: SmsComponent
			},
			{
				path: 'smtp', component: SmtpsettingsComponent
			},
			{
				path: 'user', component: UsermanagementComponent
			},
			{
				path: 'slamapping', component: SlamappingComponent
			},
			{
				path: 'spares', component: SpareComponent
			},

			{
				path: 'raiseticket', component: RaiseticketComponent
			},
			{
				path: 'newticket', component: NewticketsComponent
			},
			{
				path: 'manager_newticket', component: NewTicketsComponent
			},
			{
				path: 'manager_ongoingticket', component: OngoingTicketsComponent
			},
			{
				path: 'manager_completedtickets', component: CompletedTicketsComponent
			},
			{
				path: 'manager_tracking', component: TechnicianTrackingComponent
			},
			{	
				path: 'call_tracking', component: CustomerTrackingComponent
			},
			{
				path: 'segment', component: SegmentComponent
			},
			{
				path: 'application', component: ApplicationComponent
			},
			{
				path: 'model', component: ModalComponent
			},
			{
				path: 'manager_livetracking', component: LiveTrackingComponent
			},
			{
				path: 'manager_dashboard', component: ManagerDashboardComponent
			},

			{
				path: 'manager_attendance', component: AttendanceReportComponent
			},
			{
				path: 'manager_revenue', component: RevenueComponent
			},
			{
				path: 'manager_sat', component: SatComponent
			},
			{
				path: 'manager_productivity', component: ProductivityComponent
			},
			{
				path: 'manager_call', component: CallReportComponent
			}, {
				path: 'manager_spare', component: SpareReportComponent
			},
			{
				path: 'manager_summary', component: SummaryReportComponent
			},
			{
				path: 'manager_frm', component: FRMReportComponent
			},
			{
				path: 'manager_technicianreport', component: TechnicianReportComponent
			},
			{
				path: 'configuration', component: ConfigurationComponent
			},
			{
				path: 'ongoingticket', component: OngoingticketsComponent
			},
			{
				path: 'knowledgebase', component: KnowledgebaseComponent
			},

			{
				path: 'completedticket', component: CompletedticketsComponent
			},
			{
				path: 'location', component: LocationComponent
			},
			{
				path: 'maindashboard', component: MaindashboardComponent

			},
			{

				path: 'warehouse', component: WarehouseComponent
			},
			{

				path: 'service_spare_request', component: ServicesparerequestComponent
			},
			{

				path: 'service_dashboard', component: DashboardServicedeskComponent
			},
			{
				path: 'amc_contract', component: AmcContractComponent
			},
			{
				path: 'customer_raisecall', component: CustomerRaisecallComponent
			},
			{
				path: 'manager_mom', component: MomComponent
			},
			{
				path: 'tat_trend', component: TatTrendComponent
			},
			{
				path: 'csl_trend', component: CSLTrendComponent
			},
			{
				path:'csi_analyse', component:CsiAnalysisComponent
			},
			{
				path:'complaint_summery',component:ComplaintsummeryComponent
			},
			{
				path: 'finance_report', component: FinancialReportComponent
			},
			{
				path:'finance_replace',component:FinanceReplacementComponent
			},
			{
				path:'battery-age',component:BatteryAgeReportComponent
			},
			{
				path:'battery-replace',component:ReplacementBatteryComponent
			},
			{
				path:'ftr',component:FtrreportComponent
			},
			{
				path:'performance_report',component:PerformanceReportComponent
			},
			{
				path:'competitor',component:CompetitorreportComponent
			},
			{
				path:'reject_reason',component:RejectreasonreportComponent
			},
			{
				path:'locationwise_report',component:LocationwisereportComponent
			},
			{
				path:'ticket_age',component:TicketagereportComponent
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					'type': 'error-v6',
					'code': 403,
					'title': '403... Access forbidden',
					'desc': 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
				}
			},
			{ path: '**', redirectTo: '/login', pathMatch: 'full' }
		]
	},


];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],	
	exports: [RouterModule]
})
export class AppRoutingModule {
	
}