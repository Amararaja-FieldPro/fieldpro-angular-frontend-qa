import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbDateStruct, NgbCalendar, NgbTabset } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { MatRadioChange } from '@angular/material'; //for matradio button
import * as _ from 'lodash';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
// import moment from 'moment';
@Component({
  selector: 'kt-raise',
  templateUrl: './raise.component.html',
  styleUrls: ['./raise.component.scss']
})
export class RaiseComponent implements OnInit {
  call: any;
  labelwarranty: string;
  customerSelect: any;
  siteSelect: any;
  contactNumber: any;
  contactSelect: any;
  RaiseTicketCusInfo: FormGroup;
  application: any;
  segment: any;
  model_id: any;
  warranty_id: any;
  states: any; //variable to store the state
  cities: any; //variable to store the city
  country: any;
  locations: any; //variable to store the country
  radiochange_value: any;
  sitedetails: any;
  AddProductInfo: FormGroup;
  RaiseCall: FormGroup;
  AddIdinfo: FormGroup;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  overlayRef: OverlayRef;
  showTable: boolean = false;
  product: any;  //to store all the products
  subproduct: any;     //to store the subproduct
  work_type: any;             //store the work type
  call_category: any;
  image_name: any;
  ByDefault: boolean = false;
  maxChars: number;
  role: any;
  chars: number;
  raise_error: any;
  image: any;
  cardImageBase64: string;
  isImageSaved: boolean;
  imageError: any;
  loadingSubmit: boolean = false;
  buttonmainedit: boolean = false;
  error: string;
  image_validation: any;
  activeId: any;
  RaiseTicketProdInfo: FormGroup;
  alternate: string;
  contact: string;
  isShow: any;
  contact_no: any;
  productdata: any = [];
  NewCustomerProdInfo: FormGroup;
  site_details: any;
  batteryData: any;
  NewCustomerInfo: FormGroup;
  customer_details: any; //variable to store the customer data getting from the api call
  contract_details: any; //variable to store the contract data getting from the api call
  contract_detail: any;
  customer_code: any; //variable to get the customer code and pass it to get the customer details
  contact_number: any; //variable to get the contact number and pass it to get the customer details
  employee_id: string;
  employee_code: string;
  prioritys: { priority_id: string; priority_name: string; }[];
  contrac_type: any;
  customerID: any;
  customer_names: any;
  add: boolean;
  asideMenus: any;
  current_url: any;
  save_continue: boolean;
  con_number: string;
  contactnum_error: string;
  validation_data: any;
  params: any;
  email_error: string;
  Addnameinfo: FormGroup;
  constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private overlay: Overlay,
    private toastr: ToastrService, public _location: Location, private calendar: NgbCalendar, public tabset: NgbTabset,
    private chRef: ChangeDetectorRef, private formBuilder: FormBuilder, private config: NgbDatepickerConfig) {
    this.RaiseTicketCusInfo = new FormGroup({
      //------------------------formcontrol for customer details--------------------------------------//
      "lable_type": new FormControl(""),
      "employee_id": new FormControl(""),
      "ticket_id": new FormControl(""),
      "customer_id": new FormControl("", [Validators.required]),
      "customer_code": new FormControl("", [Validators.required]),
      "battery_bank_id": new FormControl("", [Validators.required]),
      "address": new FormControl("", [Validators.required]),
      "cust_site_id": new FormControl("", [Validators.required]),
      'batterybankids': new FormControl(""),
      "customer_name": new FormControl('', [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "site_id": new FormControl(""),
      "contact_person_name": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "contact_person_number": new FormControl("", [Validators.required, Validators.pattern("^[a-z 0-9A-Z%+-/@#.,]*"), this.noWhitespace]),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "landmark": new FormControl(''),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "country_id": new FormControl('', [Validators.required]),
      "state_id": new FormControl('', [Validators.required]),
      "city_id": new FormControl('', [Validators.required]),
      "location_id": new FormControl('', [Validators.required])
    });
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  addacontact() {
    if (this.NewCustomerInfo.value.alternate_number == this.NewCustomerInfo.value.contact_number) {
      this.contact = "Contact and alternate contact number should be different.";;
      this.NewCustomerInfo.value.invalid = true;
      this.NewCustomerInfo.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact = '';
      this.NewCustomerInfo.controls['contact_number'].setValue(this.NewCustomerInfo.value.contact_number)
      this.NewCustomerInfo.value.invalid = false;
    }
  }
  addEmailValidation() {
    this.NewCustomerInfo.value.invalid = true;
    this.params = {
      "email_id": this.NewCustomerInfo.value.email_id
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.NewCustomerInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.NewCustomerInfo.value.invalid = true;
        this.NewCustomerInfo.controls['email_id'].setErrors({ 'incorrect': true });

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });



  }
  onChangeState(state_id) {
    if (state_id != '') {
      this.RaiseTicketCusInfo.controls['city_id'].setValue("");
      this.RaiseTicketCusInfo.controls['location_id'].setValue("");
      var state = +state_id; // state: number
      var id = "state_id";
      var url = 'get_city/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, state).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.cities = result.response.data; //storing the api response in the array
          this.locations = [];
        }
        else if (result.response.response_code == "400") {
          this.cities = 'null';
          this.locations = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {

      this.locations = [];
      this.cities = [];
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  ngOnInit() {
    this.save_continue = false
    this.add = false;
    this.labelwarranty = "Select Warranty type";
    this.radiochange_value = 3;
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.asideMenus = localStorage.getItem('asideMenus');
    this.current_url = this.router.url;
    const result = JSON.parse(this.asideMenus).filter(f => f.page === this.current_url)
    if (result.length <= 0) {
      localStorage.clear();

      this.router.navigate(["/login"]);
    } else {

      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
        hasBackdrop: true,
      });
      this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
      this.getcountry()
      this.getproduct()
      this.get_servicegroup()
      this.get_call_category()
      this.getamc()
      this.customerName();
      this.customerNumber();
      this.customerId();
      this.employee_id = localStorage.getItem('employee_id');
      this.employee_code = localStorage.getItem('employee_code');
      this.prioritys = [
        { priority_id: "P1", priority_name: "P1" },
        { priority_id: "P2", priority_name: "P2" },
        { priority_id: "P3", priority_name: "P3" },
        { priority_id: "P4", priority_name: "P4" },
      ];
      // this.getDatetime();
      // this.productdata = [{ "id": "1", "product_category_id": "12", "product_sub_category": "20", "model": "12", "sysah_vol": "2v/200ah", "serial_number": "IG012563", "invoice_number": "", "invoice_date": "", "mfg_date": "", "warranty_type": "", "expiry_date": "" },
      // { "id": "2", "product_category_id": "12", "product_sub_category": "20", "model": "12", "sysah_vol": "2v/200ah", "serial_number": "IG012563", "invoice_number": "", "invoice_date": "", "mfg_date": "", "warranty_type": "", "expiry_date": "" }];
      // console.log(this.productdata);
    }
  }
  customerName() {
    var data = {}
    var url = 'get_customer_name/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customer_names = result.response.data;              //storing the api response in the array
        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  customerNumber() {
    var data = {}
    var url = 'get_customer_number/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.contactNumber = result.response.data;              //storing the api response in the array
        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  customerId() {
    var data = {}
    var url = 'get_customer_id/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.customerID = result.response.data;              //storing the api response in the array
        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  get_servicegroup() {
    var url = 'get_servicegroup/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.work_type = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }

  //get call category
  get_call_category() {
    var url = 'get_call_category/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.call_category = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }

  //get all the amc contract type
  getamc() {
    var url = 'get_amc_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.RequestOptions == "400") {
        this.contrac_type = result.response.data;
      } else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  get log() {
    return this.RaiseTicketCusInfo.controls; //error logs for create user
  }
  //get country dropdown
  getcountry() {
    var url = 'get_country/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.country = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  passcustomercode(customer_code) {
    this.con_number = ''
    this.customer_code = customer_code
  } 
  addContactValidation() {
    this.NewCustomerInfo.value.invalid = true;
    this.params = {
      "contact_number": this.NewCustomerInfo.value.contact_number
    }
    var url = 'cus_validation/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_error = '';
        this.NewCustomerInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_error = result.response.message;
        this.NewCustomerInfo.value.invalid = true;
        this.NewCustomerInfo.controls['contact_number'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  duration(item) {
    console.log(item)
    console.log(this.warranty_id)
    this.warranty_id.forEach(element => {
      if (element.warranty_id == item) {
        this.labelwarranty = element.warranty_duration;
        this.Addnameinfo.controls['contract_duration'].setValue(this.labelwarranty)
      }
      else {
        this.labelwarranty = "Select Warranty type";
      }
    });
  }
  onChangecity(cty_id) {
    if (cty_id != '') {
      this.RaiseTicketCusInfo.controls['location_id'].setValue("");
      var city = +cty_id; // state: number
      var id = "city_id";
      var url = 'get_location_details/?'; // api url for getting the cities
      this.ajax.getdataparam(url, id, city).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.locations = result.response.data; //storing the api response in the array
        }
        else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue(null); //set the base64 in create product image
          this.locations = null;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error')
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.locations = [];
    }
  }
  //function for get all customer and contract details based on contact or customer
  get_customer_contract_details(contact_customer_value, type_value: any, raise_ticket) {
    this.country = [];
    this.states = [];
    this.cities = [];
    this.locations = [];
    this.alternate = '';
    this.contact = '';
    this.contract_details = null;                 //empty the contract details
    // this.RaiseTicketCusInfo.reset();              //reset the customer details
    this.activeId = "tab-selectbyid1";
    if (type_value == 1) {
      this.customer_code = '';
      if (type_value == 1 && this.contact_number != undefined) {
        this.contact_no = this.contact_number.length;
      }
      else {
        this.contact_no = 10;
      }
      if (this.contact_number == '' || this.contact_number == null || this.contact_no < 10) {
        Swal.fire({
          title: 'Oops!',
          text: 'Please provide valid phone number or customer id',
          icon: 'info',
          confirmButtonText: 'ok',
          allowOutsideClick: false, //alert outside click by Sowndarya
        })
      }
      else {
        if ((this.customer_code === undefined && type_value == 1) || this.customer_code == '' || this.customer_code == null) {
          var contact_number = this.contact_number;
          var id = "contact_number";
          var parameter = contact_number;
        }
      }
    }
    else {
      if ((this.contact_number === undefined && type_value == 2) || (type_value == 2)) {
        if (this.customer_code !== '' && type_value !== '') {
          var customer_code = this.customer_code;
        }
        // else if (this.customer_details.customer_code !== '' && type_value == 2) {
        //   var customer_code = this.customer_details.customer_code;
        // }
        var id = "customer_code";
        var parameter = customer_code;
      }
    }
    if (type_value == 2) {
      console.log(this.siteSelect);
      var sites = this.siteSelect.customer_site;
      var splitted = sites.split("-", 3);
      console.log(splitted);
      var customer_code = splitted[0];
      var site_id = splitted[1];
      var parmas = { 'customer_code': customer_code, 'site_id': site_id }
    }
    // var parmas={};
    var url = 'get_customer_contract_details/?';
    this.overlayRef.attach(this.LoaderComponentPortal);
    // api url for getting the customer and contract details
    this.ajax.postdata(url, parmas).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.error = '';
        this.customer_details = result.response.customer_details;
        this.site_details = result.response.site_details;
        // this.contract_details = result.response.contract_details[0];
        // this.contract_detail = result.response.contract_details;
        this.chRef.detectChanges();
        this.contact_number = this.customer_details.contact_number;
        this.customer_code = this.customer_details.customer_code;
        this.batteryData = result.response.battery_bank_array;
        this.productdata = this.site_details.serial_no_details;
        console.log(this.productdata);
        // this.get_expiredate(this.contract_detail);
        this.isShow = false;
        this.RaiseTicketCusInfo.patchValue({
          'customer_id': this.customer_details.customer_id,
          'customer_code': this.customer_details.customer_code,
          'customer_name': this.customer_details.customer_name,
          'ticket_id': this.customer_details.ticket_id,
          'email_id': this.customer_details.email_id,
          'contact_number': this.customer_details.contact_number,
          'alternate_contact_number': this.customer_details.alternate_number,
          'plot_number': this.site_details.plot_number,
          'batterybankids': this.batteryData,
          'street': this.site_details.street,
          'landmark': this.site_details.landmark,
          'post_code': this.site_details.post_code,
          'country_id': this.site_details.country.country_name,
          'state_id': this.site_details.state.state_name,
          'city_id': this.site_details.city.city_name,
          'location_id': this.site_details.location.location_name
        })
        this.overlayRef.detach();

      }
      else if (result.response.response_code == "404") {
        this.customer_details = result.response.customer_details;
        // this.contract_details = result.response.contract_details[0];
        var contract_detail = result.response.contract_details;
        if (contract_detail.length == '0') {
          this.error = "There is no Contract for this customer";
          this.RaiseTicketCusInfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
            'plot_number': '',
            'street': '',
            'landmark': '',
            'post_code': '',
            'country_id': '',
            'state_id': '',
            'city_id': '',
            'location_id': ''
          })
          this.overlayRef.detach();

        }
        else {
          this.error = ''
          this.isShow = false;
          this.RaiseTicketCusInfo.patchValue({
            'customer_id': this.customer_details.customer_id,
            'customer_code': this.customer_details.customer_code,
            'customer_name': this.customer_details.customer_name,
            'ticket_id': this.customer_details.ticket_id,
            'email_id': this.customer_details.email_id,
            'contact_number': this.customer_details.contact_number,
            'alternate_contact_number': this.customer_details.alternate_number,
          })
          this.overlayRef.detach();
          this.chRef.detectChanges();
        }
      }
      else if (result.response.response_code == "400") {
        this.raise_ticket_reset();
        var default_radiovalue = '1';
        this.RaiseTicketCusInfo.controls['lable_type'].setValue(default_radiovalue);
        this.radiochange_value = 1;

        this.overlayRef.detach();
        this.customer_code = '';
        this.NewCustomerInfo.controls['contact_number'].setValue(this.contact_number); //set the base64 in create product image
        this.getcountry();
        var today = new Date();
        var today_date = this.calendar.getToday()
        var year = today_date.year;
        var month = today_date.month;
        var day = today_date.day;
        this.config.maxDate = {
          day: day,
          month: month,
          year: year,
        };

        Swal.fire({
          title: 'Are you sure?',
          text: "You want to create new customer",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, create it!',
          allowOutsideClick: false,   //alert outside click by sowndarya
        }).then((result) => {
          // this.NewCustomerInfo.setValue(NewCustomerInfo)
          if (result.value) {
            this.modalService.open(raise_ticket, {
              size: 'lg', //specifies the size of the dialog box
              backdrop: 'static',              //popup outside click by Sowndarya
              keyboard: false
            });
          }
          else {
            // this.con_number = ''
          }
          var i: number;
          for (i = this.NewCustomerProdInfo.value.contract_data.length; i >= 0; i--) {
            this.remove_dynamic_field(i);
          }
        })
      }
      else if (result.response.response_code == "500") {
        this.overlayRef.detach();
        this.error = result.response.message
      }
      else {
        this.overlayRef.detach();
        this.error = "Something went wrong"
      }
      (err) => {
        this.overlayRef.detach();
        console.log(err); //prints if it encounters an error
      }
    });

  }
  //Function to reset the raiseticket field value
  raise_ticket_reset() {
    this.contact_number = '';
    this.customer_code = '';
    this.image_name = '';
    this.image = '';
    this.RaiseTicketCusInfo.reset();
    this.RaiseTicketProdInfo.reset();
    this.NewCustomerProdInfo.reset();  //reset the form
    // this.NewCustomerInfo.reset();
    // this.NewCustomer.reset();
    this.error = ''
    this.radiochange_value = '1'
    this.RaiseTicketCusInfo.controls['lable_type'].setValue(this.radiochange_value);
    this.isShow = true;
  }
  getapplication(segment_id) {
    console.log(segment_id)
    var data = { "segment_id": segment_id };// storing the form group value
    var url = 'get_application/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.application = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  remove_dynamic_field(i) {
    this.t.removeAt(i);
  }
  get t() { return this.f.contract_data as FormArray; }
  get f() { return this.NewCustomerProdInfo.controls; }
  onchangeradio(event: MatRadioChange) {
    this.radiochange_value = event.value;
    if (this.radiochange_value == 4) {
      this.onChangeCountry(101);
    }
  }
  // function for modal box
  openLarge(raise_ticket) {
    this.product = [];
    this.subproduct = [];
    this.model_id = [];
    this.warranty_id = [];
    this.segment = [];
    this.application = [];
    this.labelwarranty = "Select Warranty type";
    this.call = [];
    this.getsegment();
    this.getproduct();
    this.getcallcategory();
    this.AddProductInfo.reset();
    this.RaiseCall.reset();
    this.modalService.dismissAll();
    this.ByDefault = false;
    this.imageError = '';
    this.cardImageBase64 = '';
    this.isImageSaved = false;
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.error = '';
    this.loadingSubmit = false;
    this.buttonmainedit = false;
    this.RaiseTicketProdInfo.reset();
    console.log(this.RaiseTicketProdInfo.value)
    this.image_validation = '';                 //image error message refreshed
    this.image_name = '';                        //image name is refershed
    this.image = '';                 //image is refershed
    this.raise_error = ''            //refresh the 500 error message while raise the ticket
    this.activeId = "tab-selectbyid1";
    this.modalService.open(raise_ticket, {
      size: 'lg', //specifies the size of the dialog box
      backdrop: 'static',              //popup outside click by Sowndarya
      keyboard: false
    });
  }
  getcallcategory() {
    var url = 'get_call_category/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.call = result.response.data; //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  //get product details
  getproduct() {
    var url = 'get_product_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") { //if sucess
        this.subproduct = "";
        this.product = result.response.data; //storing the api response in the array
        this.model_id = "";
        this.warranty_id = "";
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  getsegment() {
    var data = {};// storing the form group value
    var url = 'get_segment/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.segment = result.response.data;
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  searchReset() {
    this.sitedetails = [];
  }
  battery_bank_settings = {
    singleSelection: true,
    idField: 'id',
    textField: 'name',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search battery bank id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  siteid_settings = {
    singleSelection: true,
    idField: 'customer_site',
    textField: 'customer_site',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search Site id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customerid_settings = {
    singleSelection: true,
    idField: 'customer_id',
    textField: 'customer_code',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search customer id',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  customername_settings = {
    singleSelection: true,
    idField: 'customer_name',
    textField: 'customer_name',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search customer name',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  contactnumber_settings = {
    singleSelection: true,
    idField: 'customer_id',
    textField: 'contact_number',
    enableCheckAll: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Deselect All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 3,
    searchPlaceholderText: 'Search contact number',
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };
  public onFilterChange(item: any) {
    console.log(item);
  }
  public onItemSelect(item: any) {
    console.log(item);
    this.customerSelect = item;
    this.getSiteDetails(this.customerSelect);
  }
  public onSiteIdSelect(item: any) {
    console.log(item);
    this.siteSelect = item;
    // this.getSiteDetails(item);
  }
  public onContactSelect(item: any) {
    console.log(item);
    this.contactSelect = item;
    this.getSiteDetails(this.contactSelect);
  }

  public onDropDownClose(item: any) {
    // console.log(item);
  }
  getSiteDetails(data) {
    var data = data;
    var url = 'get_site_details/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sitedetails = result.response.data;              //storing the api response in the array
        this.showTable = true;                            //show datas if after call API
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;                                                       //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.toastr.error("Something went wrong");
      }
    }, (err) => {
      this.overlayRef.detach();
      //prints if it encounters an error
    });
  }
  onChangeCountry(country_id) {
    if (country_id != '') {
      this.RaiseTicketCusInfo.controls['city_id'].setValue("");
      this.RaiseTicketCusInfo.controls['location_id'].setValue("");
      this.RaiseTicketCusInfo.controls['state_id'].setValue("");
      var country = +country_id; // country: number
      var url = 'get_state/?'; // api url for getting the details with using post params
      var id = "country_id";
      this.ajax.getdataparam(url, id, country).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.states = result.response.data;
          this.cities = [];
          this.locations = [];
        } else if (result.response.response_code == "400") {
          // this.NewCustomerInfo.controls['city_id'].setValue('null'); //set the base64 in create product image
          this.states = null;
          this.cities = [];
          this.locations = [];
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error');
          this.cities = [];
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
          this.cities = [];
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
    else {
      this.states = null;
      this.locations = [];
      this.cities = [];
    }

  }
}
