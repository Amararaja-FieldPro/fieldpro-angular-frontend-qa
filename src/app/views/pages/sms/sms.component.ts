import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, FormBuilder, FormArray, Validators, AbstractControl } from '@angular/forms';
import Swal from 'sweetalert2';
// adding loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: 'kt-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent implements OnInit {
  sms_details: any;
  selectedfile = false;
  edit_data: any;
  data: any = []; // array push
  Editsmsplan: FormGroup;
  Addsmsplan: FormGroup;     //form group for add sms setting plan
  save: any;
  item: any;
  show: boolean = true;
  EnableAddButton: boolean = false;
  empty_error_msg: string;
  itemv: any;
  lable_value: any;
  value_value: any;
  loadingAdd: boolean = false;
  loadingUpdate: boolean = false;
  showmodelTable: boolean;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  duplicateValue: string;
  duplicateLabel: string;
  name: any;
  label: any;
  value: any;
  EditduplicateValue: string;
  EditduplicateLabel: string;
  error: any;
  firstIndexes: boolean;
  adderror: any;
  constructor(private modalService: NgbModal, private overlay: Overlay,
    private fb: FormBuilder, public ajax: ajaxservice, private router: Router,
    private toastr: ToastrService, private chRef: ChangeDetectorRef) {
    this.Addsmsplan = new FormGroup({
      // nila code forurl :Validators.pattern("https?://.+")
      //saranya url pattern changed(11-12-2020)
      "url": new FormControl("", [Validators.required, Validators.pattern("(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")]),
      "gateway": new FormControl("", [Validators.required, this.noWhitespace]),
      "label_name": this.fb.array([this.fb.control('', [Validators.required, this.noWhitespace])]),
      "value": this.fb.array([this.fb.control('', [Validators.required, this.noWhitespace])]),

    })
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getsms();


  }

  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  add(): void {
    this.item = this.item + 2;
    var label_length = this.Addsmsplan.get('label_name') as FormArray
    var value_length = this.Addsmsplan.get('value') as FormArray

    if (this.lable_value == '' || this.lable_value == undefined || this.value_value == '' || this.value_value == undefined) {
      this.empty_error_msg = "Lable and value is required";
    }
    else if (this.value_value == '' && this.lable_value == '') {

    }
    else {
      this.lable_value == ''
      this.value_value == ''
      this.empty_error_msg = "";
      (this.Addsmsplan.get('label_name') as FormArray).push(
        this.fb.control('', [Validators.required, this.noWhitespace])
      );

      (this.Addsmsplan.get('value') as FormArray).push(
        this.fb.control('', [Validators.required, this.noWhitespace])
      );
      this.Addsmsplan.value.valid = false;
    }

  }
  public editremove(i) {
    console.log(i);
    console.log(this.value.length)
    if (this.value.length > 1 || this.label.length > 1) {
      this.value.splice(i, 1);
      this.label.splice(i, 1);
    }
    else {
      this.toastr.warning("Don't delete", "Warning")
    }
    console.log(this.value)
  }
  public editremoveSection(i) {
    console.log(this.value.length)
    console.log(i)
    if (this.value.length > 0 || this.Editsmsplan.value.value.length > 1 || this.Editsmsplan.value.label_name.length > 1) {
      const control = <FormArray>this.Editsmsplan.get("label_name");
      const controls = <FormArray>this.Editsmsplan.get("value");
      control.removeAt(i);
      controls.removeAt(i);
    }
    else {
      this.toastr.warning("Don't delete", "Warning")
    }

  }
  public removeSection(i) {
    console.log(i)
    // this.serial_empty='';
    const control = <FormArray>this.Addsmsplan.get("label_name");
    const controls = <FormArray>this.Addsmsplan.get("value");
    control.removeAt(i);
    controls.removeAt(i);
  }
  get_lable_name(i, lable) {
    this.item = i;
    this.lable_value = lable
  }
  get_value_name(i, value) {
    this.itemv = i;
    this.value_value = value
  }
  addedit(): void {
    // this.Editsmsplan.value.value.forEach(element => {
    //   if (element != '') {
    //     this.firstIndexes = true;
    //     console.log(this.firstIndexes, "firstIndexes")
    //   }
    //   else {
    //     // alert("Empty");
    //     this.firstIndexes = false;
    //     // this.toastr.warning("Please fill the above fields", 'Warning')
    //   }
    // });
    // if (this.firstIndexes == true) {
    (this.Editsmsplan.get('label_name') as FormArray).push(
      this.fb.control('', [Validators.required, this.noWhitespace])
    );

    (this.Editsmsplan.get('value') as FormArray).push(
      this.fb.control('', [Validators.required, this.noWhitespace]));

    // }
    // else {
    //   // alert("Empty");
    //   this.toastr.warning("Please fill the above fields", 'Warning')
    // }

  }

  getFormControls(): AbstractControl[] {
    return (<FormArray>this.Addsmsplan.get('label_name')).controls
  }
  getcontrols(): AbstractControl[] {
    return (<FormArray>this.Editsmsplan.get('label_name')).controls
  }
  getvalueFormControls(): AbstractControl[] {
    return (<FormArray>this.Addsmsplan.get('value')).controls
  }
  getvalueControls(): AbstractControl[] {
    return (<FormArray>this.Editsmsplan.get('value')).controls
  }
  getsms() {
    this.overlayRef.attach(this.LoaderComponentPortal);                          // intialising datatable
    var url = 'get_all_sms_settings/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sms_details = result.response.data;                    //storing the api response in the array
        this.showmodelTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();

      }
      else if (result.response.response_code == "500") {
        this.error = result.response.message;
        this.showmodelTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.error = "Something Went Wrong";
        this.showmodelTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }


    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // convenience getter for easy access to form fields
  get log() {
    return this.Addsmsplan.controls;                       //error logs for create sms
    // return this.Editsmsplan.controls;
  }
  get log1() {
    // return this.Addsmsplan.controls;                       //error logs for create sms
    return this.Editsmsplan.controls;
  }
  openLarge(content6) {
    this.loadingAdd = false;
    this.error = '';
    this.adderror = '';
    this.empty_error_msg = ''
    this.Addsmsplan.reset({
      "url": '',
      "gateway": '',
      "label_name": this.fb.array([this.fb.control('')]),
      "value": this.fb.array([this.fb.control('')]),
    }

    );
    if (this.Addsmsplan.value.label_name.length > 1) {
      var i: number;
      i = 0;
      for (i = this.Addsmsplan.value.label_name.length; i >= 1; i--) {
        this.removeSection(i);
      }
    }
    this.duplicateValue = '';
    this.duplicateLabel = '';
    this.modalService.open(content6, {
      size: 'lg',                                                   //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  buttonEvent(): void {
    this.selectedfile = true; //show confirm button

  }
  addsmsplan() {
    this.duplicateValue = '';
    this.duplicateLabel = '';

    var arr1 = this.Addsmsplan.value.label_name;

    function getDuplicateArrayElements1(arr1) {

      var sorted_arr1 = arr1.slice().sort();
      var results = [];
      for (var i = 0; i < sorted_arr1.length - 1; i++) {
        if (sorted_arr1[i + 1] === sorted_arr1[i]) {
          results.push(sorted_arr1[i]);
        }
      }
      return results;
    }
    var duplicateColors1 = getDuplicateArrayElements1(arr1);
    console.log(duplicateColors1, "duplicateColors1")
    var length1 = duplicateColors1.length;
    console.log(length1, "length1")

    if (length1 != 0) {
      this.duplicateLabel = "Label Name is already exists";
      this.Addsmsplan.controls.setValue['label_name'].setValue('');

    }
    else {
      this.loadingAdd = true;
      var data = this.Addsmsplan.value;                  //Data to be passed for add api
      console.log(data)
      var url = 'create_sms_settings/'                                         //api url for add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                 //if sucess
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingAdd = false;
          this.showmodelTable = false;
          this.chRef.detectChanges();
          this.getsms();
          this.modalService.dismissAll();                                 //to close the modal box
          if (arr1.length > 1) {
            var i: number;
            i = 0;
            for (i = arr1.length; i >= 1; i--) {
              this.removeSection(i);
            }
          }
        }
        else if (result.response.response_code == "400") {
          this.adderror = result.response.message;
          this.loadingAdd = false;
        }
        else if (result.response.response_code == "500") {
          this.adderror = result.response.message;
          this.loadingAdd = false;
        }
        else {
          this.adderror = "Something went wrong";
          this.loadingAdd = false;
        }
      }, (err) => {                                                     //if error
        console.log(err);                                             //prints if it encounters an error
      });
    }



  }
  labelchange(i, $event) {
    console.log(i);
    var count = i;
    console.log($event.target.value)
    console.log(this.label);
    var index = this.label.findIndex(i => i = count)
    console.log(index);
    if (index >= -1) {
      console.log(count, "count")
      console.log(i, "i")
      this.label[i] = $event.target.value;
      console.log(this.label);
    }


    else {
      // alert("No")
    }
    console.log(this.label)

  }
  //Function to store the line item selected to edit
  editsmssetting(data, edit_sms) {
    this.loadingUpdate = false;
    this.value = [];
    var sms_id = data.sms_id; // customer_code                                    
    var url = 'get_sms_settings/?';             // api url for getting the details with using post params
    var id = "sms_id";
    this.ajax.getdataparam(url, id, sms_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.edit_data = result.response.data;                  //storing the api response in the array
        this.chRef.detectChanges();
        console.log(this.edit_data)
        this.label = this.edit_data[0].label_name;
        console.log(this.label)
        this.value = this.edit_data[0].value;
        console.log(this.value)
        this.Editsmsplan = new FormGroup({
          "sms_id": new FormControl(this.edit_data[0].sms_id),
          "label_name": this.fb.array([]),
          "value": this.fb.array([]),
        });
      
        this.modalService.open(edit_sms, {
          size: 'lg',                                                    //specifies the size of the modal box
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });


      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
    // this.loadingUpdate = false;
    // this.value = [];
    // console.log(data)
    // this.edit_data = data;
    // this.label = this.edit_data.label_name;
    // console.log(this.label)


    // this.value = this.edit_data.value;
    // console.log(this.value)

    // this.Editsmsplan = new FormGroup({
    //   "sms_id": new FormControl(this.edit_data.sms_id),
    //   "label_name": this.fb.array([]),
    //   "value": this.fb.array([]),
    // });
    // this.chRef.detectChanges();
    // this.modalService.open(edit_sms, {
    //   size: 'lg',                                                    //specifies the size of the modal box
    //   backdrop: 'static',                                    // modal will not close by outside click
    //   keyboard: false,                                       // modal will not close by keyboard click
    // });
  }

  block(id) {

    var datas = { "sms_id": +id };
    var url = 'block_sms_settings/';             // api url for getting the details with using post params

    this.ajax.postdata(url, datas).subscribe((result) => {
      if (result.response.response_code == "200") {                     //if sucess
        this.error = '';
        this.showmodelTable = false;
        this.chRef.detectChanges();
        this.getsms();                                             //reloading the component
        this.toastr.success(result.response.message, 'Success');        //toastr message for success

      }
      else if (result.response.response_code == "400") {                   //if failure
        this.error = result.response.message;
      }
      else if (result.response.response_code == "500") {                                                           //if not sucess
        this.error = result.response.message;
      }
      else {
        this.error = "Something went wrong";
      }
    }, (err) => {                                                       //if error
      console.log(err);                                        //prints if it encounters an error
    });
  }
  delete(id) {

    var data = { "sms_id": +id };
    var url = 'delete_sms_settings/';             // api url for getting the details with using post params
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, data).subscribe((result) => {
          if (result.response.response_code == "200") {                     //if sucess
            this.error = '';
            this.showmodelTable = false;
            this.chRef.detectChanges();
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.getsms();
            this.modalService.dismissAll();                                    //reloading the component


          }
          else if (result.response.response_code == "400") {                   //if failure
            this.error = result.response.message;
            this.toastr.error(result.response.message, 'Error');        //toastr message for success
          }
          else if (result.response.response_code == "500") {                                                           //if not sucess
            this.error = result.response.message;
            this.toastr.error(result.response.message, 'Error');        //toastr message for success
          }
          else {
            this.error = "Something went wrong";
            this.toastr.error(this.error, 'Error');        //toastr message for success
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }
  submitedit() {
  }
  //--------------------------------------------------------------------------------------------------//
  changevalue(i, $event) {
    console.log(i)
    var count = i;
    console.log($event.target.value);

    console.log(this.value);
    var index = this.value.findIndex(i => i = count)
    console.log(index)
    if (index >= -1) {
      console.log(count, "count")
      console.log(i, "i")
      this.value[i] = $event.target.value;
      console.log(this.value);
    }


    else {
      // alert("No")
    }
    console.log(this.value);


  }
  editsmsplan() {
    console.log(this.value, " this.value");
    console.log(this.Editsmsplan.value.value, "this.Editsmsplan.value.value");
    var arr = this.Editsmsplan.value.value;
    var arr1 = this.Editsmsplan.value.label_name;
    console.log(arr1)
    var newArr = arr1.concat(this.label);
    var newArr1 = arr.concat(this.value);
    this.EditduplicateValue = '';
    this.EditduplicateLabel = '';
    this.loadingUpdate = true;
    newArr1.forEach(element => {
      if (element != '') {
        // alert("Not empty");

      }
      else {
        //  alert("Empty");
        newArr1.splice(element, 1);
      }
    });
    newArr.forEach(element => {
      if (element != '') {
        // alert("Not empty");

      }
      else {
        //  alert("Empty")
        newArr.splice(element, 1);
      }
    });
    function getDuplicateArrayElements1(newArr) {
      var sorted_newArr = newArr.slice().sort();
      var results = [];
      for (var i = 0; i < sorted_newArr.length - 1; i++) {
        if (sorted_newArr[i + 1] === sorted_newArr[i]) {
          results.push(sorted_newArr[i]);
        }
      }
      return results;
    }
    this.Editsmsplan.value.label_name = newArr;
    this.Editsmsplan.value.value = newArr1;
    var duplicateColors = getDuplicateArrayElements1(newArr);
    var length1 = duplicateColors.length;

    if (length1 != 0) {
      this.EditduplicateLabel = "Label Name is already exists";
      this.loadingUpdate = false;
    }
    else {

      // var newArr = arr1.concat(this.label);
      // var newArr1 = arr.concat(this.value);
      // this.Editsmsplan.value.label_name = newArr;
      // this.Editsmsplan.value.value = newArr1;
      console.log(this.Editsmsplan.value)
      var url = 'edit_sms_settings/'                                           //api url of edit api                                    
      this.ajax.postdata(url, this.Editsmsplan.value)
        .subscribe((result) => {
          if (result.response.response_code == "200")                        //if sucess
          {
            this.showmodelTable = false;
            this.chRef.detectChanges();
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
            this.loadingUpdate = false;
            this.getsms();                                              //reloading the component
            this.modalService.dismissAll();                                 //to close the modal box
          }
          else if (result.response.response_code == "400") {                //if failure
            this.error = result.response.message;
            this.loadingUpdate = false;
          }
          else if (result.response.response_code == "500") {                                                               //if not sucess
            this.error = result.response.message;       //toastr message for error
            this.loadingUpdate = false;
          }
          else {
            this.error = "Something went wrong";
            this.loadingUpdate = false;
          }
        }, (err) => {                                                        //if error
          console.log(err);                                                 //prints if it encounters an error
        });
    }
  }
}

