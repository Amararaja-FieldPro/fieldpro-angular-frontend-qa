import { Component, OnInit, NgModule, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbDropdownConfig, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
// import { Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router'; // To enable routing for this component

@Component({
  selector: 'kt-productmaster',
  templateUrl: './productmaster.component.html',
  styleUrls: ['./productmaster.component.scss'],
  providers: [NgbDropdownConfig]
})

@NgModule({
  imports: [
    NgbDropdownModule,
    NgbDropdownConfig
  ]
})

export class ProductmasterComponent implements OnInit {
  httplink: any;
  show: boolean = true;
  AddProduct: FormGroup;
  Addproductedit: FormGroup;
  subproduct: FormGroup;
  Editsubproduct: FormGroup;
  BulkaddProduct: FormGroup;
  selectedfile = true;
  fileData: File = null;
  fileUploadProgress: string = null;
  edit_data: any;
  isSubmitted = false;
  product_details: any;
  cropped_img = true;               //cropping
  subscription_details: any;
  subproduct_details: any;
  edit_subproduct: any;
  file_name: any;
  showTable: boolean = false;                  //loader show and hide
  File: any;
  currentId: number = 0;
  noImage: any;
  pic: boolean = false;
  product: any;
  productid: any;
  productname: any;
  subproductname: any;
  editsubproimage: any;
  editproimage: any;
  subproductimage: any;
  filename: any;
  Filename: any;
  ByDefault: boolean = false;
  layoutUtilsService: any;
  type: any;
  error: any;
  load: string;
  edit_images: any;
  sub: any;
  imagename: any;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;  //loader component
  product_data: any;
  showimage: string;
  // showsubimage: string;
  errors: any;
  errorsb: any;
  loadingUpdate: boolean = false; //Sowndarya 
  loadingUpd: boolean = false;
  loadingUpp: boolean = false;
  loadingAdd: boolean = false;
  buttonVisible: boolean = false;
  buttonVisibleSub: boolean = false;
  buttonmainedit: boolean = true;
  buttonsubedit: boolean = true;
  message: any;
  Bulkerrors: any;
  bulkevalue: any;
  loadingupload: boolean = false;
  maxChars: number;
  role: any;
  chars: number;
  loadingBulk: boolean = false;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  constructor(public ajax: ajaxservice, private toastr: ToastrService, public _location: Location, private router: Router, private overlay: Overlay, private modalService: NgbModal, private chRef: ChangeDetectorRef, public activeModal: NgbActiveModal, private http: Http) { }
  //--------------------------------------------------------------------------------------------------//
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  // function for modal box
  openLarge(content6) {
    this.cardImageBase64 = '';
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.loadingupload = false;
    this.loadingBulk = false;
    this.bulkevalue = "";
    this.Bulkerrors = '';
    this.subproduct_details = []
    this.message = '';
    this.buttonsubedit = true;
    this.errorsb = '';
    this.errors = '';
    this.load = '';
    this.error = '';
    this.Filename = '';
    this.file_name = undefined;
    this.message = '';
    this.buttonsubedit = true;
    this.errorsb = '';
    this.errors = '';
    this.load = '';
    this.error = '';
    this.Filename = '';
    this.file_name = undefined;
    this.showimage = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                  //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });

  }
  openProduct(content6) {
    this.ByDefault = false;
    this.imageError = '';
    this.isImageSaved = false;
    this.cardImageBase64 = '';
    this.load = '';
    this.AddProduct.reset();
    this.message = '';
    this.buttonVisible = false;
    this.buttonVisibleSub = false;
    this.errors = '';
    this.showimage = '';
    // this.showsubimage = '';
    this.imageChangedEvent = "";
    this.subproductimage = "";
    this.Filename = "";
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                  //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  openmodal(productedit) {
    this.isImageSaved = true;
    this.imageError = '';
    this.cardImageBase64 = '';
    this.message = '';
    this.load = '';
    this.buttonmainedit = true;
    this.errors = '';
    this.showimage = '';
    this.imageChangedEvent = '';
    this.Filename = "";
    this.modalService.open(productedit, {
      size: 'lg',
      windowClass: "center-modalsm",                                  //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  open(upload) {
    this.message = '';
    this.errors = '';
    this.product = this.productid;
    this.modalService.open(upload, {
      size: 'lg',                                                        //specifies the size of the modal box
      windowClass: "center-modalsm"      //modal popup custom size 
    });
  }
  openview(View_more) {
    this.message = '';
    this.errors = '';
    this.modalService.open(View_more, {
      size: "lg",
      windowClass: "center-modalsm"     //modal popup resize
    });
  }


  //--------------------------------------------------------------------------------------------------//
  //Functions for image cropper
  imageChangedEvent: any = '';
  croppedImage: any = '';
  fileChangeEvents(fileInput: any) {
    this.message = '';
    this.ByDefault = true;
    if (this.Addproductedit) {
      this.Filename = fileInput.target.files[0].name;
      this.cardImageBase64 = '';
      this.showimage = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          return false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          return false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              return false;
            } else {
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
     if (this.AddProduct) {
      this.Filename = fileInput.target.files[0].name;
      this.showimage = '';
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        this.cardImageBase64 = '';
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.AddProduct.controls['product_image'].setErrors({ 'incorrect': true });
          this.AddProduct.value.invalid = true;

        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.AddProduct.controls['product_image'].setErrors({ 'incorrect': true });
          this.AddProduct.value.invalid = true;

        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.AddProduct.controls['product_image'].setErrors({ 'incorrect': true });
              this.AddProduct.value.invalid = true;

            } else {
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
              this.AddProduct.value.invalid = false;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
     if (this.subproduct) {
      this.Filename = fileInput.target.files[0].name;
      this.showimage = '';
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
          this.subproduct.value.invalid = true;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
          this.subproduct.value.invalid = true;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
              this.subproduct.value.invalid = true;
            } else {

              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
              this.subproduct.value.invalid = false;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
     if (this.Editsubproduct) {
      this.Filename = fileInput.target.files[0].name;
      this.cardImageBase64 = '';
      this.showimage = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          return false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          return false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              return false;
            } else {
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
              // console.log(this.edit_subproduct.product_sub_id)
              this.Editsubproduct.controls["product_sub_id"].setValue(this.edit_subproduct.product_sub_id)
              // console.log(this.Editsubproduct)
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
  }
  removeImage() {
    // alert("DFDSF")
    if (this.AddProduct) {
      // alert("DF")
      this.AddProduct.controls['product_image'].setErrors({ 'incorrect': true });
      this.AddProduct.value.invalid = true;
      this.cardImageBase64 = null;
      this.ByDefault = false;
      this.message = "Image is required";
      this.isImageSaved = false;
    }
    if (this.Addproductedit) {
      // alert("VV")
      // console.log(this.Addproductedit.value.invalid)
      this.Addproductedit.controls['product_name'].setErrors({ 'incorrect': true });
      this.Addproductedit.value.invalid = true;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
      this.message = "Image is required";
      // console.log(this.Addproductedit.value.invalid)
    }
    if (this.subproduct) {
      // alert("DFsf")
      this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
      this.subproduct.value.invalid = true;
      this.cardImageBase64 = null;
      this.ByDefault = false;
      this.message = "Image is required";
      this.isImageSaved = false;
    }
    if (this.Editsubproduct) {
      // alert("DDDDD")
      this.isImageSaved = false;
      // this.message = "Image is required";
      this.ByDefault = false;
      // this.Editsubproduct.controls['product_sub_id'].setValue('');
      // this.Editsubproduct.controls['product_sub_id'].setErrors({ 'incorrect': true });
      // this.Editsubproduct.value.invalid = true;
      this.cardImageBase64 = null;

    }

  }
  onchangemodel(event) {
    if (this.AddProduct) {
      if (this.AddProduct.value.product_model != '') {
        var product_model = event.target.value.trim();
        this.AddProduct.controls['product_model'].setValue(product_model)
      }
      else {
        this.AddProduct.controls['product_model'].setValue('')
      }
    }
    if (this.Addproductedit) {
      if (this.Addproductedit.value.product_model != '') {
        var product_model = event.target.value.trim();
        this.Addproductedit.controls['product_model'].setValue(product_model)
      }
      else {
        this.Addproductedit.controls['product_model'].setValue('')
      }
    }
 
  }
  removeimageedit() {

    if (this.subproduct) {
      // alert("Subproduct")
      this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
      this.subproduct.value.invalid = true;
      this.cardImageBase64 = null;
      this.ByDefault = false;
      this.message = "Image is required";
      this.isImageSaved = false;
    }
    else if (this.Editsubproduct) {
      // alert("Edit")
      // this.Editsubproduct.controls['product_sub_id'].setErrors({ 'incorrect': true });
      // this.Editsubproduct.value.invalid = true;
      this.cardImageBase64 = null;
      // this.isImageSaved = false;
      // this.message = "Image is required";
      this.ByDefault = false;
      this.isImageSaved = false;
    }
  }
  fileChangeEvent(event: any): void {
    this.message = '';
    this.load = '';
    this.buttonVisibleSub = true;
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper 
    this.imagename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.editsubproimage = '';
      this.editproimage = '';
      this.subproductimage = '';
      this.imageChangedEvent = '';
      this.buttonVisibleSub = false;
      this.load = "Only Image will be allowed ( JPG | PNG )";
      this.subproduct.value.valid = false;
      // this.subproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });
    }
    else {
      this.imageChangedEvent = event;
      this.subproduct.value.valid = true;
    }
  }
  fileChangeEventProduct(event: any): void {
    this.message = '';
    this.load = '';
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper 
    this.imagename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.editsubproimage = '';
      this.editproimage = '';
      this.subproductimage = '';
      this.imageChangedEvent = '';
      this.load = "Only Image will be allowed ( JPG | PNG )";
    }
    else {
      this.imageChangedEvent = event;
    }
  }
  imgfileUpload(event) {
    this.message = '';
    //read the file
    this.buttonmainedit = true;
    this.buttonsubedit = true;
    this.showimage = '';
    // this.showsubimage = '';
    this.error = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.imageChangedEvent = '';
      this.buttonVisible = false;
      this.buttonmainedit = false;
      this.buttonsubedit = false;
      this.load = "Only Image will be allowed ( JPG | PNG )";
      this.AddProduct.value.valid = false;
      this.Addproductedit.value.invalid = true;
      // this.Addproductedit.controls['product_image'].setErrors({ 'incorrect': true });
      this.AddProduct.controls['product_image'].setErrors({ 'incorrect': true });
      // this.Editsubproduct.value.valid = false;
      // this.Editsubproduct.controls['product_sub_image'].setErrors({ 'incorrect': true });

    }
    else {
      this.imageChangedEvent = event.target.files[0];
      this.imageChangedEvent = event;
      this.Filename = event.target.files[0].name;
      this.load = "";
      this.buttonVisible = true;
      this.AddProduct.value.valid = true;
      // this.Addproductedit.value.invalid = false;
      // this.Editsubproduct.value.valid = true;
    }
    this.ByDefault = true;
  }

  // //image cropping
  // imageaddproduct(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  // }
  // imageaddsubpro(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  //   this.subproductimage = this.imageChangedEvent;
  // }
  // imageeditpro(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  //   this.editproimage = this.imageChangedEvent;
  // }
  viewmore(items: any) {
    this.product_data = items;
  }
  // imageeditsubproduct(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  //   this.editsubproimage = this.imageChangedEvent;
  // }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  //image cropping
  // imageCropped(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  // }
  // //image cropping
  // imageeditCropped(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  // }
  upload_img() { //button to clear the cropper
    this.cropped_img = false;
    this.selectedfile = true;
  }
  index: any;
  //--------------------------------------------------------------------------------------------------//
  ngOnInit() {
    this.httplink = environment.image_static_ip
    this.maxChars = 512;
    this.role = '';
    this.chars = 0;
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    //Add product category validation
    this.AddProduct = new FormGroup({
      "product_name": new FormControl("", [Validators.required, this.noWhitespace]),
      "product_model": new FormControl(""),
      "product_description": new FormControl("", [Validators.required, this.noWhitespace]),
      "product_image": new FormControl("", [Validators.required])
    });
    this.getproductmasterdata();
  }
  //get API for product masterfor use the page view
  getproductmasterdata() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_product_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {                    //if sucess
        this.product_details = result.response.data;                    //storing the api response in the array
        this.chRef.detectChanges();
        this.overlayRef.detach();

      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
        this.overlayRef.detach();
      }
      else {
        this.errors = "Something went wrong";
        this.overlayRef.detach();
      }
    }, (err) => {
      this.overlayRef.detach();                                                  //if error
      console.log(err);  
      // this.toastr.success("Poor network", 'Error');                                                 //prints if it encounters an error
    });
  }

  setDefaultPic() {
    this.pic = !this.pic;
  }

  //Add product category control
  get log() {
    return this.AddProduct.controls;                       //error logs for product
  }
  get editpro() {
    return this.Addproductedit.controls;                       //error logs for product
  }
  //Add subproduct category control
  get log1() {
    return this.subproduct.controls;                         //error logs for subproduct
  }
  get editsubpro() {
    return this.Editsubproduct.controls;
  }
  //---------------------------------------submit functions------------------------------------------------//

  //submit for create a new subproduct
  submitaddsubproduct() {
    const formData = new FormData();
    formData.append('files', this.fileData);
    this.fileUploadProgress = '0%';
  }
  //--------------------------------------------------------------------------------------------------//
  //add product using API
  addpackageplan() {
    this.loadingAdd = true;
    var modal = this.AddProduct.value.product_model.replace(/\s/g, "");
    var data = this.AddProduct.value;                  //Data to be passed for add api
    data["product_model"] = modal;
    data["product_image"] = this.cardImageBase64;            //Data to be passed for add api
    var url = 'add_product/'                                         //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                  //if sucess
        this.loadingAdd = false;
        this.getproductmasterdata();
        this.chRef.detectChanges();                                            //reloading the component
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.modalService.dismissAll();                                 //to close the modal box
      }
      else if (result.response.response_code == "400") {
        this.errors = result.response.message;
        this.loadingAdd = false;
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
        this.loadingAdd = false;
      }
      else {
        this.errors = "Something went wrong";
        this.loadingAdd = false;
      }
    }, (err) => {                                                         //if error
      console.log(err);                                             //prints if it encounters an error
      this.loadingAdd = false;
      // this.toastr.success("Poor network", 'Error');  
    });
  }
  //add subproduct using API
  addsubproduct(content2) {
    var data = this.subproduct.value;                  //Data to be passed for add api
    this.loadingUpd = true;
    data["product_sub_image"] = this.cardImageBase64;            //Data to be passed for add api
    var url = 'add_subproduct/'                                         //api url for add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {                     //if sucess
        var datas = data.product;
        var product = +datas;                        //get the product id for identify the  product name                     
        var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
        var id = "product_id";                             //id use for url
        this.overlayRef.attach(this.LoaderComponentPortal);
        this.ajax.getdataparam(url, id, product).subscribe((result1) => {
          if (result1.response.response_code == "200") {               //if sucess

            this.subproduct_details = result1.response.data;
            this.loadingUpd = false;
            this.chRef.detectChanges();
            this.modalService.dismissAll();
            this.overlayRef.detach();
            this.modalService.open(content2, {
              size: 'lg',
              windowClass: "center-modalsm"                                  //specifies the size of the dialog box
            });
          }
          else if (result1.response.response_code == "400") {
            this.modalService.dismissAll();
            this.overlayRef.detach();
            this.errors = result1.response.message;
            this.loadingUpd = false;
          }
          else if (result1.response.response_code == "500") {
            this.modalService.dismissAll();
            this.overlayRef.detach();
            this.errors = result1.response.message;
            this.loadingUpd = false;
          }
          else {
            this.modalService.dismissAll();
            this.overlayRef.detach();
            this.errors = "Something went wrong";
            this.loadingUpd = false;
          }
        }, (err) => {
          console.log(err);    
          //  this.toastr.success("Poor network", 'Error');                                      //prints if it encounters an error
        });
        this.toastr.success(result.response.message, 'Success');        //toastr message for success
      }
      else if (result.response.response_code == "400") {
        this.errors = result.response.message;
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = "Something went wrong";
      }
    }, (err) => {                                                     //if error
      console.log(err);   
      // this.toastr.success("Poor network", 'Error');                                            //prints if it encounters an error
    });

  }
  //--------------------------------------------------------------------------------------------------//
  //Function to store the line item selected to edit
  editpackage(data) {
    this.ByDefault = true;
    this.Filename = "Image";
    this.cardImageBase64 = '';
    this.edit_data = data;
    this.showimage = environment.image_static_ip + this.edit_data.product_image;
    this.Addproductedit = new FormGroup({
      "product_name": new FormControl(this.edit_data.product_name, [Validators.required, this.noWhitespace]),
      "product_model": new FormControl(this.edit_data.product_model),
      "product_description": new FormControl(this.edit_data.product_description, [Validators.required, , this.noWhitespace]),
      "product_image": new FormControl(''),
      "product_id": new FormControl(this.edit_data.product_id),
    });
    this.productid = this.edit_data.product_id;
    var product = +this.productid;                        //get the product id for identify the  product name                     
    var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
    var id = "product_id";                             //id use for url
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {               //if sucess
        this.subproduct_details = result.response.data;
      }
      else if (result.response.response_code == "400") {
        this.errors = result.response.message;
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message;
      }
      else {
        this.errors = "Something went wrong";
      }
    }, (err) => {
      console.log(err);    
      // this.toastr.success("Poor network", 'Error');                                      //prints if it encounters an error
    });
  }
  Subproduct() {
    this.imageChangedEvent = ''
    //Add subproduct category validation   
    this.subproduct = new FormGroup({
      "product_sub_name": new FormControl("", [Validators.required, this.noWhitespace]),
      // "product_sub_model": new FormControl("", [Validators.required, this.noWhitespace]),
      "product_sub_description": new FormControl("", [Validators.required, , this.noWhitespace]),
      "product": new FormControl(this.edit_data.product_id, [Validators.required]),
      "product_sub_image": new FormControl("", [Validators.required])
    });
  }
  //Function to store the line item selected to subproduct edit
  editsubproducts(data, updatesubproduct) {
    this.cardImageBase64 = null;
    this.edit_subproduct = data;
    this.isImageSaved = true;
    this.showimage = environment.image_static_ip + this.edit_subproduct.product_sub_image;

    // console.log(this.edit_subproduct);
    // this.showsubimage = "http://" + this.edit_subproduct.product_sub_image;
    this.ByDefault = true;
    this.Filename = "Image";
    this.subproductname = this.edit_subproduct.product.product_name;
    this.Editsubproduct = new FormGroup({
      "product_sub_name": new FormControl(this.edit_subproduct.product_sub_name, [Validators.required, this.noWhitespace]),
      // "product_sub_model": new FormControl(this.edit_subproduct.product_sub_model, [Validators.required, this.noWhitespace]),
      "product_sub_description": new FormControl(this.edit_subproduct.product_sub_description, [Validators.required, , this.noWhitespace]),
      "product_id": new FormControl(this.edit_subproduct.product.product_id),
      "product_sub_id": new FormControl(this.edit_subproduct.product_sub_id, [Validators.required]),
      "product_sub_image": new FormControl('')
    });
    this.subproductname = this.edit_subproduct.product.product_sub_name;
    this.sub = this.edit_subproduct.product_sub_image;
    this.modalService.open(updatesubproduct, {
      size: 'lg',
      windowClass: "center-modalsm",                                  //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  //--------------------------------------------------------------------------------------------------//
  // function for delete the product
  trashproduct(data) {
    this.edit_data = data;
    var productid = this.edit_data.product_id;
    var product = +productid;                    //get the product id for identify the  product name                                 
    var url = 'delete_product/?';             // api url for getting the details with using post params
    var id = "product_id";                        //id for making url
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.getdataparam(url, id, product).subscribe((result) => {
          if (result.response.response_code == "200") {                //if sucess
            this.getproductmasterdata();
            this.chRef.detectChanges();                                            //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success
          }
          else if (result.response.response_code == "400") {
            this.errors = result.response.message;
          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
          else {
            this.errors = "Something went wrong";
            this.toastr.error(this.errors, 'Error');        //toastr message for success
          }
        }, (err) => {
          console.log(err);
          // this.toastr.success("Poor network", 'Error');                                          //prints if it encounters an error
        });
      }
    })
  }
  //function for delete asubproduct
  trashsubproduct(data, content2) {
    this.errors = '';
    this.edit_subproduct = data;                             //get datas from get API
    var subproductid = this.edit_subproduct.product_sub_id;
    var subproduct = +subproductid;                          //get the product id for identify the  subproduct name                     
    var url = 'delete_subproduct/?';             // api url for getting the details with using post params
    var id = "product_sub_id";                         //id for making url
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.getdataparam(url, id, subproduct).subscribe((result) => {
          if (result.response.response_code == "200") {                      //if sucess
            this.overlayRef.attach(this.LoaderComponentPortal);
            var datas = this.edit_subproduct.product.product_id;
            var product = +datas;                        //get the product id for identify the  product name                     
            var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
            var id = "product_id";                             //id use for url
            this.ajax.getdataparam(url, id, product).subscribe((result1) => {
              if (result1.response.response_code == "200") {               //if sucess
                this.subproduct_details = result1.response.data;
                this.chRef.detectChanges();

                this.toastr.success(result.response.message, 'Success');        //toastr message for success
                this.overlayRef.detach();

              }
              else if (result1.response.response_code == "400") {
                this.subproduct_details = [];
                this.chRef.detectChanges();
                this.overlayRef.detach();
                this.errors = result1.response.message;
              }
              else if (result1.response.response_code == "500") {
                this.overlayRef.detach();
                this.errors = result1.response.message;
              }
              else {
                this.overlayRef.detach();
                this.errors = "Something went wrong";
              }
            }, (err) => {
              console.log(err);                                        //prints if it encounters an error
            });

            this.toastr.success(result.response.message, 'Success');        //toastr message for success
          }
          else if (result.response.response_code == "400") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
            // this.errors = result.response.message;

          }
          else if (result.response.response_code == "500") {
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
            // this.errors = result.response.message;

          }
          else {

            this.errors = "Something went wrong";
            this.toastr.error(this.errors, 'Error');        //toastr message for success
          }
        }, (err) => {
          console.log(err);  
          // this.toastr.success("Poor network", 'Error');                                        //prints if it encounters an error
        });
      }
    })
  }
  //--------------------------------------------------------------------------------------------------//

  //Function to call the edit api 
  editproduct(edit_data) {
    var modal = this.Addproductedit.value.product_model.replace(/\s/g, "");
    var data = this.Addproductedit.value;
    this.loadingUpp = true;            //Data to be passed for add api 
    data["product_model"] = modal;
    data["product_image"] = this.editproimage;
    if (this.cardImageBase64 == '' || this.cardImageBase64 == ' ' || this.cardImageBase64 == undefined || this.showimage == this.Addproductedit.value.product_image) {
      data["product_image"] = '';
    }
    else {
      data["product_image"] = this.cardImageBase64;
    }
    var url = 'edit_product/'                                           //api url of edit api                                    
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200") {                           //if sucess
          this.getproductmasterdata();
          this.chRef.detectChanges();                                      //reloading the component
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.loadingUpp = false;
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (result.response.response_code == "400") {
          this.errors = result.response.message;
          this.loadingUpp = false;
        }
        else if (result.response.response_code == "500") {
          this.errors = result.response.message;
          this.loadingUpp = false;
        }
        else {
          this.errors = "Something went wrong";
          this.loadingUpp = false;
        }
      }, (err) => { 
        // this.toastr.success("Poor network", 'Error');                                                       //if error
        console.log(err);                                                 //prints if it encounters an error
      });

  }
  editsubproduct(edit_subproduct, content2) {
    var data = this.Editsubproduct.value;                  //Data to be passed for add api 
    this.loadingUpdate = true;
    if (this.cardImageBase64 == '' || this.cardImageBase64 == '' || this.cardImageBase64 == undefined || this.showimage == this.Editsubproduct.value.product_sub_image) {
      data["product_sub_image"] = '';
    }
    else {
      data["product_sub_image"] = this.cardImageBase64;
    }
    //Data to be passed for add api
    var url = 'edit_subproduct/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.Editsubproduct.value)
      .subscribe((result) => {
        if (result.response.response_code == "200") {                  //if sucess
          this.overlayRef.attach(this.LoaderComponentPortal);
          var datas = this.edit_subproduct.product.product_id;
          var product = +datas;                        //get the product id for identify the  product name                     
          var url = 'get_subproduct_details/?';             // api url for getting the details with using post params
          var id = "product_id";                             //id use for url
          this.ajax.getdataparam(url, id, product).subscribe((result1) => {
            if (result1.response.response_code == "200") {               //if sucess
              this.subproduct_details = result1.response.data;
              // console.log(this.subproduct_details);
              this.chRef.detectChanges();
              this.modalService.dismissAll();
              this.toastr.success(result.response.message, 'Success');        //toastr message for success
              this.loadingUpdate = false;
              this.overlayRef.detach();
              this.modalService.open(content2, {

                size: 'lg',
                windowClass: "center-modalsm"                                  //specifies the size of the dialog box
              });
            }
            else if (result1.response.response_code == "400") {
              this.errors = result1.response.message;
              this.loadingUpdate = false;
            }
            else if (result1.response.response_code == "500") {
              this.errors = result1.response.message;
              this.loadingUpdate = false;
            }
            else {
              this.errors = "Something went wrong";
              this.loadingUpdate = false;
            }
          }, (err) => {
            // this.toastr.success("Poor network", 'Error');  
            console.log(err);                                        //prints if it encounters an error
          });
        }
        else if (result.response.response_code == "400") {

          this.errors = result.response.message;
          this.modalService.open(content2, {
            size: 'lg',
            windowClass: "center-modalsm"                                  //specifies the size of the dialog box
          });
        }
        else if (result.response.response_code == "500") {

          this.errors = result.response.message;
          this.modalService.open(content2, {
            size: 'lg',
            windowClass: "center-modalsm"                                  //specifies the size of the dialog box
          });
        }
        else {
          this.errors = "Something went wrong";
          this.modalService.open(content2, {
            size: 'lg',
            windowClass: "center-modalsm"                                  //specifies the size of the dialog box
          });
        }
      }, (err) => {  
        // this.toastr.success("Poor network", 'Error');                                                                     //if error
        console.log(err);                                                 //prints if it encounters an error
      });
  }
  //--------------------------------------------------------------------------------------------------//
  fileUpload(event) {
    this.type = '';
    this.Filename = '';
    this.file_name = '';
    this.Bulkerrors = [];
    this.bulkevalue = ""
    //read the file
    this.error = "";
    this.errors = "";
    this.errorsb = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    this.ByDefault = true;
  };
  //after submit the file for product
  filesubmit(bulkupload) {
    this.Bulkerrors = []
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");

    if (this.file_name == undefined) {
      this.error = "Please select file";
    }

    else if (this.file_name.length > 0) {

      if (validExts[0] != this.type) {
        this.error = "Please select correct extension";

      }
      else if (validExts = this.type) {
        this.loadingBulk = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();         //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file
        var method = "post";                                    //post method 
        var url = 'product_bulk_upload/'                         //post url
        var i: number;                                             //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            this.error = '';
            this.Bulkerrors = [];
            this.loadingBulk = false;
            if (data['success'].length > 0) {
              for (i = 0; i < data['success'].length; i++) {
                this.getproductmasterdata();
                this.chRef.detectChanges();
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
                // this.modalService.dismissAll();             //to close the modal box       
              }
            }
            if (data['response'].error_code == "404") {
              if (data['response'].fail.length > 0) {
                for (i = 0; i < data['response'].fail.length; i++) {
                  //if employee code or employee id is error to put this
                  for (i = 0; i < data['response'].fail.length; i++) {                                //count the length
                    var a = [];
                    data['response'].fail.forEach(value => {
                      a.push(value.error_message
                      );
                    });

                    this.Bulkerrors = a;

                  }
                }
              }
            }
          }
          else if (data['response'].response_code == "400") {
            this.Bulkerrors = [];
            this.loadingBulk = false;
            this.error = data['response'].message;
          }
          else if (data['response'].response_code == "500") {
            this.Bulkerrors = [];
            this.loadingBulk = false;
            this.error = data['response'].message;
          }
          else {
            this.Bulkerrors = [];
            this.loadingBulk = false;
            this.errors = "Something went wrong";
          }
        },
          (err) => {
            this.loadingBulk = false;                                                                //if error
            console.log(err);                                                 //prints if it encounters an error
            this.errors = "Something went wrong";
          }
        );
      }
    }
  };
  //after submit the file for subproduct
  subfilesubmit(content2) {
    this.loadingupload = true;
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel");
    if (this.file_name == undefined) {
      this.error = "Please select file";
      this.loadingupload = false;

    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {

        this.error = "Please select correct extension";
        this.loadingupload = false;
      }
      else if (validExts = this.type) {
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();            //convert the formdata
        formData.append('excel_file', file);                     //append the name of excel file        
        let headers = new Headers();
        var method = "post";                                                                              //post method 
        var url = 'subproduct_bulk_upload/';                                                                 //post url
        var i: number;                                                                       //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
            if (data['response'].fail.length > 0) {
              this.error = '';
              this.loadingupload = false;                             //failure response 
              //if employee code or employee id is error to put this
              var a = [];
              data['response'].fail.forEach(value => {
                a.push(value.error_message
                );
              });
              this.bulkevalue = a;
            }
            //success response
            //if newly add product for excisting employee code or employee id
            if (data['success'].length > 0) {                         //sucess response
              this.bulkevalue = '';
              this.loadingupload = false;
              this.error = '';
              for (i = 0; i < data['success'].length; i++) {
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for sucess
                var datas = { "product_id": this.edit_subproduct.product.product_id };
                this.editpackage(datas);
              }
            }
          }
          //if other errors
          else if (data['response'].response_code == "400") {
            this.bulkevalue = '';
            this.loadingupload = false;
            this.error = data['response'].message;
          }
          else if (data['response'].response_code == "500") {
            this.loadingupload = false;
            this.error = data['response'].message
          }
          else {
            this.loadingupload = false;
            this.error = "Something went wrong";
          }
        },

          (err) => {    
            // this.toastr.success("Poor network", 'Error');                                                                  //if error
            console.log(err);                                                 //prints if it encounters an error
          }
        );
      }
    }
  };


  //saranya(09-12-2020) - On bbutton click close emptying the choose file and image
  imageClear() {
    this.ByDefault = false;
    this.imageChangedEvent = '';
    this.subproductimage = '';
    this.showimage = '';
    // this.showsubimage = '';
    this.buttonVisible = !this.buttonVisible;
    this.buttonVisibleSub = !this.buttonVisibleSub;
    this.buttonmainedit = !this.buttonmainedit;
    this.buttonsubedit = !this.buttonsubedit;
    this.message = "please select the image";
    this.AddProduct.value.valid = false;
    this.Addproductedit.value.invalid = true;
    // this.Addproductedit.controls['product_image'].setErrors({ 'incorrect': true });
  }
  //--------------------------------------------------------------------------------------------------// 
}


