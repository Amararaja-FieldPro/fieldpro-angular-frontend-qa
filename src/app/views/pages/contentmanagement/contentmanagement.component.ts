
import { Component, OnInit, Renderer2, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, AbstractControl, ValidatorFn, FormsModule } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
// import { ThumbSettings } from '@syncfusion/ej2-angular-charts';
declare var require: any
const FileSaver = require('file-saver');


@Component({
  selector: 'kt-contentmanagement',
  templateUrl: './contentmanagement.component.html',
  styleUrls: ['./contentmanagement.component.scss']
})
export class ContentmanagementComponent implements OnInit {
  response_message: string;
  cursor: boolean = false;
  spare_image: any;
  @ViewChild('div', { static: true }) div: ElementRef;
  error_types: any;
  checkdata: any;
  name: any;
  load: any;
  results: any;
  names: any;
  title: any;
  edit_assess: any;
  size: any;
  error_type: any;
  pdf: any;
  imageChanged: any;
  error: any;
  upload_video: any;
  assesment_flag: any;
  image_or_video: any;
  edit_type: any;
  assesment: any;
  assesment_details: any;
  content_details: any;
  Addcontent: FormGroup;
  EditContent: FormGroup;
  Editassessment: FormGroup;
  AddQuestion: FormGroup;
  AddQuiz: FormGroup;
  AddAsessment: FormGroup; //FormGroup for asessment
  EditQuestion: FormGroup; //FormGroup for Question/Quiz
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  cropped_img = true;
  selectedfile = true;
  question_details: any;
  correct_answer: any;
  edit_data: any;
  content_edit_data: any; //variable for store the row content data
  edit_ques: any //variable for store the editable questions
  file_name: any;
  questions: any;
  training_thumb_images: any;
  type: any;
  are: any
  product: any; //variable to sore all the products
  subproduct: any; //variable to sore all the sub product based on product id
  image_name: any; //variable to strore the image
  videopdf_name: any; //variable to strore the pdf or video
  questionselect: any //variable to strore the questoins
  questiona: any;
  questionb: any;
  questionc: any;
  questiond: any;
  assessment_id: any; //assessment_id used in add_training in assessment edit
  show: boolean = true;
  question_assessment = [];
  threshold = [];
  reference: any;
  ReferAssess: FormGroup;
  showTable: boolean = false; //loader show and hide
  overlayRef: OverlayRef;
  upload_image: any; //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>; //loader component
  filename: any; // file upload
  Filename: any; // file upload
  ByDefault: boolean = false; // file upload
  ByDefault1: boolean = false;
  currentJustify: "end"; //tabs right display
  preId: any;
  nextId: any;
  currentId: any;
  checkvalue: any;
  tabvalue: any;
  showtableassessment: boolean = false;
  imageShown: boolean;
  showTableContent: boolean = false;
  showquestion: boolean = false;
  loading: boolean = false;          //button spinner by Sowndarya
  loadingAdd: boolean = false;
  loadingEdit: boolean = false;
  loadingCrt: boolean = false;
  loadingRef: boolean = false;
  loadingContent: boolean = false;
  loadingSave: boolean = false;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtOptions1: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  videopdf: any;
  uploadname: any;
  blocknext: boolean;
  assessmt_id: any;
  fieldMatch: string;
  fieldMatch1: string;
  fieldMatch2: string;
  videos: string;
  fieldMatch3: string;
  correct: string;
  activeId: any;
  add_assess: string;
  warning: string;
  question_asses = [];
  buttonVisible: boolean = false;
  buttonmainedit: boolean = true;
  message: string;
  bulk: any;
  loadingSub: boolean = false;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  trainning_error: string;
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router,
    private toastr: ToastrService,
    private fb: FormBuilder, public _location: Location, private renderer: Renderer2, private overlay: Overlay, private chRef: ChangeDetectorRef, private elementRef: ElementRef) {

    this.dtOptions1 = {
      pagingType: 'full_numbers',
      pageLength: 5,
      lengthMenu: [5, 10, 15]
    };
    this.Addcontent = new FormGroup({
      "training_title": new FormControl('', [Validators.required, this.noWhitespace]),
      "training_type": new FormControl('', [Validators.required]),
      "training_content_type": new FormControl('', [Validators.required]),
      "training_content": new FormControl('', [Validators.required]),
      "training_thumb_image": new FormControl('', [Validators.required]),
      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
    });
    this.EditContent = new FormGroup({
      "training_id": new FormControl('', [Validators.required]),
      "training_details_id": new FormControl('', [Validators.required]),
      "training_title": new FormControl('', [Validators.required, this.noWhitespace]),
      "training_type": new FormControl('', [Validators.required]),
      "training_content_type": new FormControl('', [Validators.required]),
      "training_content": new FormControl(''),
      "training_thumb_image": new FormControl(''),
      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
    });
    this.AddAsessment = new FormGroup({
      "assessment_name": new FormControl('', [Validators.required, this.noWhitespace]),
      "assessment_type": new FormControl('', [Validators.required]),
      "product_id": new FormControl(''),
      "product_sub_id": new FormControl(''),
    });
    this.AddQuiz = new FormGroup({
      "quation": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_a": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_b": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_c": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_d": new FormControl('', [Validators.required, this.noWhitespace]),
      "correct_answer": new FormControl('', [Validators.required]),
      "assessment_id": new FormControl(this.assessmt_id),
    });
    this.EditQuestion = new FormGroup({
      "assessment_id": new FormControl('', [Validators.required]),
      "quation_id": new FormControl('', [Validators.required]),
      "quation": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_a": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_b": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_c": new FormControl('', [Validators.required, this.noWhitespace]),
      "option_d": new FormControl('', [Validators.required, this.noWhitespace]),
      "correct_answer": new FormControl('', [Validators.required])
    });
    this.ReferAssess = this.fb.group({
      training_details_id: this.fb.array([], [Validators.required])
    })
  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  changetype(type) {
    this.error_type = "";
    this.error_types = "";
    this.videopdf = '';
    this.ByDefault1 = false;
    this.Addcontent.controls['training_content'].setValue('');

  }
  changetype_edit(type) {
    this.error_type = "";
    this.error_types = "";
    this.videopdf = '';
    this.EditContent.controls['training_content_type'].setErrors({ 'incorrect': true });
    this.EditContent.value.invalid = true;
    this.correct = "Please ensure the content type!";
    this.ByDefault1 = false;
  }
  training_change() {
    if (this.EditContent.value.training_type == "Product Training") {
      this.type = 'Product Training'
    }
    else {
      this.type = ''
    }
  }
  matchValidators(option) {
    this.AddQuiz.controls['correct_answer'].setValue('');
    this.EditQuestion.controls['correct_answer'].setValue('');
    const fromValue = option;
    const toValue = this.AddQuiz.value.option_b;
    const EValue = this.EditQuestion.value.option_b;
    const cValue = this.AddQuiz.value.option_c;
    const CValue = this.EditQuestion.value.option_c;
    const dValue = this.AddQuiz.value.option_d;
    const DValue = this.EditQuestion.value.option_d;
    if (fromValue && toValue && fromValue === toValue) {
      this.AddQuiz.controls['option_a'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.AddQuiz.value.option_a = '';
      this.fieldMatch3 = "Duplicates are there"
    }
    else if (fromValue && cValue && fromValue === cValue) {
      this.AddQuiz.controls['option_a'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.AddQuiz.value.option_a = '';

      this.fieldMatch3 = "Duplicates are there"
    }
    else if (fromValue && dValue && fromValue === dValue) {
      this.AddQuiz.controls['option_a'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.AddQuiz.value.option_a = '';

      this.fieldMatch3 = "Duplicates are there"
    }
    else if (fromValue && EValue && fromValue === EValue) {
      this.EditQuestion.value.option_a = '';
      this.EditQuestion.controls['option_a'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch3 = "Duplicates are there"
    }
    else if (fromValue && CValue && fromValue === CValue) {
      this.EditQuestion.value.option_a = '';
      this.EditQuestion.controls['option_a'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch3 = "Duplicates are there"
    }
    else if (fromValue && DValue && fromValue === DValue) {
      this.EditQuestion.value.option_a = '';
      this.EditQuestion.controls['option_a'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch3 = "Duplicates are there"
    }
    else {
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch3 = '';
    }
  }
  matchValidator(option) {
    const fromValue = option;
    this.AddQuiz.controls['correct_answer'].setValue('');
    this.EditQuestion.controls['correct_answer'].setValue('');
    const toValue = this.AddQuiz.value.option_a;
    const EValue = this.EditQuestion.value.option_a;
    const cValue = this.AddQuiz.value.option_c;
    const CValue = this.EditQuestion.value.option_c;
    const dValue = this.AddQuiz.value.option_d;
    const DValue = this.EditQuestion.value.option_d;
    if (fromValue && toValue && fromValue === toValue) {
      this.AddQuiz.value.option_b = '';
      this.AddQuiz.controls['option_b'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else if (fromValue && cValue && fromValue === cValue) {
      this.AddQuiz.value.option_b = '';
      this.AddQuiz.controls['option_b'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else if (fromValue && dValue && fromValue === dValue) {
      this.AddQuiz.value.option_b = '';
      this.AddQuiz.controls['option_b'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else if (fromValue && EValue && fromValue === EValue) {
      this.EditQuestion.value.option_b = '';
      this.EditQuestion.controls['option_b'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else if (fromValue && CValue && fromValue === CValue) {
      this.EditQuestion.value.option_b = '';
      this.EditQuestion.controls['option_b'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else if (fromValue && DValue && fromValue === DValue) {
      this.EditQuestion.value.option_b = '';
      this.EditQuestion.controls['option_b'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch = "Duplicates are there"
    }
    else {
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch = '';
    }
  }
  matchValidator1(option) {
    this.AddQuiz.controls['correct_answer'].setValue('');
    this.EditQuestion.controls['correct_answer'].setValue('');
    const fromValue = option;
    const toValue1 = this.AddQuiz.value.option_a;
    const toValue = this.AddQuiz.value.option_b;
    const toValue2 = this.AddQuiz.value.option_d;
    const EValue1 = this.EditQuestion.value.option_a;
    const EValue = this.EditQuestion.value.option_b;
    const EValue2 = this.EditQuestion.value.option_d;
    if (fromValue && toValue && fromValue === toValue) {
      this.AddQuiz.value.option_c = '';
      this.AddQuiz.controls['option_c'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option b and option c are same"
    }
    else if (fromValue && toValue1 && fromValue === toValue1) {
      this.AddQuiz.value.option_c = '';
      this.AddQuiz.controls['option_c'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option a and option c are same"
    }
    else if (fromValue && toValue2 && fromValue === toValue2) {
      this.AddQuiz.value.option_c = '';
      this.AddQuiz.controls['option_c'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option d and option c are same"
    }
    else if (fromValue && EValue && fromValue === EValue) {
      this.EditQuestion.value.option_c = '';
      this.EditQuestion.controls['option_c'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option b and option c are same"
    }
    else if (fromValue && EValue1 && fromValue === EValue1) {
      this.EditQuestion.value.option_c = '';
      this.EditQuestion.controls['option_c'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option c and option a are same"
    }
    else if (fromValue && EValue2 && fromValue === EValue2) {
      this.EditQuestion.value.option_c = '';
      this.EditQuestion.controls['option_c'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch1 = "Option d and option c are same"
    }
    else {
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch1 = '';
    }
  }
  matchValidator2(option) {
    const fromValue = option;
    this.AddQuiz.controls['correct_answer'].setValue('');
    this.EditQuestion.controls['correct_answer'].setValue('');
    const toValue = this.AddQuiz.value.option_c;
    const EValue = this.EditQuestion.value.option_c;
    const toValuea = this.AddQuiz.value.option_a;
    const EValuea = this.EditQuestion.value.option_a;
    const toValueb = this.AddQuiz.value.option_b;
    const EValueb = this.EditQuestion.value.option_b;
    if (fromValue === toValue) {
      this.AddQuiz.value.option_d = '';
      this.AddQuiz.controls['option_d'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else if (fromValue === toValuea) {
      this.AddQuiz.value.option_d = '';
      this.AddQuiz.controls['option_d'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else if (fromValue === toValueb) {
      this.AddQuiz.value.option_d = '';
      this.AddQuiz.controls['option_d'].setValue('');
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else if (fromValue && EValue && fromValue === EValue) {
      this.EditQuestion.value.option_d = '';
      this.EditQuestion.controls['option_d'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else if (fromValue && EValueb && fromValue === EValueb) {
      this.EditQuestion.value.option_d = '';
      this.EditQuestion.controls['option_d'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else if (fromValue && EValuea && fromValue === EValuea) {
      this.EditQuestion.value.option_d = '';
      this.EditQuestion.controls['option_d'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch2 = "Duplicates are there"
    }
    else {
      this.AddQuiz.controls['correct_answer'].setValue('');
      this.EditQuestion.controls['correct_answer'].setValue('');
      this.fieldMatch2 = '';
    }
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getproduct();
    this.getcontentdetails();
    this.tabvalue = "tab-preventchange1";
  }
  nextTab1() {
    this.activeId = "tab-selectbyida";
    this.preId = "tab-selectbyidb";
    // if(this.blocknext==true){
    //   this.activeId = "tab-selectbyida";
    var data = { nextId: "tab-selectbyidb" };
    this.fetchNews(data);
    // }
    // else{
    //   this.activeId = "tab-selectbyidb";
    //   this.nextId = "tab-selectbyida";
    // }
  }
  nextTab(preId, currentId, nextId) {
    if (this.assesment_flag == 1) {
      this.preId = preId;
      this.currentId = currentId;
      this.nextId = nextId;
    }
    else {
      this.preId = preId;
      this.currentId = preId;
      this.nextId = preId;
    }
  }
  checkboxFunction(value) {
    return this.name.filter(checkvalue => checkvalue == value)
  }

  fetchNews(evt: any) {
    this.tabvalue = evt.nextId;
    if (this.tabvalue == "tab-preventchange1") {
      this.showTableContent = false;
      this.chRef.detectChanges();
      this.getcontentdetails();
    }
    else if (this.tabvalue == "tab-preventchange2") {
      this.showtableassessment = false;
      this.chRef.detectChanges();
      this.getassessmentdetails();
    }
    else if (this.tabvalue == "tab-selectbyidb") {
      this.chRef.detectChanges();
      this.getassessmentdetails();
    }
  }
  getproduct() {
    var url = 'get_product_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.product = result.response.data; //storing the api response in the array
        this.showTable = true;
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  onChangeProduct(product_id) {
    this.subproduct =[]
    var product = product_id; // country: number
    var url = 'get_subproduct_details/?'; // api url for getting the details with using post params
    var id = "product_id";
    this.ajax.getdataparam(url, id, product).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.subproduct = result.response.data;
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  //Functions for image cropper
  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedImage1: any = '';
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  //image upload after cropping
  upload_img() { //button to clear the cropper
    this.cropped_img = false;
    this.selectedfile = true;
  }
  upload_file() {
    this.cropped_img = false;
    this.selectedfile = true;
  }
  index: any;
  getcontentdetails() {
    this.overlayRef.attach(this.LoaderComponentPortal); // intialising datatable
    var url = 'get_training_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.content_details = result.response.data; //storing the api response in the array
        this.showTableContent = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  next() {
    this.activeId = "tab-selectbyida";
    this.preId = "tab-selectbyidb";
  }
  //--------------------------------------------------------------------------------------------------//
  getassessmentdetails() {
    // this.nextTab1();
    // this.activeId = "tab-selectbyida";
    // this.preId = "tab-selectbyidb";
    // this.overlayRef.attach(this.LoaderComponentPortal); // intialising datatable
    var url = 'get_assessment_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.assesment_details = result.response.data; //storing the api response in the array
        this.showtableassessment = true;
        this.chRef.detectChanges();
        // this.overlayRef.detach();
        this.question_assessment = [];
        for (let i = 1; i <= this.assesment_details[0].count; i++) {
          this.question_assessment.push(i);
        };
      }
      else if (result.response.response_code == "500") {
        this.showtableassessment = true;
        this.chRef.detectChanges();
        this.toastr.error(result.response.message, 'Error')
        // this.overlayRef.detach();
      }
      else {
        this.showtableassessment = true;
        this.chRef.detectChanges();
        this.toastr.error("Something went wrong", 'Error')
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
  }
  onmousedown() {
    // console.log(this.product);
    if (this.product.value) {
      if (this.product.value.length > 5) {
        this.size = 6;
      }
    }
    else {
      this.size = 0;
    }
  }
  onblur() {
    this.size = 0;
  }
  onchange() {
    this.size = 0;
  }
  getquestions(assessmt_id) {
    // this.overlayRef.attach(this.LoaderComponentPortal)
    this.assessmt_id = assessmt_id;
    var assessment_id = +assessmt_id
    var id = "assessment_id"; //id for identify warehouse name
    var url = 'get_quation_details/?'; // api url for getting the details with using post params
    this.ajax.getdataparam(url, id, assessment_id).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.questions = result.response.data;
        this.title = result.response.training_data;
        this.showquestion = true;
        this.chRef.detectChanges();
        // this.overlayRef.detach();
        this.blocknext = true;
        let frmArray = (this.ReferAssess.get('training_details_id') as FormArray)
        frmArray.clear();
        this.name = Array();
        for (var i = 0; i < this.title.length; i++) {
          if (this.name.includes(this.title[i].training_details_id)) {
          }
          else {
            this.name.push(this.title[i].training_details_id);
            (this.ReferAssess.get('training_details_id') as FormArray).push(
              this.fb.control(this.title[i].training_details_id))
          }
        }
        this.assesment_flag = 1; //storing the api response in the array
        this.warning = "";
        this.getassessmentdetails();
      }
      else if (result.response.response_code == "400") { //if sucess
        this.questions = result.response.data;
        this.title = result.response.training_data;
        this.showquestion = true;
        this.chRef.detectChanges();
        // this.overlayRef.detach();
        this.blocknext = false;
        this.warning = 'Add quiz to move next tab';
        // this.toastr.warning('', 'Warning'); //toastr message for error
      }
      else if (result.response.response_code == "500") { //if sucess
        this.showquestion = true;
        this.chRef.detectChanges();
        // this.overlayRef.detach();
        this.assesment_flag = 0; //storing the api response in the array
        this.questions = result.response.data;
        this.title = result.response.training_data;
        this.assesment_flag = 0; //storing the api response in the array
        this.blocknext = false;
        this.warning = "";
        // this.toastr.warning('Add quiz to move next tab', 'Warning'); //toastr message for error
      }
      else {
        this.warning = "";
        this.toastr.error("Something went wrong", 'Error');
        this.blocknext = false; //toastr message for error
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  //--------------------------------------------------------------------------------------------------//
  //functions for image upload
  imagefileUpload(event) {
    this.buttonmainedit = true;
    this.message = "";
    this.buttonVisible = true;
    this.load = "";
    this.selectedfile = false; //show confirm button
    this.cropped_img = true; // image cropper
    this.Filename = event.target.files[0].name;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.Filename = event.target.files[0].name;
      this.load = "Only Image will be allowed ( JPG | PNG | JPEG)";
      this.imageChangedEvent = '';
      this.buttonVisible = false;
      this.buttonmainedit = false;
      this.Addcontent.controls['training_thumb_image'].setValue('');
    }
    else {
      this.load = "";
      this.buttonVisible = true;
      this.buttonmainedit = true;
      this.imageChangedEvent = event.target.files[0];
      this.imageChangedEvent = event;
      this.image_name = event.target.files[0];
      this.Filename = event.target.files[0].name;
      this.ByDefault = true;
    }
    this.ByDefault = true;
  };
  //image cropping
  imageCropped(event: any): void {
    this.imageChangedEvent = event;
    this.imageChangedEvent = event.base64;
  }
  //functions for image/video upload
  videofileUpload(event) {
    this.error_types = "";
    this.correct = "";
    this.ByDefault1 = false;
    this.videopdf = event.target.files[0].name;
    if (this.Addcontent.value.training_content_type == 'pdf') {
      this.pdf = event.target.files[0].type;
      var validExts = new Array("application/pdf", ".pdf");
      if (this.pdf == undefined) {
        this.error_type = "Please select file";
        this.Addcontent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (validExts[0] != this.pdf) {
          this.error_type = "Please select correct extension";
          this.Addcontent.controls['training_content'].setValue('');
        }
        else {
          this.error_type = "";
        }
      }
      this.ByDefault1 = true;
      let fileList: FileList = event.target.files[0];
      this.videopdf_name = fileList;
      this.uploadname = event.target.files[0].name;
    }
    else if (this.Addcontent.value.training_content_type == 'video') {
      this.pdf = event.target.files[0].type;
      this.size = event.target.files[0].size;
      var validExts = new Array("video/mp4", ".mp4");
      const allowed_types = ['video/mp4'];
      if (!_.includes(allowed_types, this.pdf)) {
        this.error_type = "Only Video will be allowed ( mp4 )";
        this.Addcontent.controls['training_content'].setValue('');
      }
      else if (this.pdf == undefined) {
        this.error_type = "Please select file";
        this.Addcontent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (this.size > 15728640) {
          this.error_type = "Maximum size is 15 MB";
          this.Addcontent.controls['training_content'].setValue('');
        }
        else {
          this.error_type = "";
        }
      }
      this.ByDefault1 = true;
      let fileList: FileList = event.target.files[0];
      this.videopdf_name = fileList;

    }
    else if (this.Addcontent.value.training_content_type == 'word') {
      this.pdf = event.target.files[0].type;
      // console.log(this.pdf)
      // this.size = event.target.files[0].size;
      // var validExts = new Array("video/mp4", ".mp4");
      const allowed_types = ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'];
      if (!_.includes(allowed_types, this.pdf)) {
        this.error_type = "Only Video will be allowed ( docs,doc )";
        this.Addcontent.controls['training_content'].setValue('');
      }
      else if (this.pdf == undefined) {
        this.error_type = "Please select file";
        this.Addcontent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (this.size > 15728640) {
          this.error_type = "Maximum size is 20 MB";
          this.Addcontent.controls['training_content'].setValue('');
        }
        else {
          this.error_type = "";
        }
      }
      this.ByDefault1 = true;
      let fileList: FileList = event.target.files[0];
      this.videopdf_name = fileList;
    }
    else {
      this.error_type = '';
      this.error_types = "";
      let fileList: FileList = event.target.files[0];
      this.videopdf_name = fileList;
    }
    if (this.EditContent.value.training_content_type == 'pdf') {
      this.pdf = event.target.files[0].type;
      var validExts = new Array("application/pdf", ".pdf");
      if (this.pdf == undefined) {
        this.error_types = "Please select file";
        this.EditContent.value.invalid = true;
        this.EditContent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (validExts[0] != this.pdf) {
          this.error_types = "Please select correct extension";
          this.EditContent.value.invalid = true;
          this.EditContent.controls['training_content'].setValue('');
        }
        else {
          this.ByDefault1 = true;
          this.error_types = "";
          this.EditContent.controls['training_content_type'].setValue(this.EditContent.value.training_content_type);

        }
      }
    }
    else if (this.EditContent.value.training_content_type == 'video') {
      this.pdf = event.target.files[0].type;
      this.size = event.target.files[0].size;
      var validExts = new Array("video/mp4", ".mp4");
      const allowed_types = ['video/mp4'];
      if (!_.includes(allowed_types, this.pdf)) {
        this.error_types = "Only Video will be allowed ( mp4 )";
        this.EditContent.value.invalid = true;
        this.EditContent.controls['training_content'].setValue('');
      }
      else if (this.pdf == undefined) {
        this.error_types = "Please select file";
        this.EditContent.value.invalid = true;
        this.EditContent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (this.size > 15728640) {
          this.error_types = "Maximum size is 15 MB";
          this.EditContent.value.invalid = true;
          this.EditContent.controls['training_content'].setValue('');
        }
        else {
          this.ByDefault1 = true;
          this.error_types = "";
          this.EditContent.controls['training_content_type'].setValue(this.EditContent.value.training_content_type);

        }
      }
    }
    else if (this.EditContent.value.training_content_type == 'word') {
      this.pdf = event.target.files[0].type;
      var validExts = new Array("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
      if (this.pdf == undefined) {
        this.error_types = "Please select file";
        this.EditContent.value.invalid = true;
        this.EditContent.controls['training_content'].setValue('');
      }
      else if (this.pdf.length > 0) {
        if (validExts[0] != this.pdf) {
          this.error_types = "Please select correct extension";
          this.EditContent.value.invalid = true;
          this.EditContent.controls['training_content'].setValue('');
        }
        else {
          this.ByDefault1 = true;
          this.error_types = "";
          this.EditContent.value.invalid = false;
          this.EditContent.controls['training_content_type'].setValue(this.EditContent.value.training_content_type);

        }
      }

      //read the file
      let fileList: FileList = event.target.files[0];
      this.videopdf_name = fileList;

    }
  };
  fileChangeEvents(fileInput: any) {
    this.imageError = '';
    this.message = '';
    this.ByDefault = true;
    this.Filename = fileInput.target.files[0].name;
    if (this.Addcontent) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
          this.Addcontent.value.valid = false;
          // this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
          // this.Addcontent.value.valid = false;
          // this.cardImageBase64 = null;
          // this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
          this.Addcontent.value.valid = false;
          // this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
          // this.Addcontent.value.valid = false;
          // this.cardImageBase64 = null;
          // this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
              this.Addcontent.value.valid = false;
              // this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
              // this.Addcontent.value.valid = false;
              // this.cardImageBase64 = null;
              // this.isImageSaved = false;
            } else {
              let fileList: FileList = fileInput.target.files[0];
              this.image_name = fileList;
              // this.ByDefault = true;
              // this.Addcontent.value.valid = true;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
              this.Addcontent.value.valid = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
    if (this.EditContent) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.EditContent.controls['training_type'].setErrors({ 'incorrect': true });
          this.EditContent.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.EditContent.controls['training_type'].setErrors({ 'incorrect': true });
          this.EditContent.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.EditContent.controls['training_type'].setErrors({ 'incorrect': true });
              this.EditContent.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              // console.log(this.EditContent.value.training_type)
              this.EditContent.controls['training_type'].setValue(this.EditContent.value.training_type);
              this.EditContent.value.invalid = false;
              this.chRef.detectChanges();
              let fileList: FileList = fileInput.target.files[0];
              this.image_name = fileList;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
              // console.log(this.EditContent.value)
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }

  }



  removeImages() {

    if (this.EditContent) {
      // console.log(this.EditContent.value.training_type)
      this.EditContent.controls['training_type'].setErrors({ 'incorrect': true });
      this.EditContent.value.invalid = true;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
    }
    if (this.Addcontent) {
      this.isImageSaved = false;
      this.ByDefault = false;
      this.cardImageBase64 = null;
      this.message = "Image is required";
      this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
      this.Addcontent.value.valid = false;
      this.chRef.detectChanges();

      // this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
      // this.Addcontent.value.valid = false;
      // this.cardImageBase64 = null;
      // this.isImageSaved = false;
      // this.ByDefault = false;

      // this.Filename = ''
    }

  }
  //functions for bulk upload
  fileUpload(event) {
    this.type = '';
    this.Filename = '';
    this.file_name = '';
    this.bulk = [];
    //read the file
    this.error = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.Filename = event.target.files[0].name;
    this.type = event.target.files[0].type;
    this.ByDefault = true;
  };
  filesubmit(bulkupload) {
    this.bulk = [];
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) { //if file is not empty
        let file: File = this.file_name[0];
        const formData: FormData = new FormData(); //convert the formdata
        formData.append('excel_file', file); //append the name of excel file
        var method = "post"; //post method
        var url = 'quation_bulk_upload/' //post url
        var i: number; //i is the data
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") { //if sucess
            if (data['success'].length >= 0) {
              for (i = 0; i < data['success'].length; i++) {
                this.bulk = '';
                this.error = '';
                this.loading = false;
                this.showquestion = false;
                this.chRef.detectChanges();
                this.getquestions(this.assessmt_id);
                // this.toastr.success(data['success'], 'Success!')
                this.toastr.success(data['success'][i].Success_message, 'Success!');  //toastr message for Success
              }
            }
            if (data['response'].fail) {
              if (data['response'].fail.length > 0) {                                    //failure response 
                var a = [];
                data['response'].fail.forEach(value => {
                  a.push(value.error_message
                  );

                });

                this.bulk = a;
                // this.file_name = undefined;   //refresh the file 
                this.Filename = '';
                this.loading = false;
              }
            }
            // else if (data['response'].error_code == "404") {
            //   if (data['response'].fail.length > 0) {
            //     for (i = 0; i < data['response'].fail.length; i++) {
            //       if (data['response'].fail[i].error_message) {
            //         var a = [];
            //         data['response'].fail.forEach(value => {
            //           a.push(value.error_message);
            //         });
            //         this.bulk = a;
            //       }
            //     }
            //   }
            //   this.loading = false;
            // }
          }
          else if (data['response'].response_code == "400") { //if failure
            this.loading = false;
            this.bulk = '';
            this.error = data['response'].message
          }
          else if (data['response'].response_code == "500") { //if failure
            this.loading = false;
            this.bulk = '';
            this.error = data['response'].message
          }
          else { //if not sucess
            this.loading = false;
            this.bulk = '';
            this.error = "Something went wrong.";
          }
        },
          (err) => { //if error
            this.bulk = '';
            this.error = "Something went wrong"          //prints if it encounters an error
            this.loading = false;
          }
        );

      }
    }
  };
  fileChangeVideo(event: any): void {
    if (event.target.files.length !== 1) {
      this.videos = "Multiple files not allowed";
    }
    const allowed_types = ['video/mp4', 'image/png'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.videos = "Only Video will be allowed ( mp4 |png )";
    }
    else {
      this.videos = ""
      this.imageChanged = event.target.files[0];
      this.upload_video = this.imageChanged
    }
  }
  // function for modal box
  openeditLarge(add, row) {
    this.cursor = false;
    // this.showtableassessment = false;
    // this.chRef.detectChanges();
    this.activeId = "tab-selectbyida";
    this.warning = "";
    this.currentId = "tab-selectbyid1";
    this.assessment_id = row
    this.AddQuiz.controls['assessment_id'].setValue(this.assessment_id)
    this.EditQuestion.controls['assessment_id'].setValue(this.assessment_id)
    this.modalService.open(add, {
      size: 'lg', //specifies the size of the dialog box
      // windowClass: "center-modallg", // modal popup resize
      backdrop: 'static',
      keyboard: false,
      centered: true
    });
  }
  openLarge(add) {
    this.ByDefault = false;
    this.ByDefault1 = false;
    this.trainning_error = '';
    this.cardImageBase64 = '';
    this.spare_image = '';
    this.isImageSaved = false;
    this.imageError = null;
    this.bulk = '';
    this.error = '';
    this.message = "";
    this.buttonVisible = false;
    this.type = '';
    this.correct = "";
    this.AddAsessment.reset();
    this.add_assess = '';
    this.activeId = "tab-selectbyid1";
    this.AddQuiz.reset();
    this.loadingAdd = false;
    this.loadingEdit = false;
    this.loadingContent = false;
    this.videopdf = '';
    this.fieldMatch = '';
    this.fieldMatch1 = '';
    this.fieldMatch2 = '';
    this.fieldMatch3 = '';
    this.error_types = "";
    this.error_type = "";
    this.load = "";
    this.Addcontent.reset();
    this.file_name = undefined;                 //refresh the file
    this.Filename = ''                           //regresh the file name
    this.imageChangedEvent = '';
    this.EditContent.controls['training_thumb_image'].setValue('');
    this.image_name = '';
    this.modalService.open(add, {
      size: 'lg', //specifies the size of the dialog box
      windowClass: "center-modalsm", //modal popup resize
      backdrop: 'static',
      keyboard: false,
      centered: true
    });
    this.Filename = '';
  }
  openmodal(view_content, image_or_video) {
    this.image_or_video = image_or_video
    this.modalService.open(view_content, {
      size: 'lg', //specifies the size of the modal box
      windowClass: "center-modalsm",
      backdrop: 'static',
      keyboard: false
    });
  }
  // error controls for Add content
  get log() {
    return this.Addcontent.controls;
  }
  get logse() {
    return this.EditContent.controls;
  }
  // error controls for Add assessment
  get logs() {
    return this.AddAsessment.controls;
  }
  get edit() {
    return this.Editassessment.controls;
  }
  get logq() {
    return this.AddQuiz.controls;
  }
  get editq() {
    return this.EditQuestion.controls;
  }
  //Function to call the add api
  add_content_data(content) {
    this.submitted = true;
    this.loadingAdd = true;
    this.loadingContent = true;
    if (this.load != "") {

    }
    else {
      if (this.error_type != "") {
        this.loadingAdd = false;
        this.loadingContent = false;
      }
      else {
        var val = this.Addcontent.value;
        const formData: FormData = new FormData(); //convert the formdata
        formData.append('training_title', val.training_title);
        formData.append('training_type', val.training_type);
        formData.append('training_content_type', val.training_content_type);
        formData.append('training_content', this.videopdf_name);
        formData.append('training_thumb_image', this.image_name);
        formData.append('product_id', val.product_id);
        formData.append('product_sub_id', val.product_sub_id);
        var method = "post";
        var data = formData;
        var url = 'add_training/' //api url of add
        this.ajax.ajaxpost(data, method, url).subscribe(data => {
          if (data['response'].response_code == "200") {
            this.trainning_error = '';
            this.toastr.success(data['response'].message, 'Success'); //toastr message for success
            this.loadingAdd = false;
            this.loadingContent = false;
            this.showTableContent = false;
            this.chRef.detectChanges();
            this.getcontentdetails(); //reloading the component
            this.Addcontent.reset();
            this.modalService.dismissAll();
          }

          else if (data['response'].response_code == "400") {
            this.trainning_error = data['response'].message;
            this.loadingAdd = false;
            this.loadingContent = false;
          }
          else if (data['response'].response_code == "500") {
            this.trainning_error = data['response'].message;
            this.loadingAdd = false;
            this.loadingContent = false;
          }
          else {
            this.trainning_error = "Something went wrong";
            this.loadingAdd = false;
            this.loadingContent = false;
          }
        }, (err) => {

          this.loadingContent = false;
          console.log(err); //prints if it encounters an error
        });
      }

    }
  }
  doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
  }

  //Function to store the line item selected to edit
  content_data(fdata, edit_content) {
    this.cardImageBase64 = '';
    this.spare_image = '';
    this.trainning_error = '';
    this.buttonmainedit = true;
    this.imageShown = false;
    this.content_edit_data = fdata;
    // var img_url = "http://" + this.content_edit_data.training_thumb_image
    var data = {
      'training_id': fdata.training_id,
    }
    // if (this.load == "") {
    var url = 'get_training_data/?'; // api url for getting the cities
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.content_edit_data = result.response.data;
        this.chRef.detectChanges();
        this.ByDefault = true;
        this.Filename = 'Image';
        this.isImageSaved = true;
        this.spare_image = environment.image_static_ip + this.content_edit_data.training_thumb_image
        if (this.content_edit_data.training_content_type) {
          if (this.content_edit_data.training_content_type == 'pdf') {
            this.ByDefault1 = true;
            this.videopdf = 'Pdf';
          }
          else if (this.content_edit_data.training_content_type == 'video') {
            this.ByDefault1 = true;
            this.videopdf = 'Video';
          }
          else if (this.content_edit_data.training_content_type == 'word') {
            this.ByDefault1 = true;
            this.videopdf = 'Word';
          }
        }
        else {
          this.ByDefault1 = true;
          this.videopdf = '';
        }
        // this.imageChangedEvent = img_url;
        this.EditContent.setValue({
          "training_id": this.content_edit_data.training_id,
          "training_details_id": this.content_edit_data.training_details_id,
          "training_title": this.content_edit_data.training_title,
          "training_type": this.content_edit_data.training_type,
          "training_content_type": this.content_edit_data.training_content_type,
          "training_content": '',
          "training_thumb_image": '',
          "product_id": this.content_edit_data.product.product_id,
          "product_sub_id": this.content_edit_data.subproduct.product_sub_id,
        });
        this.modalService.open(edit_content, {
          size: 'lg', //specifies the size of the dialog box
          windowClass: "center-modalsm", //modal popup resize
          backdrop: 'static',
          keyboard: false,
          centered: true
        });
        if (this.content_edit_data.training_type == "Product Training") {
          this.onChangeProduct(this.content_edit_data.product.product_id);
        }



      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
      }
      else {
        this.toastr.error("Something went wrong", 'Error');
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });
    // }

    // else if (this.content_edit_data.product.product_id == "-") {
    //   // this.edit_type="others";
    // }
    // else {
    //   this.edit_type = "Product Training";
    //   this.onChangeProduct(this.content_edit_data.product.product_id);
    // }
  }

  //Function to call the edit api
  editcontent() {
    // this.submitted = true;

    // if (this.load != "") {
    // }
    // else {
    //   if (this.error_types != "") {
    //   }
    //  if (this.correct != "") {
    //   }
    //   else {
    this.loadingEdit = true;
    var val = this.EditContent.value;
    const formData: FormData = new FormData(); //convert the formdata
    formData.append('training_id', val.training_id);
    formData.append('training_title', val.training_title);
    formData.append('training_content_type', val.training_content_type);
    if (val.training_thumb_image == undefined || val.training_thumb_image == '/assets/images/image-not-found.jpg' || val.training_thumb_image == '') {
      formData.append('training_thumb_image', '');
    }
    else {
      formData.append('training_thumb_image', this.image_name);
    }
    if (val.training_content == undefined || val.training_content == 'undefined' || val.training_content == "undefined" || val.training_content == '') {
    }
    else {
      formData.append('training_content', this.videopdf_name);
    }
    formData.append('training_type', val.training_type);
    formData.append('product_id', val.product_id);
    formData.append('product_sub_id', val.product_sub_id);

    var method = "post";
    var data = formData;
    var url = 'edit_training/' //api url of add
    this.ajax.ajaxpost(data, method, url).subscribe(data => {
      if (data['response'].response_code == "200") {
        this.trainning_error = '';
        this.toastr.success(data['response'].message, 'Success'); //toastr message for success
        this.loadingEdit = false;
        this.showTableContent = false;
        this.chRef.detectChanges();
        this.getcontentdetails(); //reloading the component
        this.modalService.dismissAll();

      }
      else if (data['response'].response_code == "400") {
        this.trainning_error = data['response'].message;
        this.loadingEdit = false;
      }
      else if (data['response'].response_code == "500") {
        this.trainning_error = data['response'].message;
        this.loadingEdit = false;
      }
      else {
        this.trainning_error = "Something went wrong";
        this.loadingEdit = false;
      }
    }, (err) => {
      console.log(err); //prints if it encounters an error
    });

    // }
    // }
  }
  create_asessment() {
    this.submitted = true;
    this.loadingCrt = true;
    // stop here if form is invalid
    if (this.AddAsessment.invalid) {
      this.toastr.error("Please fill the assessment", 'Error');
      return;
    }
    else {
      var data = this.AddAsessment.value; //Data to be passed for add api
      var url = 'create_assessment/' //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.loadingCrt = false;
          this.AddAsessment.reset();
          this.showtableassessment = false;
          this.chRef.detectChanges();
          this.getassessmentdetails();
          this.modalService.dismissAll();
        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingCrt = false;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingCrt = false;
        }
        else {
          this.toastr.error("something went wrong", 'Error');
          this.loadingCrt = false;
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });
    }
  }

  //data for edit assessment
  assessment_data(data) {
    this.showquestion = false;
    this.chRef.detectChanges();
    this.assesment = data;
    this.are = this.assesment.assessment_type;

    this.Editassessment = new FormGroup({
      "assessment_id": new FormControl(this.assesment.assessment_id, [Validators.required]),
      "assessment_name": new FormControl(this.assesment.assessment_name, [Validators.required]),
      "assessment_type": new FormControl(this.are, [Validators.required]),
      "quation_to_assessment": new FormControl(this.assesment.quation_to_assessment, [Validators.required]),
      "threshold": new FormControl(this.assesment.threshold, [Validators.required]),
      "product_id": new FormControl(this.assesment.product.product_id),
      "product_sub_id": new FormControl(this.assesment.subproduct.product_sub_id),
    });
    this.getThreshold(this.assesment.quation_to_assessment);
    if (this.are == "Product Assessment") {
      this.onChangeProduct(this.assesment.product.product_id);
    }

    this.question_assessment = [];
    for (let i = 1; i <= data.count; i++) {
      this.question_assessment.push(i);
    }
  }
  getThreshold(data) {
    var data = this.Editassessment.value.quation_to_assessment;
    this.threshold = [];
    for (let i = 1; i <= data; i++) {
      this.threshold.push(i);
    }
  }
  getreference(product) {
    if (this.are == "Generic Assessment") {
      this.are = "1";
    }
    else if (this.are == "Product Assessment") {
      this.are = "2";
    }
    else {
      this.are = "3";
    }
    var data = { "assessment_id": this.assesment.assessment_id, "assessment_type": this.are, "product_id": product, "product_sub_id": this.assesment.subproduct.product_sub_id }
    var url = 'assessement_reference_details/';
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (this.are == "1") {
          this.are = "Generic Assessment";
        }
        else if (this.are == "2") {
          this.are = "Product Assessment";
        }
        else {
          this.are = "Technology Assessment";
        }
        if (result.response.response_code == "200") //if sucess
        {
          this.reference = result.response.data; //to close the modal box
        }
        else if (result.response.response_code == "400") { //if failure
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
        }
        else if (result.response.response_code == "500") { //if not sucess
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
        }
        else {
          this.toastr.error("Something went wrong", 'Error')
        }
      }, (err) => { //if error
        console.log(err); //prints if it encounters an error
      });
  }

  onCheckboxChange(e, ischecked: any) {
    const checkArray: FormArray = this.ReferAssess.get('training_details_id') as FormArray;
    var id = +e.target.value;
    if (ischecked) {
      checkArray.push(
        this.fb.control(id))
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {

        if (item.value == id) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    this.checkdata = checkArray;
  }
  submitForm() {
    this.loadingRef = true;
    // this.submitted = true;   
    var data = { "assessment_id": this.assesment.assessment_id, "training_reference": this.ReferAssess.value.training_details_id }
    var url = 'update_assessment_reference/';
    this.ajax.postdata(url, data)
      .subscribe((result) => {
        if (result.response.response_code == "200") //if sucess
        { //reloading the component        
          this.loadingRef = false;
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.modalService.dismissAll(); //to close the modal box
        }
        else if (result.response.response_code == "400") { //if failure
          this.response_message = result.response.message;
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingRef = false;
        }
        else if (result.response.response_code == "500") { //if not sucess
          this.response_message = result.response.message;
          // this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingRef = false;
        }
        else {
          this.response_message = "Something went wrong";
          // this.toastr.error("Something went wrong", 'Error')
          this.loadingRef = false;
        }
      }, (err) => { //if error
        console.log(err); //prints if it encounters an error
      });
  }
  //function for edit the assessment
  editassessment(assess) {

    this.submitted = true;
    var data = this.Editassessment.value;
    data["assessment_type"] = this.assesment.assessment_type;
    if (data.invalid) {
      this.toastr.error("Please select the dropdown", 'Error');
    }
    else {
      if (data.quation_to_assessment >= data.threshold) {
        this.getreference(this.Editassessment.value.product_id);
        this.loadingSave = true;
        var url = 'edit_assessment/' //api url of edit api
        this.ajax.postdata(url, data)
          .subscribe((result) => {
            if (result.response.response_code == "200") //if sucess
            {
              this.loadingSave = false;
              // this.toastr.success(result.response.message, 'Success'); //toastr message for success
              this.showtableassessment = false;
              this.chRef.detectChanges();
              this.getassessmentdetails();
            }
            else if (result.response.response_code == "400") { //if failure
              // this.blocknext = true;
              this.toastr.error('Add quiz to move next tab', 'Warning'); //toastr message for error
              this.loadingSave = false;
            }
            else if (result.response.response_code == "500") { //if not sucess
              this.toastr.error(result.response.message, 'Error'); //toastr message for error
              this.loadingSave = false;
            }
            else {
              this.toastr.error("something went wrong", 'Error');
              this.loadingSave = false;
            }
          }, (err) => { //if error
            this.loadingSave = false;
            console.log(err); //prints if it encounters an error
          });
      }
      else {
        this.toastr.error("Threshold is greater than quation to assessment ", 'Error');
      }
    }
  }
  //function for create the question /quiz
  create_quiz(data) {
    this.submitted = true;
    this.loading = true;
    if (this.fieldMatch == '' && this.fieldMatch1 == '' && this.fieldMatch2 == '' && this.fieldMatch3 == '') {
      var data = this.AddQuiz.value; //Data to be passed for add api
      data["assessment_id"] = this.assessmt_id;
      var url = 'add_quation/' //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.loading = false;
          this.AddQuiz.reset(
            {
              "assessment_id": this.AddQuiz.value.assessment_id
            }
          )
          this.showquestion = false;
          this.chRef.detectChanges();
          this.getquestions(this.AddQuiz.value.assessment_id);



        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loading = false;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loading = false;
        }
        else {
          this.toastr.error("Something went wrong", 'Error');
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error
      });

    }
    else {
    }

  }
  // function for delete the content
  detele_trainning(data) {
    var training_id = { "training_id": data };
    var url = 'delete_training/'; // api url for delete warehouse
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, training_id).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showTableContent = false;
            this.chRef.detectChanges();
            this.getcontentdetails();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }

  //function for delete assessment
  delete_assessment(assess_id) {
    var assessmen_id = assess_id;
    var assessment_id = { assessment_id: +assessmen_id }; //get the product id for identify the product name
    var url = 'delete_assessment/?'; // api url for getting the details with using post params
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, assessment_id).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showtableassessment = false;
            this.chRef.detectChanges();
            this.getassessmentdetails();
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") { //if failure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }
  delete_question(questin_id) {
    var questn_id = questin_id;
    var quation_id = { quation_id: +questn_id }; //get the product id for identify the product name
    var url = 'delete_quation/'; // api url for getting the details with using post params
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, quation_id).subscribe((result) => {
          if (result.response.response_code == "200") { //if sucess
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.showquestion = false;
            this.chRef.detectChanges();
            this.getquestions(this.assessmt_id);
          }
          else if (result.response.response_code == "400") { //if failiure
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else if (result.response.response_code == "500") { //if not sucess
            this.toastr.error(result.response.message, 'Error'); //toastr message for error
          }
          else {
            this.toastr.error("Something went wrong", 'Error');
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error
        });
      }
    })
  }

  removeImage() {
    this.Filename = '';
    this.croppedImage = '';
    this.training_thumb_images = '';
    this.imageShown = !this.imageShown;
  }


  //Function to store the line item selected to edit
  editquestion(data) {
    this.EditQuestion = new FormGroup({
      "assessment_id": new FormControl(this.assessment_id, [Validators.required]),
      "quation_id": new FormControl(data.quation_id, [Validators.required]),
      "quation": new FormControl(data.quation, [Validators.required, this.noWhitespace]),
      "option_a": new FormControl(data.option_a, [Validators.required, this.noWhitespace]),
      "option_b": new FormControl(data.option_b, [Validators.required, this.noWhitespace]),
      "option_c": new FormControl(data.option_c, [Validators.required, this.noWhitespace]),
      "option_d": new FormControl(data.option_d, [Validators.required, this.noWhitespace]),
      "correct_answer": new FormControl(data.correct_answer, [Validators.required])
    }); //Stores the data into the array variable
  }
  edit_question(edit_assessment) {
    this.loadingSub = true;
    this.submitted = true;
    // stop here if form is invalid
    if (this.EditQuestion.invalid) {
      return;
    }
    else {
      var data = this.EditQuestion.value; //Data to be passed for add api
      var url = 'edit_quation/' //api url of add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.loadingSub = false;
          this.showquestion = false;
          this.chRef.detectChanges();
          this.modalService.dismissAll(); //to close the modal box
          this.getquestions(this.assessmt_id); //reloading the component
          this.modalService.open(edit_assessment, {
            size: 'lg', //specifies the size of the dialog box
            // windowClass: "center-modalsm", //modal popup resize
            backdrop: 'static',
            keyboard: false,
            centered: true
          });
        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error'); //toastr message for error
          this.loadingSub = false;
        }
        else if (result.response.response_code == "500") {
          this.toastr.error(result.response.message, ' Error');
          this.loadingSub = false;
        }
        else {
          this.toastr.error("something went wrong", 'Error')
          this.loadingSub = false;
        }
      }, (err) => {
        this.loadingSub = false;
        console.log(err); //prints if it encounters an error
      });
    }
  }
  downloadPdf(pdfName: string, pdfUrl: string) {
    // console.log(pdfName, "pdfName")
    // console.log(pdfUrl, "pdfUrl")
    // var pdf = "https://" + pdfUrl
    var pdf = environment.image_static_ip + pdfUrl
    // console.log(pdf)
    FileSaver.saveAs(pdf, pdfName);
  }
  imageClear() {
    this.ByDefault = false;
    this.imageChangedEvent = '';

    this.buttonVisible = !this.buttonVisible;
    this.buttonmainedit = !this.buttonmainedit;
    this.message = "please select the image";
    this.Addcontent.value.valid = false;
    this.Addcontent.controls['training_thumb_image'].setErrors({ 'incorrect': true });
    this.EditContent.value.valid = false;

  }
}


