import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
// import { FormsModule } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { ActivatedRoute, Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
//image cropper
@Component({
    templateUrl: './managesubscription.component.html',
	encapsulation: ViewEncapsulation.None,
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})
export class ManagesubscriptionComponent implements OnInit {
  Setcharges: FormGroup;
  Setcharges1: FormGroup;
  subscription_details:any;
	closeResult: string;
	closeResult2: string;
  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router, private toastr: ToastrService) {  }
  
// modal box for renewal button
	openLarge(content6) {
		this.modalService.open(content6, {
			size: 'lg'
		});
	}
  // icon modal 
  onclick1(display1){
    this.modalService.open(display1, {
      size: 'sm',
      
		});
  }  
  //view info modal
  openLarge1(contentmodal){
    this.modalService.open(contentmodal, {
			size: 'lg'
		});

  }
  //service upgrade modal
  openLarge2(contentmodal2){
    this.modalService.open(contentmodal2, {
			size: 'lg'
		});

  }

  //renew on demand modal

  openLarge3(contentmodal3){
    this.modalService.open(contentmodal3, {
			size: 'lg'
		});

  }




	ngOnInit() {
// form group for service upgrade control
    this.Setcharges = new FormGroup({
      "service_desk": new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
      "technician_required": new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
    });
    // form group for renew on demand category control


    this.Setcharges1 = new FormGroup({
      "due_days": new FormControl("", [Validators.required,Validators.pattern("^[0-9]*$")]),
    });
  

this.get_subscription_details();
	
  }
  get_subscription_details()
	{
		var url = 'get_package_details/';                                   // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {            
		  if (result.response.response_code == "200") 
		  {           
			this.subscription_details = result.response.data;                    //storing the api response in the array
		
		  }
		}, (err) => {
		  console.log(err);                                                 //prints if it encounters an error
		});
	}
  //service upgrade control
  get log() {
		return this.Setcharges.controls;
  }

  //renew on demand category control
  get log1() {
		return this.Setcharges1.controls;
  }

// sweetalert for block button
  onclick(){

    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to Blog the User?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Block',
      cancelButtonText: 'No, Cancel'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Alert',
          'Blocked Successfully.',
          'success'
        )

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'User is safe :)',
          'error'
        )
      }
    })
  }

  // swetalert for unblock button
  onClick1(){

    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to UnBlog the User!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, UnBlock',
      cancelButtonText: 'No, Cancel'
    }).then((result) => {
      if (result.value) {
        Swal.fire({
         title: 'Alert',
          text:'Your Subscription has Expired, Renew to Continue Service!',
          icon:'warning',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel'
        }
          
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'User is safe :)',
          'error'
        )
      }
    })
  }
}