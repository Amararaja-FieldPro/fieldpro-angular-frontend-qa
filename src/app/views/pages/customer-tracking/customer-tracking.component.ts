import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
// import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../excelservice';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { LoaderComponent } from '../../pages/loader/loader.component';
import { ComponentPortal } from '@angular/cdk/portal';
@Component({
  selector: 'kt-customer-tracking',
  templateUrl: './customer-tracking.component.html',
  styleUrls: ['./customer-tracking.component.scss']
})
export class CustomerTrackingComponent implements OnInit {
  from: NgbDateStruct;
  to: NgbDateStruct;
  service_group: any;
  techId: any;
  assesment_data: any;
  visit_count: any;
  from_date: any;
  to_date: any;
  summary_report: [];
  employee_code: any;
  filterSummary: FormGroup;    //form group 
  products: any;
  locations: any;
  alltech: any;
  myDate: any;
  toDate: any;
  spares: any;
  spare_request_details: any;
  product_id: any;
  today: any;
  status: any;
  service_id: any;
  showTable: boolean = false;
  dataTable: any;
  loadingSub: boolean = false;    //button spinner by Sowndarya
  dtInstance = {};
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @ViewChild(DataTableDirective, { static: false })

  @ViewChild('dataTable', { static: true }) table;
  customer_code: any;
  constructor(private ajax: ajaxservice, private renderer: Renderer2, private modalService: NgbModal, private chRef: ChangeDetectorRef, private overlay: Overlay, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar) {
    this.from = this.calendar.getToday();
    this.to = this.calendar.getToday();
    this.filterSummary = new FormGroup({
      "customer_code":new FormControl(""),
      "from_date": new FormControl(this.from, Validators.required),
      "to_date": new FormControl(this.to, Validators.required),
      "visit_count": new FormControl('', Validators.required),
      "product_id": new FormControl('', Validators.required),
      "status": new FormControl('', Validators.required),
      "service_id": new FormControl('', Validators.required),
    })
  }


  ngOnInit() {

    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
    this.today = Fdate;
    var params = {
      "visit_count": null,
      "product_id": null,
      "to_date": Fdate,
      "from_date": Fdate,
      'status': null,
      'service_id': null,
      'customer_code':localStorage.getItem('customer_code')
    }
    this.getSummary(params,0);
    this.customer_code = localStorage.getItem('customer_code')
    // this.get_summary_report(this.customer_code)
    this.get_ticket_product_location();
    this.getServices();

  }
  exportAsXLSX(): void {
    this.excelservice.exportAsExcelFile(this.summary_report, 'summary_report');
  }

  getServices() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var employee_code = "employee_code";
    var url = 'get_servicegroup/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.service_group = result.response.data;
      }
      else if (result.response.response_code == "500") {

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  spareDetails(data) { //get Spare Details
    this.spares = data;
    // console.log(data);
    var tk_id = this.spares.ticket_id;
    var ticket_id = "ticket_id";
    var url = 'spare_details/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.spare_request_details = result.response.data; //bind result in this variable
        // console.log(this.spare_request_details);
      }
      else if (result.response.response_code == "500") {

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.products = result.response.product;              //storing the api response in the array
      }
      else if (result.response.response_code == "500") {
        // console.log(result.response.message);
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  filterSummaryF() {

    this.from_date = this.filterSummary.value.from_date;
    this.to_date = this.filterSummary.value.to_date;
    this.status = this.filterSummary.value.status;
    this.product_id = this.filterSummary.value.product_id;
    this.visit_count = this.filterSummary.value.visit_count;
    this.service_id = this.filterSummary.value.service_id;
    if (this.filterSummary.value.from_date.year == undefined) {
      this.filterSummary.value.from_date = this.today;
    }
    else {
      var Fyear = this.filterSummary.value.from_date.year;
      var Fmonth = this.filterSummary.value.from_date.month;
      var Fday = this.filterSummary.value.from_date.day;
      this.filterSummary.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
    }
    if (this.filterSummary.value.to_date.year == undefined) {
      this.filterSummary.value.to_date = this.today;
    }
    else {
      var Tyear = this.filterSummary.value.to_date.year;
      var Tmonth = this.filterSummary.value.to_date.month;
      var Tday = this.filterSummary.value.to_date.day;
      this.filterSummary.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;

    }
    if (this.product_id == "") {
      this.filterSummary.value.product_id = null;
    }
    else {
      this.filterSummary.value.product_id = +this.filterSummary.value.product_id;
    }
    if (this.service_id == "") {
      this.filterSummary.value.service_id = null;
    }
    else {
      this.filterSummary.value.service_id = +this.filterSummary.value.service_id;
    }
    if (this.status == "" || this.status == '0' || this.status == null) {
      this.filterSummary.value.status = null;
    }
    else {
      var status = +this.filterSummary.value.status
      this.filterSummary.value.status = status;
    }
    if (this.visit_count == "") {
      this.filterSummary.value.visit_count = null;
    }
    else {
      this.filterSummary.value.visit_count = +this.filterSummary.value.visit_count;
    }
    this.filterSummary.value.customer_code = this.customer_code;
    this.showTable = false;
    this.chRef.detectChanges();
    this.getSummary(this.filterSummary.value,1);
  }

  getSummary(data,type) {
 

    // console.log(data)
    // console.log(this.customer_code)
    data['customer_code'] = localStorage.getItem('customer_code')
    var url = 'customer_summary_report/';                                  // api url for getting the cities
    if(type == 1){
      this.loadingSub = true; 
    }

    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {    
        this.overlayRef.detach();
        this.summary_report = result.response.data;
        this.showTable = true;
        this.chRef.detectChanges();

        this.loadingSub = false;
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.loadingSub = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');
        this.showTable = true;
        this.chRef.detectChanges();
        this.loadingSub = false;
      }
    }, (err) => {
      console.log(err);
      this.loadingSub = false;                                       //prints if it encounters an error
    });
  }
  openLarge(content6) {
    this.modalService.open(content6, {
      size: 'lg'
    });
  }
}
