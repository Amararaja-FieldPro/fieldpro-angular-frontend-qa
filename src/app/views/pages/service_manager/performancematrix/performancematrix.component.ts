import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
	selector: 'kt-performancematrix',
	templateUrl: './performancematrix.component.html',
	styleUrls: ['./performancematrix.component.scss']
})
export class PerformancematrixComponent implements OnInit {
	employee_code: any;
	performance_update: any;
	performance_matrix_data: any;
	performance_config_data: any;
	performanceMatrix: FormGroup;    //form group 
	showTable: boolean = false;
	loadingSub: boolean = false; // btn spinner 
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	formData: any;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
		this.performanceMatrix = new FormGroup({
			"average_calls_reward": new FormControl('', [Validators.required, Validators.minLength(0), Validators.maxLength(2)]),
			"average_calls_target": new FormControl('', [Validators.required, Validators.minLength(0), Validators.maxLength(2)]),
			"contract_revenue_target": new FormControl('', Validators.required),
			"contract_revenue_reward": new FormControl('', Validators.required),
			"contribution_to_kb_reward": new FormControl('', Validators.required),
			"contribution_to_kb_target": new FormControl('', Validators.required),
			"cust_feed_target": new FormControl('', Validators.required),
			"cust_feed_reward": new FormControl('', Validators.required),
			"customer_rating_reward": new FormControl('', Validators.required),
			"customer_rating_target": new FormControl('', Validators.required),
			"first_time_reward": new FormControl(0, [Validators.required, Validators.minLength(0), Validators.maxLength(2)]),
			"first_time_target": new FormControl(0, [Validators.required, Validators.minLength(0), Validators.maxLength(2)]),
			"mttr_reward": new FormControl('', Validators.required),
			"mttr_target": new FormControl('', Validators.required),
			"sla_adher_reward": new FormControl('', Validators.required),
			"sla_adher_target": new FormControl('', Validators.required),
			"training_certification_reward": new FormControl('', Validators.required),
			"training_certification_target": new FormControl('', Validators.required),
			"technician_level": new FormControl('', Validators.required),
		});
	}
	get log() {
		return this.performanceMatrix.controls;                       //error logs for create user
	}
	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.overlayRef.attach(this.LoaderComponentPortal);
		this.getPerformanceMatrix();
	}
	getPerformanceMatrix() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'get_target_reward_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.performance_matrix_data = result.response.data;
				this.showTable = true;
				this.overlayRef.detach();
				this.chRef.detectChanges();
				this.dtTrigger.next();
				console.log(this.performance_matrix_data);

			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	updateConfig() {
		console.log(this.performanceMatrix.value);
		if (this.performanceMatrix.value.average_calls_reward == null || this.performanceMatrix.value.average_calls_target == null || this.performanceMatrix.value.contract_revenue_target == null ||
			this.performanceMatrix.value.contract_revenue_reward == null
			|| this.performanceMatrix.value.contribution_to_kb_reward == null || this.performanceMatrix.value.contribution_to_kb_target == null || this.performanceMatrix.value.cust_feed_reward == null
			|| this.performanceMatrix.value.cust_feed_target == null || this.performanceMatrix.value.customer_rating_reward == null || this.performanceMatrix.value.customer_rating_target == null
			|| this.performanceMatrix.value.first_time_reward == null || this.performanceMatrix.value.first_time_target == null || this.performanceMatrix.value.mttr_reward == null
			|| this.performanceMatrix.value.mttr_target == null || this.performanceMatrix.value.sla_adher_reward == null || this.performanceMatrix.value.sla_adher_target == null
			|| this.performanceMatrix.value.technician_level == null || this.performanceMatrix.value.training_certification_reward == null || this.performanceMatrix.value.training_certification_target == null
		) {
			this.toastr.error('Please fill the required fields', 'error');        //toastr message for success

		} else {
			this.loadingSub = true;
			console.log(this.performanceMatrix.value)
			var url = 'update_target_reward_configuration/'; //API url for View Form
			var data = this.performanceMatrix.value;
			console.log(this.formData);
			this.ajax.postdata(url, data).subscribe((result) => {
				if (result.response.response_code == "200") {
					this.performance_update = result.response.data; //bind result in this variable
					// this.ngOnInit();                                                //reloading the component
					this.toastr.success(result.response.message, 'Success');        //toastr message for success
					this.loadingSub = false;
					this.modalService.dismissAll();
					this.getPerformanceMatrix();                                //to close the modal box
					this.chRef.detectChanges();
					this.dtTrigger.next();
				}
			}, (err) => {
				console.log(err);                                        //prints if it encounters an error
			});
			console.log(this.performanceMatrix.value);
		}


	}
	getPerformanceConfig(level) {
		var level = level; // get employee code 
		var technician_level = "technician_level";
		var url = 'get_target_reward_configuration/?';                                  // api url for getting the reimbursment reject details
		this.ajax.getdataparam(url, technician_level, level).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.performance_config_data = result.response.data;
				console.log(this.performance_config_data);

				this.performanceMatrix.controls['average_calls_reward'].setValue(this.performance_config_data.average_calls_reward);

				this.performanceMatrix.controls['average_calls_target'].setValue(this.performance_config_data.average_calls_target);

				this.performanceMatrix.controls['contract_revenue_target'].setValue(this.performance_config_data.contract_revenue_target);

				this.performanceMatrix.controls['contract_revenue_reward'].setValue(this.performance_config_data.contract_revenue_reward);

				this.performanceMatrix.controls['contribution_to_kb_reward'].setValue(this.performance_config_data.contribution_to_kb_reward);

				this.performanceMatrix.controls['contribution_to_kb_target'].setValue(this.performance_config_data.contribution_to_kb_target);

				this.performanceMatrix.controls['cust_feed_reward'].setValue(this.performance_config_data.cust_feed_reward);

				this.performanceMatrix.controls['cust_feed_target'].setValue(this.performance_config_data.cust_feed_target);

				this.performanceMatrix.controls['customer_rating_reward'].setValue(this.performance_config_data.customer_rating_reward);

				this.performanceMatrix.controls['customer_rating_target'].setValue(this.performance_config_data.customer_rating_target);

				this.performanceMatrix.controls['first_time_reward'].setValue(this.performance_config_data.first_time_reward);

				this.performanceMatrix.controls['first_time_target'].setValue(this.performance_config_data.first_time_target);

				this.performanceMatrix.controls['mttr_reward'].setValue(this.performance_config_data.mttr_reward);

				this.performanceMatrix.controls['mttr_target'].setValue(this.performance_config_data.mttr_target);

				this.performanceMatrix.controls['sla_adher_reward'].setValue(this.performance_config_data.sla_adher_reward);

				this.performanceMatrix.controls['sla_adher_target'].setValue(this.performance_config_data.sla_adher_target);

				this.performanceMatrix.controls['technician_level'].setValue(this.performance_config_data.technician_level);
				this.performanceMatrix.controls['training_certification_reward'].setValue(this.performance_config_data.training_certification_reward);
				this.performanceMatrix.controls['training_certification_target'].setValue(this.performance_config_data.training_certification_target);

				this.chRef.detectChanges();
				this.dtTrigger.next();
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});

	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modal",                                  //specifies the size of the dialog box
			backdrop: 'static',                                    // modal will not close by outside click
			keyboard: false,                                       // modal will not close by keyboard click
		});
	}
}
