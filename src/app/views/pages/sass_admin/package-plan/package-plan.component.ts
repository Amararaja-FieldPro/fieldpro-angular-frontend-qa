import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'kt-package-plan',
  templateUrl: './package-plan.component.html',
  styleUrls: ['./package-plan.component.scss']
})
export class PackagePlanComponent implements OnInit {
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  plan: any;
  duration: any;
  startduration: any;
  endduration; any;
  amount: any;
  discount: any;
  package_management_details: any;
  edit_data: any;
  Edit_package_form: FormGroup;    //form group for edit package plan
  discountamount: any;
  licensecost: number;
  licenseamount: any;
  Addpackageplan: FormGroup;     //form group for add package plan
  per_license_cost: string;
  package_details: any;        //result for get API get_package_management_details
  package_plan_details: any;   //result for get API get_package_details
  showTable: boolean = false;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  package_type_id: any;
  status: number;
  discount_percentage: any;
  loadingAdd: boolean = false;   // btn spinner
  loadingEdit: boolean = false;
  from_range: string;
  to_range: string;
  from_user_range: any;
  to_user_range: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  error: any;
  Calculate_button: boolean = true;

  constructor(private modalService: NgbModal, public ajax: ajaxservice, private router: Router, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {
    this.Addpackageplan = new FormGroup({
      "package_type_id": new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
      "from_user_range": new FormControl("", [Validators.required]),
      "status": new FormControl(1),
      "to_user_range": new FormControl(0, [Validators.required]),
      "per_license_cost": new FormControl(0, [Validators.required]),
      "discount_percentage": new FormControl(0),
      "discount_amount": new FormControl(0),
      "new_license_cost": new FormControl('', [Validators.required]),
    })
    this.Edit_package_form = new FormGroup({
      "package_type_id": new FormControl("", Validators.required),
      "from_user_range": new FormControl("", Validators.required),
      "package_plan_id": new FormControl("", Validators.required),
      "to_user_range": new FormControl("", Validators.required),
      "per_license_cost": new FormControl("", Validators.required),
      "discount_percentage": new FormControl("", Validators.required),
      "discount_amount": new FormControl("", Validators.required),
      "new_license_cost": new FormControl("", Validators.required),
      "status": new FormControl("", Validators.required),
    });
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.status = 1;
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);  //loader
    this.overlayRef.attach(this.LoaderComponentPortal);  //attach loader
    this.getpackageplan();  //package plan function calling
    this.getpackagetype(); //package type function calling
  }
  //------------------------------------------set getters for formgroup controls------------------------//
  get log() {
    return this.Addpackageplan.controls;
  }
  get log2() {
    return this.Edit_package_form.controls;
  }

  //--------------------------------------------------------------------------------------------------//
  //get package details for droupdown list
  getpackagetype() {
    var url = 'get_package_details/';             // api url for getting the details 
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {                      //if sucess
        this.package_plan_details = result.response.data;                     //storing the api response in the array
      }
      else if (result.response.response_code == "400") {                           //if failure
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {                                                        //if not sucess
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {                                           //if error
      console.log(err);                                        //prints if it encounters an error
    });

  }
  check_package_type(packagetype) {
    this.package_type_id = packagetype;
    // if (this.package_type_id == 1) {
    //   //if package type is free 
    //   var no_of_license = 10;
    //   this.no_of_license_count = no_of_license
    //   this.AddSubscriptionPackageInfo.controls['number_of_license'].setValue(no_of_license); //set value for the number of license
    //   this.AddSubscriptionPackageInfo.controls['total_price'].setValue(''); //set value for the total price
    // }
    // else if (this.package_type_id == 2) {
    //   //if package type is paid    
    //   var no_of_license = 10;
    //   this.AddSubscriptionPackageInfo.controls['total_price'].setValue(''); //set value for the total price
    // }
  }
  //get the package type (type 1 is for free and type 2 is for paid)
  onchange_get_package_type(packagetype) {
    this.error = ''              //refresh the api response error msg
    console.log(packagetype)
    this.package_type_id = packagetype
    console.log(this.package_type_id)
    this.from_range = '';
    this.to_range = "";
    console.log(this.Addpackageplan)

    //  this.Addpackageplan.value.to_user_range.reset();
    if (this.package_type_id == 1) {
      console.log("if")
      // this.Addpackageplan['controls'].from_user_range.setValue(1);
      // this.Addpackageplan['controls'].to_user_range.setValue(10);

      this.Addpackageplan.patchValue({
        "from_user_range": 1,
        "status": 1,
        "to_user_range": 10,
        "per_license_cost": 0,
        "discount_percentage": 0,
        "discount_amount": 0,
        "new_license_cost": 0,
      })
      console.log(this.Addpackageplan)
      console.log(this.Addpackageplan.value)

    }
    else {
      console.log("else")

      this.Addpackageplan['controls'].from_user_range.setValue(0);
      this.Addpackageplan['controls'].to_user_range.setValue(0);
      this.Addpackageplan['controls'].discount_amount.setValue('');
      this.Addpackageplan['controls'].new_license_cost.setValue('');
    }
  }
  get_from_user_range(from_user_range) {
    this.from_user_range = from_user_range;
    if (this.package_type_id == undefined || this.package_type_id == '' || this.package_type_id == null) {
      //if package is not selected before enter the from user range then through the errror message
      this.from_range = "Select Package type before enter no of license"
    }
    else {
      this.from_range = ""
      console.log(this.package_type_id, "package_type_id")
      console.log(this.from_user_range, "this.from_user_range")
      if (this.package_type_id == 1) {
        if (this.from_user_range !== '1') {
          // alert("if")
          this.from_range = "From range for Free must be one only"
        }
        else {
          // alert("else")
          this.from_range = ''
        }
        if (this.to_user_range !== '10') {
          // alert("if")
          this.to_range = "To range for Free must be 10 only"
        }
        else {
          // alert("else")
          this.to_range = ''
        }
      }
      //if package type is paid the the user from range must be 11
      //   else{
      //   if (this.package_type_id == 2) {
      //     if (this.from_user_range >= '11') {
      //       alert("if")
      //       this.from_range = "From range for Paid must be equal to 11 or above to it"
      //     }
      //     else {
      //       alert("else")
      //       this.from_range = ''
      //     }
      //   }
      // }
    }
  }

  get_to_user_range(to_user_range) {
    this.to_user_range = to_user_range;
    if (this.package_type_id == undefined || this.package_type_id == '' || this.package_type_id == null) {
      //if package is not selected before enter the to user range then through the errror message
      this.to_range = "Select Package type before enter no of license"
    }
    else {
      this.to_range = ""
      console.log(this.to_user_range, "to_user_range")
      if (this.package_type_id == 1) {
        if (this.to_user_range !== '10') {
          // alert("if")
          this.to_range = "To range for Free must be 10 only"
        }
        else {
          // alert("else")
          this.to_range = ''
        }
      }
    }
  }

  //get plan details for list
  getpackageplan() {
    var url = 'get_package_management_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400" || result.response.response_code == "500") {
        this.package_details = result.response.data;  //store data to the variable
        console.log(this.package_details)
        this.showTable = true;
        this.overlayRef.detach();
        this.chRef.detectChanges();
        this.dtTrigger.next();                //detach loader
      }

      else {
        this.package_details = result.response.data;
        console.log(this.package_details)
        this.showTable = true;
        this.overlayRef.detach();              //detach loader
        this.chRef.detectChanges();
      }

    }, (err) => {
      this.overlayRef.detach();                                                             //if error
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  // submit for Add package plan
  addpackageplan() {
    // if (this.Calculate_button == true) {
    //   this.error = "Please click calculate button before submit"
    // } else {
    this.error = ''
    this.loadingAdd = true;
    this.Addpackageplan.value.status = 1
    console.log(this.Addpackageplan.value)
    console.log(this.Addpackageplan.valid)
    if (this.per_license_cost == '')                                                   //Check if the package type is empty
    {
      this.error = "Enter per license cost"               //If empty display the toast error message 
    }
    else {
      this.error = ''
      console.log(this.Addpackageplan.value);
      var data = this.Addpackageplan.value;                  //Data to be passed for add api
      var url = 'add_package_management/'                                         //api url for add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                       //if sucess
          // this.Addpackageplan.reset();
          this.loadingAdd = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.getpackageplan();                                            //reloading the component
          this.toastr.success(result.response.message, 'Success');    //toastr message for success   
          this.modalService.dismissAll();                                 //to close the modal box
        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {          //if failure                                                      //if not sucess
          this.error = result.response.message      //toastr message for error
          this.loadingAdd = false;
        }
        else {                                                                //if not sucess
          this.error = result.response.message      //toastr message for error
          this.loadingAdd = false;
        }
      }, (err) => {   //if error
        this.error = "Somethig went wrong"
        console.log(err);                                             //prints if it encounters an error
      });
    }
    // }

  }


  //--------------------------------------------------------------------------------------------------//
  calculate_discounted_new_license_count() {
    this.Calculate_button = false   //for add subscription
    console.log(this.licenseamount)
    if (this.discount_percentage > 100)                                                                 //if amount greater than 100
    {
      this.toastr.error("Discount percentage cannot be greater than 100")               //toastr message for error
    }
    else {
      if (this.discount_percentage == undefined || this.discount_percentage == '' || this.discount_percentage == null) {
        this.discount_percentage = 0;
      }
      if (this.licenseamount == undefined || this.licenseamount == '' || this.licenseamount == null) {
        this.licenseamount = 0;
      }
      var value = Math.round((this.discount_percentage / 100) * +this.licenseamount);
      var newvalue = +this.licenseamount - value;                    //if percentage calculate amount   
      this.Addpackageplan.controls['discount_amount'].setValue(value);                           //for add subscription                           
      this.Edit_package_form.controls['discount_amount'].setValue(value);                 //for edit subscription
      this.Edit_package_form.controls['new_license_cost'].setValue(newvalue);             //for edit subscription
      this.Addpackageplan.controls['new_license_cost'].setValue(newvalue);              //for add subscription
    }
  }
  discount_calculation(discount_percentage) {
    this.discount_percentage = discount_percentage
    this.Calculate_button = true;                //for enable the error message
    if(this.Addpackageplan){
      this.Addpackageplan.controls['discount_amount'].setValue('');
      this.Addpackageplan.controls['new_license_cost'].setValue('');
    }
  else{
    this.Edit_package_form.controls['discount_amount'].setValue('');
    this.Edit_package_form.controls['total_price'].setValue('');
  }
   
  }
  // function for per license cost
  get_per_license_cost(amount) {
    this.Calculate_button = true;              //for enable the error message
    this.licenseamount = amount;
    this.Addpackageplan.controls['new_license_cost'].setValue(amount);             //for add subscription
    this.Addpackageplan.controls['discount_percentage'].setValue(0);
    this.Addpackageplan.controls['discount_amount'].setValue(0);
    this.Edit_package_form.controls['new_license_cost'].setValue('');              //for edit subscription


  }


  //--------------------------------------------------------------------------------------------------//
  // Function to add and edit the modal box 
  openLarge(content6) {
    this.error = ""  //refresh the error from api response
    this.from_range = ''
    this.loadingEdit = false;
    this.Addpackageplan.reset({
      "from_user_range": 0,
      "status": 1,
      "to_user_range": 0,
      "per_license_cost": '',
      "discount_percentage": 0,
      "discount_amount": 0,
      "new_license_cost": '',
    });
    this.package_type_id = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",                                                     //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to store the line item selected to edit
  editpackage(data) {
    this.edit_data = data;
    //data binding
    this.Edit_package_form.setValue({
      "package_type_id": this.edit_data.package_type.package_id,
      "from_user_range": this.edit_data.from_user_range,
      "package_plan_id": this.edit_data.package_plan_id,
      "to_user_range": this.edit_data.to_user_range,
      "per_license_cost": this.edit_data.per_license_cost,
      "discount_percentage": this.edit_data.discount_percentage,
      "discount_amount": this.edit_data.discount_amount,
      "new_license_cost": this.edit_data.new_license_cost,
      "status": this.edit_data.status
    });
  }

  //--------------------------------------------------------------------------------------------------//

  //Function to call the edit api 
  edit_packageplan() {
    this.loadingEdit = true;
    console.log(this.Edit_package_form.value)
    console.log(this.Edit_package_form.valid)
    var url = 'edit_package_management/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.Edit_package_form.value)
      .subscribe((result) => {
        if (result.response.response_code == "200")                           //if sucess
        {
          // this.showTable = false;
          // this.getpackageplan();
          // this.toastr.success(result.response.message, 'Success');        //toastr message for success
          // this.loadingEdit = false;
          // this.modalService.dismissAll();                                 //to close the modal box
          this.loadingEdit = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.getpackageplan();                                            //reloading the component
          this.toastr.success(result.response.message, 'Success');    //toastr message for success   
          this.modalService.dismissAll();


        }
        else if (result.response.response_code == "400" || result.response.response_code == "500") {                  //if failure
          this.error = result.response.message           //toastr message for error
          this.loadingEdit = false;
        }
        else {                                                                     //if not sucess
          this.error = result.response.message       //toastr message for error
          this.loadingEdit = false;
        }
      }, (err) => {
        this.error = "Somethig went wrong"                                                          //if error
        console.log(err);                                                 //prints if it encounters an error
      });
  }

  //--------------------------------------------------------------------------------------------------//

}
