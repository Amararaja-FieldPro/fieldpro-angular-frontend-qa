import { Component, OnInit, ViewEncapsulation, ViewContainerRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Renderer2, ViewChildren, ElementRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router'; // To enable routing for this component
import { Location } from '@angular/common';
@Component({
  selector: 'kt-orgstructure',
  templateUrl: './orgstructure.component.html',
  styleUrls: ['./orgstructure.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrgstructureComponent implements OnInit {
  Edit_org_form: FormGroup;
  AddNodes: FormGroup;
  edit_data: any;
  getNode: any;
  getaddnodes: any;
  getorgnode: any;
  states: any;
  country: any;
  value: any;
  editIndex: number = null;
  file_name: any;
  loading: boolean = false;
  showTable: boolean = false;   //loader on and off
  overlayRef: OverlayRef;    //loader
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  submitted = false;
  filename: any;
  Filename: any;
  ByDefault: boolean = false;
  loadingUpdate: boolean = false;
  loadingUpload: boolean = false;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChildren('div') div: ElementRef;
  type: any;
  error: any;
  errors: any;
  geterror: any;
  bulk: any;
  constructor(private modalService: NgbModal, private router: Router, public _location: Location, private formBuilder: FormBuilder, private renderer: Renderer2, public ajax: ajaxservice, private toastr: ToastrService, public activeModal: NgbActiveModal, private http: Http, vcr: ViewContainerRef, private overlay: Overlay, private chRef: ChangeDetectorRef) {
  }
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);

    this.AddNodes = this.formBuilder.group({
      "parent_id": ['', Validators.required],
      "node_name": new FormControl('', [Validators.required,this.noWhitespace]),

    })
    this.getlocationdata();
    this.getnodes();

  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  fileUpload(event) {
    this.error = "";
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
    this.type = event.target.files[0].type;
    this.Filename = event.target.files[0].name;
    this.ByDefault = true;
  };

  filesubmit() {
    this.submitted = true;
    var validExts = new Array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", ".csv");
    if (this.file_name == undefined) {
      // this.loadingUpload = false;
      this.bulk = '';
      this.error = "Please select file";
    }
    else if (this.file_name.length > 0) {
      if (validExts[0] != this.type) {
        // this.loadingUpload = false;
        this.bulk = '';
        this.error = "Please select correct extension";
      }
      else if (validExts = this.type) {
        this.loadingUpload = true;
        let file: File = this.file_name[0];
        const formData: FormData = new FormData();
        formData.append('excel_file', file);
        let headers = new Headers();
        var method = "post";
        var url = 'orgstructure_bulk_upload/'
        var i: number;
        this.ajax.ajaxpost(formData, method, url).subscribe(data => {
          if (data['response_code'] == "200") {
             if (data['response'].error_code == "404") {
              if (data['response'].fail.length > 0) {
                for (i = 0; i < data['response'].fail.length; i++) {
                  if (data['response'].fail[i].error_message) {
                    var a = [];
                    data['response'].fail.forEach(value => {
                      a.push(value.error_message);
                    });
                    this.bulk = a;
                  }
                }
              }
              this.loadingUpload = false;
            }
            else if (data['success'].length > 0) {
              for (i = 0; i < data['success'].length; i++) {
                this.bulk = '';
                this.showTable = false;
                this.chRef.detectChanges();
                this.getlocationdata();
                this.toastr.success(data['success'][i].success_message, 'Success!');  //toastr message for Success
                this.loadingUpload = false;
              }
            }
        
          }
          else if (data['response'].response_code == "500") {
            this.bulk = '';
            this.error = data['response'].message
            this.loadingUpload = false;
          }
          else {
            this.bulk = '';
            this.error = "Something went wrong."      //toastr message for error
            this.loadingUpload = false;
          }
        },
          error => {
            this.bulk = '';
            this.error = "Something went wrong"          //prints if it encounters an error
            this.loadingUpload = false;
          }
        );
      }
    }
  };

  getnodes() {
    var url = 'get_org_node/';          // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getaddnodes = result.response.data;

      }
    });
  }
  onEditClick(event, index: number) {
    this.getorgnode.id = this.getorgnode[index].id;
    this.getorgnode.node = this.getorgnode[index].node;

    this.editIndex = index;
  }
  // function for modal boxes
  openLarge(content6) {
    this.bulk = '';
    this.AddNodes.reset();
    this.errors = '';
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",     //modal popup resize                                                 //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  openBulk(content6) {
    this.bulk=[];
    this.loadingUpload = false;
    this.error = '';
    this.errors = '';
    this.Filename = '';
    this.file_name = undefined;
    this.modalService.open(content6, {
      size: 'lg',
      windowClass: "center-modalsm",     //modal popup resize         
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  openeditmodal(edit) {
    this.bulk = '';
    this.errors = '';
    this.modalService.open(edit, {
      size: 'lg',
      windowClass: "center-modalsm",     //modal popup resize                                                   //specifies the size of the modal box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  getlocationdata() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_org_structure/';          // api url for getting the details

    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.getorgnode = result.response.nodes;
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }

      else if (result.response.response_code == "400") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.geterror = result.response.message;
      }
      else if (result.response.response_code == "500") {
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.geterror = result.response.message;
      }
      else {

        this.overlayRef.detach();
        this.geterror = "Something went wrong";
      }
    });
  }

  editorgform(data) {


    if (data.id) { //check availability of parentId

      this.value = {
        "node_id": data.id,
        "node_name": this.Edit_org_form.value.node_name,
        "parent_id": data.parent_id
      };

      var url = 'create_org_parent/';
      this.ajax.postdata(url, this.value).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.showTable = false;
          this.chRef.detectChanges();
          this.getnodes();
          this.getlocationdata();
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.modalService.dismissAll();                                 //to close the modal box

        }
        else if (result.response.response_code == "400") {

          this.errors = result.response.message;
        }
        else if (result.response.response_code == "500") {

          this.errors = result.response.message;
        }
        else {

          this.errors = "Something went wrong";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error 
      });
    }

    else {//if the parent ID is not available add new node

      this.value = {
        "node_name": this.Edit_org_form.value.node_name,
        "parent_id": this.Edit_org_form.value.parent_id
      };

      var url = 'create_org_structure/';

    }



  }
  test(data) {

    this.edit_data = data;
    this.Edit_org_form = new FormGroup({
      "node_id": new FormControl(this.edit_data.id, Validators.required),
      "node_name": new FormControl(this.edit_data.name, [Validators.required,this.noWhitespace]),
      "parent_id": new FormControl(this.edit_data.parent_id, Validators.required),


    });

  }
  submit(data) {
    this.submitted = true;
    this.loadingUpdate = true;
    if (this.Edit_org_form.invalid) {
      // this.toastr.error("Fill the node name", 'Error');
    }
    else {
      if (this.edit_data.id) { //check availability of parentId
        this.value = {
          "node_id": this.edit_data.id,
          "node_name": this.Edit_org_form.value.node_name,
          "parent_id": this.edit_data.parent_id
        };
        var url = 'create_org_parent/';
        this.ajax.postdata(url, this.value).subscribe((result) => {

          if (result.response.response_code == "200") {
            this.showTable = false;
            this.chRef.detectChanges();
            this.getlocationdata();
            this.getnodes();
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.loadingUpdate = false;
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") {
            this.errors = result.response.message;
            this.loadingUpdate = false;
          }
          else if (result.response.response_code == "500") {
            this.errors = result.response.message;
            this.loadingUpdate = false;
          }
          else {
            this.errors = "Something went wrong";
            this.loadingUpdate = false;
          }
        }, (err) => {
          console.log(err); //prints if it encounters an error 
        });
      }

      else {//if the parent ID is not available add new node

        this.value = {
          "node_name": this.Edit_org_form.value.node_name,
          "parent_id": this.Edit_org_form.value.parent_id
        };

        var url = 'create_org_structure/';

      }
    }
  }
  add(add) {

  }

  get f() {
    return this.AddNodes.controls;
    // return this.AddNodes.get('parent_id');
  }
  get e() {
    return this.Edit_org_form.controls;
    // return this.Edit_org_form.get('parent_id');
  }

  addnode(data) {
    this.loading = true;
    this.submitted = true;

    if (this.AddNodes.invalid) {
      // this.toastr.error("Please fill the node", 'Error');
    }
    else {
      var data = this.AddNodes.value;

      this.value = {
        "node_name": this.AddNodes.value.node_name,
        "parent_id": this.AddNodes.value.parent_id
      };

      var url = 'create_org_structure/';
      this.ajax.postdata(url, this.value).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.loading = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.getlocationdata();
          this.getnodes();
          this.toastr.success(result.response.message, 'Success'); //toastr message for success
          this.modalService.dismissAll();
        }

        else if (result.response.response_code == "400") {

          this.loading = false;
          this.errors = result.response.message;
        }
        else if (result.response.response_code == "500") {
          this.loading = false;

          this.errors = result.response.message;
        }
        else {
          this.loading = false;
          this.errors = "Something went wrong";
        }
      }, (err) => {
        console.log(err); //prints if it encounters an error 
      });
    }

  }


  // function for delete the content
  trash(data) {

    var node_id = { "node_id": this.edit_data.id };
    var url = 'delete_org_structure/';             // api url for delete warehouse
    var id = "node_id";                        //id for identify warehouse name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.ajax.postdata(url, node_id).subscribe((result) => {
          if (result.response.response_code == "200") {               //if success
            this.showTable = false;
            this.chRef.detectChanges();
            this.getlocationdata();
            this.toastr.success(result.response.message, 'Success'); //toastr message for success
            this.modalService.dismissAll();
          }
          else if (result.response.response_code == "400") {

            this.loading = false;
            this.errors = result.response.message;
          }
          else if (result.response.response_code == "500") {
            this.loading = false;

            this.errors = result.response.message;
          }
          else {
            this.loading = false;
            this.errors = "Something went wrong";
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }







}

