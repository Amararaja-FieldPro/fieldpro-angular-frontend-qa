// Angular
import { Component } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr';// To use toastr
import Swal from 'sweetalert2';

@Component({
	selector: 'kt-topbar',
	templateUrl: './topbar.component.html',
	styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent {
	employee_code: any;
	role_id: any;
	email_id: any;
	constructor(public ajax: ajaxservice, private router: Router, private toastr: ToastrService, private activatedRoute: ActivatedRoute) {

	}
	ngOnInit() {

	}
	currentRouter = this.router.url;
	myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	// logout() {
	// 	 this.router.navigate(['/login']);   
	// }  
	logout() {
		this.role_id = localStorage.getItem('role_id')
		console.log(this.role_id, "role_id")
		this.employee_code = localStorage.getItem('employee_code');
		this.email_id = localStorage.getItem('email_id');
		console.log(this.role_id, "this.role_id")
		if (this.role_id == 7) {
			var url = 'customer_logout/?';
			var id = "email";                      //id for using identify the amc name
			var data = this.email_id;
			var redirect_url='customer_login'
			console.log(data, "data")
		Swal.fire({
			title: 'Are you sure?',
			text: "Do you want to logout!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			Router
			if (result.value) {
				this.ajax.getdataparam(url, id, data).subscribe((result) => {
					if (result.response.response_code == "200") {
						localStorage.clear();
						this.router.navigate(['customer_login'])
						// window.location.reload();

					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
		}
		else {
			var url = 'fp_logout/?';             // api url for getting the details with using post params
			var id = "employee_code";                      //id for using identify the amc name
			var data = this.employee_code;
			var redirect_url='/login'
			console.log(data, "data")
			Swal.fire({
				title: 'Are you sure?',
				text: "Do you want to logout!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes',
				allowOutsideClick: false,                          //sweet alert will not close on oustside click
			}).then((result) => {
				Router
				if (result.value) {
					this.ajax.getdataparam(url, id, data).subscribe((result) => {
						if (result.response.response_code == "200") {
							localStorage.clear();
							// this.router.navigate(['login'])
							window.location.reload();
	
						}
						else if (result.response.response_code == "400") {                //if failure
							this.toastr.error(result.response.message, 'Error');            //toastr message for error
						}
						else {                                                         //if not sucess
							this.toastr.error(result.response.message, 'Error');        //toastr message for error
						}
					}, (err) => {
						console.log(err);                                        //prints if it encounters an error
					});
				}
			})
		}
		
		// console.log(data, "data")
		// Swal.fire({
		// 	title: 'Are you sure?',
		// 	text: "Do you want to logout!",
		// 	icon: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonColor: '#3085d6',
		// 	cancelButtonColor: '#d33',
		// 	confirmButtonText: 'Yes',
		// 	allowOutsideClick: false,                          //sweet alert will not close on oustside click
		// }).then((result) => {
		// 	Router
		// 	if (result.value) {
		// 		this.ajax.getdataparam(url, id, data).subscribe((result) => {
		// 			if (result.response.response_code == "200") {
		// 				localStorage.clear();
		// 				// this.router.navigate(['login'])
		// 				window.location.reload();

		// 			}
		// 			else if (result.response.response_code == "400") {                //if failure
		// 				this.toastr.error(result.response.message, 'Error');            //toastr message for error
		// 			}
		// 			else {                                                         //if not sucess
		// 				this.toastr.error(result.response.message, 'Error');        //toastr message for error
		// 			}
		// 		}, (err) => {
		// 			console.log(err);                                        //prints if it encounters an error
		// 		});
		// 	}
		// })
	}



	// 		var url = 'fp_logout/';            
	// 		this.ajax.getdata(url).subscribe((result) => {
	// 		  if (result.response.response_code == "200") {                      

	// 		  }
	// 		  else if (result.response.response_code == "500"){                           
	// 			this.toastr.error(result.response.message, 'Error');       
	// 		  }
	//   else{                                                        
	// 	this.toastr.error(result.response.message, 'Error');        
	//   }
	// 		}, (err) => {                                          
	// 		  console.log(err);                                       
	// 		});

	// 	  }
}
