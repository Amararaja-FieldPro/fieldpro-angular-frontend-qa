import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparerequestComponent } from './sparerequest.component';

describe('SparerequestComponent', () => {
  let component: SparerequestComponent;
  let fixture: ComponentFixture<SparerequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparerequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparerequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
