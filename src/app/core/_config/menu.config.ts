export class MenuConfig {
	role_id: any;
	organization_type: any;
	asideMenus: any;
	subMenus: any;
	public default1: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					roll_id: 1,
					title: 'Dashboard',
					root: true,
					icon: 'fas fa-home',
					page: '/maindashboard'
				},
				{
					roll_id: 1,
					title: 'Manage Subscription',
					root: true,
					icon: 'fas fa-plus-circle',
					page: '/sass_admin/managesubscription'
				},
				{
					roll_id: 1,
					title: 'Package Plan',
					root: true,
					bullet: 'dot',
					icon: 'fa fa-list-ul',
					translate: 'MENU.PACKAGE_PLAN',
					page: '/sass_admin/packageplan'
				},

			]
		},
	};
	public default2: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'fas fa-home',
					page: '/maindashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},
				{
					title: 'Product Master',
					root: true,
					icon: 'flaticon2-box',
					translate: 'MENU.PRODUCT_MASTER',
					page: '/productmaster'
				},
				{
					title: 'Spare Master',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cubes',
					translate: 'MENU.SPARE',
					page: '/spare-master'

				},
				{
					title: 'User Management ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user',
					translate: 'MENU.USER',
					page: '/user'

				},
				{
					title: 'Customer Management ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user-friends',
					translate: 'MENU.CUSTOMERMANAGEMENT',
					page: '/customermanagement'
				},
				{
					title: 'SLA Mapping',
					root: true,
					bullet: 'dot',
					icon: 'fa fa-map-signs',
					translate: 'MENU.SLA_MAPPING',
					page: '/slamapping'
				},
				// {
				// 	title: 'Billing-Details',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-dollar-sign',
				// 	translate: 'MENU.BILLING_DETAILS',
				// 	page: '/billing-detail'
				// },
				{
					title: 'Contract type',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-file-contract',
					translate: 'AMC.TITLE',
					page: '/amc'
				},
				{
					title: 'Service Group',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-layer-group',
					translate: 'MENU.SERVICE',
					page: '/servicegroup'
				},
				// {
				// 	title: 'Location mapping',
				// 	root: true,
				// 	icon: 'flaticon2-architecture-and-city',
				// 	page: '/locationmapping',
				// 	translate: 'MENU.LOCATION',
				// 	bullet: 'dot',
				// },				

				{
					title: 'Warehouse Master',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-warehouse',
					translate: 'MENU.WAREHOUSE',
					page: '/warehouse'
				},
				{
					title: 'Inventory Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cubes',
					translate: 'MENU.INVENT',
					page: '/spares'
				},
				{
					title: 'Content Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-envelope-open-text',
					translate: 'MENU.CONTENT',
					page: '/content'
				},
				{
					title: 'Segment',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-envelope-open-text',
					translate: 'MENU.SEG',
					page: '/segment'
				},
				{
					title: 'Application',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-envelope-open-text',
					translate: 'MENU.APP',
					page: '/application'
				},
				{
					title: 'Settings',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cog',
					submenu: [
						{
							title: ' Site Settings',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-user-cog',
							translate: 'MENU.SITE',
							page: '/sitesettings'
						},
						{
							title: ' SMS Settings',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-sms',
							translate: 'MENU.SMS',
							page: '/sms'
						},
						{
							title: ' SMTP Settings',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-sliders-h',
							translate: 'MENU.SMTP',
							page: '/smtp'
						},
						{
							title: 'Organization Structure',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-sitemap',
							translate: 'MENU.ORG_STR',
							page: '/org'
						},
						{
							title: 'Location',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-map-marker-alt',
							translate: 'MENU.LOCATION',
							page: '/location'
						},
					]
				},
			]
		},
	};
	public default3: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
					icon: 'fas fa-home',

				},

				{

					title: 'User Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user',
					translate: 'MENU.USER',
					page: '/user'

				},
				{
					roll_id: 3,
					title: 'Inventory Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cubes',
					translate: 'MENU.SPARE',
					page: '/spares'
				},


			]
		},
	};
	public default4: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/service_dashboard',
					icon: 'fas fa-home',
					translate: 'MENU.DASHBOARD'
				},
				{
					title: 'New Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-plus-circle',
					translate: 'MENU.NEWTCKT',
					page: '/newticket'
				},
				{
					title: 'Ongoing Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-circle-notch',
					// translate: 'MENU.NEWTCKT',
					page: '/ongoingticket'
				},

				{
					title: 'Completed Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-check-circle',
					// translate: 'MENU.NEWTCKT',
					page: '/completedticket'
				},

				{
					title: 'Raise Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-tasks',
					translate: 'MENU.RAISETCKT',
					page: '/raiseticket'
				},
				{
					title: 'Reports',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-infographic',
					submenu: [
						{
							title: 'Summary Report',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-clipboard-list',
							// translate: 'MENU.SPARE',
							page: '/manager_summary'
						},
						// {
						// 	title: 'TAT Trend',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-calendar-check',
						// 	page: '/tat_trend'
						// },
						{
							title: 'CSL Trend',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/csl_trend'
						},
						{
							title: 'CSL Analysis',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/csi_analyse'
						},
						{
							title: 'Financial Report',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/finance_report'
						},
						{
							title: 'Financial Replacement',
							root: true,
							icon: 'fas fa-calendar-check',
							page: '/finance_replace'
						},
						{
							title:'FTR Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/ftr'
						},
						{
							title:'Complaint Summary',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/complaint_summery'
						},
						{
							title:'Reject Reason Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/reject_reason'
						},
						{
							title:'Ticket Age Wise Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/ticket_age'
						},
					]
				},
			

				{
					title: 'Knowledge Base',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-brain',
					translate: 'MENU.KNOWLEDGE',
					page: '/knowledgebase'
				},
				{

					title: 'Spare Request',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-tools',
					translate: 'MENU.SPARE',
					page: '/service_spare_request'
				},


				// {
				// 	title: 'AMC Contract',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-file-contract',
				// 	translate: 'MENU.AMC',
				// 	page: '/amc_contract'
				// },


			]
		},
	};
	public default5: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					alignment: 'left',
					page: '/manager_dashboard',
					translate: 'MENU.DASHBOARD',
					icon: 'fas fa-home'

				},
				{
					title: 'Leaderboard',
					root: true,
					alignment: 'left',
					page: '/leaderboard',
					translate: 'MENU.LEADERBOARD',
					icon: 'fas fa-trophy'

				},
				{
					title: 'Customer Management ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user-friends',
					translate: 'MENU.CUSTOMERMANAGEMENT',
					page: '/customermanagement'
				},
				{
					title: 'Manage Tickets',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-clipboard-list',
					submenu: [
						{
							title: 'New Ticket',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-plus-circle',
							translate: 'MENU.NEWTCKT',
							page: '/manager_newticket'
						},
						{
							title: 'Ongoing Ticket',
							root: true,
							bullet: 'dot',
							icon: 'flaticon-coins',
							translate: 'MENU.NEWTCKT',
							page: '/manager_ongoingticket'
						},
						{
							title: 'Completed Tickets',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-check-circle',
							translate: 'MENU.COMPLETEDTKT',
							page: '/manager_completedtickets'
						},
					]
				},
				// {
				// 	roll_id: 2,
				// 	title: 'SLA Mapping',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fa fa-map-signs',
				// 	translate: 'MENU.SLA_MAPPING',
				// 	page: '/sla'
				// },
				{
					title: 'Spare Request',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-toolbox',
					submenu: [
						// {

						// 	title: 'Ticket Spare Request',
						// 	root: true,
						// 	bullet: 'dot',
						// 	icon: 'fas fa-tools',
						// 	translate: 'MENU.SPARE',
						// 	page: '/spare_request'
						// },
						{

							title: 'Imprest Spare Request',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-hand-holding',
							translate: 'MENU.IMPRESTSPARE',
							page: '/imprest_spare'
						},
						{

							title: 'Spare Transfer Request',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-exchange-alt',
							translate: 'MENU.TRANSFERSPARE',
							page: '/transfer_spare',

						},
					]
				},

				{
					title: 'Raise Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-tasks',
					translate: 'MENU.RAISETCKT',
					page: '/raiseticket'
				},
				{

					title: 'New Contract ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-file-signature',
					// translate: 'MENU.SPARE',
					page: '/manager_contract'
				},
				{

					title: 'Training Score ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-star',
					translate: 'MENU.TRAININGSCORE',
					page: '/training_score'
				},
				{
					title: 'Settings',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cog',
					submenu: [
						{

							title: 'Performance matrix ',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-chart-bar',
							translate: 'MENU.PERFORMANCEMATRIX',
							page: '/performance_matrix'
						},
						{
							title: 'Configuration',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-cogs',
							translate: 'MENU.CONFIG',
							page: '/configuration'
						},
					]
				},


				{
					title: 'Reimbursement',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-hand-holding-usd',
					translate: 'MENU.REIM',
					page: '/reimbursement'
				},
				{
					title: 'Reports',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-infographic',
					submenu: [
						{

							title: 'Summary Report',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-clipboard-list',
							// translate: 'MENU.SPARE',
							page: '/manager_summary'
						},
						// {
						// 	title: 'Attendance report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-calendar-check',
						// 	page: '/manager_attendance'
						// },
						{
							title: 'TAT Trend',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/tat_trend'
						},
						{
							title: 'CSL Trend',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/csl_trend'
						},
						{
							title: 'CSL Analysis',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/csi_analyse'
						},
						{
							title: 'Financial Report',
							root: true,
							// bullet: 'dot',
							icon: 'fas fa-calendar-check',
							page: '/finance_report'
						},
						{
							title: 'Financial Replacement',
							root: true,
							icon: 'fas fa-calendar-check',
							page: '/finance_replace'
						},

						{
							title:'Battery Age Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/battery-age'
						},
						{
							title:'Battery Replacement Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/battery-replace'
						},
						{
							title:'FTR Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/ftr'
						},
						{
							title:'Competitor Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/competitor'
						},
						{
							title:'Complaint Summary',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/complaint_summery'
						},
						{
							title:'Reject Reason Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/reject_reason'
						},
						{
							title:'Location Wise Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/locationwise_report'
						},
						{
							title:'Ticket Age Wise Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/ticket_age'
						},
						{
							title:'Performance Report',
							root: true,
							icon: 'fas fa-calendar-check',
							page:'/performance_report'
												},
						// {
						// 	title: 'Revenue report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-hand-holding-usd',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_revenue'
						// },
						// {

						// 	title: 'Productivity report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-chart-line',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_productivity'
						// },
						// {

						// 	title: 'C-Sat report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-calendar-check',
						// 	translate: 'MENU.CSAT',
						// 	page: '/manager_sat'
						// },
						// {

						// 	title: 'Call Report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-phone-square-alt',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_call'
						// },
						// {

						// 	title: 'Spare Report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fas fa-tools',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_spare'
						// },
						
						// {
						// 	title: 'FRM Report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fa fa-list-ul',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_frm'
						// },
						// {
						// 	title: 'Technician Report',
						// 	root: true,
						// 	// bullet: 'dot',
						// 	icon: 'fa fa-list-ul',
						// 	// translate: 'MENU.SPARE',
						// 	page: '/manager_technicianreport'
						// },
						{
							title: 'Mom',
							root: true,
							// bullet: 'dot',
							icon: 'fa fa-list-ul',
							// translate: 'MENU.SPARE',
							page: '/manager_mom'
						},


					]
				},
				{
					title: 'Technician Tracking',
					root: true,
					// bullet: 'dot',
					icon: 'fa fa-list-ul',
					// translate: 'MENU.SPARE',
					page: '/manager_tracking'
				},
				// {
				// 	title: 'Reports',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-infographic',								
				// 	// translate: 'MENU.AMC',
				// 	page: '/reports'
				// },
				// {

				// 	title: 'Attendance report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-calendar-check',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/attendance'
				// },
				// {

				// 	title: 'Revenue report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-calendar-check',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/revenue'
				// },
				// {

				// 	title: 'Productivity report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-calendar-check',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/productivity'
				// },
				// {

				// 	title: 'C-Sat report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-calendar-check',
				// 	translate: 'MENU.CSAT',
				// 	page: '/manager/sat'
				// },
				// {

				// 	title: 'Call Report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-phone-square-alt',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/call'
				// },
				// {

				// 	title: 'Spare Report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fa fa-list-ul',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/spare'
				// },
				// {

				// 	title: 'Summary Report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-clipboard-list',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/summary'
				// },
				// {

				// 	title: 'FRM Report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fa fa-list-ul',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/frm'
				// },
				// {

				// 	title: 'Technician Report',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fa fa-list-ul',
				// 	// translate: 'MENU.SPARE',
				// 	page: '/manager/technicianreport'
				// },



			]
		},
	};
	public default6: any = {
		header: {
			self: {},
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Raise Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-tasks',
					translate: 'MENU.RAISETCKT',
					page: '/raiseticket'
				},
				// {
				// 	title: 'AMC Contract',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-file-contract',
				// 	translate: 'MENU.AMC',
				// 	page: '/amc_contract'
				// },
				// {
				// 	title: 'Customer Call Tracking',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fa fa-map-marker',
				// 	translate: 'MENU.CALL',
				// 	page: '/call/tracking'
				// },

			]
		},
	};
	public default8: any = {
		header: {
			self: {},
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Raise Ticket',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-tasks',
					translate: 'MENU.RAISETCKT',
					page: '/raiseticket'
				},
				// {
				// 	title: 'AMC Contract',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-file-contract',
				// 	translate: 'MENU.AMC',
				// 	page: '/amc_contract'
				// },
				{
					title: 'Customer Call Tracking',
					root: true,
					bullet: 'dot',
					icon: 'fa fa-map-marker',
					translate: 'MENU.CALL',
					page: 'call_tracking'
				},

			]
		},
	};
	public default7: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboards',
				// 	root: true,
				// 	alignment: 'left',
				// 	page: '/dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// },

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'fas fa-home',
					page: '/maindashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},
				{
					title: 'Master Management ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-envelope-open-text',
					submenu: [
						{
							title: 'Product Master',
							root: true,
							icon: 'flaticon2-box',
							translate: 'MENU.PRODUCT_MASTER',
							page: '/productmaster'
						},
						{
							title: 'Segment',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-envelope-open-text',
							translate: 'MENU.SEG',
							page: '/segment'
						},
						{
							title: 'Application',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-envelope-open-text',
							translate: 'MENU.APP',
							page: '/application'
						},
						{
							title: 'Town',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-map-marker-alt',
							translate: 'MENU.LOCATION',
							page: '/location'
						},
						{
							title: 'Warehouse Master',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-warehouse',
							translate: 'MENU.WAREHOUSE',
							page: '/warehouse'
						},

						{
							title: 'Item Code',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-envelope-open-text',
							translate: 'MENU.APP',
							page: '/model'
						},

						{
							title: 'Spare Master',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-cubes',
							translate: 'MENU.SPARE',
							page: '/spare-master'

						},
						{
							title: ' Compensation',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-cubes',
							translate: 'MENU.COM',
							page: '/compensation'
						},
					]
				},

				{
					title: 'User Management ',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user',
					translate: 'MENU.USER',
					page: '/user'

				},
				{
					title: 'Inventory Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cubes',
					translate: 'MENU.SPARE',
					page: '/spares'
				},
				{
					title: 'Customer Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-user-friends',
					translate: 'MENU.CUSTOMERMANAGEMENT',
					page: '/customermanagement'
				},
				{
					title: 'Content Management',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-envelope-open-text',
					translate: 'MENU.CONTENT',
					page: '/content'
				},
				{
					title: 'Contract Type',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-file-contract',
					translate: 'MENU.AMC',
					page: '/amc'
				},
				{
					title: 'SLA Mapping',
					root: true,
					bullet: 'dot',
					icon: 'fa fa-map-signs',
					translate: 'MENU.SLA_MAPPING',
					page: '/slamapping'
				},
				// {
				// 	title: 'Billing-Details',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'fas fa-dollar-sign',
				// 	translate: 'MENU.BILLING_DETAILS',
				// 	page: '/billing-detail'
				// },

				{
					title: 'Service Group',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-layer-group',
					translate: 'MENU.SERVICE',
					page: '/servicegroup'
				},


				// {
				// 	title: 'Location mapping',
				// 	root: true,
				// 	icon: 'flaticon2-architecture-and-city',
				// 	page: '/locationmapping',
				// 	translate: 'MENU.LOCATION',
				// 	bullet: 'dot',
				// },						
				{
					title: 'Settings',
					root: true,
					bullet: 'dot',
					icon: 'fas fa-cog',
					submenu: [
						// {
						// 	title: ' Site Settings',
						// 	root: true,
						// 	bullet: 'dot',
						// 	icon: 'fas fa-user-cog',
						// 	translate: 'MENU.SITE',
						// 	page: '/sitesettings'
						// },
						{
							title: ' SMS Settings',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-sms',
							translate: 'MENU.SMS',
							page: '/sms'
						},
						{
							title: ' SMTP Settings',
							root: true,
							bullet: 'dot',
							icon: 'fas fa-sliders-h',
							translate: 'MENU.SMTP',
							page: '/smtp'
						},


					]
				},

			]
		},
	};


	public get configs(): any {
		this.role_id = localStorage.getItem('role_id');
		// console.log(this.role_id);
		this.organization_type = localStorage.getItem('organization_type');
		// console.log(this.organization_type)
		if (this.role_id == 1) {
			return this.default1;
		}
		else if (this.role_id == 2) {
			if (this.organization_type == 1) {
				// console.log("Centralized admin");
				this.asideMenus = localStorage.getItem('asideMenus');
				// console.log(this.default7);
				return this.default7;
			}
			else {
				// console.log("Centralized admin");
				this.asideMenus = localStorage.getItem('asideMenus');
				// console.log(this.default2);
				return this.default2;
			}

		}
		else if (this.role_id == 3) {
			return this.default3;
		}
		else if (this.role_id == 4) {
			// localStorage.removeItem('asideMenus');
			var menus = this.default4.aside.items;
			// console.log(menus);
			localStorage.setItem('asideMenus', JSON.stringify(menus));
			this.asideMenus = localStorage.getItem('asideMenus');
			this.subMenus = localStorage.getItem('submenu');
			// console.log(this.subMenus, "subMenu")
			return this.default4;
		}
		else if (this.role_id == 5) {
			// localStorage.removeItem('asideMenus');
			// console.log("inside of Manager");
			// console.log(this.default5);
			var menus = this.default5.aside.items;
			// console.log(menus);
			localStorage.setItem('asideMenus', JSON.stringify(menus));
			this.asideMenus = localStorage.getItem('asideMenus');
			this.subMenus = localStorage.getItem('submenu');
			// console.log(this.subMenus, "subMenu")
			// console.log(JSON.parse(this.asideMenus));
			return this.default5;
		}
		else if (this.role_id == 8) {
			// localStorage.removeItem('asideMenus');
			// console.log("inside of call");
			// console.log(this.default6);
			var menus = this.default6.aside.items;
			// console.log(menus);
			localStorage.setItem('asideMenus', JSON.stringify(menus));
			this.asideMenus = localStorage.getItem('asideMenus');
			this.subMenus = localStorage.getItem('submenu');
			// console.log(this.subMenus, "subMenu")
			// console.log(JSON.parse(this.asideMenus));
			return this.default6;
		}
		else if (this.role_id == 7) {
			// localStorage.removeItem('asideMenus');
			// console.log("inside of call");
			// console.log(this.default6);
			var menus = this.default6.aside.items;
			// console.log(menus);
			localStorage.setItem('asideMenus', JSON.stringify(menus));
			this.asideMenus = localStorage.getItem('asideMenus');
			this.subMenus = localStorage.getItem('submenu');
			// console.log(this.subMenus, "subMenu")
			// console.log(JSON.parse(this.asideMenus));
			return this.default8;
		}
	}

}