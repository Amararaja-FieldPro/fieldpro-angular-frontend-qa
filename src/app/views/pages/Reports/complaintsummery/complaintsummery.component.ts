import { Component, OnInit, ViewChild, ElementRef,ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
const moment = _rollupMoment || _moment;
import { FormControl } from '@angular/forms';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';// To use toastr
import * as xlsx from 'xlsx';
@Component({
  selector: 'kt-complaintsummery',
  templateUrl: './complaintsummery.component.html',
  styleUrls: ['./complaintsummery.component.scss']
})
export class ComplaintsummeryComponent implements OnInit {
  compalint_data: any[];
  start_date: any;
  end_date: any;
  fromdates = new FormControl(moment());
  todates = new FormControl(moment());
  showTable: string;
  dtOptions: DataTables.Settings = {};
  overlayRef: OverlayRef;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  constructor(public ajax: ajaxservice, private calendar: NgbCalendar, private chRef: ChangeDetectorRef, private overlay: Overlay, private toastr: ToastrService) { }

  ngOnInit() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code
    // this.title = 'CSI Response Analysis - Percentage';
    var today = this.calendar.getToday();
    var year = today.year;                  //separate the year
    var month = today.month;                         //separate the month
    var day = 1;
    this.start_date = {
      year: year,
      month: month,
      day: day,
    }
    var Fyear = this.start_date.year;
    var Fmonth = this.start_date.month;
    var Fday = this.start_date.day;
    
    if (this.start_date.month>4){
      this.start_date = Fyear + "-" + '04' + "-" + '01';
      this.end_date = (Fyear+1) + "-" + '03' + "-" + '31';
      var finance=Fyear + "-" + (Fyear+1)
    }
    else{
      this.start_date = (Fyear - 1) + "-" + '04' + "-" + '01';
      this.end_date = Fyear + "-" + '03' + "-" + '31';
      finance=(Fyear - 1) + "-" + Fyear
    }
    var data = {
      "employee_code": emp_code,
      "start_date": this.start_date,
      "end_date": this.end_date,
      "state": [],
      "product":[],
      "product_sub_id":[],
      "model_no":[],
      "service_desk": [],
      "technician": [],
      "application": [],
      "segment": [],
      "customer": [],
      "finance_year":finance,
      "fin_flag": "Year"

    };

    this.complaint_summeryreport(data)

  }
  exportToExcel() {
    const ws: xlsx.WorkSheet =   
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'ComplaintSummaryReport.xlsx');
   }
  updateDataMain(datas) {
    this.complaint_summeryreport(datas)

  }
  public complaint_summeryreport(data) {
    // this.compalint_data = []
    // var url = 'complaint_summery/';
    // this.ajax.postdata(url, data).subscribe((result) => {
    //   if (result.response.response_code == "200") {
    //     this.compalint_data = result.response.data;              //storing the api response in the array
    //     // this.title = 'CSI Response Analysis - Percentage for the period of'+ " " +"("+result.response.Financial_Year[0]+"-"+result.response.Financial_Year[1]+")";
    //   }
    // }, (err) => {
    //   console.log(err);                                        //prints if it encounters an error
    // });
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.showTable = 'false';
    var url = 'complaint_summery/';
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 50,
          lengthMenu: [[50, 100, -1], [50, 100, "All"]] ,
          data: result.response.data,
          columns: [{ data: 'State' }, { data: 'NOComplaintReceived' },{ data: 'NOComplaintClosed' },{ data: 'OpenCount' },{ data: 'TAT1' },{ data: 'ClosingTAT1' },
          { data: 'TAT2' }, {data: 'ClosingTAT2' },{ data: 'BeyondTAT' },{ data: 'ClosingTAT3' },{ data: 'PendingTAT1' },{ data: 'PendingTAT2' },{ data: 'PendingTAT3' },{ data: 'RejectCount' }
          ],
          destroy: true
        };
        this.compalint_data = result.response.data;              //storing the api response in the array
        $('#datatables').DataTable(this.dtOptions)
        
        this.overlayRef.detach();
        this.showTable = 'true';
        this.chRef.detectChanges();
      }

      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else {
        this.toastr.error("Something went wrong", 'Error')
        this.showTable = 'true';
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
      this.overlayRef.detach();
    });
  }
}
