import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { environment } from '../../../../../environments/environment';

import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../loader/loader.component';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from "moment";
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';


@Component({
  selector: 'kt-manage-subscription',
  templateUrl: './manage-subscription.component.html',
  styleUrls: ['./manage-subscription.component.scss']
})
export class ManageSubscriptionComponent implements OnInit {
  message: any;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  spare_image: any;
  currentJustify = 'end';
  package_management_details: any;
  package_details: any;
  AddSubscriptionCompanyInfo: FormGroup;              //form group for create subscription
  AddSubscriptionAddressInfo: FormGroup;
  AddSubscriptionPackageInfo: FormGroup;
  EditOrgAddressInfo: FormGroup;
  EditOrgInfo: FormGroup;
  RemoveLicense: FormGroup;                //form group for remove license from subscription
  subscription_details: any;
  licensecost: any;
  license: any;
  cropped_img = true;
  fileData: File = null;
  previewUrl: any = null;
  states: number;
  cities: number;
  selectedfile = true;
  licenseamount: string;
  no_license: string;
  discount: string = '0';  //used for onchange function for discount
  items: any;
  country: number;   //input   
  state: number;     //input
  edit_sub: any;      //set value for view subscription
  view_subcrip: any      //set value for edit subscription
  organizationtype: any;
  packagetype: any;
  packageplantype: any;
  RenewSubscription: FormGroup;             //form group for renewal license in subscription
  AddmoreSubscription: FormGroup;             //form group for add more license from subscription
  getlicense: any;
  nooflicense: any;
  newlicenseamount: any = 0;
  license_amount: any = 0;
  remaininglicene: any = 0;
  count = 0;
  discount_newlicenseamount: any = 0;
  discount_license_amount: any = 0;
  discount_remaininglicene: any = 0;
  discount_licenseinrange: any = 0;
  discount_count = 0;
  discount_package_details: any;
  discountpercent: any;
  text_licensecost: any;
  text_discountamount: any;
  number_of_license_reduced: any;
  startdate: any;
  enddate: any;
  amountpaid: any;
  showTable: boolean = false;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  activeId: any;
  preId: any;
  image_validation: any;
  AllSubscriptionData: any;
  EditOrgAddressData: any;
  evt: any;
  params: any;
  email_error: string;
  contactnum_error: string;
  validation_data: any;
  orgname_error: string;
  restrictdate: Date;
  numberoflicense_required: any;
  perdaycost: number;
  peryearcost: number;
  package_type_id: Number;
  noOflicenseerror: string;
  no_of_license_count: Number;
  discount_percentage: any;
  discount_error: string;
  noof_license_to_add: any;
  extended_date: string;
  date1: { year: number; month: number; day: number; };
  date2: { year: number; month: number; day: number; };
  license_removed_error: string;
  no_of_used_license: any;
  Filename: any;
  ByDefault: boolean = false;
  loadingSub: boolean = false;   //Btn spinner 
  loadingSubmit: boolean = false;
  loadingNew: boolean = false;
  loadingEdit: boolean = false;
  loadingCal: boolean = false;
  loadingCalc: boolean = false;
  loadingCalculate: boolean = false;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  // number_button: boolean = false; //  stop spinner
  number_buttons: boolean = false;
  company_name_error: string;
  alternate_error: string;
  contact_error: string;
  loading: boolean = false;
  Calculate_button: boolean = false;
  error: any;
  constructor(public ajax: ajaxservice, private toastr: ToastrService,
    private router: Router, private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private overlay: Overlay, public sanitizer: DomSanitizer, private config: NgbDatepickerConfig, private chRef: ChangeDetectorRef,) {
    //formgroup for addsubcription company details
    this.AddSubscriptionCompanyInfo = new FormGroup({
      "organization_name": new FormControl("", [Validators.required, Validators.maxLength(48), this.noWhitespace]),
      "logo": new FormControl('', [Validators.required]),
      "contact_person_name": new FormControl("", [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), Validators.maxLength(48), this.noWhitespace]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "email": new FormControl("", [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")]),

    });
    //formgroup for addsubcription address details
    this.AddSubscriptionAddressInfo = new FormGroup({
      "country_id": new FormControl('', [Validators.required, Validators.maxLength(4)]),
      "state_id": new FormControl('', [Validators.required, Validators.maxLength(7)]),
      "city_id": new FormControl('', [Validators.required, Validators.maxLength(4)]),
      "post_code": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern("^[0-9]*$")]),
      "landmark": new FormControl(""),
      "street": new FormControl("", [Validators.required, this.noWhitespace]),
      "plot_number": new FormControl("", [Validators.required, Validators.pattern("^[0-9%+-/A-Z a-z]{2,32}$"), this.noWhitespace]),
    });
    //formgroup for addsubcription package details
    this.AddSubscriptionPackageInfo = new FormGroup({
      "organization_type": new FormControl("", [Validators.required]),
      "package_type_id": new FormControl("", [Validators.required]),
      "status": new FormControl(1, [Validators.required]),
      "discount_type": new FormControl(2, [Validators.required]),
      "discount_percentage": new FormControl(0, [Validators.required]),
      "discount_amount": new FormControl(0, [Validators.required]),
      "new_license_cost": new FormControl(0, [Validators.required]),
      "number_of_license": new FormControl("", [Validators.required, Validators.maxLength(4)]),
      "total_price": new FormControl(0, [Validators.required])
    });
    // this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
    //formgroup for company info
    this.EditOrgInfo = new FormGroup({
      "organization_id": new FormControl('', [Validators.required]),
      "organization_name": new FormControl('', [Validators.required, Validators.maxLength(48)]),
      "logo": new FormControl(''),
      "contact_person_name": new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), Validators.maxLength(48)]),
      "contact_number": new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl('', [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "email": new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "organization_type": new FormControl('', [Validators.required]),

    });
    //formgroup for edit company address info
    this.EditOrgAddressInfo = new FormGroup({
      "country_id": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "state_id": new FormControl(0, [Validators.required, Validators.maxLength(7)]),
      "city_id": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "post_code": new FormControl(0, [Validators.required, Validators.maxLength(6), Validators.minLength(6), Validators.pattern("^[0-9]*$")]),
      "landmark": new FormControl(0),
      "street": new FormControl(0, [Validators.required]),
      "plot_number": new FormControl(0, [Validators.required, Validators.pattern("^[0-9%+-/A-Z a-z]{2,32}$"), this.noWhitespace]),
      "status": new FormControl(0, [Validators.required])
    });
    //formgroup for extend duration
    this.RenewSubscription = new FormGroup({
      "organization_id": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "number_of_license": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "amount": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "discount_percentage": new FormControl(0, [Validators.required]),
      "discount_amount": new FormControl(0, [Validators.required]),
      "end_date": new FormControl('', [Validators.required]),
      "total_price": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
    });
    //formgroup for add more license
    this.AddmoreSubscription = new FormGroup({
      "organization_id": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "number_of_license": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "number_of_license_required": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "number_of_license_added": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "amount": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
      "discount_percentage": new FormControl(0, [Validators.required]),
      "discount_amount": new FormControl(0, [Validators.required]),
      "total_price": new FormControl(0, [Validators.required, Validators.maxLength(4)]),
    });
    //formgroup for remove license
    this.RemoveLicense = new FormGroup({
      "organization_id": new FormControl('', [Validators.required]),
      "number_of_license_required": new FormControl('', [Validators.required, Validators.maxLength(4)]),
      "number_of_license": new FormControl('', [Validators.required, Validators.maxLength(4)]),
      "number_of_license_reduced": new FormControl('', [Validators.required, Validators.maxLength(4)]),
      "amount": new FormControl('', [Validators.required]),
      "days_to_extended": new FormControl('', [Validators.required]),
      "discount_percentage": new FormControl(0, [Validators.required]),
      "discount_amount": new FormControl(0, [Validators.required]),
      "total_price": new FormControl('', [Validators.required, Validators.maxLength(4)]),
    })
  }
  // ----------------------------------------------error controls starts------------------------------------------------//
  get log() {
    return this.AddSubscriptionCompanyInfo.controls;
  }
  get log2() {
    return this.AddSubscriptionAddressInfo.controls;
  }
  get log3() {
    return this.AddSubscriptionPackageInfo.controls;
  }
  get log4() {
    return this.RemoveLicense.controls;
  }
  get log5() {
    return this.EditOrgInfo.controls;
  }
  get log6() {
    return this.EditOrgAddressInfo.controls;
  }
  get log7() {
    return this.RenewSubscription.controls;
  }
  // ----------------------------------------------error controls ends--------------------------------------------------//
  add_license() {
    this.AddmoreSubscription.patchValue({
      "total_price": '',
    });
    console.log(this.AddmoreSubscription.value.number_of_license_added)
    if (this.AddmoreSubscription.value.number_of_license_added == '' || this.AddmoreSubscription.value.number_of_license_added == null || this.AddmoreSubscription.value.number_of_license_added == 0) {
      // this.number_button = false;
    }
    else {
      // this.number_button = true;
    }
  }
  fetchNews(evt: any) {
    this.activeId = evt.nextId;
    this.evt = evt; // has nextId that you can check to invoke the desired function
  }
  fileChangeEvents(fileInput: any) {
    this.message ='';
    console.log(fileInput.target.files[0].name);
    this.ByDefault = true;
    this.Filename = fileInput.target.files[0].name;
    if (this.AddSubscriptionCompanyInfo) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';
          this.AddSubscriptionCompanyInfo.value.invalid = true;
          this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.AddSubscriptionCompanyInfo.value.invalid = true;
          this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': true });
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.AddSubscriptionCompanyInfo.value.invalid = true;
              this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': true });
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.AddSubscriptionCompanyInfo.value.invalid = false;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }
    else if (this.EditOrgInfo) {
      this.cardImageBase64 = '';
      this.spare_image = '';
      this.isImageSaved = false;
      this.imageError = null;
      if (fileInput.target.files && fileInput.target.files[0]) {
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
          this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          this.EditOrgInfo.controls['logo'].setErrors({ 'incorrect': true });
          this.EditOrgInfo.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {

          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          this.EditOrgInfo.controls['logo'].setErrors({ 'incorrect': true });
          this.EditOrgInfo.value.invalid = true;
          this.cardImageBase64 = null;
          this.isImageSaved = false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const img_height = rs.currentTarget['height'];
            const img_width = rs.currentTarget['width'];
            if (img_height > max_height && img_width > max_width) {
              this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
              this.EditOrgInfo.controls['logo'].setErrors({ 'incorrect': true });
              this.EditOrgInfo.value.invalid = true;
              this.cardImageBase64 = null;
              this.isImageSaved = false;
            } else {
              this.EditOrgInfo.value.valid = true;
              const imgBase64Path = e.target.result;
              this.cardImageBase64 = imgBase64Path;
              this.isImageSaved = true;
            }
          };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
      }
    }



  }
  removeImage() {
    console.log("Remove")
    if (this.AddSubscriptionCompanyInfo) {
      this.message = "Image is required";
      this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': true });
      this.AddSubscriptionCompanyInfo.value.invalid = true;
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
    }
     if (this.EditOrgInfo) {
      console.log(this.EditOrgInfo.value)
      console.log(this.EditOrgInfo.value.valid)
      this.EditOrgInfo.controls['logo'].setErrors({ 'incorrect': true });
      this.EditOrgInfo.value.valid = false;
      this.message = "Image is required";
      this.cardImageBase64 = null;
      this.isImageSaved = false;
      this.ByDefault = false;
      console.log(this.EditOrgInfo.value.valid)
    }


  }
  //get state dropdown 
  onChangeCountry(country_id) {

    var country = +country_id; // country: number                                    
    var url = 'get_state/?';             // api url for getting the details with using post params
    var id = "country_id";
    this.ajax.getdataparam(url, id, country).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.states = result.response.data;                                                //reloading the component       
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
    // }
  }
  //get city dropdown 
  onChangeState(state_id) {
    var state = +state_id; // state: number
    var id = "state_id";
    var url = 'get_city/?';                                  // api url for getting the cities
    this.ajax.getdataparam(url, id, state).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.cities = result.response.data;              //storing the api response in the array       
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  //--------------------------------------------------------------------------------------------------//
  //image file preview
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }
  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }
  //Functions for image cropper
  imageChangedEvent: any = '';
  croppedImage: any = '';
  //image upload
  fileChangeEvent(event: any): void {
    this.AddSubscriptionCompanyInfo.value.invalid = true;
    this.Filename = event.target.files[0].name;
    this.ByDefault = true;
    this.croppedImage = '';
    console.log(event)
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!_.includes(allowed_types, event.target.files[0].type)) {
      this.imageChangedEvent = '';
      this.AddSubscriptionCompanyInfo.value.invalid = true;
      this.image_validation = "Only Image will be allowed ( JPG | PNG )"; //chack file extension
      this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': true });
    }
    else {
      this.AddSubscriptionCompanyInfo.value.invalid = false;
      // this.AddSubscriptionCompanyInfo.controls['logo'].setErrors({ 'incorrect': false });

      this.image_validation = "";
      // this.selectedfile = false; //show confinrm button
      this.cropped_img = true; // image cropper 
      this.imageChangedEvent = event;

    }
  }

  //image cropping
  // imageCropped(event: ImageCroppedEvent) {
  //   this.imageChangedEvent = event.base64;
  // }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
  }
  // upload_img() { //button to clear the cropper
  //   console.log( this.imageChangedEvent)
  //   this.cropped_img = false;
  //   // this.selectedfile = true;
  // }
  upload_img() { //button to clear the cropper
    console.log(this.imageChangedEvent)
    this.cropped_img = false;
    this.selectedfile = true;
  }
  index: any;
  ngOnInit() {
    this.restrictdate = new Date();
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.showTable = false;
    this.getsubscriptiondata();                          //load package data
    this.getcountry();                       //get country details    
  }
  //To avoid empty space
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  //get country details for dropdown listing
  getcountry() {
    var url = 'get_country/';                                  // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.country = result.response.data;              //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  //get package details
  getpackagedata() {
    var url = 'get_package_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.package_details = result.response.data;                    //storing the api response in the array
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }
  //function to get the details of organization subscribed
  getsubscriptiondata() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_organization_details/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400" || result.response.response_code == "500") {
        this.showTable = true;
        this.subscription_details = result.response.data;                    //storing the api response in the array
        console.log(this.subscription_details)
        this.overlayRef.detach();
        this.chRef.detectChanges();
      }
      else {
        this.showTable = true;
        this.subscription_details = result.response.data;                    //storing the api response in the array
        console.log(this.subscription_details)
        this.overlayRef.detach();
        this.chRef.detectChanges();
        this.dtTrigger.next();

      }


    }, (err) => {
      this.overlayRef.detach();
      console.log(err);                                                 //prints if it encounters an error
    });
    this.getpackagedata();
  }

  //---------------------------------------------- * VALIDATIONS STARTS * ---------------------------//
  //function to check the validation for org name,contact number and email
  // dataValidation(param) {
  //   this.params = param;
  //   console.log(this.params);
  //   var url = 'validate_organization/';                                  // api url for getting the cities
  //   this.ajax.postdata(url, this.params).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       console.log(this.params)
  //       this.email_error = '';
  //       this.contactnum_error = ''
  //       this.orgname_error = ''
  //     }
  //     if (result.response.response_code == "500") {
  //       this.validation_data = result.response.message;
  //       if (this.validation_data == " Email already exist") {
  //         this.email_error = "Email already exist";
  //         console.log(this.email_error)
  //       }
  //       else if (this.validation_data == " Contact number already exist") {
  //         this.contactnum_error = "Contact Number already exist";
  //         console.log(this.contactnum_error)
  //       }
  //       else if (this.validation_data == " Company name already exist") {
  //         this.orgname_error = "Company name already exist";
  //         console.log(this.orgname_error)
  //       }
  //       console.log(this.validation_data);
  //     }
  //   }, (err) => {
  //     console.log(err);                                        //prints if it encounters an error
  //   });

  // }
  //validation for email to check whether the email is already exists
  addEmailValidation() {
    this.AddSubscriptionCompanyInfo.value.invalid = true;
    this.params = {
      "email": this.AddSubscriptionCompanyInfo.value.email
    }

    var url = 'validate_organization/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.email_error = '';
        this.AddSubscriptionCompanyInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.email_error = result.response.message;
        this.AddSubscriptionCompanyInfo.value.invalid = true;
        this.AddSubscriptionCompanyInfo.controls['email'].setErrors({ 'incorrect': true });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  //validation for contact number to check whether the contact number is already exists
  addContactValidation() {
    this.AddSubscriptionCompanyInfo.value.invalid = true;
    this.params = {
      "contact_number": this.AddSubscriptionCompanyInfo.value.contact_number
    }
    var url = 'validate_organization/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.contactnum_error = '';
        this.AddSubscriptionCompanyInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.contactnum_error = result.response.message;
        this.AddSubscriptionCompanyInfo.value.invalid = true;
        this.AddSubscriptionCompanyInfo.controls['contact_number'].setErrors({ 'incorrect': true });

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });


    // this.contact_setvalue=this.NewCustomerInfo.value.contact_number

  }
  //validation for organization to check whether the organization name is already exists
  addOrgNameValidation() {
    this.AddSubscriptionCompanyInfo.value.invalid = true;
    this.params = {
      "organization_name": this.AddSubscriptionCompanyInfo.value.organization_name
    }
    var url = 'validate_organization/?';                                  // api url for getting the cities
    this.ajax.postdata(url, this.params).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.validation_data = result.response.data;
        this.orgname_error = '';
        this.AddSubscriptionCompanyInfo.value.invalid = false;
      }
      else if (result.response.response_code == "500") {
        this.orgname_error = result.response.message;
        this.AddSubscriptionCompanyInfo.value.invalid = true;
        this.AddSubscriptionCompanyInfo.controls['organization_name'].setErrors({ 'incorrect': true });

      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });

  }
  addalternatecontact() {
    console.log("alter")
    console.log(this.AddSubscriptionCompanyInfo.value.contact_number);
    console.log(this.AddSubscriptionCompanyInfo.value.alternate_contact_number);
    if (this.AddSubscriptionCompanyInfo.value.contact_number == this.AddSubscriptionCompanyInfo.value.alternate_contact_number) {
      this.alternate_error = "Contact and alternate contact number should be different.";
      this.AddSubscriptionCompanyInfo.value.invalid = true;
      this.AddSubscriptionCompanyInfo.controls['alternate_contact_number'].setErrors({ 'incorrect': true });
    }
    else {
      this.alternate_error = '';
      this.AddSubscriptionCompanyInfo.value.invalid = false;
    }
  }
  addacontact() {
    console.log("con")

    console.log(this.AddSubscriptionCompanyInfo.value.contact_number);
    console.log(this.AddSubscriptionCompanyInfo.value.alternate_contact_number);
    if (this.AddSubscriptionCompanyInfo.value.alternate_contact_number == this.AddSubscriptionCompanyInfo.value.contact_number) {
      this.contact_error = "Contact and alternate contact number should be different.";
      this.AddSubscriptionCompanyInfo.value.invalid = true;
      this.AddSubscriptionCompanyInfo.controls['contact_number'].setErrors({ 'incorrect': true });

    }
    else {
      this.contact_error = '';
      this.AddSubscriptionCompanyInfo.value.invalid = false;
    }
  }
  //company tab validation
  checkvalidation() {
    var data = this.AddSubscriptionCompanyInfo.value;
    data["logo"] = this.croppedImage;
    console.log(this.AddSubscriptionCompanyInfo.valid)
    console.log(this.AddSubscriptionCompanyInfo.invalid)

    console.log(this.AddSubscriptionCompanyInfo.value)
    //Check whether the conmpany details is valid or not
    if (this.AddSubscriptionCompanyInfo.valid)                                                   //Check if the package type is empty
    {
      //if valid allow to move next tab
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
    else {
      this.activeId = "tab-selectbyid1";
      this.preId = "tab-selectbyid2";
    }
  }
  //Adderss tab validation
  checkcompanyvalidation() {
    console.log(this.AddSubscriptionAddressInfo.value)
    console.log(this.AddSubscriptionAddressInfo.valid)
    //Check whether the address details is valid or not
    if (this.AddSubscriptionAddressInfo.valid) {
      //if valid allow to move next tab
      this.activeId = "tab-selectbyid3";
      this.preId = "tab-selectbyid2";
    }
    else {
      console.log("not valid")
    }
  }
  //Edit organization tab validation
  checkeditvalidation() {
    if (this.EditOrgInfo.value.valid == false) {
      this.activeId = "tab-selectbyid1";
      this.preId = "tab-selectbyid2";
    }
    else {
      console.log("valid")
      this.activeId = "tab-selectbyid2";
      this.preId = "tab-selectbyid1";
    }
  }
  //---------------------------------------------- * VALIDATIONS ENDS * ---------------------------//


  //------------------------------------------ * CREATE SUBSCRIPTION and EDIT ORGANIZATION STARTS * --------------------------//
  //get the number of license
  get_noOof_license(no_of_license_count) {   // no of license
    // this.Calculate_button = true;
    this.discount_percentage = '';                    //refresh the discount percentage 
    // this.AddSubscriptionPackageInfo.controls['discount_percentage'].setValue('');
    this.AddSubscriptionPackageInfo.controls['total_price'].setValue('');
    this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue('');
    this.AddSubscriptionPackageInfo.controls['discount_percentage'].setValue('');
    this.no_of_license_count = no_of_license_count;    //store the number of license to this variable
    console.log("Number of license required = " + this.no_of_license_count)
    if (this.package_type_id !== undefined) {
      //if package is selected before enter the number of license
      this.noOflicenseerror = "";
      if (this.package_type_id == 1) {
        // this.Calculate_button = false;
        //if package type is FREE
        if (no_of_license_count > 10) {
          //number of license should be 10 or less than 10
          this.noOflicenseerror = "Only 10 license will be free";
          this.AddSubscriptionPackageInfo.controls['total_price'].setValue('')
        }
        else {
          //if package type is PAID
          this.noOflicenseerror = "";

        }
      }
    }
    else {
      //if package is not selected before enter the number of license then through the errror message
      this.noOflicenseerror = "Select Package type before enter no of license"
      //if package type is already selected
    }
  }

  //get license cost and calculate total amount based on the per license cost
  calculate_createsub_total_amount() {
    this.loadingCalculate = true;
    this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0); //during number of license initiate the discount amount as 0
    this.AddSubscriptionPackageInfo.controls['discount_percentage'].setValue(0); //during number of license initiate the discount amount as 0
    // this.overlayRef.attach(this.LoaderComponentPortal);
    this.newlicenseamount = 0;
    console.log(this.no_of_license_count, "this.no_of_license_count")
    this.numberoflicense_required = this.no_of_license_count; // Number of license 
    var url = 'get_license_per_cost/';                                       //api url of add
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {//if success
        this.loadingCalculate = false;
        this.getlicense = result.response.data;  //data which contains package plan details
        console.log('api result' + this.getlicense)
        // loop to find the license range 
        for (var i = 0; i < this.getlicense.length; i++) {
          this.count = 0
          //get from and to range
          for (var j = this.getlicense[i].from_user_range; j <= this.getlicense[i].to_user_range; j++) {
            this.count++;
          }
          console.log('Count Value(User Range for license cost calculation)' + this.count)
          /* To find the user range if(
             Number license is 250  and
             User Range is 1-10  -Rs 50 /per user/per month
                           11-100 -Rs 100 /per user/per month
                           101-150 -150 /per user/per month
             *First find the last count for the particular range
             from 1 st loop we get 10 as count value
             from 2 nd loop we get 100 as count value 
             from 3 rd loop we get 150 as count value )
          */
          //check whether the number of license is greater than the count value
          if (this.numberoflicense_required > this.count) {
            //if
            // license amount is product of count of users and new license cost
            this.license_amount = this.count * this.getlicense[i].new_license_cost;
            console.log("New license cost per license = " + this.getlicense[i].new_license_cost)
            //  numberoflicense_required is subtract from  total license to to user range
            // (from the 3rd loop we get 150 as count value and numberoflicense_required is 140 so here we subtract the count value from numberoflicense_required)
            this.numberoflicense_required = this.numberoflicense_required - this.count;
            // newlicenseamount is additions of license_amount
            this.newlicenseamount = this.newlicenseamount + this.license_amount;
            console.log('total license cost' + this.newlicenseamount);

          }
          else {
            //if the count is less than the count value
            console.log("New license cost per license = " + this.getlicense[i].new_license_cost)
            this.license_amount = this.numberoflicense_required * this.getlicense[i].new_license_cost;
            // newlicenseamount is additions of license_amount
            this.newlicenseamount = this.newlicenseamount + this.license_amount;
            this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.newlicenseamount);
            this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.newlicenseamount);
            this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
            break;
          }
          this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
          this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.newlicenseamount);
          this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.newlicenseamount);
          // this.overlayRef.detach();

        }
        //-----------From above we can calculate the amount per month for the subscribed users------------------------//
        //------------Using below function we can calculate the amount per year--------------------------------------//
        //---------------The following is to determine whether the current year is leap year or not------------------//
        var d = new Date();
        var year = d.getFullYear();
        var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
        //if leap year consider the no of days in as 366
        if (leapyear) {
          this.perdaycost = this.newlicenseamount / 30; // Finding the amount per day
          console.log("Cost Perday" + this.perdaycost)
          this.peryearcost = this.perdaycost * 366;//Finding the amount per year
          console.log("Cost Peryear" + this.peryearcost)
          // this.overlayRef.detach();

        }
        else //if not leap year consider the no of days in as 365
        {
          this.perdaycost = this.newlicenseamount / 30; // Finding the amount per day
          console.log("Cost Perday" + this.perdaycost)
          this.peryearcost = this.perdaycost * 365;//Finding the amount per year
          console.log("Cost Peryear" + this.peryearcost)
          // this.overlayRef.detach();

          // break;
        }
        console.log("Final Total Price = " + this.peryearcost)
        this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
        this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.peryearcost);
        this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.peryearcost);
        // this.overlayRef.detach();

      }
      else if (result.response.response_code == "400") {             //if failure
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingCalculate = false;
        // this.overlayRef.detach();

      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingCalculate = false;
        // this.overlayRef.detach();

      }
    }, (err) => {
      this.loadingCalculate = false;
      console.log(err);//prints if it encounters an error
    });
  }

  //-----------Function for discount Calculations-----------//

  //get the discount percentage
  get_discount_percentage(disc_per) {
    console.log(disc_per, "disc_per")
    this.discount_percentage = disc_per;       // store the discount percentage to this variable
    console.log("Discount Percentage = " + this.discount_percentage)
    this.AddSubscriptionPackageInfo.controls['total_price'].setValue('');
    this.AddmoreSubscription.controls['total_price'].setValue('');
  }
  //total amount after discount
  calculate_createsub_discounted_amount() {
    console.log(this.peryearcost, "this.peryearcost")
    if (this.discount_percentage > 100)                                                                 //if amount greater than 100
    {
      //if discount percentage is greater than 100
      this.discount_error = "Discount percentage cannot be greater than 100"   //error message 
    }
    //  else if(this.discount_percentage == 0)                                                                 //if amount greater than 100
    // {
    //   console.log(this.peryearcost,"this.peryearcost")

    //   console.log(this.newlicenseamount)
    //   this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue('');

    //   this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.peryearcost);
    // }
    else {
      //if discount percentage is less than or equal 100
      this.discount_error = ""
      //Find discount in amount 
      console.log(this.discount_percentage, "discount_percentage")
      console.log(this.peryearcost, "this.peryearcost")
      if (this.discount_percentage == undefined || this.discount_percentage == '' || this.discount_percentage == null) {
        this.discount_percentage = 0
      }
      var discount_amount = Math.round((this.discount_percentage / 100) * this.peryearcost);
      console.log("Discount in Amount = " + discount_amount);
      //set the value to the formcontrol for display 
      this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(discount_amount);
      //Subract total amount for year from the discount amount to get the final total price
      var FinalTotal = this.peryearcost - discount_amount;
      console.log("Total Price After Discount= " + FinalTotal)
      //set the final total price to the formcontrol for display            
      this.AddSubscriptionPackageInfo.controls['total_price'].setValue(FinalTotal);
    }
  }

  //create organization
  create_organization() {
    this.loadingNew = true;
    //Check whether the package detail is valid or not
    console.log(this.AddSubscriptionPackageInfo.value)
    console.log(this.AddSubscriptionPackageInfo.valid)
    if (this.AddSubscriptionPackageInfo.valid && this.AddSubscriptionCompanyInfo.valid && this.AddSubscriptionAddressInfo.valid && this.noOflicenseerror == '') {
      console.log(this.AddSubscriptionPackageInfo.value)
      console.log(this.AddSubscriptionAddressInfo.valid)
      console.log(this.AddSubscriptionCompanyInfo.valid)
      //if valid
      this.AllSubscriptionData = {
        "organization_name": this.AddSubscriptionCompanyInfo.value.organization_name,
        "logo": this.cardImageBase64,
        "contact_person_name": this.AddSubscriptionCompanyInfo.value.contact_person_name,
        "contact_number": this.AddSubscriptionCompanyInfo.value.contact_number,
        "alternate_contact_number": this.AddSubscriptionCompanyInfo.value.alternate_contact_number,
        "email": this.AddSubscriptionCompanyInfo.value.email,
        "country_id": this.AddSubscriptionAddressInfo.value.country_id,
        "state_id": this.AddSubscriptionAddressInfo.value.state_id,
        "city_id": this.AddSubscriptionAddressInfo.value.city_id,
        "post_code": this.AddSubscriptionAddressInfo.value.post_code,
        "landmark": this.AddSubscriptionAddressInfo.value.landmark,
        "street": this.AddSubscriptionAddressInfo.value.street,
        "plot_number": this.AddSubscriptionAddressInfo.value.plot_number,
        "organization_type": this.AddSubscriptionPackageInfo.value.organization_type,
        "package_type_id": this.AddSubscriptionPackageInfo.value.package_type_id,
        "status": this.AddSubscriptionPackageInfo.value.status,
        "discount_type": this.AddSubscriptionPackageInfo.value.discount_type,
        "discount_percentage": this.AddSubscriptionPackageInfo.value.discount_percentage,
        "discount_amount": this.AddSubscriptionPackageInfo.value.discount_amount,
        "new_license_cost": this.AddSubscriptionPackageInfo.value.total_price, //change
        "number_of_license": this.AddSubscriptionPackageInfo.value.number_of_license,
        "total_price": this.AddSubscriptionPackageInfo.value.total_price
      }
      console.log(this.AllSubscriptionData)

      var data = this.AllSubscriptionData;
      console.log(data, "data")
      var url = 'create_organization/'                                        //api url to create organization
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.loadingNew = false;
          this.showTable = false;
          this.chRef.detectChanges();
          this.getsubscriptiondata();    //reloading the table data
          this.reset_all_create_subcriptiondata(); //reset the data
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();                             //to close the modal box
        }
        else if (result.response.response_code == "400") {
          this.error = result.response.message        //toastr message for error
          this.loadingNew = false;
        }
        else if (result.response.response_code == "500") {
          this.error = result.response.message      //toastr message for error
          this.loadingNew = false;
        }
        else {
          this.error = result.response.message       //toastr message for error
          this.loadingNew = false;
        }
      }, (err) => {
        this.error = "Somthing went wrong!"
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }
  //function to edit the organization details
  edit_organization() {
    console.log(this.EditOrgAddressInfo.value);
    console.log(this.EditOrgAddressInfo);
    if (this.EditOrgAddressInfo.valid) {
      this.EditOrgAddressData = {
        "organization_id": this.EditOrgInfo.value.organization_id,
        "organization_name": this.EditOrgInfo.value.organization_name,
        "contact_person_name": this.EditOrgInfo.value.contact_person_name,
        "contact_number": this.EditOrgInfo.value.contact_number,
        "alternate_contact_number": this.EditOrgInfo.value.alternate_contact_number,
        "email": this.EditOrgInfo.value.email,
        "organization_type": this.EditOrgInfo.value.organization_type,
        "country_id": this.EditOrgAddressInfo.value.country_id,
        "state_id": this.EditOrgAddressInfo.value.state_id,
        "city_id": this.EditOrgAddressInfo.value.city_id,
        "post_code": this.EditOrgAddressInfo.value.post_code,
        "landmark": this.EditOrgAddressInfo.value.landmark,
        "street": this.EditOrgAddressInfo.value.street,
        "plot_number": this.EditOrgAddressInfo.value.plot_number,
        "status": this.EditOrgAddressInfo.value.status,
      }
      console.log(this.EditOrgAddressData);
      var data = this.EditOrgAddressData;
      if (this.EditOrgInfo.value.logo == '' || this.EditOrgInfo.value.logo == undefined) {
        data["logo"] = '';
      }
      else {
        data["logo"] = this.cardImageBase64;
      }
      var url = 'edit_organization/'                                        //api url of add
      this.loadingEdit = true;
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.loadingEdit = false;
          this.showTable = false;
          this.getsubscriptiondata();
          this.chRef.detectChanges();
          this.modalService.dismissAll();
        }
        else if (result.response.response_code == "400") {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          this.loadingEdit = false;
        }
        else {
          this.toastr.error(result.response.message, 'Error');        //toastr message for error
          this.loadingEdit = false;
        }
      }, (err) => {
        this.loadingEdit = false;
        console.log(err);                                             //prints if it encounters an error
      });
    }
    else {
      console.log("invalid")
    }
  }
  //------------------------------------------- * CREATE SUBSCRIPTION and EDIT ORGANIZATION  ENDS * --------------------------------//


  //------------------------------------------------BUY MORE STARTS------------------------------------//
  //function is to calculate the amount for adding extra license
  calculate_buymore_total_amount(new_no_license) {
    this.discount_percentage = '';                    //refresh the discount percentage 
    this.loadingCal = true;
    console.log("Number of license subscribed from backend =", this.nooflicense)
    console.log("Number of license to add = ", new_no_license)
    if (new_no_license == undefined || new_no_license == null || new_no_license == '') {
      this.AddmoreSubscription.patchValue({
        "organization_id": this.view_subcrip.organization_id,
        "number_of_license_required": this.nooflicense,
        "number_of_license_added": 0,
        "number_of_license": 0,
        "discount_percentage": 0,
        "discount_amount": 0,
        "amount": 0,
        "total_price": 0,
      });

    } else {
      this.noof_license_to_add = new_no_license
      //--reintialising the global variables--//
      this.newlicenseamount = 0;
      this.license_amount = 0;
      //---------------------------------------//
      // this.nooflicense is the variable its sotes the data from backend which is the no of license already subscriped
      var number_of_added_license = parseInt(this.nooflicense) + parseInt(this.noof_license_to_add); // Adding the required no of license to the previous avaiable license
      console.log("Number of added License  =", number_of_added_license)   //total number of licenses
      var added_license = this.noof_license_to_add; // storing the newly entered no of license in a separate variable for passing this data to the formgroup for showing purpose
      var nolicene = parseInt(this.nooflicense)
      var url = 'get_package_management_details/'; // api url for getting the details
      this.ajax.getdata(url).subscribe((result) => {
        if (result.response.response_code == "200") {
          this.loadingCal = false;
          this.package_details = result.response.data;//storing the range value declarations into a variable
          console.log("Api result = ", this.package_details)
          //---iterating the variable to calculate the amount for the license-----------------//
          var newcount = 0;
          for (var i = 0; i < this.package_details.length; i++) {
            this.count = 0;// this variable is to store the no of license in the range
            for (var j = this.package_details[i].from_user_range; j <= this.package_details[i].to_user_range; j++) {
              this.count++;// here the count will be incremented till the no of license available in the range
            }
            /* To find the user range if(
              Number license is 250  and
              User Range is 1-10  -Rs 50 /per user/per month
                            11-100 -Rs 100 /per user/per month
                            101-150 -150 /per user/per month
              *First find the last count for the particular range
              from 1 st loop we get 10 as count value
              from 2 nd loop we get 100 as count value 
              from 3 rd loop we get 150 as count value )
           */
            console.log("count Value for number of license to add =", newcount)
            console.log("Count Value for number of license required =", this.count)
            console.log("nolicene", nolicene)
            console.log("Number of license to add=", new_no_license)
            console.log("Newcost per license=", this.package_details[i].new_license_cost)
            //product the number of license to add with new cost per license
            this.license_amount = new_no_license * this.package_details[i].new_license_cost;//multiply the licensecost with the count
            console.log("New cost for added new license per month" + this.license_amount)
            this.noof_license_to_add = parseInt(new_no_license) - this.count;// this is to calculate the remaining license since the range count is less than the no of license subscribed
            this.newlicenseamount = this.license_amount;// adding the values in the loop to a variable from second iteration
            console.log(this.newlicenseamount);
          }
          //-----------From above we can calculate the amount per month for the subscribed users------------------------//
          //------------Using below function we can calculate the amount per year--------------------------------------//
          //---------------The following is to determine whether the current year is leap year or not------------------//
          var d = new Date();
          var year = d.getFullYear();
          var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
          //if leap year consider the no of days in as 366
          if (leapyear) {
            console.log(this.newlicenseamount)
            this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
            console.log("license per day =", this.newlicenseamount)
            // this.newlicenseamount = this.newlicenseamount * 366;//Finding the amount per year
          }
          else {
            //if not leap year consider the no of days in as 365
            this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
            console.log("license per day =", this.newlicenseamount)
            //   this.newlicenseamount = this.newlicenseamount * 365;//Finding the amount per year
          }
          //------to calculate the amount for the added license for the remaining days   
          const dt1 = moment(this.startdate, "DD-MM-YYYY h:mma").toDate();// change the start date to date format
          const dt2 = moment(this.enddate, "DD-MM-YYYY h:mma").toDate();;// change the End date to date format
          //To calculate the total no of days subscribed
          //subracting the end date from start date
          var total_no_of_days = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
          console.log("Total no of days subscribed  =", total_no_of_days)
          var today = new Date();//Getting the current date
          //To calculate the total no of days completed subscribed
          //subracting the start date from today date i.e renew date

          // var total_no_of_days_completed = Math.floor((Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) - Date.UTC(today.getFullYear(), today.getMonth(), today.getDate())) / (1000 * 60 * 60 * 24));
          // console.log("Total days completed =", total_no_of_days_completed)

          var total_no_of_days_completed = Math.floor((Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
          console.log("Total days completed =", total_no_of_days_completed)

          //To calculate the remaining days in the subscription
          //subracting the end date from today date
          var total_days_remaining = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(today.getFullYear(), today.getMonth(), today.getDate())) / (1000 * 60 * 60 * 24));
          console.log("Total days remaining =", total_days_remaining)
          //calculate the additional amount for the newly subscribed licenses by product the license per cost for the license to add with remaining number of days
          var additional_amount_to_be_paid = this.newlicenseamount * total_days_remaining;
          //storing the amount to paid to a variable
          this.newlicenseamount = additional_amount_to_be_paid;
          console.log("Additional amount to be paid  =", this.newlicenseamount)
          this.AddmoreSubscription.patchValue({
            "organization_id": this.view_subcrip.organization_id,
            "number_of_license_required": this.nooflicense,
            "number_of_license_added": added_license,
            "number_of_license": number_of_added_license,
            "discount_percentage": 0,
            "discount_amount": 0,
            "amount": Math.round(this.newlicenseamount),
            "total_price": Math.round(this.newlicenseamount),
          });
        }
      }, (err) => {
        this.loadingCal = false;
        console.log(err);                                                 //prints if it encounters an error
      });
    }
  }
  //function is to calculate the discount amount for adding extra license
  calculate_buymore_discounted_amount() {
    console.log("Total price Before discount =", this.newlicenseamount)
    if (this.discount_percentage > 100)                                                                 //if amount greater than 100
    {
      //if discount percentage is greater than 100
      this.discount_error = "Discount percentage cannot be greater than 100"   //error message 
    }
    else {
      //if discount percentage is less than or equal 100
      this.discount_error = ""
      if (this.discount_percentage == undefined || this.discount_percentage == '' || this.discount_percentage == null) {
        this.discount_percentage = 0
        var discount_in_amount = 0
        this.AddmoreSubscription.controls['discount_amount'].setValue(0);
        // this.AddmoreSubscription.controls['total_price'].setValue(0);                           //for add subscription                           
      }
      else {
        //Find discount in amount 
        var discount_in_amount = Math.round((this.discount_percentage / 100) * +this.newlicenseamount);
        //set the discount amount to the formgroup for display
        this.AddmoreSubscription.controls['discount_amount'].setValue(discount_in_amount);
        console.log("Discount in Amount =", discount_in_amount);
        //calculating the total amount after discount reduction
        var total_price = Math.round(this.newlicenseamount - discount_in_amount)                    //if percentage calculate amount   
        console.log("Total price After dicount =", total_price)
        //set the discount amount to the formgroup for display
        this.AddmoreSubscription.controls['total_price'].setValue(total_price);                           //for add subscription                           
      }
    }
  }
  //function is to submit the details when the license is added
  add_more_license() {
    console.log(this.AddmoreSubscription)
    console.log(this.AddmoreSubscription.value, "sdfg")
    this.loadingSubmit = true;
    var data = this.AddmoreSubscription.value;// storing the form group value
    var url = 'renew_add/';//api url of add
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.loadingSubmit = false;
        this.showTable = false;
        this.getsubscriptiondata();
        this.chRef.detectChanges();
        this.toastr.success(result.response.message, 'Success');//toastr message for success
        this.AddmoreSubscription.reset();
        //dismiss the modal box
        this.modalService.dismissAll();                                          //to close the modal box
      }
      else if (result.response.response_code == "400") {
        this.loadingSubmit = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
      else {
        this.loadingSubmit = false;
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
      }
    }, (err) => {
      this.loadingSubmit = false;
      console.log(err);                                             //prints if it encounters an error
    });
  }
  //--------------------------------------------------BUY MORE ENDS----------------------------------//


  //--------------------------------------------- * EXTEND DURATION STARTS * -------------------------------------//

  //calculation for extending the duration
  calculate_extendduration_total_amount(number_of_license) {
    this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0); //during number of license initiate the discount amount as 0
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.newlicenseamount = 0;
    this.numberoflicense_required = number_of_license; // Number of license 
    var url = 'get_license_per_cost/';                                       //api url of add
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {//if success
        this.getlicense = result.response.data;  //data which contains package plan details
        console.log('api result' + this.getlicense)
        // loop to find the license range 
        for (var i = 0; i < this.getlicense.length; i++) {
          this.count = 0
          //get from and to range
          for (var j = this.getlicense[i].from_user_range; j <= this.getlicense[i].to_user_range; j++) {
            this.count++;
          }
          console.log('Count Value(User Range for license cost calculation)' + this.count)
          /* To find the user range if(
             Number license is 250  and
             User Range is 1-10  -Rs 50 /per user/per month
                           11-100 -Rs 100 /per user/per month
                           101-150 -150 /per user/per month
             *First find the last count for the particular range
             from 1 st loop we get 10 as count value
             from 2 nd loop we get 100 as count value 
             from 3 rd loop we get 150 as count value )
          */
          //check whether the number of license is greater than the count value
          if (this.numberoflicense_required > this.count) {
            //if
            // license amount is product of count of users and new license cost
            this.license_amount = this.count * this.getlicense[i].new_license_cost;
            console.log("New license cost per license = " + this.getlicense[i].new_license_cost)
            //  numberoflicense_required is subtract from  total license to to user range
            // (from the 3rd loop we get 150 as count value and numberoflicense_required is 140 so here we subtract the count value from numberoflicense_required)
            this.numberoflicense_required = this.numberoflicense_required - this.count;
            // newlicenseamount is additions of license_amount
            this.newlicenseamount = this.newlicenseamount + this.license_amount;
            console.log('total license cost' + this.newlicenseamount);
          }
          else {
            //if the count is less than the count value
            console.log("New license cost per license = " + this.getlicense[i].new_license_cost)
            this.license_amount = this.numberoflicense_required * this.getlicense[i].new_license_cost;
            // newlicenseamount is additions of license_amount
            this.newlicenseamount = this.newlicenseamount + this.license_amount;
            this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.newlicenseamount);
            this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.newlicenseamount);
            this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
            break;
          }
          this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
          this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.newlicenseamount);
          this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.newlicenseamount);
          this.overlayRef.detach();
        }
        //-----------From above we can calculate the amount per month for the subscribed users------------------------//
        //------------Using below function we can calculate the amount per year--------------------------------------//
        //---------------The following is to determine whether the current year is leap year or not------------------//
        var d = new Date();
        var year = d.getFullYear();
        var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
        //if leap year consider the no of days in as 366
        if (leapyear) {
          this.perdaycost = this.newlicenseamount / 30; // Finding the amount per day
          console.log("Cost Perday" + this.perdaycost)
          this.peryearcost = this.perdaycost * 366;//Finding the amount per year
          console.log("Cost Peryear" + this.peryearcost)
          this.overlayRef.detach();
        }
        else //if not leap year consider the no of days in as 365
        {
          this.perdaycost = this.newlicenseamount / 30; // Finding the amount per day
          console.log("Cost Perday" + this.perdaycost)
          this.peryearcost = this.perdaycost * 365;//Finding the amount per year
          console.log("Cost Peryear" + this.peryearcost)
          this.overlayRef.detach();
          // break;
        }
        console.log("Final Total Price = " + this.peryearcost)
        this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
        this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(this.peryearcost);
        this.AddSubscriptionPackageInfo.controls['total_price'].setValue(this.peryearcost);
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "400") {             //if failure
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);//prints if it encounters an error
    });
  }
  //function to Renewal for same license extending only the years
  extend_duration() {
    this.loading = true;
    console.log(this.RenewSubscription.valid)
    var year = this.RenewSubscription.value.end_date.year;
    var month = this.RenewSubscription.value.end_date.month;
    var day = this.RenewSubscription.value.end_date.day;
    var dateFormatedd = year + "-" + month + "-" + day; //changing the date format
    this.RenewSubscription.value.end_date = dateFormatedd;
    this.extended_date = this.RenewSubscription.value.end_date
    //set the value to the a variable which is going to passed as parameter for calculation function
    this.no_of_license_count = this.RenewSubscription.value.number_of_license;
    // this.get_amount_for_renewal(this.extended_date);
    console.log(this.extended_date, "extended_date")
    // this.overlayRef.attach(this.LoaderComponentPortal); //loader
    console.log("Number of license subscribed from backend =", this.nooflicense)
    //--reintialising the global variables--//
    this.newlicenseamount = 0;
    this.license_amount = 0;
    var url = 'get_package_management_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.package_details = result.response.data;//storing the range value declarations into a variable
        console.log("Api result = ", this.package_details)
        //---iterating the variable to calculate the amount for the license-----------------//
        var newcount = 0;
        for (var i = 0; i < this.package_details.length; i++) {
          this.count = 0;// this variable is to store the no of license in the range
          for (var j = this.package_details[i].from_user_range; j <= this.package_details[i].to_user_range; j++) {
            this.count++;// here the count will be incremented till the no of license available in the range
          }
          /* To find the user range if(
            Number license is 250  and
            User Range is 1-10  -Rs 50 /per user/per month
                          11-100 -Rs 100 /per user/per month
                          101-150 -150 /per user/per month
            *First find the last count for the particular range
            from 1 st loop we get 10 as count value
            from 2 nd loop we get 100 as count value 
            from 3 rd loop we get 150 as count value )
         */
          console.log("count Value for number of license to add =", newcount)
          console.log("Count Value for number of license required =", this.count)
          console.log("Newcost per license=", this.package_details[i].new_license_cost)
          //product the number of license to add with new cost per license
          this.license_amount = this.nooflicense * this.package_details[i].new_license_cost;//multiply the licensecost with the count
          console.log("New cost for added new license per month" + this.license_amount)
          this.noof_license_to_add = parseInt(this.nooflicense) - this.count;// this is to calculate the remaining license since the range count is less than the no of license subscribed
          this.newlicenseamount = this.license_amount;// adding the values in the loop to a variable from second iteration
          console.log(this.newlicenseamount);
        }
        //-----------From above we can calculate the amount per month for the subscribed users------------------------//
        //------------Using below function we can calculate the amount per year--------------------------------------//
        //---------------The following is to determine whether the current year is leap year or not------------------//
        var d = new Date();
        var year = d.getFullYear();
        var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
        //if leap year consider the no of days in as 366
        if (leapyear) {
          console.log(this.newlicenseamount)
          this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
          console.log("license per day =", this.newlicenseamount)
          // this.newlicenseamount = this.newlicenseamount * 366;//Finding the amount per year
        }
        else {
          //if not leap year consider the no of days in as 365
          this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
          console.log("license per day =", this.newlicenseamount)
          //   this.newlicenseamount = this.newlicenseamount * 365;//Finding the amount per year
        }
        //------to calculate the amount for the added license for the remaining days   
        var extended_date = this.extended_date
        console.log(this.startdate, "Subscription Start Date")
        console.log(this.enddate, "Subscription End date")
        console.log(extended_date, "Subscription Extended date")
        var splitted = extended_date.split("-", 3);
        var year: number = +splitted[0];
        var day = splitted[2];
        var month = splitted[1];
        var data = {
          year: year,
          month: month,
          day: day,
        }
        console.log(data, "data")
        var dateFormated = day + "-" + month + "-" + year; //change the date format as dd-mm-yyyy
        console.log(dateFormated)
        const dt1 = moment(this.startdate, "DD-MM-YYYY h:mma").toDate();// change the start date to date format
        const dt2 = moment(this.enddate, "DD-MM-YYYY h:mma").toDate();// change the End date to date format
        const dt3 = moment(dateFormated, "DD-MM-YYYY h:mma").toDate();// change the Extended date to date format
        console.log("Start date =", dt1)
        console.log("End date =", dt2)
        console.log("Extended date=", dt3)
        //To calculate the total no of days subscribed
        //subracting the end date from start date
        var total_no_of_days = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        console.log("Total no of days subscribed  =", total_no_of_days)
        var today = new Date();//Getting the current date
        //To calculate the total no of days completed subscribed
        //subracting the start date from today date i.e renew date
        var total_no_of_days_completed = Math.floor((Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        console.log("Total days completed =", total_no_of_days_completed)
        //To calculate the remaining days in the subscription
        //subracting the end date from today date
        var total_days_remaining = Math.floor((Date.UTC(dt3.getFullYear(), dt3.getMonth(), dt3.getDate()) - Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate())) / (1000 * 60 * 60 * 24));
        console.log("Total days remaining =", total_days_remaining)
        //its may need for calculate the total number of days added and extended .Its need some clarification
        // var total_numberof_day_to_be_use=(total_no_of_days - total_no_of_days_completed) +total_days_remaining
        // console.log("Total number of days going to use=",total_numberof_day_to_be_use)
        //calculate the additional amount for the newly subscribed licenses by product the license per cost for the license to add with remaining number of days
        console.log(this.newlicenseamount, " this.newlicenseamount")
        console.log(total_days_remaining, "total_days_remaining")
        var additional_amount_to_be_paid = this.newlicenseamount * total_days_remaining;
        //storing the amount to paid to a variable
        this.newlicenseamount = additional_amount_to_be_paid;
        console.log("Additional amount to be paid  =", this.newlicenseamount)
        console.log(this.newlicenseamount, "this.newlicenseamount")
        console.log(this.amountpaid, "this.amountpaid")
        var total_amount = (+this.newlicenseamount) + (+this.amountpaid)
        console.log(total_amount, "total_amount")
        console.log(parseFloat(typeof (total_amount)).toFixed(2)),
          //set the value to the formgroup 
          this.RenewSubscription.patchValue({
            "organization_id": this.view_subcrip.organization_id,
            "number_of_license": this.nooflicense,
            "end_date": dateFormatedd,
            // "amount": this.newlicenseamount, //if back end need amount need to pass this currently not need so just pass 0
            "amount": this.newlicenseamount,
            "total_price": total_amount.toFixed(2),
          })
        console.log(this.RenewSubscription.value)
        var data1 = this.RenewSubscription.value; // storing the form group value
        var url = 'renew_organization/'                                         //api url of add
        this.ajax.postdata(url, data1).subscribe((result) => {
          if (result.response.response_code == "200") {
            this.toastr.success(result.response.message, 'Success');    //toastr message for success
            this.loading = false;
            this.showTable = false;
            this.getsubscriptiondata();
            this.chRef.detectChanges();
            this.modalService.dismissAll();                             //to close the modal box
          }
          else if (result.response.response_code == "400") {
            this.loading = false;
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
          else {
            this.loading = false;
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
        }, (err) => {
          this.loading = false;

          console.log(err);                                             //prints if it encounters an error
        });
      }
    }, (err) => {
      this.loading = false;
      console.log(err);      //prints if it encounters an error
    });

    // }
  }
  //Function to calculate the amount for extending the subscribed license----------------------------------------------------------------------------------------//
  // get_amount_for_renewal(extended_date) {
  //   console.log(extended_date, "extended_date")
  //   this.overlayRef.attach(this.LoaderComponentPortal); //loader
  //   console.log("Number of license subscribed from backend =", this.nooflicense)
  //   //--reintialising the global variables--//
  //   this.newlicenseamount = 0;
  //   this.license_amount = 0;
  //   var url = 'get_package_management_details/'; // api url for getting the details
  //   this.ajax.getdata(url).subscribe((result) => {
  //     if (result.response.response_code == "200") {
  //       this.package_details = result.response.data;//storing the range value declarations into a variable
  //       console.log("Api result = ", this.package_details)
  //       //---iterating the variable to calculate the amount for the license-----------------//
  //       var newcount = 0;
  //       for (var i = 0; i < this.package_details.length; i++) {
  //         this.count = 0;// this variable is to store the no of license in the range
  //         for (var j = this.package_details[i].from_user_range; j <= this.package_details[i].to_user_range; j++) {
  //           this.count++;// here the count will be incremented till the no of license available in the range
  //         }
  //         /* To find the user range if(
  //           Number license is 250  and
  //           User Range is 1-10  -Rs 50 /per user/per month
  //                         11-100 -Rs 100 /per user/per month
  //                         101-150 -150 /per user/per month
  //           *First find the last count for the particular range
  //           from 1 st loop we get 10 as count value
  //           from 2 nd loop we get 100 as count value 
  //           from 3 rd loop we get 150 as count value )
  //        */
  //         console.log("count Value for number of license to add =", newcount)
  //         console.log("Count Value for number of license required =", this.count)
  //         console.log("Newcost per license=", this.package_details[i].new_license_cost)
  //         //product the number of license to add with new cost per license
  //         this.license_amount = this.nooflicense * this.package_details[i].new_license_cost;//multiply the licensecost with the count
  //         console.log("New cost for added new license per month" + this.license_amount)
  //         this.noof_license_to_add = parseInt(this.nooflicense) - this.count;// this is to calculate the remaining license since the range count is less than the no of license subscribed
  //         this.newlicenseamount = this.license_amount;// adding the values in the loop to a variable from second iteration
  //         console.log(this.newlicenseamount);
  //       }
  //       //-----------From above we can calculate the amount per month for the subscribed users------------------------//
  //       //------------Using below function we can calculate the amount per year--------------------------------------//
  //       //---------------The following is to determine whether the current year is leap year or not------------------//
  //       var d = new Date();
  //       var year = d.getFullYear();
  //       var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
  //       //if leap year consider the no of days in as 366
  //       if (leapyear) {
  //         console.log(this.newlicenseamount)
  //         this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
  //         console.log("license per day =", this.newlicenseamount)
  //         // this.newlicenseamount = this.newlicenseamount * 366;//Finding the amount per year
  //       }
  //       else {
  //         //if not leap year consider the no of days in as 365
  //         this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
  //         console.log("license per day =", this.newlicenseamount)
  //         //   this.newlicenseamount = this.newlicenseamount * 365;//Finding the amount per year
  //       }
  //       //------to calculate the amount for the added license for the remaining days   
  //       console.log(this.startdate, "Subscription Start Date")
  //       console.log(this.enddate, "Subscription End date")
  //       console.log(extended_date, "Subscription Extended date")
  //       var splitted = extended_date.split("-", 3);
  //       var year: number = splitted[0];
  //       var day: number = splitted[2];
  //       var month: number = splitted[1];
  //       var data = {
  //         year: year,
  //         month: month,
  //         day: day,
  //       }
  //       var dateFormated = day + "-" + month + "-" + year; //change the date format as dd-mm-yyyy
  //       console.log(dateFormated)
  //       const dt1 = moment(this.startdate, "DD-MM-YYYY h:mma").toDate();// change the start date to date format
  //       const dt2 = moment(this.enddate, "DD-MM-YYYY h:mma").toDate();// change the End date to date format
  //       const dt3 = moment(dateFormated, "DD-MM-YYYY h:mma").toDate();// change the Extended date to date format
  //       console.log("Start date =", dt1)
  //       console.log("End date =", dt2)
  //       console.log("Extended date=", dt3)
  //       //To calculate the total no of days subscribed
  //       //subracting the end date from start date
  //       var total_no_of_days = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
  //       console.log("Total no of days subscribed  =", total_no_of_days)
  //       var today = new Date();//Getting the current date
  //       //To calculate the total no of days completed subscribed
  //       //subracting the start date from today date i.e renew date
  //       var total_no_of_days_completed = Math.floor((Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
  //       console.log("Total days completed =", total_no_of_days_completed)
  //       //To calculate the remaining days in the subscription
  //       //subracting the end date from today date
  //       var total_days_remaining = Math.floor((Date.UTC(dt3.getFullYear(), dt3.getMonth(), dt3.getDate()) -Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate())) / (1000 * 60 * 60 * 24));
  //       console.log("Total days remaining =", total_days_remaining)

  //       //its may need for calculate the total number of days added and extended .Its need some clarification
  //       // var total_numberof_day_to_be_use=(total_no_of_days - total_no_of_days_completed) +total_days_remaining
  //       // console.log("Total number of days going to use=",total_numberof_day_to_be_use)

  //       //calculate the additional amount for the newly subscribed licenses by product the license per cost for the license to add with remaining number of days
  //       console.log(this.newlicenseamount, " this.newlicenseamount")
  //       console.log(total_days_remaining, "total_days_remaining")
  //       var additional_amount_to_be_paid = this.newlicenseamount * total_days_remaining;
  //       //storing the amount to paid to a variable
  //       this.newlicenseamount = additional_amount_to_be_paid;
  //       console.log("Additional amount to be paid  =", this.newlicenseamount)
  //       //set the value to the formgroup 
  //       // this.RenewSubscription.patchValue({
  //       //   "organization_id": this.view_subcrip.organization_id,
  //       //   "number_of_license": this.nooflicense,
  //       //   // "amount": this.newlicenseamount, //if back end need amount need to pass this currently not need so just pass 0
  //       //   "amount": 0,
  //       //   "total_price": this.newlicenseamount,
  //       // })
  //       console.log(this.newlicenseamount,"this.newlicenseamount")      
  //       console.log(this.amountpaid,"this.amountpaid")
  //       var total_amount=(+this.newlicenseamount)+(+this.amountpaid)
  //       console.log(total_amount,"total_amount")
  //       //set the value to the formgroup 
  //       this.RenewSubscription.patchValue({
  //         "organization_id": this.view_subcrip.organization_id,
  //         "number_of_license": this.nooflicense,
  //         // "amount": this.newlicenseamount, //if back end need amount need to pass this currently not need so just pass 0
  //         "amount": this.newlicenseamount,
  //         "total_price": total_amount,
  //       })
  //     }
  //     this.overlayRef.detach();
  //   }, (err) => {
  //     console.log(err);      //prints if it encounters an error
  //   });
  // }
  //-------------------------------------------- * EXTEND DURATION ENDS * ---------------------------//


  //----------------------usable license ----------------------------------------------------------//
  nolicenseused(amount) {
    if (amount > this.no_license) {
      this.toastr.error("Discount amount cannot be greater than license cost");         //toastr message for error
    }
    else {
      this.AddSubscriptionPackageInfo.controls['no_licenseused'];                                   //get from formgroup
    }
  }
  //-------------------------------Modal boxes---------------------------------------------------------//    
  // Function to open the ng-template
  Opento(view) {
    this.message = '';
    this.imageError = '';
    this.isImageSaved = false;
    this.Filename = '';
    this.loadingCal = false;
    this.loadingCalc = false;
    this.loadingCalculate = false;
    this.loadingSubmit = false;
    this.loadingSub = false;
    this.loadingNew = false;
    this.loadingEdit = false;
    this.reset_all_create_subcriptiondata();
    this.spare_image = '';
    this.cardImageBase64 = '';
    this.peryearcost = 0;
    this.activeId = "tab-selectbyid1";
    this.modalService.open(view, {
      size: 'lg',
      windowClass: "center-modalsm",                                     //response
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click

    });

  }
  //Function to open the list of actions to perform on edit------------//
  myFunction(id) {
    console.log(id)
    document.getElementById("myDropdown" + id).classList.toggle("show");
  }
  //------------------------------------------------------------------------------------//
  //Function to store the line item selected to edit
  editpackage(data) {
    this.cardImageBase64 = '';
    console.log(data);
    this.edit_sub = data;
    this.Filename = "Image";
    this.isImageSaved = true;
    this.ByDefault = false;
    this.message = '';
    this.spare_image = environment.image_static_ip+ this.edit_sub.logo;
    //set the value to the formgroup
    this.EditOrgInfo.setValue({
      "organization_id": this.edit_sub.organization_id,
      "organization_name": this.edit_sub.organization_name,
      "logo": '',
      "contact_person_name": this.edit_sub.contact_person_name,
      "contact_number": this.edit_sub.contact_number,
      "alternate_contact_number": this.edit_sub.alternate_contact_number,
      "email": this.edit_sub.email,
      "organization_type": this.edit_sub.organization_type,
    });

    //set the value to the formgroup
    this.EditOrgAddressInfo.setValue({
      "country_id": this.edit_sub.country.country_id,
      "state_id": this.edit_sub.state.state_id,
      "city_id": this.edit_sub.city.city_id,
      "post_code": this.edit_sub.post_code,
      "landmark": this.edit_sub.landmark,
      "street": this.edit_sub.street,
      "plot_number": this.edit_sub.plot_number,
      "status": this.edit_sub.status,
    });
    this.onChangeCountry(this.edit_sub.country.country_id);
    this.onChangeState(this.edit_sub.state.state_id)
    console.log(this.edit_sub.start_date);
    console.log(this.edit_sub.end_date);
    const edt1 = new Date(this.edit_sub.start_date);
    const edt2 = new Date(this.edit_sub.end_date);
    this.RenewSubscription.controls['end_date'].setValue(edt2);
    // var totalnoofdays = Math.floor((Date.UTC(edt2.getFullYear(), edt2.getMonth(), edt2.getDate()) - Date.UTC(edt1.getFullYear(), edt1.getMonth(), edt1.getDate())) / (1000 * 60 * 60 * 24));
    // console.log(totalnoofdays);
    // var today = new Date();
    // console.log(today);
    // const remainingdays = Math.floor((Date.UTC(edt2.getFullYear(), edt2.getMonth(), edt2.getDate()) - Date.UTC(today.getFullYear(), today.getMonth(), today.getDate())) / (1000 * 60 * 60 * 24));;
    // console.log(remainingdays)
    // var d = new Date();
    // var year = d.getFullYear();
    // var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
    // //if leap year consider the no of days in as 366
    // if (leapyear) {
    //   this.numberofdays = +remainingdays + 366;//Finding the amount per year
    // }
    // else //if not leap year consider the no of days in as 365
    // {
    //   this.numberofdays = +remainingdays + 365;//Finding the amount per year
    // }
    // console.log(this.numberofdays);

  }
  //function to format the date
  date_format(date) {
    console.log(date)
    var splitted = date.split("-", 3);
    var year_date = splitted[2].split(",", 1)
    var year: number = +year_date[0];
    var day: number = +splitted[0];
    var month: number = +splitted[1];
    var data = {
      year: year,
      month: month,
      day: day,
    }
    return data;
  }

  //----------------Function to bind the data to the formgroups on edit----------------------------------------------------------------------------------//
  viewpackage(data) {
    this.no_of_used_license = ''           //refresh the  previous data of number of used license
    this.nooflicense = 0; // intializing the nooflicense to 0 to avoid appending to previous values
    this.view_subcrip = data; // storing the complete data to a array for further use
    console.log(this.view_subcrip)
    //-------Storing the required values into a separate variable for using in another functions----------//
    this.croppedImage = environment.image_static_ip + this.view_subcrip.logo;
    // this.discount = this.view_subcrip.discount_type;
    this.no_of_used_license = this.view_subcrip.no_of_used_license
    this.nooflicense = this.view_subcrip.number_of_license;
    this.startdate = this.view_subcrip.start_date.substring(0, 10);
    this.enddate = this.view_subcrip.end_date.substring(0, 10);
    this.date1 = this.date_format(this.startdate)
    this.date2 = this.date_format(this.enddate)
    console.log(this.date1, "date1")
    console.log(this.date2, "date2")
    // this.enddate = this.view_subcrip.end_date;
    // console.log(parseInt(this.view_subcrip.toFixed(4)))
    var set_total_price = this.view_subcrip.total_price
    var set_number_of_license = this.view_subcrip.number_of_license
    //for ngb date format
    var date = this.view_subcrip.end_date
    var splitted = date.split("-", 3);
    var year_date = splitted[2].split(",", 1)
    var year: number = +year_date[0];
    var day: number = +splitted[0];
    var month: number = +splitted[1];
    var set_end_date = {
      year: year,
      month: month,
      day: day,
    }
    this.config.minDate = {
      day: day,
      month: month,
      year: year,
    };
    //config.maxDate = { year: 2099, month: 12, day: 31 };
    this.config.outsideDays = 'hidden';
    console.log(set_end_date, "set_end_date")
    this.RenewSubscription.patchValue({
      "organization_id": this.view_subcrip.organization_id,
      "total_price": parseFloat(this.view_subcrip.total_price).toFixed(2),
      "number_of_license": set_number_of_license,
      "end_date": set_end_date
    })

    console.log(this.RenewSubscription.value)
    this.amountpaid = this.view_subcrip.total_price;
    //---------------------------------------------------------------------------------------------------//
    //--------------------Binding the values into the formgroup------------------------------------------//

    //Remove license formgroup
    this.RemoveLicense.patchValue({
      "organization_id": this.view_subcrip.organization_id,
      "number_of_license_required": this.nooflicense,
      "amount": parseFloat(this.view_subcrip.total_price).toFixed(2),
      "number_of_license_reduced": 0,
      "days_to_extended": 0,
      "number_of_license": this.nooflicense,
      "discount_percentage": 0,
      "discount_amount": 0,
      "total_price": 0,
    })
    console.log(typeof (parseFloat(this.view_subcrip.total_price)))
    console.log(parseFloat(this.view_subcrip.total_price).toFixed(2))
    //Add license formgroup Buy
    this.AddmoreSubscription.patchValue({
      "organization_id": this.view_subcrip.organization_id,
      "number_of_license": 0,
      "number_of_license_required": this.view_subcrip.number_of_license,
      "number_of_license_added": 0,
      "amount": 0,
      "discount_percentage": 0,
      "discount_amount": 0,
      "total_price": parseFloat(this.view_subcrip.total_price).toFixed(2),
    });
    console.log(this.AddmoreSubscription.value)
    // this.get_amount_for_renewal(this.nooflicense); // Function to calculate the amount for renewal based the license that they have already subscribed
    //-----------calling function to automatically populate the country , state------------------------------------//
    // this.onChangeCountry(this.view_subcrip.country.country_id);
    // this.onChangeState(this.view_subcrip.state.state_id);
    //-------------------------------------------------------------------------------------------------------------//
    // this.getpackagetype(this.view_subcrip.package_type.package_type_id);
  }


  //reset all the form groups
  reset_all_create_subcriptiondata() {
    this.AddSubscriptionPackageInfo.reset();
    this.AddSubscriptionAddressInfo.reset();
    this.AddSubscriptionCompanyInfo.reset();
    this.AddSubscriptionPackageInfo.controls['discount_amount'].setValue(0);
    this.AddSubscriptionPackageInfo.controls['discount_percentage'].setValue(0);
    this.AddSubscriptionPackageInfo.controls['status'].setValue(1);
    this.AddSubscriptionPackageInfo.controls['discount_type'].setValue(2);
    this.AddmoreSubscription.reset();
    this.RemoveLicense.reset();
  }
  //get the organization type (if type ==1 its single and if type ==2 its multiple)
  getorganizationtype(orgtype) {
    this.organizationtype = orgtype;
    if ((this.packageplantype != '' && this.packageplantype != undefined) && (this.packagetype != '' && this.packagetype != undefined)) {
      this.calculate_createsub_total_amount();
    }
  }
  //get the package type (type 1 is for free and type 2 is for paid)
  get_package_type(packagetype) {
    this.noOflicenseerror = ""; //error msg for number of license required
    this.package_type_id = packagetype;
    if (this.package_type_id == 1) {
      this.Calculate_button = false;
      //if package type is free 
      var no_of_license = 10;
      this.no_of_license_count = no_of_license
      this.AddSubscriptionPackageInfo.controls['number_of_license'].setValue(no_of_license); //set value for the number of license
      this.AddSubscriptionPackageInfo.controls['total_price'].setValue(0); //set value for the total price
      this.AddSubscriptionPackageInfo.controls['new_license_cost'].setValue(0); //set value for the total price

    }
    else if (this.package_type_id == 2) {
      this.Calculate_button = true;
      this.AddSubscriptionPackageInfo.controls['total_price'].setValue(''); //set value for the total price
    }
  }
  //-------------------------------------------------------------------------------------------------//
  onrenewdiscountChange(discountpercent) {
    console.log(discountpercent)
    this.discountpercent = 0;
    this.discountpercent = discountpercent;
  }
  //------This function is to calculate the discount amount while renewing for the same subscribed license--//
  calculate_discount_amount(percentage) {
    this.discount_remaininglicene = this.nooflicense;//no of license already subscribed from a global variable
    //--------Reintializing the global variables------//
    this.discount_newlicenseamount = 0;
    this.discount_license_amount = 0;
    //-----------------------------------------------//
    var url = 'get_package_management_details/';// api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.discount_package_details = result.response.data;//storing the api response in the array
        //---------To iterate over the array to calculate the amount-----------------------//
        for (var i = 0; i < this.discount_package_details.length; i++) {
          this.discount_count = 0;//reintializing the variable which stores the no of license in the range
          for (var j = this.discount_package_details[i].from_user_range; j <= this.discount_package_details[i].to_user_range; j++) {
            this.discount_count++; //here the count will be incremented till the no of license available in the range
          }
          //--------now checking the no of license is greater than the no of license available in the range----//
          if (this.discount_remaininglicene > this.discount_count) {// if yes
            var licensediscount = (this.discount_package_details[i].new_license_cost * (this.discountpercent / 100));//calculate the amount of discount from discount percentage
            var newlicensecost = this.discount_package_details[i].new_license_cost - licensediscount; // subract the discounted amount from the total amount to calculate the amount after discount
            this.discount_license_amount = this.discount_count * newlicensecost; // now calculate the license cost with the new amount of license
            this.discount_remaininglicene = this.discount_remaininglicene - this.discount_count; // reduce the no of license in the range from the no required
            this.discount_newlicenseamount = this.discount_newlicenseamount + this.discount_license_amount; //add the amount the existing from the second iteration
          }
          else {// if no
            var licensediscount = (this.discount_package_details[i].new_license_cost * (this.discountpercent / 100));//calculate the amount of discount from discount percentage
            var newlicensecost = this.discount_package_details[i].new_license_cost - licensediscount;// subract the discounted amount from the total amount to calculate the amount after discount
            this.discount_license_amount = this.discount_remaininglicene * newlicensecost;// now calculate the license cost with the new amount of license
            this.discount_newlicenseamount = this.discount_newlicenseamount + this.discount_license_amount;//add the amount the existing from the second iteration
          }
        }
        //-----------From above we can calculate the amount per month for the subscribed users------------------------//
        //------------Using below function we can calculate the amount per year--------------------------------------//
        //---------------The following is to determine whether the current year is leap year or not------------------//
        var d = new Date();
        var year = d.getFullYear();
        var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
        //if leap year consider the no of days in as 366
        if (leapyear) {
          this.discount_newlicenseamount = this.discount_newlicenseamount / 30;// Finding the amount per day
          this.discount_newlicenseamount = this.discount_newlicenseamount * 366;//Finding the amount per year
        }
        else//if not leap year consider the no of days in as 365
        {
          this.discount_newlicenseamount = this.discount_newlicenseamount / 30;// Finding the amount per day
          this.discount_newlicenseamount = this.discount_newlicenseamount * 365;//Finding the amount per year
        }
        var discountedamount = this.newlicenseamount - this.discount_newlicenseamount; // to get the discounted amount
        this.text_licensecost = this.discount_newlicenseamount // storing it in global variable
        this.text_discountamount = discountedamount;// storing it in global variable
        //setting the values into the form group--/////////////
        //this.RenewSubscription.controls['total_price'].setValue(this.discount_newlicenseamount.toFixed(2));
        // this.RenewSubscription.controls['discount_amount'].setValue(discountedamount.toFixed(2));
        this.AddmoreSubscription.controls['discount_amount'].setValue(discountedamount.toFixed(2));
        this.AddmoreSubscription.controls['total_price'].setValue(discountedamount.toFixed(2));
        // this.ngOnInit();                                                //reloading the component
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }

  //-----------------------------------------------REMOVE LICENSE STARTS-------------------------------------//
  //get the number of license going to remove
  get_noOf_license_removal(nooflicense_removel) {
    this.RemoveLicense.patchValue({
      "days_to_extended": '',
      "discount_percentage": 0,
      "discount_amount": 0,
    })
    // //store the noof license to the variable
    // if (this.RemoveLicense.value.number_of_license_reduced == '' || this.RemoveLicense.value.number_of_license_reduced == null || this.RemoveLicense.value.number_of_license_reduced == 0) {
    //   // this.number_buttons = false;
    // }
    // else {
    //   // this.number_buttons = true;
    // }
    this.number_of_license_reduced = nooflicense_removel

  }
  //function is to calculate the amount after removing license from the existing license
  calculate_licenseRemoval_total_amount() {
    this.loadingCalc = true;
    this.newlicenseamount = 0;     //reintializing the variable
    console.log("Total number of license supscribed=", this.nooflicense)
    console.log("Total number of license used=", this.no_of_used_license)
    var unused_license = this.nooflicense - this.no_of_used_license
    console.log("Total number of unused license=", unused_license)
    if (this.number_of_license_reduced > unused_license) {
      this.license_removed_error = "you can remove unused licenses .You have only " + unused_license + " unused license."
      // this.number_buttons = false;
    }
    else {
      if (this.number_of_license_reduced == 0 || this.number_of_license_reduced == undefined || this.number_of_license_reduced == '' || this.number_of_license_reduced == null) {
        this.RemoveLicense.patchValue({
          // "number_of_license_reduced": this.number_of_license_reduced,
          // "number_of_license_required": number_of_license,
          "days_to_extended": 0,
          // "discount_percentage": 0,
          // "discount_amount": 0,      
        })
      }
      else {
        this.license_removed_error = ""
        console.log("Number of license reduced=", this.number_of_license_reduced)
        if (this.number_of_license_reduced > this.nooflicense) {
          //error message
          this.license_removed_error = "Number of license to remove must be less than the number of license "
        }
        else {
          this.license_removed_error = ''
          var license = this.nooflicense - this.number_of_license_reduced; // calculate the license available after removal
          console.log("Number of license after removal=", license)
          var number_of_license = license //storing it in variable
          var url = 'get_package_management_details/';// api url for getting the details
          this.ajax.getdata(url).subscribe((result) => {
            if (result.response.response_code == "200") {
              this.loadingCalc = false;
              this.package_details = result.response.data;//storing the api response in the array
              console.log("Api result =", this.package_details)
              //---iterating the variable to calculate the amount for the license-----------------//
              for (var i = 0; i < this.package_details.length; i++) {
                this.count = 0// this variable is to store the no of license in the range
                console.log("From range=", this.package_details[i].from_user_range)
                for (var j = this.package_details[i].from_user_range; j <= this.package_details[i].to_user_range; j++) {
                  this.count++;// here the count will be incremented till the no of license available in the range
                }
                console.log("Count", this.count)
                //checking the no of license is greater than the no of license available in the range--//
                if (license > this.count) {
                  //if yes
                  console.log(license, "license from if")
                  console.log("Cost Per license=", this.package_details[i].new_license_cost)
                  this.license_amount = this.count * this.package_details[i].new_license_cost; // calculate the amount of license
                  license = license - this.count; // reduce the calculated license from the total license
                  this.newlicenseamount = this.newlicenseamount + this.license_amount; //adding the amount to the existing value from second iteration
                  console.log("License per month=", this.newlicenseamount)
                }
                else { //if no
                  console.log("Cost Per license=", this.package_details[i].new_license_cost)
                  this.license_amount = number_of_license * this.package_details[i].new_license_cost;// calculate the amount of license
                  //license = license - this.count; // reduce the calculated license from the total license
                  this.newlicenseamount = this.license_amount;//adding the amount to the existing value 
                  console.log("License per month=", this.newlicenseamount)
                  // break;
                }

              }
              //-----------From above we can calculate the amount per month for the subscribed users------------------------//
              //------------Using below function we can calculate the amount per year--------------------------------------//
              //---------------The following is to determine whether the current year is leap year or not------------------//
              var d = new Date();
              var year = d.getFullYear();
              var leapyear = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0)
              //if leap year consider the no of days in as 366
              if (leapyear) {
                this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
                console.log("License per day=", this.newlicenseamount)
                // this.newlicenseamount = this.newlicenseamount * 366;//Finding the amount per year
                // console.log("License per year=",this.newlicenseamount)
              }
              else //if not leap year consider the no of days in as 365
              {
                this.newlicenseamount = this.newlicenseamount / 30; // Finding the amount per day
                console.log("License per day=", this.newlicenseamount)
                // this.newlicenseamount = this.newlicenseamount * 365;//Finding the amount per year
                // console.log("License per year=",this.newlicenseamount)
              }
              //------to calculate the amount for the added license for the remaining days 
              var start_date = this.date1.day + "-" + this.date1.month + "-" + this.date1.year; //change the date format as dd-mm-yyyy
              var end_date = this.date2.day + "-" + this.date2.month + "-" + this.date2.year; //change the date format as dd-mm-yyyy
              const dt1 = moment(start_date, "DD-MM-YYYY h:mma").toDate();// change the start date to date format
              const dt2 = moment(end_date, "DD-MM-YYYY h:mma").toDate();// change the End date to date format
              // const dt1 = this.date1; // change the start date to date format
              // const dt2 = this.date2;// change the End date to date format
              console.log("Subscription Start date", dt1)
              console.log("Subscription End date", dt2)
              //To calculate the total no of days subscribed
              //subracting the end date from start date
              var total_no_of_days = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
              console.log("Total no of days Subscribed=", total_no_of_days)
              var today = new Date();//Getting the current date
              //To calculate the total no of days completed subscribed
              //subracting the end date from today date i.e renew date
              var today = new Date();
              var total_no_of_days_completed = Math.floor((Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
              console.log("Total number of days completed=", total_no_of_days_completed)
              //To calculate the remaining days in the subscription
              var total_days_remaining = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(today.getFullYear(), today.getMonth(), today.getDate())) / (1000 * 60 * 60 * 24));
              console.log("Total number of days remaining=", total_days_remaining)
              //This to calculate the amount that is charged per day for the subscription
              console.log("Total amount paid=", this.amountpaid)
              var amount_paid_per_day = this.amountpaid / total_no_of_days;
              console.log("Amount paid per day for total number of license subscribed=", amount_paid_per_day)
              //to calculate the amount that has been used from the amount paid till the renewal date
              var used_amount = amount_paid_per_day * total_no_of_days_completed; // to calculate the amount that used till date
              console.log("Used Amount=", used_amount)
              var remaining_amount = this.amountpaid - used_amount; //to calculate the remaining amount
              console.log("Remaining Amount=", remaining_amount)
              console.log("Remaining License=", number_of_license)
              console.log("Per day Cost for balance no of licenses=", this.newlicenseamount)
              var total_added_days = Math.round(remaining_amount / this.newlicenseamount); // to calculate the days to extend divide the remaing amount from the actual license cost
              console.log("Total number of remaining days=", total_added_days)
              //calculate Days to be extended from the subraction of total number of subscription and total number of remaing days(additional days calculated)
              var days_to_be_extended = total_added_days - total_days_remaining
              console.log("Days to be extended =", days_to_be_extended)
              //---Bind the values to the Form group---///
              this.RemoveLicense.patchValue({
                "number_of_license": number_of_license,
                "number_of_license_reduced": this.number_of_license_reduced,
                // "amount": remaining_amount.toFixed(2),
                "days_to_extended": Math.round(days_to_be_extended),
                "discount_percentage": 0,
                "discount_amount": 0,
                "total_price": parseFloat(remaining_amount.toFixed(2)),
              })

            }
          }, (err) => {
            this.loadingCalc = false;
            console.log(err);                                                 //prints if it encounters an error
          });
        }
      }
    }
  }
  //function is to submit the details when the license is removed
  remove_license() {
    this.loadingSub = true;
    var data = this.RemoveLicense.value;// storing the form group value
    var url = 'renew_remove/'                                         //api url of remove license
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.toastr.success(result.response.message, 'Success');    //toastr message for success
        this.showTable = false;
        this.getsubscriptiondata();
        this.chRef.detectChanges();
        this.modalService.dismissAll();                             //to close the modal box
        this.loadingSub = false;
      }

      else if (result.response.response_code == "400") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingSub = false;
      }
      else if (result.response.response_code == "500") {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingSub = false;
      }
      else {
        this.toastr.error(result.response.message, 'Error');        //toastr message for error
        this.loadingSub = false;
      }
    }, (err) => {
      console.log(err);                                             //prints if it encounters an error
    });

  }
  //-----------------------------------------------REMOVE LICENSE ENDS-------------------------------------//
}
