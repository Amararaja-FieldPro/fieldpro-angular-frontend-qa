// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	// api_url: 'https://fieldpro.kaspontech.com/djadmin-qa/',
	  api_url: 'https://fieldpro.kaspontech.com/djadmin/',
	//  api_url: 'http://dev.kaspontech.com/djadmin/',	 // Dev URL
	// api_url: 'http://3.212.33.214:8000/'// QA URL
	//  api_url: 'http://localhost:8000/',// Local
	// api_url: 'http://192.168.1.29:8000/',// Local
	image_static_ip:"https://fieldpro.kaspontech.com/",



	// firebaseConfig1: {
    //     apiKey: 'Api_key',
    //     authDomain: 'domain',
    //     databaseURL: '-------',
    //     projectId: '------',
    //     storageBucket: '',
    //     messagingSenderId: '222589750737',
    //     appId: 'didkdkdkkd'
    // },

	 firebaseConfig :{
		apiKey: "AIzaSyC2BB0419UlZu4bZl3PYMr6aMmOgZL9zWc",
		authDomain: "amararja-web.firebaseapp.com",
		projectId: "amararja-web",
		storageBucket: "amararja-web.appspot.com",
		messagingSenderId: "877398470185",
		appId: "1:877398470185:web:2c7d677203621dff22f638",
		measurementId: "G-VZC0W4GGG2"
	  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thgitrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
