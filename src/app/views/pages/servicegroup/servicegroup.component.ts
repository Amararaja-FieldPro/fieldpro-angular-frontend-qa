import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'; // To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { ToastrService } from 'ngx-toastr';// To use toastr
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //for using form group
import Swal from 'sweetalert2';
//loader
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
// import { DataTableDirective } from 'angular-datatables';
// import { Subject } from 'rxjs';



@Component({
  selector: 'app-root',
  templateUrl: './servicegroup.component.html',
  styleUrls: ['./servicegroup.component.scss']
})
export class ServicegroupComponent implements OnInit {
  service_group: any;                                //get the list of service group for using get API
  service_data: any;                                     //get the datas from edit API
  edit_data: any;                                       //get the datas from edit API
  addServiceGroupForm: FormGroup;            //form group for create service group
  editServiceGroupForm: FormGroup;                    //form group for edit service group
  submitted = false;                        //for use error notation
  showTable: boolean = false;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;
  loadingAdd: boolean = false;          //variable declaration for spinner add
  loadingUpd: boolean = false;          //variable declaration for spinner update
  error: any;
  errors: string;

  constructor(private modalService: NgbModal, public ajax: ajaxservice, private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) {

    this.addServiceGroupForm = this.formBuilder.group({
      work_type: ['', [Validators.required, this.noWhitespace, Validators.maxLength(30)]],
    });

  }

  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }

  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getservicegroup();
  }
  onReset() {

    this.addServiceGroupForm.reset();  //data clearing Sowndarya 05-12-2020
  }

 //function for edit service group
 editServiceGroup(data) {
  this.edit_data = data;
  //form group for edit service group
  this.editServiceGroupForm = new FormGroup({
    "work_type_id": new FormControl(this.edit_data.work_type_id),
    "work_type": new FormControl(this.edit_data.work_type, [Validators.required, this.noWhitespace,Validators.maxLength(30)]),
  });                                         //Stores the data into the array variable



}


  // API for edit after press update
  serviceGroup(service_data) {
    this.error = '';
    this.errors = '';
    this.loadingUpd = true;                             //adding spinner in submit button
    if (this.editServiceGroupForm.invalid)                                                   //Check if the package type is empty
    {
      this.loadingUpd = false;    //dismissing spinner in submit button 
      this.submitted = true;
    }
    else {
      this.loadingUpd = true;                             //adding spinner in submit button 
      var url = 'edit_servicegroup/'                                      //api url of edit api
      this.ajax.postdata(url, this.editServiceGroupForm.value).subscribe((result) => {
        if (result.response.response_code == "200") {                             //if sucess
          this.loadingUpd = false;    //dismissing spinner in submit button 
          this.showTable = false;
          this.chRef.detectChanges();
          this.getservicegroup();                                                     //reloading the component
          this.toastr.success(result.response.message, 'Success');                 //toastr message for success
          this.modalService.dismissAll();                                           //to close the modal box
        }

        else if (result.response.response_code == "400" || result.response.response_code == "500") {
          this.loadingUpd = false;    //dismissing spinner in submit button 
          this.errors = result.response.message
          this.overlayRef.detach();
        }
        else {
          this.loadingUpd = false;    //dismissing spinner in submit button
          this.errors = "Something Went wrong";
          this.overlayRef.detach();
        }
      }, (err) => {
        console.log(err);                                    //prints if it encounters an error
      });
    }
  }
  // get API for Service group
  getservicegroup() {
    this.error = '';
    this.errors = '';
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_servicegroup/';                                   // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400")                               //if sucess
      {
        this.service_group = result.response.data;
        this.showTable = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.errors = result.response.message
        this.overlayRef.detach();
      }
      else {
        this.errors = "Something Went wrong";
        this.overlayRef.detach();
      }
    }, (err) => {
      console.log(err);                                                 //prints if it encounters an error
    });
  }


  // convenience getter for easy access to form fields
  get f() { return this.addServiceGroupForm.controls; }            //error log for create service group
  get fe() { return this.editServiceGroupForm.controls; }               //error log for update service group

  //function for submit add service group
  addservicegroup() {
    this.error = '';
    this.errors = '';
    this.loadingAdd = true                                                      //adding spinner in submit button 

    if (this.addServiceGroupForm.invalid)                                                   //Check if the package type is empty
    {
      this.loadingAdd = false;    //dismissing spinner in submit button
      this.toastr.error("Please enter work type!", 'Error');                //If empty display the toast error message 
    }

    else {
      this.loadingAdd = true;    //dismissing spinner in submit button
      var data = this.addServiceGroupForm.value;                  //Data to be passed for add api

      var url = 'create_servicegroup/'                                         //api url for add
      this.ajax.postdata(url, data).subscribe((result) => {
        if (result.response.response_code == "200") {                      //if sucess

          this.loadingAdd = false;    //dismissing spinner in submit button
          //to close the modal box
          this.showTable = false;
          this.chRef.detectChanges();
          this.getservicegroup();
          this.addServiceGroupForm.reset();                                               //reloading the component
          this.toastr.success(result.response.message, 'Success');    //toastr message for success
          this.modalService.dismissAll();
        }

        else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
          this.loadingAdd = false;    //dismissing spinner in submit button
          this.showTable = true;
          this.errors = result.response.message;                         //toastr message for error
        }
        else {
          this.loadingAdd = false;    //dismissing spinner in submit button
          this.showTable = true;
          this.errors = "Something Went Wrong"        //toastr message for error
        }
      }, (err) => {
        console.log(err);                                             //prints if it encounters an error
      });
    }
  }


  //submit for create service group
  onSubmit() {
    this.submitted = true;

  }

  //function for modal boxes
  openLarge(content6) {
    this.error = '';
    this.errors = '';
    this.addServiceGroupForm.reset();
    this.modalService.open(content6, {
      size: 'lg',                                                      //specifies the size of the modal box
      windowClass: "center-modalsm",                                    // modal popup for resizing the popup
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }

  //function for modal boxes large
  openModal(content6) {
    this.error = '';
    this.errors = '';
    this.modalService.open(content6, {
      size: 'lg',                                                //specifies the size of the modal box
      windowClass: "center-modalchecklist"                                    // modal popup for resizing the popup
    });
  }

  //function for Delete APUI for Servicegroup
  trashservicegroup(data) {
    this.error = '';
    this.errors = '';
    this.edit_data = data;                                   //get datas from edit API
    var worktypeid = this.edit_data.work_type_id;
    var worktype = +worktypeid;
    var url = 'delete_servicegroup/?';             // api url for getting the details with using post params
    var id = "work_type_id";                            //id for specific the servicegroup name
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      allowOutsideClick: false,                          //sweet alert will not close on oustside click
    }).then((result) => {
      if (result.value) {
        this.ajax.getdataparam(url, id, worktype).subscribe((result) => {
          if (result.response.response_code == "200") {                 //if sucess

            this.showTable = false;
            this.chRef.detectChanges();
            this.getservicegroup();                    //reloading the component
            this.toastr.success(result.response.message, 'Success');        //toastr message for success

          }
          else if (result.response.response_code == "400" || result.response.response_code == "500") {                         //if failure
            this.toastr.error(result.response.message, 'Error');        //toastr message for error
          }
          else {
            this.toastr.error("Something Went Wrong", 'Error');        //toastr message for error
          }
        }, (err) => {
          console.log(err);                                        //prints if it encounters an error
        });
      }
    })
  }


  //--------------------------------------------------------------------------------------------------//
}
