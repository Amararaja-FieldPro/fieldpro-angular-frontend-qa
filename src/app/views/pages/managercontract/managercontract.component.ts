import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
// import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
// import { DataTableDirective } from 'angular-datatables';
// import { Subject } from 'rxjs';
@Component({
	selector: 'kt-managercontract',
	templateUrl: './managercontract.component.html',
	styleUrls: ['./managercontract.component.scss']
})
export class ManagercontractComponent implements OnInit {

	employee_code: any;
	technician_data: any;
	techId: any;
	cusId: any;
	contract_data: any;
	customer_data: any;
	ticket_data: any;
	showTable: boolean = false;
	overlayRef: OverlayRef;
	LoaderComponentPortal: ComponentPortal<LoaderComponent>;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private overlay: Overlay, private chRef: ChangeDetectorRef) { }

	ngOnInit() {
		this.overlayRef = this.overlay.create({
			positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
			hasBackdrop: true,
		});
		this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
		this.showTable = false;
		this.getNewContract();

	}
	getNewContract() {
		this.overlayRef.attach(this.LoaderComponentPortal);
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var employee_code = "employee_code";
		var url = 'new_contracts/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.contract_data = result.response.data;              //storing the api response in the array    
				this.showTable = true;
				this.chRef.detectChanges();
				this.overlayRef.detach();           //detach the loader
			}
			else if (result.response.response_code = "500") {                                                         //if not Success
				this.toastr.error(result.response.message, 'Error');        //toastr message for error
				this.overlayRef.detach();                 //detach the loader
			}
			else {                                                         //if not Success
				this.toastr.error("Some thing went wrong");        //toastr message for error
				this.overlayRef.detach();                    //detach the loader
			}
		}, (err) => {
			this.overlayRef.detach();
			console.log(err);                                        //prints if it encounters an error
		});
	}

	contractApprove(contract_id) {
		Swal.fire({ //sweet alert for accptance
			title: 'Are you sure?',
			text: "You want to accept this Contract !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Accept',
			allowOutsideClick: false,                          //sweet alert will not close on oustside click
		}).then((result) => {
			if (result.value) {
				var url = 'new_contract_approve/'; //API url for spare transfer approve
				var data = { "contract_id": contract_id }//params for API
				this.ajax.postdata(url, data).subscribe((result) => {
					if (result.response.response_code == "200") {
						this.showTable = false;
						this.chRef.detectChanges();
						this.getNewContract();
						this.toastr.success(result.response.message, 'Success');        //toastr message for success
					}
					else if (result.response.response_code == "400") {                //if failure
						this.toastr.error(result.response.message, 'Error');            //toastr message for error
					}
					else {                                                         //if not sucess
						this.toastr.error(result.response.message, 'Error');        //toastr message for error
					}
				}, (err) => {
					console.log(err);                                        //prints if it encounters an error
				});
			}
		})
	}

	fetchNews(evt: any) {
		console.log(evt); // has nextId that you can check to invoke the desired function
	}
	customerDetails(cusId, customer_details) {
		this.cusId = cusId;
		var customer_code = "customer_code";
		var url = 'get_customer_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, customer_code, cusId).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.customer_data = result.response.data; //bind results in this variable
				this.modalService.open(customer_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
			else if(result.response.response_code == "400" || result.response.response_code == "500"){
				this.toastr.error(result.response.message, "Error");
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}

	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',
					windowClass: "center-modalsm",                                  //specifies the size of the dialog box
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				});
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	openLarge(add, row) { //Modal popup
		this.employee_code = row
		this.modalService.open(add, {
			size: 'lg',
			windowClass: "center-modalsm"                                  //specifies the size of the dialog box
		});
	}

}
