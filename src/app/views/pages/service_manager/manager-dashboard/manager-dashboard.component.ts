import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ChartComponent, IAccLoadedEventArgs, ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
import { FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { ajaxservice } from '../../../../ajaxservice';// Common API service for both get and post
// import Swal from 'sweetalert2';
// import { Location } from '@angular/common';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { IPointRenderEventArgs } from '@syncfusion/ej2-angular-charts';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'kt-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss'],
})
export class ManagerDashboardComponent implements OnInit {
  @ViewChild('asideMenu', { static: true }) asideMenu: ElementRef;

  public piedataticket: Object[];
  public piedata: Object[];
  public datalabel: Object;
  public datalabelticket: Object;
  public csatdatalabel: Object;
  public legendSettings: Object;
  public enableBorderOnMouseMove: boolean;
  public tooltip: Object;
  @ViewChild(('chart'), { static: false })
  public chart: ChartComponent;
  @ViewChild(('chart1'), { static: false })
  public chart1: ChartComponent;
  @ViewChild(('chart2'), { static: false })
  public chart2: ChartComponent;
  @ViewChild(('chart3'), { static: false })
  public chart3: ChartComponent;
  @ViewChild(('chart4'), { static: false })
  public chart4: ChartComponent;
  @ViewChild(('chart5'), { static: false })
  public chart5: ChartComponent;
  @ViewChild(('chart6'), { static: false })
  public chart6: ChartComponent;
  @ViewChild(('chart7'), { static: false })
  public chart7: ChartComponent;
  public animation: Object;

  all_ticket_data: any;
  ticket_distribution: any;

  fresh: any;
  carryforward: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  title: string;
  public chartData: Object[];
  public primaryXAxis: Object;
  public primaryYAxis: Object;
  public primaryXAxis1: Object;
  public primaryYAxis1: Object;
  public primaryYAxis5: Object;
  public primaryXAxis5: Object;
  public primaryXAxis6: Object;
  public primaryYAxis6: Object;
  public primaryXAxis7: Object;
  public primaryYAxis7: Object;

  ticketdis_title: string;
  palette: string[];
  public marker: Object;
  public marker1: Object;
  csat_data: { x: string; y: any; }[];
  DatewiseData: Object[];
  DatewiseData1: Object[];
  LinechartComprehensiveData: Object[];
  LinechartLabourData: Object[];
  LinechartDemandData: Object[];
  LinechartPartData: Object[];
  LinechartWarrantyData: Object[];
  LinechartTotalData: Object[];
  c_sat_title: string;
  ticket_insights_title: string;
  spare_insights_title: string;
  SpareInsightsAmcData: Object[];
  SpareInsightsWarrantyData: Object[];
  RevenueInsightsNewAmcData: Object[];
  RevenueInsightsPercallData: Object[];
  RevenueInsightsContractRenData: Object[];
  RevenueInsightsSparesaleData: Object[];
  revenue_insights_title: string;
  public loaded;
  datevalue: any;
  product_id: any;
  product: any;
  CSatGroup: FormGroup;
  csat_data1: { x: { x: string; y: any; }; y: number; }[];
  tickets: any = [];
  ticketinsight: any;
  empty: Object;
  asideMenus: any;
  current_url: any;
  exist: any;

  constructor(public ajax: ajaxservice, private chRef: ChangeDetectorRef, private router: Router) {
    this.CSatGroup = new FormGroup({
      "product_id": new FormControl('', Validators.required),
      "datetime_value": new FormControl('Today', Validators.required)
    })
  }
  public pointRender(args: IPointRenderEventArgs): void {
    let seriesColor: string[] = ['#00bdae', '#404041', '#357cd2', '#e56590', '#f8b883',
      '#70ad47', '#dd8abd', '#7f84e8', '#7bb4eb', '#ea7a57'];
    args.fill = seriesColor[args.point.index];
  };
  public emptyPointSettings: Object = {
    fill: '#e6e6e6',
  };

  // custom code start
  public load(args: ILoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.chart.theme = <ChartTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
    if (selectedTheme === 'highcontrast') {
      args.chart.series[0].marker.dataLabel.font.color = '#000000';
      args.chart.series[1].marker.dataLabel.font.color = '#000000';
      args.chart.series[2].marker.dataLabel.font.color = '#000000';
    }
  };
  // custom code end
  public chartArea: Object = {
    border: {
      width: 0
    }
  };

  ngOnInit(): void {
    this.asideMenus = localStorage.getItem('asideMenus');
    console.log(JSON.parse(this.asideMenus));
    console.log(this.router.url);
    this.current_url = this.router.url;
    this.exist = this.asideMenus.includes(this.current_url);
    console.log(this.exist);
    if (this.exist == false) {
      this.router.navigate(["/login"]);
    } else {
      this.datevalue = "This Week";
      this.product_id = null;
      this.getproduct();
      this.get_current_ticketes();
      this.get_ticket_distribution();
      this.get_c_sat();
      this.get_ticket_insight();
      // this.get_spare_insight();
      // this.get_revenue_insight();
      this.animation = { enable: true };
      this.enableBorderOnMouseMove = false;
      this.datalabel = { visible: true, name: 'text', position: 'Outside' };
      this.csatdatalabel = { visible: true, name: 'text', position: 'Outside' };
      this.datalabelticket = { visible: true, name: 'text', position: 'Outside' };
      this.legendSettings = {
        visible: false
      };
      this.tooltip = {
        enable: true
      }
    
      this.empty = {
        mode: 'Zero', fill: 'pink', border: { width: 2, color: 'black' }
      }
      
      // this.datalabel = {
      //   enable: true
      // };
      this.marker = {
        dataLabel: {
          visible: true
        }, visible: true, width: 10, height: 10
      };
      this.palette = ["#E94649", "#F6B53F", "#6FAAB0", "#C4C24A"];
    }
    this.marker1 = {
      dataLabel: {
        visible: true,
        position: 'Top',
        font: {
          fontWeight: '600',
          color: '#ffffff',
        },
      },
    };
  }
  getproduct() {
    var url = 'get_product_details/'; // api url for getting the details
    this.ajax.getdata(url).subscribe((result) => {
      if (result.response.response_code == "200") { //if sucess
        this.product = result.response.data; //storing the api response in the array
        console.log(this.product)
      }
    }, (err) => { //if error
      console.log(err); //prints if it encounters an error
    });
  }
  //print the chart
  printTicket() { //print tickets
    console.log(this.chart)
    this.chart.print();
  }
  printTricketSpare() { //print ticket distribution
    console.log(this.chart1)
    this.chart1.print();
  }
  printCsat() { //print csat
    // alert("print2")
    console.log(this.chart2)
    this.chart2.print();
  }
  printInsight() { //print Insight
    console.log(this.chart3)
    this.chart3.print();
  }
  printInsightAmc() { //print insight Amc 
    console.log(this.chart4)
    this.chart4.print();
  }
  printRevenueInsight() { //print Revenue insight
    console.log(this.chart5)
    this.chart5.print();
  }
  exportTicketPNG() { //Export ticket PNG format
    this.chart.exportModule.export('PNG', 'Current Fresh Ticket');
    this.chart6.exportModule.export('PNG', 'Current Carry Forward Ticket');
  }
  exportTicketPDF() {//Export ticket PDF format
    this.chart.exportModule.export('PDF', 'Current Ticket');
    this.chart6.exportModule.export('PDF', 'Current Carry Forward Ticket');
  }
  exportTicketSparePNG() { //Export Ticket Spare PNG format
    this.chart1.exportModule.export('PNG', 'Ticket Distribution');
  }
  exportTicketSparePDF() { //Export tiket spare PDF format
    this.chart1.exportModule.export('PDF', 'Ticket Distribution');
  }
  exportCsatPNG() {//Export CSAT PNG format
    this.chart2.exportModule.export('PDF', 'C-Sat');
    this.chart7.exportModule.export('PDF', 'C-Sat Actual and Target Value');
  }
  exportCsatPDF() { //Export CSAT PDF  format
    this.chart2.exportModule.export('PDF', 'C-Sat');
    this.chart7.exportModule.export('PDF', 'C-Sat Actual and Target Value');
  }
  exportInsightPNG() {//Export Insignt PNG format
    this.chart3.exportModule.export('PNG', 'Ticket Insights');
  }
  exportInsightPDF() { //Export Insignt PDF format
    this.chart3.exportModule.export('PDF', 'Ticket Insights');
  }
  exportInsightAmcPNG() { //Export Insight Amc PNG format
    this.chart4.exportModule.export('PNG', 'Spare Insights');
  }
  exportInsightAmcPDF() { // Export Insight AMC PDF Format
    this.chart4.exportModule.export('PDF', 'Spare Insights');
  }
  exportRevenueInsightPNG() { //Export Revenue Insight PNG format
    this.chart5.exportModule.export('PNG', 'Revenue Insights');
  }
  exportRevenueInsightPDF() {//Export Revenue Insight PDF format
    this.chart5.exportModule.export('PDF', 'Revenue Insights');
  }

  get_current_ticketes() {
    var url = 'current_ticket?';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var employee_code = "employee_code";// Define post format variable
    this.ajax.getdataparam(url, employee_code, emp_code).subscribe((result) => {//Initiate an API call to get the current tickets details 
      if (result.response.response_code == "200") {
        this.all_ticket_data = result.response.data;//Json resonse from API
        console.log(this.all_ticket_data, "this.all_ticket_data")
        this.fresh = "Fresh:" + this.all_ticket_data['fresh'];//get the count for fresh tickets
        this.carryforward = "Carry Forward:" + this.all_ticket_data['carry_forward'];//count of carry forwarded tickets
        this.piedataticket = [
          { 'x': 'Assigined', y: this.all_ticket_data['assigned'] }, { 'x': 'Unassigned', y: this.all_ticket_data['unassigned'] },
          { 'x': 'Accepted', y: this.all_ticket_data['accepted'] }, { 'x': 'Deffered Support', y: this.all_ticket_data['deffered'] },
        ];//bind ticket data to pie chart 
        this.piedata = [
          { 'x': 'Esclated', y: this.all_ticket_data['eslated'] }, { 'x': 'Spare requested', y: this.all_ticket_data['spare_requested'] },
          { 'x': 'Inprogress', y: this.all_ticket_data['in_progress'] },
        ];//bind data to pie chart with esclates ,spare requested and Inprgress datas
        this.title = "CURRENT TICKETS";//define title to the pie chart
        this.chRef.detectChanges();//to detect the changes
        this.dtTrigger.next();

      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  onchangeDay(value, identify) {//on change day or year month
    this.palette = ["#E94649", "#F6B53F", "#6FAAB0", "#C4C24A"];
    this.datevalue = value
    if (identify == 1) {//if 1 call ticket distribution
      this.get_ticket_distribution();
    }
    else if (identify == 2) {// else 
      // alert("c sat")
      console.log(this.CSatGroup.value)
      this.datevalue = this.CSatGroup.value.datetime_value;//get date time 
      this.product_id = this.CSatGroup.value.product_id; //get product id
      if (this.datevalue == "") { //if the date time is null 
        this.datevalue = null;
      }
      else {
        this.datevalue = this.CSatGroup.value.datetime_value;
      }
      if (this.product_id == "") {//if product id is null
        this.product_id = null;
      }
      else {
        this.product_id = this.CSatGroup.value.product_id;
      }

      this.get_c_sat();// API call Function to CSAT
    }
    else if (identify == 3) {
      this.get_ticket_insight();// Function call to initiate ticket insight
    }
    else if (identify == 4) {
      this.get_ticket_insight();// Function call to initiate ticket insight
    }
    else if (identify == 5) {
      this.get_spare_insight();// Function call to initiate Spare insight
    }
    else if (identify == 6) {
      this.get_revenue_insight(); // Function call to initiate Revenue insight
    }
  }
  onChangeProduct(value, identify) {
    this.product_id = value
    this.get_c_sat();

  }
  get_ticket_distribution() {//function to get ticket distribution details
    this.ticketdis_title = "TICKET DISTRIBUTION";
    var url = 'ticket_distribution/';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code, "value": this.datevalue }//Json post data format
    this.ajax.postdata(url, data).subscribe((result) => {//API call to initiate to get the ticket distribution details
      if (result.response.response_code == "200") {
        this.ticket_distribution = result.response.data;//Json respose from API
        if (this.datevalue == "Today") {
          this.primaryXAxis = {
            valueType: 'Category',
            title: 'Time'
          };
        }
        else if (this.datevalue == "This Week") {
          this.primaryXAxis = {
            valueType: 'Category',
            title: 'Days'
          };
        }
        else if (this.datevalue == "This Month") {
          this.primaryXAxis = {
            valueType: 'Category',
            title: 'Days'
          };
        } else {
          this.primaryXAxis = {
            valueType: 'Category',
            title: 'Months'
          };

        }
        console.log(this.ticket_distribution)

        this.primaryYAxis = {

          // interval: 20,
          title: ''
        };
        this.chRef.detectChanges();//Detect changes
        this.dtTrigger.next();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_c_sat() {//Function to get CSAT 

    this.c_sat_title = "C-SAT INSIGHTS(IN PERCENTAGE)";//Define title for Chart
    var url = 'c_sat/';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code, "value": this.datevalue, "product_id": this.product_id }
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.csat_data = result.response.data1;//Get resposne data from API
        this.csat_data1 = result.response.data;
        console.log(this.csat_data, "csat_data")
        this.primaryXAxis1 = {
          valueType: 'Category',
          title: '',
          labelIntersectAction: 'Rotate45'
        };
        this.primaryYAxis1 = {
          // minimum: 0, maximum: 150,
          // interval: 50, 
          title: 'Percentage',
        };
        this.chRef.detectChanges();//Detect chnages
        this.dtTrigger.next();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_ticket_insight() {
    this.ticket_insights_title = 'TICKET INSIGHTS';//Define Insight title
    var url = 'ticket_insight/';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code, "value": this.datevalue }
    this.ajax.postdata(url, data).subscribe((result) => {//Initiate API Call
      if (result.response.response_code == "200") {//if response code 200 i.e Success
        this.LinechartComprehensiveData = result.response.data;//Get respose data
        this.primaryXAxis5 = {
          valueType: 'Category',
          title: 'Product',
          labelIntersectAction: 'Rotate45'
        };
        //Initializing Primary Y Axis
        this.primaryYAxis5 = {
          labelFormat: '{value}',
          title: 'No.of.tickets',
          // edgeLabelPlacement: 'Shift',
          // interval: 100,  
          // labelStyle: {
          //   color: 'transparent',
          // },
        };

        this.chRef.detectChanges();//Detect Changes
        this.dtTrigger.next();
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  get_spare_insight() {    //Function Call to Spare Insight
    this.ticket_insights_title = 'TICKET INSIGHTS';//Define Spare insight title
    var url = 'spare_insight/';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code, "value": this.datevalue }//Post data  format
    this.ajax.postdata(url, data).subscribe((result) => {//Initiate API Call
      if (result.response.response_code == "200") {//if resposne code is 200 i.e success

        this.LinechartComprehensiveData = result.response.data;
        this.SpareInsightsAmcData = [
          { x: 'Refrigirator', y: 6 }, { x: 'AirConditionar', y: 4 }, { x: 'Washingmachine', y: 5 }, { x: 'Waterpurifier', y: 9 }
        ];// Define Insight AMC data
        this.SpareInsightsWarrantyData = [
          { x: 'Refrigirator', y: 6 }, { x: 'AirConditionar', y: 0 }, { x: 'Washingmachine', y: 5 }, { x: 'Waterpurifier', y: 10 }
        ];// Declare Insight Waranty data
        this.spare_insights_title = 'SPARE INSIGHTS';//Define  Spare Insight title

        this.primaryYAxis6 = {
          minimum: 0, maximum: 10,
          interval: 2, title: 'No.of.Spares'

        };
        this.primaryXAxis6 = {
          valueType: 'Category',
          labelIntersectAction: 'Rotate45'
        }
        this.chRef.detectChanges();
        this.dtTrigger.next();
      }
    }), (err) => {
      console.log(err);                                        //prints if it encounters an error
    }
  }

  get_revenue_insight() {    //Fucntion to get Revenue insights
    this.revenue_insights_title = 'REVENUE INSIGHTS';
    var url = 'revenue_insight/';             // api url for getting the details with using post params
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = { "employee_code": emp_code, "value": this.datevalue }
    this.ajax.postdata(url, data).subscribe((result) => {//API call to get data
      if (result.response.response_code == "200") { // Get Respose data 
        this.RevenueInsightsNewAmcData = result.response.data;
        console.log(this.RevenueInsightsNewAmcData, "this.RevenueInsightsNewAmcData")

        this.primaryYAxis7 = {

          valueType: 'Category',

        };
        this.primaryXAxis7 = {
          // minimum: 0, maximum: 2500,
          // interval: 500, 
          title: '(Revenue in crore)'
        }

      }
    }), (err) => {
      console.log(err);                                        //prints if it encounters an error
    }
  }
}
