import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomermanagemantComponent } from './customermanagemant.component';

describe('CustomermanagemantComponent', () => {
  let component: CustomermanagemantComponent;
  let fixture: ComponentFixture<CustomermanagemantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomermanagemantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomermanagemantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
