import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlamappingComponent } from './slamapping.component';

describe('SlamappingComponent', () => {
  let component: SlamappingComponent;
  let fixture: ComponentFixture<SlamappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlamappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlamappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
