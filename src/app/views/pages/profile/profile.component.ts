import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../ajaxservice';// Common API service for both get and post
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';// To use toastr
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../loader/loader.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'kt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  LoaderComponentPortal: ComponentPortal<LoaderComponent>; //loader component
  overlayRef: OverlayRef;

  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtInstance: Promise<DataTables.Api>;
  AddUser: FormGroup;
  changepassword: FormGroup;
  email: any;
  role: any;
  fieldTextType: boolean;
  fieldNewPassword: boolean;
  fieldOldPassword: boolean;
  name: any;
  passwordMismatch: string;
  PassMessage: string;
  loading: boolean = false;   //button spinner by Sowndarya
  loadingSave: boolean = false;
  rode_name: any;
  web_error: string;
  profile: string;
  country: any;
  state: any;
  city: any[];
  city_view: string;
  state_view: string;
  town: any;
  town_view: string;
  country_view: string;
  servicedesk_view: string;
  servicedesk_type: string;
  segment_view: string;
  segment:any[];
  constructor(private ajax: ajaxservice, private toastr: ToastrService, private modalService: NgbModal, private overlay: Overlay, private chRef: ChangeDetectorRef, fb: FormBuilder) {
    this.AddUser = new FormGroup({
      "employee_id": new FormControl('', [Validators.required]),
      "email_id": new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      "full_name": new FormControl('', [Validators.required,this.noWhitespace]),
      "contact_number": new FormControl(0, [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      "alternate_contact_number": new FormControl(0, [Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]*$")]),
      // "country": new FormControl(''),
      // "state": new FormControl(''),
      // "city": new FormControl(''),
      // "location": new FormControl(''),
      // "role_name": new FormControl('', [Validators.required]),
      // "role_id": new FormControl(0, [Validators.required]),
      // 'employee_id': new FormControl('', [Validators.required]),
   
    });
    this.changepassword = new FormGroup({
      "email": new FormControl(''),
      "role_name": new FormControl(''),
      "old_password": new FormControl('', [Validators.required,Validators.minLength(6),this.noWhitespace]),
      "new_password": new FormControl('', [Validators.required, Validators.minLength(6),this.noWhitespace]),
      "confirm_password": new FormControl('', [Validators.required, Validators.minLength(6),this.noWhitespace]),

    },
      
    );



  }
  public noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  employee_code: string
  user: any
  users: any;
  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    this.getuser();

  }

  onReset() {

    this.changepassword.reset();
  }

  get log() {
    return this.AddUser.controls;                       //error logs for create user
  }
  get change() {
    return this.changepassword.controls;                       //error logs for create user
  }
  getuser() {
    this.overlayRef.attach(this.LoaderComponentPortal);
    this.employee_code = localStorage.getItem('employee_code');
    var employee_code = this.employee_code; // country: number                                    
    var url = 'get_userdetails/?';             // api url for getting the details with using post params
    var id = "employee_code";
    // console.log(url, id, employee_code);
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.user = result.response.data;
        this.user.forEach(element => {
          this.AddUser.controls['employee_id'].setValue(this.employee_code);
          this.AddUser.controls['email_id'].setValue(element.email_id);
          this.AddUser.controls['full_name'].setValue(element.full_name);
          this.AddUser.controls['contact_number'].setValue(element.contact_number);
          this.AddUser.controls['alternate_contact_number'].setValue(element.alternate_contact_number);
          this.rode_name=element.role.role_name;
          this.country=element.assigned_country.assigned_country_name;
          // this.state=element.assigned_state;
          // alert(element.role.role_id)
        if(element.role.role_id==4 ) {
          this.country_view="true";
          this.servicedesk_view="true";
          if(element.servicedesk_type==1){
            this.servicedesk_type="ARBL service desk"
          }
          else {
            this.servicedesk_type="Partner service desk"
          }
          this.segment = [];
          if(element.allocated_segment!=''||element.allocated_segment!=null){
            element.allocated_segment.forEach((seg) => {
              var issegment = element.allocated_segment.some(function (el) {
                return el === seg.segment_name
              });
              if (issegment) {
              } else {
                this.segment.push(seg.segment_name);
              }
            }) 
            this.segment_view="true";
          }
          else{
            this.segment_view="false";
          }
          this.state = [];
          if(element.assigned_state!=''||element.assigned_state!=null){
            element.assigned_state.forEach((st) => {
              var isstate = element.assigned_state.some(function (el) {
                return el === st.state_name
              });
              if (isstate) {
              } else {
                this.state.push(st.state_name);
              }
            }) 
            this.state_view="true";
          }
          else{
            this.state_view="false";
          }
          this.city = [];
          if(element.assigned_city==null){
             
            this.city_view="false";
          }
          else{
            element.assigned_city.forEach((ci) => {
              var iscity = element.assigned_city.some(function (el) {
                return el === ci.city_name
              });
              if (iscity) {
              } else {
                this.city.push(ci.city_name);
              }
            })
            this.city_view="true"; 
          }
          this.town = [];
          if(element.assigned_location==null){
             
            this.town_view="false";
          }
          else{
            element.assigned_location.forEach((loc) => {
              var isloc = element.assigned_location.some(function (el) {
                return el === loc.location_name
              });
              if (isloc) {
              } else {
                this.town.push(loc.location_name);
              }
            })
            this.town_view="true"; 
          }
        }
        else if (element.role.role_id==5) {
          this.servicedesk_view="false";
          this.country_view="true";
          this.state = [];
          if(element.assigned_state!=''||element.assigned_state!=null){
            element.assigned_state.forEach((st) => {
              var isstate = element.assigned_state.some(function (el) {
                return el === st.state_name
              });
              if (isstate) {
              } else {
                this.state.push(st.state_name);
              }
            }) 
            this.state_view="true";
          }
          else{
            this.state_view="false";
          }
          this.city = [];
          if(element.assigned_city==null){
             
            this.city_view="false";
          }
          else{
            element.assigned_city.forEach((ci) => {
              var iscity = element.assigned_city.some(function (el) {
                return el === ci.city_name
              });
              if (iscity) {
              } else {
                this.city.push(ci.city_name);
              }
            })
            this.city_view="true"; 
          }
          this.town = [];
          if(element.assigned_location==null){
             
            this.town_view="false";
          }
          else{
            element.assigned_location.forEach((loc) => {
              var isloc = element.assigned_location.some(function (el) {
                return el === loc.location_name
              });
              if (isloc) {
              } else {
                this.town.push(loc.location_name);
              }
            })
            this.town_view="true"; 
          }
        }
        else{
          this.country_view="false";
          this.state_view="false"; 
          this.city_view="false";
          this.town_view="false";
        }
          // this.AddUser.controls['role_name'].setValue(element.role.role_name);
          // this.AddUser.controls['role_id'].setValue(element.role.role_id);
          // this.AddUser.controls['employee_id'].setValue(element.employee_id);
          // this.AddUser.controls['country_id'].setValue(element.country.country_id);
          // this.AddUser.controls['state_id'].setValue(element.state.state_id);
          // this.AddUser.controls['city_id'].setValue(element.city.city_id);
          // this.AddUser.controls['post_code'].setValue(element.post_code);
          // this.AddUser.controls['street'].setValue(element.street);
          // this.AddUser.controls['plot_number'].setValue(element.plot_number);
          // this.AddUser.controls['location_label'].setValue(element.location_label);
          // this.AddUser.controls['node_id'].setValue(element.node_id);
          // this.AddUser.controls['location_id'].setValue(element.location.location_id);
          // this.changepassword.controls['email'].setValue(element.email_id);
          // this.changepassword.controls['role_name'].setValue(element.role.role_name);
        


        })
        // this.user.forEach(element => {
        //   this.changepassword.controls['email'].setValue(element.email_id);
        //   this.changepassword.controls['role_name'].setValue(element.role.role_name);

        // });
        this.changepassword.controls['email'].setValue(this.email);
        this.name = this.user[0].full_name;
        this.email = this.user[0].email_id;
        this.role = this.user[0].role.role_id;
        this.chRef.detectChanges();
        // this.dtTrigger.next();
        this.overlayRef.detach();
        // console.log(this.user[0].email_id);
        // this.email = this.user[0].email_id;
        // this.role = this.user[0].role.role_id;
        //  console.log(this.changepassword.controls['email'].setValue(this.user[0].email_id))
      }
      else if(result.response.response_code == "500"){
        this.overlayRef.detach();
      }
    })
  }
  getpass() {
    this.employee_code = localStorage.getItem('employee_code');
    var employee_code = this.employee_code; // country: number                                    
    var url = 'get_userdetails/?';             // api url for getting the details with using post params
    var id = "employee_code";
    // console.log(url, id, employee_code);
    this.ajax.getdataparam(url, id, employee_code).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.users = result.response.data;
        

        // this.users.forEach(element => {
        //   this.changepassword.controls['email'].setValue(element.email_id);
        //   this.changepassword.controls['role_name'].setValue(element.role.role_name);

        // });
        this.chRef.detectChanges();
      

        // console.log(this.users[0].email_id);
        // this.email = this.users[0].email_id;
        // this.role = this.users[0].role.role_id;
        // this.changepassword.controls['email'].setValue(this.email);
        //  console.log(this.changepassword.controls['email'].setValue(this.user[0].email_id))
      }
    })
  }
  // modal box for change password
  openModal(changepasswords) {
    this.PassMessage = "";
    // this.overlayRef.detach();
    this.modalService.open(changepasswords, {
      size: 'lg',
      windowClass: "center-modalsm",                          //specifies the size of the dialog box
      backdrop: 'static',                                    // modal will not close by outside click
      keyboard: false,                                       // modal will not close by keyboard click
    });
  }
  toggleFieldTextType() {                                       //Toggle show / hide func
    this.fieldTextType = !this.fieldTextType;
    // console.log(this.fieldTextType);
    // console.log("Hi")
  }
  toggleFieldNewPassword() {
    this.fieldNewPassword = !this.fieldNewPassword;
  }
  toggleFieldOldPassword() {
    this.fieldOldPassword = !this.fieldOldPassword;
  }
  old_validation(){
    // console.log(this.changepassword.value.old_password);
    if(this.changepassword.value.old_password){

    }
  }
  passwordValidation() {
    // console.log(this.changepassword.value);
    // console.log(this.changepassword.value.new_password);
    // console.log(this.changepassword.value.confirm_password);
    if (this.changepassword.value.new_password !== this.changepassword.value.confirm_password) {
      this.passwordMismatch = "true";
      // this.changepassword.controls['new_password'].setErrors({'incorrect': true});
      // this.changepassword.controls['confirm_password'].setErrors({'incorrect': true});
      this.PassMessage = "Passsword and ConfirmPassword didn't match.";
      console.log("Mis match");
      this.changepassword.value.invalid = true;
      this.changepassword.controls['confirm_password'].setErrors({ 'incorrect': true });

      // this.passwordmissmatch=""
    } else {
      // this.changepassword.controls['new_password'].setErrors({'incorrect': false});
      // this.changepassword.controls['confirm_password'].setErrors({'incorrect': false});
      this.passwordMismatch = "false";
      this.PassMessage = "";
      // console.log("matching");
    }
    // console.log(this.passwordMismatch);
  }
  change_password() {
    this.loading = true;
    if (this.changepassword.value.new_password !== this.changepassword.value.confirm_password) {
      this.passwordMismatch = "true";
      // this.changepassword.controls['new_password'].setErrors({'incorrect': true});
      // this.changepassword.controls['confirm_password'].setErrors({'incorrect': true});
      this.PassMessage = "Passsword and ConfirmPassword didn't match.";
      // console.log("Mis match");
      // this.passwordmissmatch=""
    } else {
      this.passwordMismatch = "false";
      // this.changepassword.controls['new_password'].setErrors({'incorrect': false});
      // this.changepassword.controls['confirm_password'].setErrors({'incorrect': false});
      this.PassMessage = "";
      // console.log("matching");
    }
    // console.log(this.passwordMismatch);
    var data=this.changepassword.value;
    data["email"] = this.email; 
    data["role_name"] = this.rode_name; 
    var url = 'web_change_password/'                                           //api url of edit api                                    
    this.ajax.postdata(url, this.changepassword.value)
      .subscribe((result) => {
        if (result.response.response_code == "200")                   //if sucess
        {
          // this.ngOnInit();                                                //reloading the component
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
          this.loading = false;
          this.web_error='';
        }
        else if (result.response.response_code == "500") {  
          this.web_error=result.response.message;               //if failiure
                 
          this.loading = false;
        }
        else { 
          this.web_error="Something Went Wrong";                                                            //if not sucess
              //toastr message for error
          this.loading = false;
        }
      }, (err) => {
        console.log(err);                                                 //prints if it encounters an error
      });
  }
  editprofile() {
    this.loadingSave = true; 
    this.chRef.detectChanges(); 
   var url = 'edit_user_profile/'   
                                      //api url of edit api                                    
    this.ajax.postdata(url, this.AddUser.value)
      .subscribe((result) => {
        if (result.response.response_code == "200")                   //if sucess
        {
          this.profile='';      
          this.loadingSave = false;                                           
          this.chRef.detectChanges(); 
          this.toastr.success(result.response.message, 'Success');        //toastr message for success
      
         
        }
        else if (result.response.response_code == "500") {  

          this.profile=result.response.message;               //if failiure
         this.loadingSave = false;
         this.chRef.detectChanges(); 
        }
        else {
          this.profile="Something Went Wrong";                                                             //if not sucess
          this.loadingSave = false;
          this.chRef.detectChanges(); 
        }
      }, (err) => {
        this.loadingSave = false;
        this.chRef.detectChanges(); 
        console.log(err);                                                 //prints if it encounters an error
      });

  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}



