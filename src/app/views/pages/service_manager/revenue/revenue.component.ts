import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { Router } from '@angular/router'; // To enable routing for this component
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; //To use create form group
import Swal from 'sweetalert2';
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { ExcelService } from '../../../../excelservice';
@Component({
	selector: 'kt-revenue',
	templateUrl: './revenue.component.html',
	styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {

	from: NgbDateStruct;
	to: NgbDateStruct;
	service_group: any;
	techId: any;
	assesment_data: any;
	technician_code: any;
	from_date: any;
	to_date: any;
	spare_report: any;
	employee_code: any;
	filterSpare: FormGroup;    //form group 
	products: any;
	locations: any;
	alltech: any;
	myDate: any;
	toDate: any;
	spares: any;
	spare_request_details: any;
	product_id: any;
	today: any;
	location_id: any;
	type: any;
	data: any;
	spare_amc: any;
	spare_revenue: any;
	spare_service: any;
	spare_details: any;
	ticket_data: any;
	technician_details: any;
	SpareTable: boolean = false;
	loadingSub: boolean;
	constructor(private ajax: ajaxservice, private modalService: NgbModal, private router: Router, private toastr: ToastrService, private excelservice: ExcelService, private calendar: NgbCalendar) {
		this.from = this.calendar.getToday();
		this.to = this.calendar.getToday();
		this.filterSpare = new FormGroup({
			"type": new FormControl(this.from, Validators.required),
			"from_date": new FormControl(this.from, Validators.required),
			"to_date": new FormControl(this.to, Validators.required),
			"technician_code": new FormControl('', Validators.required),
			"product_id": new FormControl('', Validators.required),
			"location_id": new FormControl('', Validators.required),
			"contract_type": new FormControl('', Validators.required),
		})

	}

	ngOnInit() {
		this.type = 'AMC Contract';
		console.log(this.type);
		var dob = new Date();
		var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
		console.log(Fdate);
		this.today = Fdate;
		console.log(this.today);
		// this.from_date=Fdate;
		// this.to_date=Fdate;
		var params = {
			"technician_code": null,
			"product_id": null,
			"to_date": Fdate,
			"from_date": Fdate,
			'location_id': null
		};
		this.data = {
			"from_date": Fdate,
			"to_date": Fdate,
			"technician_code": null,
			"product_id": null,
			"location_id": null,
		}
		this.getTech();
		this.getSpare(params);
		// this.getamc(params);
		this.get_ticket_product_location();

	}
	// onchange($event) {
	// 	console.log($event);
	// 	this.type = $event;
	// 	console.log(this.type);
	// }
	exportAsXLSX(): void {
		this.excelservice.exportAsExcelFile(this.spare_report, 'spare_reports');
	}
	getTech() {
		// var url = 'get_alltechnician_details/';                                  // api url for getting the details
		var url = 'technician_filter_data/';                                  // api url for getting the details
		this.ajax.getdata(url).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.alltech = result.response.data;              //storing the api response in the array
				//storing the api response in the array     
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	spareDetails(data) { //get Spare Details
		this.spares = data;
		console.log(data);
		var tk_id = this.spares.ticket_id;
		var ticket_id = "ticket_id";
		var url = 'spare_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, ticket_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_request_details = result.response.data; //bind result in this variable
				console.log(this.spare_request_details);
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	get_ticket_product_location() {
		var emp_code = localStorage.getItem('employee_code'); // get employee code 
		var data = { "employee_code": emp_code };
		var url = 'get_ticket_product_location/';                                  // api url for getting the details
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.products = result.response.product;              //storing the api response in the array
				this.locations = result.response.location;              //storing the api response in the array     
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	filterSpareF() {

		console.log(this.filterSpare)
		console.log(this.filterSpare.value)
		this.from_date = this.filterSpare.value.from_date;
		this.to_date = this.filterSpare.value.to_date;
		this.technician_code = this.filterSpare.value.technician_code;
		this.product_id = this.filterSpare.value.product_id;
		this.location_id = this.filterSpare.value.location_id;
		console.log(this.filterSpare.value);
		if (this.filterSpare.value.from_date.year == undefined) {
			this.filterSpare.value.from_date = this.filterSpare.value.from_date;
			console.log(this.filterSpare.value.from_date);
		}
		else {
			console.log(this.filterSpare.value.from_date)
			var Fyear = this.filterSpare.value.from_date.year;
			var Fmonth = this.filterSpare.value.from_date.month;
			var Fday = this.filterSpare.value.from_date.day;
			this.filterSpare.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
			console.log(this.filterSpare.value.from_date);
		}
		if (this.filterSpare.value.to_date.year == undefined) {
			this.filterSpare.value.to_date = this.today;
			console.log(this.filterSpare.value.to_date);
		}
		else {
			var Tyear = this.filterSpare.value.to_date.year;
			var Tmonth = this.filterSpare.value.to_date.month;
			var Tday = this.filterSpare.value.to_date.day;
			this.filterSpare.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;
			console.log(this.filterSpare.value.to_date);

		}
		if (this.product_id == "") {
			this.filterSpare.value.product_id = null;
		}
		else {
			this.filterSpare.value.product_id = this.filterSpare.value.product_id;
		}
		if (this.location_id == "") {
			this.filterSpare.value.location_id = null;
		}
		else {
			this.filterSpare.value.location_id = this.filterSpare.value.location_id;
		}
		if (this.technician_code == "") {
			this.filterSpare.value.technician_code = null;
		}
		else {
			this.filterSpare.value.technician_code = this.filterSpare.value.technician_code;
		}
		if (this.filterSpare.value.contract_type == 'AMC Contract') {


			this.getamc(this.filterSpare.value);
		}
		else if (this.filterSpare.value.contract_type == 'Service billing') {

			this.getservice(this.filterSpare.value);
		}
		else if (this.filterSpare.value.contract_type == 'Spare billing') {

			this.getrevenuespare(this.filterSpare.value);
		}
		this.getSpare(this.filterSpare.value);
		// this.getamc(this.filterSpare.value);
		// this.getrevenuespare(this.filterSpare.value);
		// this.getservice(this.filterSpare.value);
	}

	getSpare(data) {

		var url = 'spare_reports/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_report = result.response.data;


			}

		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getamc(data) {
		console.log(data)
		this.loadingSub = true;
		var url = 'get_revenue_contract_details/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_amc = result.response.data;
				this.loadingSub = false;
			}
			else if (result.response.response_code == "500") {
				this.loadingSub = false;
			}
			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getrevenuespare(data) {
		this.loadingSub = true;
		var url = 'get_revenue_spare_details/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_revenue = result.response.data;
				this.loadingSub = false;
			}
			else if (result.response.response_code == "500") {
				this.loadingSub = false;
			}
			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getservice(data) {
		this.loadingSub = true;
		var url = 'get_revenue_service_details/';                                  // api url for getting the cities
		this.ajax.postdata(url, data).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_service = result.response.data;
				this.loadingSub = false;
			}
			else if (result.response.response_code == "500") {
				this.loadingSub = false;
			}
			else {
				this.toastr.error(result.response.message, 'Error');
				this.loadingSub = false;
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	// Function to add and edit the modal box 
	openLarge(spare) {

		this.modalService.open(spare, {
			size: 'lg',                                                       //specifies the size of the modal box
			windowClass: "center-modalsm"      //modal popup custom size     
		});
	}
	ticketDetails(ticket_id, ticket_details) { //get Spare Details
		var tk_id = ticket_id;
		var tickets_id = "ticket_id";
		var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
		this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.ticket_data = result.response.data; //bind result in this variable
				this.modalService.open(ticket_details, {
					size: 'lg',
					windowClass: "center-modalsm",
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				},
				)
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	customerDetails(technician_id, tech_details) {
		this.techId = technician_id;
		var technician_code = "technician_code";
		var url = 'get_technician_data/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, technician_code, technician_id).subscribe((result) => {
			if (result.response.response_code == "200") {
				this.technician_details = result.response.data; //bind results in this variable
				this.modalService.open(tech_details, {
					size: 'lg',
					windowClass: "center-modalsm",
					backdrop: 'static',                                    // modal will not close by outside click
					keyboard: false,                                       // modal will not close by keyboard click
				}

				)
			}
			else if (result.response.response_code == "400") {
				this.technician_details = result.response.data; //bind results in this variable
			}
		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
	getspare(item, spares) {
		console.log(item);
		// var spare={"ticket_id":item};
		var id = "ticket_id";
		var spare = item;
		var url = 'spare_details/?';                                  // api url for getting the cities
		this.ajax.getdataparam(url, id, spare).subscribe((result) => {
			if (result.response.response_code == "200" || result.response.response_code == "400") {
				this.spare_details = result.response.data;
				this.SpareTable = true;
				this.modalService.open(spares, {
					size: 'lg',
					// windowClass: "center-modalsm" 
				})
			}
			else if (result.response.response_code == "500") {
				this.toastr.error(result.response.data, 'Error');
				this.modalService.dismissAll();
			}

		}, (err) => {
			console.log(err);                                        //prints if it encounters an error
		});
	}
}
