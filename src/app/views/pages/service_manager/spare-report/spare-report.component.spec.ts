import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpareReportComponent } from './spare-report.component';

describe('SpareReportComponent', () => {
  let component: SpareReportComponent;
  let fixture: ComponentFixture<SpareReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpareReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpareReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
