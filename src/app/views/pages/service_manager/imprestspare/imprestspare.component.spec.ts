import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprestspareComponent } from './imprestspare.component';

describe('ImprestspareComponent', () => {
  let component: ImprestspareComponent;
  let fixture: ComponentFixture<ImprestspareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImprestspareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprestspareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
