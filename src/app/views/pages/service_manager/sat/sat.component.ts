import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ajaxservice } from '../../../../ajaxservice'; // Common API service for both get and post
import { FormGroup, FormControl, Validators, NgModel, } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';// To use modal boxes
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { formatDate } from '@angular/common';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoaderComponent } from '../../../pages/loader/loader.component';
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'kt-sat',
  templateUrl: './sat.component.html',
  styleUrls: ['./sat.component.scss']
})
export class SatComponent implements OnInit {
  satsla: any;
  Location: FormGroup;
  actual: any;
  target: any;
  product_id: any;
  date_format: any;
  location_id: any;
  locations: any;
  products: any;
  from: any;
  to: any;
  showTableSat: boolean = false;
  sla_feed: any;
  technician_code: any;
  today: any;
  from_date: any;
  to_date: string;
  ticket_data: any;
  technicians_details: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  customer_details: any;
  contract_detail: any;
  c_sat_feedback: any;
  Actual_SLA_Percent: any;
  feedback_actual: any;
  c_sat_rating: any;
  feedback_target: any;
  rating_actual: any;
  rating_target: any;
  c_sat_sla: any;
  SLA_Compliance_Target: any;
  loadingSub: boolean = false;
  overlayRef: OverlayRef;
  LoaderComponentPortal: ComponentPortal<LoaderComponent>;

  constructor(private ajax: ajaxservice, private calendar: NgbCalendar, private modalService: NgbModal, private chRef: ChangeDetectorRef, private overlay: Overlay, private toastr: ToastrService,) {
    this.from = this.calendar.getToday();
    console.log(this.from);
    var y = this.from.year;
    var m = this.from.month;
    var d = this.from.day;
    var from_date = y + "-" + m + "-" + d;
    console.log(from_date);
    this.to = this.calendar.getToday();
    this.technician_code = localStorage.getItem('employee_code');
    this.Location = new FormGroup({
      "technician_code": new FormControl(this.technician_code, Validators.required),
      "from_date": new FormControl(this.from, Validators.required),
      "to_date": new FormControl(this.to, Validators.required),
      "product_id": new FormControl('', Validators.required),
      "location_id": new FormControl('', Validators.required),
      // "date_format": new FormControl('', Validators.required)
    })
 
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true
    }
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
    this.LoaderComponentPortal = new ComponentPortal(LoaderComponent);
    // this.overlayRef.attach(this.LoaderComponentPortal);

    // this.getcSatComponent(date);
    this.get_ticket_product_location()
    this.product_id = null;
    this.location_id = null;
    this.date_format = null;
    var Fdate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
    this.today = Fdate;
    console.log(Fdate);

    var data = {
      "product_id": null,
      "location_id": null,
      "from_date": Fdate,
      "to_date": Fdate

    }
    var Year = new Date().getFullYear();
    var month = new Date().getMonth();
    var date = {
      "month": month,
      "year": Year
    }
    this.getcSatComponent(date);
    this.getfeed(data);
    // this.date();

  }
  getcSatComponent(date) {
    var data = date;
    var url = 'c_sat_data/';                                  // api url for getting the cities
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {

        this.c_sat_feedback = result.response.c_sat_feedback;
        this.feedback_actual = this.c_sat_feedback.feedback_actual;
        this.feedback_target = this.c_sat_feedback.feedback_target;
        this.c_sat_rating = result.response.c_sat_rating;
        this.rating_actual = this.c_sat_rating.rating_actual;
        this.rating_target = this.c_sat_rating.rating_target;
        this.c_sat_sla = result.response.c_sat_sla;
        this.Actual_SLA_Percent = this.c_sat_sla.Actual_SLA_Percent;
        this.SLA_Compliance_Target = this.c_sat_sla.SLA_Compliance_Target;
       
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the product and location details
  get_ticket_product_location() {
    var emp_code = localStorage.getItem('employee_code'); // get employee code 
    var data = {"employee_code":emp_code};
    var url = 'get_ticket_product_location/';                                  // api url for getting the details
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.products = result.response.product;              //storing the api response in the array
        this.locations = result.response.location;              //storing the api response in the array     
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // function for getting the Technician details based on TechnicianId 
  get_technician_details(ticket_id, technician_id, view_techniciandetails) {
    var ticket_id = ticket_id;
    var technician_id = technician_id;
    var url = 'get_technician_details/?';             // api url for getting the details with using get params
    var id = "ticket_id";
    var id1 = "technician_id";
    this.ajax.getdatalocation(url, id, ticket_id, technician_id, id1).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.technicians_details = result.response.data;
        console.log(this.technicians_details);
        this.modalService.open(view_techniciandetails, {
          size: 'lg',
          windowClass: "center-modalsm",                                    // modal popup for resizing the popup
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
      }

    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  // --------------------------------function for view the modal box------------------------------------------------------//
  openmodal(modal) {
    this.modalService.open(modal, {
      size: 'lg',
      windowClass: "center-modalsm"                                    // modal popup for resizing the popup
    });
  }
  get log() {
    return this.Location.controls;                       //error logs for create user
  }
  //onchange functoin for getting the product id
  date() {
    this.loadingSub = true;
    this.showTableSat = false;
    this.chRef.detectChanges();
    //alert( this.showTableSat)
    console.log(this.Location.value.from_date);
    if (this.Location.value.from_date.year == undefined) {
      this.Location.value.from_date = this.Location.value.from_date;
      console.log(this.Location.value.from_date);
    }
    else {
      console.log(this.Location.value.from_date)
      var Fyear = this.Location.value.from_date.year;
      var Fmonth = this.Location.value.from_date.month;
      var Fday = this.Location.value.from_date.day;
      this.Location.value.from_date = Fyear + "-" + Fmonth + "-" + Fday;
      console.log(this.Location.value.from_date);
    }
    if (this.Location.value.to_date.year == undefined) {
      this.Location.value.to_date = this.today;
      console.log(this.Location.value.to_date);
      console.log(this.Location.value.to_date);
    }
    else {
      var Tyear = this.Location.value.to_date.year;
      var Tmonth = this.Location.value.to_date.month;
      var Tday = this.Location.value.to_date.day;
      this.Location.value.to_date = Tyear + "-" + Tmonth + "-" + Tday;
      console.log(this.Location.value.to_date);

    }
    if (this.Location.value.location_id == "") {

      this.Location.value.location_id = null;
    }
    else {
      this.location_id = this.Location.value.location_id;
    }
    if (this.Location.value.product_id == "") {
      this.Location.value.product_id = null;
    }
    else {
      this.product_id = this.Location.value.product_id;
    }
    
    this.getfeed(this.Location.value);
  }
  getfeed(data) {
 
    this.overlayRef.attach(this.LoaderComponentPortal);
    var url = 'get_c_sat_details/';                                  // api url for getting the cities
    this.ajax.postdata(url, data).subscribe((result) => {
      if (result.response.response_code == "200" || result.response.response_code == "400") {
        this.sla_feed = result.response.data;
        this.showTableSat = true;
        this.loadingSub = false;
        //alert( this.showTableSat)
        this.chRef.detectChanges();
        this.overlayRef.detach();
      }
      else if (result.response.response_code == "500") {
        this.showTableSat = true;
        this.loadingSub = false;
        this.overlayRef.detach();
      }

      else {
        this.showTableSat = true;
        this.chRef.detectChanges();
        this.overlayRef.detach();
        this.toastr.error(result.response.message, 'Error');
        this.loadingSub = false;
      }
    }, (err) => {
      this.loadingSub = false;
      this.showTableSat = true;
      this.chRef.detectChanges();
      this.overlayRef.detach();
      console.log(err);                                        //prints if it encounters an error
    });
  }
  fetchNews(evt: any) {

    console.log(evt); // has nextId that you can check to invoke the desired function
  }
  ticketDetails(ticket_id, ticket_details) { //get Spare Details
    var tk_id = ticket_id;
    var tickets_id = "ticket_id";
    var url = 'get_ticket_details/?';                                  // api url for getting the ticket details
    this.ajax.getdataparam(url, tickets_id, tk_id).subscribe((result) => {
      if (result.response.response_code == "200") {
        this.ticket_data = result.response.data; //bind result in this variable
        this.modalService.open(ticket_details, {
          size: 'lg',
          windowClass: "center-modalsm",                                    // modal popup for resizing the popup
          backdrop: 'static',                                    // modal will not close by outside click
          keyboard: false,                                       // modal will not close by keyboard click
        });
      }
    }, (err) => {
      console.log(err);                                        //prints if it encounters an error
    });
  }
  customerDetails(customer_code, customerdetails) {
    var id = "customer_code";
    var parameter = customer_code;
    var url = 'get_amc_contract_details/?';
    this.ajax.getdataparam(url, id, parameter).subscribe((result) => {
      if (result.response.response_code == "200") {
        console.log(result.response.contract_details)
        console.log(result.response.customer_details)
        this.customer_details = result.response.customer_details;
        this.contract_detail = result.response.contract_details;
        this.modalService.open(customerdetails, {
          size: 'lg',
          windowClass: "center-modalsm"                                    // modal popup for resizing the popup
        });
      }
      (err) => {
        console.log(err);                                             //prints if it encounters an error
      }
    });
  }
}
